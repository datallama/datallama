/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.util.dbre;

import java.util.ArrayList;

public class DbreSchema {
    private String schema_name = "";
    private String shortdesc = "";
    private ArrayList<DbreTableView> table_view_list;
    private ArrayList<DbreRelationship> relationship_list;

    public DbreSchema(String schema_name, String shortdesc) {
        this.schema_name = schema_name;
        this.shortdesc = shortdesc;
        this.table_view_list = new ArrayList<DbreTableView>();
        this.relationship_list = new ArrayList<DbreRelationship>();
    }

    public String getSchema_name() {
        return schema_name;
    }

    public void setSchema_name(String schema_name) {
        this.schema_name = schema_name;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public ArrayList<DbreTableView> getTable_view_list() {
        return table_view_list;
    }

    public void setTable_view_list(ArrayList<DbreTableView> table_view_list) {
        this.table_view_list = table_view_list;
    }

    public void addTableViewDefn(DbreTableView tableViewDefn) {
        this.table_view_list.add(tableViewDefn);
    }

    public ArrayList<DbreRelationship> getRelationship_list() {
        return relationship_list;
    }

    public void setRelationship_list(ArrayList<DbreRelationship> relationship_list) {
        this.relationship_list = relationship_list;
    }

    public void addRelationshipDefn(DbreRelationship relationshipDefn) {
        this.relationship_list.add(relationshipDefn);
    }
}
