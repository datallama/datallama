/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.util.dbre;

public class DbreColumn {
    private String column_name = "";
    private String shortdesc = "";
    private Integer ordinal = 0;
    private String datatype = "";
    private Integer scale = 0;
    private Integer precision = 0;
    private Integer length = 0;
    private String datatype_formatted = "";
    private Boolean nullable = false;
    private Boolean primary_key = false;
    private Boolean auto_increment = false;

    /**
     * Constructor for formatted datatype definitions. Scale, precision, length and auto_increment are not included in parameter list.
     * @param column_name
     * @param shortdesc
     * @param ordinal
     * @param datatype
     * @param nullable
     * @param primary_key
     */
    public DbreColumn(String column_name, String shortdesc, Integer ordinal, String datatype,
                      Boolean nullable, Boolean primary_key) {
        this.column_name = column_name;
        this.shortdesc = shortdesc;
        this.ordinal = ordinal;
        this.datatype = datatype;
        this.scale = 0;
        this.precision = 0;
        this.length = 0;
        this.nullable = nullable;
        this.primary_key = primary_key;
        this.auto_increment = false;
    }

    /**
     * Constructor with full parameter list for all members.
     * @param column_name
     * @param shortdesc
     * @param ordinal
     * @param datatype
     * @param scale
     * @param precision
     * @param length
     * @param nullable
     * @param primary_key
     * @param auto_increment
     */
    public DbreColumn(String column_name, String shortdesc, Integer ordinal, String datatype, Integer scale,
                      Integer precision, Integer length, Boolean nullable, Boolean primary_key, Boolean auto_increment) {
        this.column_name = column_name;
        this.shortdesc = shortdesc;
        this.ordinal = ordinal;
        this.datatype = datatype;
        this.scale = scale;
        this.precision = precision;
        this.length = length;
        this.nullable = nullable;
        this.primary_key = primary_key;
        this.auto_increment = auto_increment;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public Integer getScale() {
        return scale;
    }

    public void setScale(Integer scale) {
        this.scale = scale;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getDatatype_formatted() {
        return datatype_formatted;
    }

    public void setDatatype_formatted(String datatype_formatted) {
        this.datatype_formatted = datatype_formatted;
    }

    public Boolean getNullable() {
        return nullable;
    }

    public void setNullable(Boolean nullable) {
        this.nullable = nullable;
    }

    public Boolean getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(Boolean primary_key) {
        this.primary_key = primary_key;
    }

    public Boolean getAuto_increment() {
        return auto_increment;
    }

    public void setAuto_increment(Boolean auto_increment) {
        this.auto_increment = auto_increment;
    }

}
