/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.util.dbre;

import java.util.ArrayList;

public class DbreTableView {
    private String table_view_name = "";
    private String shortdesc = "";
    private String table_view_type = "";
    private ArrayList<DbreColumn> column_list;

    public DbreTableView(String table_view_name, String shortdesc, String table_view_type) {
        this.table_view_name = table_view_name;
        this.shortdesc = shortdesc;
        this.table_view_type = table_view_type;
        this.column_list = new ArrayList<DbreColumn>();
    }

    public String getTable_view_name() {
        return table_view_name;
    }

    public void setTable_view_name(String table_view_name) {
        this.table_view_name = table_view_name;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public String getTable_view_type() {
        return table_view_type;
    }

    public void setTable_view_type(String table_view_type) {
        this.table_view_type = table_view_type;
    }

    public ArrayList<DbreColumn> getColumn_list() {
        return column_list;
    }

    public void setColumn_list(ArrayList<DbreColumn> column_list) {
        this.column_list = column_list;
    }

    public void addColumnDefn(DbreColumn columnDefn) {
        this.column_list.add(columnDefn);
    }
}
