/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.util.dbre;

import java.util.ArrayList;

public class DbreJsonDefn {
    private String dl_json_type = "";
    private String extract_create_dt = "";
    private String extract_author = "";
    private String db_name = "";
    private ArrayList<DbreSchema> schema_list;

    public DbreJsonDefn() {
        dl_json_type = "dlDbSmd";
        extract_create_dt = "2022-03-12 17:28:00";
        schema_list = new ArrayList<DbreSchema>();
    }

    public String getDl_json_type() {
        return dl_json_type;
    }

    public void setDl_json_type(String dl_json_type) {
        this.dl_json_type = dl_json_type;
    }

    public String getExtract_create_dt() {
        return extract_create_dt;
    }

    public void setExtract_create_dt(String extract_create_dt) {
        this.extract_create_dt = extract_create_dt;
    }

    public String getExtract_author() {
        return extract_author;
    }

    public void setExtract_author(String extract_author) {
        this.extract_author = extract_author;
    }

    public String getDb_name() {
        return db_name;
    }

    public void setDb_name(String db_name) {
        this.db_name = db_name;
    }

    public ArrayList<DbreSchema> getSchema_list() {
        return schema_list;
    }

    public void setSchema_list(ArrayList<DbreSchema> schema_list) {
        this.schema_list = schema_list;
    }

    public void addSchemaDefn(DbreSchema schemaDefn) {
        this.schema_list.add(schemaDefn);
    }
}
