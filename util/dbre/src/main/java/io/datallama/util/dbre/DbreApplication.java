package io.datallama.util.dbre;

import io.datallama.api.smd.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

/**
 * The DbreApplication is used to reverse engineer a databases and generate a DlDbJson formated JSON file that contains
 * structural metadata definitions that can be loaded into Data Llama.
 */
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class
})
public class DbreApplication implements ApplicationRunner {

    private static Logger LOG = LoggerFactory
            .getLogger(DbreApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DbreApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        String configFilename = "";
        RevEngResult revEngResult = new RevEngResult();
        ObjectMapper objectMapper = new ObjectMapper();

        revEngResult.setResultStatus(1);

        /*String[] clArgs = args.getSourceArgs();
        for (int i = 0; i < clArgs.length; ++i) {
            LOG.info("args[{}]: {}", i, clArgs[i]);
        }*/

        // 1. process command line options and arguments. The --config option specifies a DB reverse engineering config file
        if (args.containsOption("config")) {
            configFilename = args.getOptionValues("config").get(0);
        }
        else
            configFilename = "dbre_config.json";

        LOG.info("configFilename = {}", configFilename);

        // 2. Open the configuration file and parse to JSON
        File jsonFile = new File(configFilename);
        JsonNode rootNode = null;
        try {
            rootNode = objectMapper.readTree(jsonFile);
        } catch(Exception e) {
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(-200350);
            String msg = String.format("Error encountered when trying to parse the config JSON file. Check that file %s exists and is a valid JSON file.", jsonFile.getAbsolutePath());
            revEngResult.setGeneralErrorMessage(msg);
            revEngResult.setSqlErrorMessage(e.getMessage());
        }

        // 3. process each database specification in the db_list JSON array
        if (revEngResult.getResultStatus() == 1) {
            ArrayNode dbList = (ArrayNode) rootNode.get("db_list");
            if (dbList != null) {
                Iterator<JsonNode> dbSpecs = dbList.elements();

                while (dbSpecs.hasNext()) {
                    JsonNode dbSpec = dbSpecs.next();
                    String dbName = dbSpec.get("db_name").asText();
                    String outputFile = dbSpec.get("output_dldbjson_filename").asText();

                    ArrayNode schemaList = (ArrayNode) dbSpec.get("schema_list");
                    Iterator<JsonNode> jsonSchemaNames = schemaList.elements();

                    String[] schemaNames = null;
                    ArrayList<String> alSchemaNames = new ArrayList<String>();
                    if (jsonSchemaNames != null) {
                        while (jsonSchemaNames.hasNext()) {
                            JsonNode jsonSchemaName = jsonSchemaNames.next();
                            alSchemaNames.add(jsonSchemaName.asText());
                        }
                        schemaNames = new String[alSchemaNames.size()];
                        for (int i = 0; i < alSchemaNames.size(); i++) {
                            schemaNames[i] = alSchemaNames.get(i);
                        }
                    }
                    else {
                        revEngResult.setResultStatus(-1);
                        revEngResult.setErrorNumber(-200352);
                        revEngResult.setGeneralErrorMessage("Error: The configuration file is not a valid DbreApplication JSON file. " +
                                "The schema_list array element is missing for database " + dbName);
                    }

                    // 3.1. create an SmdDbCredentials object from the credentials specified in the config file
                    if (revEngResult.getResultStatus() == 1 && schemaNames != null) {
                        try {
                            SmdDbCredentials dbCredentials = new SmdDbCredentials(
                                    0,
                                    dbSpec.get("db_hostname").asText(),
                                    dbName,
                                    dbSpec.get("db_port").asInt(),
                                    dbSpec.get("db_user").asText(),
                                    dbSpec.get("db_pwd").asText(),
                                    dbSpec.get("jdbc_driver_family").asText()
                            );

                            // 3.2. instantiate the appropriate reverse engineering class for the JDBC driver family
                            ReverseEngineerDatabase revEngInterface = null;
                            SmdDatabaseDefinition dbDefn = null;

                            switch (dbCredentials.getJdbcDriverFamily()) {
                                case "mysql":
                                    revEngInterface = new RevEngMysql();
                                    break;
                                case "postgresql":
                                    revEngInterface = new RevEngPostgresql();
                                    break;
                                case "sqlserver":
                                    revEngInterface = new RevEngMsSqlServer();
                                    break;
                                case "oracle":
                                    revEngInterface = new RevEngOracle();
                                    break;
                                default:
                                    revEngResult.setResultStatus(-200201);
                                    throw new IllegalStateException("Unexpected value: " + dbCredentials.getJdbcDriverFamily());
                            }

                            // 3.3. execute the reverse engineer and get the db catalog
                            if (revEngResult.getResultStatus() == 1) {
                                try {
                                    RevEngResult connectorResult = revEngInterface.reverseEngineerDatabase(dbCredentials, schemaNames);

                                    if (connectorResult.getErrorNumber() == 0) {
                                        dbDefn = revEngInterface.getReverseEngineerCatalog();
                                        LOG.info("Reverse engineering database {}", dbDefn.getDatabaseName());

                                        // 3.4. Create new Json root node, populate with dbDefn and save to file
                                        DbreJsonDefn jsonDefn = new DbreJsonDefn();
                                        jsonDefn.setDb_name(dbDefn.getDatabaseName());
                                        jsonDefn.setExtract_author(System.getProperty("user.name", "user_unkown"));

                                        JsonNode dlDbRevEngRoot = objectMapper.valueToTree(jsonDefn);

                                        // 3.4.1. process each reverse engineered schema
                                        ArrayList<SmdSchemaDefinition> smdSchemaList = dbDefn.getSchemaList();
                                        for (SmdSchemaDefinition smdSchemaDefn : smdSchemaList) {
                                            DbreSchema dbreSchemaDefn = new DbreSchema(smdSchemaDefn.getSchemaName(), smdSchemaDefn.getSchemaShortDesc());

                                            // 3.4.1.1. process each reverse engineered table/view
                                            ArrayList<SmdTableViewDefinition> smdTableViewList = smdSchemaDefn.getTableViewList();
                                            for (SmdTableViewDefinition smdTableViewDefn : smdTableViewList) {

                                                DbreTableView dbreTableViewDefn = new DbreTableView(
                                                        smdTableViewDefn.getTableViewName(),
                                                        smdTableViewDefn.getTableViewShortDesc(),
                                                        smdTableViewDefn.getClassCode().substring(3).toLowerCase());

                                                // 3.4.1.1.1. process each reverse engineered column
                                                ArrayList<SmdColumnDefinition> smdColumnList = smdTableViewDefn.getColumnList();
                                                for (SmdColumnDefinition smdColumnDefn : smdColumnList) {
                                                    DbreColumn dbreColumnDefn = new DbreColumn(
                                                            smdColumnDefn.getColumnName(),
                                                            smdColumnDefn.getColumnShortDesc(),
                                                            smdColumnDefn.getColumnOrdinal(),
                                                            smdColumnDefn.getColumnDatatype(),
                                                            smdColumnDefn.getNullable(),
                                                            smdColumnDefn.getPrimaryKey()
                                                    );
                                                    dbreColumnDefn.setDatatype_formatted(smdColumnDefn.getColumnDatatype());

                                                    // add the table/view definition to the schema definition
                                                    dbreTableViewDefn.addColumnDefn(dbreColumnDefn);
                                                }

                                                // add the table/view definition to the schema definition
                                                dbreSchemaDefn.addTableViewDefn(dbreTableViewDefn);

                                            }

                                            // 3.4.1.2. process each reverse engineered relationship
                                            ArrayList<SmdRelationshipDefinition> smdRelationshipList = smdSchemaDefn.getRelationshipList();
                                            for (SmdRelationshipDefinition smdRelDefn : smdRelationshipList) {
                                                DbreRelationship dbreRelDefn = new DbreRelationship(
                                                        smdRelDefn.getRelationshipName(),
                                                        smdRelDefn.getParentTableViewName(),
                                                        smdRelDefn.getParentTableViewSchemaName(),
                                                        smdRelDefn.getChildTableViewName(),
                                                        smdRelDefn.getChildTableViewSchemaName()
                                                );

                                                // add the column pairs
                                                ArrayList<SmdRelationshipColumnPair> smdColPairList = smdRelDefn.getColumnPairs();
                                                for (SmdRelationshipColumnPair smdColPair : smdColPairList) {
                                                    DbreRelationshipColumnPair dbreColPair = new DbreRelationshipColumnPair(
                                                            smdColPair.getParentColumnName(),
                                                            smdColPair.getChildColumnName()
                                                    );
                                                    dbreRelDefn.addColumn_pair(dbreColPair);
                                                }

                                                // add the relationship definition to the schema
                                                dbreSchemaDefn.addRelationshipDefn(dbreRelDefn);
                                            }

                                            // add the schema definition to the JSON output definition
                                            jsonDefn.addSchemaDefn(dbreSchemaDefn);
                                        }

                                        File dlDbRevEngOutFile = new File(outputFile);
                                        objectMapper.writeValue(dlDbRevEngOutFile, jsonDefn);
                                    }
                                    else {
                                        revEngResult.setResultStatus(-1);
                                        revEngResult.setErrorNumber(connectorResult.getErrorNumber());
                                        revEngResult.setGeneralErrorMessage(connectorResult.getGeneralErrorMessage());
                                    }
                                } catch (Exception e) {
                                    revEngResult.setResultStatus(-1);
                                    revEngResult.setErrorNumber(-200354);
                                    revEngResult.setGeneralErrorMessage("Error invoking reverse engineering connector for JDBC driver family " + dbCredentials.getJdbcDriverFamily());
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            revEngResult.setResultStatus(-1);
                            revEngResult.setErrorNumber(-200353);
                            String msg = String.format("Error encountered when trying to parse connection details for database %s.", dbName);
                            revEngResult.setGeneralErrorMessage(msg);
                            revEngResult.setSqlErrorMessage(e.getMessage());
                        }
                    }

                }
            }
            else {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200351);
                revEngResult.setGeneralErrorMessage("Error: The configuration file is not a valid DbreApplication JSON file. The db_list array element is missing.");
            }
        }

        // exit the application
        if (revEngResult.getResultStatus() != 1) {
            LOG.info("ERROR: Generation of DlDbJson file failed with the following error:");
            LOG.info(String.valueOf(revEngResult.getErrorNumber()));
            LOG.info(revEngResult.getGeneralErrorMessage());
            LOG.info(revEngResult.getSqlErrorMessage());
        }
        else {
            LOG.info("SUCCESS. Generation of DlDbJson file for database schema completed.");
        }


    }

}
