/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.util.dbre;

public class DbreRelationshipColumnPair {
    private String parent_column_name;
    private String child_column_name;

    /**
     * Constructor without primary key indicator params
     * @param parent_column_name
     * @param child_column_name
     */
    public DbreRelationshipColumnPair(String parent_column_name, String child_column_name) {
        this.parent_column_name = parent_column_name;
        this.child_column_name = child_column_name;
    }

    public String getParent_column_name() {
        return parent_column_name;
    }

    public void setParent_column_name(String parent_column_name) {
        this.parent_column_name = parent_column_name;
    }

    public String getChild_column_name() {
        return child_column_name;
    }

    public void setChild_column_name(String child_column_name) {
        this.child_column_name = child_column_name;
    }

}
