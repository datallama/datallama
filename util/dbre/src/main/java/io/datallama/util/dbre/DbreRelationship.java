/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.util.dbre;

import java.util.ArrayList;

public class DbreRelationship {
    private String relationship_name;
    private String parent_table_name;
    private String parent_table_schema_name;
    private String child_table_name;
    private String child_table_schema_name;
    private String relationship_short_desc;
    private ArrayList<DbreRelationshipColumnPair> column_pair_list;

    /**
     * Constructor
     * @param relationship_name
     * @param parent_table_name
     * @param parent_table_schema_name
     * @param child_table_name
     * @param child_table_schema_name
     */
    public DbreRelationship(String relationship_name, String parent_table_name, String parent_table_schema_name,
                            String child_table_name, String child_table_schema_name) {
        this.relationship_name = relationship_name;
        this.parent_table_name = parent_table_name;
        this.parent_table_schema_name = parent_table_schema_name;
        this.child_table_name = child_table_name;
        this.child_table_schema_name = child_table_schema_name;
        this.relationship_short_desc = "";
        this.column_pair_list = new ArrayList<DbreRelationshipColumnPair>();
    }

    public String getRelationship_name() {
        return relationship_name;
    }

    public void setRelationship_name(String relationship_name) {
        this.relationship_name = relationship_name;
    }

    public String getParent_table_name() {
        return parent_table_name;
    }

    public void setParent_table_name(String parent_table_name) {
        this.parent_table_name = parent_table_name;
    }

    public String getParent_table_schema_name() {
        return parent_table_schema_name;
    }

    public void setParent_table_schema_name(String parent_table_schema_name) {
        this.parent_table_schema_name = parent_table_schema_name;
    }

    public String getChild_table_name() {
        return child_table_name;
    }

    public void setChild_table_name(String child_table_name) {
        this.child_table_name = child_table_name;
    }

    public String getChild_table_schema_name() {
        return child_table_schema_name;
    }

    public void setChild_table_schema_name(String child_table_schema_name) {
        this.child_table_schema_name = child_table_schema_name;
    }

    public String getRelationship_short_desc() {
        return relationship_short_desc;
    }

    public void setRelationship_short_desc(String relationship_short_desc) {
        this.relationship_short_desc = relationship_short_desc;
    }

    public ArrayList<DbreRelationshipColumnPair> getColumn_pair_list() {
        return column_pair_list;
    }

    public void setColumn_pair_list(ArrayList<DbreRelationshipColumnPair> column_pair_list) {
        this.column_pair_list = column_pair_list;
    }

    public void addColumn_pair(DbreRelationshipColumnPair column_pair) {
        this.column_pair_list.add(column_pair);
    }

}

