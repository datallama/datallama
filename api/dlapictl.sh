#!/usr/bin/bash


if [ $# -ne 1 ]
then
  echo -e "ERROR: incorrect number of arguments. You must specify one of start|stop|status."
  exit 10
else
  ACTION=$1
fi

case ${ACTION} in
  start)
    echo "starting the Data Llama API (dl-api.jar)"
    java --add-opens java.base/java.lang=ALL-UNNAMED -Dfile.encoding=UTF-8 -jar /opt/datallama/api/dl-api.jar > /opt/datallama/api/log/dl-api.log &
    CHECK=`ps -ef | grep dl-api.jar | grep java | wc -l`
    if [ ${CHECK} -eq 1 ]
    then
      echo "Data Llama API successfully started."
      ps -ef | grep dl-api.jar | grep java
    else
      echo "ERROR: there was a problem starting the Data Llama API"
    fi
    ;;
  stop)
    echo "stopping the Data Llama API (dl-api.jar)"
    DL_API_PID=`ps -ef | grep dl-api.jar | grep java | awk '{print $2}'`
    if [ ${#DL_API_PID} -eq 0 ]
    then
      echo "Data Llama API is not running so cannot be stopped."
    else
      kill ${DL_API_PID}

      echo "waiting for the Data Llama API to shutdown ..."
      sleep 5
      CHECK=`ps -ef | grep dl-api.jar | grep java | wc -l`
      if [ ${CHECK} -eq 0 ]
      then
        echo "Data Llama API successfully stopped."
      else
        echo "ERROR: there was a problem stopping the Data Llama API"
      fi
    fi
    ;;
  status)
    echo "checking run status of the Data Llama API (dl-api.jar)"
    ps -ef | grep dl-api.jar | grep java
    ;;
  *)
    echo -e "ERROR: incorrect action specified. You must specify one of start|stop|status."
    exit 11
    ;;
esac

exit 0

