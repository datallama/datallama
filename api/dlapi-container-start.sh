#!/usr/bin/bash

echo "starting the Data Llama API (dl-api.jar)"
java --add-opens java.base/java.lang=ALL-UNNAMED -Dfile.encoding=UTF-8 -jar /opt/datallama/api/dl-api.jar > /opt/datallama/api/log/dl-api.log

