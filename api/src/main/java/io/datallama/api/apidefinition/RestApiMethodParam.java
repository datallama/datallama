/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

public class RestApiMethodParam {
    private String paramName;
    private String paramDescription;
    private String paramDefinitionType;
    private String paramDatatype;

    /**
     * Default constructor with no parameters.
     */
    public RestApiMethodParam() {
        this.paramName = "";
        this.paramDescription = "";
        this.paramDefinitionType = "";
        this.paramDatatype = "";
    }

    /**
     * Constructor with all parameters
     * @param paramName
     */
    public RestApiMethodParam(String paramName, String paramDescription, String paramDefinitionType, String paramDatatype) {
        this.paramName = paramName;
        this.paramDescription = paramDescription;
        this.paramDefinitionType = paramDefinitionType;
        this.paramDatatype = paramDatatype;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamDescription() {
        return paramDescription;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

    public String getParamDefinitionType() {
        return paramDefinitionType;
    }

    public void setParamDefinitionType(String paramDefinitionType) {
        this.paramDefinitionType = paramDefinitionType;
    }

    public String getParamDatatype() {
        return paramDatatype;
    }

    public void setParamDatatype(String paramDatatype) {
        this.paramDatatype = paramDatatype;
    }
}
