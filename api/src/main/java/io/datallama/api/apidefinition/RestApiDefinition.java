/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

import java.util.ArrayList;

public class RestApiDefinition {
    private Integer objectId;
    private Integer classId;
    private String apiSpecificationStandard;
    private String apiDescription;
    private String apiVersion;
    private ArrayList<RestApiPathSegment> pathElementList;

    /**
     * Default constructor with no parameters.
     */
    public RestApiDefinition() {
        this.objectId = 0;
        this.classId = 0;
        this.apiSpecificationStandard = "NONE";
        this.apiDescription = "";
        this.apiVersion = "";
        this.pathElementList = new ArrayList<RestApiPathSegment>();
    }

    /**
     * Constructor with required parameters.
     * @param objectId
     * @param classId
     * @param apiSpecificationStandard
     */
    public RestApiDefinition(Integer objectId, Integer classId, String apiSpecificationStandard) {
        this.objectId = objectId;
        this.classId = classId;
        this.apiSpecificationStandard = apiSpecificationStandard;
        this.apiDescription = "";
        this.apiVersion = "";
        this.pathElementList = new ArrayList<RestApiPathSegment>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getApiSpecificationStandard() {
        return apiSpecificationStandard;
    }

    public void setApiSpecificationStandard(String apiSpecificationStandard) {
        this.apiSpecificationStandard = apiSpecificationStandard;
    }

    public String getApiDescription() {
        return apiDescription;
    }

    public void setApiDescription(String apiDescription) {
        this.apiDescription = apiDescription;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public ArrayList<RestApiPathSegment> getPathElementList() {
        return pathElementList;
    }

    public void setPathElementList(ArrayList<RestApiPathSegment> pathElementList) {
        this.pathElementList = pathElementList;
    }

    public void addPathElement(RestApiPathSegment pathElement) {
        this.pathElementList.add(pathElement);
    }

}
