/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.apidefinition;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlApiDefinition {
    private Integer objectId;
    private Integer classId;
    private String className;
    private String objectName;
    private String objectShortDesc;
    private String objectTable;
    private String objectDetailState;
    private Object apiBaseConfig;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private DlCustomForm customForm;


    public DlApiDefinition() {
        this.objectId = 0;
        this.classId = 0;
        this.className = "";
        this.objectName = "";
        this.objectShortDesc = "";
        this.objectTable = "";
        this.objectDetailState = "";
        this.apiBaseConfig = null;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    public DlApiDefinition(Integer objectId, Integer classId, String objectName, String objectShortDesc) {
        this.objectId = objectId;
        this.classId = classId;
        this.className = "";
        this.objectName = objectName;
        this.objectShortDesc = objectShortDesc;
        this.objectTable = "";
        this.objectDetailState = "";
        this.apiBaseConfig = null;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public String getObjectTable() {
        return objectTable;
    }

    public void setObjectTable(String objectTable) {
        this.objectTable = objectTable;
    }

    public String getObjectDetailState() {
        return objectDetailState;
    }

    public void setObjectDetailState(String objectDetailState) {
        this.objectDetailState = objectDetailState;
    }

    public Object getApiBaseConfig() {
        return apiBaseConfig;
    }

    public void setApiBaseConfig(Object apiBaseConfig) {
        this.apiBaseConfig = apiBaseConfig;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }
}
