/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.datallama.api.association.AssociationUtil;
import io.datallama.api.association.MdAssociation;
import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.MoClassConstants;
import io.datallama.api.common.RevEngReconciliation;
import io.datallama.api.common.ReverseEngineerRunCmn;
import io.datallama.api.formats.JsonDefinition;
import io.datallama.api.formats.ReverseEngineerFormatUtil;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for API definition reverse engineering runs. The class is responsible for instantiating the appropriate API specification parser class, invoking the Interface functions and saving the generated information to the DL DB.
 */
public class ReverseEngineerApiUtil {

    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;
    private ReverseEngineerFormatUtil jsonDefnRevEngUtil;
    private AssociationUtil associationUtil;

    /**
     * Default constructor
     *
     * @param dataSource
     */
    public ReverseEngineerApiUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
        this.jsonDefnRevEngUtil = new ReverseEngineerFormatUtil(dataSource, userId);
        this.associationUtil = new AssociationUtil(dataSource, userId);
    }

    /**
     * Executes an offline reverse engineering run from a REST API specification identified by apiSpecFileName.
     * Comprises following steps:
     * 1. checks that the specification file is valid
     * 2. generates a new reverse engineering run to track progress of the execution
     * 3. loads the appropriate specification parser on the restApiSpecificationStandard
     * 4. invokes the parser's reverse engineering method
     * 5. saves the reverse engineered REST API specification to the DL DB
     *
     * @return
     */
    public RevEngResultCmn executeOfflineRevEngRest(Integer objectId, String apiSpecFileName) {

        RestApiDefinition restApiDefn = new RestApiDefinition();

        // 2. create a new reverse engineer run record
        ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn(dataSource, objectId, moClassConstants.REST_API);
        Path path = Paths.get(apiSpecFileName);
        revEngRun.setInputSource(path.getFileName().toString());
        RevEngResultCmn revEngResult = revEngRun.generateRevEngRun();

        // 3. check specification file is valid
        if (revEngResult.getResultStatus() == 1) {

            File checkDataFile = new File(apiSpecFileName);
            if (!(checkDataFile.exists() && !checkDataFile.isDirectory())) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(200310);
                revEngResult.setGeneralErrorMessage(String.format("API specification file %s does not exist or is a diretory.", checkDataFile));
                System.out.println(String.format("Error: API specification file %s does not exist or is a diretory.", checkDataFile));
            }
        }

        // 3. instantiate the appropriate API specification parser class for the REST API Specification Standard
        ReverseEngineerRestApi revEngInterface = null;

        String restApiSpecificationStandard = "OPENAPI3-0"; // TODO: this must be configuration driven from the connector framework
        if (revEngResult.getResultStatus() == 1) {
            switch (restApiSpecificationStandard.toUpperCase()) {
                case "OPENAPI3-0":
                    revEngInterface = new RevEngRestApiOpenAPI3();
                    break;
                default:
                    revEngResult.setResultStatus(-1);
                    revEngResult.setErrorNumber(200311);
                    revEngResult.setGeneralErrorMessage(String.format("Unexpected value for REST API specification standard name: %s", restApiSpecificationStandard));
                    throw new IllegalStateException(String.format("Unexpected value for REST API specification standard name: %s", restApiSpecificationStandard));
            }
        }

        // 4. execute the reverse engineer
        if (revEngResult.getResultStatus() == 1) {
            try {
                revEngResult = revEngInterface.reverseEngineerApi(apiSpecFileName);
                if (revEngResult.getResultStatus() < 0) {
                    RevEngResultCmn result = revEngRun.closeRevEngRun(revEngResult);
                    if (result.getResultStatus() < 0) {
                        revEngResult.setResultStatus(result.getResultStatus());
                        revEngResult.setErrorNumber(result.getErrorNumber());
                        revEngResult.setGeneralErrorMessage(result.getGeneralErrorMessage());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // 5. save the reverse engineered API definition to the DL DB
        if (revEngResult.getResultStatus() == 1) {
            try {
                restApiDefn = revEngInterface.getReverseEngineerApiDefinition();
            } catch (Exception e) {
                e.printStackTrace();    // TODO: update revEngResult for this exception
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(200312);
                revEngResult.setGeneralErrorMessage("Error retrieving the reverse engineered API definition from the API specification parser class.");
            }

            if (revEngResult.getResultStatus() == 1) {
                restApiDefn.setObjectId(objectId);
                restApiDefn.setClassId(moClassConstants.REST_API);
                RevEngResultCmn persistApiDefnResult = persistRestApiDefinition(restApiDefn, revEngRun);

                if (persistApiDefnResult.getResultStatus() != 1) {
                    revEngResult.setResultStatus(persistApiDefnResult.getResultStatus());
                    revEngResult.setErrorNumber(200313);
                    revEngResult.setGeneralErrorMessage("Error when saving the reverse engineered REST API definition to the DL DB.");
                }
            }
        }

        // 6. if reverse engineer run was successful, check whether reconciliation is required and update the reverse engineer run as completed.
        if (revEngResult.getResultStatus() == 1) {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_api_reveng_run_status");
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);
            Map<String, Object> out = jdbcCall.execute(in);
            Integer runResult = (Integer) out.get("_run_result");
            String runResultDesc = out.get("_run_result_desc").toString();

            revEngResult.setResultStatus(runResult);
            revEngResult.setGeneralErrorMessage(runResultDesc);
        }

        return revEngResult;
    }

    /**
     * Persists a reverse engineered RestApiDefinition object to the DL DB.
     *
     * @param restApiDefn
     * @return
     */
    private RevEngResultCmn persistRestApiDefinition(RestApiDefinition restApiDefn, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn result = new RevEngResultCmn();

        ArrayList<RestApiPathSegment> pathSegmentList = restApiDefn.getPathElementList();
        for (int i = 0; i < pathSegmentList.size(); i++) {
            Integer result2 = persistRestPathSegment(pathSegmentList.get(i), restApiDefn.getObjectId(), restApiDefn.getClassId(),
                    0, revEngRun);
        }

        return result;
    }

    /**
     * Persist a REST Path Segment, and all parameters, HTTP Methods, and method responses associated with the segment
     *
     * @param pathSegment
     * @param parentObjectId
     * @param parentClassId
     * @param parentElementObjectId
     * @param revEngRun
     * @return
     */
    private Integer persistRestPathSegment(RestApiPathSegment pathSegment, Integer parentObjectId, Integer parentClassId,
                                           Integer parentElementObjectId, ReverseEngineerRunCmn revEngRun) {
        Integer result = 0;
        int i, j = 0;
        Integer newPathSegmentId = 0;
        ObjectMapper jsonGenerator = new ObjectMapper();

        // 1. save the path segment
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_api_rest_element_reveng");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", this.userId, Types.INTEGER)
                .addValue("_class_id", moClassConstants.REST_API_PATH_SEGMENT, Types.INTEGER)
                .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                .addValue("_parent_class_id", parentClassId, Types.INTEGER)
                .addValue("_parent_element_object_id", parentElementObjectId, Types.INTEGER)
                .addValue("_element_level", pathSegment.getPathSegmentLevel(), Types.INTEGER)
                .addValue("_element_name", pathSegment.getPathSegmentName(), Types.VARCHAR)
                .addValue("_element_shortdesc", pathSegment.getPathSegmentShortDesc(), Types.VARCHAR)
                .addValue("_element_parameters", null, Types.VARCHAR)
                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);


        Map<String, Object> out = jdbcCall.execute(in);

        newPathSegmentId = (Integer) out.get("_return_id");

        // 2. Save all http methods for this path segment and the parameters and responses associated with each http method
        ArrayList<RestApiMethod> methodList = pathSegment.getMethodList();
        for (i = 0; i < methodList.size(); i++) {
            RestApiMethod currentMethod = methodList.get(i);
            Integer newMethodId = 0;

            // 2.1. Save the http method
            String methodParams = "";
            try {
                methodParams = jsonGenerator.writeValueAsString(currentMethod.getParameters());
            } catch (JsonProcessingException e) {
                e.printStackTrace();  // TODO: update the reveng result for this exception
            }

            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_api_rest_element_reveng");
            in = new MapSqlParameterSource()
                    .addValue("_user_id", this.userId, Types.INTEGER)
                    .addValue("_class_id", moClassConstants.REST_API_METHOD, Types.INTEGER)
                    .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                    .addValue("_parent_class_id", parentClassId, Types.INTEGER)
                    .addValue("_parent_element_object_id", newPathSegmentId, Types.INTEGER)
                    .addValue("_element_level", currentMethod.getMethodLevel(), Types.INTEGER)
                    .addValue("_element_name", currentMethod.getMethodName(), Types.VARCHAR)
                    .addValue("_element_shortdesc", currentMethod.getMethodDescription(), Types.VARCHAR)
                    .addValue("_element_parameters", methodParams, Types.VARCHAR)
                    .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);
            out = jdbcCall.execute(in);
            newMethodId = (Integer) out.get("_return_id");
            Integer jsonDefnObjectId = 0;

            // 2.2. Save the request schema for the method, if one exists
            if(currentMethod.getRequestJsonPayload() != null) {
                jsonDefnObjectId = persistJsonDefinition(currentMethod.getRequestJsonPayload(), parentObjectId, revEngRun);

                if (jsonDefnObjectId > 0) {
                    MdAssociation assoc = new MdAssociation(-1, newMethodId, moClassConstants.REST_API_METHOD, jsonDefnObjectId, moClassConstants.JSON_MESSAGE_FORMAT,
                            "JSON definition for the REST Request schema", 1000);

                    associationUtil.writeAssociation(assoc);
                }
            }

            // 2.1. save all response codes for the method
            ArrayList<RestApiResponse> responseList = currentMethod.getResponses();
            for (j = 0; j < responseList.size(); j++) {
                RestApiResponse currentResponse = responseList.get(j);
                Integer newResponseId = 0;

                jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_api_rest_element_reveng");
                in = new MapSqlParameterSource()
                        .addValue("_user_id", this.userId, Types.INTEGER)
                        .addValue("_class_id", moClassConstants.REST_API_RESPONSE, Types.INTEGER)
                        .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                        .addValue("_parent_class_id", parentClassId, Types.INTEGER)
                        .addValue("_parent_element_object_id", newMethodId, Types.INTEGER)
                        .addValue("_element_level", currentResponse.getResponseLevel(), Types.INTEGER)
                        .addValue("_element_name", currentResponse.getResponseCode().toString(), Types.VARCHAR)
                        .addValue("_element_shortdesc", currentResponse.getResponseDescription(), Types.VARCHAR)
                        .addValue("_element_parameters", null, Types.VARCHAR)
                        .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);
                out = jdbcCall.execute(in);
                newResponseId = (Integer) out.get("_return_id");

                // 2.1.1. save the JSON payload definition for this response
                jsonDefnObjectId = persistJsonDefinition(currentResponse.getResponseJsonPayload(), parentObjectId, revEngRun);

                // 2.1.2. create a DL lineage association between response and JSON payload
                // TODO: set the association type to REST API Response Payload
                if (jsonDefnObjectId > 0) {
                    MdAssociation assoc = new MdAssociation(-1, newResponseId, moClassConstants.REST_API_RESPONSE, jsonDefnObjectId, moClassConstants.JSON_MESSAGE_FORMAT,
                            "JSON definition for the schema returned by REST Response", 1000);

                    associationUtil.writeAssociation(assoc);
                }
            }
        }

        // 3. save child elements
        ArrayList<RestApiPathSegment> pathSegmentList = pathSegment.getChildPathSegments();
        for (i = 0; i < pathSegmentList.size(); i++) {
            Integer result2 = persistRestPathSegment(pathSegmentList.get(i), parentObjectId, parentClassId,
                    newPathSegmentId, revEngRun);
        }

        return result;
    }

    /**
     * Save a JsonDefinition to the DL DB or update an existing JsonDefinition and returns the objectId
     * @param jsonDefn
     * @return
     */
    private Integer persistJsonDefinition(JsonDefinition jsonDefn, Integer parentObjectId, ReverseEngineerRunCmn revEngRun) {
        Integer objectId = 0;
        Integer revEngCreateSource = 2;

        // 1. create a new JSON Definition in the DL DB or get the formatId and classId if it already exists.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_api_schema_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_class_id", moClassConstants.JSON_MESSAGE_FORMAT, Types.INTEGER)
                .addValue("_api_object_id", parentObjectId, Types.INTEGER)
                .addValue("_object_name", jsonDefn.getDefnName(), Types.VARCHAR)
                .addValue("_object_shortdesc", jsonDefn.getDefnDescription(), Types.VARCHAR)
                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            objectId = (Integer) out.get("_return_id");
            jsonDefn.setObjectId(objectId);
            jsonDefn.setClassId(moClassConstants.JSON_MESSAGE_FORMAT);
        } catch (Exception e) {
            objectId = -2;
        }

        // 2. persist the JSON definition in the DL DB.
        if (objectId > 0) {
            //ReverseEngineerRunCmn jsonDefnRevEngRun = new ReverseEngineerRunCmn(dataSource, objectId, moClassConstants.JSON_MESSAGE_FORMAT);
            //jsonDefnRevEngRun.setInputSource(revEngRun.getInputSource());
            //RevEngResultCmn result02 = jsonDefnRevEngRun.generateRevEngRun();
            RevEngResultCmn result03 = new RevEngResultCmn();

            //if (result02.getResultStatus() == 1) {
                result03 = jsonDefnRevEngUtil.persistJsonDefinition(jsonDefn, revEngRun);
            //}
            //else {
                // TODO. Failed to create a new reverse engineer run for the JSON payload definition. This needs to be capture and presented to user in result description for REST API RevEng
            //}

            //result02.setResultStatus(2);
            //result02.setGeneralErrorMessage("Reverse engineer run completed successfully.");
            //jsonDefnRevEngRun.closeRevEngRun(result02);

        }

        return objectId;
    }

    /**
     * Retrieves the reverse engineer reconciliation report for the REST API definition identified by objectId and classId.
     * The report states whether or not the latest reverse engineer run requires reconciliation and lists all deleted and added elements.
     * @param objectId
     * @param classId
     * @return
     */
    public Map<String, Object> getApiRestRevEngReconcileReport(Integer objectId, Integer classId) {
        Integer resultStatus = 1;
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        ArrayList<RevEngReconciliation> deletedElements = new ArrayList<RevEngReconciliation>();
        ArrayList<RevEngReconciliation> addedElements = new ArrayList<RevEngReconciliation>();

        // 1. call DL DB stored procedure
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_rest_reveng_recon");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        Integer latestRevEngRunId = (Integer) out.get("_latest_reveng_run_id");
        Integer latestRunResult = (Integer) out.get("_latest_run_result");
        String latestRunResultDesc = out.get("_latest_run_result_desc").toString();
        ArrayList dbResultList01 = (ArrayList) out.get("#result-set-1");    // the set of deleted elements requiring reconciliation
        ArrayList dbResultList02 = (ArrayList) out.get("#result-set-2");    // the set of added elements

        DlMetaDataContext mdContext = new DlMetaDataContext();

        // 2. process the set of deleted elements
        for (Object listElement : dbResultList01) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation element = new RevEngReconciliation();
            element.setObjectId((Integer) elementInfo.get("object_id"));
            element.setClassId((Integer) elementInfo.get("class_id"));
            element.setObjectName(elementInfo.get("element_name").toString());
            element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

            deletedElements.add(element);
        }

        // 3. process the set of added elements
        for (Object listElement : dbResultList02) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation element = new RevEngReconciliation();
            element.setObjectId((Integer) elementInfo.get("object_id"));
            element.setClassId((Integer) elementInfo.get("class_id"));
            element.setObjectName(elementInfo.get("element_name").toString());
            element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

            addedElements.add(element);
        }

        resultData.put("latest_reveng_run_id", latestRevEngRunId);
        resultData.put("latest_run_result", latestRunResult);
        resultData.put("latest_run_result_desc", latestRunResultDesc);
        resultData.put("deleted", deletedElements);
        resultData.put("added", addedElements);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Executes the reconcile_api_rest_reveng stored procedure in the DL DB for each RevEngReconciliation object in parameter revEngReconciliation
     * @param revEngReconciliationList
     * @return
     */
    public RevEngResultCmn reconcileRevEng(Integer objectId, Integer classId, ArrayList<RevEngReconciliation> revEngReconciliationList) {
        RevEngResultCmn result = new RevEngResultCmn();

        SimpleJdbcCall jdbcCall = null;
        SqlParameterSource in = null;
        Map<String, Object> out = null;

        // 1. get details of the latest reverse engineer run for object
        ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn(dataSource, objectId, classId);
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_run_latest");
        in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        out = jdbcCall.execute(in);

        revEngRun.setRevEngRunId((Integer) out.get("_reveng_run_id"));
        revEngRun.setRevEngRunDateTime(out.get("_run_datetime").toString());
        revEngRun.setRevEngRunResult((Integer) out.get("_run_result"));
        revEngRun.setRevEngRunDesc(out.get("_run_result_desc").toString());
        revEngRun.setInputSource(out.get("_input_source").toString());
        revEngRun.setUserId((Integer) out.get("_user_id"));

        // 2. process each deleted element in the reconciliation list
        for (int i = 0; i < revEngReconciliationList.size(); i++) {
            RevEngReconciliation revEngRecon = revEngReconciliationList.get(i);

            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("reconcile_api_rest_reveng");
            in = new MapSqlParameterSource().addValue("_object_id", revEngRecon.getObjectId(), Types.INTEGER)
                    .addValue("_class_id", revEngRecon.getClassId(), Types.INTEGER)
                    .addValue("_target_object_id", revEngRecon.getReconciledObjectId(), Types.INTEGER)
                    .addValue("_target_class_id", revEngRecon.getReconciledClassId(), Types.INTEGER)
                    .addValue("_reconciliation_action", revEngRecon.getReconciliationStatus(), Types.INTEGER);
            out = jdbcCall.execute(in);
        }

        // 2. check the reverse engineer status of the API definition
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_api_reveng_run_status");
        in = new MapSqlParameterSource()
                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        result.setResultStatus((Integer) out.get("_run_result"));
        result.setGeneralErrorMessage(out.get("_run_result_desc").toString());

        return result;
    }

    /**
     * Returns the history of reverse engineer runs for the metadata object identified by this.objectId and this.classId
     * @return
     */
    public ArrayList<ReverseEngineerRunCmn> getRevEngRunHistory(Integer objectId, Integer classId) {

        ArrayList<ReverseEngineerRunCmn> revEngRunHistory = new ArrayList<ReverseEngineerRunCmn>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_run_list_cmn");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap revEngRunInfo = (LinkedCaseInsensitiveMap) listElement;
            ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn();
            revEngRun.setRevEngRunId((Integer) revEngRunInfo.get("reveng_run_id"));
            revEngRun.setRevEngRunDateTime(revEngRunInfo.get("run_datetime").toString());
            revEngRun.setRevEngRunResult((Integer) revEngRunInfo.get("run_result"));
            revEngRun.setRevEngRunDesc(revEngRunInfo.get("run_result_desc").toString());
            revEngRun.setInputSource(revEngRunInfo.get("input_source").toString());
            revEngRun.setUserLogin(revEngRunInfo.get("user_login").toString());

            revEngRunHistory.add(revEngRun);
        }

        return revEngRunHistory;
    }


}
