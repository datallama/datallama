/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.formats.JsonDefinition;
import io.datallama.api.formats.JsonElement;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.RequestBody;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.parser.OpenAPIV3Parser;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Reverse engineers a REST API definition from an OpenAPI3 specification.
 */
public class RevEngRestApiOpenAPI3 implements ReverseEngineerRestApi {

    private RestApiDefinition restApiDefn;
    private static OpenAPI openAPI;
    private Map<String, Schema> schemaList;

    /**
     * Default constructor with no parameters
     */
    public RevEngRestApiOpenAPI3 () {
        this.restApiDefn = new RestApiDefinition();
        openAPI = null;
        schemaList = null;
    }

    /**
     * Parses an OpenAPI3 specification file and generates a Data Llama REST API definition from it.
     * @param apiSpecificationFile
     * @return
     * @throws Exception
     */
    public RevEngResultCmn reverseEngineerApi (String apiSpecificationFile) throws Exception {
        RevEngResultCmn revEngResult = new RevEngResultCmn();
        Map<String, RestApiPathSegment> pathElemList = new HashMap<>();     // tracks the path elements that have been created.
        Map<String, JsonDefinition> requestSchemaRef = new HashMap<>();     // tracks the mapping between requests and generated JSON schema.
        Map<String, JsonDefinition> responseSchemaRef = new HashMap<>();    // tracks the mapping between responses and JSON schema.
        ArrayList<RestApiResponse> responseList = new ArrayList<RestApiResponse>();     // tracks responses & schema to facilitate association with JSON definitions
        ArrayList<RestApiMethod> methodRequestBodyList = new ArrayList<RestApiMethod>();    // tracks API Methods & request schema to facilitate association with JSON definitions
        String apiSpecFilename = "";

        Path apiSpecFullPath = Paths.get(apiSpecificationFile);
        apiSpecFilename = apiSpecFullPath.getFileName().toString();

        // 1. parse the OpenAPI3 specification file
        try {
            openAPI = new OpenAPIV3Parser().read(apiSpecificationFile);
        } catch (Exception e) {
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(200351);
            revEngResult.setGeneralErrorMessage(String.format("Error parsing OpenAPI3 specification file %s. Check the file is valid OpenAPI3 file.", apiSpecFilename));
        }

        // 2. get general API info
        restApiDefn.setApiDescription(openAPI.getInfo().getDescription());
        restApiDefn.setApiVersion(openAPI.getInfo().getVersion());

        // 3. process all paths
        if (revEngResult.getResultStatus() == 1) {

            openAPI.getPaths().forEach((apiPath, apiPathItem) -> {

                String apiPathComplete = "";  // ensure the apiPath ends with '/'
                if (apiPath.endsWith("/"))
                    apiPathComplete = apiPath;
                else
                    apiPathComplete = apiPath.concat("/");

                String apiPathSummary = (apiPathItem.getSummary() == null) ? "" : apiPathItem.getSummary();
                String apiPathDesc = (apiPathItem.getDescription() == null) ? "" : apiPathItem.getDescription();

                // 3.1. generate each element in the path
                int pathElemCount = StringUtils.countOccurrencesOf(apiPathComplete, "/");
                int fromIndex = 0;
                int currentPos = 0;
                int pathLevel = 0;

                for (int i = 0; i < pathElemCount; i++) {
                    currentPos = apiPath.indexOf('/', fromIndex);

                    if (pathLevel > 0) {   // ignore the root position "/"
                        // check whether path has already been processed
                        String currentPath = "";
                        String currentElem = "";
                        if (currentPos == -1) {
                            currentPath = apiPathComplete.substring(0, apiPathComplete.length() - 1);
                            currentElem = apiPathComplete.substring(fromIndex, apiPathComplete.length() - 1);
                        }
                        else {
                            currentPath = apiPathComplete.substring(0, currentPos);
                            currentElem = apiPathComplete.substring(fromIndex, currentPos);
                        }

                        if (!pathElemList.containsKey(currentPath)) {
                            RestApiPathSegment pathSegment = new RestApiPathSegment(currentElem, pathLevel - 1);

                            if (pathLevel == 1) {   // top level path elements are added directly to the RestApiDefinition object
                                restApiDefn.addPathElement(pathSegment);
                            }
                            else {
                                // get the parent ApiPathElement and add new path element to it
                                String parentPath = currentPath.substring(0, currentPath.lastIndexOf('/'));
                                RestApiPathSegment parentElem = pathElemList.get(parentPath);

                                parentElem.addChildPathSegment(pathSegment);
                            }

                            pathElemList.put(currentPath, pathSegment);
                        }
                    }
                    fromIndex = currentPos + 1;
                    pathLevel++;
                }

                // 3.3. process all Methods for the path, and all parameters, responses and schema for the each method
                String operationPath = apiPathComplete.substring(0, apiPathComplete.length() - 1);
                RestApiPathSegment pathSegment = pathElemList.get(operationPath);
                pathSegment.setPathSegmentShortDesc(apiPathSummary);
                pathSegment.setPathSegmentFullDesc(apiPathDesc);

                Integer methodLevel = pathSegment.getPathSegmentLevel() + 1;

                // get all operations for pathItem
                Map<PathItem.HttpMethod, Operation> methodMap = apiPathItem.readOperationsMap();
                methodMap.forEach((httpMethod, operation) -> {
                    RestApiMethod restApiMethod = new RestApiMethod(httpMethod.name(), methodLevel);
                    restApiMethod.setMethodDescription(operation.getDescription());
                    ApiResponses getMethodResponses = operation.getResponses();

                    // 3.3.1. process all parameters for the REST Method
                    if(operation.getParameters() != null) {
                        for (Parameter param : operation.getParameters()) {
                            RestApiMethodParam newParameter = new RestApiMethodParam(param.getName(), param.getDescription(), param.getIn(), param.getSchema().getType());
                            restApiMethod.addParameter(newParameter);
                        }

                    }

                    // 3.3.2. process the request body for the REST method, if one exists
                    if(operation.getRequestBody() != null) {
                        // Get the Request Body for this operation, add the #ref to both the API Method and the list of request bodies
                        RequestBody requestBody = operation.getRequestBody();
                        Content requestBodyContent = requestBody.getContent();
                        MediaType rqbJsonMediaType = requestBodyContent.get("application/json");
                        if (rqbJsonMediaType != null) {
                            Schema rqbJsonSchema = rqbJsonMediaType.getSchema();
                            System.out.println(String.format("Method Type: %s, Method Path: %s, Request Body #ref: %s", httpMethod.name(), apiPath, rqbJsonSchema.get$ref()));
                            restApiMethod.setRequestSchemaRef(rqbJsonSchema.get$ref());
                            if (!requestSchemaRef.containsKey(rqbJsonSchema.get$ref())) {    // add the schema reference to responseSchemaRef only if it doesn't already exist
                                requestSchemaRef.put(rqbJsonSchema.get$ref(), new JsonDefinition());
                            }
                            methodRequestBodyList.add(restApiMethod);   // TODO: this is probably redundant
                        }
                    }

                    // 3.3.2. process all responses for the REST Method
                    getMethodResponses.forEach((responseName, response) -> {
                        try {
                            RestApiResponse currentResponse = new RestApiResponse(Integer.parseInt(responseName), response.getDescription(), methodLevel + 1);
                            restApiMethod.addResponse(currentResponse);

                            // get the schema reference for this response. NOTE: schema can be shared between responses
                            response.getContent().forEach((contentType, contentSchema) -> {
                                if (contentType.equals("application/json")) {
                                    Schema responseSchema = contentSchema.getSchema(); // ref to top level schema for response. It could have properties
                                    String topLevelSchemaRef = responseSchema.get$ref();
                                    currentResponse.setResponseSchemaRef(topLevelSchemaRef);

                                    if (!responseSchemaRef.containsKey(topLevelSchemaRef)) {    // add the schema reference to responseSchemaRef only if it doesn't already exist
                                        responseSchemaRef.put(topLevelSchemaRef, new JsonDefinition());
                                    }

                                    // add response to the tracking list
                                    responseList.add(currentResponse);
                                }
                            });


                        } catch (NumberFormatException e) {
                            // ignore responses that cannot be converted to an integer
                        }
                    });
                    pathSegment.addRestMethod(restApiMethod);
                });

            });
        }

        schemaList = openAPI.getComponents().getSchemas();

        // 4. process all request JSON schema
        // 4.1. create all JSON schema for schema references listed as request schema
        for (Map.Entry<String, JsonDefinition> mapEntry : requestSchemaRef.entrySet()) {
            String schemaRef = mapEntry.getKey();
            JsonDefinition currentRequestJsonDefn = mapEntry.getValue();   // the JsonDefinition object for this schema is created ONCE

            currentRequestJsonDefn.setDefnName(schemaRef.substring(schemaRef.lastIndexOf('/') + 1));
            Schema topLevelSchema = schemaList.get(currentRequestJsonDefn.getDefnName());
            String defnDescStart = "";
            if (topLevelSchema.getDescription() != null)
                defnDescStart = topLevelSchema.getDescription() + " - ";
            currentRequestJsonDefn.setDefnDescription(String.format("%sReverse engineered from OpenAPI3 specification file %s", defnDescStart, apiSpecFilename));

            // start traversing the schema hierarchy to add
            topLevelSchema.getProperties().forEach((propertyName, propertySchemaDefn) -> {
                JsonElement newElement = processProp((String) propertyName, (Schema) propertySchemaDefn, 0, ".");
                currentRequestJsonDefn.addElement(newElement);
            });

            System.out.println(currentRequestJsonDefn.getDefnName());
            for (int i = 0; i < methodRequestBodyList.size(); i++) {
                RestApiMethod currentMethod = methodRequestBodyList.get(i);
                if (requestSchemaRef.containsKey(currentMethod.getRequestSchemaRef())) {
                    currentMethod.setRequestJsonPayload(requestSchemaRef.get(currentMethod.getRequestSchemaRef()));
                }
            }

        }

        // 4.2. associate the request JSON schema to API Method.


        // 5. process all response JSON schema
        // 5.1. create all JSON schema for schema references listed as response schema
        for (Map.Entry<String, JsonDefinition> mapEntry : responseSchemaRef.entrySet()) {
            String schemaRef = mapEntry.getKey();
            JsonDefinition currentJsonDefn = mapEntry.getValue();   // the JsonDefinition object for this schema is created ONCE when responses are parsed

            currentJsonDefn.setDefnName(schemaRef.substring(schemaRef.lastIndexOf('/') + 1));
            Schema topLevelSchema = schemaList.get(currentJsonDefn.getDefnName());
            String defnDescStart = "";
            if (topLevelSchema.getDescription() != null)
                defnDescStart = topLevelSchema.getDescription() + " - ";
            currentJsonDefn.setDefnDescription(String.format("%sReverse engineered from OpenAPI3 specification file %s", defnDescStart, apiSpecFilename));

            // start traversing the schema hierarchy to add
            topLevelSchema.getProperties().forEach((propertyName, propertySchemaDefn) -> {
                JsonElement newElement = processProp((String) propertyName, (Schema) propertySchemaDefn, 0, ".");
                currentJsonDefn.addElement(newElement);
            });
        }

        // 5.2. associate the response JSON schema to API Method responses that contain the schema #ref.
        for (int i = 0; i < responseList.size(); i++) {
            RestApiResponse currentResponse = responseList.get(i);
            if (responseSchemaRef.containsKey(currentResponse.getResponseSchemaRef())) {
                currentResponse.setResponseJsonPayload(responseSchemaRef.get(currentResponse.getResponseSchemaRef()));
            }
        }

        return revEngResult;
    }

    /**
     * This function is invoked by {@link ReverseEngineerApiUtil} subsequent to invocation of the reverseEngineerApi method which populates the member restApiDefn.
     * @return A { @link RestApiDefinition} object.
     * @throws Exception
     */
    public RestApiDefinition getReverseEngineerApiDefinition () throws Exception {
        return this.restApiDefn;
    }

    /**
     *
     * @param propName
     * @param propSchemaDefn
     * @param level
     * @param parentPath
     * @return
     */
    private JsonElement processProp(String propName, Schema propSchemaDefn, Integer level, String parentPath) {
        JsonElement newElement = new JsonElement(propName, parentPath);
        String propType = propSchemaDefn.getType() == null ? "notset" : propSchemaDefn.getType();
        newElement.setElementType(propType);
        String propDesc = propSchemaDefn.getDescription() == null ? "notset" : propSchemaDefn.getDescription();
        newElement.setElementDescription(propDesc);

        Integer nextLevel = level++;

        if (propSchemaDefn.getProperties() != null) {
            if (propType.equals("notset")) newElement.setElementType("object");

            // process the schema's properties if any are defined
            propSchemaDefn.getProperties().forEach((nextPropSchemaName, nextPropSchemaDefn) -> {
                JsonElement nextLevelElement = processProp((String) nextPropSchemaName, (Schema) nextPropSchemaDefn, nextLevel, newElement.getElementPath());
                newElement.addElement(nextLevelElement);
            });
        } else {
            // if there is a $ref defined then use that to retrieve next schema
            String nextLevelSchemaRef = propSchemaDefn.get$ref();
            if (nextLevelSchemaRef != null) {
                Schema nextLevelSchema = schemaList.get(nextLevelSchemaRef.substring(nextLevelSchemaRef.lastIndexOf('/') + 1));

                if (nextLevelSchema.getType() != null && propType.equals("notset")) {
                    newElement.setElementType(nextLevelSchema.getType());
                } else if (nextLevelSchema.getProperties() != null && propType.equals("notset")) {
                    newElement.setElementType("object");
                }

                if (nextLevelSchema.getDescription() != null && propDesc.equals("notset")) {
                    newElement.setElementDescription(nextLevelSchema.getDescription());
                }
                ArrayList<JsonElement> newElementList = processSchema(nextLevelSchema, nextLevel, newElement.getElementPath());
                for (int i = 0; i < newElementList.size(); i++) {
                    newElement.addElement(newElementList.get(i));
                }
            }
        }
        return newElement;
    }

    /**
     *
     * @param schemaDefn
     * @param level
     * @param parentPath
     * @return
     */
    private ArrayList<JsonElement> processSchema(Schema schemaDefn, Integer level, String parentPath) {
        ArrayList<JsonElement> newElementList = new ArrayList<JsonElement>();
        // process the schema's properties if any are defined
        if (schemaDefn.getProperties() != null) {
            schemaDefn.getProperties().forEach((nextPropName, nextPropSchemaDefn) -> {
                JsonElement newElement = processProp((String) nextPropName, (Schema) nextPropSchemaDefn, level, parentPath);
                newElementList.add(newElement);
            });
        }

        return newElementList;
    }

}
