/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.apidefinition;

import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.metamodel.DlMetaModelClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DlApiDefinitionController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Returns the list of API Definitions from the DL database
     * URL Path: /apidefinition/list
     *
     * @return Map<String, Object> results
     */
    @RequestMapping(method = GET, value = {"/apidefinition/list"})
    public Map<String, Object> listApiDefinitions() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        ArrayList<DlApiDefinition> apiDefinitionList = new ArrayList<DlApiDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listObject : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) listObject;
            DlApiDefinition apiDefinition = new DlApiDefinition(
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("object_name").toString(),
                    objectInfo.get("object_shortdesc").toString()
            );
            apiDefinition.setClassName(objectInfo.get("class_name").toString());
            apiDefinition.setObjectTable(objectInfo.get("object_table").toString());
            apiDefinition.setObjectDetailState(objectInfo.get("object_detail_state").toString());

            // add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            apiDefinition.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));
            apiDefinitionList.add(apiDefinition);
        }

        results.put("result_data", apiDefinitionList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /apidefinition/class/list
     * Lists all classes of message formats that can be defined via Data Llama
     */
    @RequestMapping(method = GET, value = "/apidefinition/class/list")
    @ResponseBody
    public Map<String, Object> listApiDefinitionClasses() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList<DlMetaModelClass> classList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_classes");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap classInfo = (LinkedCaseInsensitiveMap) listElement;
            DlMetaModelClass classDefn = new DlMetaModelClass(
                    (Integer) classInfo.get("class_id"),
                    (Integer) classInfo.get("meta_class_id"),
                    classInfo.get("class_name").toString(),
                    classInfo.get("class_shortdesc").toString()
            );
            classDefn.setObjectDetailState(classInfo.get("object_detail_state").toString());
            classDefn.setObjectTable(classInfo.get("object_table").toString());

            classList.add(classDefn);
        }

        results.put("result_data", classList);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * @param apiDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/apidefinition/summary")
    public Map<String, Object> writeApiDefinition(@RequestBody DlApiDefinition apiDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_api_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", apiDefinition.getObjectId(), Types.INTEGER)
                .addValue("_class_id", apiDefinition.getClassId(), Types.INTEGER)
                .addValue("_object_name", apiDefinition.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc", apiDefinition.getObjectShortDesc(), Types.VARCHAR)
                .addValue("_api_base_config", apiDefinition.getApiBaseConfig(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnId", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /apidefinition/rest/content/{objectId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/apidefinition/rest/content/{objectId}")
    @ResponseBody
    public Map<String, Object> readApiRestContent(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates the status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList<DlApiDefinitionElement> restElementList = new ArrayList<>(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        // 1. generate the the API definition summary
        DlApiDefinition apiDefn = new DlApiDefinition(
                objectId,                               // objectId
                (Integer) out.get("_class_id"),         // classId
                out.get("_object_name").toString(),     // objectName
                out.get("_object_shortdesc").toString() // objectShortDesc
        );
        apiDefn.setClassName(out.get("_class_name").toString());
        apiDefn.setObjectTable(out.get("_object_table").toString());
        apiDefn.setObjectDetailState(out.get("_object_detail_state").toString());

        // 1.1. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        apiDefn.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        // 1.2. add the custom form definition
        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        apiDefn.setCustomForm(customForm);


        resultData.put("api_summary", apiDefn);

        // 2. generate an XML Element object for every row in result set and add to Array List xmlElementList
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_rest_elements");
        out = jdbcCall.execute(in);
        ArrayList dbElementsResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;
        for (Object listElement : dbElementsResultList) {
            LinkedCaseInsensitiveMap restElementInfo = (LinkedCaseInsensitiveMap) listElement;

            String elementDescRevEng = restElementInfo.get("element_desc_reveng") == null ? "" : restElementInfo.get("element_desc_reveng").toString();

            //make the xml element
            DlApiDefinitionElement restElement = new DlApiDefinitionElement(
                    (Integer) restElementInfo.get("object_id"),
                    (Integer) restElementInfo.get("class_id"),
                    (Integer) restElementInfo.get("parent_object_id"),
                    (Integer) restElementInfo.get("parent_class_id"),
                    (Integer) restElementInfo.get("parent_element_object_id"),
                    (Integer) restElementInfo.get("element_level"),
                    (Integer) restElementInfo.get("element_level_sequence"),
                    restElementInfo.get("element_hierarchy_number").toString(),
                    (Boolean) restElementInfo.get("is_parent"),
                    restElementInfo.get("element_name").toString(),
                    restElementInfo.get("element_shortdesc").toString(),
                    elementDescRevEng,
                    restElementInfo.get("element_guid").toString(),
                    restElementInfo.get("http_method").toString(),
                    (Integer) restElementInfo.get("response_code")
            );
            restElement.setResultSetSequence(resultSetSequence);
            resultSetSequence++;

            // add the md_context
            restElement.setMdContext(mdContext.generateMdContextList(restElementInfo.get("md_context").toString()));

            restElementList.add(restElement);
        }

        resultData.put("api_elements", restElementList);

        // 3: get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource()
                .addValue("_class_id", apiDefn.getClassId(), Types.INTEGER)
                .addValue("_object_id", apiDefn.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context for the AssociatedObject instance
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);


        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /apidefinition/rest/content/flat/{objectId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/apidefinition/rest/element/{objectId}")
    @ResponseBody
    public Map<String, Object> readApiRestElement(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates the status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // this list is returned in the results Map

        // 1: get the core attributes of the REST element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_rest_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) dbResultList.get(0);
        DlApiDefinitionElement element = new DlApiDefinitionElement(
                (Integer) elementInfo.get("object_id"),
                (Integer) elementInfo.get("class_id"),
                (Integer) elementInfo.get("parent_object_id"),
                (Integer) elementInfo.get("parent_class_id"),
                (Integer) elementInfo.get("parent_element_object_id"),
                (Integer) elementInfo.get("element_level"),
                (Integer) elementInfo.get("element_level_sequence"),
                elementInfo.get("element_hierarchy_number").toString(),
                (Boolean) elementInfo.get("is_parent"),
                elementInfo.get("element_name").toString(),
                elementInfo.get("element_shortdesc").toString(),
                elementInfo.get("element_desc_reveng").toString(),
                elementInfo.get("element_guid").toString(),
                elementInfo.get("http_method").toString(),
                (Integer) elementInfo.get("response_code")
        );
        element.setElementParameters(elementInfo.get("element_parameters").toString());


        // 1.1: add md_context for REST element
        DlMetaDataContext mdContext = new DlMetaDataContext();
        element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

        resultData.put("attributes", element);

        // 2: get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", element.getClassId(), Types.INTEGER)
                .addValue("_object_id", element.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context for the AssociatedObject instance
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 3. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", element.getClassId(), Types.INTEGER)
                .addValue("_object_id", element.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        }
        catch(NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);


        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes a new REST element to the DL database
     * URL Path: /apidefinition/rest/element
     * @param newElement
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/apidefinition/rest/element")
    @ResponseBody
    public Map<String, Object> writeApiRestElement(@RequestBody DlApiDefinitionElement newElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_api_rest_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newElement.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newElement.getParentObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", newElement.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", newElement.getParentElementObjectId(), Types.INTEGER)
                .addValue("_element_name", newElement.getElementName(), Types.VARCHAR)
                .addValue("_element_shortdesc", newElement.getElementShortDesc(), Types.VARCHAR)
                .addValue("_http_method", newElement.getHttpMethod(), Types.VARCHAR)
                .addValue("_response_code", newElement.getResponseCode(), Types.INTEGER)
                .addValue("_custom_form_data", null, Types.VARCHAR)
                .addValue("_import_run_id", 0, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) rslt.get(0);

        // instantiate a DlApiDefinitionElement object to return to invoker
        DlApiDefinitionElement element = new DlApiDefinitionElement(
                (Integer) elementInfo.get("object_id"),
                (Integer) elementInfo.get("class_id"),
                (Integer) elementInfo.get("parent_object_id"),
                (Integer) elementInfo.get("parent_class_id"),
                (Integer) elementInfo.get("parent_element_object_id"),
                (Integer) elementInfo.get("element_level"),
                (Integer) elementInfo.get("element_level_sequence"),
                elementInfo.get("element_hierarchy_number").toString(),
                (Boolean) elementInfo.get("is_parent"),
                elementInfo.get("element_name").toString(),
                elementInfo.get("element_shortdesc").toString(),
                elementInfo.get("element_desc_reveng").toString(),
                elementInfo.get("element_guid").toString(),
                elementInfo.get("http_method").toString(),
                (Integer) elementInfo.get("response_code")
        );

        // add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

        results.put("result_data", element);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/apidefinition/{objectId}")
    public Map<String, Object> deleteApi(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_api");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/apidefinition/rest/element/{objectId}")
    public Map<String, Object> deleteApiRestDetail(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_api_rest_detail");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }
}