/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;


import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.RevEngReconciliation;
import io.datallama.api.common.ReverseEngineerRunCmn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class ReverseEngineerApiController {
    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.file.upload}")
    private String uploadFilePath;

    /**
     * This endpoint is invoked to reverse engineer a REST API from a specification standard (e.g. OpenAPI 3.0).
     * Saves the upload file (multipart form data) and then instantiates a ReverseEngineerApiUtils object and invokes the executeOfflineRevEngRest
     *
     * @param userId
     * @param uploadFile
     * @return
     */
    @RequestMapping(method = POST, value = "/apidefinition/rest/reverse-engineer/{objectId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> ApiRestRevEngOffline(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId,
                                                    @RequestParam("file") MultipartFile uploadFile) {
        Map<String, Object> results = new HashMap<>();
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        String uploadFileName = "";
        if (uploadFilePath.endsWith("/"))
            uploadFileName = String.format("%s%s", uploadFilePath, uploadFile.getOriginalFilename());
        else
            uploadFileName = String.format("%s/%s", uploadFilePath, uploadFile.getOriginalFilename());

        try {
            File convertFile = new File(uploadFileName);

            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(uploadFile.getBytes());
            fout.close();
        } catch (IOException e) {
            Integer resultStatus = -2;

            revEngResult.setResultStatus(resultStatus);
            revEngResult.setErrorNumber(-200304);
            revEngResult.setGeneralErrorMessage(String.format("Failed to save upload file to %s.", uploadFileName));
            results.put("result_data", revEngResult);
            results.put("result_status", resultStatus);
        }

        if (revEngResult.getResultStatus() == 1) {
            ReverseEngineerApiUtil revEngUtils = new ReverseEngineerApiUtil(dataSource, Integer.parseInt(userId));
            revEngResult = revEngUtils.executeOfflineRevEngRest(objectId, uploadFileName);
            results.put("result_data", revEngResult);
            results.put("result_status", revEngResult.getResultStatus());
        }

        return results;
    }

    /**
     * Returns the history of reverse engineer runs for the REST API identified by {objectId} and {classId}
     *
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/apidefinition/rest/reverse-engineer/history/{objectId}/{classId}")
    public Map<String, Object> getApiRestRevEngHistory(@PathVariable Integer objectId, @PathVariable Integer classId, @RequestHeader("dlUserId") String userId) {
        Integer resultStatus = 1;
        Map<String, Object> results = new HashMap<>();

        ReverseEngineerApiUtil revEngUtil = new ReverseEngineerApiUtil(dataSource, Integer.parseInt(userId));
        ArrayList<ReverseEngineerRunCmn> revEngRunHistory = revEngUtil.getRevEngRunHistory(objectId, classId);

        results.put("result_status", resultStatus);
        results.put("result_data", revEngRunHistory);
        return results;
    }

    /**
     * Retrieves the set of deleted elements requiring reconciliation and the set of added elements
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/apidefinition/rest/reverse-engineer/reconcile/{objectId}/{classId}")
    public Map<String, Object> getApiRestRevEngReconcileReport(@PathVariable Integer objectId, @PathVariable Integer classId, @RequestHeader("dlUserId") String userId) {
        ReverseEngineerApiUtil revEngUtil = new ReverseEngineerApiUtil(dataSource, Integer.parseInt(userId));

        Map<String, Object> results = revEngUtil.getApiRestRevEngReconcileReport(objectId, classId);

        return results;
    }

    /**
     * Returns a reconciliation report for tables/views added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param objectId
     * @return
     */
    @RequestMapping(method = POST, value = "/apidefinition/rest/reverse-engineer/reconcile/{objectId}/{classId}")
    @ResponseBody
    public Map<String, Object> reconcileRevEngDeletedColumn(@PathVariable Integer objectId, @PathVariable Integer classId,
                                                            @RequestBody ArrayList<RevEngReconciliation> revEngReconciliationList,
                                                            @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();

        ReverseEngineerApiUtil revEngUtils = new ReverseEngineerApiUtil(dataSource, Integer.parseInt(userId));

        // 1. execute the reconciliation action
        RevEngResultCmn result = revEngUtils.reconcileRevEng(objectId, classId, revEngReconciliationList); // TODO: check resultStatus for errors

        results.put("result_status", result.getResultStatus());
        results.put("result_data", result);

        return results;
    }

}