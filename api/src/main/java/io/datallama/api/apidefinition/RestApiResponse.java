/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

import io.datallama.api.formats.JsonDefinition;

public class RestApiResponse {
    private Integer responseCode;
    private String responseDescription;
    private Integer responseLevel;
    private JsonDefinition responseJsonPayload;     // the JSON payload that is sent with the response
    private String responseSchemaRef;

    /**
     * Default constructor
     */
    public RestApiResponse() {
        this.responseCode = 0;
        this.responseDescription = "";
        this.responseLevel = 0;
        this.responseJsonPayload = new JsonDefinition();
        this.responseSchemaRef = "";
    }

    /**
     * Constructor with two parameters, response code and description
     * @param responseCode
     * @param responseDescription
     */
    public RestApiResponse(Integer responseCode, String responseDescription) {
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
        this.responseLevel = 0;
        this.responseJsonPayload = new JsonDefinition();
        this.responseSchemaRef = "";
    }

    /**
     * Constructor with parameters for all members.
     * @param responseCode
     * @param responseDescription
     * @param responseLevel
     */
    public RestApiResponse(Integer responseCode, String responseDescription, Integer responseLevel) {
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
        this.responseLevel = responseLevel;
        this.responseJsonPayload = new JsonDefinition();
        this.responseSchemaRef = "";
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public Integer getResponseLevel() {
        return responseLevel;
    }

    public void setResponseLevel(Integer responseLevel) {
        this.responseLevel = responseLevel;
    }

    public JsonDefinition getResponseJsonPayload() {
        return responseJsonPayload;
    }

    public void setResponseJsonPayload(JsonDefinition responseJsonPayload) {
        this.responseJsonPayload = responseJsonPayload;
    }

    public String getResponseSchemaRef() {
        return responseSchemaRef;
    }

    public void setResponseSchemaRef(String responseSchemaRef) {
        this.responseSchemaRef = responseSchemaRef;
    }
}
