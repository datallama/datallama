/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

import io.datallama.api.formats.JsonDefinition;

import java.util.ArrayList;

public class RestApiMethod {
    private String methodName;
    private String methodDescription;
    private Integer methodLevel;
    private ArrayList<RestApiMethodParam> parameters;
    private ArrayList<RestApiResponse> responses;
    private String requestSchemaRef;
    private JsonDefinition requestJsonPayload;     // the JSON payload that is sent with the request

    /**
     * Default Constructor with no parameters
     */
    public RestApiMethod () {
        this.methodName = "";
        this.methodDescription = "";
        this.methodLevel = 0;
        this.parameters = new ArrayList<RestApiMethodParam>();
        this.responses = new ArrayList<RestApiResponse>();
        this.requestSchemaRef = "";
        this.requestJsonPayload = new JsonDefinition();
    }

    /**
     * Constructor with one parameter, method Name.
     */
    public RestApiMethod (String methodName) {
        this.methodName = methodName;
        this.methodDescription = "";
        this.methodLevel = 0;
        this.parameters = new ArrayList<RestApiMethodParam>();
        this.responses = new ArrayList<RestApiResponse>();
        this.requestSchemaRef = "";
        this.requestJsonPayload = new JsonDefinition();
    }

    /**
     * Constructor with two parameters, method name and method level.
     */
    public RestApiMethod (String methodName, Integer methodLevel) {
        this.methodName = methodName;
        this.methodDescription = "";
        this.methodLevel = methodLevel;
        this.parameters = new ArrayList<RestApiMethodParam>();
        this.responses = new ArrayList<RestApiResponse>();
        this.requestSchemaRef = "";
        this.requestJsonPayload = new JsonDefinition();
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodDescription() {
        return methodDescription;
    }

    public void setMethodDescription(String methodDescription) {
        this.methodDescription = methodDescription;
    }

    public Integer getMethodLevel() {
        return methodLevel;
    }

    public void setMethodLevel(Integer methodLevel) {
        this.methodLevel = methodLevel;
    }

    public ArrayList<RestApiMethodParam> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<RestApiMethodParam> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(RestApiMethodParam parameter) {
        this.parameters.add(parameter);
    }

    public ArrayList<RestApiResponse> getResponses() {
        return responses;
    }

    public void setResponses(ArrayList<RestApiResponse> responses) {
        this.responses = responses;
    }

    public void addResponse(RestApiResponse response) {
        this.responses.add(response);
    }

    public String getRequestSchemaRef() {
        return requestSchemaRef;
    }

    public void setRequestSchemaRef(String requestSchemaRef) {
        this.requestSchemaRef = requestSchemaRef;
    }

    public JsonDefinition getRequestJsonPayload() {
        return requestJsonPayload;
    }

    public void setRequestJsonPayload(JsonDefinition requestJsonPayload) {
        this.requestJsonPayload = requestJsonPayload;
    }
}
