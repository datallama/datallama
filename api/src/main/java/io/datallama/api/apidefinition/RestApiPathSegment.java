/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.apidefinition;

import java.util.ArrayList;

public class RestApiPathSegment {
    private String pathSegmentName;
    private Integer pathSegmentLevel;
    private String pathSegmentShortDesc;
    private String pathSegmentFullDesc;
    private ArrayList<RestApiPathSegment> childPathSegments;
    private ArrayList<RestApiMethod> methodList;

    /**
     * Default constructor. No parameters.
     */
    public RestApiPathSegment() {
        this.pathSegmentName = "";
        this.pathSegmentLevel = 0;
        this.pathSegmentShortDesc = "";
        this.pathSegmentFullDesc = "";
        this.childPathSegments = new ArrayList<RestApiPathSegment>();
        this.methodList = new ArrayList<RestApiMethod>();
    }

    /**
     * Constructor with only pathElementName as a parameter.
     * @param pathSegmentName
     */
    public RestApiPathSegment(String pathSegmentName) {
        this.pathSegmentName = pathSegmentName;
        this.pathSegmentLevel = 0;
        this.pathSegmentShortDesc = "";
        this.pathSegmentFullDesc = "";
        this.childPathSegments = new ArrayList<RestApiPathSegment>();
        this.methodList = new ArrayList<RestApiMethod>();
    }

    /**
     * Constructor with pathElementName an pathElementLevel as parameters.
     * @param pathSegmentName
     */
    public RestApiPathSegment(String pathSegmentName, Integer pathSegmentLevel) {
        this.pathSegmentName = pathSegmentName;
        this.pathSegmentLevel = pathSegmentLevel;
        this.pathSegmentShortDesc = "";
        this.pathSegmentFullDesc = "";
        this.childPathSegments = new ArrayList<RestApiPathSegment>();
        this.methodList = new ArrayList<RestApiMethod>();
    }

    public String getPathSegmentName() {
        return pathSegmentName;
    }

    public void setPathSegmentName(String pathSegmentName) {
        this.pathSegmentName = pathSegmentName;
    }

    public Integer getPathSegmentLevel() {
        return pathSegmentLevel;
    }

    public void setPathSegmentLevel(Integer pathSegmentLevel) {
        this.pathSegmentLevel = pathSegmentLevel;
    }

    public String getPathSegmentShortDesc() {
        return pathSegmentShortDesc;
    }

    public void setPathSegmentShortDesc(String pathSegmentShortDesc) {
        this.pathSegmentShortDesc = pathSegmentShortDesc;
    }

    public String getPathSegmentFullDesc() {
        return pathSegmentFullDesc;
    }

    public void setPathSegmentFullDesc(String pathSegmentFullDesc) {
        this.pathSegmentFullDesc = pathSegmentFullDesc;
    }

    public ArrayList<RestApiPathSegment> getChildPathSegments() {
        return childPathSegments;
    }

    public void setChildPathSegments(ArrayList<RestApiPathSegment> childPathSegments) {
        this.childPathSegments = childPathSegments;
    }

    public void addChildPathSegment(RestApiPathSegment pathElement) {
        this.childPathSegments.add(pathElement);
    }

    public ArrayList<RestApiMethod> getMethodList() {
        return methodList;
    }

    public void setMethodList(ArrayList<RestApiMethod> methodList) {
        this.methodList = methodList;
    }

    public void addRestMethod(RestApiMethod restMethod) {
        this.methodList.add(restMethod);
    }

}
