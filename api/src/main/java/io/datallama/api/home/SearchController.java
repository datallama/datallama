/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.home;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.metamodel.DlMetaDataContextNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class SearchController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /taxonomy/list
     */
    @RequestMapping(method = GET, value = "/home/search/simple/{searchKeyword}")
    @ResponseBody
    public Map<String, Object> simpleSearch(@PathVariable String searchKeyword) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList simpleSearchResultList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("search_name_shortdesc");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_search_keyword", searchKeyword, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        // process db results into SimpleSearchResult objects
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;

            // first, create the SimpleSearchResult instance
            SimpleSearchResult simpleSearchResult = new SimpleSearchResult(
                    (Integer) elementInfo.get("object_id"),
                    (Integer) elementInfo.get("class_id"),
                    elementInfo.get("object_name").toString(),
                    elementInfo.get("class_name").toString(),
                    (Integer) elementInfo.get("parent_object_id"),
                    (Integer) elementInfo.get("parent_class_id"),
                    elementInfo.get("object_shortdesc").toString()
            );

            // second, add the metadata context
            // TODO: candidate for utility class
            List<LinkedHashMap> contextList = null;
            try {
                contextList = objectMapper.readValue((String) elementInfo.get("md_context"), List.class);
                for(LinkedHashMap contextNode:contextList) {
                    DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                            Integer.parseInt(contextNode.get("class_id").toString()),
                            contextNode.get("class_name").toString(),
                            Integer.parseInt(contextNode.get("object_id").toString()),
                            contextNode.get("object_name").toString()
                    );
                    if(contextNode.containsKey("internal_path")) {
                        mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                    }
                    else {
                        mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                    }
                    simpleSearchResult.addMdContextNode(mdContextNode);
                }
            }
            catch (Exception e) {
                System.out.println("Exception thrown while parsing md_context JSON array.");
                System.out.println(e);
            }

            simpleSearchResultList.add(simpleSearchResult);
        }

        results.put("result_data", simpleSearchResultList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /taxonomy/list
     */
    @RequestMapping(method = POST, value = "/home/search/filtered")
    @ResponseBody
    public Map<String, Object> simpleFiltered(@RequestBody SearchFilter searchFilter, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList filteredSearchResultList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("search_filtered");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_search_keyword", searchFilter.getKeyword(), Types.VARCHAR)
                .addValue("_fltr_name", searchFilter.getObjectName(), Types.BOOLEAN)
                .addValue("_fltr_name_exact", searchFilter.getObjectNameExact(), Types.BOOLEAN)
                .addValue("_fltr_desc", searchFilter.getObjectShortDesc(), Types.BOOLEAN)
                .addValue("_fltr_fmt", searchFilter.getMessageFormat(), Types.BOOLEAN)
                .addValue("_fltr_land", searchFilter.getLandscapeObjects(), Types.BOOLEAN)
                .addValue("_fltr_dflow", searchFilter.getDataFlows(), Types.BOOLEAN)
                .addValue("_fltr_dbase", searchFilter.getDataBases(), Types.BOOLEAN)
                .addValue("_fltr_dgrm", searchFilter.getDiagrams(), Types.BOOLEAN)
                .addValue("_fltr_taxo", searchFilter.getTaxonomy(), Types.BOOLEAN)
                .addValue("_fltr_endnodes", searchFilter.getEndNodesOnly(), Types.BOOLEAN);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        // process db results into SimpleSearchResult objects
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;

            // first, create the SimpleSearchResult instance
            SimpleSearchResult simpleSearchResult = new SimpleSearchResult(
                    (Integer) elementInfo.get("object_id"),
                    (Integer) elementInfo.get("class_id"),
                    elementInfo.get("object_name").toString(),
                    elementInfo.get("class_name").toString(),
                    (Integer) elementInfo.get("parent_object_id"),
                    (Integer) elementInfo.get("parent_class_id"),
                    elementInfo.get("object_shortdesc").toString()
            );

            // second, add the metadata context
            // TODO: candidate for utility class
            List<LinkedHashMap> contextList = null;
            try {
                contextList = objectMapper.readValue((String) elementInfo.get("md_context"), List.class);
                for(LinkedHashMap contextNode:contextList) {
                    DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                            Integer.parseInt(contextNode.get("class_id").toString()),
                            contextNode.get("class_name").toString(),
                            Integer.parseInt(contextNode.get("object_id").toString()),
                            contextNode.get("object_name").toString()
                    );
                    if(contextNode.containsKey("internal_path")) {
                        mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                    }
                    else {
                        mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                    }
                    simpleSearchResult.addMdContextNode(mdContextNode);
                }
            }
            catch (Exception e) {
                System.out.println("Exception thrown while parsing md_context JSON array.");
                System.out.println(e);
            }

            filteredSearchResultList.add(simpleSearchResult);
        }

        results.put("result_data", filteredSearchResultList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: this class is used to process a request to move a taxonomy element
     */
    static class SearchFilter {
        private String keyword;
        private Boolean objectName;
        private Boolean objectNameExact;
        private Boolean objectShortDesc;
        private Boolean messageFormat;
        private Boolean landscapeObjects;
        private Boolean dataFlows;
        private Boolean dataBases;
        private Boolean diagrams;
        private Boolean taxonomy;
        private Boolean endNodesOnly;

        public SearchFilter() {
            keyword = "";
            objectName = false;
            objectNameExact = false;
            objectShortDesc = false;
            messageFormat = false;
            taxonomy = false;
            endNodesOnly = false;
        }

        public Boolean getLandscapeObjects() {
            return landscapeObjects;
        }

        public void setLandscapeObjects(Boolean landscapeObjects) {
            this.landscapeObjects = landscapeObjects;
        }

        public Boolean getDataFlows() {
            return dataFlows;
        }

        public void setDataFlows(Boolean dataFlows) {
            this.dataFlows = dataFlows;
        }

        public Boolean getDataBases() {
            return dataBases;
        }

        public void setDataBases(Boolean dataBases) {
            this.dataBases = dataBases;
        }

        public Boolean getDiagrams() {
            return diagrams;
        }

        public void setDiagrams(Boolean diagrams) {
            this.diagrams = diagrams;
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public Boolean getObjectName() {
            return objectName;
        }

        public void setObjectName(Boolean objectName) {
            this.objectName = objectName;
        }

        public Boolean getObjectNameExact() {
            return objectNameExact;
        }

        public void setObjectNameExact(Boolean objectNameExact) {
            this.objectNameExact = objectNameExact;
        }

        public Boolean getObjectShortDesc() {
            return objectShortDesc;
        }

        public void setObjectShortDesc(Boolean objectShortDesc) {
            this.objectShortDesc = objectShortDesc;
        }

        public Boolean getMessageFormat() {
            return messageFormat;
        }

        public void setMessageFormat(Boolean messageFormat) {
            this.messageFormat = messageFormat;
        }

        public Boolean getTaxonomy() {
            return taxonomy;
        }

        public void setTaxonomy(Boolean taxonomy) {
            this.taxonomy = taxonomy;
        }

        public Boolean getEndNodesOnly() {
            return endNodesOnly;
        }

        public void setEndNodesOnly(Boolean endNodesOnly) {
            this.endNodesOnly = endNodesOnly;
        }
    }

}
