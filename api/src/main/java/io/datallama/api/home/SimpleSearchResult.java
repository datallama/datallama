/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.home;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class SimpleSearchResult {
    private Integer objectId;
    private Integer classId;
    private String objectName;
    private String className;
    private Integer parentObjectId;
    private Integer parentClassId;
    private String objectShortDesc;
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Default no-parameter constructor.
     */
    public SimpleSearchResult() {
        this.objectId = 0;
        this.classId = 0;
        this.objectName = "";
        this.className = "";
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.objectShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    /**
     * Full constructor with all scalar parameters specified
     * @param objectId
     * @param classId
     * @param objectName
     * @param className
     * @param objectShortDesc
     */
    public SimpleSearchResult(Integer objectId, Integer classId, String objectName, String className,
                              Integer parentObjectId, Integer parentClassId, String objectShortDesc) {
        this.objectId = objectId;
        this.classId = classId;
        this.objectName = objectName;
        this.className = className;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.objectShortDesc = objectShortDesc;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }
}
