/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.home;

import io.datallama.api.common.OverviewPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class HomeController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /ontology/overview/{stateName}
     * TODO: should really be in an Ontology controller, but not much else to go in there yet
     */
    @RequestMapping(method = GET, value = "/home/overview/{stateName}")
    @ResponseBody
    public Map<String, Object> readHomeOverview(@PathVariable String stateName) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_overview_page");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_state_name", stateName, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList resultList = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap pageContent = (LinkedCaseInsensitiveMap) resultList.get(0);
        OverviewPage overviewPage = new OverviewPage(
                (Integer) pageContent.get("overview_id"),
                pageContent.get("state_name").toString(),
                pageContent.get("page_content").toString()
        );

        results.put("result_data", overviewPage);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: saves the home overview page content in the DL database
     */
    @RequestMapping(method = POST, value = "/home/overview")
    public Map<String, Object> writeOntologyOverview(@RequestBody OverviewPage overviewPage) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_overview_page");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_state_name", overviewPage.getStateName(), Types.VARCHAR)
                .addValue("_page_content", overviewPage.getPageContent(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        // TODO: stored proc should return a status.

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


}
