/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.datamodels;

public class ConnectorContent {
    private int cnctId;
    private int classId;
    private String cnctName;
    private String cnctDesc;
    private String cnctStartNtt;
    private String cnctEndNtt;
    private String cnctVertices;
    private Boolean inclusionStatus;

    public ConnectorContent(int cnctId, int classId, String cnctName, String cnctDesc, String cnctStartNtt, String cnctEndNtt, String cnctVertices, Boolean inclusionStatus) {
        this.cnctId = cnctId;
        this.classId = classId;
        this.cnctName = cnctName;
        this.cnctDesc = cnctDesc;
        this.cnctStartNtt = cnctStartNtt;
        this.cnctEndNtt = cnctEndNtt;
        this.cnctVertices = cnctVertices;
        this.inclusionStatus = inclusionStatus;
    }

    public int getCnctId() {
        return cnctId;
    }

    public void setCnctId(int cnctId) {
        this.cnctId = cnctId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getCnctName() {
        return cnctName;
    }

    public void setCnctName(String cnctName) {
        this.cnctName = cnctName;
    }

    public String getCnctDesc() {
        return cnctDesc;
    }

    public void setCnctDesc(String cnctDesc) {
        this.cnctDesc = cnctDesc;
    }

    public String getCnctStartNtt() {
        return cnctStartNtt;
    }

    public void setCnctStartNtt(String cnctStartNtt) {
        this.cnctStartNtt = cnctStartNtt;
    }

    public String getCnctEndNtt() {
        return cnctEndNtt;
    }

    public void setCnctEndNtt(String cnctEndNtt) {
        this.cnctEndNtt = cnctEndNtt;
    }

    public String getCnctVertices() {
        return cnctVertices;
    }

    public void setCnctVertices(String cnctVertices) {
        this.cnctVertices = cnctVertices;
    }

    public Boolean getInclusionStatus() {
        return inclusionStatus;
    }

    public void setInclusionStatus(Boolean inclusionStatus) {
        this.inclusionStatus = inclusionStatus;
    }
}
