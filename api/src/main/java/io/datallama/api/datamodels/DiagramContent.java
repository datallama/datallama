/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.datamodels;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

public class DiagramContent {
    private int diagramId;
    private Object properties;
    private ArrayList<Object> entities;
    private ArrayList<Object> connectors;
    private ArrayList<Object> entityDeletes;
    private ArrayList<Object> connectorDeletes;

    public int getDiagramId() {
        return diagramId;
    }

    public void setDiagramId(int diagramId) {
        this.diagramId = diagramId;
    }

    public Object getProperties() {
        return properties;
    }

    public void setProperties(Object properties) {
        this.properties = properties;
    }

    public ArrayList<Object> getEntities() {
        return entities;
    }

    public void setEntities(ArrayList<Object> entities) {
        this.entities = entities;
    }

    public ArrayList<Object> getConnectors() {
        return connectors;
    }

    public void setConnectors(ArrayList<Object> connectors) {
        this.connectors = connectors;
    }

    public ArrayList<Object> getEntityDeletes() {
        return entityDeletes;
    }

    public void setEntityDeletes(ArrayList<Object> entityDeletes) {
        this.entityDeletes = entityDeletes;
    }

    public ArrayList<Object> getConnectorDeletes() {
        return connectorDeletes;
    }

    public void setConnectorDeletes(ArrayList<Object> connectorDeletes) {
        this.connectorDeletes = connectorDeletes;
    }

    public String getDiagramPropertiesAsString() {
        ObjectMapper objectMapper = new ObjectMapper(); // used to diagram properties Object as JSON string
        String diagramProps = new String();
        try {
            diagramProps = objectMapper.writeValueAsString(this.properties);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return diagramProps;
    }
}
