/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.datamodels;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DataModelController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /data-models/pdm/diagram/list
     */
    @RequestMapping(method = GET, value = "/data-models/diagram/list/{diagramClassId}")
    public Map<String, Object> readDiagramList(@PathVariable Long diagramClassId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dm_diagrams");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", diagramClassId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        results.put("result_data", rslt);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/data-models/diagram/shape-templates")
    public Map<String, Object> readShapeTemplates() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_shape_templates");
        Map<String, Object> out = jdbcCall.execute();

        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        results.put("result_data", rslt);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /data-models/pdm/diagram/list
     */
    @RequestMapping(method = POST, value = "/data-models/diagram/create")
    public Map<String, Object> createDiagram(@RequestBody DataModelDiagram dmDiagram, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dm_diagram");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_dgrm_id", dmDiagram.getDiagramId(), Types.INTEGER)
                .addValue("_class_id", dmDiagram.getClassId(), Types.INTEGER)
                .addValue("_dgrm_name", dmDiagram.getDiagramName(), Types.VARCHAR)
                .addValue("_dgrm_desc", dmDiagram.getDiagramShortDesc(), Types.VARCHAR)
                .addValue("_dgrm_prop", dmDiagram.getDiagramPropertiesAsString(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("returnID", out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Updates the summary information for a data model diagram
     * @param diagramSummary
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/data-models/diagram/summary")
    public Map<String, Object> writeDiagramSummary(@RequestBody DataModelDiagram diagramSummary, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dm_diagram");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_dgrm_id", diagramSummary.getDiagramId(), Types.INTEGER)
                .addValue("_class_id", diagramSummary.getClassId(), Types.INTEGER)
                .addValue("_dgrm_name", diagramSummary.getDiagramName(), Types.VARCHAR)
                .addValue("_dgrm_desc", diagramSummary.getDiagramShortDesc(), Types.VARCHAR)
                .addValue("_dgrm_prop", diagramSummary.getDiagramPropertiesAsString(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("returnID", out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes a diagram from the DL database. Entities and connectors in the diagram are deleted via cascading foreign keys.
     * @param diagramId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/data-models/diagram/{diagramId}")
    public Map<String, Object> deleteTaxonomyElement(@PathVariable Integer diagramId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // TODO: lookup user id from DlAuthDB using the API Token in Authorization Header
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_dm_diagram");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_dgrm_id", diagramId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/data-models/arrow")
    @ResponseBody
    public Map<String, Object> writeArrow(@RequestBody ArrowDetails arrowDetails, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_arrow");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_class_id", 44, Types.INTEGER)
                .addValue("_object_id", arrowDetails.getObjectId(), Types.INTEGER)
                .addValue("_source_object_id", arrowDetails.getStartNttId(), Types.INTEGER)
                .addValue("_target_object_id", arrowDetails.getEndNttId(), Types.INTEGER)
                .addValue("_object_name", arrowDetails.getName(), Types.VARCHAR)
                .addValue("_object_shortdesc", arrowDetails.getShortDesc(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap newID = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("returnId", newID.get("returnId"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    @RequestMapping(method = POST, value = "/data-models/shape")
    @ResponseBody
    public Map<String, Object> writeShape(@RequestBody ShapeDetails shapeDetails, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_shape");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_class_id", 43, Types.INTEGER)
                .addValue("_object_id", shapeDetails.getObjectId(), Types.INTEGER)
                .addValue("_shape_template_id", shapeDetails.getShapeTemplateId(), Types.INTEGER)
                .addValue("_object_name", shapeDetails.getName(), Types.VARCHAR)
                .addValue("_object_shortdesc", shapeDetails.getShortDesc(), Types.VARCHAR)
                .addValue("_is_text", shapeDetails.getIsText(), Types.BOOLEAN)
                .addValue("_text", shapeDetails.getText(), Types.VARCHAR)
                .addValue("_h_align", shapeDetails.gethAlign(), Types.VARCHAR)
                .addValue("_v_align", shapeDetails.getvAlign(), Types.INTEGER)
                .addValue("_ct_class_id", shapeDetails.getCtClassId(), Types.INTEGER)
                .addValue("_ct_object_id", shapeDetails.getCtObjectId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap resultData = (LinkedCaseInsensitiveMap) rslt.get(0);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    @RequestMapping(method = POST, value = "/data-models/shape/ct")
    @ResponseBody
    public Map<String, Object> writeShapeCt(@RequestBody ShapeDetails shapeDetails, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("update_diagram_shape_ct");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", shapeDetails.getObjectId(), Types.INTEGER)
                .addValue("_ct_class_id", shapeDetails.getCtClassId(), Types.INTEGER)
                .addValue("_ct_object_id", shapeDetails.getCtObjectId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    @RequestMapping(method = POST, value = "/data-models/shape/align")
    @ResponseBody
    public Map<String, Object> writeShapeAlign(@RequestBody ShapeDetails shapeDetails, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_shape_align");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", shapeDetails.getObjectId(), Types.INTEGER)
                .addValue("_h_align", shapeDetails.gethAlign(), Types.VARCHAR)
                .addValue("_v_align", shapeDetails.getvAlign(), Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * PdmDiagram models the summary information of a Data Model Diagram.
     *  The diagram class can be Physical or Semantic or user defined.
     */
    static class DataModelDiagram {
        private Integer diagramId;
        private Integer classId;
        private String diagramName;
        private String diagramShortDesc;
        private Object diagramProperties;
        private String content;

        public DataModelDiagram() {
            this.diagramId = 0;
            this.classId = 0;
            this.diagramName = "";
            this.diagramShortDesc = "";
            this.diagramProperties = new Object();
        }

        public Integer getDiagramId() {
            return diagramId;
        }

        public void setDiagramId(Integer diagramId) {
            this.diagramId = diagramId;
        }

        public Integer getClassId() {
            return classId;
        }

        public void setClassId(Integer classId) {
            this.classId = classId;
        }

        public String getDiagramName() {
            return diagramName;
        }

        public void setDiagramName(String diagramName) {
            this.diagramName = diagramName;
        }

        public Object getDiagramProperties() { return diagramProperties; }

        public void setDiagramProperties(Object diagramProperties) { this.diagramProperties = diagramProperties; }

        public String getDiagramShortDesc() {
            return diagramShortDesc;
        }

        public void setDiagramShortDesc(String diagramShortDesc) {
            this.diagramShortDesc = diagramShortDesc;
        }

        public String getDiagramPropertiesAsString() {
            ObjectMapper objectMapper = new ObjectMapper(); // used to diagram properties Object as JSON string
            String diagramProps = new String();
            try {
                diagramProps = objectMapper.writeValueAsString(this.diagramProperties);
            }
            catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return diagramProps;
        }

    }

    /**
     * TODO: this should be a public class that extends io.datallama.api.common.DlConnector
     */
    static class ArrowDetails {
        private Integer objectId;
        private String name;
        private Integer startNttId;
        private Integer endNttId;
        private String shortDesc;

        public ArrowDetails() {}

        public ArrowDetails(Integer objectId, Integer classId, String name, String shortDesc) {
            this.objectId = objectId;
            this.name = name;
            this.shortDesc = shortDesc;
        }

        public Integer getObjectId() {
            return objectId;
        }

        public void setObjectId(Integer objectId) {
            this.objectId = objectId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getStartNttId() {
            return startNttId;
        }

        public void setStartNttId(Integer startNttId) {
            this.startNttId = startNttId;
        }

        public Integer getEndNttId() {
            return endNttId;
        }

        public void setEndNttId(Integer endNttId) {
            this.endNttId = endNttId;
        }

        public String getShortDesc() {
            return shortDesc;
        }

        public void setShortDesc(String shortDesc) {
            this.shortDesc = shortDesc;
        }
    }

    /**
     * TODO: this should be a public class that extends io.datallama.api.common.DlEntity
     */
    static class ShapeDetails {
        private Integer objectId;
        private String name;
        private String shortDesc;
        private Integer shapeTemplateId;
        private Boolean isText;
        private String text;
        private String hAlign;
        private Integer vAlign;
        private Integer ctClassId;
        private Integer ctObjectId;

        public ShapeDetails() {
            this.objectId = 0;
            this.name = "";
            this.shortDesc = "";
            this.shapeTemplateId = 0;
            this.isText = Boolean.FALSE;
            this.text = "";
            this.hAlign = "";
            this.vAlign = 0;
            this.ctClassId = 0;
            this.ctObjectId = 0;
        }

        public Boolean getIsText() {
            return isText;
        }

        public void setIsText(Boolean text) {
            this.isText = text;
        }

        public String getText(){
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Integer getObjectId() {
            return objectId;
        }

        public void setObjectId(Integer objectId) {
            this.objectId = objectId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortDesc() {
            return shortDesc;
        }

        public void setShortDesc(String shortDesc) {
            this.shortDesc = shortDesc;
        }

        public Integer getShapeTemplateId() {
            return shapeTemplateId;
        }

        public void setShapeTemplateId(Integer shapeTemplateId) {
            this.shapeTemplateId = shapeTemplateId;
        }

        public String gethAlign() {
            return hAlign;
        }

        public void sethAlign(String hAlign) {
            this.hAlign = hAlign;
        }

        public Integer getvAlign() {
            return vAlign;
        }

        public void setvAlign(Integer vAlign) {
            this.vAlign = vAlign;
        }

        public Integer getCtClassId() {
            return ctClassId;
        }

        public void setCtClassId(Integer ctClassId) {
            this.ctClassId = ctClassId;
        }

        public Integer getCtObjectId() {
            return ctObjectId;
        }

        public void setCtObjectId(Integer ctObjectId) {
            this.ctObjectId = ctObjectId;
        }
    }



}
