/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.datamodels;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.smd.SmdColumnDefinition;
import io.datallama.api.smd.SmdRelationshipDefinition;
import io.datallama.api.smd.SmdTableViewDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class PhysicalDataModelController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /data-models/pdm/diagram/{diagramId}/database/{databaseId}
     * Description: reads all tableviews in the SMD database associated with the diagram identified by diagramId
     *  NOTE: in the MVP databaseId is not strictly required because a PDM diagram can be associated with only ONE SMD database.
     *  However, in future this may become a one-to-many relationship.
     */
    @RequestMapping(method = GET, value = "/data-models/pdm/diagram/{diagramId}/database/{databaseId}")
    public Map<String, Object> readDiagramSmdDatabase(@PathVariable Long diagramId, @PathVariable Long databaseId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // first: get the summary information for the pdm diagram
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_candidate_tableviews");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER)
                .addValue("_dgrm_id", diagramId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        resultData.put("allEntities", rslt);

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_candidate_connectors");
        out = jdbcCall.execute(in);
        rslt = (ArrayList) out.get("#result-set-1");
        ArrayList<ConnectorContent> allConnectors = new ArrayList<>();
        for(Object listElement:rslt) {
            LinkedCaseInsensitiveMap connectorInfo = (LinkedCaseInsensitiveMap) listElement;
            Boolean inclusionSatus = (Integer)connectorInfo.get("included") == 1;
            String cnctStartNtt = String.format("%s:%s",connectorInfo.get("cnct_start_class_id"),connectorInfo.get("cnct_start_ntt"));
            String cnctEndNtt = String.format("%s:%s",connectorInfo.get("cnct_end_class_id"),connectorInfo.get("cnct_end_ntt"));

            ConnectorContent connector = new ConnectorContent(
                    (Integer)connectorInfo.get("object_id"),
                    (Integer)connectorInfo.get("class_id"),
                    connectorInfo.get("object_name").toString(),
                    connectorInfo.get("object_shortdesc").toString(),
                    cnctStartNtt,
                    cnctEndNtt,
                    "{}",
                    inclusionSatus
            );
            allConnectors.add(connector);
        }
        resultData.put("allConnectors", allConnectors);

        // TODO: update this to use the PdmTableViewDefinition private class

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /data-models/pdm/diagram/candidaterelationships/{tableViewIdList}
     * Description: retrieves all relationships between tableviews included in parameter tableViewIdList
     *
     */
    @RequestMapping(method = GET, value = "/data-models/pdm/diagram/candidaterelationships/{tableViewIdList}")
    public Map<String, Object> readDiagramSmdRelationships(@PathVariable String tableViewIdList) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.
        ArrayList<SmdRelationshipDefinition> relationshipResults = new ArrayList<SmdRelationshipDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_candidate_relationships");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_tableview_id_list", tableViewIdList, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        for(Object listElement:rslt) {
            LinkedCaseInsensitiveMap relationshipInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdRelationshipDefinition relationship = new SmdRelationshipDefinition(
                    (Integer) relationshipInfo.get("smd_relationship_id"),
                    (Integer) relationshipInfo.get("class_id"),
                    (Integer) relationshipInfo.get("create_mode"),
                    relationshipInfo.get("smd_relationship_name").toString(),
                    (Integer) relationshipInfo.get("parent_table_view_id"),
                    (Integer) relationshipInfo.get("parent_class_id"),
                    relationshipInfo.get("parent_table_view_name").toString(),
                    (Integer) relationshipInfo.get("child_table_view_id"),
                    (Integer) relationshipInfo.get("child_class_id"),
                    relationshipInfo.get("child_table_view_name").toString() );

            relationshipResults.add(relationship);

        }

        resultData.put("relationship_list", relationshipResults);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * URL Path: /data-models/pdm/diagram/connector/candidaterelationships/{parentTableViewId}/{childTableViewId}
     * Description: retrieves all relationships between a parent tableview and child parent that are candidates for a
     * match to a manually added connector in a PDM diagram
     *
     */
    @RequestMapping(method = GET, value = "/data-models/pdm/diagram/connector/candidaterelationships/{parentTableViewId}/{childTableViewId}")
    public Map<String, Object> readConnectorCandidateSmdRelationships(@PathVariable Integer parentTableViewId, @PathVariable Integer childTableViewId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.
        ArrayList<SmdRelationshipDefinition> relationshipResults = new ArrayList<SmdRelationshipDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_connector_candidate_relationships");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_parent_tableview_id", parentTableViewId, Types.INTEGER)
                .addValue("_child_tableview_id", childTableViewId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        for(Object listElement:rslt) {
            LinkedCaseInsensitiveMap relationshipInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdRelationshipDefinition relationship = new SmdRelationshipDefinition(
                    (Integer) relationshipInfo.get("smd_relationship_id"),
                    (Integer) relationshipInfo.get("class_id"),
                    (Integer) relationshipInfo.get("create_mode"),
                    relationshipInfo.get("smd_relationship_name").toString(),
                    (Integer) relationshipInfo.get("parent_table_view_id"),
                    (Integer) relationshipInfo.get("parent_class_id"),
                    relationshipInfo.get("parent_table_view_name").toString(),
                    (Integer) relationshipInfo.get("child_table_view_id"),
                    (Integer) relationshipInfo.get("child_class_id"),
                    relationshipInfo.get("child_table_view_name").toString() );

            relationshipResults.add(relationship);
        }

        resultData.put("relationship_list", relationshipResults);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * URL Path: /data-models/pdm/diagram/content/{diagramId}
     * Description: reads summary info and details of the diagram content, i.e. table/view entities and relationships.
     *  Steps are as follows:
     *   1) get summary info for the PDM diagram
     *   2) get the table/view and column content
     *   3) link tables/views and columns into a result set
     *   4) get the relationships between table/view entities
     */
    @RequestMapping(method = GET, value = "/data-models/pdm/diagram/content/{diagramId}")
    public Map<String, Object> readDiagramContent(@PathVariable Long diagramId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        resultData.put("id",diagramId);
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dm_diagram_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("class_id", (Integer) rsltMap.get("class_id"));
        resultData.put("name", rsltMap.get("dgrm_name"));
        resultData.put("properties", rsltMap.get("dgrm_prop"));
        resultData.put("shortDesc", rsltMap.get("dgrm_desc"));

        // second, get the tableview and column content
        Map<String, PdmTableViewDefinition> tableViewDefinitions = new HashMap<>();
        ArrayList<PdmTableViewDefinition> tableViewResults = new ArrayList<PdmTableViewDefinition>();
        ArrayList<PdmRelationshipDefinition> relationshipResults = new ArrayList<PdmRelationshipDefinition>();

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_smd_tableview");
        in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);
        Map<String, Object> pdmTablesViews = jdbcCall.execute(in);

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_smd_column");
        in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);
        Map<String, Object> pdmColumns = jdbcCall.execute(in);

        // third, link tableviews and columns into a result set using the utility classes SmdTableViewDefinition and SmdColumnDefinition
        ArrayList tableViewList = (ArrayList) pdmTablesViews.get("#result-set-1");

        for(Object listElement:tableViewList) {
            LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) listElement;
            PdmTableViewDefinition tableViewDefinition = new PdmTableViewDefinition(
                    (Integer) tableViewInfo.get("smd_table_view_id"),
                    (Integer) tableViewInfo.get("class_id"),
                    (Integer) tableViewInfo.get("smd_schema_id"),
                    tableViewInfo.get("smd_table_view_name").toString(),
                    (Integer) tableViewInfo.get("smd_ntt_x"),
                    (Integer) tableViewInfo.get("smd_ntt_y"),
                    (Integer) tableViewInfo.get("smd_ntt_height"),
                    (Integer) tableViewInfo.get("smd_ntt_width"),
                    tableViewInfo.get("color_defn").toString());

            tableViewDefinitions.put(tableViewInfo.get("smd_table_view_id").toString(), tableViewDefinition);
        }

        ArrayList columnList = (ArrayList) pdmColumns.get("#result-set-1");
        for(Object listElement:columnList) {
            LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdColumnDefinition columnDefinition = new SmdColumnDefinition(
                    (Integer) columnInfo.get("smd_column_id"),
                    columnInfo.get("smd_column_name").toString(),
                    (Integer) columnInfo.get("smd_column_ordinal"),
                    columnInfo.get("smd_column_datatype").toString()
            );

            PdmTableViewDefinition tableViewDefinition = tableViewDefinitions.get(columnInfo.get("smd_table_view_id").toString());
            tableViewDefinition.addColumnDefinition(columnDefinition);

            tableViewDefinitions.replace(columnInfo.get("smd_table_view_id").toString(), tableViewDefinition);
        }

        Iterator it = tableViewDefinitions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            tableViewResults.add((PdmTableViewDefinition) pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }

        resultData.put("entities", tableViewResults);

        // fourth, get the relationships between tableview entities
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_smd_connector");
        in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);
        Map<String, Object> pdmRelationships = jdbcCall.execute(in);

        ArrayList relationshipList = (ArrayList) pdmRelationships.get("#result-set-1");
        for(Object listElement:relationshipList) {
            LinkedCaseInsensitiveMap relationshipInfo = (LinkedCaseInsensitiveMap) listElement;

            PdmRelationshipDefinition relationshipDefinition = new PdmRelationshipDefinition(
                    (Integer) relationshipInfo.get("smd_relationship_id"),
                    (Integer) relationshipInfo.get("class_id"),
                    (Integer) relationshipInfo.get("create_mode"),
                    relationshipInfo.get("smd_relationship_name").toString(),
                    (Integer) relationshipInfo.get("parent_table_view_id"),
                    (Integer) relationshipInfo.get("parent_column_id"),
                    (Integer) relationshipInfo.get("parent_class_id"),
                    relationshipInfo.get("parent_table_view_name").toString(),
                    (Integer) relationshipInfo.get("child_table_view_id"),
                    (Integer) relationshipInfo.get("child_column_id"),
                    (Integer) relationshipInfo.get("child_class_id"),
                    relationshipInfo.get("child_table_view_name").toString(),
                    relationshipInfo.get("cnct_start_port").toString(),
                    relationshipInfo.get("cnct_end_port").toString(),
                    relationshipInfo.get("cnct_vertices").toString()
            );

            relationshipResults.add(relationshipDefinition);
        }
        resultData.put("connectors", relationshipResults);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/data-models/pdm/diagram/connectors/{diagramId}")
    public Map<String, Object> readAllConnectors(@PathVariable Long diagramId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        resultData.put("id",diagramId);
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pdm_diagram_candidate_connectors");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        resultData.put("allConnectors", rslt);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /data-models/pdm/diagram/content
     * Description: writes content of diagram to database
     */
    @RequestMapping(method = POST, value = "/data-models/pdm/diagram/content")
    public Map<String, Object> saveDiagramContent(@RequestBody DiagramContent diagramContent, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.
        ObjectMapper objectMapper = new ObjectMapper(); // used to parse Maps to JSON strings

        //first, save the diagram properties
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("update_diagram_properties");
        SqlParameterSource in =  new MapSqlParameterSource()
                .addValue("_user_id",Integer.parseInt(userId), Types.INTEGER)
                .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                .addValue("_dgrm_prop", diagramContent.getDiagramPropertiesAsString(), Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        // second, save the table/view entities
        ArrayList tableViewList = diagramContent.getEntities();
        for(Object listElement:tableViewList) {
            LinkedHashMap<String, Object> tableViewInfo = (LinkedHashMap) listElement;
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_pdm_diagram_tableview");
            in = new MapSqlParameterSource()
                    .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                    .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_smd_table_view_id", tableViewInfo.get("nttId"), Types.INTEGER)
                    .addValue("_smd_ntt_x", tableViewInfo.get("x"), Types.INTEGER)
                    .addValue("_smd_ntt_y", tableViewInfo.get("y"), Types.INTEGER)
                    .addValue("_smd_ntt_height", tableViewInfo.get("height"), Types.INTEGER)
                    .addValue("_smd_ntt_width", tableViewInfo.get("width"), Types.INTEGER);

            rslt = jdbcCall.execute(in);
            // TODO: handle errors from the JDBC Call
        }

        // third, delete table/view entities
        ArrayList tableViewDeletes = diagramContent.getEntityDeletes();
        for(Object listElement:tableViewDeletes) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_pdm_diagram_tableview");
            in = new MapSqlParameterSource().addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_smd_table_view_id", (Integer) listElement, Types.INTEGER);
            rslt = jdbcCall.execute(in);
        }

        // fourth, save the relationships
        ArrayList relationshipList = diagramContent.getConnectors();
        for(Object listElement:relationshipList) {
            LinkedHashMap<String, Object> relationshipInfo = (LinkedHashMap) listElement;

            String verticesJSON = "";
            try {
                verticesJSON = objectMapper.writeValueAsString(relationshipInfo.get("cnctVertices"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            Map<String, Object> diagramProperties = null;
            try {
                diagramProperties = objectMapper.readValue(diagramContent.getDiagramPropertiesAsString(), Map.class);
            }
            catch (Exception e) {
                System.out.println("Exception thrown while parsing JSON for diagram properties. Handle this correctly.");
                results.put("result_data", "Exception thrown while parsing JSON for diagram properties. Handle this correctly.");
                results.put("result_status", -1);
                return results;
            }
            String startPort = relationshipInfo.get("cnctStartPort").toString();
            String endPort = relationshipInfo.get("cnctEndPort").toString();

            String[] startIds = startPort.split(":");
            String[] endIds = endPort.split(":");

            Integer parentTableViewId = Integer.parseInt(startIds[1]);
            Integer childTableViewId = Integer.parseInt(endIds[1]);

            String  shortDesc = "";
            if (relationshipInfo.get("cnctDesc") != null)
                shortDesc = relationshipInfo.get("cnctDesc").toString();

            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_pdm_diagram_connector");
            in = new MapSqlParameterSource()
                    .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                    .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_smd_database_id", diagramProperties.get("smdDatabaseId"), Types.INTEGER)
                    .addValue("_smd_relationship_id", (Integer) relationshipInfo.get("cnctId"), Types.INTEGER)
                    .addValue("_class_id", (Integer) relationshipInfo.get("classId"), Types.INTEGER)
                    .addValue("_parent_table_view_id", parentTableViewId, Types.INTEGER)
                    .addValue("_child_table_view_id", childTableViewId, Types.INTEGER)
                    .addValue("_smd_relationship_name", relationshipInfo.get("cnctName").toString(), Types.VARCHAR)
                    .addValue("_smd_relationship_shortdesc", shortDesc, Types.VARCHAR)
                    .addValue("_cnct_start_port", startPort, Types.VARCHAR)
                    .addValue("_cnct_end_port", endPort, Types.VARCHAR)
                    .addValue("_vertices", verticesJSON, Types.VARCHAR);

            rslt = jdbcCall.execute(in);
            // TODO: handle errors from the JDBC Call
        }

        // fifth, delete the relationships
        ArrayList relationshipDeletes = diagramContent.getConnectorDeletes();
        for(Object listElement:relationshipDeletes) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_pdm_diagram_connector");
            in = new MapSqlParameterSource().addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_smd_relationship_id", listElement, Types.INTEGER);
            rslt = jdbcCall.execute(in);
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * PdmDiagramContent models the table/view and key relationship content of a physical data model diagram.
     */
    static class PdmDiagramContent {
        private Integer diagramId;
        private Integer classId;
        private String diagramName;
        private String diagramDesc;
        private Integer smdDatabaseId;
        private String smdDatabaseName;
        private Object diagramProperties;
        private ArrayList<Object> tableViewList;
        private ArrayList<Object> tableViewDeletes;
        private ArrayList<Object> relationshipList;
        private ArrayList<Object> relationshipDeletes;

        public Integer getDiagramId() { return diagramId; }

        public void setDiagramId(Integer diagramId) { this.diagramId = diagramId; }

        public Integer getSmdDatabaseId() { return smdDatabaseId; }

        public void setSmdDatabaseId(Integer smdDatabaseId) { this.smdDatabaseId = smdDatabaseId; }

        public Integer getClassId() { return classId; }

        public void setClassId(Integer classId) { this.classId = classId; }

        public String getDiagramName() { return diagramName; }

        public void setDiagramName(String diagramName) { this.diagramName = diagramName; }

        public String getDiagramDesc() { return diagramDesc; }

        public void setDiagramDesc(String diagramDesc) { this.diagramDesc = diagramDesc; }

        public String getSmdDatabaseName() { return smdDatabaseName; }

        public void setSmdDatabaseName(String smdDatabaseName) { this.smdDatabaseName = smdDatabaseName; }

        public Object getDiagramProperties() { return diagramProperties; }

        public void setDiagramProperties(Object diagramProperties) { this.diagramProperties = diagramProperties; }

        public ArrayList<Object> getTableViewList() { return tableViewList; }

        public void setTableViewList(ArrayList<Object> tableViewList) { this.tableViewList = tableViewList; }

        public ArrayList<Object> getTableViewDeletes() { return tableViewDeletes; }

        public void setTableViewDeletes(ArrayList<Object> tableViewDeletes) { this.tableViewDeletes = tableViewDeletes; }

        public ArrayList<Object> getRelationshipList() { return relationshipList; }

        public void setRelationshipList(ArrayList<Object> relationshipList) { this.relationshipList = relationshipList; }

        public ArrayList<Object> getRelationshipDeletes() { return relationshipDeletes; }

        public void setRelationshipDeletes(ArrayList<Object> relationshipDeletes) { this.relationshipDeletes = relationshipDeletes; }

        public String getDiagramPropertiesAsString() {
            ObjectMapper objectMapper = new ObjectMapper(); // used to diagram properties Object as JSON string
            String diagramProps = new String();
            try {
                diagramProps = objectMapper.writeValueAsString(this.diagramProperties);
            }
            catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return diagramProps;
        }

    }

    /**
     * PdmTableViewDefinition models an SMD table/view entity that is part of an SMD database definition as well as
     * and optionally part of a PDM diagram.
     */
    static class PdmTableViewDefinition extends SmdTableViewDefinition {
        private Integer x;
        private Integer y;
        private Integer height;
        private Integer width;
        private String colorDefn;
        private Integer Selected = 0;
        private Integer SelectionChange = 0;
        private Integer nttId;
        private String nttName;
        private String nttDesc;

/*
        lo.object_id as nttId,
        lo.class_id as classId,
        lo.object_name as nttName,
        ifnull(lo.object_shortdesc, 'No description available') as nttDesc,
        de.ntt_x as x,
        de.ntt_y as y,
        de.ntt_height as height,
        de.ntt_width as width,
        de.color_defn as colorDefn
 */

        public PdmTableViewDefinition(Integer tableViewId, Integer classId, Integer schemaId, String tableViewName,
                                      Integer x, Integer y, Integer height, Integer width, String colorDefn) {
            super(tableViewId, classId, schemaId, tableViewName);
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.colorDefn = colorDefn;
            this.nttId = tableViewId;
            this.nttName = tableViewName;
            this.nttDesc = "Description is not yet configured in the Data Llama API";
        }

        public Integer getX() {
            return x;
        }

        public void setX(Integer x) {
            this.x = x;
        }

        public Integer getY() {
            return y;
        }

        public void setY(Integer y) {
            this.y = y;
        }

        public Integer getHeight() {
            return height;
        }

        public void setHeight(Integer height) {
            this.height = height;
        }

        public Integer getWidth() {
            return width;
        }

        public void setWidth(Integer width) {
            this.width = width;
        }

        public String getColorDefn() {
            return colorDefn;
        }

        public void setColorDefn(String colorDefn) {
            this.colorDefn = colorDefn;
        }

        public Integer getSelected() { return Selected; }

        public void setSelected(Integer selected) { Selected = selected; }

        public Integer getSelectionChange() { return SelectionChange; }

        public void setSelectionChange(Integer selectionChange) { SelectionChange = selectionChange; }

        public Integer getNttId() {
            return nttId;
        }

        public void setNttId(Integer nttId) {
            this.nttId = nttId;
        }

        public String getNttName() {
            return nttName;
        }

        public void setNttName(String nttName) {
            this.nttName = nttName;
        }

        public String getNttDesc() {
            return nttDesc;
        }

        public void setNttDesc(String nttDesc) {
            this.nttDesc = nttDesc;
        }
    }

    /**
     * Description: PdmRelationshipDefinition models an relationship between SMD tableview entities in a PDM diagram.
     */
    static class PdmRelationshipDefinition extends SmdRelationshipDefinition {
        private String cnctStartPort;
        private String cnctEndPort;
        private String cnctVertices;
        private int cnctId;
        private String cnctName;

        public PdmRelationshipDefinition(Integer relationshipId, Integer classId, Integer createMode, String relationshipName,
                                         Integer parentTableViewId, Integer parentColumnId, Integer parentClassId, String parentTableViewName,
                                         Integer childTableViewId, Integer childColumnId, Integer childClassId, String childTableViewName,
                                         String startPortName, String endPortName, String verticesString) {

            super (relationshipId, classId, createMode, relationshipName, parentTableViewId, parentClassId, parentTableViewName,
                    childTableViewId, childClassId, childTableViewName);
            this.cnctStartPort = startPortName;
            this.cnctEndPort = endPortName;
            this.cnctVertices = verticesString;
            this.cnctId = relationshipId;
            this.cnctName = relationshipName;
        }

        public String getCnctStartPort() { return cnctStartPort; }

        public void setCnctStartPort(String cnctStartPort) { this.cnctStartPort = cnctStartPort; }

        public String getCnctEndPort() { return cnctEndPort; }

        public void setCnctEndPort(String cnctEndPort) { this.cnctEndPort = cnctEndPort; }

        public String getCnctVertices() { return cnctVertices; }

        public void setCnctVertices(String cnctVertices) { this.cnctVertices = cnctVertices; }

        public int getCnctId() {
            return cnctId;
        }

        public void setCnctId(int cnctId) {
            this.cnctId = cnctId;
        }

        public String getCnctName() {
            return cnctName;
        }

        public void setCnctName(String cnctName) {
            this.cnctName = cnctName;
        }
    }

    /**
     * Description: PdmCoordinate models a simple x,y coordinate in a vertice. It is used in PdmRelationshipDefinition
     */
    static class PdmCoordinate {
        private Integer x;
        private Integer y;

        public PdmCoordinate(String x, String y) {
            this.x = Integer.parseInt(x);
            this.y = Integer.parseInt(y);
        }

        public Integer getX() { return x; }

        public void setX(Integer x) { this.x = x; }

        public Integer getY() { return y; }

        public void setY(Integer y) { this.y = y; }
    }
}
