/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.association;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

/**
 * Models the full lineage association chain incoming and outgoing for a specific DL Metadata Object
 */
public class LineageDetails {
    private Integer objectId;
    private Integer classId;
    private Boolean repeated;
    private String  bg;
    private Integer lineageLevel;
    private String hierarchyNumber;
    private String lineageDescription;
    private ArrayList<DlMetaDataContextNode> mdContext;

    public LineageDetails() {}

    public LineageDetails(Integer objectId, Integer classId, Boolean repeated, Integer lineageLevel, String hierarchyNumber, String lineageDescription) {
        this.objectId = objectId;
        this.classId = classId;
        this.repeated = repeated;
        this.lineageLevel = lineageLevel;
        this.hierarchyNumber = hierarchyNumber;
        this.lineageDescription = lineageDescription;
        this.mdContext = new ArrayList<>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getLineageLevel() {
        return lineageLevel;
    }

    public void setLineageLevel(Integer lineageLevel) {
        this.lineageLevel = lineageLevel;
    }

    public String getHierarchyNumber() {
        return hierarchyNumber;
    }

    public void setHierarchyNumber(String hierarchyNumber) {
        this.hierarchyNumber = hierarchyNumber;
    }

    public String getLineageDescription() {
        return lineageDescription;
    }

    public void setLineageDescription(String lineageDescription) {
        this.lineageDescription = lineageDescription;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public Boolean getRepeated() {
        return repeated;
    }

    public void setRepeated(Boolean repeated) {
        this.repeated = repeated;
    }
}
