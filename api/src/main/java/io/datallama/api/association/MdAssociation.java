package io.datallama.api.association;

/**
 * MdAssociation models an association between source and target DL metadata objects.
 */
public class MdAssociation {
    private Integer assocId;
    private Integer sourceObjectId;
    private Integer sourceClassId;
    private Integer targetObjectId;
    private Integer targetClassId;
    private String assocShortDesc;
    private Integer assocTypeId;

    public MdAssociation() {
        this.assocId = 0;
        this.sourceObjectId = 0;
        this.sourceClassId = 0;
        this.targetObjectId = 0;
        this.targetClassId = 0;
        this.assocShortDesc = "";
        this.assocTypeId = 0;
    }

    public MdAssociation(Integer assocId, Integer sourceObjectId, Integer sourceClassId, Integer targetObjectId, Integer targetClassId, String assocShortDesc, Integer assocTypeId) {
        this.assocId = assocId;
        this.sourceObjectId = sourceObjectId;
        this.sourceClassId = sourceClassId;
        this.targetObjectId = targetObjectId;
        this.targetClassId = targetClassId;
        this.assocShortDesc = assocShortDesc;
        this.assocTypeId = assocTypeId;
    }

    public Integer getAssocTypeId() {
        return assocTypeId;
    }

    public void setAssocTypeId(Integer assocTypeId) {
        this.assocTypeId = assocTypeId;
    }

    public Integer getSourceObjectId() {
        return sourceObjectId;
    }

    public void setSourceObjectId(Integer sourceObjectId) {
        this.sourceObjectId = sourceObjectId;
    }

    public Integer getSourceClassId() {
        return sourceClassId;
    }

    public void setSourceClassId(Integer sourceClassId) {
        this.sourceClassId = sourceClassId;
    }

    public Integer getTargetObjectId() {
        return targetObjectId;
    }

    public void setTargetObjectId(Integer targetObjectId) {
        this.targetObjectId = targetObjectId;
    }

    public Integer getTargetClassId() {
        return targetClassId;
    }

    public void setTargetClassId(Integer targetClassId) {
        this.targetClassId = targetClassId;
    }

    public String getAssocShortDesc() {
        return assocShortDesc;
    }

    public void setAssocShortDesc(String assocShortDesc) {
        this.assocShortDesc = assocShortDesc;
    }

    public Integer getAssocId() {
        return assocId;
    }

    public void setAssocId(Integer assocId) {
        this.assocId = assocId;
    }

}
