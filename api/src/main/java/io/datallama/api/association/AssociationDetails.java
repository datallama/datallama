/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.association;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class AssociationDetails {
    private Integer assocId;
    private Integer assocTypeId;
    private String  assocTypeName;
    private String  assocSourcePhrase;
    private String  assocTargetPhrase;
    private String  assocShortDesc;
    private Integer sourceClassId;
    private Integer sourceObjectId;
    private String  sourceClassName;
    private String  sourceObjectName;
    private String  sourceShortDesc;
    private ArrayList<DlMetaDataContextNode> sourceMdContext;
    private Integer targetClassId;
    private Integer targetObjectId;
    private String  targetClassName;
    private String  targetObjectName;
    private ArrayList<DlMetaDataContextNode> targetMdContext;
    private String  targetShortDesc;

    public AssociationDetails() {}

    public AssociationDetails(Integer assocId, Integer assocTypeId, String assocTypeName, String assocSourcePhrase,
                              String assocTargetPhrase, String assocShortDesc, Integer sourceClassId, Integer sourceObjectId,
                              String sourceClassName, String sourceObjectName, String sourceShortDesc, Integer targetClassId,
                              Integer targetObjectId, String targetClassName, String targetObjectName, String targetShortDesc) {
        this.assocId = assocId;
        this.assocTypeId = assocTypeId;
        this.assocTypeName = assocTypeName;
        this.assocSourcePhrase = assocSourcePhrase;
        this.assocTargetPhrase = assocTargetPhrase;
        this.assocShortDesc = assocShortDesc;
        this.sourceClassId = sourceClassId;
        this.sourceObjectId = sourceObjectId;
        this.sourceMdContext = new ArrayList<>();
        this.sourceClassName = sourceClassName;
        this.sourceObjectName = sourceObjectName;
        this.sourceShortDesc = sourceShortDesc;
        this.targetClassId = targetClassId;
        this.targetObjectId = targetObjectId;
        this.targetMdContext = new ArrayList<>();
        this.targetClassName = targetClassName;
        this.targetObjectName = targetObjectName;
        this.targetShortDesc = targetShortDesc;
    }

    public Integer getAssocId() {
        return assocId;
    }

    public void setAssocId(Integer assocId) {
        this.assocId = assocId;
    }

    public Integer getAssocTypeId() {
        return assocTypeId;
    }

    public void setAssocTypeId(Integer assocTypeId) {
        this.assocTypeId = assocTypeId;
    }

    public String getAssocTypeName() {
        return assocTypeName;
    }

    public void setAssocTypeName(String assocTypeName) {
        this.assocTypeName = assocTypeName;
    }

    public String getAssocSourcePhrase() {
        return assocSourcePhrase;
    }

    public void setAssocSourcePhrase(String assocSourcePhrase) {
        this.assocSourcePhrase = assocSourcePhrase;
    }

    public String getAssocTargetPhrase() {
        return assocTargetPhrase;
    }

    public void setAssocTargetPhrase(String assocTargetPhrase) {
        this.assocTargetPhrase = assocTargetPhrase;
    }

    public String getAssocShortDesc() {
        return assocShortDesc;
    }

    public void setAssocShortDesc(String assocShortDesc) {
        this.assocShortDesc = assocShortDesc;
    }

    public Integer getSourceClassId() {
        return sourceClassId;
    }

    public void setSourceClassId(Integer sourceClassId) {
        this.sourceClassId = sourceClassId;
    }

    public Integer getSourceObjectId() {
        return sourceObjectId;
    }

    public void setSourceObjectId(Integer sourceObjectId) {
        this.sourceObjectId = sourceObjectId;
    }

    public String getSourceClassName() {
        return sourceClassName;
    }

    public void setSourceClassName(String sourceClassName) {
        this.sourceClassName = sourceClassName;
    }

    public String getSourceObjectName() {
        return sourceObjectName;
    }

    public void setSourceObjectName(String sourceObjectName) {
        this.sourceObjectName = sourceObjectName;
    }

    public Integer getTargetClassId() {
        return targetClassId;
    }

    public void setTargetClassId(Integer targetClassId) {
        this.targetClassId = targetClassId;
    }

    public Integer getTargetObjectId() {
        return targetObjectId;
    }

    public void setTargetObjectId(Integer targetObjectId) {
        this.targetObjectId = targetObjectId;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public void setTargetClassName(String targetClassName) {
        this.targetClassName = targetClassName;
    }

    public String getTargetObjectName() {
        return targetObjectName;
    }

    public void setTargetObjectName(String targetObjectName) {
        this.targetObjectName = targetObjectName;
    }

    public ArrayList<DlMetaDataContextNode> getSourceMdContext() {
        return sourceMdContext;
    }

    public void setSourceMdContext(ArrayList<DlMetaDataContextNode> sourceMdContext) {
        this.sourceMdContext = sourceMdContext;
    }

    public ArrayList<DlMetaDataContextNode> getTargetMdContext() {
        return targetMdContext;
    }

    public void setTargetMdContext(ArrayList<DlMetaDataContextNode> targetMdContext) {
        this.targetMdContext = targetMdContext;
    }

    public void addSourceMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.sourceMdContext.add(mdContextNode);
    }

    public void addTargetMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.targetMdContext.add(mdContextNode);
    }

    public String getSourceShortDesc() {
        return sourceShortDesc;
    }

    public void setSourceShortDesc(String sourceShortDesc) {
        this.sourceShortDesc = sourceShortDesc;
    }

    public String getTargetShortDesc() {
        return targetShortDesc;
    }

    public void setTargetShortDesc(String targetShortDesc) {
        this.targetShortDesc = targetShortDesc;
    }
}

