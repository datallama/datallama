/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.association;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class TaxonomyElementAssociation {
    private Integer elementId;
    private Integer classId;
    private String elementName;
    private String elementShortDesc;
    private Boolean parent;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private ArrayList<AssociatedObject> associatedObjects;

    public TaxonomyElementAssociation() {
        this.elementId = 0;
        this.classId = 0;
        this.elementName = "";
        this.elementShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.associatedObjects = new ArrayList<AssociatedObject>();
        this.parent = false;
    }

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementShortDesc() {
        return elementShortDesc;
    }

    public void setElementShortDesc(String elementShortDesc) {
        this.elementShortDesc = elementShortDesc;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public ArrayList<AssociatedObject> getAssociatedObjects() {
        return associatedObjects;
    }

    public void setAssociatedObjects(ArrayList<AssociatedObject> associatedObjects) {
        this.associatedObjects = associatedObjects;
    }

    public void addAssociatedObject(AssociatedObject associatedObject) {
        this.associatedObjects.add(associatedObject);
    }
}
