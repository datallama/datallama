/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.association;

import java.util.*;

public class TaxonomyAssociationFullView {
    private ArrayList<TaxonomyElementAssociation> taxonomyElementAssociations;

    public TaxonomyAssociationFullView() {
        this.taxonomyElementAssociations = new ArrayList<TaxonomyElementAssociation>();
    }

    public ArrayList<TaxonomyElementAssociation> getTaxonomyElementAssociations() {
        return taxonomyElementAssociations;
    }

    public void setTaxonomyElementAssociations(ArrayList<TaxonomyElementAssociation> taxonomyElementAssociations) {
        this.taxonomyElementAssociations = taxonomyElementAssociations;
    }

    public void addTaxonomyElementAssociation(TaxonomyElementAssociation taxonomyElementAssociation) {
        this.taxonomyElementAssociations.add(taxonomyElementAssociation);
    }
}
