/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.association;

import io.datallama.api.metamodel.DlMetaDataContextNode;
import java.util.ArrayList;

public class AssociatedObject {
    private Integer associationId;
    private Integer objectId;
    private Integer classId;
    private String objectName;
    private String className;
    private String associationShortDesc;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private String assocTypeName;
    private String verbPhrase;
    private String reverseVerbPhrase;

    /**
     * Description: default no-parameter constructor
     */
    public AssociatedObject() {
        this.associationId = 0;
        this.objectId = 0;
        this.classId = 0;
        this.objectName = "";
        this.className = "";
        this.associationShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.assocTypeName = "";
        this.verbPhrase = "";
    }

    /**
     * Description: full constructor with all parameters specified
     * @param associationId
     * @param objectId
     * @param classId
     * @param objectName
     * @param className
     * @param associationShortDesc
     */
    public AssociatedObject(Integer associationId, Integer objectId, Integer classId, String objectName, String className,
                            String associationShortDesc, String assocTypeName, String verbPhrase) {
        this.associationId = associationId;
        this.objectId = objectId;
        this.classId = classId;
        this.objectName = objectName;
        this.className = className;
        this.associationShortDesc = associationShortDesc;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.assocTypeName = assocTypeName;
        this.verbPhrase = verbPhrase;
    }

    public String getAssocTypeName() {
        return assocTypeName;
    }

    public void setAssocTypeName(String assocTypeName) {
        this.assocTypeName = assocTypeName;
    }

    public String getVerbPhrase() {
        return verbPhrase;
    }

    public void setVerbPhrase(String verbPhrase) {
        this.verbPhrase = verbPhrase;
    }

    public Integer getAssociationId() {
        return associationId;
    }

    public void setAssociationId(Integer associationId) {
        this.associationId = associationId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getAssociationShortDesc() {
        return associationShortDesc;
    }

    public void setAssociationShortDesc(String associationShortDesc) {
        this.associationShortDesc = associationShortDesc;
    }

    public String getReverseVerbPhrase() {
        return reverseVerbPhrase;
    }

    public void setReverseVerbPhrase(String reverseVerbPhrase) {
        this.reverseVerbPhrase = reverseVerbPhrase;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }
}
