/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.association;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.metamodel.DlMetaDataContextNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class AssociationController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Saves a new metadata association in the DL database
     * @param associationList
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/association/content")
    @ResponseBody
    public Map<String, Object> writeAssociation(@RequestBody ArrayList<MdAssociation> associationList, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        ArrayList<Integer> returnIds = new ArrayList<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        AssociationUtil assocUtil = new AssociationUtil(dataSource, Integer.parseInt(userId));
        for (MdAssociation newAssociation : associationList) {
            Integer newAssocId = assocUtil.writeAssociation(newAssociation);
            returnIds.add(newAssocId);
        }

        results.put("result_data", returnIds);
        results.put("result_status", resultStatus);

        return results;
    }

    //Get all the defined association types
    @RequestMapping(method = GET, value = "/association/types")
    @ResponseBody
    public Map<String, Object> readAssociationTypes() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association_types");
        Map<String, Object> out = jdbcCall.execute();

        ArrayList<MdAssociationType> associationTypes = new ArrayList<>();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");
        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // first, create the AssociatedObject instance
            MdAssociationType assocType = new MdAssociationType(
                    (Integer) objectInfo.get("assoc_type_id"),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("assoc_type_shortdesc").toString(),
                    objectInfo.get("source_phrase").toString(),
                    objectInfo.get("target_phrase").toString()
            );
            associationTypes.add(assocType);
        }
        results.put("result_data", associationTypes);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Get details of the association identified by assocId
     * @param assocId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/{assocId}")
    @ResponseBody
    public Map<String, Object> readAssociationByAssocId(@PathVariable Integer assocId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association_by_assoc_id");
        MapSqlParameterSource in = new MapSqlParameterSource()
                .addValue("_assoc_id", assocId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        Map<String, Object> details = (Map<String, Object>)((ArrayList<Object>) out.get("#result-set-1")).get(0);
        AssociationDetails associationDetails = new AssociationDetails(
                assocId,
                (Integer) details.get("assoc_type_id"),
                details.get("assoc_type_name").toString(),
                details.get("source_phrase").toString(),
                details.get("target_phrase").toString(),
                details.get("assoc_shortdesc").toString(),
                (Integer) details.get("source_class_id"),
                (Integer) details.get("source_object_id"),
                details.get("source_class_name").toString(),
                details.get("source_object_name").toString(),
                details.get("source_shortdesc").toString(),
                (Integer) details.get("target_class_id"),
                (Integer) details.get("target_object_id"),
                details.get("target_class_name").toString(),
                details.get("target_object_name").toString(),
                details.get("target_shortdesc").toString()
        );

        // TODO: update AssociationDetails so that it stores a DlMetaDataContext object for source and target rather than DlMetaDataContextNode objects
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the md_context json array returned by stored procedure
        // second, add the metadata context for the source object
        List<LinkedHashMap> contextList = null;
        try {
            contextList = objectMapper.readValue((String) details.get("source_md_context"), List.class);
            for(LinkedHashMap contextNode:contextList) {
                DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                        Integer.parseInt(contextNode.get("class_id").toString()),
                        contextNode.get("class_name").toString(),
                        Integer.parseInt(contextNode.get("object_id").toString()),
                        contextNode.get("object_name").toString()
                );
                if(contextNode.containsKey("internal_path")) {
                    mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                }
                else {
                    mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                }
                associationDetails.addSourceMdContextNode(mdContextNode);
            }
        }
        catch (Exception e) {
            System.out.println("Exception thrown while parsing md_context JSON array.");
            System.out.println(e);
        }

        // third, add the metadata context for the target object
        try {
            contextList = objectMapper.readValue((String) details.get("target_md_context"), List.class);
            for(LinkedHashMap contextNode:contextList) {
                DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                        Integer.parseInt(contextNode.get("class_id").toString()),
                        contextNode.get("class_name").toString(),
                        Integer.parseInt(contextNode.get("object_id").toString()),
                        contextNode.get("object_name").toString()
                );
                if(contextNode.containsKey("internal_path")) {
                    mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                }
                else {
                    mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                }
                associationDetails.addTargetMdContextNode(mdContextNode);
            }
        }
        catch (Exception e) {
            System.out.println("Exception thrown while parsing md_context JSON array.");
            System.out.println(e);
        }

        results.put("result_data", associationDetails);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Get list of associations for the object identified by objectId, classId
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/associations/{objectId}/{classId}")
    @ResponseBody
    public Map<String, Object> readAssociations(@PathVariable Integer objectId, @PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // 1. get the list of associations for the md object
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        MapSqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList<AssociatedObject> associations = new ArrayList<>();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 1.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );
            associations.add(assocObject);
        }
        results.put("result_data", associations);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Retrieves all elements in a taxonomy and all metadata objects associated to elements
     * URL Path: /association/taxonomy/fullview
     * @param taxonomyId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/fullview/taxonomy/{taxonomyId}")
    @ResponseBody
    public Map<String, Object> readTaxonomyAssociationFullView(@PathVariable Integer taxonomyId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        TaxonomyAssociationFullView fullView = new TaxonomyAssociationFullView(); // this list will be returned in the results Map

        // 1: get the flattened list of elements for the taxonomy
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_elements");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_taxonomy_id", taxonomyId, Types.INTEGER)
                .addValue("_abbreviate", 0, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        TaxonomyElementAssociation elementAssoc = null;
        Map<Integer, TaxonomyElementAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object taxonomyElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) taxonomyElement;

            // 1.1: create the TaxonomyElementAssociation instance if control break triggered
            Integer currentElementId = (Integer) elementInfo.get("element_id");
            elementAssoc = new TaxonomyElementAssociation();
            elementAssoc.setElementId(currentElementId);
            elementAssoc.setClassId((Integer) elementInfo.get("class_id"));
            elementAssoc.setElementName(elementInfo.get("element_name").toString());
            elementAssoc.setElementShortDesc(elementInfo.get("element_shortdesc").toString());
            elementAssoc.setParent((Boolean) elementInfo.get("element_is_parent"));


            // 1.2: add the taxonomy element metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            elementAssoc.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

            fullView.addTaxonomyElementAssociation(elementAssoc);
            // add this ptDefinition to the lookup map
            parentLookup.put(elementAssoc.getElementId(), elementAssoc);
        }

        // 2: get all associated objects and add them to elements using elementId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_associations");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // 2.1: create an AssociatedObject for each row in the result set and add to the parent Taxonomy Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));
            TaxonomyElementAssociation parent = parentLookup.get((Integer) assocInfo.get("element_id"));
            if (parent != null)
                parent.addAssociatedObject(associatedObject);
        }

        resultData.put("taxonomy_elements", fullView.getTaxonomyElementAssociations());
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Deletes the association identified by assocId
     * @param assocId
     * @param userId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/association/{assocId}")
    public Map<String, Object> deleteAssociation(@PathVariable Integer assocId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_association");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_assoc_id", assocId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Retrieves all elements in a taxonomy and all metadata objects associated to elements
     * URL Path: /association/mdobject/fullview
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/fullview/format/ghmf/{objectId}")
    @ResponseBody
    public Map<String, Object> readGhmfAssociationFullView(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        MdObjectAssociationFullView fullView = new MdObjectAssociationFullView(); // this list will be returned in the results Map

        // 1: get the list of elements for the taxonomy
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_ghmf_elements");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_fmt_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        MdObjectAssociation elementAssoc = null;
        Map<Integer, MdObjectAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object taxonomyElement : dbResultList) {
            LinkedCaseInsensitiveMap mdObjectInfo = (LinkedCaseInsensitiveMap) taxonomyElement;

            // 1.1: create the TaxonomyElementAssociation instance if control break triggered
            Integer currentObjectId = (Integer) mdObjectInfo.get("object_id");
            elementAssoc = new MdObjectAssociation();
            elementAssoc.setObjectId(currentObjectId);
            elementAssoc.setClassId((Integer) mdObjectInfo.get("class_id"));
            elementAssoc.setObjectName(mdObjectInfo.get("element_name").toString());
            elementAssoc.setObjectShortDesc(mdObjectInfo.get("element_shortdesc").toString());
            elementAssoc.setParent((Boolean) mdObjectInfo.get("element_is_parent"));

            // 1.2: add the taxonomy element metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            elementAssoc.setMdContext(mdContext.generateMdContextList(mdObjectInfo.get("md_context").toString()));

            fullView.addMdObjectAssociation(elementAssoc);
            // add this ptDefinition to the lookup map
            parentLookup.put(elementAssoc.getObjectId(), elementAssoc);
        }

        // 2: get all associated objects and add them to elements using elementId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_ghmf_associations");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // create an AssociatedObject for each row in the result set and add to the parent GHMF Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );
            associatedObject.setReverseVerbPhrase(assocInfo.get("reverse_verb_phrase").toString());

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));
            MdObjectAssociation parent = parentLookup.get((Integer) assocInfo.get("object_id"));
            parent.addAssociatedObject(associatedObject);

        }

        results.put("result_data", fullView.getMdObjectAssociations());
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Retrieves all elements in a taxonomy and all metadata objects associated to elements
     * URL Path: /association/mdobject/fullview
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/fullview/smd/hierarchy/{objectId}")
    @ResponseBody
    public Map<String, Object> readSmdHierarchyAssociationFullView(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        MdObjectAssociationFullView fullView = new MdObjectAssociationFullView(); // this list will be returned in the results Map

        // 1. get the list of elements for the smd hierarchy and create a new MdObjectAssociation object for each element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_elements");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        MdObjectAssociation elementAssoc = null;
        Map<Integer, MdObjectAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object element : dbResultList) {
            LinkedCaseInsensitiveMap mdObjectInfo = (LinkedCaseInsensitiveMap) element;

            // 1.1. create a new MdObjectAssociation instance and add it to the fullView (MdObjectAssociationFullView) object
            Integer currentObjectId = (Integer) mdObjectInfo.get("object_id");
            elementAssoc = new MdObjectAssociation();
            elementAssoc.setObjectId(currentObjectId);
            elementAssoc.setClassId((Integer) mdObjectInfo.get("class_id"));
            elementAssoc.setObjectName(mdObjectInfo.get("object_name").toString());
            elementAssoc.setObjectShortDesc(mdObjectInfo.get("object_shortdesc").toString());
            elementAssoc.setParent((Boolean) mdObjectInfo.get("element_is_parent"));

            // 1.2: add the element md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            elementAssoc.setMdContext(mdContext.generateMdContextList(mdObjectInfo.get("md_context").toString()));

            fullView.addMdObjectAssociation(elementAssoc);

            // 1.3. add this mdObjectAssociation object to the lookup map
            parentLookup.put(elementAssoc.getObjectId(), elementAssoc);
        }

        // 2. get all associated objects and add them to elements using objectId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_associations");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // create an AssociatedObject for each row in the result set and add to the parent GHMF Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );
            associatedObject.setReverseVerbPhrase(assocInfo.get("reverse_verb_phrase").toString());

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));
            MdObjectAssociation parent = parentLookup.get((Integer) assocInfo.get("object_id"));
            parent.addAssociatedObject(associatedObject);

        }

        results.put("result_data", fullView.getMdObjectAssociations());
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Retrieves all elements in a taxonomy and all metadata objects associated to elements
     * URL Path: /association/mdobject/fullview
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/fullview/api/rest/{objectId}")
    @ResponseBody
    public Map<String, Object> readRestApiAssociationFullView(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        MdObjectAssociationFullView fullView = new MdObjectAssociationFullView(); // this list will be returned in the results Map

        // 1: get the list of elements for the taxonomy
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_rest_elements");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        MdObjectAssociation elementAssoc = null;
        Map<Integer, MdObjectAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object taxonomyElement : dbResultList) {
            LinkedCaseInsensitiveMap mdObjectInfo = (LinkedCaseInsensitiveMap) taxonomyElement;

            // 1.1: create the TaxonomyElementAssociation instance if control break triggered
            Integer currentObjectId = (Integer) mdObjectInfo.get("object_id");
            elementAssoc = new MdObjectAssociation();
            elementAssoc.setObjectId(currentObjectId);
            elementAssoc.setClassId((Integer) mdObjectInfo.get("class_id"));
            elementAssoc.setObjectName(mdObjectInfo.get("element_name").toString());
            elementAssoc.setObjectShortDesc(mdObjectInfo.get("element_shortdesc").toString());
            elementAssoc.setParent((Boolean) mdObjectInfo.get("is_parent"));
            elementAssoc.setSequence(mdObjectInfo.get("element_hierarchy_number").toString());

            // 1.2: add the taxonomy element metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            elementAssoc.setMdContext(mdContext.generateMdContextList(mdObjectInfo.get("md_context").toString()));

            fullView.addMdObjectAssociation(elementAssoc);
            // add this ptDefinition to the lookup map
            parentLookup.put(elementAssoc.getObjectId(), elementAssoc);
        }

        // 2: get all associated objects and add them to elements using elementId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_api_rest_associations");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // create an AssociatedObject for each row in the result set and add to the parent GHMF Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));
            MdObjectAssociation parent = parentLookup.get((Integer) assocInfo.get("object_id"));
            parent.addAssociatedObject(associatedObject);

        }

        results.put("result_data", fullView.getMdObjectAssociations());
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Retrieves all elements in a json format definition and all metadata objects associated to each json element
     * URL Path: /association/fullview/format/json/{object}
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/fullview/format/json/{objectId}")
    @ResponseBody
    public Map<String, Object> readJsonAssociationFullView(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        MdObjectAssociationFullView fullView = new MdObjectAssociationFullView(); // this list will be returned in the results Map

        // 1: get the list of elements for the json format definition
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_json_nodes");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_fmt_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        MdObjectAssociation elementAssoc = null;
        Map<Integer, MdObjectAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap mdObjectInfo = (LinkedCaseInsensitiveMap) listElement;

            // 1.1: create the TaxonomyElementAssociation instance if control break triggered
            Integer currentObjectId = (Integer) mdObjectInfo.get("json_node_id");
            elementAssoc = new MdObjectAssociation();
            elementAssoc.setObjectId(currentObjectId);
            elementAssoc.setClassId((Integer) mdObjectInfo.get("class_id"));
            elementAssoc.setObjectName(mdObjectInfo.get("json_node_name").toString());
            elementAssoc.setObjectShortDesc(mdObjectInfo.get("json_node_shortdesc").toString());
            elementAssoc.setParent((Boolean) mdObjectInfo.get("json_node_is_parent"));
            elementAssoc.setSequence(mdObjectInfo.get("json_node_hierarchy_number").toString());

            // 1.2: add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            elementAssoc.setMdContext(mdContext.generateMdContextList(mdObjectInfo.get("md_context").toString()));

            fullView.addMdObjectAssociation(elementAssoc);
            // add this ptDefinition to the lookup map
            parentLookup.put(elementAssoc.getObjectId(), elementAssoc);
        }

        // 2: get all associated objects and add them to elements using elementId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_json_associations");
        in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // create an AssociatedObject for each row in the result set and add to the parent GHMF Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );
            associatedObject.setReverseVerbPhrase(assocInfo.get("reverse_verb_phrase").toString());

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));
            MdObjectAssociation parent = parentLookup.get((Integer) assocInfo.get("object_id"));
            if (parent != null) {
                parent.addAssociatedObject(associatedObject);
            }

        }

        results.put("result_data", fullView.getMdObjectAssociations());
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Retrieves all elements in a json format definition and all metadata objects associated to each json element
     * URL Path: /association/fullview/format/json/{object}
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/fullview/format/flatfile/{objectId}")
    @ResponseBody
    public Map<String, Object> readFlatFileAssociationFullView(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        MdObjectAssociationFullView fullView = new MdObjectAssociationFullView(); // this list will be returned in the results Map

        // 1: get the list of elements for the json format definition
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_flatfile_elements");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_fmt_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        MdObjectAssociation elementAssoc = null;
        Map<Integer, MdObjectAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap mdObjectInfo = (LinkedCaseInsensitiveMap) listElement;

            // 1.1: create the TaxonomyElementAssociation instance if control break triggered
            Integer currentObjectId = (Integer) mdObjectInfo.get("object_id");
            elementAssoc = new MdObjectAssociation();
            elementAssoc.setObjectId(currentObjectId);
            elementAssoc.setClassId((Integer) mdObjectInfo.get("class_id"));
            elementAssoc.setObjectName(mdObjectInfo.get("object_name").toString());
            elementAssoc.setObjectShortDesc(mdObjectInfo.get("object_shortdesc").toString());
            elementAssoc.setParent(false);
            elementAssoc.setSequence(mdObjectInfo.get("element_sequence").toString());

            // 1.2: add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            elementAssoc.setMdContext(mdContext.generateMdContextList(mdObjectInfo.get("md_context").toString()));

            fullView.addMdObjectAssociation(elementAssoc);
            // add this ptDefinition to the lookup map
            parentLookup.put(elementAssoc.getObjectId(), elementAssoc);
        }

        // 2: get all associated objects and add them to elements using objectId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_flatfile_associations");
        in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // create an AssociatedObject for each row in the result set and add to the parent GHMF Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );
            associatedObject.setReverseVerbPhrase(assocInfo.get("reverse_verb_phrase").toString());

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));
            MdObjectAssociation parent = parentLookup.get((Integer) assocInfo.get("object_id"));
            if (parent != null) {
                parent.addAssociatedObject(associatedObject);
            }

        }

        results.put("result_data", fullView.getMdObjectAssociations());
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     *
     * @param tableViewId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/smd/tableview/fullview/{tableViewId}")
    @ResponseBody
    public Map<String, Object> readSmdTableViewAssociationFullView(@PathVariable Integer tableViewId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        MdObjectAssociationFullView fullView = new MdObjectAssociationFullView(); // this list will be returned in the results Map

        // 1. get the list of columns for the table/view
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_columns");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_smd_table_view_id", tableViewId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        MdObjectAssociation objectAssoc = null;
        Map<Integer, MdObjectAssociation> parentLookup = new HashMap<>(); // this is used to lookup parent elements when adding associations

        for (Object resultInfo : dbResultList) {
            LinkedCaseInsensitiveMap mdObjectInfo = (LinkedCaseInsensitiveMap) resultInfo;

            // 1.1: create the MdObjectAssociation instance for each object in result set
            objectAssoc = new MdObjectAssociation();
            objectAssoc.setObjectId((Integer) mdObjectInfo.get("smd_column_id"));
            objectAssoc.setClassId((Integer) mdObjectInfo.get("class_id"));
            objectAssoc.setObjectName(mdObjectInfo.get("smd_column_name").toString());
            objectAssoc.setObjectShortDesc(mdObjectInfo.get("smd_column_shortdesc").toString());
            objectAssoc.setSequence(String.format("%04d", mdObjectInfo.get("smd_column_ordinal")));

            // 1.2: add the taxonomy element metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            objectAssoc.setMdContext(mdContext.generateMdContextList(mdObjectInfo.get("md_context").toString()));

            fullView.addMdObjectAssociation(objectAssoc);
            // add this ptDefinition to the lookup map
            parentLookup.put(objectAssoc.getObjectId(), objectAssoc);
        }

        // 2. get all associated objects and add them to elements using elementId/element object map
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_tableview_associations");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // create an AssociatedObject for each row in the result set and add to the parent GHMF Element
        for (Object row : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap assocInfo = (LinkedCaseInsensitiveMap) row;

            AssociatedObject associatedObject = new AssociatedObject(
                    (Integer) assocInfo.get("assoc_id"),
                    (Integer) assocInfo.get("assoc_object_id"),
                    (Integer) assocInfo.get("assoc_class_id"),
                    assocInfo.get("assoc_object_name").toString(),
                    assocInfo.get("assoc_class_name").toString(),
                    assocInfo.get("assoc_shortdesc").toString(),
                    assocInfo.get("assoc_type_name").toString(),
                    assocInfo.get("verb_phrase").toString()
            );

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            associatedObject.setMdContext(mdContext.generateMdContextList(assocInfo.get("assoc_md_context").toString()));

            MdObjectAssociation parent = parentLookup.get((Integer) assocInfo.get("smd_column_id"));
            parent.addAssociatedObject(associatedObject);

        }

        resultData.put("columns", fullView.getMdObjectAssociations());
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Get the lineage associations for the object defined by objectId, classId
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/association/lineage/{objectId}/{classId}")
    @ResponseBody
    public Map<String, Object> readLineageAssociations(@PathVariable Integer objectId, @PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // 1. Get the incoming lineage associations
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_lineage_incoming");
        MapSqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList<LineageDetails> lineage = new ArrayList<>();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {
            LinkedCaseInsensitiveMap details = (LinkedCaseInsensitiveMap) assocDetails;
            // 1.1. create the LineageDetails instance
            LineageDetails lineageObject = new LineageDetails(
                    (Integer) details.get("object_id"),
                    (Integer) details.get("class_id"),
                    (Boolean) details.get("repeated"),
                    (Integer) details.get("lineage_level"),
                    details.get("hierarchy_number").toString(),
                    details.get("lineage_desc").toString()
            );

            // 1.2: add the metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            lineageObject.setMdContext(mdContext.generateMdContextList(details.get("md_context").toString()));

            lineage.add(lineageObject);
        }
        resultData.put("incoming",lineage);

        // 2. get the outgoing lineage associations
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_lineage_outgoing");
        out = jdbcCall.execute(in);
        lineage = new ArrayList<>();
        dbResultList = (ArrayList) out.get("#result-set-1");
        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap details = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1. create the LineageDetails instance
            LineageDetails lineageObject = new LineageDetails(
                    (Integer) details.get("object_id"),
                    (Integer) details.get("class_id"),
                    (Boolean) details.get("repeated"),
                    (Integer) details.get("lineage_level"),
                    details.get("hierarchy_number").toString(),
                    details.get("lineage_desc").toString()
            );

            // 2.2: add the metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            lineageObject.setMdContext(mdContext.generateMdContextList(details.get("md_context").toString()));

            lineage.add(lineageObject);
        }
        resultData.put("outgoing",lineage);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     *
     */
    static class MdAssociationList{
        private ArrayList<MdAssociation> associations;

        public MdAssociationList() {
        }

        public ArrayList<MdAssociation> getAssociations() {
            return associations;
        }

        public void setAssociations(ArrayList<MdAssociation> associations) {
            this.associations = associations;
        }
    }
}
