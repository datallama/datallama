/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.association;

import io.datallama.api.common.MoClassConstants;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Types;
import java.util.Map;

/**
 * A utility class for DL metadata object association creation and management.
 */
public class AssociationUtil {

    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;

    /**
     * Default constructor
     */
    public AssociationUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Creates a new association in the DL DB. Calls stored procedure write_association
     * @param assoc
     * @return
     */
    public Integer writeAssociation(MdAssociation assoc) {
        Integer associationId = 0;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_association");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_assoc_id", assoc.getAssocId(), Types.INTEGER)
                .addValue("_source_class_id", assoc.getSourceClassId(), Types.INTEGER)
                .addValue("_source_object_id", assoc.getSourceObjectId(), Types.INTEGER)
                .addValue("_target_class_id", assoc.getTargetClassId(), Types.INTEGER)
                .addValue("_target_object_id", assoc.getTargetObjectId(), Types.INTEGER)
                .addValue("_assoc_shortdesc", assoc.getAssocShortDesc(), Types.VARCHAR)
                .addValue("_assoc_type_id", assoc.getAssocTypeId(), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        associationId = (Integer) out.get("_return_id");

        return associationId;
    }

}
