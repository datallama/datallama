/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.association;

/**
 * MdAssociationType is a utilty class used to model association type definitions to send back to the UI
 */
public class MdAssociationType {
    private Integer assocTypeId;
    private String assocTypeName;
    private String assocTypeShortDesc;
    private String targetPhrase;
    private String sourcePhrase;

    public MdAssociationType() {
        this.assocTypeId = 0;
        this.assocTypeName = "";
        this.assocTypeShortDesc = "";
        this.targetPhrase = "";
        this.sourcePhrase = "";
    }

    public MdAssociationType(Integer assocTypeId, String assocTypeName, String assocTypeShortDesc,
                             String sourcePhrase, String targetPhrase) {
        this.assocTypeId = assocTypeId;
        this.assocTypeName = assocTypeName;
        this.assocTypeShortDesc = assocTypeShortDesc;
        this.targetPhrase = targetPhrase;
        this.sourcePhrase = sourcePhrase;
    }

    public Integer getAssocTypeId() {
        return assocTypeId;
    }

    public void setAssocTypeId(Integer assocTypeId) {
        this.assocTypeId = assocTypeId;
    }

    public String getAssocTypeName() {
        return assocTypeName;
    }

    public void setAssocTypeName(String assocTypeName) {
        this.assocTypeName = assocTypeName;
    }

    public String getAssocTypeShortDesc() {
        return assocTypeShortDesc;
    }

    public void setAssocTypeShortDesc(String assocTypeShortDesc) {
        this.assocTypeShortDesc = assocTypeShortDesc;
    }

    public String getTargetPhrase() {
        return targetPhrase;
    }

    public void setTargetPhrase(String targetPhrase) {
        this.targetPhrase = targetPhrase;
    }

    public String getSourcePhrase() {
        return sourcePhrase;
    }

    public void setSourcePhrase(String sourcePhrase) {
        this.sourcePhrase = sourcePhrase;
    }
}