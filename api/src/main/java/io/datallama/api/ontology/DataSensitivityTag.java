/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.ontology;

public class DataSensitivityTag {
    private Integer dataSensitivityId;
    private String dataSensitivityLevel;
    private String dataSensitivityName;
    private String dataSensitivityShortDesc;
    private String lastChangeTimestamp;
    private Integer lastChangedByUserId;
    private String lastChangedByUsername;

    /**
     * Default constructor with no parameters
     */
    public DataSensitivityTag() {
        this.dataSensitivityId = 0;
        this.dataSensitivityLevel = "";
        this.dataSensitivityName = "";
        this.dataSensitivityShortDesc = "";
        this.lastChangeTimestamp = "";
        this.lastChangedByUserId = 0;
        this.lastChangedByUsername = "";
    }

    /**
     * Constructor 01: Parameters for core members
     * @param dataSensitivityId
     * @param dataSensitivityLevel
     * @param dataSensitivityName
     * @param dataSensitivityShortDesc
     */
    public DataSensitivityTag(Integer dataSensitivityId, String dataSensitivityLevel, String dataSensitivityName, String dataSensitivityShortDesc) {
        this.dataSensitivityId = dataSensitivityId;
        this.dataSensitivityLevel = dataSensitivityLevel;
        this.dataSensitivityName = dataSensitivityName;
        this.dataSensitivityShortDesc = dataSensitivityShortDesc;
        this.lastChangeTimestamp = "";
        this.lastChangedByUserId = 0;
        this.lastChangedByUsername = "";
    }

    /**
     * Constructor 02: Parameters for all members
     * @param dataSensitivityId
     * @param dataSensitivityLevel
     * @param dataSensitivityName
     * @param dataSensitivityShortDesc
     * @param lastChangeTimestamp
     * @param lastChangedByUsername
     */
    public DataSensitivityTag(Integer dataSensitivityId, String dataSensitivityLevel, String dataSensitivityName,
                              String dataSensitivityShortDesc, String lastChangeTimestamp, Integer lastChangedByUserId,
                              String lastChangedByUsername) {
        this.dataSensitivityId = dataSensitivityId;
        this.dataSensitivityLevel = dataSensitivityLevel;
        this.dataSensitivityName = dataSensitivityName;
        this.dataSensitivityShortDesc = dataSensitivityShortDesc;
        this.lastChangeTimestamp = lastChangeTimestamp;
        this.lastChangedByUserId = lastChangedByUserId;
        this.lastChangedByUsername = lastChangedByUsername;
    }

    public Integer getDataSensitivityId() {
        return dataSensitivityId;
    }

    public void setDataSensitivityId(Integer dataSensitivityId) {
        this.dataSensitivityId = dataSensitivityId;
    }

    public String getDataSensitivityLevel() {
        return dataSensitivityLevel;
    }

    public void setDataSensitivityLevel(String dataSensitivityLevel) {
        this.dataSensitivityLevel = dataSensitivityLevel;
    }

    public String getDataSensitivityName() {
        return dataSensitivityName;
    }

    public void setDataSensitivityName(String dataSensitivityName) {
        this.dataSensitivityName = dataSensitivityName;
    }

    public String getDataSensitivityShortDesc() {
        return dataSensitivityShortDesc;
    }

    public void setDataSensitivityShortDesc(String dataSensitivityShortDesc) {
        this.dataSensitivityShortDesc = dataSensitivityShortDesc;
    }

    public String getLastChangeTimestamp() {
        return lastChangeTimestamp;
    }

    public void setLastChangeTimestamp(String lastChangeTimestamp) {
        this.lastChangeTimestamp = lastChangeTimestamp;
    }

    public Integer getLastChangedByUserId() {
        return lastChangedByUserId;
    }

    public void setLastChangedByUserId(Integer lastChangedByUserId) {
        this.lastChangedByUserId = lastChangedByUserId;
    }

    public String getLastChangedByUsername() {
        return lastChangedByUsername;
    }

    public void setLastChangedByUsername(String lastChangedByUsername) {
        this.lastChangedByUsername = lastChangedByUsername;
    }
}
