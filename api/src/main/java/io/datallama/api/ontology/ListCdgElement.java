/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.ontology;

public class ListCdgElement {
    private Integer listCdgId;
    private Integer listCdgClassId;
    private Integer elementId;
    private Integer classId;
    private String elementName;
    private String elementGUID;
    private String elementShortDesc;
    private Integer elementSelected;
    private String customFormData;

    public ListCdgElement() {
        this.listCdgId = 0;
        this.listCdgClassId = 0;
        this.elementId = 0;
        this.classId = 0;
        this.elementName = "";
        this.elementGUID = "";
        this.elementShortDesc = "";
        this.elementSelected = 0;
        this.customFormData = "";
    }

    public ListCdgElement(Integer listCdgId, Integer listCdgClassId, Integer elementId, Integer classId, String elementName, String elementShortDesc) {
        this.listCdgId = listCdgId;
        this.listCdgClassId = listCdgClassId;
        this.elementId = elementId;
        this.classId = classId;
        this.elementName = elementName;
        this.elementGUID = "";
        this.elementShortDesc = elementShortDesc;
        this.elementSelected = 0;
        this.customFormData = "";
    }

    public Integer getListCdgClassId() {
        return listCdgClassId;
    }

    public void setListCdgClassId(Integer listCdgClassId) {
        this.listCdgClassId = listCdgClassId;
    }

    public Integer getListCdgId() {
        return listCdgId;
    }

    public void setListCdgId(Integer listCdgId) {
        this.listCdgId = listCdgId;
    }

    public Integer getElementId() {
        return elementId;
    }

    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementGUID() {
        return elementGUID;
    }

    public void setElementGUID(String elementGUID) {
        this.elementGUID = elementGUID;
    }

    public String getElementShortDesc() {
        return elementShortDesc;
    }

    public void setElementShortDesc(String elementShortDesc) {
        this.elementShortDesc = elementShortDesc;
    }

    public Integer getElementSelected() {
        return elementSelected;
    }

    public void setElementSelected(Integer elementSelected) {
        this.elementSelected = elementSelected;
    }

    public String getCustomFormData() {
        return customFormData;
    }

    public void setCustomFormData(String customFormData) {
        this.customFormData = customFormData;
    }
}
