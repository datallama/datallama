/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.ontology;

public class ListCdg {
    private Integer listCdgId;
    private Integer classId;
    private String listCdgName;
    private String className;
    private String listCdgShortDesc;

    /**
     * default constructor used with POST calls
     */
    public ListCdg() {
        this.listCdgId = 0;
        this.classId = 0;
        this.listCdgName = "";
        this.className = "";
        this.listCdgShortDesc = "";
    }

    /**
     * Full constructor used when processing database result sets
     * @param listCdgId
     * @param classId
     * @param listCdgName
     * @param className
     * @param listCdgShortDesc
     */
    public ListCdg(Integer listCdgId, Integer classId, String listCdgName, String className, String listCdgShortDesc) {
        this.listCdgId = listCdgId;
        this.classId = classId;
        this.listCdgName = listCdgName;
        this.className = className;
        this.listCdgShortDesc = listCdgShortDesc;
    }

    public Integer getListCdgId() {
        return listCdgId;
    }

    public void setListCdgId(Integer listCdgId) {
        this.listCdgId = listCdgId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getListCdgName() {
        return listCdgName;
    }

    public void setListCdgName(String listCdgName) {
        this.listCdgName = listCdgName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getListCdgShortDesc() {
        return listCdgShortDesc;
    }

    public void setListCdgShortDesc(String listCdgShortDesc) {
        this.listCdgShortDesc = listCdgShortDesc;
    }
}
