/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.ontology;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.datamodels.ConnectorContent;
import io.datallama.api.datamodels.DiagramContent;
import io.datallama.api.dsdf.DsdfDiagramController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class OntologyDiagramController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /*
     ** URL Path: /ontology/diagram/list
     */
    @RequestMapping(method = GET, value = "/ontology/diagram/list")
    public Map<String, Object> readDiagramList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        //Todo: This shouldn't be hard coded.
        int ontologyDiagramClassId = 42;
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dm_diagrams");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", ontologyDiagramClassId);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        results.put("result_data", rslt);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/ontology/diagram/content/{diagramId}")
    @ResponseBody
    public Map<String, Object> readOntologyDiagram(@PathVariable Long diagramId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        resultData.put("id",diagramId);
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dm_diagram_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("name", rsltMap.get("dgrm_name"));
        resultData.put("properties", rsltMap.get("dgrm_prop"));

        // first: get shape list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_diagram_shapes");
        in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList shapeList = (ArrayList) out.get("#result-set-1");
        resultData.put("entities", shapeList);

        // second: get arrow list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_diagram_arrows");
        out = jdbcCall.execute(in);
        ArrayList arrowList = (ArrayList) out.get("#result-set-1");
        resultData.put("connectors", arrowList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/ontology/diagram/content")
    @ResponseBody
    public Map<String, Object> writeOntologyDiagram(@RequestBody DiagramContent diagramContent, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        ObjectMapper objectMapper = new ObjectMapper(); // used to parse Maps to JSON strings

        //zeroth, save the diagram properties
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("update_diagram_properties");;
        SqlParameterSource in =  new MapSqlParameterSource()
                .addValue("_user_id",Integer.parseInt(userId), Types.INTEGER)
                .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                .addValue("_dgrm_prop", diagramContent.getDiagramPropertiesAsString(), Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        // first: save all diagram entities
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_entity");

        for (Object listElement : diagramContent.getEntities()) {
            LinkedHashMap entityInfo = (LinkedHashMap) listElement;

            Object cd = entityInfo.get("colorDefn");
            String colorDefinitionJSON = "";

            try {
                colorDefinitionJSON = objectMapper.writeValueAsString(cd);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            in = new MapSqlParameterSource()
                    .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                    .addValue("_class_id", entityInfo.get("classId"), Types.INTEGER)
                    .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_ntt_id", entityInfo.get("nttId"), Types.INTEGER)
                    .addValue("_ntt_x", entityInfo.get("x"), Types.INTEGER)
                    .addValue("_ntt_y", entityInfo.get("y"), Types.INTEGER)
                    .addValue("_ntt_z", entityInfo.get("z"), Types.INTEGER)
                    .addValue("_ntt_height", entityInfo.get("height"), Types.INTEGER)
                    .addValue("_ntt_width", entityInfo.get("width"), Types.INTEGER)
                    .addValue("_color_defn", colorDefinitionJSON, Types.VARCHAR)
                    .addValue("_color_source",entityInfo.get("colorSource"), Types.INTEGER);

            jdbcCall.execute(in);
        }

        // second: save all diagram connectors
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_connector");

        for (Object listElement : diagramContent.getConnectors()) {
            LinkedHashMap connectorInfo = (LinkedHashMap) listElement;

            String verticesJSON = "";
            try {
                verticesJSON = objectMapper.writeValueAsString(connectorInfo.get("cnctVertices"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            in = new MapSqlParameterSource()
                    .addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_class_id", connectorInfo.get("classId"), Types.INTEGER)
                    .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_cnct_id", connectorInfo.get("cnctId"), Types.INTEGER)
                    .addValue("_cnct_start_port", connectorInfo.get("cnctStartPort"), Types.VARCHAR)
                    .addValue("_cnct_end_port", connectorInfo.get("cnctEndPort"), Types.VARCHAR)
                    .addValue("_cnct_vertices", verticesJSON, Types.VARCHAR)
                    .addValue("_cnct_z", connectorInfo.get("z"), Types.INTEGER);

            jdbcCall.execute(in);
        }

        //Third delete entities
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_diagram_entity");
        for (Object delEntityId : diagramContent.getEntityDeletes()) {
            in = new MapSqlParameterSource()
                    .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_ntt_id", delEntityId, Types.INTEGER);
            jdbcCall.execute(in);
        }

        //Fourth delete connectors
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_diagram_connector");
        for (Object delConnectorId : diagramContent.getConnectorDeletes()) {
            in = new MapSqlParameterSource()
                    .addValue("_dgrm_id", diagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_cnct_id", delConnectorId, Types.INTEGER);
            jdbcCall.execute(in);
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }
}