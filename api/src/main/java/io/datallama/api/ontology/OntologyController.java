/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.ontology;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.MoComment;
import io.datallama.api.common.OverviewPage;
import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.metamodel.DlMetaDataContextNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class OntologyController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /ontology/overview/{stateName}
     */
    @RequestMapping(method = GET, value = "/ontology/overview/{stateName}")
    @ResponseBody
    public Map<String, Object> readOntologyOverview(@PathVariable String stateName) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_overview_page");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_state_name", stateName, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList resultList = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap pageContent = (LinkedCaseInsensitiveMap) resultList.get(0);
        OverviewPage overviewPage = new OverviewPage(
                (Integer) pageContent.get("overview_id"),
                pageContent.get("state_name").toString(),
                pageContent.get("page_content").toString()
        );

        results.put("result_data", overviewPage);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: saves the ontology overview page content in the DL database
     * TODO: path should be changed to /ontology/summary
     */
    @RequestMapping(method = POST, value = "/ontology/overview")
    public Map<String, Object> writeOntologyOverview(@RequestBody OverviewPage overviewPage) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_overview_page");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_state_name", overviewPage.getStateName(), Types.VARCHAR)
                .addValue("_page_content", overviewPage.getPageContent(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        // TODO: stored proc should return a status.

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /ontology/listcdg/list
     */
    @RequestMapping(method = GET, value = "/ontology/listcdg/list")
    @ResponseBody
    public Map<String, Object> readListCdgList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList listCdgList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_list_cdg_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResults = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResults) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap listCdgInfo = (LinkedCaseInsensitiveMap) listElement;
            ListCdg listCdg = new ListCdg(
                    (Integer) listCdgInfo.get("list_cdg_id"),
                    (Integer) listCdgInfo.get("class_id"),
                    listCdgInfo.get("list_cdg_name").toString(),
                    listCdgInfo.get("class_name").toString(),
                    listCdgInfo.get("list_cdg_shortdesc").toString()
            );
            listCdgList.add(listCdg);
        }

        results.put("result_data", listCdgList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /ontology/listcdg
     */
    @RequestMapping(method = POST, value = "/ontology/listcdg")
    public Map<String, Object> writeListCdg(@RequestBody ListCdg listCdg, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_list_cdg");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id",           Integer.parseInt(userId), Types.INTEGER)
                .addValue("_list_cdg_id",       listCdg.getListCdgId(), Types.INTEGER)
                .addValue("_class_id",          listCdg.getClassId(), Types.INTEGER)
                .addValue("_list_cdg_name",     listCdg.getListCdgName(), Types.VARCHAR)
                .addValue("_list_cdg_shortdesc",listCdg.getListCdgShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap newID = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("returnID", newID.get("returnID"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * URL Path: /ontology/listcdg/element
     */
    @RequestMapping(method = POST, value = "/ontology/listcdg/element")
    public Map<String, Object> writeListCdgElement(@RequestBody ListCdgElement listCdgElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_list_cdg_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id",           Integer.parseInt(userId), Types.INTEGER)
                .addValue("_list_cdg_id",       listCdgElement.getListCdgId(), Types.INTEGER)
                .addValue("_element_id",        listCdgElement.getElementId(), Types.INTEGER)
                .addValue("_class_id",          listCdgElement.getClassId(), Types.INTEGER)
                .addValue("_element_name",      listCdgElement.getElementName(), Types.VARCHAR)
                .addValue("_element_shortdesc", listCdgElement.getElementShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap newID = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("returnID", newID.get("returnID"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /ontology/listcdg/elements/{listCdgId}
     */
    @RequestMapping(method = GET, value = "/ontology/listcdg/elements/{listCdgId}")
    @ResponseBody
    public Map<String, Object> readListCdgElements(@PathVariable Integer listCdgId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList listCdgElements = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_list_cdg_elements");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_list_cdg_id", listCdgId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResults = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResults) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap listCdgElementInfo = (LinkedCaseInsensitiveMap) listElement;
            ListCdgElement listCdgElement = new ListCdgElement(
                    (Integer) listCdgElementInfo.get("list_cdg_id"),
                    (Integer) listCdgElementInfo.get("list_cdg_class_id"),
                    (Integer) listCdgElementInfo.get("element_id"),
                    (Integer) listCdgElementInfo.get("class_id"),
                    listCdgElementInfo.get("element_name").toString(),
                    listCdgElementInfo.get("element_shortdesc").toString()
            );
            listCdgElements.add(listCdgElement);
        }

        results.put("result_data", listCdgElements);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /ontology/listcdg/content/{listCdgId}
     */
    @RequestMapping(method = GET, value = "/ontology/listcdg/content/{listCdgId}")
    @ResponseBody
    public Map<String, Object> readListCdgContent(@PathVariable Integer listCdgId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_list_cdg");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_list_cdg_id", listCdgId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        ListCdg listCdg = new ListCdg(
                listCdgId,
                (Integer) out.get("_class_id"),
                out.get("_list_cdg_name").toString(),
                out.get("_class_name").toString(),
                out.get("_list_cdg_shortdesc").toString()
        );

        resultData.put("list_cdg_summary", listCdg);

        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);

        resultData.put("custom_form", customForm);

        ArrayList listCdgElements = new ArrayList(); // this list will be returned in the results Map

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_list_cdg_elements");
        in = new MapSqlParameterSource().addValue("_list_cdg_id", listCdgId, Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResults = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResults) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap listCdgElementInfo = (LinkedCaseInsensitiveMap) listElement;
            ListCdgElement listCdgElement = new ListCdgElement(
                    (Integer) listCdgElementInfo.get("list_cdg_id"),
                    (Integer) listCdgElementInfo.get("list_cdg_class_id"),
                    (Integer) listCdgElementInfo.get("element_id"),
                    (Integer) listCdgElementInfo.get("class_id"),
                    listCdgElementInfo.get("element_name").toString(),
                    listCdgElementInfo.get("element_shortdesc").toString()
            );
            listCdgElements.add(listCdgElement);
        }

        resultData.put("list_cdg_elements",listCdgElements);

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", listCdg.getClassId(), Types.INTEGER)
                .addValue("_object_id", listCdg.getListCdgId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);
        }

        resultData.put("associations",associationList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Deletes a full list from the DL database (and archives it).
     * URL Path: /ontology/listcdg/
     */
    @RequestMapping(method = DELETE, value = "/ontology/listcdg/{listCdgId}")
    public Map<String, Object> deleteList(@PathVariable Integer listCdgId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_list_cdg");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_list_cdg_id", listCdgId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Updates the custom form id for a list.
     * URL Path: /ontology/listcdg/customform/
     */
    @RequestMapping(method = POST, value = "/ontology/listcdg/customform/{listCdgId}")
    public Map<String, Object> updateCustomForm(@RequestBody Integer customFormId, @RequestHeader("dlUserId") String userId, @PathVariable Integer listCdgId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_list_cdg_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_list_cdg_id", listCdgId, Types.INTEGER)
                .addValue("_custom_form_id", customFormId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Writes the custom form data for a list cdg element.
     * URL Path: /ontology/listcdg/element/
     */
    @RequestMapping(method = POST, value = "/ontology/listcdg/element/customformdata")
    public Map<String, Object> writeListCdgElementCustomFormData(@RequestBody OntologyController.CustomFormData customFormData, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_list_cdg_element_custom_form_data");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_element_id", customFormData.getElementId(), Types.INTEGER)
                .addValue("_custom_form_data", customFormData.getCustomFormData(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Deletes a list cdg element from the DL database.
     * URL Path: /ontology/listcdg/element/
     */
    @RequestMapping(method = DELETE, value = "/ontology/listcdg/element/{elementId}")
    public Map<String, Object> deleteListCdgElement(@PathVariable Integer elementId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_list_cdg_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_element_id", elementId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Retrieves from the DL Database details of the List element identified by {elementId}
     * URL Path: /ontology/listcdg/elements/{listCdgId}
     * @param elementId
     * @return
     */
    @RequestMapping(method = GET, value = "/ontology/listcdg/element/{elementId}")
    @ResponseBody
    public Map<String, Object> readListCdgElement(@PathVariable Integer elementId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // list list is returned in the results Map

        // first: get the element core details
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_list_cdg_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_element_id", elementId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        ListCdgElement element = new ListCdgElement(
                (Integer) out.get("_list_cdg_id"),
                (Integer) out.get("_list_cdg_class_id"),
                elementId,
                (Integer) out.get("_class_id"),
                out.get("_element_name").toString(),
                out.get("_element_shortdesc").toString()
        );
        element.setCustomFormData(out.get("_custom_form_data").toString());
        element.setElementGUID(out.get("_element_guid").toString());

        resultData.put("attributes", element);

        // 2: get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", element.getClassId(), Types.INTEGER)
                .addValue("_object_id", element.getElementId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);
        }

        // third, get the comments for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", element.getElementId(), Types.INTEGER)
                .addValue("_class_id", element.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }

        resultData.put("associations", associationList);
        resultData.put("comments", commentList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Retrieves from the DL Database the full list of Data Sensitivity Tags
     * TODO: move this method to /common/DataSensitivityController
     * URL Path: /ontology/listcdg/elements/{listCdgId}
     * @return
     */
    @RequestMapping(method = GET, value = "/ontology/datasensitivity/list")
    @ResponseBody
    public Map<String, Object> readDataSensitivityTagList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList dataSensitivityTagList = new ArrayList();  // this list is returned in the results Map

        // first: get the element core details
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_tags");
        Map<String, Object> out = jdbcCall.execute();

        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object dsTagDetailsDb : dbResultList) {
            LinkedCaseInsensitiveMap dsTagDetails = (LinkedCaseInsensitiveMap) dsTagDetailsDb;
            DataSensitivityTag dsTag = new DataSensitivityTag (
                    (Integer) dsTagDetails.get("data_sensitivity_id"),
                    dsTagDetails.get("data_sensitivity_level").toString(),
                    dsTagDetails.get("data_sensitivity_name").toString(),
                    dsTagDetails.get("data_sensitivity_shortdesc").toString(),
                    dsTagDetails.get("last_change_ts").toString(),
                    (Integer) dsTagDetails.get("last_change_user_id"),
                    dsTagDetails.get("last_change_user_login").toString()
            );
            dataSensitivityTagList.add(dsTag);
        }
        resultData.put("data_sensitivity_tag_list", dataSensitivityTagList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


        /**
         * Utility class for receiving custom form data from UI
         */
    static class CustomFormData {
        private Integer elementId;
        private String customFormData;

        public CustomFormData() {
            this.elementId = 0;
            this.customFormData = "";
        }

        public CustomFormData(Integer elementId, String customFormData) {
            this.elementId = elementId;
            this.customFormData = customFormData;
        }

        public Integer getElementId() {
            return elementId;
        }

        public void setElementId(Integer elementId) {
            this.elementId = elementId;
        }

        public String getCustomFormData() {
            return customFormData;
        }

        public void setCustomFormData(String customFormData) {
            this.customFormData = customFormData;
        }
    }

}

