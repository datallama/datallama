/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class    DlSqlError {
    private Integer sqlErrorCode;
    private String sqlErrorState;
    private String sqlErrorMessage;

    /**
     * Default constructor with no paramaters.
     */
    public DlSqlError() {
        this.sqlErrorCode = 0;
        this.sqlErrorState = "";
        this.sqlErrorMessage = "";
    }

    public Integer getSqlErrorCode() {
        return sqlErrorCode;
    }

    public void setSqlErrorCode(Integer sqlErrorCode) {
        this.sqlErrorCode = sqlErrorCode;
    }

    public String getSqlErrorState() {
        return sqlErrorState;
    }

    public void setSqlErrorState(String sqlErrorState) {
        this.sqlErrorState = sqlErrorState;
    }

    public String getSqlErrorMessage() {
        return sqlErrorMessage;
    }

    public void setSqlErrorMessage(String sqlErrorMessage) {
        this.sqlErrorMessage = sqlErrorMessage;
    }
}
