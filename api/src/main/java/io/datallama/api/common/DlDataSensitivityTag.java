/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * A data sensitivity tag which is applied to metadata objects in the Implementation Feature Set.
 */
package io.datallama.api.common;

public class DlDataSensitivityTag {
    private Integer dlDsTagId;
    private Integer createMode;
    private String dlDsLevel;
    private String dlDsName;
    private String dlDsShortDesc;

    /**
     * Default constructor. No parameters
     */
    public DlDataSensitivityTag() {
        this.dlDsTagId = 0;
        this.createMode = 0;
        this.dlDsLevel = "None";
        this.dlDsName = "None";
        this.dlDsShortDesc = "None";
    }

    /**
     * Constructor with all essential parameters
     * @param dlDsTagId
     * @param dlDsLevel
     * @param dlDsName
     */
    public DlDataSensitivityTag(Integer dlDsTagId, String dlDsLevel, String dlDsName) {
        this.dlDsTagId = dlDsTagId;
        this.createMode = 0;
        this.dlDsLevel = dlDsLevel;
        this.dlDsName = dlDsName;
        this.dlDsShortDesc = "None";
    }

    public Integer getDlDsTagId() {
        return dlDsTagId;
    }

    public void setDlDsTagId(Integer dlDsTagId) {
        this.dlDsTagId = dlDsTagId;
    }

    public Integer getCreateMode() {
        return createMode;
    }

    public void setCreateMode(Integer createMode) {
        this.createMode = createMode;
    }

    public String getDlDsLevel() {
        return dlDsLevel;
    }

    public void setDlDsLevel(String dlDsLevel) {
        this.dlDsLevel = dlDsLevel;
    }

    public String getDlDsName() {
        return dlDsName;
    }

    public void setDlDsName(String dlDsName) {
        this.dlDsName = dlDsName;
    }

    public String getDlDsShortDesc() {
        return dlDsShortDesc;
    }

    public void setDlDsShortDesc(String dlDsShortDesc) {
        this.dlDsShortDesc = dlDsShortDesc;
    }
}
