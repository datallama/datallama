/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

/**
 * Models a reverse engineering connector defined in the DL DB.
 */
public class RevEngConnector {
    private Integer connectorId;
    private Integer classId;
    private String connectorName;
    private String connectorVersion;
    private String connectorProvider;
    private String connectorShortDesc;
    private Integer credentialTypeId;
    private Boolean connectorEnabled;
    private String jarName;
    private String javaClassName;
    private CredentialType credentialType;

    /**
     * Default constructor with no paramaters.
     */
    public RevEngConnector() {
        this.connectorId = 0;
        this.classId = 0;
        this.connectorName = "";
        this.connectorVersion = "";
        this.connectorProvider = "";
        this.connectorShortDesc = "";
        this.credentialTypeId = 0;
        this.connectorEnabled = false;
        this.jarName = "";
        this.javaClassName = "";
        this.credentialType = new CredentialType();
    }

    /**
     * Constructor with all parameters except the CredentialType object
     * @param connectorId
     * @param classId
     * @param connectorName
     * @param connectorVersion
     * @param connectorProvider
     * @param connectorShortDesc
     * @param credentialTypeId
     * @param connectorEnabled
     * @param jarName
     * @param javaClassName
     */
    public RevEngConnector(Integer connectorId, Integer classId, String connectorName, String connectorVersion,
                           String connectorProvider, String connectorShortDesc, Integer credentialTypeId,
                           Boolean connectorEnabled, String jarName, String javaClassName) {
        this.connectorId = connectorId;
        this.classId = classId;
        this.connectorName = connectorName;
        this.connectorVersion = connectorVersion;
        this.connectorProvider = connectorProvider;
        this.connectorShortDesc = connectorShortDesc;
        this.credentialTypeId = credentialTypeId;
        this.connectorEnabled = connectorEnabled;
        this.jarName = jarName;
        this.javaClassName = javaClassName;
        this.credentialType = new CredentialType();
    }

    public Integer getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Integer connectorId) {
        this.connectorId = connectorId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorVersion() {
        return connectorVersion;
    }

    public void setConnectorVersion(String connectorVersion) {
        this.connectorVersion = connectorVersion;
    }

    public String getConnectorProvider() {
        return connectorProvider;
    }

    public void setConnectorProvider(String connectorProvider) {
        this.connectorProvider = connectorProvider;
    }

    public String getConnectorShortDesc() {
        return connectorShortDesc;
    }

    public void setConnectorShortDesc(String connectorShortDesc) {
        this.connectorShortDesc = connectorShortDesc;
    }

    public Integer getCredentialTypeId() {
        return credentialTypeId;
    }

    public void setCredentialTypeId(Integer credentialTypeId) {
        this.credentialTypeId = credentialTypeId;
    }

    public Boolean getConnectorEnabled() {
        return connectorEnabled;
    }

    public void setConnectorEnabled(Boolean connectorEnabled) {
        this.connectorEnabled = connectorEnabled;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public CredentialType getCredentialType() {
        return credentialType;
    }

    public void setCredentialType(CredentialType credentialType) {
        this.credentialType = credentialType;
    }
}
