/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.common;

/**
 * Data Llama Metadata Object Class Identifier Constants
 */
public class MoClassConstants {
    public static final Integer INTERNAL_DATA_STORE = 1;
    public static final Integer DATA_FLOW = 2;
    public static final Integer DSDF_DIAGRAM = 3;
    public static final Integer PDM_DIAGRAM = 4;
    public static final Integer RDBMS_TABLE = 5;
    public static final Integer RDBMS_VIEW = 6;
    public static final Integer RDBMS_FOREIGN_KEY = 7;
    public static final Integer DB_TABLE_COLUMN = 8;
    public static final Integer TAXONOMY_DEFINITION = 9;
    public static final Integer TAXONOMY_ELEMENT = 10;
    public static final Integer JSON_MESSAGE_FORMAT = 11;
    public static final Integer JSON_NODE = 12;
    public static final Integer RELATIONAL_DATABASE = 13;
    public static final Integer LIST_CDG = 14;
    public static final Integer LIST_CDG_ELEMENT = 15;
    public static final Integer GHMF = 16;
    public static final Integer GHMF_ELEMENT = 17;
    public static final Integer ERROR = 18;
    public static final Integer DB_SCHEMA = 19;
    public static final Integer CUSTOM_FORM = 20;
    public static final Integer EXTERNAL_DATA_STORE = 21;
    public static final Integer INBOUND_DATA_FEED = 22;
    public static final Integer OUTBOUND_DATA_FEED = 23;
    public static final Integer ETL_PROCESSOR = 24;
    public static final Integer STREAM_MSG_PROCESSOR = 25;
    public static final Integer DATA_VISUALISATION_APP = 26;
    public static final Integer DATA_REPORTING_APP = 27;
    public static final Integer ICON = 28;
    public static final Integer LIST_CATALOGUE = 29;
    public static final Integer LIST_GLOSSARY = 30;
    public static final Integer LIST_THESAURUS = 31;
    public static final Integer LIST_DICTIONARY = 32;
    public static final Integer LIST_ATTRIBUTE_DOMAIN = 33;
    public static final Integer FILE_STORE = 34;
    public static final Integer FILE_STORE_DIR = 35;
    public static final Integer XML_MESSAGE_FORMAT = 36;
    public static final Integer XML_ELEMENT = 37;
    public static final Integer XML_ATTRIBUTE = 38;
    public static final Integer REST_API = 39;
    public static final Integer REST_API_PATH_SEGMENT = 40;
    public static final Integer REST_API_METHOD = 41;
    public static final Integer ONTOLOGY_DIAGRAM = 42;
    public static final Integer DIAGRAM_SHAPE = 43;
    public static final Integer DIAGRAM_ARROW = 44;
    public static final Integer REST_API_RESPONSE = 45;
    public static final Integer OBJECT_ASSOCIATION = 46;
    public static final Integer INTERNAL_API_LO = 47;
    public static final Integer EXTERNAL_API_LO = 48;
    public static final Integer PRSNT_RPT = 49;
    public static final Integer PRSNT_RPT_FIELD = 50;
    public static final Integer PRSNT_FORM = 51;
    public static final Integer PRSNT_FORM_FIELD = 52;
    public static final Integer PRSNT_VIS = 53;
    public static final Integer PRSNT_VIS_FEATURE = 54;
    public static final Integer LANDSCAPE_OBJECT = 55;
    public static final Integer LANDSCAPE_DATA_FLOW = 56;
    public static final Integer BUSINESS_ONTOLOGY = 57;
    public static final Integer IMPLEMENTATION = 58;
    public static final Integer DIAGRAM = 59;
    public static final Integer DL_INTERNAL = 60;
    public static final Integer DATA_STORE = 61;
    public static final Integer DATA_PROCESSOR = 62;
    public static final Integer DATA_FEED = 63;
    public static final Integer API_LO = 64;
    public static final Integer DATA_APP = 65;
    public static final Integer DATA_MOVER = 66;
    public static final Integer PERSISTENT_STORE = 67;
    public static final Integer STREAMING_MESSAGING = 68;
    public static final Integer DATA_INTEGRATION = 69;
    public static final Integer DATA_PRESENTATION = 70;
    public static final Integer FILE_MSG_FORMAT = 71;
    public static final Integer API_DEFINITION = 72;
    public static final Integer DB_VIEW_COLUMN = 73;
    public static final Integer LIST_DEFINITION = 74;
    public static final Integer OBJECT_STORE_SPACE = 75;
    public static final Integer OBJECT_STORE_CONTAINER = 76;
    public static final Integer OBJECT_STORE_DIRECTORY = 77;
    public static final Integer DATA_INTEGRATION_PROJECT = 78;
    public static final Integer DATA_INTEGRATION_PACKAGE = 79;
    public static final Integer DATA_INTEGRATION_JOB = 80;
    public static final Integer DELIMITED_FLATFILE = 81;
    public static final Integer DELIMITED_FLATFILE_ELEMENT = 82;
    public static final Integer FIXED_WIDTH_FLATFILE = 83;
    public static final Integer FIXED_WIDTH_FLATFILE_ELEMENT = 84;

}
