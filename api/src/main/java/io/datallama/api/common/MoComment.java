/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;


public class MoComment {
    private Integer userId;
    private Integer commentId;
    private Integer objectId;
    private Integer classId;
    private String commentDateTime;
    private String commentText;
    private String username;

    /**
     * default constructor used with POST calls
     */
    public MoComment() {
        this.userId = 0;
        this.commentId = 0;
        this.objectId = 0;
        this.classId = 0;
        this.commentDateTime = "";
        this.commentText = "";
        this.username = "";
    }

    /**
     * Full constructor used when processing database result sets
     * @param userId
     * @param commentId
     * @param objectId
     * @param classId
     * @param commentDateTime
     * @param commentText
     */
    public MoComment(Integer userId, Integer commentId, Integer objectId, Integer classId, String commentDateTime, String commentText) {
        this.userId = userId;
        this.commentId = commentId;
        this.objectId = objectId;
        this.classId = classId;
        this.commentDateTime = commentDateTime;
        this.commentText = commentText;
        this.username = "";
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public String getCommentDateTime() {
        return commentDateTime;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public void setCommentDateTime(String commentDateTime) {
        this.commentDateTime = commentDateTime;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}