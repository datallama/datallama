/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class RevEngConCredConfig {
    private Integer objectId;
    private Integer classId;
    private Integer connectorId;
    private String connectorName;
    private String connectorShortDesc;
    private Integer credentialId;
    private String credentialName;
    private String credentialShortDesc;

    /**
     * Default constructor with no parameters.
     */
    public RevEngConCredConfig() {
        this.objectId = 0;
        this.classId = 0;
        this.connectorId = 0;
        this.connectorName = "";
        this.connectorShortDesc = "";
        this.credentialId = 0;
        this.credentialName = "";
        this.credentialShortDesc = "";
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Integer connectorId) {
        this.connectorId = connectorId;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorShortDesc() {
        return connectorShortDesc;
    }

    public void setConnectorShortDesc(String connectorShortDesc) {
        this.connectorShortDesc = connectorShortDesc;
    }

    public Integer getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Integer credentialId) {
        this.credentialId = credentialId;
    }

    public String getCredentialName() {
        return credentialName;
    }

    public void setCredentialName(String credentialName) {
        this.credentialName = credentialName;
    }

    public String getCredentialShortDesc() {
        return credentialShortDesc;
    }

    public void setCredentialShortDesc(String credentialShortDesc) {
        this.credentialShortDesc = credentialShortDesc;
    }
}
