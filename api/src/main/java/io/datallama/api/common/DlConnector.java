/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class DlConnector {
    private Integer connectorId;
    private Integer classId;
    private String connectorName;
    private String connectorShortDesc;

    private Integer startEntityId;
    private Integer startEntityClassId;
    private String startEntityName;

    private Integer endEntityId;
    private Integer endEntityClassId;
    private String endEntityName;

    public DlConnector(Integer connectorId, Integer classId, String connectorName, String connectorShortDesc,
                       Integer startEntityId, Integer startEntityClassId, String startEntityName,
                       Integer endEntityId, Integer endEntityClassId, String endEntityName) {
        this.connectorId = connectorId;
        this.classId = classId;
        this.connectorName = connectorName;
        this.connectorShortDesc = connectorShortDesc;
        this.startEntityId = startEntityId;
        this.startEntityClassId = startEntityClassId;
        this.startEntityName = startEntityName;
        this.endEntityId = endEntityId;
        this.endEntityClassId = endEntityClassId;
        this.endEntityName = endEntityName;
    }

    public Integer getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Integer connectorId) {
        this.connectorId = connectorId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorShortDesc() {
        return connectorShortDesc;
    }

    public void setConnectorShortDesc(String connectorShortDesc) {
        this.connectorShortDesc = connectorShortDesc;
    }

    public Integer getStartEntityId() {
        return startEntityId;
    }

    public void setStartEntityId(Integer startEntityId) {
        this.startEntityId = startEntityId;
    }

    public Integer getStartEntityClassId() {
        return startEntityClassId;
    }

    public void setStartEntityClassId(Integer startEntityClassId) {
        this.startEntityClassId = startEntityClassId;
    }

    public String getStartEntityName() {
        return startEntityName;
    }

    public void setStartEntityName(String startEntityName) {
        this.startEntityName = startEntityName;
    }

    public Integer getEndEntityId() {
        return endEntityId;
    }

    public void setEndEntityId(Integer endEntityId) {
        this.endEntityId = endEntityId;
    }

    public Integer getEndEntityClassId() {
        return endEntityClassId;
    }

    public void setEndEntityClassId(Integer endEntityClassId) {
        this.endEntityClassId = endEntityClassId;
    }

    public String getEndEntityName() {
        return endEntityName;
    }

    public void setEndEntityName(String endEntityName) {
        this.endEntityName = endEntityName;
    }
}
