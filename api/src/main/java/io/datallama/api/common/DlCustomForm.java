/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.datallama.api.formats.DlFormat;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.Types;
import java.util.*;

public class DlCustomForm {
    private Integer customFormId;
    private String customFormName;
    private String customFormShortDesc;
    private String schemaDefinition;
    private String formDefinition;
    private String formData;
    private JsonNode schemaDefinitionJson;

    /**
     * The classId for the metadata object with which the custom form or form data is associated
     */
    private Integer classId;
    /**
     * The objectId for the metadata object with which the custom form or form data is associated
     */
    private Integer objectId;
    /**
     * The classId for the metadata object that is the parent of the metadata object with which the custom form or form data is associated
     */
    private Integer parentClassId;
    /**
     * The objectId for the metadata object that is the parent of the metadata object with which the custom form or form data is associated
     */
    private Integer parentObjectId;

    public DlCustomForm() {
        this.customFormId = 0;
        this.customFormName = "";
        this.schemaDefinition = "";
        this.formDefinition = "";
        this.formData = "{}";
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.schemaDefinitionJson = null;
    }

    public DlCustomForm(Integer customFormId, String customFormName, String schemaDefinition, String formDefinition) {
        this.customFormId = customFormId;
        this.customFormName = customFormName;
        this.schemaDefinition = schemaDefinition;
        this.formDefinition = formDefinition;
        this.formData = "{}";
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.schemaDefinitionJson = null;
    }

    public DlCustomForm(Integer customFormId, String customFormName, String schemaDefinition, String formDefinition,
                        String formData) {
        this.customFormId = customFormId;
        this.customFormName = customFormName;
        this.schemaDefinition = schemaDefinition;
        this.formDefinition = formDefinition;
        this.formData = formData;
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public String getCustomFormName() {
        return customFormName;
    }

    public void setCustomFormName(String customFormName) {
        this.customFormName = customFormName;
    }

    public String getCustomFormShortDesc() {
        return customFormShortDesc;
    }

    public void setCustomFormShortDesc(String customFormShortDesc) {
        this.customFormShortDesc = customFormShortDesc;
    }

    public String getSchemaDefinition() {
        return schemaDefinition;
    }

    public void setSchemaDefinition(String schemaDefinition) {
        this.schemaDefinition = schemaDefinition;
    }

    public String getFormDefinition() {
        return formDefinition;
    }

    public void setFormDefinition(String formDefinition) {
        this.formDefinition = formDefinition;
    }

    public String getFormData() {
        return formData;
    }

    public void setFormData(String formData) {
        this.formData = formData;
    }

    /**
     * {@link DlCustomForm#objectId}
     * @return
     */
    public Integer getObjectId() {
        return objectId;
    }

    /**
     * {@link DlCustomForm#objectId}
     * @return
     */
    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    /**
     * {@link DlCustomForm#classId}
     * @return
     */
    public Integer getClassId() {
        return classId;
    }

    /**
     * {@link DlCustomForm#classId}
     * @return
     */
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    /**
     * {@link DlCustomForm#parentObjectId}
     * @return
     */
    public Integer getParentObjectId() {
        return parentObjectId;
    }

    /**
     * {@link DlCustomForm#parentObjectId}
     * @return
     */
    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    /**
     * {@link DlCustomForm#parentClassId}
     * @return
     */
    public Integer getParentClassId() {
        return parentClassId;
    }

    /**
     * {@link DlCustomForm#parentClassId}
     * @return
     */
    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public JsonNode getSchemaDefinitionJson() {
        return schemaDefinitionJson;
    }

    public void setSchemaDefinitionJson(JsonNode schemaDefinitionJson) {
        this.schemaDefinitionJson = schemaDefinitionJson;
    }

    /**
     * Checks whether the Custom Form passes minimum validity checks: valid classId, objectId, parentClassId, parentObjectId and customFormId
     * @return
     */
    public Boolean minimumValidity() {
        Boolean isValid = Boolean.TRUE;

        if (this.classId <= 0) isValid = Boolean.FALSE;
        if (this.objectId <= 0) isValid = Boolean.FALSE;
        if (this.parentClassId < 0) isValid = Boolean.FALSE;
        if (this.parentObjectId < 0) isValid = Boolean.FALSE;
        if (this.customFormId <= 0) isValid = Boolean.FALSE;

        return isValid;
    }

    /**
     * Prepares the custom form definition by merging any enumeration associations into enumeration definitions.
     * TODO: throw exceptions if the Jackson parser cannot parse the custom form definition JSON string
     * TODO: need to check that customFormId is not null!
     */
    public void prepareSchemaDefinition(DriverManagerDataSource dataSource) {
        Map<String, ArrayList> enumAssocValues = new HashMap<>();

        // 1. read from the DL DB the enumeration associations for this custom form.
        ArrayList<DlCustomFormEnumAssoc> enumAssocs = new ArrayList<DlCustomFormEnumAssoc>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_enum_assoc");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_custom_form_id", this.customFormId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // 1.1. process each enumeration association
        for (Object resultRow : dbResultList) {
            LinkedCaseInsensitiveMap resultRowMap = (LinkedCaseInsensitiveMap) resultRow;
            Integer assocObjectId = (Integer) resultRowMap.get("assoc_object_id");
            Integer assocClassId = (Integer) resultRowMap.get("assoc_class_id");
            String enumAssocName = resultRowMap.get("enum_assoc_name").toString();

            // 1.2. read from the DL DB the elements for the Ontology list identified by assocObjectName
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_list_cdg_elements");
            in = new MapSqlParameterSource()
                    .addValue("_list_cdg_id", assocObjectId, Types.INTEGER);
            out = jdbcCall.execute(in);
            ArrayList dbResultList02 = (ArrayList) out.get("#result-set-1");

            enumAssocValues.put(enumAssocName, dbResultList02);
        }

        // 2. parse the formDefinition JSON string into a JSON object so that enumeration associations can be interpolated.
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            this.schemaDefinitionJson = objectMapper.readTree(this.schemaDefinition);
        }
        catch (Exception e) {
            // TODO: handle the exception!
        }

        // 3. find enumeration associations and replace them with full value lists.
        List<JsonNode> enumNodes = this.schemaDefinitionJson.findValues("enum");
        for (JsonNode enumNode : enumNodes ) {
            Integer removeIndex = -1;
            Iterator<JsonNode> enumValues = enumNode.elements();
            Integer i = 0;

            while(enumValues.hasNext()) {
                JsonNode enumValue = enumValues.next();
                if (enumAssocValues.containsKey(enumValue.asText())) {
                    removeIndex = i;

                    ArrayList replaceEnumAssoc = (ArrayList) enumAssocValues.get(enumValue.asText());

                    for (Object listElement : replaceEnumAssoc) {
                        LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
                        ((ArrayNode) enumNode).add(elementInfo.get("element_name").toString());
                    }
                    break;
                }
                i++;
            }
            if (removeIndex > -1) {
                ((ArrayNode) enumNode).remove(removeIndex);
            }
        }

    }

}
