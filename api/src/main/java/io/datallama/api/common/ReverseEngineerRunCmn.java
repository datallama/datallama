/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Types;
import java.util.Map;

public class ReverseEngineerRunCmn {
    private DriverManagerDataSource dataSource;
    private Integer objectId;       // The objectId of the DL metadata object associated with the reverse engineering run
    private Integer classId;        // The classId of the DL metadata object associated with the reverse engineering run
    private Integer revEngRunId;
    private Integer revEngRunResult;
    private String revEngRunCode;
    private String revEngRunDesc;
    private String revEngRunDt;
    private Integer userId;
    private String userLogin;
    private String inputSource;

    /**
     * Default Constructor. No parameters
     */
    public ReverseEngineerRunCmn() {
        this.dataSource = new DriverManagerDataSource();
        this.objectId = 0;
        this.classId = 0;
        this.revEngRunId = 0;
        this.revEngRunResult = 0;
        this.revEngRunCode = "";
        this.revEngRunDesc = "";
        this.revEngRunDt = "";
        this.userId = 0;
        this.userLogin = "";
        this.inputSource = "";
    }

    /**
     * Paramatized Constructor 01 - only objectId and classId parameters. Used when generating a new revEngRun record in the DL DB
     * @param objectId
     * @param classId
     */
    public ReverseEngineerRunCmn(DriverManagerDataSource dataSource, Integer objectId, Integer classId) {
        this.dataSource = dataSource;
        this.objectId = objectId;
        this.classId = classId;
        this.revEngRunId = 0;
        this.revEngRunResult = 0;
        this.revEngRunCode = "";
        this.revEngRunDesc = "";
        this.revEngRunDt = "";
        this.userId = 0;
        this.userLogin = "";
        this.inputSource = "";
    }

    /**
     * Paramatized Constructor 02 - all members specified except revEngRunDescription
     * @param revEngRunId
     * @param revEngRunResult
     * @param revEngRunCode
     * @param revEngRunDt
     * @param userId
     * @param userLogin
     */
    public ReverseEngineerRunCmn(DriverManagerDataSource dataSource, Integer revEngRunId, Integer revEngRunResult,
                                 String revEngRunCode, String revEngRunDt, Integer userId, String userLogin) {
        this.dataSource = dataSource;
        this.objectId = 0;
        this.classId = 0;
        this.revEngRunId = revEngRunId;
        this.revEngRunResult = revEngRunResult;
        this.revEngRunCode = revEngRunCode;
        this.revEngRunDesc = "";
        this.revEngRunDt = revEngRunDt;
        this.userId = userId;
        this.userLogin = userLogin;
        this.inputSource = "";
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getRevEngRunId() {
        return revEngRunId;
    }

    public void setRevEngRunId(Integer revEngRunId) {
        this.revEngRunId = revEngRunId;
    }

    public Integer getRevEngRunResult() {
        return revEngRunResult;
    }

    public void setRevEngRunResult(Integer revEngRunResult) {
        this.revEngRunResult = revEngRunResult;
    }

    public String getRevEngRunCode() {
        return revEngRunCode;
    }

    public void setRevEngRunCode(String revEngRunCode) {
        this.revEngRunCode = revEngRunCode;
    }

    public String getRevEngRunDesc() {
        return revEngRunDesc;
    }

    public void setRevEngRunDesc(String revEngRunDesc) {
        this.revEngRunDesc = revEngRunDesc;
    }

    public String getRevEngRunDt() {
        return revEngRunDt;
    }

    public void setRevEngRunDateTime(String revEngRunDt) {
        this.revEngRunDt = revEngRunDt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getInputSource() {
        return inputSource;
    }

    public void setInputSource(String inputSource) {
        this.inputSource = inputSource;
    }

    /**
     * Generates a new revEngRunId in the DL DB and returns the revEngRunId.
     * @return
     */
    public RevEngResultCmn generateRevEngRun() {
        Integer revEngRunId = 0;
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // create a new reverse engineering run record in the DL DB
        Integer newRunId = -1;
        Integer newRunResult = 1;
        revEngResult.setGeneralErrorMessage("Starting reverse engineering run ...");

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_reveng_run_cmn");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                .addValue("_reveng_run_result", newRunResult, Types.INTEGER)
                .addValue("_reveng_run_desc", revEngResult.getGeneralErrorMessage(), Types.VARCHAR)
                .addValue("_input_source", inputSource, Types.VARCHAR);
        Map out = jdbcCall.execute(in);
        this.revEngRunId = (Integer) out.get("_return_id");

        if (this.revEngRunId < -1) {
            revEngResult.setResultStatus(-2);
            revEngResult.setGeneralErrorMessage("Failed to create a new reverse engineer run record in table dl_reveng_run_cmn.");
            revEngResult.setErrorNumber(-201301);
        }

        return revEngResult;
    }

    /**
     * Closes a reverse engineering run
     * @param revEngResult
     * @return
     */
    public RevEngResultCmn closeRevEngRun(RevEngResultCmn revEngResult) {
        RevEngResultCmn sqlExecuteResult = new RevEngResultCmn();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_reveng_run_cmn");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_reveng_run_id", revEngRunId, Types.INTEGER)
                .addValue("_reveng_run_result", revEngResult.getResultStatus(), Types.INTEGER)
                .addValue("_reveng_run_desc", revEngResult.getGeneralErrorMessage(), Types.VARCHAR)
                .addValue("_input_source", "not required", Types.VARCHAR);
        try {
            Map out = jdbcCall.execute(in);
        } catch (Exception e) {
            sqlExecuteResult.setResultStatus(-2);
            sqlExecuteResult.setGeneralErrorMessage("Error when trying to close the reverse engineer run record. Stored procedure name write_reveng_run_cmn.");
            sqlExecuteResult.setErrorNumber(200305);
        }

        return sqlExecuteResult;
    }

}
