/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.common;

public class DlMoClass {
    private Integer classId;
    private String className;
    private String classCode;
    private String classShortDesc;
    private Integer customFormId;
    private String customFormName;
    private String objectTable;
    private String objectDetailState;
    private DlMoClassConfig classConfig;

    public DlMoClass() {
        this.classId = 0;
        this.className = "";
        this.classCode = "";
        this.classShortDesc = "";
        this.customFormId = 0;
        this.customFormName = "";
        this.objectTable = "";
        this.objectDetailState = "";
        this.classConfig = new DlMoClassConfig();
    }

    public DlMoClass(Integer classId, String className, String classShortDesc, Integer customFormId, String customFormName) {
        this.classId = classId;
        this.className = className;
        this.classCode = "";
        this.classShortDesc = classShortDesc;
        this.customFormId = customFormId;
        this.customFormName = customFormName;
        this.objectTable = "";
        this.objectDetailState = "";
        this.classConfig = new DlMoClassConfig();
    }

    public DlMoClass(Integer classId, String className, String classShortDesc, String objectTable, String objectDetailState) {
        this.classId = classId;
        this.className = className;
        this.classCode = "";
        this.classShortDesc = classShortDesc;
        this.customFormId = 0;
        this.customFormName = "";
        this.objectTable = objectTable;
        this.objectDetailState = objectDetailState;
        this.classConfig = new DlMoClassConfig();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassShortDesc() {
        return classShortDesc;
    }

    public void setClassShortDesc(String classShortDesc) {
        this.classShortDesc = classShortDesc;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public String getCustomFormName() {
        return customFormName;
    }

    public void setCustomFormName(String customFormName) {
        this.customFormName = customFormName;
    }

    public String getObjectTable() {
        return objectTable;
    }

    public void setObjectTable(String objectTable) {
        this.objectTable = objectTable;
    }

    public String getObjectDetailState() {
        return objectDetailState;
    }

    public void setObjectDetailState(String objectDetailState) {
        this.objectDetailState = objectDetailState;
    }

    public DlMoClassConfig getClassConfig() {
        return classConfig;
    }

    public void setClassConfig(DlMoClassConfig classConfig) {
        this.classConfig = classConfig;
    }
}
