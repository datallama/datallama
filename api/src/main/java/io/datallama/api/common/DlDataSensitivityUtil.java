package io.datallama.api.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public class DlDataSensitivityUtil {
    private DriverManagerDataSource dataSource;
    private Integer userId;

    private ObjectMapper objectMapper;


    /**
     * Default constructor with all parameters
     * @param dataSource
     * @param userId
     */
    public DlDataSensitivityUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.objectMapper = new ObjectMapper();
    }

    /**
     * Reads from the DL DB details of the Data Sensitivity Tag identified by dataSensitivityId
     * @param dataSensitivityId
     * @return
     */
    public DlDataSensitivityTag getDataSensitivityTag (Integer dataSensitivityId) {
        DlDataSensitivityTag dsTag = new DlDataSensitivityTag();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_tag");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_data_sensitivity_id", dataSensitivityId, Types.INTEGER);

        Map<String, Object> rslts = jdbcCall.execute(in);

        Integer tagExists = (Integer) rslts.get("_tag_exists");

        if (tagExists.equals(0)) {
            dsTag.setDlDsTagId(-1);
        }
        else {
            dsTag.setDlDsTagId(dataSensitivityId);
            dsTag.setDlDsLevel(rslts.get("_data_sensitivity_level").toString());
            dsTag.setDlDsName(rslts.get("_data_sensitivity_name").toString());
            if(rslts.get("_data_sensitivity_name") != null)
                dsTag.setDlDsShortDesc(rslts.get("_data_sensitivity_name").toString());
        }

        return dsTag;
    }

    /**
     * Runs the Data Sensitivity Auto Assignment Task identified by dsAutoAssignTaskId
     * @param dsAutoAssignTaskId
     * @param userId
     * @return
     */
    public Map<String, Object> runDsAutoAssignTask (Integer dsAutoAssignTaskId, Integer userId) {
        Integer resultStatus = 1;
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();

        // 1. get the task details
        DlDataSensitivityAutoAssignTask dsAutoAssignTask = getDataSensitivityAutoAssignTask(dsAutoAssignTaskId);

        if (dsAutoAssignTask.getTaskId().equals(-1)) {
            resultStatus = -1;
            // TODO: md_object not found errors need to be handled via a standardised pattern.
            resultData.put("error_details", "A Data Sensitivity Auto Assignment Task does not exists with the specified dsAutoAssignTaskId");
        }

        // 2. call auto assignment stored procedures for each class collection.
        if (resultStatus == 1) {
            DlDsAutoAssignRunReport dsAutoAssignRunReport = new DlDsAutoAssignRunReport();
            dsAutoAssignRunReport.setTaskId(dsAutoAssignTaskId);
            dsAutoAssignRunReport.setUserId(userId);
            dsAutoAssignRunReport.setInvocationMode(dsAutoAssignTask.getInvocationMode());

            Integer totalRowsUpdated = 0;
            for (String classCollectionCode: dsAutoAssignTask.getClassList().getClassCollections()) {
                if (classCollectionCode.toUpperCase().equals("TVCOL")) {
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_ds_auto_assign_columns");
                    SqlParameterSource in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)
                            .addValue("_data_sensitivity_id", dsAutoAssignTask.getDsTag().getDlDsTagId(), Types.INTEGER)
                            .addValue("_object_name_match_include", dsAutoAssignTask.getObjectNameMatchInclude(), Types.VARCHAR)
                            .addValue("_object_name_match_exclude", dsAutoAssignTask.getObjectNameMatchExclude(), Types.VARCHAR)
                            .addValue("_datatype_match", dsAutoAssignTask.getDatatypeMatch(), Types.VARCHAR)
                            .addValue("_overwrite", dsAutoAssignTask.getOverwrite(), Types.BOOLEAN);

                    Map<String, Object> out = jdbcCall.execute(in);
                    Integer rowsUpdated = (Integer) out.get("_rows_updated");
                    dsAutoAssignRunReport.addUpdateDetail("TVCOL", rowsUpdated);
                    totalRowsUpdated += rowsUpdated;
                }
                if (classCollectionCode.toUpperCase().equals("PRESFF")) {
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_ds_auto_assign_smd_hierarchy");
                    SqlParameterSource in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)
                            .addValue("_ds_class_collection_code", "PRESFF", Types.VARCHAR)
                            .addValue("_data_sensitivity_id", dsAutoAssignTask.getDsTag().getDlDsTagId(), Types.INTEGER)
                            .addValue("_object_name_match_include", dsAutoAssignTask.getObjectNameMatchInclude(), Types.VARCHAR)
                            .addValue("_object_name_match_exclude", dsAutoAssignTask.getObjectNameMatchExclude(), Types.VARCHAR)
                            .addValue("_datatype_match", dsAutoAssignTask.getDatatypeMatch(), Types.VARCHAR)
                            .addValue("_overwrite", dsAutoAssignTask.getOverwrite(), Types.BOOLEAN);

                    Map<String, Object> out = jdbcCall.execute(in);
                    Integer rowsUpdated = (Integer) out.get("_rows_updated");
                    dsAutoAssignRunReport.addUpdateDetail("PRESFF", rowsUpdated);
                    totalRowsUpdated += rowsUpdated;
                }
                if (classCollectionCode.toUpperCase().equals("MSGFF")) {
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_ds_auto_assign_smd_hierarchy");
                    SqlParameterSource in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)
                            .addValue("_ds_class_collection_code", "MSGFF", Types.VARCHAR)
                            .addValue("_data_sensitivity_id", dsAutoAssignTask.getDsTag().getDlDsTagId(), Types.INTEGER)
                            .addValue("_object_name_match_include", dsAutoAssignTask.getObjectNameMatchInclude(), Types.VARCHAR)
                            .addValue("_object_name_match_exclude", dsAutoAssignTask.getObjectNameMatchExclude(), Types.VARCHAR)
                            .addValue("_datatype_match", dsAutoAssignTask.getDatatypeMatch(), Types.VARCHAR)
                            .addValue("_overwrite", dsAutoAssignTask.getOverwrite(), Types.BOOLEAN);

                    Map<String, Object> out = jdbcCall.execute(in);
                    Integer rowsUpdated = (Integer) out.get("_rows_updated");
                    dsAutoAssignRunReport.addUpdateDetail("MSGFF", rowsUpdated);
                    totalRowsUpdated += rowsUpdated;
                }
                if (classCollectionCode.toUpperCase().equals("FFFF")) {
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_ds_auto_assign_flatfile");
                    SqlParameterSource in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)
                            .addValue("_data_sensitivity_id", dsAutoAssignTask.getDsTag().getDlDsTagId(), Types.INTEGER)
                            .addValue("_object_name_match_include", dsAutoAssignTask.getObjectNameMatchInclude(), Types.VARCHAR)
                            .addValue("_object_name_match_exclude", dsAutoAssignTask.getObjectNameMatchExclude(), Types.VARCHAR)
                            .addValue("_datatype_match", dsAutoAssignTask.getDatatypeMatch(), Types.VARCHAR)
                            .addValue("_overwrite", dsAutoAssignTask.getOverwrite(), Types.BOOLEAN);

                    Map<String, Object> out = jdbcCall.execute(in);
                    Integer rowsUpdated = (Integer) out.get("_rows_updated");
                    dsAutoAssignRunReport.addUpdateDetail("FFFF", rowsUpdated);
                    totalRowsUpdated += rowsUpdated;
                }

            }
            dsAutoAssignRunReport.addUpdateDetail("TOTAL", totalRowsUpdated);

            // 3. write the run report to the DL DB
            dsAutoAssignRunReport = writeDsAutoAssignRunReport(dsAutoAssignRunReport);

            resultData.put("dsAutoAssignRunReport", dsAutoAssignRunReport);
        }

        results.put("result_status", resultStatus);
        results.put("result_data", resultData);

        return results;
    }

    /**
     * Reads from the DL DB details of the Data Sensitivity Auto Assignment Task identified by dsAutoAssignTaskId
     * @param dsAutoAssignTaskId
     * @return
     */
    public DlDataSensitivityAutoAssignTask getDataSensitivityAutoAssignTask (Integer dsAutoAssignTaskId) {

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_ds_auto_assign_task");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_ds_auto_assign_task_id", dsAutoAssignTaskId, Types.INTEGER);
        Map<String, Object> rslts = jdbcCall.execute(in);
        DlDataSensitivityAutoAssignTask dsAutoAssignTask = new DlDataSensitivityAutoAssignTask();

        Integer taskExists = (Integer) rslts.get("_task_exists");
        if (taskExists.equals(1)) {
            Integer taskDsTagId = (Integer) rslts.get("_data_sensitivity_id");

            dsAutoAssignTask.setTaskId(dsAutoAssignTaskId);
            dsAutoAssignTask.setDsTag(getDataSensitivityTag(taskDsTagId));
            dsAutoAssignTask.setTaskName(rslts.get("_ds_auto_assign_task_name").toString());
            if (rslts.get("_ds_auto_assign_task_shortdesc") != null)
                dsAutoAssignTask.setTaskShortDesc(rslts.get("_ds_auto_assign_task_shortdesc").toString());
            // add the class list object
            try {
                DlDsAutoAssignTaskClassList taskClassList = objectMapper.readValue(rslts.get("_class_list").toString(), DlDsAutoAssignTaskClassList.class);
                dsAutoAssignTask.setClassList(taskClassList);
            } catch (Exception e) {
                System.out.println("ERROR: could not parse the JSON document for DS Auto Assign Task class_list.");
                e.printStackTrace();
            }
            dsAutoAssignTask.setInvocationMode((Integer) rslts.get("_invocation_mode"));
            dsAutoAssignTask.setObjectNameMatchInclude(rslts.get("_object_name_match_include").toString());
            if (rslts.get("_object_name_match_exclude") != null)
                dsAutoAssignTask.setObjectNameMatchExclude(rslts.get("_object_name_match_exclude").toString());
            dsAutoAssignTask.setDatatypeMatch(rslts.get("_datatype_match").toString());
            dsAutoAssignTask.setOverwrite((Boolean) rslts.get("_overwrite"));
            // TODO: Add in the schedule definition when scheduling is implemented.
        }
        else {
            dsAutoAssignTask.setTaskId(-1);
        }

        return dsAutoAssignTask;
    }

    /**
     * Writes the Data Sensitivity Auto Assignment Run Report to the DL DB.
     * @param runReport
     * @return
     */
    public DlDsAutoAssignRunReport writeDsAutoAssignRunReport(DlDsAutoAssignRunReport runReport) {

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_ds_auto_assign_task_run");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", runReport.getUserId(), Types.INTEGER)
                .addValue("_ds_auto_assign_task_id", runReport.getTaskId(), Types.INTEGER)
                .addValue("_invocation_mode", runReport.getInvocationMode(), Types.INTEGER)
                .addValue("_run_report", runReport.getUpdateDetailsAsJsonString(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        runReport.setTaskRunId((Integer) out.get("_ds_auto_assign_task_run_id"));
        return runReport;
    }

}
