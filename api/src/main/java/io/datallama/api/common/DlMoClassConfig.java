/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.common;

public class DlMoClassConfig {
    private Integer classConfigId;
    private String classConfigName;
    private String configSchemaDefn;
    private String configFormDefn;
    private String classConfigData;

    /**
     * Default constructor with no parameters
     */
    public DlMoClassConfig() {
        this.classConfigId = 0;
        this.classConfigName = "";
        this.configSchemaDefn = "";
        this.configFormDefn = "";
        this.classConfigData = "";
    }

    /**
     * Constructor 02 with all parameters.
     * @param classConfigId
     * @param classConfigName
     * @param configSchemaDefn
     * @param configFormDefn
     */
    public DlMoClassConfig(Integer classConfigId, String classConfigName, String configSchemaDefn, String configFormDefn) {
        this.classConfigId = classConfigId;
        this.classConfigName = classConfigName;
        this.configSchemaDefn = configSchemaDefn;
        this.configFormDefn = configFormDefn;
        this.classConfigData = "";
    }

    public Integer getClassConfigId() {
        return classConfigId;
    }

    public void setClassConfigId(Integer classConfigId) {
        this.classConfigId = classConfigId;
    }

    public String getClassConfigName() {
        return classConfigName;
    }

    public void setClassConfigName(String classConfigName) {
        this.classConfigName = classConfigName;
    }

    public String getConfigSchemaDefn() {
        return configSchemaDefn;
    }

    public void setConfigSchemaDefn(String configSchemaDefn) {
        this.configSchemaDefn = configSchemaDefn;
    }

    public String getConfigFormDefn() {
        return configFormDefn;
    }

    public void setConfigFormDefn(String configFormDefn) {
        this.configFormDefn = configFormDefn;
    }

    public String getClassConfigData() {
        return classConfigData;
    }

    public void setClassConfigData(String classConfigData) {
        this.classConfigData = classConfigData;
    }
}
