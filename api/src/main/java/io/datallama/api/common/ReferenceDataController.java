/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class ReferenceDataController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Returns the list constant names and values for all metadata object classes defined in Data Llama
     * URL Path: /ref/class/constants
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/ref/class/constants")
    @ResponseBody
    public Map<String, Object> readClassConstants() {
        Map<String, Object> results = new HashMap<>();
        //Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList<DlMoClass> moClassList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_all_classes_detail");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // second: create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listObject : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap classInfo = (LinkedCaseInsensitiveMap) listObject;
            DlMoClass moClass = new DlMoClass();
            moClass.setClassId((Integer) classInfo.get("class_id"));
            moClass.setCustomFormId((Integer) classInfo.get("custom_form_id"));
            moClass.setClassName(classInfo.get("class_name").toString());
            moClass.setClassCode(classInfo.get("class_code").toString());
            moClass.setObjectTable(classInfo.get("object_table").toString());
            moClass.setObjectDetailState(classInfo.get("object_detail_state").toString());

            moClassList.add(moClass);
        }

        results.put("result_data", moClassList);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Returns the list constant names and values for all metadata object classes defined in Data Llama
     * URL Path: /ref/class/constants
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/ref/class/hierarchy")
    @ResponseBody
    public Map<String, Object> readClassHierarchy() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();

        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList<MoClassHierarchyElement> moClassList = new ArrayList(); // this list will be returned in the resultData Map

        // 1. call the DL DB stored procedure to read class hierarchy
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_class_hierarchy");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // 2. create a class hierarchy element for each class returned by DL DB
        for (Object listObject : dbResultList) {
            LinkedCaseInsensitiveMap classInfo = (LinkedCaseInsensitiveMap) listObject;

            MoClassHierarchyElement moClassHierarchyElement = new MoClassHierarchyElement(
                    (Integer) classInfo.get("class_id"),
                    classInfo.get("class_name").toString(),
                    classInfo.get("class_shortdesc").toString(),
                    (Integer) classInfo.get("custom_form_id"),
                    classInfo.get("custom_form_name").toString(),
                    (Integer) classInfo.get("parent_class_id"),
                    (Integer) classInfo.get("class_level"),
                    (Integer) classInfo.get("class_level_sequence"),
                    classInfo.get("class_hierarchy_number").toString(),
                    (Boolean) classInfo.get("is_parent"),
                    (Boolean) classInfo.get("is_abstract")
            );

            moClassList.add(moClassHierarchyElement);
        }

        results.put("result_data", moClassList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL database details of the metadata object class identified by {classId}
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/ref/class/detail/{classId}")
    @ResponseBody
    public Map<String, Object> readClassDetail(@PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // first, get the summary information for the group.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_class_detail");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        MoClassHierarchyElement dlMoClass = new MoClassHierarchyElement(
                classId,
                out.get("_class_name").toString(),
                out.get("_class_shortdesc").toString(),
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                (Integer) out.get("_class_parent_id"),
                (Integer) out.get("_class_level"),
                (Integer) out.get("_class_level_sequence"),
                out.get("_class_hierarchy_number").toString(),
                (Boolean) int2bool((Integer) out.get("_is_parent")),
                (Boolean) int2bool((Integer) out.get("_is_abstract"))
        );

        results.put("result_data", dlMoClass);
        results.put("result_status", resultStatus);

        return results;
    }

    private Boolean int2bool(Integer val) {
        if (val <= 0) return false;
        return true;
    }
}