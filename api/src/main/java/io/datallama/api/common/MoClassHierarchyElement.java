/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.common;

/**
 * Represents a metadata objec class as an element in the MO Class Hierarchy. This is used for display on the DL UI.
 */
public class MoClassHierarchyElement extends DlMoClass {
    private Integer classParentId;
    private Integer classLevel;
    private Integer classLevelSequence;
    private String classHierarchyNumber;
    private Boolean isParent;
    private Boolean isAbstract;

    /**
     * Explicit Constructor with all parameters
     */
    public MoClassHierarchyElement(Integer classId, String className, String classShortDesc, Integer customFormId, String customFormName,
                                   Integer classParentId, Integer classLevel, Integer classLevelSequence, String classHierarchyNumber,
                                   Boolean isParent, Boolean isAbstract) {
        this.setClassId(classId);
        this.setClassName(className);
        this.setClassShortDesc(classShortDesc);
        this.setCustomFormId(customFormId);
        this.setCustomFormName(customFormName);
        this.classParentId = classParentId;
        this.classLevel = classLevel;
        this.classLevelSequence = classLevelSequence;
        this.classHierarchyNumber = classHierarchyNumber;
        this.isParent = isParent;
        this.isAbstract = isAbstract;
    }

    public Integer getClassParentId() {
        return classParentId;
    }

    public void setClassParentId(Integer classParentId) {
        this.classParentId = classParentId;
    }

    public Integer getClassLevel() {
        return classLevel;
    }

    public void setClassLevel(Integer classLevel) {
        this.classLevel = classLevel;
    }

    public Integer getClassLevelSequence() {
        return classLevelSequence;
    }

    public void setClassLevelSequence(Integer classLevelSequence) {
        this.classLevelSequence = classLevelSequence;
    }

    public String getClassHierarchyNumber() {
        return classHierarchyNumber;
    }

    public void setClassHierarchyNumber(String classHierarchyNumber) {
        this.classHierarchyNumber = classHierarchyNumber;
    }

    public Boolean getParent() {
        return isParent;
    }

    public void setParent(Boolean parent) {
        isParent = parent;
    }

    public Boolean getAbstract() {
        return isAbstract;
    }

    public void setAbstract(Boolean anAbstract) {
        isAbstract = anAbstract;
    }
}
