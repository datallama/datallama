/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

/**
 * Models a credential type. Varying types of credentials are used when reverse engineering structural metadata.
 */
public class CredentialType {
    private Integer credentialTypeId;
    private String credentialTypeName;
    private String credentialTypeShortDesc;
    private String credentialTemplate;
    private String schemaDefinition;
    private String formDefinition;

    /**
     * Default constructor with no parameters.
     */
    public CredentialType() {
        this.credentialTypeId = 0;
        this.credentialTypeName = "";
        this.credentialTypeShortDesc = "";
        this.credentialTemplate = "";
        this.schemaDefinition = "";
        this.formDefinition = "";
    }

    /**
     * Constructor 02 with all parameters.
     * @param credentialTypeId
     * @param credentialTypeName
     * @param credentialTypeShortDesc
     */
    public CredentialType(Integer credentialTypeId, String credentialTypeName, String credentialTypeShortDesc) {
        this.credentialTypeId = credentialTypeId;
        this.credentialTypeName = credentialTypeName;
        this.credentialTypeShortDesc = credentialTypeShortDesc;
        this.credentialTemplate = "";
        this.schemaDefinition = "";
        this.formDefinition = "";
    }

    public Integer getCredentialTypeId() {
        return credentialTypeId;
    }

    public void setCredentialTypeId(Integer credentialTypeId) {
        this.credentialTypeId = credentialTypeId;
    }

    public String getCredentialTypeName() {
        return credentialTypeName;
    }

    public void setCredentialTypeName(String credentialTypeName) {
        this.credentialTypeName = credentialTypeName;
    }

    public String getCredentialTypeShortDesc() {
        return credentialTypeShortDesc;
    }

    public void setCredentialTypeShortDesc(String credentialTypeShortDesc) {
        this.credentialTypeShortDesc = credentialTypeShortDesc;
    }

    public String getCredentialTemplate() {
        return credentialTemplate;
    }

    public void setCredentialTemplate(String credentialTemplate) {
        this.credentialTemplate = credentialTemplate;
    }

    public String getSchemaDefinition() {
        return schemaDefinition;
    }

    public void setSchemaDefinition(String schemaDefinition) {
        this.schemaDefinition = schemaDefinition;
    }

    public String getFormDefinition() {
        return formDefinition;
    }

    public void setFormDefinition(String formDefinition) {
        this.formDefinition = formDefinition;
    }
}
