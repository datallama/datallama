/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class MoFullDescriptionController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Reads the full description for a metadata object from the DL database
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/common/fulldescription/{objectId}/{classId}")
    @ResponseBody
    public Map<String, Object> readObjectComment(@PathVariable Integer objectId, @PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_full_description");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        String fulldesctext;
        try {
            fulldesctext = out.get("_full_desc").toString();
        }
        catch (NullPointerException exc) {
            fulldesctext = "No full description has been defined for this object.";
        }

        MoFullDescription fulldesc = new MoFullDescription(
                objectId,
                classId,
                fulldesctext
        );

        results.put("result_data", fulldesc);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Write the full description for a metadata object into the DL database
     * @param fulldesc
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/fulldescription")
    public Map<String, Object> writeObjectComment(@RequestBody MoFullDescription fulldesc, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_full_description");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", fulldesc.getObjectId(), Types.INTEGER)
                .addValue("_class_id", fulldesc.getClassId(), Types.INTEGER)
                .addValue("_full_desc", fulldesc.getFullDescription(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

}
