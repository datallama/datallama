/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DlDsAutoAssignTaskClassList {
    @JsonProperty("class_id_list")
    private Integer[] classIdList;
    @JsonProperty("selection_method")
    private String selectionMethod;
    @JsonProperty("class_collections")
    private String[] classCollections;

    public DlDsAutoAssignTaskClassList() {
        this.classIdList = new Integer[0];
        this.selectionMethod = "";
        this.classCollections = new String[0];
    }

    public Integer[] getClassIdList() {
        return classIdList;
    }

    public void setClassIdList(Integer[] classIdList) {
        this.classIdList = classIdList;
    }

    public String getSelectionMethod() {
        return selectionMethod;
    }

    public void setSelectionMethod(String selectionMethod) {
        this.selectionMethod = selectionMethod;
    }

    public String[] getClassCollections() {
        return classCollections;
    }

    public void setClassCollections(String[] classCollections) {
        this.classCollections = classCollections;
    }

    public String toJson() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace(); return null;
        }
    }

}
