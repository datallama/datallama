/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.common;

import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.smd.DlDataSensitivityAssignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DataSensitivityController {
    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Returns the list constant names and values for all metadata object classes defined in Data Llama
     * URL Path: /common/datasensitivity/tag/list
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/common/datasensitivity/tag/list")
    @ResponseBody
    public Map<String, Object> readDataSensitivityTagList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList<DlDataSensitivityTag> dsTagList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_tags");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listObject : dbResultList) {
            LinkedCaseInsensitiveMap tagInfo = (LinkedCaseInsensitiveMap) listObject;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag();
            dsTag.setDlDsTagId((Integer) tagInfo.get("data_sensitivity_id"));
            dsTag.setDlDsLevel(tagInfo.get("data_sensitivity_level").toString());
            dsTag.setDlDsName(tagInfo.get("data_sensitivity_name").toString());
            dsTag.setDlDsShortDesc(tagInfo.get("data_sensitivity_shortdesc").toString());

            dsTagList.add(dsTag);
        }

        results.put("result_data", dsTagList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the full list of specifically assigned data sensitivity tags:
     * 1. specific single DS tag assignments for smd columns
     * 2. specific single DS tag assignments for flatfiles
     * 3. specific single DS tag assignments for smd hierarchies
     * 4. specific multiple DS tag assignments for smd hierarchies
     * URL Path: /common/datasensitivity/report/full
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/common/datasensitivity/report/full")
    @ResponseBody
    public Map<String, Object> readDataSensitivityFullReport() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList<DlDataSensitivityAssignment> dsFullReport = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_full_report");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResults = (ArrayList) out.get("#result-set-1");

        // 2. create a hierarchy of Array Lists from the simple result set returned by database
        for (Object dsReport : dbResults) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap dsReportInfo = (LinkedCaseInsensitiveMap) dsReport;

            // 1.1. create the base DlDataSensitivityAssignment object
            DlDataSensitivityAssignment dsAssignment = new DlDataSensitivityAssignment();
            dsAssignment.setObjectId((Integer) dsReportInfo.get("object_id"));
            dsAssignment.setClassId((Integer) dsReportInfo.get("class_id"));
            dsAssignment.setObjectName(dsReportInfo.get("object_name").toString());
            dsAssignment.setClassName(dsReportInfo.get("class_name").toString());

            // 1.2. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            dsAssignment.setMdContext(mdContext.generateMdContextList(dsReportInfo.get("md_context").toString()));

            // 1.3. add the data sensitivity tag - the most important part!
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                    (Integer) dsReportInfo.get("data_sensitivity_id"),
                    (String) dsReportInfo.get("data_sensitivity_level"),
                    (String) dsReportInfo.get("data_sensitivity_name")
            );
            dsAssignment.setDsTag(dsTag);

            dsFullReport.add(dsAssignment);

        }

        results.put("result_data", dsFullReport);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves the data sensitivity tag details contained in object dlDsTag to the DL DB
     *
     * @param dlDsTag
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/datasensitivity/tag")
    public Map<String, Object> writeDataSensitivityTag(@RequestBody DlDataSensitivityTag dlDsTag, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_data_sensitivity_tag");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_data_sensitivity_id", dlDsTag.getDlDsTagId(), Types.INTEGER)
                .addValue("_data_sensitivity_level", dlDsTag.getDlDsLevel(), Types.VARCHAR)
                .addValue("_data_sensitivity_name", dlDsTag.getDlDsName(), Types.VARCHAR)
                .addValue("_data_sensitivity_shortdesc", dlDsTag.getDlDsShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        Integer returnId = (Integer) out.get("_return_id");
        resultStatus = (returnId < 0) ? returnId : 1;
        resultData.put("returnId", returnId);

        results.put("result_status", resultStatus);
        results.put("result_data", resultData);

        return results;
    }

    /**
     * Deletes a data sensitivity tag from the DL DB.  All assignments of that tag are purged.
     *
     * @param dlDsTagId
     * @param userId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/common/datasensitivity/tag/{dlDsTagId}")
    public Map<String, Object> deleteDataSensitivityTag(@PathVariable Integer dlDsTagId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_data_sensitivity_tag");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_data_sensitivity_id", dlDsTagId, Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Updates the specific multi assignment for the md object identified in the DlDataSensitivityMultiUpdater object
     *
     * @param dlDsMultiUpdate
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/datasensitivity/multi")
    public Map<String, Object> writeDataSensitivityMulti(@RequestBody DlDataSensitivityMultiUpdater dlDsMultiUpdate, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        Integer objectId = dlDsMultiUpdate.getObjectId();
        Integer classId = dlDsMultiUpdate.getClassId();
        ArrayList<DlDataSensitivityTag> dlDsTagList = dlDsMultiUpdate.getDlDsTagList();

        // 1. process existing or newly added DS tags
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_data_sensitivity_multi");
        for (DlDataSensitivityTag dlDsTag : dlDsTagList) {
            if (dlDsTag.getCreateMode() > 0) {
                SqlParameterSource in = new MapSqlParameterSource()
                        .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                        .addValue("_object_id", objectId, Types.INTEGER)
                        .addValue("_class_id", classId, Types.INTEGER)
                        .addValue("_data_sensitivity_id", dlDsTag.getDlDsTagId(), Types.INTEGER)
                        .addValue("_create_mode", dlDsTag.getCreateMode(), Types.TINYINT);

                Map<String, Object> out = jdbcCall.execute(in);
            }
        }

        // 2. process deleted DS tags
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_data_sensitivity_multi");
        for (DlDataSensitivityTag dlDsTag : dlDsTagList) {
            if (dlDsTag.getCreateMode() < 0) {
                SqlParameterSource in = new MapSqlParameterSource()
                        .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                        .addValue("_object_id", objectId, Types.INTEGER)
                        .addValue("_class_id", classId, Types.INTEGER)
                        .addValue("_data_sensitivity_id", dlDsTag.getDlDsTagId(), Types.INTEGER);

                Map<String, Object> out = jdbcCall.execute(in);
            }
        }

        // 3. get the multi DS tag list for this md object
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_multi");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> rslts = jdbcCall.execute(in);

        ArrayList<DlDataSensitivityTag> resultDsTagList = new ArrayList<DlDataSensitivityTag>();
        ArrayList dbDsTagList = (ArrayList) rslts.get("#result-set-1");

        for (Object listElement : dbDsTagList) {
            LinkedCaseInsensitiveMap dsTagInfo = (LinkedCaseInsensitiveMap) listElement;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                    (Integer) dsTagInfo.get("data_sensitivity_id"),
                    dsTagInfo.get("data_sensitivity_level").toString(),
                    dsTagInfo.get("data_sensitivity_name").toString()
            );
            dsTag.setCreateMode((Integer) dsTagInfo.get("create_mode"));
            resultDsTagList.add(dsTag);
        }

        resultData.put("dlDsTagList", resultDsTagList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * @return
     */
    @RequestMapping(method = GET, value = "/common/datasensitivity/autoassigntask/list")
    @ResponseBody
    public Map<String, Object> readDsAutoAssignTaskList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<DlDataSensitivityAutoAssignTask> taskList = new ArrayList(); // this list will be returned in the results Map

        // 1. get the list of DS auto assignment tasks from the DL DB together with the list of DS tags
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_ds_auto_assign_task_list");
        Map<String, Object> rslts = jdbcCall.execute();

        ArrayList taskResultSet = (ArrayList) rslts.get("#result-set-1");
        ArrayList tagResultSet = (ArrayList) rslts.get("#result-set-2");
        Map<Integer, DlDataSensitivityTag> dsTagMap = new HashMap<>();

        // 1.1 load tags into a map
        for (Object tagElement : tagResultSet) {
            LinkedCaseInsensitiveMap tagInfo = (LinkedCaseInsensitiveMap) tagElement;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag();

            dsTag.setDlDsTagId((Integer) tagInfo.get("data_sensitivity_id"));
            dsTag.setDlDsLevel(tagInfo.get("data_sensitivity_level").toString());
            dsTag.setDlDsName(tagInfo.get("data_sensitivity_name").toString());
            dsTag.setDlDsShortDesc(tagInfo.get("data_sensitivity_shortdesc").toString());

            dsTagMap.put((Integer) tagInfo.get("data_sensitivity_id"), dsTag);
        }

        // 1.2 load tasks into taskList ArrayList
        for (Object taskElement : taskResultSet) {
            LinkedCaseInsensitiveMap dsAutoAssignTaskInfo = (LinkedCaseInsensitiveMap) taskElement;
            DlDataSensitivityAutoAssignTask dsAutoAssignTask = new DlDataSensitivityAutoAssignTask(
                    (Integer) dsAutoAssignTaskInfo.get("ds_auto_assign_task_id"),
                    dsAutoAssignTaskInfo.get("ds_auto_assign_task_name").toString(),
                    dsAutoAssignTaskInfo.get("ds_auto_assign_task_shortdesc").toString(),
                    (Integer) dsAutoAssignTaskInfo.get("invocation_mode"),
                    dsAutoAssignTaskInfo.get("object_name_match_include").toString(),
                    dsAutoAssignTaskInfo.get("datatype_match").toString()
            );
            if (dsAutoAssignTaskInfo.get("object_name_match_exclude") != null)
                dsAutoAssignTask.setObjectNameMatchExclude(dsAutoAssignTaskInfo.get("object_name_match_exclude").toString());

            // 1.2.1. add the DS Tag object
            dsAutoAssignTask.setDsTag(dsTagMap.get((Integer) dsAutoAssignTaskInfo.get("data_sensitivity_id")));

            // 1.2.2. add the class list object
            try {
                DlDsAutoAssignTaskClassList taskClassList = objectMapper.readValue(dsAutoAssignTaskInfo.get("class_list").toString(), DlDsAutoAssignTaskClassList.class);
                dsAutoAssignTask.setClassList(taskClassList);
            } catch (Exception e) {
                System.out.println("ERROR: could not parse the JSON document for DS Auto Assign Task class_list.");
                e.printStackTrace();
            }


            taskList.add(dsAutoAssignTask);
        }

        resultData.put("dlDsAutoAssignmentTaskList", taskList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads details of a single data sensitivity auto allocation task
     * @return
     */
    @RequestMapping(method = GET, value = "/common/datasensitivity/autoassigntask/{dsAutoAssignTaskId}")
    @ResponseBody
    public Map<String, Object> readDsAutoAssignTask(@PathVariable Integer dsAutoAssignTaskId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. get the auto assign task details
        DlDataSensitivityUtil dataSensitivityUtil = new DlDataSensitivityUtil(dataSource, 0);   // userId not required for GET
        DlDataSensitivityAutoAssignTask dsAutoAssignTask = dataSensitivityUtil.getDataSensitivityAutoAssignTask(dsAutoAssignTaskId);

        // 2. get the list of data sensitivity tags
        ArrayList<DlDataSensitivityTag> dsTagList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_tags");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listObject : dbResultList) {
            LinkedCaseInsensitiveMap tagInfo = (LinkedCaseInsensitiveMap) listObject;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag();
            Integer dsTagId = (Integer) tagInfo.get("data_sensitivity_id");
            dsTag.setDlDsTagId(dsTagId);
            dsTag.setDlDsLevel(tagInfo.get("data_sensitivity_level").toString());
            dsTag.setDlDsName(tagInfo.get("data_sensitivity_name").toString());
            dsTag.setDlDsShortDesc(tagInfo.get("data_sensitivity_shortdesc").toString());

            dsTagList.add(dsTag);
        }

        resultData.put("dsAutoAssignTask", dsAutoAssignTask);
        resultData.put("dsTagList", dsTagList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Creates a new data sensitivity auto assignment task in the DL DB
     * @param dsAutoAssignTask
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/datasensitivity/autoassigntask")
    @ResponseBody
    public Map<String, Object> writeDsAutoAssignTask(@RequestBody DlDataSensitivityAutoAssignTask dsAutoAssignTask,
                                                         @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        DlDataSensitivityTag dsTag = dsAutoAssignTask.getDsTag();
        DlDsAutoAssignTaskClassList classList = dsAutoAssignTask.getClassList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_ds_auto_assign_task");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_ds_auto_assign_task_id", dsAutoAssignTask.getTaskId(), Types.INTEGER)
                .addValue("_data_sensitivity_id", dsTag.getDlDsTagId(), Types.INTEGER)
                .addValue("_invocation_mode", dsAutoAssignTask.getInvocationMode(), Types.INTEGER)
                .addValue("_ds_auto_assign_task_name", dsAutoAssignTask.getTaskName(), Types.VARCHAR)
                .addValue("_ds_auto_assign_task_shortdesc", dsAutoAssignTask.getTaskShortDesc(), Types.VARCHAR)
                .addValue("_class_list", classList.toJson(), Types.VARCHAR)
                .addValue("_object_name_match_include", dsAutoAssignTask.getObjectNameMatchInclude(), Types.VARCHAR)
                .addValue("_object_name_match_exclude", dsAutoAssignTask.getObjectNameMatchExclude(), Types.VARCHAR)
                .addValue("_datatype_match", dsAutoAssignTask.getDatatypeMatch(), Types.VARCHAR)
                .addValue("_schedule_defn", dsAutoAssignTask.getScheduleDefn(), Types.VARCHAR)
                .addValue("_overwrite", dsAutoAssignTask.getOverwrite(), Types.BOOLEAN);

        Map<String, Object> out = jdbcCall.execute(in);
        Integer returnId = (Integer) out.get("_return_id");
        resultStatus = (returnId < 0) ? returnId : 1;
        resultData.put("returnId", returnId);

        results.put("result_status", resultStatus);
        results.put("result_data", resultData);

        return results;
    }


    /**
     * Runs the data sensitivity auto assign task identified by {dsAutoAssignTaskId}
     * @param dsAutoAssignTaskId
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/datasensitivity/runautoassign/{dsAutoAssignTaskId}")
    @ResponseBody
    public Map<String, Object> writeDsAutoAssignTaskRun(@PathVariable Integer dsAutoAssignTaskId,
                                                         @RequestHeader("dlUserId") String userId) {
        //Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();

        // call is made here to the data sensitivity auto assign task runner (DataSensitivityUtil)
        // this method can be invoked via user interaction or scheduled task run.
        DlDataSensitivityUtil dsUtil = new DlDataSensitivityUtil(dataSource, Integer.parseInt(userId));
        results = dsUtil.runDsAutoAssignTask(dsAutoAssignTaskId, Integer.parseInt(userId));

        return results;
    }
}