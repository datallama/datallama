/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class DlDataSensitivityAutoAssignTask {
    private Integer taskId;
    private String taskName;
    private String taskShortDesc;
    private DlDataSensitivityTag dsTag;
    private DlDsAutoAssignTaskClassList classList;
    private Integer invocationMode;
    private String objectNameMatchInclude;
    private String objectNameMatchExclude;
    private String datatypeMatch;
    private String scheduleDefn;
    private Boolean overwrite;
    private String latestRunDatetime;

    /**
     * Default constructor (no parameters)
     */
    public DlDataSensitivityAutoAssignTask() {
        this.taskId = 0;
        this.taskName = "";
        this.taskShortDesc = "";
        this.dsTag = new DlDataSensitivityTag();
        this.classList = new DlDsAutoAssignTaskClassList();
        this.invocationMode = 0;
        this.objectNameMatchInclude = "";
        this.datatypeMatch = "";
        this.scheduleDefn = "{}";
        this.overwrite = Boolean.FALSE;
    }

    /**
     * Constructor 02 - all scalar parameters.
     * @param taskId
     * @param taskName
     * @param taskShortDesc
     * @param invocationMode
     * @param objectNameMatchInclude
     * @param datatypeMatch
     */
    public DlDataSensitivityAutoAssignTask(Integer taskId, String taskName, String taskShortDesc,
                                           Integer invocationMode, String objectNameMatchInclude, String datatypeMatch) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.taskShortDesc = taskShortDesc;
        this.invocationMode = invocationMode;
        this.objectNameMatchInclude = objectNameMatchInclude;
        this.objectNameMatchExclude = "";
        this.datatypeMatch = datatypeMatch;
        this.dsTag = new DlDataSensitivityTag();
        this.classList = new DlDsAutoAssignTaskClassList();
        this.scheduleDefn = "{}";
        this.overwrite = Boolean.FALSE;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskShortDesc() {
        return taskShortDesc;
    }

    public void setTaskShortDesc(String taskShortDesc) {
        this.taskShortDesc = taskShortDesc;
    }

    public DlDataSensitivityTag getDsTag() {
        return dsTag;
    }

    public void setDsTag(DlDataSensitivityTag dsTag) {
        this.dsTag = dsTag;
    }

    public DlDsAutoAssignTaskClassList getClassList() {
        return classList;
    }

    public void setClassList(DlDsAutoAssignTaskClassList classList) {
        this.classList = classList;
    }

    public void parseJsonClassIdList(String jsonClassIdList) {

    }

    public Integer getInvocationMode() {
        return invocationMode;
    }

    public void setInvocationMode(Integer invocationMode) {
        this.invocationMode = invocationMode;
    }

    public String getObjectNameMatchInclude() {
        return objectNameMatchInclude;
    }

    public void setObjectNameMatchInclude(String objectNameMatchInclude) {
        this.objectNameMatchInclude = objectNameMatchInclude;
    }

    public String getObjectNameMatchExclude() {
        return objectNameMatchExclude;
    }

    public void setObjectNameMatchExclude(String objectNameMatchExclude) {
        this.objectNameMatchExclude = objectNameMatchExclude;
    }

    public String getDatatypeMatch() {
        return datatypeMatch;
    }

    public void setDatatypeMatch(String datatypeMatch) {
        this.datatypeMatch = datatypeMatch;
    }

    public String getScheduleDefn() {
        return scheduleDefn;
    }

    public void setScheduleDefn(String scheduleDefn) {
        this.scheduleDefn = scheduleDefn;
    }

    public Boolean getOverwrite() {
        return overwrite;
    }

    public void setOverwrite(Boolean overwrite) {
        this.overwrite = overwrite;
    }
}
