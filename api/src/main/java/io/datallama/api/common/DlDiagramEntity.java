/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class DlDiagramEntity extends DlEntity {
    private Integer x;
    private Integer y;
    private Integer height;
    private Integer width;
    private String colorDefinition;

    public DlDiagramEntity(Integer entityId, Integer classId, String entityName, String shortDescription,
                           Integer x, Integer y, Integer height, Integer width, String colorDefinition) {

        super(entityId, classId, entityName, shortDescription);
        this.height = height;
        this.width = width;
        this.x = x;
        this.y = y;
        this.colorDefinition = colorDefinition;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public String getColorDefinition() {
        return colorDefinition;
    }

    public void setColorDefinition(String colorDefinition) {
        this.colorDefinition = colorDefinition;
    }
}
