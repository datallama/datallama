/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class MoCustomFormAssoc {
    private Integer objectId;
    private Integer classId;
    private Integer customFormId;

    public MoCustomFormAssoc() {
        this.objectId = 0;
        this.classId = 0;
        this.customFormId = 0;
    }

    public MoCustomFormAssoc(Integer objectId, Integer classId, Integer customFormId) {
        this.objectId = objectId;
        this.classId = classId;
        this.customFormId = customFormId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }
}
