/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DlDsAutoAssignRunReport {
    private Integer taskRunId;
    private Integer taskId;
    private String taskRunDt;
    private Integer userId;
    private String userLogin;
    private Integer invocationMode;
    private Map<String, Integer> updateDetails;
    private ObjectMapper objectMapper;

    /**
     * Default Constructor. No parameters
     */
    public DlDsAutoAssignRunReport() {
        this.taskRunId = 0;
        this.taskId = 0;
        this.taskRunDt = "";
        this.userId = 0;
        this.userLogin = "";
        this.invocationMode = 0;
        this.updateDetails = new HashMap<>();
        this.objectMapper = new ObjectMapper();
    }

    public Integer getTaskRunId() {
        return taskRunId;
    }

    public void setTaskRunId(Integer taskRunId) {
        this.taskRunId = taskRunId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getTaskRunDt() {
        return taskRunDt;
    }

    public void setTaskRunDt(String taskRunDt) {
        this.taskRunDt = taskRunDt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Integer getInvocationMode() {
        return invocationMode;
    }

    public void setInvocationMode(Integer invocationMode) {
        this.invocationMode = invocationMode;
    }

    public Map<String, Integer> getUpdateDetails() {
        return updateDetails;
    }

    public void setUpdateDetails(Map<String, Integer> updateDetails) {
        this.updateDetails = updateDetails;
    }

    public void addUpdateDetail(String detailKey, Integer detailValue) {
        this.updateDetails.put(detailKey, detailValue);
    }

    public String getUpdateDetailsAsJsonString() {
        String updateDetailsJsonString;
        try {
            updateDetailsJsonString = objectMapper.writeValueAsString(updateDetails);
        } catch (JsonProcessingException e) {
            updateDetailsJsonString = e.getMessage();
        }
        return updateDetailsJsonString;
    }
}
