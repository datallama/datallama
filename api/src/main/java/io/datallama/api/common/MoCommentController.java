/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class MoCommentController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /comment
     */
    @RequestMapping(method = GET, value = "/comment/{objectId}/{classId}")
    @ResponseBody
    public Map<String, Object> readObjectComment(@PathVariable Integer objectId, @PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList commentList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");
        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap commentInfo = (LinkedCaseInsensitiveMap) commentDetails;
            // first, create the MoComment instance
            MoComment moComment = new MoComment(
                    (Integer) commentInfo.get("user_id"),
                    (Integer) commentInfo.get("comment_id"),
                    (Integer) commentInfo.get("object_id"),
                    (Integer) commentInfo.get("class_id"),
                    commentInfo.get("last_change_ts").toString(),
                    commentInfo.get("comment_text").toString()
            );
            moComment.setUsername(commentInfo.get("user_login").toString());
            commentList.add(moComment);
        }

        results.put("result_data", commentList);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/comment")
    public Map<String, Object> writeObjectComment(@RequestBody MoComment moComment, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_object_comment");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id",       Integer.parseInt(userId),      Types.INTEGER)
                .addValue("_comment_id",    moComment.getCommentId(),   Types.INTEGER)
                .addValue("_object_id",     moComment.getObjectId(),    Types.INTEGER)
                .addValue("_class_id",      moComment.getClassId(),     Types.INTEGER)
                .addValue("_comment_text",  moComment.getCommentText(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap newID = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("returnID", newID.get("returnID"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

}