/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

public class DlCustomFormEnumAssoc {
    private Integer enumAssocId = 0;
    private Integer customFormId = 0;
    private Integer assocObjectId = 0;
    private Integer assocClassId = 0;
    private String enumAssocName = "";
    private String enumAssocShortDesc = "";
    private String assocObjectName = "";

    /**
     * Default no-argument constructor.
     */
    public DlCustomFormEnumAssoc() {
        this.enumAssocId = 0;
        this.customFormId = 0;
        this.assocObjectId = 0;
        this.assocClassId = 0;
        this.enumAssocName = "";
        this.enumAssocShortDesc = "";
        this.assocObjectName = "";
    }

    /**
     * Database result set constructor.
     * @param enumAssocId
     * @param assocObjectId
     * @param assocClassId
     * @param enumAssocName
     * @param enumAssocShortDesc
     */
    public DlCustomFormEnumAssoc(Integer enumAssocId, Integer assocObjectId, Integer assocClassId,
                                 String enumAssocName, String enumAssocShortDesc, String assocObjectName) {
        this.enumAssocId = enumAssocId;
        this.customFormId = 0;
        this.assocObjectId = assocObjectId;
        this.assocClassId = assocClassId;
        this.enumAssocName = enumAssocName;
        this.enumAssocShortDesc = enumAssocShortDesc;
        this.assocObjectName = assocObjectName;
    }

    public Integer getEnumAssocId() {
        return enumAssocId;
    }

    public void setEnumAssocId(Integer enumAssocId) {
        this.enumAssocId = enumAssocId;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public Integer getAssocObjectId() {
        return assocObjectId;
    }

    public void setAssocObjectId(Integer assocObjectId) {
        this.assocObjectId = assocObjectId;
    }

    public Integer getAssocClassId() {
        return assocClassId;
    }

    public void setAssocClassId(Integer assocClassId) {
        this.assocClassId = assocClassId;
    }

    public String getEnumAssocName() {
        return enumAssocName;
    }

    public void setEnumAssocName(String enumAssocName) {
        this.enumAssocName = enumAssocName;
    }

    public String getEnumAssocShortDesc() {
        return enumAssocShortDesc;
    }

    public void setEnumAssocShortDesc(String enumAssocShortDesc) {
        this.enumAssocShortDesc = enumAssocShortDesc;
    }

    public String getAssocObjectName() {
        return assocObjectName;
    }

    public void setAssocObjectName(String assocObjectName) {
        this.assocObjectName = assocObjectName;
    }
}
