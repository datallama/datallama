/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import io.datallama.api.metamodel.DlMetaDataContextNode;
import java.util.ArrayList;

public class RevEngReconciliation {
    private Integer objectId;
    private Integer classId;
    private String objectName;
    private Integer parentObjectId;
    private Integer parentClassId;
    private String parentObjectName;
    private Integer reconciledObjectId;
    private Integer reconciledClassId;
    private String reconciledObjectName;
    private Integer revengRunId;
    private Integer revengRunCount;
    private Integer reconciliationStatus;
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Default no-parameter constructor
     */
    public RevEngReconciliation() {
        this.objectId = 0;
        this.classId = 0;
        this.objectName = "";
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentObjectName = "";
        this.reconciledObjectId = 0;
        this.reconciledClassId = 0;
        this.reconciledObjectName = "";
        this.revengRunId = 0;
        this.revengRunCount = 0;
        this.reconciliationStatus = 0;
        this.mdContext =  new ArrayList<DlMetaDataContextNode>();
    }

    /**
     * Constructor without parent object parameters
     * @param objectId
     * @param classId
     * @param objectName
     * @param revengRunId
     * @param revengRunCount
     */
    public RevEngReconciliation(Integer objectId, Integer classId, String objectName, Integer revengRunId, Integer revengRunCount) {
        this.objectId = objectId;
        this.classId = classId;
        this.objectName = objectName;
        this.revengRunId = revengRunId;
        this.revengRunCount = revengRunCount;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentObjectName = "";
        this.reconciledObjectId = 0;
        this.reconciledClassId = 0;
        this.reconciledObjectName = "";
        this.reconciliationStatus = 0;
        this.mdContext =  new ArrayList<DlMetaDataContextNode>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getParentObjectName() {
        return parentObjectName;
    }

    public void setParentObjectName(String parentObjectName) {
        this.parentObjectName = parentObjectName;
    }

    public Integer getReconciledObjectId() {
        return reconciledObjectId;
    }

    public void setReconciledObjectId(Integer reconciledObjectId) {
        this.reconciledObjectId = reconciledObjectId;
    }

    public Integer getReconciledClassId() {
        return reconciledClassId;
    }

    public void setReconciledClassId(Integer reconciledClassId) {
        this.reconciledClassId = reconciledClassId;
    }

    public String getReconciledObjectName() {
        return reconciledObjectName;
    }

    public void setReconciledObjectName(String reconciledObjectName) {
        this.reconciledObjectName = reconciledObjectName;
    }

    public Integer getRevengRunId() {
        return revengRunId;
    }

    public void setRevengRunId(Integer revengRunId) {
        this.revengRunId = revengRunId;
    }

    public Integer getRevengRunCount() {
        return revengRunCount;
    }

    public void setRevengRunCount(Integer revengRunCount) {
        this.revengRunCount = revengRunCount;
    }

    public Integer getReconciliationStatus() {
        return reconciliationStatus;
    }

    public void setReconciliationStatus(Integer reconciliationStatus) {
        this.reconciliationStatus = reconciliationStatus;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }
}