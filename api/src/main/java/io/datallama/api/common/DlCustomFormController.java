/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class DlCustomFormController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Reads from the DL DB details of the custom form identified by {customFormId}
     *
     * @param customFormId
     * @return
     */
    @RequestMapping(method = GET, value = "/common/customform/{customFormId}")
    @ResponseBody
    public Map<String, Object> readCustomForm(@PathVariable Integer customFormId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_custom_form_id", customFormId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm(
                customFormId,
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );

        customForm.prepareSchemaDefinition(dataSource);

        results.put("result_data", customForm);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL DB the custom form data for a metadata object identified by objectId and classId
     *
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/common/customform/data/{classId}/{objectId}")
    @ResponseBody
    public Map<String, Object> readMdObjectCustomFormData(@PathVariable Integer classId, @PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();

        customForm.setObjectId(objectId);
        customForm.setClassId(classId);
        customForm.setParentClassId((Integer) out.get("_parent_class_id"));
        customForm.setParentObjectId((Integer) out.get("_parent_object_id"));
        customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
        customForm.setFormData((String) out.get("_custom_form_data"));

        resultData.put("custom_form", customForm);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes to the DL database the custom form data for the metadata object identified by classId and objectId
     *
     * @param customForm
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/customform/data")
    @ResponseBody
    public Map<String, Object> writeMdObjectCustomFormData(@RequestBody DlCustomForm customForm, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        // 1. ensure all mandatory custom form attributes have been supplied
        if (!customForm.minimumValidity()) {
            resultStatus = 90901;
        } else {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_custom_form_data");
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_class_id", customForm.getClassId(), Types.INTEGER)
                    .addValue("_object_id", customForm.getObjectId(), Types.INTEGER)
                    .addValue("_parent_class_id", customForm.getParentClassId(), Types.INTEGER)
                    .addValue("_parent_object_id", customForm.getParentObjectId(), Types.INTEGER)
                    .addValue("_custom_form_id", customForm.getCustomFormId(), Types.INTEGER)
                    .addValue("_custom_form_data", customForm.getFormData(), Types.VARCHAR)
                    .addValue("_last_change_user_id", Integer.parseInt(userId), Types.INTEGER);

            Map<String, Object> out = jdbcCall.execute(in);
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL DB the metadata objects associated with a customform to be used as sources of enum values.
     *
     * @param customFormId
     * @return
     */
    @RequestMapping(method = GET, value = "/common/customform/enumassoc/{customFormId}")
    @ResponseBody
    public Map<String, Object> readCustomFormEnumAssoc(@PathVariable Integer customFormId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList associatedLists = new ArrayList(); // TODO: eventually the association should support MD Objects other than Ontology Lists

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_enum_assoc");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_custom_form_id", customFormId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap assocListInfo = (LinkedCaseInsensitiveMap) listElement;
            DlCustomFormEnumAssoc enumAssoc = new DlCustomFormEnumAssoc(
                    (Integer) assocListInfo.get("enum_assoc_id"),
                    (Integer) assocListInfo.get("assoc_object_id"),
                    (Integer) assocListInfo.get("assoc_class_id"),
                    assocListInfo.get("enum_assoc_name").toString(),
                    assocListInfo.get("enum_assoc_shortdesc").toString(),
                    assocListInfo.get("assoc_object_name").toString()
                    );

            associatedLists.add(enumAssoc);
        }

        results.put("result_data", associatedLists);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves to the DL DB an association between a Custom Form and an Ontology List
     * @param customFormEnumAssoc
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/common/customform/enumassoc")
    @ResponseBody
    public Map<String, Object> writeCustomFormEnumAssoc(@RequestBody DlCustomFormEnumAssoc customFormEnumAssoc, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_custom_form_enum_assoc");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_enum_assoc_id", customFormEnumAssoc.getEnumAssocId(), Types.INTEGER)
                .addValue("_custom_form_id", customFormEnumAssoc.getCustomFormId(), Types.INTEGER)
                .addValue("_assoc_object_id", customFormEnumAssoc.getAssocObjectId(), Types.INTEGER)
                .addValue("_assoc_class_id", customFormEnumAssoc.getAssocClassId(), Types.INTEGER)
                .addValue("_enum_assoc_shortdesc", customFormEnumAssoc.getEnumAssocShortDesc().toString(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("returnId", (Integer) out.get("_return_id"));
        resultData.put("enumAssocName", out.get("_enum_assoc_name").toString());
        resultData.put("assocObjectName", out.get("_assoc_object_name").toString());
        resultData.put("enumAssocShortDesc", customFormEnumAssoc.getEnumAssocShortDesc());

        if ((Integer) out.get("_return_id") == 0) {
            resultStatus = -1;
            resultData.put("errorMsg", "This association already exists. Nothing to do.");
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Reads from the DL DB details of the custom form identified by {customFormId}
     *
     * @param customFormId
     * @return
     */
    @RequestMapping(method = GET, value = "/common/customform02/{customFormId}")
    @ResponseBody
    public Map<String, Object> readCustomForm02(@PathVariable Integer customFormId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_custom_form_id", customFormId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm(
                customFormId,
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);

        results.put("result_data", customForm);
        results.put("result_status", resultStatus);

        return results;
    }

}