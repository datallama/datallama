/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.admin;

// TODO: this class needs to be removed and the DlCustomForm class used its place.
public class CustomForm {
    private Integer customFormId;
    private Integer classId;
    private String customFormName;
    private String customFormShortDesc;
    private String schemaDefinition;
    private String formDefinition;

    public CustomForm() {
        customFormId = 0;
        classId = 0;
        customFormName = "";
        customFormShortDesc = "";
        schemaDefinition = "";
        formDefinition = "";
    }

    public CustomForm(Integer customFormId, Integer classId, String customFormName, String customFormShortDesc) {
        this.customFormId = customFormId;
        this.classId = classId;
        this.customFormName = customFormName;
        this.customFormShortDesc = customFormShortDesc;
        schemaDefinition = "";
        formDefinition = "";
    }

    public CustomForm(Integer customFormId, Integer classId, String customFormName,
                      String customFormShortDesc, String schemaDefinition, String formDefinition) {
        this.customFormId = customFormId;
        this.classId = classId;
        this.customFormName = customFormName;
        this.customFormShortDesc = customFormShortDesc;
        this.schemaDefinition = schemaDefinition;
        this.formDefinition = formDefinition;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getCustomFormName() {
        return customFormName;
    }

    public void setCustomFormName(String customFormName) {
        this.customFormName = customFormName;
    }

    public String getCustomFormShortDesc() {
        return customFormShortDesc;
    }

    public void setCustomFormShortDesc(String customFormShortDesc) {
        this.customFormShortDesc = customFormShortDesc;
    }

    public String getSchemaDefinition() {
        return schemaDefinition;
    }

    public void setSchemaDefinition(String schemaDefinition) {
        this.schemaDefinition = schemaDefinition;
    }

    public String getFormDefinition() {
        return formDefinition;
    }

    public void setFormDefinition(String formDefinition) {
        this.formDefinition = formDefinition;
    }
}
