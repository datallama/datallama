/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.admin;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.association.MdAssociationType;
import io.datallama.api.common.DlCustomFormEnumAssoc;
import io.datallama.api.common.DlMoClass;
import io.datallama.api.security.DlGroup;
import io.datallama.api.security.DlGroupPermission;
import io.datallama.api.security.DlUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class AdminController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Returns the list of all customforms defined in Data Llama
     * URL Path: /admin/customform/list
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/customform/list")
    @ResponseBody
    public Map<String, Object> listCustomForms() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList customFormList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // second: create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listForm : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap formInfo = (LinkedCaseInsensitiveMap) listForm;
            CustomForm customForm = new CustomForm(
                    (Integer) formInfo.get("custom_form_id"),
                    (Integer) formInfo.get("class_id"),
                    formInfo.get("custom_form_name").toString(),
                    formInfo.get("custom_form_shortdesc").toString()
            );
            customFormList.add(customForm);
        }

        results.put("result_data", customFormList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the JSON definition of the custom form identified by {customFormId}
     * URL Path: /admin/customform/content/{customFormId}
     *
     * @param customFormId
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/customform/content/{customFormId}")
    @ResponseBody
    public Map<String, Object> readCustomFormContent(@PathVariable Integer customFormId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. read from the DL DB the core details of the custom form
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_custom_form_id", customFormId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        CustomForm customForm = new CustomForm(
                customFormId,
                (Integer) out.get("_class_id"),
                out.get("_custom_form_name").toString(),
                out.get("_custom_form_shortdesc").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );

        results.put("core_details", customForm);

        // 2. read from the DL DB the set of Ontology Lists associated with the custom form as enumeration attribute domains
        ArrayList associatedLists = new ArrayList(); // TODO: eventually the association should support MD Objects other than Ontology Lists

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_enum_assoc");
        out = jdbcCall.execute(in);

        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap assocListInfo = (LinkedCaseInsensitiveMap) listElement;
            DlCustomFormEnumAssoc enumAssoc = new DlCustomFormEnumAssoc(
                    (Integer) assocListInfo.get("enum_assoc_id"),
                    (Integer) assocListInfo.get("assoc_object_id"),
                    (Integer) assocListInfo.get("assoc_class_id"),
                    assocListInfo.get("enum_assoc_name").toString(),
                    assocListInfo.get("enum_assoc_shortdesc").toString(),
                    assocListInfo.get("assoc_object_name").toString()
            );
            associatedLists.add(enumAssoc);
        }

        results.put("enum_assoc", associatedLists);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /admin/customform/content/detail
     * Writes the JSON definitions for a custom form definition into DL database table dl_custom_form
     *
     * @param customForm
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/customform/content/detail")
    @ResponseBody
    public Map<String, Object> writeCustomFormContentDetail(@RequestBody CustomForm customForm, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_custom_form_detail");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_custom_form_id", customForm.getCustomFormId(), Types.INTEGER)
                .addValue("_schema_definition", customForm.getSchemaDefinition(), Types.VARCHAR)
                .addValue("_form_definition", customForm.getFormDefinition(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        // TODO: should return something
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /admin/customform/content
     * Create a new custom form definition in DL database table dl_custom_form
     *
     * @param customForm
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/customform/content")
    @ResponseBody
    public Map<String, Object> writeCustomFormContent(@RequestBody CustomForm customForm, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        System.out.println(customForm.getCustomFormId());
        System.out.println(customForm.getSchemaDefinition());

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_custom_form");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_custom_form_id", customForm.getCustomFormId(), Types.INTEGER)
                .addValue("_class_id", customForm.getClassId(), Types.INTEGER)
                .addValue("_custom_form_name", customForm.getCustomFormName(), Types.VARCHAR)
                .addValue("_custom_form_shortdesc", customForm.getCustomFormShortDesc(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("returnId", (Integer) out.get("_class_id"));
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/admin/assoctype/content")
    @ResponseBody
    public Map<String, Object> writeAssocTypeContent(@RequestBody MdAssociationType associationType, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_association_type");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_assoc_type_id", associationType.getAssocTypeId(), Types.INTEGER)
                .addValue("_assoc_type_name", associationType.getAssocTypeName(), Types.VARCHAR)
                .addValue("_assoc_type_shortdesc", associationType.getAssocTypeShortDesc(), Types.VARCHAR)
                .addValue("_source_phrase", associationType.getSourcePhrase(), Types.VARCHAR)
                .addValue("_target_phrase", associationType.getTargetPhrase(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("returnId", out.get("_class_id"));
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the list of all users and all groups defined in Data Llama
     * URL Path: /admin/auth/list_users_groups
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/auth/list_users_groups")
    @ResponseBody
    public Map<String, Object> listUsersGroups() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList userList = new ArrayList();
        ArrayList groupList = new ArrayList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_users");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // second: create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap userInfo = (LinkedCaseInsensitiveMap) listElement;
            DlUser user = new DlUser(
                    (Integer) userInfo.get("user_id"),
                    userInfo.get("user_login").toString(),
                    userInfo.get("firstname").toString(),
                    userInfo.get("lastname").toString(),
                    (Boolean) userInfo.get("enabled")
            );
            user.setGroupList(userInfo.get("user_groups").toString());
            userList.add(user);
        }

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_groups");
        out = jdbcCall.execute();
        dbResultList = (ArrayList) out.get("#result-set-1");

        // second: create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap groupInfo = (LinkedCaseInsensitiveMap) listElement;
            DlGroup group = new DlGroup(
                    (Integer) groupInfo.get("group_id"),
                    groupInfo.get("group_name").toString(),
                    groupInfo.get("group_shortdesc").toString()
            );
            groupList.add(group);
        }

        resultData.put("users", userList);
        resultData.put("groups", groupList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the details of the user account identified by {userId}, including the groups to which the user is assigned.
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/auth/user/{userId}")
    @ResponseBody
    public Map<String, Object> readUserDetails(@PathVariable Integer userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList groupList = new ArrayList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        // fist: process the core details of the user
        DlUser user = new DlUser(
            userId,
            out.get("_username").toString(),
            out.get("_firstname").toString(),
            out.get("_lastname").toString(),
            (Integer) out.get("_enabled")
        );
        resultData.put("core", user);

        // second: process the group assignments for the user
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_groups");
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap groupInfo = (LinkedCaseInsensitiveMap) listElement;
            DlGroup group = new DlGroup(
                    (Integer) groupInfo.get("group_id"),
                    groupInfo.get("group_name").toString(),
                    groupInfo.get("group_shortdesc").toString()
            );
            group.setAssigned((Boolean) groupInfo.get("assigned"));
            groupList.add(group);
        }

        resultData.put("groupAssignment", groupList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Creates or updates a user definition in the DL databsae.
     * @param targerUser
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/auth/user")
    @ResponseBody
    public Map<String, Object> writeUser(@RequestBody DlUser targerUser, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_user");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_target_user_id", targerUser.getUserId(), Types.INTEGER)
                .addValue("_user_login", targerUser.getUsername(), Types.VARCHAR)
                .addValue("_firstname", targerUser.getFirstname(), Types.VARCHAR)
                .addValue("_lastname", targerUser.getLastname(), Types.VARCHAR)
                .addValue("_enabled", targerUser.getEnabled(), Types.BOOLEAN)
                .addValue("_email_address", targerUser.getEmailAddress(), Types.VARCHAR)
                .addValue("_password", targerUser.getPassword(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnId", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Creates or updates the list of groups to which a user is assigned
     * @param userGroupList
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/auth/user/grouplist")
    @ResponseBody
    public Map<String, Object> writeUserGroupList(@RequestBody UserGroupList userGroupList, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_user_groups");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_target_user_id", userGroupList.getUserId(), Types.INTEGER)
                .addValue("_group_list", userGroupList.getGroupListAsString(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves a password reset for the target user Id into the DL database
     * @param userPasswordReset
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/auth/user/passwordreset")
    @ResponseBody
    public Map<String, Object> writeUserPasswordReset(@RequestBody UserPasswordReset userPasswordReset, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_user_password_reset");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_target_user_id", userPasswordReset.getUserId(), Types.INTEGER)
                .addValue("_new_password", userPasswordReset.getNewPassword(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Creates or updates a group definition in the DL database
     * @param group
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/auth/group")
    @ResponseBody
    public Map<String, Object> writeGroup(@RequestBody DlGroup group, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_group");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_group_id", group.getGroupId(), Types.INTEGER)
                .addValue("_group_name", group.getGroupName(), Types.VARCHAR)
                .addValue("_group_shortdesc", group.getGroupShortDesc(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnId", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Creates or updates a group definition in the DL database
     * @param group
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/auth/group/permissions")
    @ResponseBody
    public Map<String, Object> writeGroupPermissions(@RequestBody DlGroup group, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        System.out.println(group.getGroupPermissionsAsString());

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_group_permissions");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_group_id", group.getGroupId(), Types.INTEGER)
                .addValue("_group_permissions", group.getGroupPermissionsAsString(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the details of the user account identified by {userId}, including the groups to which the user is assigned.
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/auth/group/{groupId}")
    @ResponseBody
    public Map<String, Object> readGroupDetails(@PathVariable Integer groupId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList groupList = new ArrayList();

        // first, get the summary information for the group.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_group");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_group_id", groupId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        // fist: process the core details of the user
        DlGroup group = new DlGroup(
                groupId,
                out.get("_group_name").toString(),
                out.get("_group_shortdesc").toString()
        );
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap groupPermList = (LinkedCaseInsensitiveMap) listElement;
            DlGroupPermission groupPerm = new DlGroupPermission(
                    (Integer) groupPermList.get("module_id"),
                    groupPermList.get("module_code").toString(),
                    (Integer) groupPermList.get("action_category_id"),
                    groupPermList.get("action_category_name").toString(),
                    (Boolean) groupPermList.get("access_granted")
            );
            groupPerm.setModuleName(groupPermList.get("module_name").toString());
            group.addGroupPermission(groupPerm);
        }

        resultData.put("group", group);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the list of all classes from the DL database
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/class/list")
    @ResponseBody
    public Map<String, Object> readClassList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dlMoClassList = new ArrayList();

        // first, get the summary information for the group.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_class_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");


        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementMap = (LinkedCaseInsensitiveMap) listElement;
            DlMoClass dlMoClass = new DlMoClass(
                    (Integer) elementMap.get("class_id"),
                    elementMap.get("class_name").toString(),
                    elementMap.get("class_shortdesc").toString(),
                    (Integer) elementMap.get("custom_form_id"),
                    elementMap.get("custom_form_name").toString()
            );
            dlMoClassList.add(dlMoClass);
        }
        resultData.put("classList", dlMoClassList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL database details of the metadata object class identified by {classId}
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/admin/class/detail/{classId}")
    @ResponseBody
    public Map<String, Object> readClassDetail(@PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // first, get the summary information for the group.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_class_detail");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlMoClass dlMoClass = new DlMoClass(
                classId,
                out.get("_class_name").toString(),
                out.get("_class_desc").toString(),
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString()
        );

        results.put("result_data", dlMoClass);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL database details of the metadata object class identified by {classId}
     * @param moClass
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/admin/class/detail")
    @ResponseBody
    public Map<String, Object> writeClassDetail(@RequestBody DlMoClass moClass, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // first, get the summary information for the group.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_class_detail");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", moClass.getClassId(), Types.INTEGER)
                .addValue("_class_desc", moClass.getClassShortDesc(), Types.VARCHAR)
                .addValue("_custom_form_id", moClass.getCustomFormId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * The UserGroupList internal class is used to capture the list of user group assingments received from the GUI
     */
    static class UserGroupList {
        private Integer userId;
        private ArrayList<Object> groupList;

        public UserGroupList() {
            this.userId = 0;
            this.groupList = new ArrayList<Object>();
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public ArrayList<Object> getGroupList() {
            return groupList;
        }

        public void setGroupList(ArrayList<Object> groupList) {
            this.groupList = groupList;
        }

        public String getGroupListAsString() {
            ObjectMapper objectMapper = new ObjectMapper(); // used to diagram properties Object as JSON string
            String groupList = new String();
            try {
                groupList = objectMapper.writeValueAsString(this.groupList);
            }
            catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return groupList;
        }

    }

    static class UserPasswordReset {
        private Integer userId;
        private String newPassword;

        public UserPasswordReset() {
            this.userId = 0;
            this.newPassword = "";
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }
    }
}