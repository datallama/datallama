/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.metamodel;

public class DlMetaModelClass {
    private Integer classId;
    private Integer metaClassId;
    private String className;
    private String classShortDesc;
    private String objectTable;
    private String objectDetailState;

    public DlMetaModelClass() {
        this.classId = 0;
        this.metaClassId = 0;
        this.className = "";
        this.classShortDesc = "";
        this.objectTable = "";
        this.objectDetailState = "";
    }

    public DlMetaModelClass(Integer classId, Integer metaClassId, String className, String classShortDesc) {
        this.classId = classId;
        this.metaClassId = metaClassId;
        this.className = className;
        this.classShortDesc = classShortDesc;
        this.objectTable = "";
        this.objectDetailState = "";
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getMetaClassId() {
        return metaClassId;
    }

    public void setMetaClassId(Integer metaClassId) {
        this.metaClassId = metaClassId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassShortDesc() {
        return classShortDesc;
    }

    public void setClassShortDesc(String classShortDesc) {
        this.classShortDesc = classShortDesc;
    }

    public String getObjectTable() {
        return objectTable;
    }

    public void setObjectTable(String objectTable) {
        this.objectTable = objectTable;
    }

    public String getObjectDetailState() {
        return objectDetailState;
    }

    public void setObjectDetailState(String objectDetailState) {
        this.objectDetailState = objectDetailState;
    }
}
