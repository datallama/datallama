/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.metamodel;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin
@RestController
public class DlMetaModelController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /metamodel/object/list
     */
    @RequestMapping(method = GET, value = "/metamodel/object/list/{classId}")
    @ResponseBody
    public Map<String, Object> listMetaModelObjects(@PathVariable Long classId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList objectList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_mmobject_list");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        for (Object listObject : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) listObject;
            // first, create the DlMetaModelObject instance
            DlMetaModelObject mmObject = new DlMetaModelObject(
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    (Integer) objectInfo.get("parent_object_id"),
                    objectInfo.get("object_name").toString(),
                    objectInfo.get("class_name").toString(),
                    objectInfo.get("parent_object_name").toString(),
                    objectInfo.get("object_shortdesc").toString()
            );

            // second, add the metadata context
            List<LinkedHashMap> contextList = null;
            try {
                contextList = objectMapper.readValue((String) objectInfo.get("md_context"), List.class);
                for(LinkedHashMap contextNode:contextList) {
                    DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                            Integer.parseInt(contextNode.get("class_id").toString()),
                            contextNode.get("class_name").toString(),
                            Integer.parseInt(contextNode.get("object_id").toString()),
                            contextNode.get("object_name").toString()
                    );
                    if(contextNode.containsKey("internal_path")) {
                        mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                    }
                    else {
                        mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                    }
                    mmObject.addMdContextNode(mdContextNode);
                }
            }
            catch (Exception e) {
                System.out.println("Exception thrown while parsing md_context JSON array.");
                System.out.println(e);
            }


            objectList.add(mmObject);

        }

        results.put("result_data", objectList);
        results.put("result_status", resultStatus);

        return results;
    }

}
