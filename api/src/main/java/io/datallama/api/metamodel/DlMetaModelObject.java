/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.metamodel;

import java.util.ArrayList;
import io.datallama.api.metamodel.DlMetaDataContextNode;

public class DlMetaModelObject {
    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private String objectName;
    private String className;
    private String parentObjectName;
    private String objectShortDesc;
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Description: default no-parameter constructor
     */
    public DlMetaModelObject() {
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.objectName = "";
        this.className = "";
        this.parentObjectName = "";
        this.objectShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    /**
     * Description: full constructor with all parameters specified
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param objectName
     * @param className
     * @param parentObjectName
     * @param objectShortDesc
     */
    public DlMetaModelObject(Integer objectId, Integer classId, Integer parentObjectId, String objectName,
                             String className, String parentObjectName, String objectShortDesc) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = 0;
        this.objectName = objectName;
        this.className = className;
        this.parentObjectName = parentObjectName;
        this.objectShortDesc = objectShortDesc;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getParentObjectName() {
        return parentObjectName;
    }

    public void setParentObjectName(String parentObjectName) {
        this.parentObjectName = parentObjectName;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }
}
