/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.metamodel;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class DlMetaDataContext {
    private String mdContext;
    private ArrayList<DlMetaDataContextNode> mdContextList;

    public DlMetaDataContext() {
        this.mdContext = "";
        this.mdContextList = new ArrayList<DlMetaDataContextNode>();
    }

    public DlMetaDataContext(String mdContext) {
        this.mdContext = mdContext;
        this.mdContextList = new ArrayList<DlMetaDataContextNode>();
    }

    public void setMdContext(String mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContextList.add(mdContextNode);
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContextList;
    }

    public ArrayList<DlMetaDataContextNode> generateMdContextList(String mdContext) {
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the md_context json array returned by stored procedure
        ArrayList<DlMetaDataContextNode> mdContextList = new ArrayList();
        List<LinkedHashMap> contextList = null;
        Integer parentObjectId = 0;
        Integer parentClassId = 0;

        try {
            contextList = objectMapper.readValue(mdContext, List.class);

            for(LinkedHashMap contextNode: contextList) {
                DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                        Integer.parseInt(contextNode.get("class_id").toString()),
                        contextNode.get("class_name").toString(),
                        Integer.parseInt(contextNode.get("object_id").toString()),
                        contextNode.get("object_name").toString()
                );
                if(contextNode.containsKey("internal_path")) {
                    mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                }
                else {
                    mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                }
                mdContextNode.setParentObjectId(parentObjectId);
                mdContextNode.setParentClassId(parentClassId);
                parentObjectId = mdContextNode.getObjectId();
                parentClassId = mdContextNode.getClassId();

                mdContextList.add(mdContextNode);
            }
        }
        catch (Exception e) {
            System.out.println("Exception thrown while parsing md_context JSON array.");
            System.out.println(e);
        }

        return(mdContextList);
    }
}
