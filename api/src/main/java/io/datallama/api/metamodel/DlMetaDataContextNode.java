/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.metamodel;

public class DlMetaDataContextNode {
    private Integer classId;
    private String className;
    private Integer objectId;
    private String objectName;
    private Integer parentObjectId;
    private Integer parentClassId;
    private String internalPath;

    public DlMetaDataContextNode() {
        this.classId = 0;
        this.className = "";
        this.objectId = 0;
        this.objectName = "";
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.internalPath = "";
    }

    public DlMetaDataContextNode(Integer classId, String className, Integer objectId, String objectName) {
        this.classId = classId;
        this.className = className;
        this.objectId = objectId;
        this.objectName = objectName;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.internalPath = "";
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getInternalPath() {
        return internalPath;
    }

    public void setInternalPath(String internalPath) {
        this.internalPath = internalPath;
    }
}
