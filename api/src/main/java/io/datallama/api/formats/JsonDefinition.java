/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import java.util.ArrayList;

public class JsonDefinition {
    private Integer objectId;
    private Integer classId;
    private String defnName;
    private String defnDescription;
    private ArrayList<JsonElement> elementList;

    /**
     * Default constructor with no parameters.
     */
    public JsonDefinition() {
        this.objectId = 0;
        this.classId = 0;
        this.defnName = "";
        this.defnDescription = "";
        this.elementList = new ArrayList<JsonElement>();
    }

    /**
     * Constructor 2 with only the JSON Definition Name as constructor
     * @param defnName
     */
    public JsonDefinition(String defnName) {
        this.objectId = 0;
        this.classId = 0;
        this.defnName = defnName;
        this.defnDescription = "";
        this.elementList = new ArrayList<JsonElement>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getDefnName() {
        return defnName;
    }

    public void setDefnName(String defnName) {
        this.defnName = defnName;
    }

    public String getDefnDescription() {
        return defnDescription;
    }

    public void setDefnDescription(String defnDescription) {
        this.defnDescription = defnDescription;
    }

    public ArrayList<JsonElement> getElementList() {
        return elementList;
    }

    public void setElementList(ArrayList<JsonElement> elementList) {
        this.elementList = elementList;
    }

    public void addElement(JsonElement element) {
        this.elementList.add(element);
    }

}
