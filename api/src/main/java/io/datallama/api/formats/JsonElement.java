/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import java.util.ArrayList;

public class JsonElement {
    private Integer userId;
    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private Integer parentElementObjectId;
    private String objectGUID;
    private String elementName;
    private String elementType;
    private String objectConfig;
    private String elementDescription;
    private String elementPath;
    private ArrayList<JsonElement> elementList;

    /**
     *
     */
    public JsonElement() {
        this.userId = 0;
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentElementObjectId = 0;
        this.objectGUID = "";
        this.elementName = "";
        this.elementType = "";
        this.objectConfig = "";
        this.elementDescription = "";
        this.elementPath = "";
        this.elementList = new ArrayList<JsonElement>();
    }

    /**
     * Constructor 02: minimal parameters
     * @param elementName
     * @param parentPath
     */
    public JsonElement(String elementName, String parentPath) {
        this.userId = 0;
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentElementObjectId = 0;
        this.objectGUID = "";
        this.elementName = elementName;
        this.elementPath = parentPath + this.elementName + ".";
        this.elementType = "";
        this.elementDescription = "";
        this.elementList = new ArrayList<JsonElement>();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getParentElementObjectId() {
        return parentElementObjectId;
    }

    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.parentElementObjectId = parentElementObjectId;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getObjectConfig() {
        return objectConfig;
    }

    public void setObjectConfig(String objectConfig) {
        this.objectConfig = objectConfig;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public String getElementDescription() {
        return elementDescription;
    }

    public void setElementDescription(String elementDescription) {
        this.elementDescription = elementDescription;
    }

    public String getElementPath() {
        return elementPath;
    }

    public void setElementPath(String elementPath) {
        this.elementPath = elementPath;
    }

    public void generatePath(String parentPath) {
        this.elementPath = parentPath.concat(elementName);
    }

    public ArrayList<JsonElement> getElementList() {
        return elementList;
    }

    public void setElementList(ArrayList<JsonElement> elementList) {
        this.elementList = elementList;
    }

    public void addElement(JsonElement element) {
        this.elementList.add(element);
    }

}
