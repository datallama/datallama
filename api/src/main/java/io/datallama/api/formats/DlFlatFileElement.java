/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlFlatFileElement {
    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private Integer elementSequence;
    private String elementDatatype;
    private String objectGUID;
    private String objectName;
    private String objectShortDesc;
    private String objectDescReveng;
    private Integer elementSelected;
    private Boolean elementMatched;
    private Integer fwfStartPosition;   // Fixed Width Field starting position
    private Integer fwfLength;          // Fixed Width Field length
    private Integer fwfEndPosition;     // End pos = start pos + length - 1. Set in the generateEndPosition method
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Default constructor with no Parameters
     */
    public DlFlatFileElement() {
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.elementSequence = 0;
        this.elementDatatype = "string";
        this.objectGUID = "";
        this.objectName = "";
        this.objectShortDesc = "";
        this.objectDescReveng = "";
        this.elementSelected = 0;
        this.elementMatched = false;
        this.fwfStartPosition = 0;
        this.fwfLength = 0;
        this.fwfEndPosition = 0;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    /**
     * Constructor 02 with core parameters
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param elementSequence
     * @param elementDatatype
     * @param objectGUID
     * @param objectName
     */
    public DlFlatFileElement(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                             Integer elementSequence, String elementDatatype, String objectGUID, String objectName) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.elementSequence = elementSequence;
        this.elementDatatype = elementDatatype;
        this.objectGUID = objectGUID;
        this.objectName = objectName;
        this.objectShortDesc = "";
        this.objectDescReveng = "";
        this.elementSelected = 0;
        this.elementMatched = false;
        this.fwfStartPosition = 0;
        this.fwfLength = 0;
        this.fwfEndPosition = 0;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getElementSequence() {
        return elementSequence;
    }

    public void setElementSequence(Integer elementSequence) {
        this.elementSequence = elementSequence;
    }

    public String getElementDatatype() {
        return elementDatatype;
    }

    public void setElementDatatype(String elementDatatype) {
        this.elementDatatype = elementDatatype;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public String getObjectDescReveng() {
        return objectDescReveng;
    }

    public void setObjectDescReveng(String objectDescReveng) {
        this.objectDescReveng = objectDescReveng;
    }

    public Integer getElementSelected() {
        return elementSelected;
    }

    public void setElementSelected(Integer elementSelected) {
        this.elementSelected = elementSelected;
    }

    public Boolean getElementMatched() {
        return elementMatched;
    }

    public void setElementMatched(Boolean elementMatched) {
        this.elementMatched = elementMatched;
    }

    public Integer getFwfStartPosition() {
        return fwfStartPosition;
    }

    public void setFwfStartPosition(Integer fwfStartPosition) {
        this.fwfStartPosition = fwfStartPosition;
    }

    public Integer getFwfLength() {
        return fwfLength;
    }

    public void setFwfLength(Integer fwfLength) {
        this.fwfLength = fwfLength;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public Integer getFwfEndPosition() {
        return fwfEndPosition;
    }

    public void setFwfEndPosition(Integer fwfEndPosition) {
        this.fwfEndPosition = fwfEndPosition;
    }

    public void generateFwfEndPosition() {
        this.fwfEndPosition = this.fwfStartPosition + this.fwfLength - 1;
    }
}