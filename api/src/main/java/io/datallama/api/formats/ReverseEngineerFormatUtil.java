/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.common.MoClassConstants;
import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.ReverseEngineerRunCmn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

public class ReverseEngineerFormatUtil {

    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;

    /**
     * Default constructor
     *
     * @param dataSource
     */
    public ReverseEngineerFormatUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Persists a JsonDefinition object to the DL DB.
     * @param jsonDefn
     * @param revEngRun
     * @return
     */
    public RevEngResultCmn persistJsonDefinition(JsonDefinition jsonDefn, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn result = new RevEngResultCmn();

        ArrayList<JsonElement> childElementList = jsonDefn.getElementList();
        for (int i = 0; i < childElementList.size(); i++) {
            RevEngResultCmn result02 = persistJsonElement(childElementList.get(i), jsonDefn.getObjectId(), jsonDefn.getClassId(), 0, revEngRun);
            // TODO: check result02 for error
        }

        return result;
    }

    /**
     * Saves a JSON Element definition to the DL DB. Called recursively to save the entire JSON element hierarchy.
     * @param jsonElement
     * @param parentObjectId
     * @param parentClassId
     * @param parentElementObjectId
     * @param revEngRun
     * @return
     */
    private RevEngResultCmn persistJsonElement(JsonElement jsonElement, Integer parentObjectId, Integer parentClassId,
                                               Integer parentElementObjectId, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn result = new RevEngResultCmn();
        Integer newElementId = 0;

        // 1. persist element to DL DB.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", newElementId, Types.INTEGER)
                .addValue("_class_id", moClassConstants.JSON_NODE, Types.INTEGER)
                .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                .addValue("_parent_class_id", moClassConstants.JSON_MESSAGE_FORMAT, Types.INTEGER)
                .addValue("_parent_element_object_id", parentElementObjectId, Types.INTEGER)
                .addValue("_object_guid", jsonElement.getObjectGUID(), Types.VARCHAR)
                .addValue("_object_name", jsonElement.getElementName(), Types.VARCHAR)
                .addValue("_object_shortdesc", jsonElement.getElementDescription(), Types.VARCHAR)
                .addValue("_element_type", jsonElement.getElementType(), Types.VARCHAR)
                .addValue("_object_config", null, Types.VARCHAR);
                //.addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            newElementId = (Integer) out.get("_return_id");
        } catch (Exception e) {
            result.setResultStatus(-1);
            result.setGeneralErrorMessage(String.format("Exception Class: %s. Exception Message: %s", e.getClass().getName(), e.getMessage()));
        }

        // 2. process all child elements
        if (result.getResultStatus() == 1) {
            ArrayList<JsonElement> childElementList = jsonElement.getElementList();
            for (int i = 0; i < childElementList.size(); i++) {
                RevEngResultCmn result02 = persistJsonElement(childElementList.get(i), parentObjectId, parentClassId, newElementId, revEngRun);
                // TODO: check result02 for error
            }
        }

        return result;
    }
}
