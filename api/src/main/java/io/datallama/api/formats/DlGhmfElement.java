/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.formats;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlGhmfElement {
    private Integer userId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private Integer objectId;
    private Integer classId;
    private String elementType;
    private Integer parentElementObjectId;
    private Integer elementLevel;
    private Integer elementLevelSequence;
    private Integer resultSetSequence;
    private String elementGUID;
    private String elementHierarchyNumber;
    private String elementName;
    private String elementDesc;
    private String objectConfig;
    private Integer elementVisible;
    private Integer initialExpandLevel;
    private String indentCss;
    private Boolean expanded;
    private Boolean parent;
    private Integer elementSelected;
    private Boolean elementMatched;
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Default constructor. No parameters
     */
    public DlGhmfElement() {
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.userId = 0;
        this.objectId = 0;
        this.classId = 0;
        this.elementType = "string";
        this.parentElementObjectId = 0;
        this.elementLevel = 0;
        this.elementLevelSequence = 0;
        this.resultSetSequence = 0;
        this.elementGUID = "";
        this.elementHierarchyNumber = "";
        this.elementName = "";
        this.elementDesc = "";
        this.objectConfig = "{}";
        this.elementVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.elementSelected = 0;
        this.elementMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    /**
     * Constructor 02. Essential parameters
     * @param parentObjectId
     * @param objectId
     * @param classId
     * @param elementType
     * @param parentElementObjectId
     * @param elementLevel
     * @param elementLevelSequence
     * @param elementHierarchyNumber
     * @param elementName
     * @param elementDesc
     */
    public DlGhmfElement(Integer parentObjectId, Integer parentClassId, Integer objectId, Integer classId, String elementType,
                         Integer parentElementObjectId, Integer elementLevel, Integer elementLevelSequence,
                         String elementHierarchyNumber, String elementName, String elementDesc) {
        this.userId = 0;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.objectId = objectId;
        this.classId = classId;
        this.elementType = elementType;
        this.parentElementObjectId = parentElementObjectId;
        this.elementLevel = elementLevel;
        this.elementLevelSequence = elementLevelSequence;
        this.elementHierarchyNumber = elementHierarchyNumber;
        this.elementName = elementName;
        this.elementDesc = elementDesc;
        this.objectConfig = "{}";
        this.elementVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.elementSelected = 0;
        this.elementMatched = false;
        this.elementGUID = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();

        this.determineVisibility();
        this.determineExpanded();
        this.generateIndentCss();
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public Integer getParentElementObjectId() {
        return parentElementObjectId;
    }

    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.parentElementObjectId = parentElementObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getElementLevel() {
        return elementLevel;
    }

    public void setElementLevel(Integer elementLevel) {
        this.elementLevel = elementLevel;
    }

    public Integer getElementLevelSequence() {
        return elementLevelSequence;
    }

    public void setElementLevelSequence(Integer elementLevelSequence) {
        this.elementLevelSequence = elementLevelSequence;
    }

    public String getElementGUID() {
        return elementGUID;
    }

    public void setElementGUID(String elementGUID) {
        this.elementGUID = elementGUID;
    }

    public void setElementGUID() {
        this.elementGUID = this.parentElementObjectId.toString() + ":" + this.objectId.toString();
    }

    public String getElementHierarchyNumber() {
        return elementHierarchyNumber;
    }

    public void setElementHierarchyNumber(String elementHierarchyNumber) {
        this.elementHierarchyNumber = elementHierarchyNumber;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementDesc() {
        return elementDesc;
    }

    public void setElementDesc(String elementDesc) {
        this.elementDesc = elementDesc;
    }

    public String getObjectConfig() {
        return objectConfig;
    }

    public void setObjectConfig(String objectConfig) {
        this.objectConfig = objectConfig;
    }

    public Integer getElementVisible() {
        return elementVisible;
    }

    public void setElementVisible(Integer elementVisible) {
        this.elementVisible = elementVisible;
    }

    public void determineVisibility() {
        elementVisible = (elementLevel <= initialExpandLevel) ? 1 : 0;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public void determineExpanded() {
        this.expanded = (elementLevel < initialExpandLevel) ? true : false;
    }

    public String getIndentCss() {
        return indentCss;
    }

    public void setIndentCss(String indentCss) {
        this.indentCss = indentCss;
    }

    public void generateIndentCss() {
        this.indentCss = "{'margin-left':" + Integer.toString(elementLevel * 15) + ";}";
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public Integer getElementSelected() {
        return elementSelected;
    }

    public void setElementSelected(Integer elementSelected) {
        this.elementSelected = elementSelected;
    }

    public Boolean getElementMatched() {
        return elementMatched;
    }

    public void setElementMatched(Boolean elementMatched) {
        this.elementMatched = elementMatched;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }
}
