/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlMoClassConfig;

public class DlFormat {
    private Integer formatId;
    private Integer classId;
    private String className;
    private String objectTable;
    private String objectDetailState;
    private String formatName;
    private String formatShortDesc;
    private String objectGUID;
    private String objectConfig;
    private DlCustomForm customForm;
    private Integer createSource;
    private DlMoClassConfig classConfig;

    /**
     * Default constructor with no parameters
     */
    public DlFormat() {
        this.formatId = 0;
        this.classId = 0;
        this.className = "";
        this.objectTable = "";
        this.objectDetailState = "";
        this.formatName = "";
        this.formatShortDesc = "";
        this.objectGUID = "";
        this.objectConfig = "{}";
        this.customForm = new DlCustomForm();
        this.createSource = 0;
        this.classConfig = new DlMoClassConfig();
    }

    /**
     * Constructor 02 with core identifier parameters
     * @param formatId
     * @param classId
     * @param className
     * @param formatName
     * @param formatShortDesc
     */
    public DlFormat(Integer formatId, Integer classId, String className, String formatName, String formatShortDesc) {
        this.formatId = formatId;
        this.classId = classId;
        this.className = className;
        this.formatName = formatName;
        this.formatShortDesc = formatShortDesc;
        this.objectGUID = "";
        this.objectConfig = "{}";
        this.objectTable = "";
        this.objectDetailState = "";
        this.customForm = new DlCustomForm();
        this.createSource = 0;
        this.classConfig = new DlMoClassConfig();
    }

    public Integer getFormatId() {
        return formatId;
    }

    public void setFormatId(Integer formatId) {
        this.formatId = formatId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectTable() {
        return objectTable;
    }

    public void setObjectTable(String objectTable) {
        this.objectTable = objectTable;
    }

    public String getObjectDetailState() {
        return objectDetailState;
    }

    public void setObjectDetailState(String objectDetailState) {
        this.objectDetailState = objectDetailState;
    }

    public String getFormatName() {
        return formatName;
    }

    public void setFormatName(String formatName) {
        this.formatName = formatName;
    }

    public String getFormatShortDesc() {
        return formatShortDesc;
    }

    public void setFormatShortDesc(String formatShortDesc) {
        this.formatShortDesc = formatShortDesc;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public String getObjectConfig() {
        return objectConfig;
    }

    public void setObjectConfig(String objectConfig) {
        this.objectConfig = objectConfig;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }

    public Integer getCreateSource() {
        return createSource;
    }

    public void setCreateSource(Integer createSource) {
        this.createSource = createSource;
    }

    public DlMoClassConfig getClassConfig() {
        return classConfig;
    }

    public void setClassConfig(DlMoClassConfig classConfig) {
        this.classConfig = classConfig;
    }
}
