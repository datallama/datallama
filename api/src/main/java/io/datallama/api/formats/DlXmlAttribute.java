/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlXmlAttribute {

    private int objectId;
    private int classId;
    private int parentObjectId;
    private int parentClassId;
    private String xmlAttributeType;
    private int xmlAttributeSequence;
    private String xmlAttributeGUID;
    private String xmlAttributeName;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private Integer resultSetSequence;
    private String shortDesc;

    public DlXmlAttribute() {
    }

    public DlXmlAttribute(int objectId, int classId, int parentObjectId, int parentClassId, String xmlAttributeType, int xmlAttributeSequence, String xmlAttributeGUID, String xmlAttributeName, String shortDesc) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.xmlAttributeType = xmlAttributeType;
        this.xmlAttributeSequence = xmlAttributeSequence;
        this.xmlAttributeGUID = xmlAttributeGUID;
        this.xmlAttributeName = xmlAttributeName;
        this.shortDesc = shortDesc;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public int getObjectId() {
        return objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(int parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public int getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(int parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getXmlAttributeType() {
        return xmlAttributeType;
    }

    public void setXmlAttributeType(String xmlAttributeType) {
        this.xmlAttributeType = xmlAttributeType;
    }

    public int getXmlAttributeSequence() {
        return xmlAttributeSequence;
    }

    public void setXmlAttributeSequence(int xmlAttributeSequence) {
        this.xmlAttributeSequence = xmlAttributeSequence;
    }

    public String getXmlAttributeGUID() {
        return xmlAttributeGUID;
    }

    public void setXmlAttributeGUID(String xmlAttributeGUID) {
        this.xmlAttributeGUID = xmlAttributeGUID;
    }

    public String getXmlAttributeName() {
        return xmlAttributeName;
    }

    public void setXmlAttributeName(String xmlAttributeName) {
        this.xmlAttributeName = xmlAttributeName;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }
}
