/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

public class DlFormatMoveElementDefinition {
    private Long objectId;
    private Long newParentObjectId;
    private Long targetObjectId;

    /**
     * Default no-parameters constructor
     */
    public DlFormatMoveElementDefinition() {
        this.objectId = Long.valueOf(0);
        this.newParentObjectId = Long.valueOf(0);
        this.targetObjectId = Long.valueOf(0);
    }

    /**
     * Constructor
     * @param objectId
     * @param newParentObjectId
     */
    public DlFormatMoveElementDefinition (Long objectId, Long newParentObjectId) {
        this.objectId = objectId;
        this.newParentObjectId = newParentObjectId;
        this.targetObjectId = Long.valueOf(0);
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getNewParentObjectId() {
        return newParentObjectId;
    }

    public void setNewParentObjectId(Long newParentObjectId) {
        this.newParentObjectId = newParentObjectId;
    }

    public Long getTargetObjectId() {
        return targetObjectId;
    }

    public void setTargetObjectId(Long targetObjectId) {
        this.targetObjectId = targetObjectId;
    }
}
