/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlJsonNode {
    private Integer formatId;
    private Integer parentClassId;
    private Integer jsonNodeId;
    private Integer classId;
    private Integer parentElementObjectId;
    private Integer jsonNodeLevel;
    private Integer jsonNodeLevelSequence;
    private String jsonNodeType;
    private String jsonNodeGUID;
    private String jsonNodeHierarchyNumber;
    private String jsonNodeName;
    private String jsonNodeShortDesc;
    private Integer jsonNodeVisible;
    private Integer initialExpandLevel;
    private String indentCss;
    private Boolean expanded;
    private Boolean parent;
    private Integer resultSetSequence;
    private Integer jsonNodeSelected;
    private Boolean jsonNodeMatched;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private String customFormData;

    /**
     * Constructor. Default no-parameters.
     */
    public DlJsonNode() {
        this.formatId = 0;
        this.parentClassId = 0;
        this.jsonNodeId = 0;
        this.classId = 0;
        this.parentElementObjectId = 0;
        this.jsonNodeLevel = 0;
        this.jsonNodeLevelSequence = 0;
        this.jsonNodeType = "";
        this.jsonNodeGUID = "";
        this.jsonNodeHierarchyNumber = "";
        this.jsonNodeName = "";
        this.jsonNodeShortDesc = "";
        this.jsonNodeVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.jsonNodeSelected = 0;
        this.jsonNodeMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customFormData = "";
    }

    /**
     * Constructor.
     * @param formatId
     * @param jsonNodeId
     * @param classId
     * @param parentElementObjectId
     * @param jsonNodeLevel
     * @param jsonNodeLevelSequence
     * @param jsonNodeType
     * @param jsonNodeHierarchyNumber
     * @param jsonNodeName
     * @param jsonNodeShortDesc
     */
    public DlJsonNode(Integer formatId, Integer jsonNodeId, Integer classId, Integer parentElementObjectId, Integer jsonNodeLevel,
                      Integer jsonNodeLevelSequence, String jsonNodeType, String jsonNodeHierarchyNumber, String jsonNodeName, String jsonNodeShortDesc) {
        this.formatId = formatId;
        this.parentClassId = 0;
        this.jsonNodeId = jsonNodeId;
        this.classId = classId;
        this.parentElementObjectId = parentElementObjectId;
        this.jsonNodeLevel = jsonNodeLevel;
        this.jsonNodeLevelSequence = jsonNodeLevelSequence;
        this.jsonNodeType = jsonNodeType;
        this.jsonNodeHierarchyNumber = jsonNodeHierarchyNumber;
        this.jsonNodeName = jsonNodeName;
        this.jsonNodeShortDesc = jsonNodeShortDesc;
        this.jsonNodeVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.jsonNodeSelected = 0;
        this.jsonNodeMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customFormData = "";

        this.determineVisibility();
        this.determineExpanded();
        this.generateIndentCss();
    }

    public Integer getFormatId() {
        return formatId;
    }

    public void setFormatId(Integer formatId) {
        this.formatId = formatId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getJsonNodeId() {
        return jsonNodeId;
    }

    public void setJsonNodeId(Integer jsonNodeId) {
        this.jsonNodeId = jsonNodeId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentElementObjectId() {
        return parentElementObjectId;
    }

    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.parentElementObjectId = parentElementObjectId;
    }

    public Integer getJsonNodeLevel() {
        return jsonNodeLevel;
    }

    public void setJsonNodeLevel(Integer jsonNodeLevel) {
        this.jsonNodeLevel = jsonNodeLevel;
    }

    public String getJsonNodeType() {
        return jsonNodeType;
    }

    public void setJsonNodeType(String jsonNodeType) {
        this.jsonNodeType = jsonNodeType;
    }

    public Integer getJsonNodeLevelSequence() {
        return jsonNodeLevelSequence;
    }

    public void setJsonNodeLevelSequence(Integer jsonNodeLevelSequence) {
        this.jsonNodeLevelSequence = jsonNodeLevelSequence;
    }

    public String getJsonNodeGUID() {
        return jsonNodeGUID;
    }

    public void setJsonNodeGUID(String jsonNodeGUID) {
        this.jsonNodeGUID = jsonNodeGUID;
    }

    public String getJsonNodeHierarchyNumber() {
        return jsonNodeHierarchyNumber;
    }

    public void setJsonNodeHierarchyNumber(String jsonNodeHierarchyNumber) {
        this.jsonNodeHierarchyNumber = jsonNodeHierarchyNumber;
    }

    public String getJsonNodeName() {
        return jsonNodeName;
    }

    public void setJsonNodeName(String jsonNodeName) {
        this.jsonNodeName = jsonNodeName;
    }

    public String getJsonNodeShortDesc() {
        return jsonNodeShortDesc;
    }

    public void setJsonNodeShortDesc(String jsonNodeShortDesc) {
        this.jsonNodeShortDesc = jsonNodeShortDesc;
    }

    public Integer getJsonNodeVisible() {
        return jsonNodeVisible;
    }

    public void setJsonNodeVisible(Integer jsonNodeVisible) {
        this.jsonNodeVisible = jsonNodeVisible;
    }

    public void determineVisibility() {
        jsonNodeVisible = (jsonNodeLevel <= initialExpandLevel) ? 1 : 0;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public String getIndentCss() {
        return indentCss;
    }

    public void setIndentCss(String indentCss) {
        this.indentCss = indentCss;
    }

    public void generateIndentCss() {
        this.indentCss = "{'margin-left':" + Integer.toString(jsonNodeLevel * 15) + ";}";
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public void determineExpanded() {
        this.expanded = (jsonNodeLevel < initialExpandLevel) ? true : false;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public Integer getJsonNodeSelected() {
        return jsonNodeSelected;
    }

    public void setJsonNodeSelected(Integer jsonNodeSelected) {
        this.jsonNodeSelected = jsonNodeSelected;
    }

    public Boolean getJsonNodeMatched() {
        return jsonNodeMatched;
    }

    public void setJsonNodeMatched(Boolean jsonNodeMatched) {
        this.jsonNodeMatched = jsonNodeMatched;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public String getCustomFormData() {
        return customFormData;
    }

    public void setCustomFormData(String customFormData) {
        this.customFormData = customFormData;
    }
}
