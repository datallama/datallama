/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.*;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import java.io.*;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DlFormatController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.file.upload}")
    private String uploadFilePath;

    /**
     * Retrieves the full list of message/file formats defined in DL
     * URL Path: /format/list
     */
    @RequestMapping(method = GET, value = "/format/list")
    @ResponseBody
    public Map<String, Object> listFormats() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList formatList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_msg_formats_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            DlFormat format = new DlFormat(
                    (Integer) elementInfo.get("object_id"),
                    (Integer) elementInfo.get("class_id"),
                    elementInfo.get("class_name").toString(),
                    elementInfo.get("object_name").toString(),
                    elementInfo.get("object_shortdesc").toString()
            );
            format.setObjectTable(elementInfo.get("object_table").toString());
            format.setObjectDetailState(elementInfo.get("object_detail_state").toString());
            formatList.add(format);
        }

        results.put("result_data", formatList);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Reads the format definition identified by {formatId}
     * URL Path: /format/{formatId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/format/{objectId}/{classId}")
    @ResponseBody
    public Map<String, Object> readFormatSummaryByObjectIdClassId(@PathVariable Integer objectId, @PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        DlFormatUtil formatUtil = new DlFormatUtil(dataSource);
        results = formatUtil.readFormatSummaryByObjectId(objectId, classId);

        return results;
    }

    /**
     * Reads the format definition identified by {formatId}
     * URL Path: /format/{formatId}
     *
     * @param objectGUID
     * @return
     */
    @RequestMapping(method = GET, value = "/format/guid/{objectGUID}")
    @ResponseBody
    public Map<String, Object> readFormatSummaryByGuid(@PathVariable String objectGUID) {
        Map<String, Object> results;
        DlFormatUtil formatUtil = new DlFormatUtil(dataSource);
        results = formatUtil.readFormatSummaryByGUID(objectGUID);

        return results;
    }

    /**
     * Deletes a format definition
     * URL Path: /format/{formatId}
     *
     * @param formatId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/format/{formatId}")
    @ResponseBody
    public Map<String, Object> deleteFormat(@PathVariable Integer formatId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_fmt");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_fmt_id", formatId, Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * URL Path: /format/class/list
     * Lists all classes of message formats that can be defined via Data Llama
     */
    @RequestMapping(method = GET, value = "/format/class/list")
    @ResponseBody
    public Map<String, Object> listFormatClasses() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList<DlFormatClass> formatClassList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_classes");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap formatClassInfo = (LinkedCaseInsensitiveMap) listElement;
            DlFormatClass formatClass = new DlFormatClass(
                    (Integer) formatClassInfo.get("class_id"),
                    formatClassInfo.get("class_name").toString(),
                    formatClassInfo.get("class_shortdesc").toString(),
                    formatClassInfo.get("object_table").toString(),
                    formatClassInfo.get("object_detail_state").toString()
            );
            formatClassList.add(formatClass);
        }

        results.put("result_data", formatClassList);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Description: Writes a new Format definition to the DL database.
     * URL Path: /format/summary
     *
     * @return
     */
    @RequestMapping(method = POST, value = "/format/summary")
    public Map<String, Object> writeFormat(@RequestBody DlFormat newFormat, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.
        DlDbExceptionHandler xcptnHandler = new DlDbExceptionHandler();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newFormat.getFormatId(), Types.INTEGER)
                .addValue("_class_id", newFormat.getClassId(), Types.INTEGER)
                .addValue("_object_name", newFormat.getFormatName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newFormat.getFormatShortDesc(), Types.VARCHAR)
                .addValue("_object_guid", newFormat.getObjectGUID(), Types.VARCHAR)
                .addValue("_object_config", newFormat.getObjectConfig(), Types.VARCHAR)
                .addValue("_defn_method", newFormat.getCreateSource(), Types.INTEGER);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            resultData.put("returnId", (Integer) out.get("_new_object_id"));
        } catch (Exception e) {
            xcptnHandler.setExceptionDetails(e);
            resultData.put("exception", xcptnHandler);
            resultStatus = -1;
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Associates a format definition with a custom form.
     *
     * @param customFormAssoc
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/customform")
    public Map<String, Object> writeFormatCustomForm(@RequestBody MoCustomFormAssoc customFormAssoc, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", customFormAssoc.getObjectId(), Types.INTEGER)
                .addValue("_class_id", customFormAssoc.getClassId(), Types.INTEGER)
                .addValue("_custom_form_id", customFormAssoc.getCustomFormId(), Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /format/json/content/{formatId}
     * Returns all JSON nodes in the json format definition identified by {formatId}
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/format/json/content/{objectId}")
    @ResponseBody
    public Map<String, Object> readJsonFormatContent(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dbResultList = new ArrayList(); // this list will be returned in the results Map
        ArrayList jsonNodeList = new ArrayList();


        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.BIGINT);
        Map<String, Object> out = jdbcCall.execute(in);

        DlFormat format = new DlFormat(
                objectId,
                (Integer) out.get("_class_id"),       // classId
                out.get("_class_name").toString(),    // className
                out.get("_object_name").toString(),      // formatName
                out.get("_object_shortdesc").toString() // formatShortDesc
        );
        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        format.setCustomForm(customForm);

        resultData.put("format_summary", format);


        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_elements");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // 2. generate a JsonNode object for every row in result set and add to Array List jsonNodeList
        Integer resultSetSequence = 0;
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap jsonNodeInfo = (LinkedCaseInsensitiveMap) listElement;
            DlJsonNode jsonNode = new DlJsonNode(
                    objectId,
                    (Integer) jsonNodeInfo.get("object_id"),
                    (Integer) jsonNodeInfo.get("class_id"),
                    (Integer) jsonNodeInfo.get("parent_element_object_id"),
                    (Integer) jsonNodeInfo.get("element_level"),
                    (Integer) jsonNodeInfo.get("element_level_sequence"),
                    jsonNodeInfo.get("element_type").toString(),
                    jsonNodeInfo.get("element_hierarchy_number").toString(),
                    jsonNodeInfo.get("object_name").toString(),
                    jsonNodeInfo.get("object_shortdesc").toString());

            jsonNode.setResultSetSequence(resultSetSequence);
            resultSetSequence++;
            jsonNode.setParent((Boolean) jsonNodeInfo.get("element_is_parent"));
            jsonNode.setJsonNodeGUID(jsonNodeInfo.get("object_guid").toString());

            // add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            jsonNode.setMdContext(mdContext.generateMdContextList(jsonNodeInfo.get("md_context").toString()));

            jsonNodeList.add(jsonNode);

        }

        resultData.put("json_nodes", jsonNodeList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /format/json/content/{formatId}
     * Returns all JSON nodes in the json format definition identified by {formatId}
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/format/json/{formatId}/export")
    @ResponseBody
    public Map<String, Object> readJsonFormatExport(@PathVariable Integer formatId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList jsonNodeList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_msg_format_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_fmt_id", formatId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // TODO: can probably get rid of the summary data
        LinkedCaseInsensitiveMap formatInfo = (LinkedCaseInsensitiveMap) dbResultList.get(0);
        DlFormat format = new DlFormat(
                (Integer) formatInfo.get("object_id"),         // formatId
                (Integer) formatInfo.get("class_id"),       // classId
                formatInfo.get("class_name").toString(),    // className
                formatInfo.get("object_name").toString(),      // formatName
                formatInfo.get("object_shortdesc").toString() // formatShortDesc
        );
        DlCustomForm customForm = new DlCustomForm(
                (Integer) formatInfo.get("custom_form_id"),
                formatInfo.get("custom_form_name").toString(),
                formatInfo.get("schema_definition").toString(),
                formatInfo.get("form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        format.setCustomForm(customForm);

        resultData.put("format_summary", format);

        // 2. read all json nodes from DL DB, generate a JsonNode object for each node and add to Array List jsonNodeList
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_elements_export");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap jsonNodeInfo = (LinkedCaseInsensitiveMap) listElement;
            DlJsonNode jsonNode = new DlJsonNode(
                    (Integer) jsonNodeInfo.get("parent_object_id"),
                    (Integer) jsonNodeInfo.get("object_id"),
                    (Integer) jsonNodeInfo.get("class_id"),
                    (Integer) jsonNodeInfo.get("parent_element_object_id"),
                    (Integer) jsonNodeInfo.get("element_level"),
                    (Integer) jsonNodeInfo.get("element_level_sequence"),
                    jsonNodeInfo.get("element_type").toString(),
                    jsonNodeInfo.get("element_hierarchy_number").toString(),
                    jsonNodeInfo.get("object_name").toString(),
                    jsonNodeInfo.get("object_shortdesc").toString());

            jsonNode.setParent((Boolean) jsonNodeInfo.get("element_is_parent"));
            jsonNode.setJsonNodeGUID(jsonNodeInfo.get("object_guid").toString());
            jsonNode.setCustomFormData(jsonNodeInfo.get("custom_form_data").toString());

            // 2.1. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            jsonNode.setMdContext(mdContext.generateMdContextList(jsonNodeInfo.get("md_context").toString()));

            jsonNodeList.add(jsonNode);

        }

        resultData.put("json_nodes", jsonNodeList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /format/ghmf/content/{formatId}
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/format/ghmf/content/{objectId}")
    @ResponseBody
    public Map<String, Object> readGhmfFormatContent(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList dbResultList = new ArrayList();
        ArrayList ghmfElementList = new ArrayList();

        // 1. get the message format summary info
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlFormat format = new DlFormat(
                objectId,
                (Integer) out.get("_class_id"),
                out.get("_class_name").toString(),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString()
        );
        DlCustomForm customForm = new DlCustomForm(
                    (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        format.setCustomForm(customForm);

        resultData.put("format_summary", format);

        // 2. get all elements in the message format
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_elements");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;

        // 2.1. process each element in the result set from the DL database
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap ghmfElementInfo = (LinkedCaseInsensitiveMap) listElement;
            DlGhmfElement ghmfElement = new DlGhmfElement(
                    objectId,
                    (Integer) ghmfElementInfo.get("parent_class_id"),
                    (Integer) ghmfElementInfo.get("object_id"),
                    (Integer) ghmfElementInfo.get("class_id"),
                    ghmfElementInfo.get("element_type").toString(),
                    (Integer) ghmfElementInfo.get("parent_element_object_id"),
                    (Integer) ghmfElementInfo.get("element_level"),
                    (Integer) ghmfElementInfo.get("element_level_sequence"),
                    ghmfElementInfo.get("element_hierarchy_number").toString(),
                    ghmfElementInfo.get("object_name").toString(),
                    ghmfElementInfo.get("object_shortdesc").toString()
            );
            ghmfElement.setElementGUID(ghmfElementInfo.get("object_guid").toString());
            ghmfElement.setResultSetSequence(resultSetSequence);
            ghmfElement.setParent((Boolean) ghmfElementInfo.get("element_is_parent"));
            resultSetSequence++;

            // 2.1.1 add the metadata context for the GHMF element
            DlMetaDataContext mdContext = new DlMetaDataContext();
            ghmfElement.setMdContext(mdContext.generateMdContextList(ghmfElementInfo.get("md_context").toString()));

            ghmfElementList.add(ghmfElement);
        }

        resultData.put("elements", ghmfElementList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the set of XML Elements and Element Attributes for an XML format identified by {formatId}
     * URL Path: /format/xml/content/flat/{formatId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/format/xml/content/{objectId}")
    @ResponseBody
    public Map<String, Object> readXmlFormatContent(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dbResultList = new ArrayList();
        ArrayList<DlXmlElement> xmlFlatList = new ArrayList<>(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.BIGINT);
        Map<String, Object> out = jdbcCall.execute(in);

        // 1. generate the the format summary
        DlFormat format = new DlFormat(
                objectId,
                (Integer) out.get("_class_id"),
                out.get("_class_name").toString(),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString()
        );
        format.setObjectGUID(out.get("_object_guid").toString());

        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        format.setCustomForm(customForm);

        resultData.put("format_summary", format);

        // 2. read the XML Elements for XML format definition
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_elements");
        out = jdbcCall.execute(in);
        ArrayList dbElementsResultList = (ArrayList) out.get("#result-set-1");

        // 3. add XML elements and Attributes to the result set
        Integer resultSetSequence = 0;
        for (Object listElement : dbElementsResultList) {
            LinkedCaseInsensitiveMap xmlElementInfo = (LinkedCaseInsensitiveMap) listElement;

            // 3.1. instantiate the xml element
            DlXmlElement xmlElement = new DlXmlElement(
                    (Integer) xmlElementInfo.get("object_id"),
                    (Integer) xmlElementInfo.get("class_id"),
                    (Integer) xmlElementInfo.get("parent_object_id"),
                    (Integer) xmlElementInfo.get("parent_class_id"),
                    (Integer) xmlElementInfo.get("parent_element_object_id"),
                    xmlElementInfo.get("element_type").toString(),
                    (Integer) xmlElementInfo.get("element_level"),
                    (Integer) xmlElementInfo.get("element_level_sequence"),
                    xmlElementInfo.get("element_hierarchy_number").toString(),
                    (Boolean) xmlElementInfo.get("element_is_parent"),
                    xmlElementInfo.get("object_name").toString(),
                    xmlElementInfo.get("object_shortdesc").toString(),
                    xmlElementInfo.get("object_guid").toString()
            );
            xmlElement.setResultSetSequence(resultSetSequence);
            resultSetSequence++;

            // 3.2. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            xmlElement.setMdContext(mdContext.generateMdContextList(xmlElementInfo.get("md_context").toString()));

            xmlFlatList.add(xmlElement);
        }

        resultData.put("xml_flat_list", xmlFlatList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/format/reverse-engineer/xml/{formatId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> revEngXML(@RequestParam("file") MultipartFile file, @RequestHeader("dlUserId") String userId, @PathVariable Integer formatId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();

        XMLInputFactory factory = XMLInputFactory.newInstance();

        try {
            // 1. Validate input file. The file must have an extension of *.xml
            if (Objects.requireNonNull(file.getOriginalFilename()).split("\\.").length != 2 || !file.getOriginalFilename().split("\\.")[1].equals("xml")) {
                throw new IOException();
            }
            File convertFile = new File(uploadFilePath + file.getOriginalFilename());

            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();

            // 2. create new format import run record in DL DB
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_import_run");
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_fmt_id", formatId, Types.INTEGER)
                    .addValue("_source_file", file.getOriginalFilename());
            Map<String, Object> out = jdbcCall.execute(in);
            Integer importRunId = (Integer) out.get("_return_id");
            if (importRunId < 0) {
                results.put("result_data", "Couldn't start format import run. The specified formatId does not exist in the DL DB.");
                results.put("result_status", -1);
                return results;
            }

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(convertFile);

            // 3. Process all children of root node and recursively all child nodes
            NodeList children = doc.getChildNodes();

            for (int i = 0; i < children.getLength(); i++) {
                if (writeXmlElementRevEng(children.item(i), Integer.parseInt(userId), formatId, 36, 0, importRunId) != 1) {
                    throw new IOException();
                }
            }

            resultData.put("returnId", formatId);
            results.put("result_data", resultData);
            results.put("result_status", 1);

        } catch (Exception e) {
            e.printStackTrace();

            //todo: call the restore function to undo the half made changes.

            results.put("result_data", "Couldn't parse the file");
            results.put("result_status", -1);
        }

        return results;
    }

    /**
     * Writes an XML Element or Attribute definition to the DL DB as part of an XML Format Reverse Engineer run
     *
     * @param node
     * @param userId
     * @param parentObjectId
     * @param parentClassId
     * @param parentElementObjectId
     * @param importRunId
     * @return
     */
    private int writeXmlElementRevEng(Node node, int userId, int parentObjectId, int parentClassId, int parentElementObjectId, int importRunId) {
        System.out.println(String.format("parentObjectId: %d, parentClassId: %d", parentObjectId, parentClassId));

        try {
            if (node.getNodeType() != Node.ELEMENT_NODE)
                return (1);

            String elementType = "other";
            switch (node.getNodeType()) {
                case Node.ELEMENT_NODE:
                    elementType = "element";
                    break;
                case Node.CDATA_SECTION_NODE:
                    elementType = "cdata";
                    break;
                case Node.TEXT_NODE:
                    elementType = "text";
                    break;
                default:
                    elementType = "other";
                    break;
            }

            // 1. write the new Node to the data base
            // TODO: include isParent in write proc
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_xml_element");
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_object_id", -1, Types.INTEGER)
                    .addValue("_class_id", 37, Types.INTEGER)
                    .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                    .addValue("_parent_class_id", parentClassId, Types.INTEGER)
                    .addValue("_fmt_import_run_id", importRunId, Types.INTEGER)
                    .addValue("_parent_element_object_id", parentElementObjectId, Types.INTEGER)
                    .addValue("_xml_element_type", elementType, Types.VARCHAR)
                    .addValue("_xml_element_name", node.getNodeName(), Types.VARCHAR)
                    .addValue("_xml_element_shortdesc", null, Types.VARCHAR);
            Map<String, Object> out = jdbcCall.execute(in);
            ArrayList rslt = (ArrayList) out.get("#result-set-1");

            // if new XML element is saved to DL DB successfully then the objectId for the new element will be returned
            LinkedCaseInsensitiveMap xmlElementInfo = (LinkedCaseInsensitiveMap) rslt.get(0);

            // 1.2. If the node has any attributes, write those to the DB
            if (node.getAttributes() != null) {
                for (int i = 0; i < node.getAttributes().getLength(); i++) {
                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_xml_element");
                    in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)
                            .addValue("_object_id", -1, Types.INTEGER)
                            .addValue("_class_id", 38, Types.INTEGER)   // TODO: must be replaced with a Metadata Object Class Service
                            .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                            .addValue("_parent_class_id", parentClassId, Types.INTEGER)
                            .addValue("_fmt_import_run_id", importRunId, Types.INTEGER)
                            .addValue("_parent_element_object_id", (Integer) xmlElementInfo.get("object_id"), Types.INTEGER)
                            .addValue("_xml_element_type", "attribute", Types.VARCHAR)
                            .addValue("_xml_element_name", node.getAttributes().item(i).getNodeName(), Types.VARCHAR)
                            .addValue("_xml_element_shortdesc", null, Types.VARCHAR);
                    jdbcCall.execute(in);
                }
            }

            // 2. For all the children of current node recursively run this function.
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                if (writeXmlElementRevEng(children.item(i), userId, parentObjectId, parentClassId, (Integer) xmlElementInfo.get("object_id"), importRunId) != 1) {
                    return -1;
                }
            }
        } catch (RuntimeException e) {
            System.out.println(e.toString());
            return -1;
        }

        return 1;
    }

    /**
     * Generate a JSON format definition from an uploaded JSON file.
     *
     * @param file
     * @param userId
     * @param formatId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/reverse-engineer/json/{formatId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> revEngJson(@RequestParam("file") MultipartFile file, @RequestHeader("dlUserId") String userId, @PathVariable Integer formatId) {
        Integer resultStatus = 1;
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();

        ObjectMapper objectMapper = new ObjectMapper();
        File convertFile = null;
        Integer importRunId = 0;

        // 1. save the uploaded JSON file and parse it.
        try {
            // 1.1. check that the uploaded file has a file extension of *.json
            if (Objects.requireNonNull(file.getOriginalFilename()).split("\\.").length != 2 || !file.getOriginalFilename().split("\\.")[1].equals("json")) {
                results.put("result_data", "The file you selected does not have a file extension of the form *.json");
                resultStatus = -1;
                results.put("result_status", resultStatus);
            }

            if (resultStatus > 0) {
                convertFile = new File(uploadFilePath + file.getOriginalFilename());

                // write the MultipartFile data to the local filesystem.
                FileOutputStream fout = new FileOutputStream(convertFile);
                fout.write(file.getBytes());
                fout.close();

                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_import_run");
                SqlParameterSource in = new MapSqlParameterSource()
                        .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                        .addValue("_fmt_id", formatId, Types.INTEGER)
                        .addValue("_source_file", file.getOriginalFilename());
                Map<String, Object> out = jdbcCall.execute(in);
                importRunId = (Integer) out.get("_return_id");

                if (importRunId < 0) {
                    results.put("result_data", "Couldn't start format import run. The specified formatId does not exist in the DL DB.");
                    resultStatus = -1;
                    results.put("result_status", resultStatus);
                }
            }

            // 1.2. parse the JSON file using the objectMapper
            JsonNode rootNode = null;
            if (resultStatus > 0) {
                try {
                    rootNode = objectMapper.readTree(convertFile);
                } catch (Exception e) {
                    results.put("result_data", "Error encountered when trying to parse the JSON file. Check that the file is a valid JSON file.");
                    resultStatus = -1;
                    results.put("result_status", resultStatus);
                }
            }

            // 1.3. process all nodes in the JSON file
            if (resultStatus > 0) {
                // 1.3.1. process the top level nodes
                Iterator<Map.Entry<String, JsonNode>> nodes = rootNode.fields();
                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_json_node_load");
                Integer jsonNodeClassId = 12;     // TODO: this should not be hard coded

                while (nodes.hasNext()) {
                    Map.Entry<String, JsonNode> entry = nodes.next(); // (Map.Entry<String, JsonNode>)
                    JsonNode currentNode = entry.getValue();

                    SqlParameterSource in = new MapSqlParameterSource()
                            .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                            .addValue("_class_id", jsonNodeClassId, Types.INTEGER)
                            .addValue("_fmt_id", formatId, Types.INTEGER)
                            .addValue("_fmt_import_run_id", importRunId, Types.INTEGER)
                            .addValue("_json_node_type", currentNode.getNodeType().toString().toLowerCase(), Types.VARCHAR)
                            .addValue("_parent_object_id", 0, Types.INTEGER) // top level nodes always have parent_object_id = 0
                            .addValue("_json_node_name", entry.getKey(), Types.VARCHAR);

                    Map<String, Object> out = jdbcCall.execute(in);
                    Integer newParentObjectId = (Integer) out.get("_new_object_id");

                    if (currentNode.getNodeType() == JsonNodeType.OBJECT || currentNode.getNodeType() == JsonNodeType.ARRAY) {
                        processJsonNodeChildren(currentNode, Integer.parseInt(userId), formatId, importRunId, newParentObjectId);
                    }

                }
                resultData.put("returnId", formatId);
                results.put("result_data", resultData);
                results.put("result_status", resultStatus);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //TODO: there should be a restore function to undo the half made changes.
            results.put("result_data", "Error encountered when to trying to parse the JSON file.");
            resultStatus = -1;
            results.put("result_status", resultStatus);
        }

        return results;
    }

    /**
     * Writes a reverse engineered JSON node to the DL DB. This function is called recursively as the JSON tree is traversed.
     *
     * @param jsonNode
     * @return
     */
    private int processJsonNodeChildren(JsonNode jsonNode, Integer userId, Integer formatId,
                                        Integer importRunId, Integer parentObjectId) {
        Integer resultStatus = 1;

        Iterator<Map.Entry<String, JsonNode>> nodes = jsonNode.fields();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_json_node_load");
        Integer jsonNodeClassId = 12;     // TODO: this should not be hard coded

        while (nodes.hasNext()) {
            Map.Entry<String, JsonNode> entry = nodes.next();
            JsonNode currentNode = entry.getValue();

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_class_id", jsonNodeClassId, Types.INTEGER)
                    .addValue("_fmt_id", formatId, Types.INTEGER)
                    .addValue("_fmt_import_run_id", importRunId, Types.INTEGER)
                    .addValue("_json_node_type", currentNode.getNodeType().toString().toLowerCase(), Types.VARCHAR)
                    .addValue("_parent_object_id", parentObjectId, Types.INTEGER)
                    .addValue("_json_node_name", entry.getKey(), Types.VARCHAR);

            Map<String, Object> out = jdbcCall.execute(in);
            Integer newParentObectId = (Integer) out.get("_new_object_id");

            if (currentNode.getNodeType() == JsonNodeType.OBJECT || currentNode.getNodeType() == JsonNodeType.ARRAY) {
                processJsonNodeChildren(currentNode, userId, formatId, importRunId, newParentObectId);
            }
        }

        return resultStatus;
    }


    /**
     * Retrieves details of the GHMF element identified by {objectId}
     * URL Path: /format/ghmf/content/element/{objectId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/format/ghmf/content/element/{objectId}")
    @ResponseBody
    public Map<String, Object> readGhmfElementDetails(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList dbResultList = new ArrayList();       // used for database results
        ArrayList associationList = new ArrayList();    // this list is returned in the results Map
        ArrayList commentList = new ArrayList();        // list list is returned in the results Map

        // 1: get the core attributes of the GHMF element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlGhmfElement ghmfElement = new DlGhmfElement(
                (Integer) out.get("_parent_object_id"),
                (Integer) out.get("_parent_class_id"),
                objectId,
                (Integer) out.get("_class_id"),
                out.get("_element_type").toString(),
                (Integer) out.get("_parent_element_object_id"),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_hierarchy_number").toString(),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString()
        );
        ghmfElement.setElementGUID(out.get("_object_guid").toString());
        resultData.put("attributes", ghmfElement);

        // 2. add the metadata context for the GHMF element
        DlMetaDataContext mdContext = new DlMetaDataContext();
        ghmfElement.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        // 3. get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", ghmfElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", ghmfElement.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 3.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 3.2. add the metadata context for the AssociatedObject instance
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 4: get the comments for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", ghmfElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", ghmfElement.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }

        resultData.put("comments", commentList);

        // 5. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", ghmfElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", ghmfElement.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        } catch (NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Calls stored procedure write_fmt_ghmf_element to save details of the GHMF element in the DL DB.
     * URL Path: /format/ghmf/content/element
     *
     * @param ghmfElement
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/ghmf/content/element")
    public Map<String, Object> writeGhmfElementDetails(@RequestBody DlGhmfElement ghmfElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        // 1. save the new GHMF element to the DL_DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", ghmfElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", ghmfElement.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", ghmfElement.getParentObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", ghmfElement.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", ghmfElement.getParentElementObjectId(), Types.INTEGER)
                .addValue("_object_guid", ghmfElement.getElementGUID(), Types.VARCHAR)
                .addValue("_object_name", ghmfElement.getElementName(), Types.VARCHAR)
                .addValue("_object_shortdesc", ghmfElement.getElementDesc(), Types.VARCHAR)
                .addValue("_element_type", ghmfElement.getElementType(), Types.VARCHAR)
                .addValue("_object_config", ghmfElement.getObjectConfig(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        Integer returnId = (Integer) out.get("_return_id");

        // 2. instantiate a new SMD Hierarchy Element object to return to invoking function
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
        in = new MapSqlParameterSource()
                .addValue("_object_id", returnId, Types.INTEGER);
        out = jdbcCall.execute(in);

        DlGhmfElement element = new DlGhmfElement(
                (Integer) out.get("_parent_object_id"),
                (Integer) out.get("_parent_class_id"),
                returnId,
                (Integer) out.get("_class_id"),
                out.get("_element_type").toString(),
                (Integer) out.get("_parent_element_object_id"),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_hierarchy_number").toString(),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString()
        );
        element.setElementGUID(out.get("_object_guid").toString());

        // 3. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        element.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        results.put("result_data", element);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * Calls stored procedure write_ghmf_element_move to change the location of the selected element in the GHMF hierarchy.
     * URL Path: /format/ghmf/content/element/move
     *
     * @param moveElementDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/ghmf/content/element/move")
    public Map<String, Object> writeGhmfElementMove(@RequestBody DlFormatMoveElementDefinition moveElementDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element_move");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", moveElementDefinition.getObjectId(), Types.INTEGER)
                .addValue("_new_parent_element_object_id", moveElementDefinition.getNewParentObjectId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes an element from a GHMF definition.
     *
     * @param objectId
     * @param userId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/format/ghmf/content/element/{objectId}")
    @ResponseBody
    public Map<String, Object> deleteGhmfElement(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_fmt_ghmf_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Reads from the DL database the details of the JSON node identified by {jsonNodeId}
     * URL Path: /format/json/content/node/{jsonNodeId}
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/format/json/content/node/{objectId}")
    @ResponseBody
    public Map<String, Object> readJsonNodeDetails(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dbResultList = new ArrayList();
        ArrayList associationList = new ArrayList(); // this list will be returned in the results Map

        // 1. get the core attributes of the json node
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlJsonNode jsonNode = new DlJsonNode(
                (Integer) out.get("_parent_object_id"),
                objectId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_parent_element_object_id"),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_type").toString(),
                out.get("_element_hierarchy_number").toString(),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString()
        );
        jsonNode.setJsonNodeGUID(out.get("_object_guid").toString());

        // 1.1. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        jsonNode.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        resultData.put("attributes", jsonNode);

        // 2. get the association for the json node
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", jsonNode.getClassId(), Types.INTEGER)
                .addValue("_object_id", jsonNode.getJsonNodeId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2. add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }

        resultData.put("associations", associationList);

        // 3. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", jsonNode.getClassId(), Types.INTEGER)
                .addValue("_object_id", jsonNode.getJsonNodeId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        } catch (NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Calls stored procedure write_fmt_json_node to save details of a new JSON node in the DL DB.
     * URL Path: /format/json/content/node
     *
     * @param newJsonNode
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/json/content/node")
    public Map<String, Object> writeJsonNode(@RequestBody DlJsonNode newJsonNode, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        DlSqlError sqlError = new DlSqlError();
        Integer resultStatus = 1;   // this indicates status of database results.
        Integer returnId = 0;

        // 1. save the new json element to the DL_DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newJsonNode.getJsonNodeId(), Types.INTEGER)
                .addValue("_class_id", newJsonNode.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newJsonNode.getFormatId(), Types.INTEGER)
                .addValue("_parent_class_id", newJsonNode.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", newJsonNode.getParentElementObjectId(), Types.INTEGER)
                .addValue("_object_guid", newJsonNode.getJsonNodeGUID(), Types.VARCHAR)
                .addValue("_object_name", newJsonNode.getJsonNodeName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newJsonNode.getJsonNodeShortDesc(), Types.VARCHAR)
                .addValue("_element_type", newJsonNode.getJsonNodeType(), Types.VARCHAR)
                .addValue("_object_config", null, Types.VARCHAR);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            returnId = (Integer) out.get("_return_id");
        } catch (UncategorizedSQLException e) {
            resultStatus = -1;
            SQLException sqle = e.getSQLException();
            sqlError.setSqlErrorCode(sqle.getErrorCode());
            sqlError.setSqlErrorState(sqle.getSQLState());
            sqlError.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            resultStatus = -1;
            sqlError.setSqlErrorCode(0);
            sqlError.setSqlErrorState("Unknown SQL Error State.");
            sqlError.setSqlErrorMessage(e.getMessage());
        }


        // 2. instantiate a DlJsonNode object to return to invoker
        DlJsonNode jsonNode = new DlJsonNode();

        if (resultStatus == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
            in = new MapSqlParameterSource()
                    .addValue("_object_id", returnId, Types.INTEGER);
            Map<String, Object> out = jdbcCall.execute(in);

            jsonNode.setFormatId((Integer) out.get("_parent_object_id"));
            jsonNode.setJsonNodeId(returnId);
            jsonNode.setClassId((Integer) out.get("_class_id"));
            jsonNode.setParentElementObjectId((Integer) out.get("_parent_element_object_id"));
            jsonNode.setJsonNodeLevel((Integer) out.get("_element_level"));
            jsonNode.setJsonNodeLevelSequence((Integer) out.get("_element_level_sequence"));
            jsonNode.setJsonNodeType(out.get("_element_type").toString());
            jsonNode.setJsonNodeHierarchyNumber(out.get("_element_hierarchy_number").toString());
            jsonNode.setJsonNodeName(out.get("_object_name").toString());
            jsonNode.setJsonNodeShortDesc(out.get("_object_shortdesc").toString());
            jsonNode.setJsonNodeGUID(out.get("_object_guid").toString());

            // 3. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            jsonNode.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));
        }

        results.put("result_data", jsonNode);
        results.put("result_status", resultStatus);
        results.put("sql_error", sqlError);

        return results;
    }

    /**
     * Calls stored procedure write_json_node_move to change the location of the selected element in the JSON hierarchy.
     *
     * @param moveElementDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/json/content/node/move")
    @ResponseBody
    public Map<String, Object> writeJsonNodeMove(@RequestBody DlFormatMoveElementDefinition moveElementDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. call the json node move stored proc
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element_move");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", moveElementDefinition.getObjectId(), Types.INTEGER)
                .addValue("_new_parent_element_id", moveElementDefinition.getNewParentObjectId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes a node from a JSON definition.
     *
     * @param objectId
     * @param userId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/format/json/content/node/{objectId}")
    @ResponseBody
    public Map<String, Object> deleteJsonNode(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_fmt_json_node");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Reads from the DL DB details of the XML element identified by {objectId}
     * URL Path: /format/xml/content/element/{objectId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/format/xml/content/element/{objectId}")
    @ResponseBody
    public Map<String, Object> readXmlElementDetails(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dbResultList = new ArrayList();
        ArrayList associationList = new ArrayList(); // this list will be returned in the results Map

        // 1. get the core details of the xml element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlXmlElement xmlElement = new DlXmlElement(
                objectId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_parent_object_id"),
                (Integer) out.get("_parent_class_id"),
                (Integer) out.get("_parent_element_object_id"),
                out.get("_element_type").toString(),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_hierarchy_number").toString(),
                (Boolean) out.get("_element_is_parent"),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString(),
                out.get("_object_guid").toString()
        );

        DlMetaDataContext mdContext = new DlMetaDataContext();
        xmlElement.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        resultData.put("attributes", xmlElement);

        // 2. Get associated metadata objects for the XML element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", xmlElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", xmlElement.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2. add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }

        resultData.put("associations", associationList);

        // 3. get the custom form data for the XML element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", xmlElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", xmlElement.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        } catch (NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL DB details of the XML attribute identified by {objectId}
     * URL Path: /format/xml/content/attribute/{objectId}
     *
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/format/xml/content/attribute/{objectId}")
    @ResponseBody
    public Map<String, Object> readXmlAttributeDetails(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList(); // this list will be returned in the results Map

        // 1. get the attributes of the xml attribute
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_xml_attribute");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap xmlAttributeInfo = (LinkedCaseInsensitiveMap) dbResultList.get(0);
        DlXmlAttribute xmlAttribute = new DlXmlAttribute(
                (Integer) xmlAttributeInfo.get("object_id"),
                (Integer) xmlAttributeInfo.get("class_id"),
                (Integer) xmlAttributeInfo.get("parent_object_id"),
                (Integer) xmlAttributeInfo.get("parent_class_id"),
                xmlAttributeInfo.get("xml_attribute_type").toString(),
                (Integer) xmlAttributeInfo.get("xml_attribute_sequence"),
                xmlAttributeInfo.get("xml_attribute_guid").toString(),
                xmlAttributeInfo.get("xml_attribute_name").toString(),
                xmlAttributeInfo.get("xml_attribute_shortdesc").toString()
        );

        // 2. add the metadata context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        xmlAttribute.setMdContext(mdContext.generateMdContextList(xmlAttributeInfo.get("md_context").toString()));

        resultData.put("attributes", xmlAttribute);

        // 3. get the association for the xml Attribute
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", xmlAttribute.getClassId(), Types.INTEGER)
                .addValue("_object_id", xmlAttribute.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 3.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 3.2. add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }

        resultData.put("associations", associationList);

        // 4. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", xmlAttribute.getClassId(), Types.INTEGER)
                .addValue("_object_id", xmlAttribute.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        } catch (NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Calls stored procedure write_fmt_xml_element to save details of a new XML element in the DL DB.
     * URL Path: /format/xml/content/element
     *
     * @param newElement
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/xml/content/element")
    public Map<String, Object> writeXmlElement(@RequestBody DlXmlElement newElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        Integer returnId = 0;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newElement.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newElement.getParentObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", newElement.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", newElement.getParentElementObjectId(), Types.INTEGER)
                .addValue("_object_guid", newElement.getXmlElementGUID(), Types.VARCHAR)
                .addValue("_object_name", newElement.getXmlElementName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newElement.getXmlElementShortDesc(), Types.VARCHAR)
                .addValue("_element_type", newElement.getXmlElementType(), Types.VARCHAR)
                .addValue("_object_config", "{}", Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        returnId = (Integer) out.get("_return_id");

        // 2. instantiate a DlXmlElement object to return to invoker
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
        in = new MapSqlParameterSource()
                .addValue("_object_id", returnId, Types.INTEGER);
        out = jdbcCall.execute(in);

        DlXmlElement xmlElement = new DlXmlElement (
                returnId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_parent_object_id"),
                (Integer) out.get("_parent_class_id"),
                (Integer) out.get("_parent_element_object_id"),
                out.get("_element_type").toString(),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_hierarchy_number").toString(),
                (Boolean) out.get("_element_is_parent"),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString(),
                out.get("_object_guid").toString()
        );

        // 3. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        xmlElement.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        results.put("result_data", xmlElement);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes an element/attribute from an XML definition.
     *
     * @param objectId
     * @param userId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/format/xml/content/element/{objectId}")
    @ResponseBody
    public Map<String, Object> deleteXmlElement(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_fmt_xml_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns summary information and all elements for the flat file format definition identified by {objectId}
     * URL Path: /format/flatfile/content/{objectId}
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/format/flatfile/content/{objectId}")
    @ResponseBody
    public Map<String, Object> readFlatfileFormatContent(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dbResultList = new ArrayList();
        ArrayList elementList = new ArrayList(); // this list will be returned in the results Map

        // 1. get the summary information for the flat file format
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_flatfile_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlFormat format = new DlFormat(
                objectId,         // formatId
                (Integer) out.get("_class_id"),       // classId
                out.get("_class_name").toString(),    // className
                out.get("_object_name").toString(),      // formatName
                out.get("_object_shortdesc").toString()  // formatShortDesc
        );
        format.setObjectGUID(out.get("_object_guid").toString());
        format.setObjectConfig(out.get("_object_config").toString());

        // 1.1. get the class configuration details
        DlMoClassConfig classConfig = new DlMoClassConfig();
        classConfig.setClassConfigId((Integer) out.get("_class_config_id"));
        classConfig.setConfigSchemaDefn(out.get("_config_schema_defn").toString());
        classConfig.setConfigFormDefn(out.get("_config_form_defn").toString());
        classConfig.setClassConfigData(out.get("_object_config").toString());
        format.setClassConfig(classConfig);

        // 1.2. get the custom form details.
        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        format.setCustomForm(customForm);

        resultData.put("format_summary", format);

        // 2. get all elements for the flat file format
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_flatfile_elements");
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        // 2.1. instantiate a DlFlatFileElement object for each row returned from DL DB
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            DlFlatFileElement element = new DlFlatFileElement(
                    (Integer) elementInfo.get("object_id"),
                    (Integer) elementInfo.get("class_id"),
                    (Integer) elementInfo.get("parent_object_id"),
                    (Integer) elementInfo.get("parent_class_id"),
                    (Integer) elementInfo.get("element_sequence"),
                    elementInfo.get("element_datatype").toString(),
                    elementInfo.get("object_guid").toString(),
                    elementInfo.get("object_name").toString());

            if (elementInfo.get("object_shortdesc") != null)
                element.setObjectShortDesc(elementInfo.get("object_shortdesc").toString());

            // 2.2. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

            elementList.add(element);
        }

        resultData.put("format_elements", elementList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from DL DB the details of the Flat File format element identified by {objectId}
     * URL Path: /format/flatfile/content/element/{objectId}
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/format/flatfile/content/element/{objectId}")
    @ResponseBody
    public Map<String, Object> readFlatFileElementDetails(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // list list is returned in the results Map

        // 1. get the core attributes of the directory
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_flatfile_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.BIGINT);
        Map<String, Object> out = jdbcCall.execute(in);

        DlFlatFileElement element = new DlFlatFileElement(
                objectId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_parent_object_id"),
                (Integer) out.get("_parent_class_id"),
                (Integer) out.get("_element_sequence"),
                out.get("_element_datatype").toString(),
                out.get("_object_guid").toString(),
                out.get("_object_name").toString());

        element.setFwfStartPosition((Integer) out.get("_fwf_start_pos"));
        element.setFwfLength((Integer) out.get("_fwf_length"));
        element.generateFwfEndPosition();

        if (out.get("_object_shortdesc") != null)
            element.setObjectShortDesc(out.get("_object_shortdesc").toString());
        if (out.get("_object_desc_reveng") != null)
            element.setObjectDescReveng(out.get("_object_desc_reveng").toString());

        DlMetaDataContext mdContext = new DlMetaDataContext();
        element.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        resultData.put("attributes", element);

        // 2. get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", element.getClassId(), Types.INTEGER)
                .addValue("_object_id", element.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2. add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 3. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", element.getClassId(), Types.INTEGER)
                .addValue("_object_id", element.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        } catch (NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);

        // 4. get the comments for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", element.getObjectId(), Types.INTEGER)
                .addValue("_class_id", element.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }

        resultData.put("comments", commentList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Calls stored procedure write_fmt_flatfile_element to save details of a new Flat File element in the DL DB.
     * URL Path: /format/flatfile/content/element
     *
     * @param newElement
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/flatfile/content/element")
    public Map<String, Object> writeFlatFileElement(@RequestBody DlFlatFileElement newElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        DlSqlError sqlError = new DlSqlError();
        Integer resultStatus = 1;   // this indicates status of database results.

        // 1. save the new DlFlatFileElement to the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_flatfile_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newElement.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newElement.getParentObjectId(), Types.INTEGER)
                .addValue("_fwf_length", newElement.getFwfLength(), Types.INTEGER)
                .addValue("_object_name", newElement.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newElement.getObjectShortDesc(), Types.VARCHAR)
                .addValue("_element_datatype", newElement.getElementDatatype(), Types.VARCHAR);
        try {
            Map<String, Object> out = jdbcCall.execute(in);

            resultData.put("return_id", (Integer) out.get("_return_id"));
            resultData.put("fwf_end_pos", (Integer) out.get("_fwf_end_pos"));
        }
        catch (UncategorizedSQLException e) {
            resultStatus = -1;
            SQLException sqle = e.getSQLException();
            sqlError.setSqlErrorCode(sqle.getErrorCode());
            sqlError.setSqlErrorState(sqle.getSQLState());
            sqlError.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            resultStatus = -1;
            sqlError.setSqlErrorCode(0);
            sqlError.setSqlErrorState("Unknown.");
            sqlError.setSqlErrorMessage(e.getMessage());
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        results.put("sql_error", sqlError);

        return results;
    }

    /**
     * Calls stored procedure write_fmt_flatfile_element to save details of a new Flat File element in the DL DB.
     * URL Path: /format/flatfile/content/element/move
     *
     * @param moveElementDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/format/flatfile/content/element/move")
    public Map<String, Object> writeFlatFileElementMove(@RequestBody DlFormatMoveElementDefinition moveElementDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // 1. save the new DlFlatFileElement to the DL DB
        // TODO: add Exception handling
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_flatfile_element_move");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", moveElementDefinition.getObjectId(), Types.INTEGER)
                .addValue("_target_object_id", moveElementDefinition.getTargetObjectId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes an element from a Flat File format definition.
     *
     * @param objectId
     * @param userId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/format/flatfile/content/element/{objectId}")
    @ResponseBody
    public Map<String, Object> deleteFlatFileElement(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_fmt_flatfile_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

}