/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlXmlElement {

    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private Integer parentElementObjectId;
    private String xmlElementType;
    private Integer xmlElementLevel;
    private Integer xmlElementLevelSequence;
    private String xmlElementHierarchyNumber;
    private Boolean xmlElementIsParent;
    private String xmlElementName;
    private String xmlElementShortDesc;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private ArrayList<DlXmlAttribute> attributes;
    private Boolean isAttribute;
    private String xmlElementGUID;
    private Integer xmlElementVisible;
    private Integer initialExpandLevel;
    private Boolean expanded;
    private Integer xmlElementSelected;
    private Boolean xmlElementMatched;
    private Integer resultSetSequence;

    public DlXmlElement() {
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentElementObjectId = 0;
        this.xmlElementType = "";
        this.xmlElementLevel = 0;
        this.xmlElementLevelSequence = 0;
        this.xmlElementHierarchyNumber = "";
        this.xmlElementIsParent = false;
        this.xmlElementName = "";
        this.xmlElementShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.attributes = new ArrayList<DlXmlAttribute>();
        this.isAttribute = false;
        this.xmlElementGUID = "";
        this.xmlElementVisible = 0;
        this.initialExpandLevel = 0;
        this.expanded = false;
        this.xmlElementSelected = 0;
        this.xmlElementMatched = false;
        this.resultSetSequence = 0;
    }

    public DlXmlElement(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                        Integer parentElementObjectId, String xmlElementType, Integer xmlElementLevel,
                        Integer xmlElementLevelSequence, String xmlElementHierarchyNumber, Boolean xmlElementIsParent,
                        String xmlElementName, String xmlElementShortDesc, String xmlElementGUID) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.parentElementObjectId = parentElementObjectId;
        this.xmlElementType = xmlElementType;
        this.xmlElementLevel = xmlElementLevel;
        this.xmlElementLevelSequence = xmlElementLevelSequence;
        this.xmlElementHierarchyNumber = xmlElementHierarchyNumber;
        this.xmlElementIsParent = xmlElementIsParent;
        this.xmlElementName = xmlElementName;
        this.xmlElementShortDesc = xmlElementShortDesc;
        this.xmlElementGUID = xmlElementGUID;
        this.xmlElementVisible = 0;
        this.initialExpandLevel = 2;
        this.expanded = false;
        this.xmlElementSelected = 0;
        this.xmlElementMatched = false;
        this.resultSetSequence = 0;
        this.attributes = new ArrayList<DlXmlAttribute>();
        // TODO: this needs to be replaced with a Metadata Object Class Service
        if (classId == 38)
            this.isAttribute = true;
        else
            this.isAttribute = false;

        this.determineVisibility();
        this.determineExpanded();
    }

    public void setXMLElementGUID() {
        this.xmlElementGUID = this.parentObjectId.toString() + ":" + this.objectId.toString();
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getParentElementObjectId() {
        return parentElementObjectId;
    }

    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.parentElementObjectId = parentElementObjectId;
    }

    public void setXmlElementLevel(Integer xmlElementLevel) {
        this.xmlElementLevel = xmlElementLevel;
    }

    public void setXmlElementLevelSequence(Integer xmlElementLevelSequence) {
        this.xmlElementLevelSequence = xmlElementLevelSequence;
    }

    public void setXmlElementGUID(String xmlElementGUID) {
        this.xmlElementGUID = xmlElementGUID;
    }

    public void setXmlElementVisible(Integer xmlElementVisible) {
        this.xmlElementVisible = xmlElementVisible;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public void setXmlElementSelected(Integer xmlElementSelected) {
        this.xmlElementSelected = xmlElementSelected;
    }

    public void determineVisibility() {
        xmlElementVisible = (xmlElementLevel <= initialExpandLevel) ? 1 : 0;
    }

    public void determineExpanded() {
        this.expanded = (xmlElementLevel < initialExpandLevel) ? true : false;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public Integer getXmlElementVisible() {
        return xmlElementVisible;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public Integer getXmlElementSelected() {
        return xmlElementSelected;
    }

    public Boolean getXmlElementMatched() {
        return xmlElementMatched;
    }

    public void setXmlElementMatched(Boolean xmlElementMatched) {
        this.xmlElementMatched = xmlElementMatched;
    }

    public String getXmlElementGUID() {
        return xmlElementGUID;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public String getXmlElementType() {
        return xmlElementType;
    }

    public void setXmlElementType(String xmlElementType) {
        this.xmlElementType = xmlElementType;
    }

    public Integer getXmlElementLevel() {
        return xmlElementLevel;
    }

    public Integer getXmlElementLevelSequence() {
        return xmlElementLevelSequence;
    }

    public String getXmlElementHierarchyNumber() {
        return xmlElementHierarchyNumber;
    }

    public void setXmlElementHierarchyNumber(String xmlElementHierarchyNumber) {
        this.xmlElementHierarchyNumber = xmlElementHierarchyNumber;
    }

    public Boolean getXmlElementIsParent() {
        return xmlElementIsParent;
    }

    public void setXmlElementIsParent(Boolean xmlElementIsParent) {
        this.xmlElementIsParent = xmlElementIsParent;
    }

    public String getXmlElementName() {
        return xmlElementName;
    }

    public void setXmlElementName(String xmlElementName) {
        this.xmlElementName = xmlElementName;
    }

    public String getXmlElementShortDesc() {
        return xmlElementShortDesc;
    }

    public void setXmlElementShortDesc(String xmlElementShortDesc) {
        this.xmlElementShortDesc = xmlElementShortDesc;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public ArrayList<DlXmlAttribute> getAttributes() {
        return attributes;
    }

    public Boolean getAttribute() {
        return isAttribute;
    }

    public void setAttribute(Boolean attribute) {
        isAttribute = attribute;
    }

    public void setAttributes(ArrayList<DlXmlAttribute> attributes) {
        this.attributes = attributes;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }
}
