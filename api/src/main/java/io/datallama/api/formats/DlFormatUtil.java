/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlMoClassConfig;
import io.datallama.api.common.MoClassConstants;
import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.smd.SmdCrudResult;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DlFormatUtil {
    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;

    /**
     * Constructor 01: dataSource and userId parameters. Used by POST & DELETE functions in the REST Controller class.
     *
     * @param dataSource
     * @param userId
     */
    public DlFormatUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Constructor 02: dataSource parameter only. Used by GET functions in the REST Controller class.
     *
     * @param dataSource
     */
    public DlFormatUtil(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Reads a format definition from the DL DB, including associations, custom form, etc.
     * TODO: code can be rationalised between readFormatSummaryByObjectId and readFormatSummaryByGUID
     * @param objectId
     * @return
     */
    public Map<String, Object> readFormatSummaryByObjectId(Integer objectId, Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dbResultList = new ArrayList();
        ArrayList associationList = new ArrayList();

        // 1. get the summary information for the format
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fmt_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id_in", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlFormat format = new DlFormat(
                objectId,         // formatId
                (Integer) out.get("_class_id"),       // classId
                out.get("_class_name").toString(),    // className
                out.get("_object_name").toString(),      // formatName
                out.get("_object_shortdesc").toString() // formatShortDesc
        );
        format.setObjectGUID(out.get("_object_guid").toString());
        if (out.get("_object_config") != null) {
            format.setObjectConfig(out.get("_object_config").toString());
        }

        // 1.1. get the class configuration details
        DlMoClassConfig classConfig = new DlMoClassConfig();
        classConfig.setClassConfigId((Integer) out.get("_class_config_id"));
        classConfig.setConfigSchemaDefn(out.get("_config_schema_defn").toString());
        classConfig.setConfigFormDefn(out.get("_config_form_defn").toString());
        if (out.get("_object_config") != null) {
            classConfig.setClassConfigData(out.get("_object_config").toString());
        }
        format.setClassConfig(classConfig);

        // 1.2. get the custom form details
        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        format.setCustomForm(customForm);

        resultData.put("format_summary", format);

        // 3: get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource()
                .addValue("_class_id", format.getClassId(), Types.INTEGER)
                .addValue("_object_id", format.getFormatId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context for the AssociatedObject instance
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);
        }
        resultData.put("associations", associationList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the format definition identified by the GUID objectGUID from the DL_DB, including associations, custom form, etc.
     * TODO: code can be rationalised between readFormatSummaryByObjectId and readFormatSummaryByGUID
     * @param objectGUID
     * @return
     */
    public Map<String, Object> readFormatSummaryByGUID(String objectGUID) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> results02 = new HashMap<>();

        Integer resultStatus = 1; // this indicates status of database results.
        Integer objectId = 0;
        Integer classId = 0;

        // 1. get the object and class Ids for the format
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_format_ids_by_guid");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_guid", objectGUID, Types.VARCHAR);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            smdCrudResult.setReturnId((Integer) out.get(1));

            objectId = (Integer) out.get("_object_id");
            if (objectId == null) objectId = 0;
            classId = (Integer) out.get("_class_id");
            if (classId == null) classId = 0;
        } catch (UncategorizedSQLException e) {
            smdCrudResult.setResultStatus(-1);
            SQLException sqle = e.getSQLException();
            smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
            smdCrudResult.setSqlErrorState(sqle.getSQLState());
            smdCrudResult.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            smdCrudResult.setResultStatus(-1);
            smdCrudResult.setSqlErrorCode(0);
            smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
            smdCrudResult.setSqlErrorMessage(e.getMessage());
        }

        if (smdCrudResult.getResultStatus() == 1) {
            results02 = readFormatSummaryByObjectId(objectId, classId); // TODO: this call should be inside a try catch block
            results.put("result_data", results02.get("result_data"));
        }

        results.put("result_status", smdCrudResult.getResultStatus());
        results.put("sql_error", smdCrudResult.getSqlError());

        return results;
    }
}
