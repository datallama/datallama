/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.formats;

public class DlFormatClass {
    private Integer classId;
    private String className;
    private String classDesc;
    private String objectTable;
    private String objectDetailState;

    public DlFormatClass() {
        this.classId = 0;
        this.className = "";
        this.classDesc = "";
        this.objectTable = "";
        this.objectDetailState = "";
    }

    public DlFormatClass(Integer classId, String className, String classDesc,
                         String objectTable, String objectDetailState) {
        this.classId = classId;
        this.className = className;
        this.classDesc = classDesc;
        this.objectTable = objectTable;
        this.objectDetailState = objectDetailState;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassDesc() {
        return classDesc;
    }

    public void setClassDesc(String classDesc) {
        this.classDesc = classDesc;
    }

    public String getObjectTable() {
        return objectTable;
    }

    public void setObjectTable(String objectTable) {
        this.objectTable = objectTable;
    }

    public String getObjectDetailState() {
        return objectDetailState;
    }

    public void setObjectDetailState(String objectDetailState) {
        this.objectDetailState = objectDetailState;
    }
}
