/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.dsdf;

public class DlReport{
    private Integer reportId;
    private String reportName;
    private String reportShortDesc;
    private String objectFilter;
    private String columnSet;

    public DlReport(Integer reportId, String reportName, String reportShortDesc, String objectFilter, String columnSet) {
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportShortDesc = reportShortDesc;
        this.objectFilter = objectFilter;
        this.columnSet = columnSet;
    }

    public DlReport() {
        this.reportId = 0;
        this.reportName = "";
        this.reportShortDesc = "";
        this.objectFilter = "";
        this.columnSet = "";
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportShortDesc() {
        return reportShortDesc;
    }

    public void setReportShortDesc(String reportShortDesc) {
        this.reportShortDesc = reportShortDesc;
    }

    public String getObjectFilter() {
        return objectFilter;
    }

    public void setObjectFilter(String objectFilter) {
        this.objectFilter = objectFilter;
    }

    public String getColumnSet() {
        return columnSet;
    }

    public void setColumnSet(String columnSet) {
        this.columnSet = columnSet;
    }
}
