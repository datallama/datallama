/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * TODO: This script needs to be renamed to LandscapeObjectController.java
 */
package io.datallama.api.dsdf;

import java.sql.Types;
import java.util.*;

import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlMoClass;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.jdbc.core.namedparam.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DataStoreController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /dsdf/datastore/list
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/datastore/list")
    @ResponseBody
    public Map<String, Object> readDatastoreList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_all_datastores_summary");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList datastoreList = (ArrayList) out.get("#result-set-1");

        results.put("result_data", datastoreList);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * URL Path: GET /dsdf/datastore/summary/{datastoreId}
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/datastore/summary/{objectId}")
    @ResponseBody
    public Map<String, Object> readDatastoreSummary(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. get the core details for the data store
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_landscape_object_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap loInfo = (LinkedCaseInsensitiveMap) rslt.get(0);
        DlLandscapeObject landscapeObject = new DlLandscapeObject((Integer) loInfo.get("class_id"), (Integer) loInfo.get("object_id"),
                (Integer) loInfo.get("parent_class_id"), (Integer) loInfo.get("parent_object_id"), loInfo.get("object_name").toString());
        landscapeObject.setClassName(loInfo.get("class_name").toString());
        landscapeObject.setObjectShortDesc(loInfo.get("object_shortdesc").toString());
        landscapeObject.setBusinessContact(loInfo.get("business_contact").toString());
        landscapeObject.setTechnicalContact(loInfo.get("technical_contact").toString());
        // TODO: add hosted realm

        DlCustomForm customForm = new DlCustomForm((Integer) loInfo.get("custom_form_id"), loInfo.get("custom_form_name").toString(),
                loInfo.get("schema_definition").toString(), loInfo.get("form_definition").toString());
        if (loInfo.get("custom_form_data") == null)
            customForm.setFormData("{}");
        else
            customForm.setFormData(loInfo.get("custom_form_data").toString());

        customForm.setClassId(landscapeObject.getClassId());
        customForm.setObjectId(landscapeObject.getObjectId());
        landscapeObject.setCustomForm(customForm);

        resultData.put("core_details", landscapeObject);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /dsdf/datastore/class/list
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/datastore/class/list")
    public Map<String, Object> getDatastoreClassList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList<DlMoClass> datastoreClassList = new ArrayList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_datastore_classes");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap classInfo = (LinkedCaseInsensitiveMap) listElement;
            DlMoClass moClass = new DlMoClass(
                    (Integer) classInfo.get("class_id"),
                    classInfo.get("class_name").toString(),
                    classInfo.get("class_shortdesc").toString(),
                    classInfo.get("object_table").toString(),
                    classInfo.get("object_detail_state").toString()
            );
            datastoreClassList.add(moClass);
        }

        results.put("result_data", datastoreClassList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: POST /dsdf/datastore
     *
     * @return
     */
    @RequestMapping(method = POST, value = "/dsdf/datastore")
    @ResponseBody
    public Map<String, Object> writeDatastore(@RequestBody DlLandscapeObject datastoreDetail, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_landscape_object");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", datastoreDetail.getObjectId(), Types.INTEGER)
                .addValue("_class_id", datastoreDetail.getClassId(), Types.INTEGER)
                .addValue("_object_name", datastoreDetail.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc",  datastoreDetail.getObjectShortDesc(), Types.VARCHAR)
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnId", out.get("_return_id").toString());
        if ((Integer) out.get("_return_id") == 0) {
            resultStatus = -1;
            String msg = "A landscape object with this name already exists.\nAdd the landscape object to the diagram using the Landscape Object -> Add/Remove Objects menu command.";
            resultData.put("errorMessage", msg);
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * URL Path: /dsdf/datastore/detail/{datastoreId}
     * Reads the details of a data store from the DL database. First it reads core details. Second it the list of data flows
     * associated with the data store. Third it reads the list of diagrams in which the data store is included. Fourth,
     * it reads the list of SMD databases associated with the data store.
     * @return
     * TODO: This should be returning DlLandscapeObject objects rather than simply passing through the stored proc result
     */
    @RequestMapping(method = GET, value = "/dsdf/datastore/detail/{objectId}")
    @ResponseBody
        public Map<String, Object> readDatastoreDetail(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SqlParameterSource in_object_id = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        SqlParameterSource in_datastore_id = new MapSqlParameterSource().addValue("_datastore_id", objectId, Types.INTEGER);

        // 1. get the core details for the data store
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_landscape_object_summary");
        Map<String, Object> out = jdbcCall.execute(in_object_id);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap loInfo = (LinkedCaseInsensitiveMap) rslt.get(0);
        DlLandscapeObject landscapeObject = new DlLandscapeObject((Integer) loInfo.get("class_id"), (Integer) loInfo.get("object_id"),
                (Integer) loInfo.get("parent_class_id"), (Integer) loInfo.get("parent_object_id"), loInfo.get("object_name").toString());
        landscapeObject.setClassName(loInfo.get("class_name").toString());
        landscapeObject.setObjectShortDesc(loInfo.get("object_shortdesc").toString());
        landscapeObject.setBusinessContact(loInfo.get("business_contact").toString());
        landscapeObject.setTechnicalContact(loInfo.get("technical_contact").toString());
        landscapeObject.setIconSvgString(loInfo.get("icon_svg_string").toString());
        // TODO: add hosted realm

        DlCustomForm customForm = new DlCustomForm((Integer) loInfo.get("custom_form_id"), loInfo.get("custom_form_name").toString(),
                loInfo.get("schema_definition").toString(), loInfo.get("form_definition").toString());
        if (loInfo.get("custom_form_data") == null) {
            customForm.setFormData("{}");
        }
        else {
            customForm.setFormData(loInfo.get("custom_form_data").toString());
        }
        customForm.setClassId(landscapeObject.getClassId());
        customForm.setObjectId(landscapeObject.getObjectId());
        landscapeObject.setCustomForm(customForm);

        resultData.put("core_details", landscapeObject);

        // 2. get the list of data flows associated with this data store, both as origin and target.
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_datastore_dataflows");
        out = jdbcCall.execute(in_datastore_id);
        ArrayList dataflowList = (ArrayList) out.get("#result-set-1");
        resultData.put("dataflows", dataflowList);

        // 3. get the list of diagrams in which this data store is included.
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_datastore_diagrams");
        out = jdbcCall.execute(in_datastore_id);
        ArrayList diagramList = (ArrayList) out.get("#result-set-1");
        resultData.put("diagrams", diagramList);

        // 4. get the list of SMD databases associated with the datastore
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", landscapeObject.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList<AssociatedObject> associations = new ArrayList<>();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");
        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 4.1 create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));
            associations.add(assocObject);
        }
        resultData.put("associations", associations);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     *
     * @param smdDatabaseEdits
     * @return
     */
    @RequestMapping(method = POST, value = "/dsdf/datastore/smd_database")
    @ResponseBody
    public Map<String, Object> writeDatastoreSmdDatabase(@RequestBody DatastoreSmdDatabaseEdits smdDatabaseEdits) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_datastore_smd_database");

        for (Object listElement : smdDatabaseEdits.getSmdDbEdits()) {
            LinkedHashMap editInfo = (LinkedHashMap) listElement;
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("_ntt_id", smdDatabaseEdits.getDatastoreId(), Types.INTEGER)
                    .addValue("_smd_database_id", (Integer) editInfo.get("databaseId"), Types.INTEGER)
                    .addValue("_action", (Integer) editInfo.get("action"), Types.INTEGER);
            Map<String, Object> out = jdbcCall.execute(in);
        }

        // TODO: need some success/failure checks here
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * Writes the custom form data for a landscape object to the DL database
     * @param customFormData
     * @param userId
     * @return
     * TODO: this is redundant. Delete it
     *
     */
    @RequestMapping(method = POST, value = "/dsdf/datastore/customformdata")
    public Map<String, Object> writeLandscapeObjectCustomFormData(@RequestBody DlCustomForm customFormData,
                                                                  @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_landscape_object_custom_form_data");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", customFormData.getObjectId(), Types.INTEGER)
                .addValue("_custom_form_data", customFormData.getFormData(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /dsdf/datastore/persistence-technology/list
     * TODO: move to ReferenceDataController
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/datastore/persistence-technology/list")
    @ResponseBody
    public Map<String, Object> readPersistenceTechnologyHierarchy() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList persistenceTechnologyList = new ArrayList(); // this list will be returned in the results Map
        Map<Integer, PersistenceTechnologyDefinition> ptLookup = new HashMap<>(); // this is used to lookup parent PTs when adding children

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_pt_hierarchy");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList ptDbResultList = (ArrayList) out.get("#result-set-1");

        // second: create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listElement : ptDbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap ptInfo = (LinkedCaseInsensitiveMap) listElement;
            PersistenceTechnologyDefinition ptDefinition = new PersistenceTechnologyDefinition(
                    (Integer) ptInfo.get("pt_id"),
                    (Integer) ptInfo.get("pt_parent_id"),
                    (Integer) ptInfo.get("pt_level"),
                    (Integer) ptInfo.get("pt_level_sequence"),
                    ptInfo.get("pt_hierarchy_number").toString(),
                    ptInfo.get("pt_name").toString(),
                    ptInfo.get("pt_desc").toString());

            if (ptDefinition.getPtParentId() == 0) {
                persistenceTechnologyList.add(ptDefinition);
            } else {
                // add ptDefinition object to its parent
                PersistenceTechnologyDefinition parent = ptLookup.get(ptDefinition.getPtParentId());
                parent.addChild(ptDefinition);
            }
            // add this ptDefinition to the lookup map
            ptLookup.put(ptDefinition.getPtId(), ptDefinition);
        }

        results.put("result_data", persistenceTechnologyList);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/dsdf/dataflow/{objectId}")
    public Map<String, Object> deleteDataflow(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        //Todo: Put all the deleted database stuff in a separate archive.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_dataflow");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/dsdf/datastore/{objectId}")
    public Map<String, Object> deleteLandscapeObject(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        //Todo: Put all the deleted database stuff in a separate archive.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_landscape_object");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /dsdf/datastore/hosted-realm/list
     * TODO: move to ReferenceDataController
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/datastore/hosted-realm/list")
    @ResponseBody
    public Map<String, Object> readHostedRealmHierarchy() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList hostedRealmList = new ArrayList(); // this list will be returned in the results Map
        Map<Integer, HostedRealmDefinition> hrLookup = new HashMap<>(); // this is used to lookup parent PTs when adding children

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_hr_hierarchy");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList hrDbResultList = (ArrayList) out.get("#result-set-1");

        // second: create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listElement : hrDbResultList) {
            LinkedCaseInsensitiveMap hrInfo = (LinkedCaseInsensitiveMap) listElement;

            HostedRealmDefinition hrDefinition = new HostedRealmDefinition(
                    (Integer) hrInfo.get("hr_id"),
                    (Integer) hrInfo.get("hr_parent_id"),
                    (Integer) hrInfo.get("hr_level"),
                    (Integer) hrInfo.get("hr_level_sequence"),
                    hrInfo.get("hr_hierarchy_number").toString(),
                    hrInfo.get("hr_name").toString(),
                    hrInfo.get("hr_desc").toString());

            if (hrDefinition.getHrParentId() == 0) {
                hostedRealmList.add(hrDefinition);
            } else {
                // add hrDefinition object to its parent
                HostedRealmDefinition parent = hrLookup.get(hrDefinition.getHrParentId());
                parent.addChild(hrDefinition);
            }
            // add this ptDefinition to the lookup map
            hrLookup.put(hrDefinition.getHrId(), hrDefinition);
        }

        results.put("result_data", hostedRealmList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * PersistenceTechnologyDefinition models a definition of a persistence technology.
     */
    static class PersistenceTechnologyDefinition {
        private Integer ptId;
        private Integer ptParentId;
        private Integer ptLevel;
        private Integer ptLevelSequence;
        private String ptHierarchyNumber;
        private String ptName;
        private String ptDesc;
        private String label;
        private ArrayList<PersistenceTechnologyDefinition> children;

        public PersistenceTechnologyDefinition(Integer ptId, Integer ptParentId, Integer ptLevel, Integer ptLevelSequence,
                                               String ptHierarchyNumber, String ptName, String ptDesc) {
            this.ptId = ptId;
            this.ptParentId = ptParentId;
            this.ptLevel = ptLevel;
            this.ptLevelSequence = ptLevelSequence;
            this.ptHierarchyNumber = ptHierarchyNumber;
            this.ptName = ptName;
            this.ptDesc = ptDesc;
            this.label = ptName;
            this.children = new ArrayList<PersistenceTechnologyDefinition>();
        }

        public Integer getPtId() {
            return ptId;
        }

        public void setPtId(Integer ptId) {
            this.ptId = ptId;
        }

        public Integer getPtParentId() {
            return ptParentId;
        }

        public void setPtParentId(Integer ptParentId) {
            this.ptParentId = ptParentId;
        }

        public Integer getPtLevel() {
            return ptLevel;
        }

        public void setPtLevel(Integer ptLevel) {
            this.ptLevel = ptLevel;
        }

        public Integer getPtLevelSequence() {
            return ptLevelSequence;
        }

        public void setPtLevelSequence(Integer ptLevelSequence) {
            this.ptLevelSequence = ptLevelSequence;
        }

        public String getPtHierarchyNumber() {
            return ptHierarchyNumber;
        }

        public void setPtHierarchyNumber(String ptHierarchyNumber) {
            this.ptHierarchyNumber = ptHierarchyNumber;
        }

        public String getPtName() {
            return ptName;
        }

        public void setPtName(String ptName) {
            this.ptName = ptName;
        }

        public String getPtDesc() {
            return ptDesc;
        }

        public void setPtDesc(String ptDesc) {
            this.ptDesc = ptDesc;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public ArrayList<PersistenceTechnologyDefinition> getChildren() {
            return children;
        }

        public void setChildren(ArrayList<PersistenceTechnologyDefinition> children) {
            this.children = children;
        }

        public void addChild(PersistenceTechnologyDefinition ptDefinition) {
            this.children.add(ptDefinition);
        }

    }

    /**
     * HostedRealmDefinition models a definition of a hosted realm
     */
    static class HostedRealmDefinition {
        private Integer hrId;
        private Integer hrParentId;
        private Integer hrLevel;
        private Integer hrLevelSequence;
        private String hrHierarchyNumber;
        private String hrName;
        private String hrDesc;
        private String label;
        private ArrayList<HostedRealmDefinition> children;

        public HostedRealmDefinition(Integer hrId, Integer hrParentId, Integer hrLevel, Integer hrLevelSequence,
                                     String hrHierarchyNumber, String hrName, String hrDesc) {
            this.hrId = hrId;
            this.hrParentId = hrParentId;
            this.hrLevel = hrLevel;
            this.hrLevelSequence = hrLevelSequence;
            this.hrHierarchyNumber = hrHierarchyNumber;
            this.hrName = hrName;
            this.hrDesc = hrDesc;
            this.label = hrName;
            this.children = new ArrayList<HostedRealmDefinition>();
        }

        public Integer getHrId() {
            return hrId;
        }

        public void setHrId(Integer hrId) {
            this.hrId = hrId;
        }

        public Integer getHrParentId() {
            return hrParentId;
        }

        public void setHrParentId(Integer hrParentId) {
            this.hrParentId = hrParentId;
        }

        public Integer getHrLevel() {
            return hrLevel;
        }

        public void setHrLevel(Integer hrLevel) {
            this.hrLevel = hrLevel;
        }

        public Integer getHrLevelSequence() {
            return hrLevelSequence;
        }

        public void setHrLevelSequence(Integer hrLevelSequence) {
            this.hrLevelSequence = hrLevelSequence;
        }

        public String getHrHierarchyNumber() {
            return hrHierarchyNumber;
        }

        public void setHrHierarchyNumber(String hrHierarchyNumber) {
            this.hrHierarchyNumber = hrHierarchyNumber;
        }

        public String getHrName() {
            return hrName;
        }

        public void setHrName(String hrName) {
            this.hrName = hrName;
        }

        public String getHrDesc() {
            return hrDesc;
        }

        public void setHrDesc(String hrDesc) {
            this.hrDesc = hrDesc;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public ArrayList<HostedRealmDefinition> getChildren() {
            return children;
        }

        public void setChildren(ArrayList<HostedRealmDefinition> children) {
            this.children = children;
        }

        public void addChild(HostedRealmDefinition hrDefinition) {
            this.children.add(hrDefinition);
        }

    }

    /**
     * DatastoreSmdDatabaseEdits models the changes made to associated SMD databsaes in the Datastore details view.
     */
    static class DatastoreSmdDatabaseEdits {
        private Integer datastoreId;
        private ArrayList<Object> smdDbEdits;

        public Integer getDatastoreId() {
            return datastoreId;
        }

        public void setDatastoreId(Integer datastoreId) {
            this.datastoreId = datastoreId;
        }

        public ArrayList<Object> getSmdDbEdits() {
            return smdDbEdits;
        }

        public void setSmdDbEdits(ArrayList<Object> smdDbEdits) {
            this.smdDbEdits = smdDbEdits;
        }
    }
}