/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.dsdf;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DsdfReportController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /*
     ** URL Path: /dsdf/report/content/{reportId}
     */
    @RequestMapping(method = GET, value = "/dsdf/report/content/{reportId}")
    public Map<String, Object> readReportContent(@PathVariable Long reportId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> result_data = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the json arrays returned by stored procedure

        //get the report definition
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_report");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_report_id", reportId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        Map<String, Object> reportDefinition;
        try{
            ArrayList<Map<String, Object>> temp = (ArrayList<Map<String, Object>>) out.get("#result-set-1");
            reportDefinition = temp.get(0);
        }
        catch (Exception e){
            result_data.put("message","Sorry! Something went wrong while trying to read the report definition. Check the console for more information.");
            result_data.put("exception",e.getMessage());
            results.put("result_data", result_data);
            results.put("result_status", -11);
            System.out.println(e.toString());
            return results;
        }

        //get all the data landscape objects
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_all_landscape_objects");
        out = jdbcCall.execute();
        ArrayList<Map<String, Object>> objects;
        try{
            objects = (ArrayList<Map<String, Object>>) out.get("#result-set-1");
        }
        catch (Exception e){
            result_data.put("message","Sorry! Something went wrong while trying to read the objects. Check the console for more information.");
            result_data.put("exception",e.getMessage());
            results.put("result_data", result_data);
            results.put("result_status", -11);
            System.out.println(e.toString());
            return results;
        }


        //get the object filters
        List<Map<String,Object>> filters;
        try {
            filters = objectMapper.readValue(reportDefinition.get("md_object_filter").toString(), List.class);
        }
        catch (Exception e) {
            result_data.put("message","Sorry! Something went wrong while trying to read the object filters. Check the console for more information.");
            result_data.put("exception",e.getMessage());
            results.put("result_data", result_data);
            results.put("result_status", -11);
            System.out.println(e.toString());
            return results;
        }

        //get the column set
        List<String> columnSet;
        try {
            columnSet = objectMapper.readValue(reportDefinition.get("column_set_def").toString(), List.class);
        }
        catch (Exception e) {
            result_data.put("message","Sorry! Something went wrong while trying to read the column set. Check the console for more information.");
            result_data.put("exception",e.getMessage());
            results.put("result_data", result_data);
            results.put("result_status", -11);
            System.out.println(e.toString());
            return results;
        }

        //Build the data that will be displayed in the data table
        ArrayList<Object> reportData = new ArrayList<>();
        try{
            //Loop through all defined data landscape objects
            for(Map<String, Object> object : objects){
                boolean include = true;
                //For each object check if complies with all the filters.
                //If it doesn't don't include it.
                for(Map<String,Object> filter : filters){
                    ArrayList values = (ArrayList) filter.get("values");
                    if(!values.contains(object.get(filter.get("field")))){
                        include = false;
                    }
                }
                //If the object complies with all the filters
                //get the values for each column in the report
                if(include){
                    Map<String, String> newRow = new LinkedHashMap<>();
                    for(String column : columnSet){
                        String data = "None Specified";
                        if(object.get(column) != null){
                            data = object.get(column).toString();
                        }
                        //if the column isn't in the object check the custom form data
                        else if(object.get("custom_form_data") != null){
                            Map<String, Object> customFormData;
                            customFormData = objectMapper.readValue(object.get("custom_form_data").toString(), Map.class);
                            if(customFormData.get(column) != null){
                                data = customFormData.get(column).toString();
                            }
                        }
                        newRow.put(column,data);
                    }
                    reportData.add(newRow);
                }
            }
        }
        catch(Exception e){
            result_data.put("message","Sorry! Something went wrong while trying to build the report data. Check the console for more information.");
            result_data.put("exception",e.getMessage());
            results.put("result_data", result_data);
            results.put("result_status", -11);
            System.out.println(e.toString());
            return results;
        }

        result_data.put("reportData", reportData);
        String reportName = "None Provided";
        String reportShortDesc = "None Provided";
        try{
            reportName = reportDefinition.get("report_name").toString();
            reportShortDesc = reportDefinition.get("report_shortdesc").toString();
        }catch (Exception e){
            System.out.println(e.toString());
        }
        result_data.put("reportName",reportName);
        result_data.put("reportShortDesc",reportShortDesc);
        result_data.put("selectedFilters",filters);
        result_data.put("selectedColumns",columnSet);

        results.put("result_data", result_data);
        results.put("result_status", resultStatus);

        return results;
    }

    /*
     ** URL Path: /dsdf/report/columns
     */
    @RequestMapping(method = POST, value = "/dsdf/report/columns")
    public Map<String, Object> readReportColumns(@RequestBody ArrayList<Integer> selectedObjects) {
        Map<String, Object> results = new HashMap<>();
        Set<String> result_data = new LinkedHashSet<>();
        Integer resultStatus = 1;   // this indicates status of database results
        ObjectMapper objectMapper = new ObjectMapper(); //this is for reading the form definition

        result_data.add("object_name");
        result_data.add("object_shortdesc");
        result_data.add("business_contact");
        result_data.add("technical_contact");

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_definition_by_class");
        for( Integer objectClassId : selectedObjects){
            List<String> columns;
            try{
                SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", objectClassId, Types.INTEGER);
                Map<String, Object> out = jdbcCall.execute(in);
                ArrayList<Object> rslt = (ArrayList<Object>)out.get("#result-set-1");
                try{
                    LinkedCaseInsensitiveMap<Object> rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);
                    columns = objectMapper.readValue(rsltMap.get("form_definition").toString(), List.class);
                }
                catch(IndexOutOfBoundsException i){
                    break;
                }

            }
            catch (Exception e){
                System.out.println(e.toString());
                results.put("result_data", "Something went wrong while reading the custom form definitions");
                results.put("result_status", -12);

                return results;
            }
            result_data.addAll(columns);
        }

        results.put("result_data", result_data);
        results.put("result_status", resultStatus);

        return results;
    }

    /*
     ** URL Path: /dsdf/report/list
     */
    @RequestMapping(method = GET, value = "/dsdf/report/list")
    public Map<String, Object> readReportList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_reports");

        Map<String, Object> out = jdbcCall.execute();
        ArrayList<Object> rslt = (ArrayList<Object>) out.get("#result-set-1");

        results.put("result_data", rslt);
        results.put("result_status", resultStatus);

        return results;
    }

    /*
     ** URL Path: /dsdf/report
     */
    @RequestMapping(method = POST, value = "/dsdf/report")
    public Map<String, Object> writeReport(@RequestBody DlReport reportDef, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> result_data = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_report_definition");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_report_id", reportDef.getReportId(), Types.INTEGER)
                .addValue("_report_name", reportDef.getReportName(), Types.VARCHAR)
                .addValue("_report_shortdesc", reportDef.getReportShortDesc(), Types.VARCHAR)
                .addValue("_md_object_filter", reportDef.getObjectFilter(), Types.VARCHAR)
                .addValue("_column_set_def", reportDef.getColumnSet(), Types.VARCHAR)
                .addValue("_last_change_user_id", userId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        result_data.put("returnId", out.get("_return_id"));

        results.put("result_data", result_data);
        results.put("result_status", resultStatus);

        return results;
    }

    /*
     ** URL Path: /dsdf/report
     */
    @RequestMapping(method = DELETE, value = "/dsdf/report/{reportId}")
    public Map<String, Object> deleteReport(@PathVariable Long reportId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> result_data = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_report");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_report_id", reportId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", result_data);
        results.put("result_status", resultStatus);

        return results;
    }

    /*
     ** URL Path: /dsdf/report/update
     */
    @RequestMapping(method = POST, value = "/dsdf/report/update")
    public Map<String, Object> updateReport(@RequestBody DlReport reportDef, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> result_data = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall;
        SqlParameterSource in;
        Map<String, Object> out;

        if (reportDef.getReportName() != null) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_report_core");
            in = new MapSqlParameterSource()
                    .addValue("_report_id", reportDef.getReportId(), Types.INTEGER)
                    .addValue("_report_name", reportDef.getReportName(), Types.VARCHAR)
                    .addValue("_report_shortdesc", reportDef.getReportShortDesc(), Types.VARCHAR)
                    .addValue("_last_change_user_id", userId, Types.INTEGER);

            jdbcCall.execute(in);
        }

        if (reportDef.getColumnSet() != null) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_report_columns");
            in = new MapSqlParameterSource()
                    .addValue("_report_id", reportDef.getReportId(), Types.INTEGER)
                    .addValue("_column_set_def", reportDef.getColumnSet(), Types.VARCHAR)
                    .addValue("_last_change_user_id", userId, Types.INTEGER);


            jdbcCall.execute(in);
        }

        if (reportDef.getObjectFilter() != null) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_report_filters");
            in = new MapSqlParameterSource()
                    .addValue("_report_id", reportDef.getReportId(), Types.INTEGER)
                    .addValue("_md_object_filter", reportDef.getObjectFilter(), Types.VARCHAR)
                    .addValue("_last_change_user_id", userId, Types.INTEGER);

            jdbcCall.execute(in);
        }

        results.put("result_data", result_data);
        results.put("result_status", resultStatus);

        return results;
    }

}