/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.dsdf;

import io.datallama.api.association.AssociatedObject;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class DataFlowController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * URL Path: /dsdf/dataflow/list
     * TODO: move to ReferenceDataController
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/dataflow/list")
    @ResponseBody
    public Map<String, Object> readDataflowList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList<DlDataflow> dataflowList = new ArrayList<DlDataflow>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_all_dataflows_summary");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        for(Object listElement:rslt) {
            LinkedCaseInsensitiveMap dataflowInfo = (LinkedCaseInsensitiveMap) listElement;
            DlDataflow dataflow = new DlDataflow(
                    (Integer) dataflowInfo.get("dataflow_id"),
                    (Integer) dataflowInfo.get("class_id"),
                    dataflowInfo.get("dataflow_name").toString(),
                    dataflowInfo.get("dataflow_desc").toString(),
                    (Integer) dataflowInfo.get("dataflow_start_ntt_id"),
                    (Integer) dataflowInfo.get("start_ntt_class_id"),
                    dataflowInfo.get("start_ntt_name").toString(),
                    (Integer) dataflowInfo.get("dataflow_end_ntt_id"),
                    (Integer) dataflowInfo.get("end_ntt_class_id"),
                    dataflowInfo.get("end_ntt_name").toString(),
                    (Integer) dataflowInfo.get("business_contact_id"),
                    dataflowInfo.get("business_contact").toString(),
                    (Integer) dataflowInfo.get("technical_contact_id"),
                    dataflowInfo.get("technical_contact").toString()
            );

            dataflowList.add(dataflow);
        }

        resultData.put("dataflow_list", dataflowList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /dsdf/dataflow/detail/{dataflowId}
     * Reads the details of a data flow from the database, including core details and the list of DSDF diagrams in which
     * the dataflow in included.
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/dataflow/detail/{dataflowId}")
    @ResponseBody
    public Map<String, Object> readDataflowDetail(@PathVariable Long dataflowId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // first, get the core details for the data flow
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dataflow_core_details");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_dataflow_id", dataflowId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        DlDataflow dataflow = null;
        try{
            LinkedCaseInsensitiveMap dataflowInfo = (LinkedCaseInsensitiveMap) rslt.get(0);

            dataflow = new DlDataflow(
                    (Integer) dataflowInfo.get("dataflow_id"),
                    (Integer) dataflowInfo.get("class_id"),
                    dataflowInfo.get("dataflow_name").toString(),
                    dataflowInfo.get("dataflow_desc").toString(),
                    (Integer) dataflowInfo.get("dataflow_start_ntt_id"),
                    (Integer) dataflowInfo.get("start_ntt_class_id"),
                    dataflowInfo.get("start_ntt_name").toString(),
                    (Integer) dataflowInfo.get("dataflow_end_ntt_id"),
                    (Integer) dataflowInfo.get("end_ntt_class_id"),
                    dataflowInfo.get("end_ntt_name").toString(),
                    (Integer) dataflowInfo.get("business_contact_id"),
                    dataflowInfo.get("business_contact").toString(),
                    (Integer) dataflowInfo.get("technical_contact_id"),
                    dataflowInfo.get("technical_contact").toString()
            );
            dataflow.setDataflowFullDesc(dataflowInfo.get("full_desc").toString());
        }
        catch (Exception e) {
            System.out.println("Exception thrown while processing database results.");
            System.out.println(e);
        }


        resultData.put("core_details", dataflow);

        // second, get the list of diagrams in which this data store is included.
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dataflow_diagrams");
        out = jdbcCall.execute(in);
        ArrayList diagramList = (ArrayList) out.get("#result-set-1");
        resultData.put("diagrams", diagramList);

        // third, get the list of SMD databases associated with the datastore
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        DlDataflow coreDetails = (DlDataflow)resultData.get("core_details");
        int classId = coreDetails.getClassId();
        in = new MapSqlParameterSource()
                .addValue("_object_id", dataflowId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList<AssociatedObject> associations = new ArrayList<>();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");
        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // first, create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));
            associations.add(assocObject);
        }
        resultData.put("associations", associations);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /dsdf/dataflow
     * @param dataflowDetail
     * @return
     */
    @RequestMapping(method = POST, value = "/dsdf/dataflow")
    @ResponseBody
    public Map<String, Object> writeDataflow(@RequestBody DataflowDetail dataflowDetail, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataflow");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_dataflow_id", dataflowDetail.getDataflowId(), Types.INTEGER)
                .addValue("_dataflow_name", dataflowDetail.getDataflowName(), Types.VARCHAR)
                .addValue("_dataflow_start_ntt_id", dataflowDetail.getStartNttId(), Types.INTEGER)
                .addValue("_dataflow_end_ntt_id", dataflowDetail.getEndNttId(), Types.INTEGER)
                .addValue("_business_contact", dataflowDetail.getBusinessContact(), Types.VARCHAR)
                .addValue("_technical_contact", dataflowDetail.getTechnicalContact(), Types.VARCHAR)
                .addValue("_dataflow_shortdesc", dataflowDetail.getDataflowShortDesc(), Types.VARCHAR)
                .addValue("_user_id", userId, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap newID = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("returnId", newID.get("returnId"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * URL Path: /dsdf/dataflow/fulldesc
     * Writes the full description text of a data flow to the database.
     */
    @RequestMapping(method = POST, value = "/dsdf/dataflow/fulldesc")
    @ResponseBody
    public Map<String, Object> saveDataflowFullDesc(@RequestBody DataflowFullDescription dataflowFullDescription, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        System.out.println(dataflowFullDescription.getDataflowFullDesc());

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_full_description");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", dataflowFullDescription.getConnectorId(), Types.INTEGER)
                .addValue("_class_id", 2, Types.INTEGER)
                .addValue("_full_desc", dataflowFullDescription.getDataflowFullDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * DataflowDetail models the core details of a data flow metadata object
     */

    static class DataflowDetail {
        private Integer dataflowId;
        private String dataflowName;
        private Integer startNttId;
        private Integer endNttId;
        private String businessContact;
        private String technicalContact;
        private String dataflowShortDesc;

        public DataflowDetail() {
            this.dataflowId = 0;
            this.dataflowName = "";
            this.dataflowShortDesc = "";
            this.businessContact = "";
            this.technicalContact = "";
        }

        public DataflowDetail(Integer dataflowId, Integer classId, String dataflowName, String dataflowShortDesc, String businessContact, String technicalContact) {
            this.dataflowId = dataflowId;
            this.dataflowName = dataflowName;
            this.dataflowShortDesc = dataflowShortDesc;
            this.businessContact = businessContact;
            this.technicalContact = technicalContact;
        }

        public Integer getStartNttId() {
            return startNttId;
        }

        public void setStartNttId(Integer startNttId) {
            this.startNttId = startNttId;
        }

        public Integer getEndNttId() {
            return endNttId;
        }

        public void setEndNttId(Integer endNttId) {
            this.endNttId = endNttId;
        }

        public Integer getDataflowId() {
            return dataflowId;
        }

        public void setDataflowId(Integer dataflowId) {
            this.dataflowId = dataflowId;
        }

        public String getDataflowName() {
            return dataflowName;
        }

        public void setDataflowName(String dataflowName) {
            this.dataflowName = dataflowName;
        }

        public String getDataflowShortDesc() {
            return dataflowShortDesc;
        }

        public void setDataflowShortDesc(String dataflowShortDesc) {
            this.dataflowShortDesc = dataflowShortDesc;
        }

        public String getBusinessContact() {
            return businessContact;
        }

        public void setBusinessContact(String businessContact) {
            this.businessContact = businessContact;
        }

        public String getTechnicalContact() {
            return technicalContact;
        }

        public void setTechnicalContact(String technicalContact) {
            this.technicalContact = technicalContact;
        }
    }

    /**
     * DataflowFullDescription is used to save the Full Description text for a data flow.
     * The full description can be edited independently of the core details so it is necessary to use this utility
     * class to receive data for the POST call that saves the description.
     */
    static class DataflowFullDescription {
        private Integer connectorId;
        private String dataflowFullDesc;

        public Integer getConnectorId() {
            return connectorId;
        }

        public void setConnectorId(Integer connectorId) {
            this.connectorId = connectorId;
        }

        public String getDataflowFullDesc() {
            return dataflowFullDesc;
        }

        public void setDataflowFullDesc(String dataflowFullDesc) {
            this.dataflowFullDesc = dataflowFullDesc;
        }
    }
}