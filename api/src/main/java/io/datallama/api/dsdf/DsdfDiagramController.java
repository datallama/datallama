/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.dsdf;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.datamodels.ConnectorContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class DsdfDiagramController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Reads from the DL DB the list of Data Landscape diagrams
     * URL Path: /dsdf/diagram/list
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/diagram/list")
    public Map<String, Object> readDiagramList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        int dsdfDiagramClassId = 3;
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dm_diagrams");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", dsdfDiagramClassId);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");

        results.put("result_data", rslt);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads from the DL DB the icon SVG string for the Landscape Object identified by {objectId}
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/dsdf/icon/{objectId}")
    public Map<String, Object> readObjectIconString(@PathVariable Long objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_icon");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("iconSvgString",rsltMap.get("iconSvgString"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/dsdf/diagram/content/{diagramId}")
    @ResponseBody
    public Map<String, Object> readDsdfDiagram(@PathVariable Long diagramId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        ArrayList<Object> entities = new ArrayList<>();
        ArrayList<Object> connectors = new ArrayList<>();
        resultData.put("id",diagramId);
        Integer resultStatus = 1;   // this indicates status of database results

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dm_diagram_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("name", rsltMap.get("dgrm_name"));
        resultData.put("properties", rsltMap.get("dgrm_prop"));

        // first: get data store list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_entities");
        in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dataStoreList = (ArrayList) out.get("#result-set-1");
        entities.addAll(dataStoreList);

        // second: get data flow list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_connectors");
        out = jdbcCall.execute(in);
        ArrayList dataFlowList = (ArrayList) out.get("#result-set-1");
        connectors.addAll(dataFlowList);

        // third: get shape list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_diagram_shapes");
        out = jdbcCall.execute(in);
        ArrayList shapeList = (ArrayList) out.get("#result-set-1");
        entities.addAll(shapeList);

        // fourth: get arrow list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_diagram_arrows");
        out = jdbcCall.execute(in);
        ArrayList arrowList = (ArrayList) out.get("#result-set-1");
        connectors.addAll(arrowList);

        resultData.put("entities", entities);
        resultData.put("connectors", connectors);
        // fifth: get data processor list
        //Todo: make a stored proc to do all this in a single super efficient call
        ArrayList<Map<String, Object>> dataProcessorList = new ArrayList<>();
        //for each data flow in the diagram
        for (Object con : dataFlowList) {
            Map<?,?> connector = (Map<?,?>) con;
            //read the associations to see if it is associated with a data processor
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
            in = new MapSqlParameterSource()
                    .addValue("_class_id", connector.get("classId"), Types.INTEGER)
                    .addValue("_object_id", connector.get("cnctId"), Types.INTEGER);
            out = jdbcCall.execute(in);
            ArrayList<?> res = (ArrayList<?>) out.get("#result-set-1");
            //for each object associated with the data flow
            for(Object assoc : res){
                Map<?,?> association = (Map<?, ?>) assoc;
                String associationType = (String)association.get("assoc_type_name");
                //if the type of the association is processor then get the colour and add it to the dataProcessorList
                if(associationType.equals("Processor")){
                    boolean newProcessor = true;
                    //check the data processor list to see if it is already on the list
                    for(Map<String, Object> dataProcessor :dataProcessorList){
                        //if it is on the list update the processor to include the current data flow in its list of connectors
                        if(dataProcessor.get("id").equals(association.get("target_object_id"))){
                            ArrayList<Integer> connectorList = (ArrayList<Integer>) dataProcessor.get("connectorList");
                            connectorList.add((Integer)connector.get("cnctId"));
                            dataProcessor.remove("connectorList");
                            dataProcessor.put("connectorList",connectorList);
                            newProcessor = false;
                            break;
                        }
                    }
                    //if it isn't on the list
                    if(newProcessor){
                        Map<String, Object> processor = new HashMap<>();

                        //check the color assigned to the data processor
                        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_color");
                        in = new MapSqlParameterSource().addValue("_object_id", association.get("target_object_id"), Types.INTEGER);
                        out = jdbcCall.execute(in);
                        rslt = (ArrayList) out.get("#result-set-1");
                        rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);
                        String colorDef = (String)rsltMap.get("colorDef");

                        ArrayList<Integer> connectorList = new ArrayList<>();
                        connectorList.add((Integer)connector.get("cnctId"));

                        //make a new processor object and add it to the data processor list
                        processor.put("colorDef", colorDef);
                        processor.put("id",association.get("target_object_id"));
                        processor.put("name", association.get("target_object_name"));
                        processor.put("connectorList",connectorList);
                        processor.put("show", false);
                        dataProcessorList.add(processor);
                    }
                }
            }
        }

        Map<String, Object> customData = new HashMap<>();
        customData.put("processors", dataProcessorList);
        resultData.put("customData", customData);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/dsdf/diagram/entities/{diagramId}")
    @ResponseBody
    public Map<String, Object> readAllDsdfEntities(@PathVariable Long diagramId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        // get the list of all data landscape objects
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_landscape");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_dgrm_id", diagramId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList landscapeObjects = (ArrayList) out.get("#result-set-1");
        resultData.put("allEntities", landscapeObjects);

        // get the list of all data connectors
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dsdf_candidate_connectors");
        out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        ArrayList<ConnectorContent> allConnectors = new ArrayList<>();
        for(Object listElement:rslt) {
            LinkedCaseInsensitiveMap connectorInfo = (LinkedCaseInsensitiveMap) listElement;
            Boolean inclusionSatus = (Integer)connectorInfo.get("included") == 1;
            String cnctStartNtt = String.format("%s:%s",connectorInfo.get("cnct_start_class_id"),connectorInfo.get("cnct_start_ntt"));
            String cnctEndNtt = String.format("%s:%s",connectorInfo.get("cnct_end_class_id"),connectorInfo.get("cnct_end_ntt"));

            ConnectorContent connector = new ConnectorContent(
                    (Integer)connectorInfo.get("object_id"),
                    (Integer)connectorInfo.get("class_id"),
                    connectorInfo.get("object_name").toString(),
                    connectorInfo.get("object_shortdesc").toString(),
                    cnctStartNtt,
                    cnctEndNtt,
                    "{}",
                    inclusionSatus
            );
            allConnectors.add(connector);
        }
        resultData.put("allConnectors", allConnectors);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/dsdf/processor/color/{objectId}")
    @ResponseBody
    public Map<String, Object> saveProcessorColor(@RequestBody String color, @RequestHeader("dlUserId") String userId, @PathVariable Long objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dsdf_color");;
        SqlParameterSource in =  new MapSqlParameterSource()
                .addValue("_user_id",Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_color_def", color, Types.VARCHAR);
        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;

    }

    /**
     * URL Path: POST /dsdf/diagram/content
     *
     * @param dsdfDiagramContent
     * @return
     */
    @RequestMapping(method = POST, value = "/dsdf/diagram/content")
    @ResponseBody
    public Map<String, Object> saveDiagramContent(@RequestBody DsdfDiagramContent dsdfDiagramContent, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        ObjectMapper objectMapper = new ObjectMapper(); // used to parse Maps to JSON strings

        //zeroth, save the diagram properties
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("update_diagram_properties");
        SqlParameterSource in =  new MapSqlParameterSource()
                .addValue("_user_id",Integer.parseInt(userId), Types.INTEGER)
                .addValue("_dgrm_id", dsdfDiagramContent.getDiagramId(), Types.INTEGER)
                .addValue("_dgrm_prop", dsdfDiagramContent.getDiagramPropertiesAsString(), Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        // first: save all diagram entities
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_entity");

        for (Object listElement : dsdfDiagramContent.getEntities()) {
            LinkedHashMap entityInfo = (LinkedHashMap) listElement;

            Object cd = entityInfo.get("colorDefn");
            String colorDefinitionJSON = "";

            try {
                colorDefinitionJSON = objectMapper.writeValueAsString(cd);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            in = new MapSqlParameterSource()
                    .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                    .addValue("_class_id", entityInfo.get("classId"), Types.INTEGER)
                    .addValue("_dgrm_id", dsdfDiagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_ntt_id", entityInfo.get("nttId"), Types.INTEGER)
                    .addValue("_ntt_x", entityInfo.get("x"), Types.INTEGER)
                    .addValue("_ntt_y", entityInfo.get("y"), Types.INTEGER)
                    .addValue("_ntt_z", entityInfo.get("z"), Types.INTEGER)
                    .addValue("_ntt_height", entityInfo.get("height"), Types.INTEGER)
                    .addValue("_ntt_width", entityInfo.get("width"), Types.INTEGER)
                    .addValue("_color_defn", colorDefinitionJSON, Types.VARCHAR)
                    .addValue("_color_source",entityInfo.get("colorSource"), Types.INTEGER);

            jdbcCall.execute(in);
        }

        // second: save all diagram connectors
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_diagram_connector");

        for (Object listElement : dsdfDiagramContent.getConnectors()) {
            LinkedHashMap connectorInfo = (LinkedHashMap) listElement;

            String verticesJSON = "";
            try {
                verticesJSON = objectMapper.writeValueAsString(connectorInfo.get("cnctVertices"));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            in = new MapSqlParameterSource()
                    .addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_class_id", connectorInfo.get("classId"), Types.INTEGER)
                    .addValue("_dgrm_id", dsdfDiagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_cnct_id", connectorInfo.get("cnctId"), Types.INTEGER)
                    .addValue("_cnct_start_port", connectorInfo.get("cnctStartPort"), Types.VARCHAR)
                    .addValue("_cnct_end_port", connectorInfo.get("cnctEndPort"), Types.VARCHAR)
                    .addValue("_cnct_vertices", verticesJSON, Types.VARCHAR)
                    .addValue("_cnct_z", connectorInfo.get("z"), Types.INTEGER);

            jdbcCall.execute(in);
        }

        //Third delete entities
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_diagram_entity");
        for (Object delEntityId : dsdfDiagramContent.getEntityDeletes()) {
            in = new MapSqlParameterSource()
                    .addValue("_dgrm_id", dsdfDiagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_ntt_id", delEntityId, Types.INTEGER);
            jdbcCall.execute(in);
        }

        //Fourth delete connectors
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_diagram_connector");
        for (Object delConnectorId : dsdfDiagramContent.getConnectorDeletes()) {
            in = new MapSqlParameterSource()
                    .addValue("_dgrm_id", dsdfDiagramContent.getDiagramId(), Types.INTEGER)
                    .addValue("_cnct_id", delConnectorId, Types.INTEGER);
            jdbcCall.execute(in);
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;

    }


    /**
     * DsdfDiagramContent models the data store and data flow content of a diagram and persist it to the database
     */
    static class DsdfDiagramContent {
        private Integer diagramId;
        private Object properties;
        private ArrayList<Object> entities;
        private ArrayList<Object> connectors;
        private ArrayList<Object> entityDeletes;
        private ArrayList<Object> connectorDeletes;

        public ArrayList<Object> getEntityDeletes() {
            return entityDeletes;
        }

        public void setEntityDeletes(ArrayList<Object> entityDeletes) {
            this.entityDeletes = entityDeletes;
        }

        public ArrayList<Object> getConnectorDeletes() {
            return connectorDeletes;
        }

        public void setConnectorDeletes(ArrayList<Object> connectorDeletes) {
            this.connectorDeletes = connectorDeletes;
        }

        public Integer getDiagramId() {
            return diagramId;
        }

        public void setDiagramId(Integer diagramId) {
            this.diagramId = diagramId;
        }

        public ArrayList<Object> getEntities() {
            return entities;
        }

        public void setEntities(ArrayList<Object> entities) {
            this.entities = entities;
        }

        public ArrayList<Object> getConnectors() {
            return connectors;
        }

        public void setConnectors(ArrayList<Object> connectors) {
            this.connectors = connectors;
        }

        public Object getProperties() {
            return properties;
        }

        public void setProperties(Object properties) {
            this.properties = properties;
        }

        public String getDiagramPropertiesAsString() {
            ObjectMapper objectMapper = new ObjectMapper(); // used to diagram properties Object as JSON string
            String diagramProps = new String();
            try {
                diagramProps = objectMapper.writeValueAsString(this.properties);
            }
            catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return diagramProps;
        }
    }
}