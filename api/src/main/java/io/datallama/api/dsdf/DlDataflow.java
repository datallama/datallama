/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.dsdf;

import io.datallama.api.common.DlConnector;

public class DlDataflow extends DlConnector {
    private Integer businessContactId;
    private String businessContactName;
    private Integer technicalContactId;
    private String technicalContactName;
    private String dataflowFullDesc;

    public DlDataflow(Integer connectorId, Integer classId, String connectorName, String connectorShortDesc,
                      Integer startEntityId, Integer startEntityClassId, String startEntityName,
                      Integer endEntityId, Integer endEntityClassId, String endEntityName,
                      Integer businessContactId, String businessContactName,
                      Integer technicalContactId, String technicalContactName) {
        super(connectorId, classId, connectorName, connectorShortDesc,
                startEntityId, startEntityClassId, startEntityName, endEntityId, endEntityClassId, endEntityName);
        this.businessContactId = businessContactId;
        this.businessContactName = businessContactName;
        this.technicalContactId = technicalContactId;
        this.technicalContactName = technicalContactName;
        this.dataflowFullDesc = "None Specified";
    }

    public Integer getBusinessContactId() {
        return businessContactId;
    }

    public void setBusinessContactId(Integer businessContactId) {
        this.businessContactId = businessContactId;
    }

    public String getBusinessContactName() {
        return businessContactName;
    }

    public void setBusinessContactName(String businessContactName) {
        this.businessContactName = businessContactName;
    }

    public Integer getTechnicalContactId() {
        return technicalContactId;
    }

    public void setTechnicalContactId(Integer technicalContactId) {
        this.technicalContactId = technicalContactId;
    }

    public String getTechnicalContactName() {
        return technicalContactName;
    }

    public void setTechnicalContactName(String technicalContactName) {
        this.technicalContactName = technicalContactName;
    }

    public String getDataflowFullDesc() {
        return dataflowFullDesc;
    }

    public void setDataflowFullDesc(String dataflowFullDesc) {
        this.dataflowFullDesc = dataflowFullDesc;
    }
}
