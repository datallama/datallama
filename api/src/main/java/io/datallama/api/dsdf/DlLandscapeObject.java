package io.datallama.api.dsdf;

import io.datallama.api.common.DlCustomForm;

public class DlLandscapeObject {
    private Integer classId;
    private Integer objectId;
    private Integer parentClassId;
    private Integer parentObjectId;
    private String objectName;
    private String className;
    private Integer persistentTechnologyId;
    private Integer hostedRealmId;
    private Integer customFormId;
    private String businessContact;
    private String technicalContact;
    private String objectShortDesc;
    private String iconSvgString;
    private DlCustomForm customForm;

    /**
     * Default constructor.
     */
    public DlLandscapeObject() {
        this.classId = 0;
        this.objectId = 0;
        this.parentClassId = 0;
        this.parentObjectId = 0;
        this.objectName = "";
        this.className = "";
        this.persistentTechnologyId = 0;
        this.hostedRealmId = 0;
        this.customFormId = 0;
        this.businessContact = "";
        this.technicalContact = "";
        this.objectShortDesc = "";
        this.iconSvgString = "";
        this.customForm = new DlCustomForm();
    }

    /**
     * Minimum mandatory fields constructor.
     * @param classId
     * @param objectId
     * @param parentClassId
     * @param parentObjectId
     * @param objectName
     */
    public DlLandscapeObject(Integer classId, Integer objectId, Integer parentClassId, Integer parentObjectId, String objectName) {
        this.classId = classId;
        this.objectId = objectId;
        this.parentClassId = parentClassId;
        this.parentObjectId = parentObjectId;
        this.objectName = objectName;
        this.className = "";
        this.persistentTechnologyId = 0;
        this.hostedRealmId = 0;
        this.customFormId = 0;
        this.businessContact = "";
        this.technicalContact = "";
        this.objectShortDesc = "";
        this.iconSvgString = "";
        this.customForm = new DlCustomForm();
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getPersistentTechnologyId() {
        return persistentTechnologyId;
    }

    public void setPersistentTechnologyId(Integer persistentTechnologyId) {
        this.persistentTechnologyId = persistentTechnologyId;
    }

    public Integer getHostedRealmId() {
        return hostedRealmId;
    }

    public void setHostedRealmId(Integer hostedRealmId) {
        this.hostedRealmId = hostedRealmId;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public String getBusinessContact() {
        return businessContact;
    }

    public void setBusinessContact(String businessContact) {
        this.businessContact = businessContact;
    }

    public String getTechnicalContact() {
        return technicalContact;
    }

    public void setTechnicalContact(String technicalContact) {
        this.technicalContact = technicalContact;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public String getIconSvgString() {
        return iconSvgString;
    }

    public void setIconSvgString(String iconSvgString) {
        this.iconSvgString = iconSvgString;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }
}
