/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

public class DlGroup {
    private Integer groupId;
    private String groupName;
    private String groupShortDesc;
    private Boolean assigned;
    private ArrayList<DlGroupPermission> groupPermissions;

    public DlGroup() {
        this.groupId = 0;
        this.groupName = "";
        this.groupShortDesc = "";
        this.assigned = false;
        this.groupPermissions = new ArrayList<>();
    }

    public DlGroup(Integer groupId, String groupName, String groupShortDesc) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupShortDesc = groupShortDesc;
        this.assigned = false;
        this.groupPermissions = new ArrayList<>();
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupShortDesc() {
        return groupShortDesc;
    }

    public void setGroupShortDesc(String groupShortDesc) {
        this.groupShortDesc = groupShortDesc;
    }

    public Boolean getAssigned() {
        return assigned;
    }

    public void setAssigned(Boolean assigned) {
        this.assigned = assigned;
    }

    public ArrayList<DlGroupPermission> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(ArrayList<DlGroupPermission> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    public void addGroupPermission(DlGroupPermission groupPermission) {
        this.groupPermissions.add(groupPermission);
    }

    public String getGroupPermissionsAsString() {
        ObjectMapper objectMapper = new ObjectMapper(); // used to diagram properties Object as JSON string
        String groupPermissions = new String();
        try {
            groupPermissions = objectMapper.writeValueAsString(this.groupPermissions);
        }
        catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return groupPermissions;
    }

}
