/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.sql.Types;
import java.util.Map;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.*;
import org.jose4j.keys.HmacKey;
import org.jose4j.keys.resolvers.HttpsJwksVerificationKeyResolver;
import org.jose4j.jwk.HttpsJwks;

import org.jose4j.lang.JoseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class DlJwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(DlJwtUtils.class);

    @Autowired
    private DriverManagerDataSource dataSource;

    //Internal Data Llama JWT properties
    @Value("${dl.auth.dlint.secret}")
    private String jwtSecret;
    @Value("${dl.auth.dlint.expire-min}")
    private int jwtExpirationM;

    //Microsoft Idenity Platform properties
    @Value("${dl.auth.msoidc.tenant-id}")
    private String msoidcTenantId;
    @Value("${dl.auth.msoidc.client-id}")
    private String msoidcClientId;

    public String generateDlJwt(Authentication authentication) throws JoseException {

        DlUserDetails userPrincipal = (DlUserDetails) authentication.getPrincipal();

        JwtClaims claims = new JwtClaims();
        claims.setExpirationTimeMinutesInTheFuture(jwtExpirationM);
        claims.setSubject(userPrincipal.getUsername());
        claims.setIssuer("dlInt");
        claims.setAudience("dlApi");

        Key key = new HmacKey(jwtSecret.getBytes(StandardCharsets.UTF_8));

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setKey(key);

        return jws.getCompactSerialization();
    }

    public String getUserNameFromJwt(String jwt) {
        JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder()
                .setSkipAllValidators()
                .setDisableRequireSignature()
                .setSkipSignatureVerification()
                .build();

        try{
            JwtContext jwtContext = firstPassJwtConsumer.process(jwt);
            String issuer = jwtContext.getJwtClaims().getIssuer();
            String username = null;
            if(issuer.equals("dlInt")){
                username = jwtContext.getJwtClaims().getSubject();

            }
            else if(issuer.equals("https://login.microsoftonline.com/"+msoidcTenantId+"/v2.0")){
                String email = jwtContext.getJwtClaims().getClaimValueAsString("email");
                if(email != null){
                    SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_name_by_email");
                    MapSqlParameterSource in = new MapSqlParameterSource().addValue("_email_address", email, Types.VARCHAR);
                    Map<String, Object> out = jdbcCall.execute(in);

                    username = out.get("_username").toString();
                }
            }
            return username;
        }
        catch (Exception e){
            return null;
        }
    }

    public boolean validateJwt(String jwt) {

        JwtConsumer firstPassJwtConsumer = new JwtConsumerBuilder()
                .setSkipAllValidators()
                .setDisableRequireSignature()
                .setSkipSignatureVerification()
                .build();

        String issuer;
        try{
            JwtContext jwtContext = firstPassJwtConsumer.process(jwt);
             issuer = jwtContext.getJwtClaims().getIssuer();
        }
        catch (Exception e){
            return false;
        }

        JwtConsumer jwtConsumer;

        if(issuer.equals("dlInt")){
            Key key = new HmacKey(jwtSecret.getBytes(StandardCharsets.UTF_8));

            jwtConsumer = new JwtConsumerBuilder()
                    .setRequireExpirationTime()
                    .setRequireSubject()
                    .setExpectedIssuer("dlInt")
                    .setExpectedAudience("dlApi")
                    .setVerificationKey(key)
                    .setJwsAlgorithmConstraints(AlgorithmConstraints.ConstraintType.PERMIT, AlgorithmIdentifiers.HMAC_SHA256)
                    .build();

        }
        else if(issuer.equals("https://login.microsoftonline.com/" + msoidcTenantId + "/v2.0")){
            HttpsJwks httpsJwks = new HttpsJwks("https://login.microsoftonline.com/" + msoidcTenantId + "/discovery/v2.0/keys");
            HttpsJwksVerificationKeyResolver httpsJwksKeyResolver = new HttpsJwksVerificationKeyResolver(httpsJwks);

            jwtConsumer = new JwtConsumerBuilder()
                    .setRequireExpirationTime()
                    .setRequireSubject()
                    .setExpectedIssuer("https://login.microsoftonline.com/" + msoidcTenantId + "/v2.0")
                    .setExpectedAudience(msoidcClientId)
                    .setVerificationKeyResolver(httpsJwksKeyResolver)
                    .setJwsAlgorithmConstraints(AlgorithmConstraints.ConstraintType.PERMIT, AlgorithmIdentifiers.RSA_USING_SHA256)
                    .build();
        }
        else{
            return false;
        }

        try
        {
            //if it can process it without throwing an error it's all good
            jwtConsumer.processToClaims(jwt);
            return true;
        }
        catch (InvalidJwtException e)
        {
            logger.error("Something went wrong: {}", e.getMessage());
        }

        return false;
    }
}


