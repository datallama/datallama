/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

public class DlGroupPermission {
    private Integer moduleId;
    private String moduleCode;
    private String moduleName;
    private Integer actionCategoryId;
    private String actionCategoryName;
    private Boolean accessGranted;

    public DlGroupPermission() {
        this.moduleId = 0;
        this.moduleCode = "";
        this.moduleName = "";
        this.actionCategoryId = 0;
        this.actionCategoryName = "";
        this.accessGranted = false;
    }

    public DlGroupPermission(Integer moduleId, String moduleCode, Integer actionCategoryId, String actionCategoryName,
                             Boolean accessGranted) {
        this.moduleId = moduleId;
        this.moduleCode = moduleCode;
        this.moduleName = "";
        this.actionCategoryId = actionCategoryId;
        this.actionCategoryName = actionCategoryName;
        this.accessGranted = accessGranted;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public Integer getActionCategoryId() {
        return actionCategoryId;
    }

    public void setActionCategoryId(Integer actionCategoryId) {
        this.actionCategoryId = actionCategoryId;
    }

    public String getActionCategoryName() {
        return actionCategoryName;
    }

    public void setActionCategoryName(String actionCategoryName) {
        this.actionCategoryName = actionCategoryName;
    }

    public Boolean getAccessGranted() {
        return accessGranted;
    }

    public void setAccessGranted(Boolean accessGranted) {
        this.accessGranted = accessGranted;
    }
}
