/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import java.util.LinkedHashSet;

public class DlPathAuthorityCouple {
    private final AntPathRequestMatcher path;
    private final LinkedHashSet<SimpleGrantedAuthority> requiredAuthorities;

    public DlPathAuthorityCouple(AntPathRequestMatcher path, LinkedHashSet<SimpleGrantedAuthority> requiredAuthorities) {
        this.path = path;
        this.requiredAuthorities = requiredAuthorities;
    }

    public AntPathRequestMatcher getPath() {
        return path;
    }

    public LinkedHashSet<SimpleGrantedAuthority> getRequiredAuthorities() {
        return requiredAuthorities;
    }

}
