/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class DlAuthorityTable {
    private final ArrayList<DlPathAuthorityCouple> table;

    public DlAuthorityTable() {

        LinkedHashSet<SimpleGrantedAuthority> requiredAuthorities;
        this.table = new ArrayList<>();

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/taxonomy/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:delete"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/taxonomy/**","DELETE"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:update"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:update"));
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:update"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/association/content","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/reverse-engineer/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/smd/database/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:delete"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/smd/database/**","DELETE"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("refc:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("refc:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/admin/customform/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("user:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("user:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/admin/auth/**","GET"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("user:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("user:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/admin/auth/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("refc:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("refc:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/admin/class/detail","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:update"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:update"));
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("penv:update"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/common/fulldescription","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/data-models/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:delete"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/data-models/**","DELETE"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("home:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("home:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/home/overview","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("ont:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/ontology/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/dsdf/**","POST"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("dsdf:delete"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/dsdf/**","DELETE"),requiredAuthorities));

        requiredAuthorities = new LinkedHashSet<>();
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:create"));
        requiredAuthorities.add(new SimpleGrantedAuthority("dmf:update"));
        this.table.add(new DlPathAuthorityCouple(new AntPathRequestMatcher("/format/**","POST"),requiredAuthorities));

    }

    public ArrayList<DlPathAuthorityCouple> getTable() {
        return table;
    }
}
