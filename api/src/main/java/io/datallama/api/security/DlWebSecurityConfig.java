/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;

@Configuration
@EnableWebSecurity
@EnableWebMvc
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class DlWebSecurityConfig extends WebSecurityConfigurerAdapter {

    //public end points (don't require authentication)
    @Value("${dl.auth.public-end-points}")
    private String publicEndPoints;

    @Autowired
    private DlUserDetailsService dlUserDetailsService;

    @Autowired
    private DlAuthEntryPointJwt unauthorizedHandler;

    @Bean
    public DlAuthTokenFilter authenticationJwtTokenFilter() {
        return new DlAuthTokenFilter();
    }

    @Bean
    public DlAuthorisationFilter authorisationFilter() {return new DlAuthorisationFilter();}

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new DlMd5PasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

        authenticationManagerBuilder.userDetailsService(dlUserDetailsService).passwordEncoder(passwordEncoder());

        // add an authenticationManagerBuilder here for MS AD LDAP
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String[] publicEndPointsList = publicEndPoints.split(",");
        ArrayList<String> temp = new ArrayList<>();
        for(String publicEndPoint : publicEndPointsList){
            String[] values = publicEndPoint.split(":");
            temp.add(values[0]);
        }
        String[] publicUris = temp.toArray(new String[0]);

        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(publicUris).permitAll()
                .anyRequest().authenticated();

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(authorisationFilter(), FilterSecurityInterceptor.class);
    }

}
