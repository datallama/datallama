/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

public class DlAuthorisationFilter extends OncePerRequestFilter {

    @Autowired
    private DlUserDetailsService userDetailsService;

    // The list of public end points. These can be accessed without authentication
    @Value("${dl.auth.public-end-points}")
    private String publicEndPoints;

    /**
     * Checks that the request Headers are well formed and that the user is authorised to make the request
     * @param request
     * @param response
     * @param filterChain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if(!isPublicEndPoint(request)){
            if(request.getHeader("dlUserId") == null) {
                // The header is no well formed (dlUserId) so reject request as unauthorised (401).
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            // Load the permission list for the user and check if user has privileges for current request
            Collection<? extends GrantedAuthority> userAuthorities = userDetailsService.loadUserByUserId(Integer.parseInt(request.getHeader("dlUserId"))).getAuthorities();

            for(DlPathAuthorityCouple couple : new DlAuthorityTable().getTable()){
                if(couple.getPath().matches(request) && !userAuthorities.containsAll(couple.getRequiredAuthorities())){
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    return;
                }
            }
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Checks whether the request is on a public end point. If it isn't then authorization is required.
     * @param request
     * @return
     */
    private boolean isPublicEndPoint(HttpServletRequest request){
        String[] publicEndPointsList = publicEndPoints.split(",");
        for(String publicEndPoint : publicEndPointsList){
            String[] values = publicEndPoint.split(":");
            if(new AntPathRequestMatcher(values[0],values[1]).matches(request)){
                return true;
            }
        }
        return false;
    }
}
