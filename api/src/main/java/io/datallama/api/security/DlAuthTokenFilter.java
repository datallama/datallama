/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DlAuthTokenFilter extends OncePerRequestFilter {
    @Autowired
    private DlJwtUtils jwtUtils;

    @Autowired
    private DlUserDetailsService userDetailsService;

    private static final Logger logger = (Logger) LoggerFactory.getLogger(DlAuthTokenFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = parseJwt(request);
            if(request.getHeader("dlUserId") != null){
                if (jwt != null && jwtUtils.validateJwt(jwt)) {
                    String tokenUsername = jwtUtils.getUserNameFromJwt(jwt);
                    String headerUsername =  userDetailsService.loadUserByUserId(Integer.parseInt(request.getHeader("dlUserId"))).getUsername();

                    if(tokenUsername.equals(headerUsername)){
                        UserDetails userDetails = userDetailsService.loadUserByUsername(tokenUsername);
                        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                }
            }
            else{
                logger.debug("No dlUserId header sent with the request to: {}",request.getRequestURI());
            }
        } catch (Exception e) {
            logger.error("Cannot authenticate user: {}: {}", request.getRequestURI(), e.toString());
        }

        filterChain.doFilter(request, response);
    }

    private String parseJwt(HttpServletRequest request) {

        if(request.getCookies() != null){
            Cookie[] cookies = request.getCookies();
            for(Cookie cookie: cookies){
                if(cookie.getName().equals("auth_token")){
                    return cookie.getValue();
                }
            }
        }
        else{
            logger.debug("No cookies sent with the request to: {}",request.getRequestURI());
        }

        return null;
    }
}
