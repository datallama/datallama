/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import java.util.Map;

public class DlLoginResponse {
    private Integer userId;
    private Map<String, Boolean> groupPermissions;
    private Integer resultStatus;

    public DlLoginResponse(Integer userId, Map<String, Boolean> groupPermissions, Integer resultStatus) {
        this.userId = userId;
        this.resultStatus = resultStatus;
        this.groupPermissions = groupPermissions;
    }

    public Map<String, Boolean> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(Map<String, Boolean> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    public Integer getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Integer resultStatus) {
        this.resultStatus = resultStatus;
    }


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
