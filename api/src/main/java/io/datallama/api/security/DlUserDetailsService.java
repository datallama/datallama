/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

@Service
public class DlUserDetailsService implements UserDetailsService {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_by_username");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_username", username, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        DlUser user;
        if( out.get("_enabled") == null || out.get("_enabled") != (Integer) 1 ){
            throw new UsernameNotFoundException(String.format("No account found for user %s", username));
        }
        else{
            user = new DlUser(
                    (Integer)out.get("_user_id"),
                    username,
                    out.get("_firstname").toString(),
                    out.get("_lastname").toString(),
                    (Integer) out.get("_enabled")
            );
        }
        user.setPassword(out.get("_pwd_hash").toString());

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_group_perms");
        in = new MapSqlParameterSource().addValue("_user_id", user.getUserId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");
        ArrayList<String> authList = new ArrayList<>();
        for(Object listElement : dbResultList){
            LinkedCaseInsensitiveMap permInfo = (LinkedCaseInsensitiveMap) listElement;
            try {
                if((Boolean) permInfo.get("access_granted")){
                    String element = String.format("%s:%s",permInfo.get("module_code"),permInfo.get("action_category_name"));
                    authList.add(element);
                }
            } catch (NullPointerException n){
                System.out.println(n.toString());
                System.out.println("Warning! Failed to read a permission");
            }
        }
        user.setAuthList(authList);

        return new DlUserDetails(user);

    }

    public UserDetails loadUserByUserId(int userId) throws UsernameNotFoundException {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_name_by_user_id");
        MapSqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        String username = out.get("_username").toString();
        return loadUserByUsername(username);
    }
}
