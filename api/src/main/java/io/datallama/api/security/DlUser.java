/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.security;

import java.util.ArrayList;

/**
 * Stores the details of a user defined in the Data Llama internal user directory
 */
public class DlUser {
    private Integer userId;
    private String username;
    private String firstname;
    private String lastname;
    private Boolean enabled;
    private String groupList;
    private String emailAddress;
    private String password;    // only used when a new user is created
    private ArrayList<String> authList;

    public DlUser() {
        this.userId = 0;
        this.username = "";
        this.firstname = "";
        this.lastname = "";
        this.enabled = false;
        this.groupList = "";
        this.emailAddress = "";
        this.password = "";
        this.authList = new ArrayList<>();
    }

    public DlUser(Integer userId, String username, String firstname, String lastname, Boolean enabled) {
        this.userId = userId;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.enabled = enabled;
        this.groupList = "";
        this.emailAddress = "";
        this.password = "";
        this.authList = new ArrayList<>();
    }

    /**
     * This constructor variant is necessary because JDBC does not recognised MariaDB OUT variables as Boolean
     * @param userId
     * @param username
     * @param firstname
     * @param lastname
     * @param enabled
     */
    public DlUser(Integer userId, String username, String firstname, String lastname, Integer enabled) {
        this.userId = userId;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.groupList = "";
        this.emailAddress = "";
        this.authList = new ArrayList<>();
        this.enabled = enabled >= 1;
    }

    public ArrayList<String> getAuthList() {
        return authList;
    }

    public void setAuthList(ArrayList<String> authList) {
        this.authList = authList;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getGroupList() {
        return groupList;
    }

    public void setGroupList(String groupList) {
        this.groupList = groupList;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
