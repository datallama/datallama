/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;

@Configuration
@EnableWebMvc
public class DlWebConfig implements WebMvcConfigurer {

    //allowed Credential CORS origins
    @Value("${dl.cors.allowed-origins}")
    private String origins;

    //allowed end points for CORS
    @Value("${dl.cors.allowed-end-points}")
    private String allowedEndPoints;

    @Override
    public void addCorsMappings(CorsRegistry registry) {

        String[] publicEndPointsList = allowedEndPoints.split(",");
        for(String publicEndPoint : publicEndPointsList){
            String[] values = publicEndPoint.split(":");
            registry.addMapping(values[0]).allowedOrigins("*");
        }

        String[] originsList = origins.split(",");
        for(String origin : originsList){
            registry.addMapping("/**").allowedOrigins(origin).allowCredentials(true);
        }
    }
}
