/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.security;

import org.jose4j.json.internal.json_simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class DlAuthController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Autowired
    private DlJwtUtils jwtUtils;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private DlUserDetailsService dlUserDetailsService;

    @Autowired
    PasswordEncoder encoder;

    @Value("${dl.auth.oidc.redirect-entry-url}")
    private String entryUrl;

    @Value("${dl.auth.oidc.logout-exit-url}")
    private String exitUrl;

    @Value("${dl.security.ui.cookies.secure:false}")
    private String cookieSecure;

    /**
     * This DL API end point is called when a login attempt fails.
     * URI: /auth/login_failed
     */
    @RequestMapping(method = GET, value = "/auth/login_failed")
    @ResponseBody
    public Map<String, Object> loginAuthentication() {
        System.out.println("User authentication failure occurred.");
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = -10; // this indicates status of database results.

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * This DL API end point is called when a user attempts to authenticate and the Microsoft Identity Platform Login
     * is the configured user directory for authentication.
     * URI: /auth/redirect/msoidc
     * @param request_payload
     * @param response
     * @return
     */
    @RequestMapping(method = POST, value = "/auth/redirect/msoidc")
    @ResponseBody
    public ResponseEntity<String> mipLogin(@RequestBody String request_payload, HttpServletResponse response){

        String[] elements = request_payload.split("&");
        for(String element:elements){
            String[] value = element.split("=");
            if(value[0].equals("id_token") && jwtUtils.validateJwt(value[1])){

                response.addHeader("Set-Cookie","auth_token=" + value[1] + "; Path=/; HttpOnly; Secure; max-age=36000");

                String username = jwtUtils.getUserNameFromJwt(value[1]);
                DlUserDetails userDetails = (DlUserDetails)dlUserDetailsService.loadUserByUsername(username);

                response.addHeader("Set-Cookie","user_id=" + userDetails.getUserId() + "; Path=/; SameSite=strict; Secure; max-age=36000");

                //get the group permissions for authenticated user
                SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_group_perms");
                MapSqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userDetails.getUserId(), Types.VARCHAR);
                Map<String, Object> out = jdbcCall.execute(in);
                ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

                Map<String, Boolean> groupPermissions = new HashMap<>();
                for (Object listElement : dbResultList) {
                    LinkedCaseInsensitiveMap permInfo = (LinkedCaseInsensitiveMap) listElement;
                    String permkey = permInfo.get("module_code").toString() + "_" + permInfo.get("action_category_name").toString();
                    groupPermissions.put(permkey, (Boolean) permInfo.get("access_granted"));
                }

                response.addHeader("Set-Cookie","group_perms=" + new JSONObject(groupPermissions).toJSONString() + "; Path=/; SameSite=strict; Secure; max-age=36000");

                return new ResponseEntity<>(
                        "<html><head><script type=\"text/javascript\">" +
                        "const dlAjsAppUrl = \"" + entryUrl + "\"; " +
                        "window.onload = function() {window.location.href = dlAjsAppUrl;}" +
                        "</script></head></html>",
                        HttpStatus.OK
                );
            }
        }

        return new ResponseEntity<>(
                "",
                HttpStatus.UNAUTHORIZED
        );

    }

    /**
     * This DL API end point is called when a user attempts to authenticate and the Data Llama Internal Login Database
     * is the configured user directory for authentication.
     * URI: /auth/login02
     * @param loginRequest
     * @param response
     * @return
     */
    @RequestMapping(method = POST, value = "/auth/login02")
    @ResponseBody
    public ResponseEntity<?> dlintLogin(@Valid @RequestBody DlLoginRequest loginRequest, HttpServletResponse response) {
        String cookieSecurity = "";
        if (cookieSecure.toUpperCase().equals("TRUE")) {
            cookieSecurity = "SameSite=Strict; Secure;";
        }
        else {
            cookieSecurity = "SameSite=Lax;";
        }

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        try{
            String jwt = jwtUtils.generateDlJwt(authentication);
            String headerValue = String.format("auth_token=%s; Path=/; HttpOnly; %s max-age=36000", jwt, cookieSecurity);
            response.addHeader("Set-Cookie", headerValue);

        } catch (Exception e){
            response.addHeader("Set-Cookie", String.format("auth_token=; Path=/; HttpOnly; %s max-age=0", cookieSecurity));
            response.addHeader("Set-Cookie", String.format("user_id=; Path=/; %s max-age=0", cookieSecurity));
            response.addHeader("Set-Cookie", String.format("group_perms=; Path=/; %s max-age=0", cookieSecurity));
            return new ResponseEntity<>(
                    "",
                    HttpStatus.UNAUTHORIZED
            );
        }


        DlUserDetails userDetails = (DlUserDetails) authentication.getPrincipal();
        response.addHeader("Set-Cookie", String.format("user_id=%d; Path=/; SameSite=Lax; %s max-age=36000", userDetails.getUserId(), cookieSecurity));

        //get the group permissions for authenticated user
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_user_group_perms");
        MapSqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userDetails.getUserId(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        Map<String, Boolean> groupPermissions = new HashMap<>();
        String groupPerms = new JSONObject(groupPermissions).toJSONString();
        response.addHeader("Set-Cookie", String.format("group_perms=%s; Path=/; SameSite=Lax; %s max-age=36000", groupPerms, cookieSecurity));

        String permkey;
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap permInfo = (LinkedCaseInsensitiveMap) listElement;
            permkey = permInfo.get("module_code").toString() + "_" + permInfo.get("action_category_name").toString();
            groupPermissions.put(permkey, (Boolean) permInfo.get("access_granted"));
        }

        return ResponseEntity.ok(new DlLoginResponse(
                userDetails.getUserId(),
                groupPermissions,
                1
        ));
    }

    /**
     * This DL API end point is called when the user logs out and the Data Llama Internal Database
     * is configured as the user directory
     * @param response
     * @return
     */
    @RequestMapping(method = POST, value = "/auth/logout02")
    @ResponseBody
    public ResponseEntity<?> dlintLogout(HttpServletResponse response) {
        String cookieSecurity = "";
        if (cookieSecure.toUpperCase().equals("TRUE")) {
            cookieSecurity = "SameSite=Strict; Secure;";
        }
        else {
            cookieSecurity = "SameSite=Lax;";
        }

        response.addHeader("Set-Cookie",String.format("auth_token=; Path=/; HttpOnly; %s max-age=0", cookieSecurity));
        response.addHeader("Set-Cookie",String.format("user_id=; Path=/; HttpOnly; %s max-age=0", cookieSecurity));
        response.addHeader("Set-Cookie",String.format("group_perms=; Path=/; HttpOnly; %s max-age=0", cookieSecurity));
        return new ResponseEntity<>(
                "",
                HttpStatus.OK
        );
    }

    /**
     * This DL API end point is called when the user logs out and the Microsoft Identity Platform
     * is configured as the user directory
     * @param response
     * @return
     */
    @RequestMapping(method = GET, value = "/auth/logout-redirect/msoidc")
    @ResponseBody
    public ResponseEntity<?> mipLogout(HttpServletResponse response) {
        response.addHeader("Set-Cookie","auth_token=; Path=/; HttpOnly; Secure; max-age=0");
        response.addHeader("Set-Cookie","user_id=; Path=/; HttpOnly; Secure; max-age=0");
        response.addHeader("Set-Cookie","group_perms=; Path=/; HttpOnly; Secure; max-age=0");
        return new ResponseEntity<>(
                "<html><head><script type=\"text/javascript\">" +
                        "const dlAjsAppUrl = \"" + exitUrl + "\"; " +
                        "window.onload = function() {window.location.href = dlAjsAppUrl;}" +
                        "</script></head></html>",
                HttpStatus.OK
        );
    }

    /**
     * URI: /auth/changepassword
     */
    @RequestMapping(method = POST, value = "/auth/changepassword")
    @ResponseBody
    public Map<String, Object> changePassword(@RequestBody UserPasswordChange userCredentials, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // first, get the credentials for the database
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_user_password");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_current_password", userCredentials.getCurrentPassword(), Types.VARCHAR)
                .addValue("_new_password", userCredentials.getNewPassword(), Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        System.out.println(out.get("_pwd_change_status"));

        resultData.put("pwd_change_status", out.get("_pwd_change_status"));
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Descritpion: utility class to receive user credentials from POST call
     */
    static class UserPasswordChange {
        private Integer userId;
        private String currentPassword;
        private String newPassword;

        public UserPasswordChange() {
            this.userId = 0;
            this.currentPassword = "";
            this.newPassword = "";
        }

        public UserPasswordChange(String currentPassword, String newPassword) {
            this.userId = 0;
            this.currentPassword = currentPassword;
            this.newPassword = newPassword;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getCurrentPassword() {
            return currentPassword;
        }

        public void setCurrentPassword(String currentPassword) {
            this.currentPassword = currentPassword;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }
    }

}