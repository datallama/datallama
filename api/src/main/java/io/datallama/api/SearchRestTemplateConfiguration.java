package io.datallama.api;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

@Configuration
public class SearchRestTemplateConfiguration {

    @Value("${dl.srch.trust.store}")
    private Resource trustStore;

    @Value("${dl.srch.trust.store.password}")
    private String trustStorePassword;

    /**
     * Creates and returns a RestTemplate that is configures with SSL using the certificate in the java truststore
     *  identified by class members trustStore and trustStorePassword
     *
     * @return RestTemplate
     * @throws KeyManagementException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws CertificateException
     * @throws IOException
     */
    @Bean
    public RestTemplate restTemplate() throws KeyManagementException, NoSuchAlgorithmException,
            KeyStoreException, CertificateException, IOException {

        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(
                trustStore.getURL(),
                trustStorePassword.toCharArray()
        ).build();

        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
        HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);

        return new RestTemplate(factory);
    }

}

