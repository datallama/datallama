/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.taxonomy;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlSqlError;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public class TaxonomyUtil {
    private DriverManagerDataSource dataSource;

    /**
     * Default constructor.
     * @param dataSource
     */
    public TaxonomyUtil(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Retrieves from the DL DB core attributes of the Taxonomy Summary identified by objectId and returns them as
     * a Taxonomy object.
     * @param objectId
     * @param abbreviate
     * @return
     */
    public Map<String, Object> getTaxonomySummary (Integer objectId, Integer abbreviate) {
        Map<String, Object> results = new HashMap<>();
        DlSqlError sqlError = new DlSqlError();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. get the core attributes for the taxonomy summary
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_taxonomy_id", objectId, Types.INTEGER)
                .addValue("_abbreviate", abbreviate, Types.INTEGER);
        // TODO: put execute statement in try/catch and store results in sqlError object
        Map<String, Object> out = jdbcCall.execute(in);

        // 1.1. create taxonomy object from stored proc results
        Taxonomy taxonomy = new Taxonomy(
                objectId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_abbr_scheme_id"),
                out.get("_taxonomy_name").toString(),
                out.get("_class_name").toString(),
                out.get("_taxonomy_shortdesc").toString()
        );
        taxonomy.setObjectGUID(out.get("_object_guid").toString());

        // 1.2. instantiate a custom form object and add it to the taxonomy object
        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);

        taxonomy.setCustomForm(customForm);

        results.put("result_status", resultStatus);
        results.put("sql_error", sqlError);
        results.put("taxonomy", taxonomy);

        return results;
    }

    /**
     * Retrieves from the DL DB the objectId and classId for the taxonomy md_object identified by objectGUID.
     * Then calls getTaxonomySummary to retrieve the taxonomy summary object based on objectId.
     * @param objectGUID
     * @return
     */
    public Map<String, Object> getTaxonomySummaryByGUID (String objectGUID) {
        Map<String, Object> results = new HashMap<>();
        DlSqlError sqlError = new DlSqlError();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. get the core attributes for the taxonomy summary
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_ids_by_guid");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_guid", objectGUID, Types.VARCHAR);
        // TODO: put execute statement in try/catch and store results in sqlError object
        Map<String, Object> out = jdbcCall.execute(in);

        Integer objectId = (Integer) out.get("_object_id");
        Integer classId = (Integer) out.get("_class_id");

        results = this.getTaxonomySummary(objectId, 0);

        return results;
    }

    /**
     * Retrieves from the DL DB core attributes of the Taxonomy Element identified by objectId and returns them as
     * a TaxonomyElement object.
      * @param objectId
     * @return
     */
    public Map<String, Object> getTaxonomyElement (Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        DlSqlError sqlError = new DlSqlError();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1: get the core attributes of the element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_element_id", objectId, Types.INTEGER);
        // TODO: put execute statement in try/catch and store results in sqlError object
        Map<String, Object> out = jdbcCall.execute(in);

        TaxonomyElement element = new TaxonomyElement(
                (Integer) out.get("_taxonomy_id"),
                objectId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_element_parent_id"),
                out.get("_element_guid").toString(),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_hierarchy_number").toString(),
                out.get("_element_name").toString(),
                out.get("_element_shortdesc").toString()
        );
        element.setCustomFormData(out.get("_custom_form_data").toString());

        // 1.1. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        element.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        results.put("result_status", resultStatus);
        results.put("sql_error", sqlError);
        results.put("taxonomy_element", element);

        return results;
    }

    /**
     * Retrieves from the DL DB the objectId and classId for the taxonomy element md_object identified by objectGUID.
     * Then calls getTaxonomyElement to retrieve the taxonomy element object based on objectId.
     * @param objectGUID
     * @return
     */
    public Map<String, Object> getTaxonomyElementByGUID (String objectGUID) {
        Map<String, Object> results = new HashMap<>();
        DlSqlError sqlError = new DlSqlError();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. get the core attributes for the taxonomy summary
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_element_ids_by_guid");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_guid", objectGUID, Types.VARCHAR);
        // TODO: put execute statement in try/catch and store results in sqlError object
        Map<String, Object> out = jdbcCall.execute(in);

        Integer objectId = (Integer) out.get("_object_id");
        Integer classId = (Integer) out.get("_class_id");

        results = this.getTaxonomyElement(objectId);

        return results;
    }

}
