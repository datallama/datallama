/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.taxonomy;

import java.util.Date;

public class TaxonomyElementArchive {
    private Long elementArchiveId;
    private String elementName;
    private String elementShortDesc;
    private String elementHierarchyNumber;
    private String changeAction;
    private Date archiveTimestamp;
    private String archiveTimestampString;
    private Integer changeUserId;
    private String changeUserLogin;
    private Date lastChangeTimestamp;
    private String lastChangeTimestampString;
    private Integer lastChangeUserId;
    private String lastChangeUserLogin;

    public TaxonomyElementArchive() {
        this.elementArchiveId = Long.valueOf(0);
        this.elementName = "";
        this.elementShortDesc = "";
        this.elementHierarchyNumber = "";
        this.changeAction = "";
        this.archiveTimestamp = new Date();
        this.archiveTimestampString = "";
        this.changeUserId = 0;
        this.changeUserLogin = "";
        this.lastChangeTimestamp = new Date();
        this.lastChangeTimestampString = "";
        this.lastChangeUserId = 0;
        this.lastChangeUserLogin = "";
    }

    public TaxonomyElementArchive(Long elementArchiveId, String elementName, String elementShortDesc,
                                  String elementHierarchyNumber, String lastChangeTimestampString,
                                  Integer lastChangeUserId, String lastChangeUserLogin) {
        this.elementArchiveId = elementArchiveId;
        this.elementName = elementName;
        this.elementShortDesc = elementShortDesc;
        this.elementHierarchyNumber = elementHierarchyNumber;
        this.changeAction = "";
        this.archiveTimestamp = new Date();
        this.archiveTimestampString = "";
        this.changeUserId = 0;
        this.changeUserLogin = "";
        this.lastChangeTimestamp = new Date();
        this.lastChangeTimestampString = lastChangeTimestampString;
        this.lastChangeUserId = lastChangeUserId;
        this.lastChangeUserLogin = lastChangeUserLogin;
    }

    public Long getElementArchiveId() {
        return elementArchiveId;
    }

    public void setElementArchiveId(Long elementArchiveId) {
        this.elementArchiveId = elementArchiveId;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementShortDesc() {
        return elementShortDesc;
    }

    public void setElementShortDesc(String elementShortDesc) {
        this.elementShortDesc = elementShortDesc;
    }

    public String getElementHierarchyNumber() {
        return elementHierarchyNumber;
    }

    public void setElementHierarchyNumber(String elementHierarchyNumber) {
        this.elementHierarchyNumber = elementHierarchyNumber;
    }

    public String getChangeAction() {
        return changeAction;
    }

    public void setChangeAction(String changeAction) {
        this.changeAction = changeAction;
    }

    public void setChangeActionByFlag(String changeFlag) {
        if (changeFlag.equals("U"))
            setChangeAction("Update");
        else if (changeFlag.equals("D"))
            setChangeAction("Delete");
        else if (changeFlag.equals("M"))
            setChangeAction("Move");
        else if (changeFlag.equals("L"))
            setChangeAction("Latest");
    }

    public Date getArchiveTimestamp() {
        return archiveTimestamp;
    }

    public void setArchiveTimestamp(Date archiveTimestamp) {
        this.archiveTimestamp = archiveTimestamp;
    }

    public String getArchiveTimestampString() {
        return archiveTimestampString;
    }

    public void setArchiveTimestampString(String archiveTimestampString) {
        this.archiveTimestampString = archiveTimestampString;
    }

    public Integer getChangeUserId() {
        return changeUserId;
    }

    public void setChangeUserId(Integer changeUserId) {
        this.changeUserId = changeUserId;
    }

    public String getChangeUserLogin() {
        return changeUserLogin;
    }

    public void setChangeUserLogin(String changeUserLogin) {
        this.changeUserLogin = changeUserLogin;
    }

    public Date getLastChangeTimestamp() {
        return lastChangeTimestamp;
    }

    public void setLastChangeTimestamp(Date lastChangeTimestamp) {
        this.lastChangeTimestamp = lastChangeTimestamp;
    }

    public String getLastChangeTimestampString() {
        return lastChangeTimestampString;
    }

    public void setLastChangeTimestampString(String lastChangeTimestampString) {
        this.lastChangeTimestampString = lastChangeTimestampString;
    }

    public Integer getLastChangeUserId() {
        return lastChangeUserId;
    }

    public void setLastChangeUserId(Integer lastChangeUserId) {
        this.lastChangeUserId = lastChangeUserId;
    }

    public String getLastChangeUserLogin() {
        return lastChangeUserLogin;
    }

    public void setLastChangeUserLogin(String lastChangeUserLogin) {
        this.lastChangeUserLogin = lastChangeUserLogin;
    }
}
