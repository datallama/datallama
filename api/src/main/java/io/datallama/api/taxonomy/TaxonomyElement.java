/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.taxonomy;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class TaxonomyElement {
    private Integer taxonomyId;
    private Integer elementId;
    private Integer classId;
    private Integer elementParentId;
    private Integer elementLevel;
    private Integer elementLevelSequence;
    private String elementGUID;
    private String elementHierarchyNumber;
    private String elementName;
    private String elementDesc;
    private String customFormData;
    private String elementFullPathName;
    private Integer elementVisible;
    private Integer initialExpandLevel;
    private String indentCss;
    private Boolean expanded;
    private Boolean parent;
    private Integer resultSetSequence;
    private Integer elementSelected;
    private Boolean elementMatched;
    private ArrayList<DlMetaDataContextNode> mdContext;

    public TaxonomyElement() {
        this.taxonomyId = 0;
        this.elementId = 0;
        this.classId = 0;
        this.elementParentId = 0;
        this.elementGUID = "";
        this.elementLevel = 0;
        this.elementLevelSequence = 0;
        this.elementHierarchyNumber = "";
        this.elementName = "";
        this.elementDesc = "";
        this.customFormData = "";
        this.elementVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.elementSelected = 0;
        this.elementMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    public TaxonomyElement(Integer taxonomyId, Integer elementId, Integer classId, Integer elementParentId, String elementGUID, Integer elementLevel,
                           Integer elementLevelSequence, String elementHierarchyNumber, String elementName, String elementDesc) {
        this.taxonomyId = taxonomyId;
        this.elementId = elementId;
        this.classId = classId;
        this.elementParentId = elementParentId;
        this.elementGUID = elementGUID;
        this.elementLevel = elementLevel;
        this.elementLevelSequence = elementLevelSequence;
        this.elementHierarchyNumber = elementHierarchyNumber;
        this.elementName = elementName;
        this.elementDesc = elementDesc;
        this.customFormData = "";
        this.elementVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.elementSelected = 0;
        this.elementMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();

        this.determineVisibility();
        this.determineExpanded();
        this.generateIndentCss();
    }

    public Integer getTaxonomyId() {
        return taxonomyId;
    }
    public Integer getParentObjectId() {
        return taxonomyId;
    }

    public void setTaxonomyId(Integer taxonomyId) {
        this.taxonomyId = taxonomyId;
    }
    public void setParentObjectId(Integer objectId) {
        this.taxonomyId = objectId;
    }

    public Integer getElementId() {
        return elementId;
    }
    public Integer getObjectId() {
        return elementId;
    }
    public void setElementId(Integer elementId) {
        this.elementId = elementId;
    }
    public void setObjectId(Integer objectId) {
        this.elementId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getElementParentId() {
        return elementParentId;
    }
    public Integer getParentElementObjectId() {
        return elementParentId;
    }
    public void setElementParentId(Integer elementParentId) {
        this.elementParentId = elementParentId;
    }
    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.elementParentId = parentElementObjectId;
    }

    public String getElementGUID() {
        return elementGUID;
    }
    public String getObjectGUID() {
        return elementGUID;
    }
    public void setElementGUID(String elementGUID) {
        this.elementGUID = elementGUID;
    }
    public void setObjectGUID(String objectGUID) {
        this.elementGUID = objectGUID;
    }

    public Integer getElementLevel() {
        return elementLevel;
    }
    public void setElementLevel(Integer elementLevel) {
        this.elementLevel = elementLevel;
    }

    public Integer getElementLevelSequence() {
        return elementLevelSequence;
    }
    public void setElementLevelSequence(Integer elementLevelSequence) {
        this.elementLevelSequence = elementLevelSequence;
    }

    public String getElementHierarchyNumber() {
        return elementHierarchyNumber;
    }
    public void setElementHierarchyNumber(String elementHierarchyNumber) {
        this.elementHierarchyNumber = elementHierarchyNumber;
    }

    public String getElementName() {
        return elementName;
    }
    public String getObjectName() {
        return elementName;
    }
    public void setElementName(String elementName) {
        this.elementName = elementName;
    }
    public void setObjectName(String objectName) {
        this.elementName = objectName;
    }

    public String getElementDesc() {
        return elementDesc;
    }
    public String getObjectShortDesc() {
        return elementDesc;
    }
    public void setElementDesc(String elementDesc) {
        this.elementDesc = elementDesc;
    }
    public void setObjectShortDesc(String objectShortDesc) {
        this.elementDesc = objectShortDesc;
    }

    public String getCustomFormData() {
        return customFormData;
    }

    public void setCustomFormData(String customFormData) {
        this.customFormData = customFormData;
    }

    public Integer getElementVisible() {
        return elementVisible;
    }

    public void setElementVisible(Integer elementVisible) {
        this.elementVisible = elementVisible;
    }

    public void determineVisibility() {
        elementVisible = (elementLevel <= initialExpandLevel) ? 1 : 0;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public String getIndentCss() {
        return indentCss;
    }

    public void setIndentCss(String indentCss) {
        this.indentCss = indentCss;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public void generateIndentCss() {
        this.indentCss = "{'margin-left':" + Integer.toString(elementLevel * 15) + ";}";
    }

    public void determineExpanded() {
        this.expanded = (elementLevel < initialExpandLevel) ? true : false;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public Integer getElementSelected() {
        return elementSelected;
    }

    public void setElementSelected(Integer elementSelected) {
        this.elementSelected = elementSelected;
    }

    public Boolean getElementMatched() {
        return elementMatched;
    }

    public void setElementMatched(Boolean elementMatched) {
        this.elementMatched = elementMatched;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }
    public String getElementFullPathName() {
        return elementFullPathName;
    }

    public void setElementFullPathName(String elementFullPathName) {
        // TODO: the name formatting should be configurable
        this.elementFullPathName = elementFullPathName.toLowerCase().replaceAll("\\s", "_");
    }

}
