/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.taxonomy;

import io.datallama.api.common.DlCustomForm;

public class Taxonomy {
    private Integer taxonomyId;
    private Integer classId;
    private Integer abbreviationSchemeId;
    private String taxonomyName;
    private String className;
    private String objectGUID;
    private String taxonomyShortDesc;
    private DlCustomForm customForm;

    /**
     * Description: default constructor used with POST calls
     */
    public Taxonomy() {
        this.taxonomyId = 0;
        this.classId = 0;
        this.abbreviationSchemeId = 0;
        this.taxonomyName = "";
        this.className = "";
        this.objectGUID = "";
        this.taxonomyShortDesc = "";
        this.customForm = new DlCustomForm();
    }

    /**
     * Full constructor used when processing database result sets
     * @param taxonomyId
     * @param classId
     * @param taxonomyName
     * @param className
     * @param taxonomyShortDesc
     */
    public Taxonomy(Integer taxonomyId, Integer classId, Integer abbreviationSchemeId,
                    String taxonomyName, String className, String taxonomyShortDesc) {
        this.taxonomyId = taxonomyId;
        this.classId = classId;
        this.abbreviationSchemeId = abbreviationSchemeId;
        this.taxonomyName = taxonomyName;
        this.className = className;
        this.objectGUID = "";
        this.taxonomyShortDesc = taxonomyShortDesc;
        this.customForm = new DlCustomForm();
    }

    public Integer getTaxonomyId() {
        return taxonomyId;
    }
    public Integer getObjectId() {
        return taxonomyId;
    }
    public void setTaxonomyId(Integer taxonomyId) {
        this.taxonomyId = taxonomyId;
    }
    public void setObjectId(Integer objectId) {
        this.taxonomyId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }
    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {return new Integer(0); };
    public Integer getParentClassId() {return new Integer(0);};

    public Integer getAbbreviationSchemeId() {
        return abbreviationSchemeId;
    }
    public void setAbbreviationSchemeId(Integer abbreviationSchemeId) {
        this.abbreviationSchemeId = abbreviationSchemeId;
    }

    public String getTaxonomyName() {
        return taxonomyName;
    }
    public String getObjectName() {
        return taxonomyName;
    }
    public void setTaxonomyName(String taxonomyName) {
        this.taxonomyName = taxonomyName;
    }
    public void setObjectName(String objectName) {
        this.taxonomyName = objectName;
    }

    public String getClassName() {
        return className;
    }
    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getTaxonomyShortDesc() {
        return taxonomyShortDesc;
    }
    public String getObjectShortDesc() {
        return taxonomyShortDesc;
    }
    public void setTaxonomyShortDesc(String taxonomyShortDesc) {
        this.taxonomyShortDesc = taxonomyShortDesc;
    }
    public void setObjectShortDesc(String objectShortDesc) {
        this.taxonomyShortDesc = objectShortDesc;
    }

    public Integer getCustomFormId() {
        return this.customForm.getCustomFormId();
    }

    public void setCustomFormId(Integer customFormId) {
        this.customForm.setCustomFormId(customFormId);
    }

    public String getCustomFormName() {
        return this.customForm.getCustomFormName();
    }

    public void setCustomFormName(String customFormName) {
        this.customForm.setCustomFormName(customFormName);
    }

    public String getSchemaDefinition() {
        return this.customForm.getSchemaDefinition();
    }

    public void setSchemaDefinition(String schemaDefinition) {
        this.customForm.setSchemaDefinition(schemaDefinition);
    }

    public String getFormDefinition() {
        return this.customForm.getFormDefinition();
    }

    public void setFormDefinition(String formDefinition){
        this.customForm.setFormDefinition(formDefinition);
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }
}
