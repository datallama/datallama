/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.taxonomy;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.MoComment;
import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.metamodel.DlMetaDataContextNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class TaxonomyController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Description: Reads the list of all taxonomies in the data base
     * URL Path: /taxonomy/list
    */
    @RequestMapping(method = GET, value = "/taxonomy/list")
    @ResponseBody
    public Map<String, Object> listTaxonomies() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList taxonomyList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomies_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // 2. create a hierarchy of Array Lists from the simple result set returned by database
        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            Taxonomy taxonomy = new Taxonomy(
                    (Integer) elementInfo.get("taxonomy_id"),
                    (Integer) elementInfo.get("class_id"),
                    (Integer) elementInfo.get("attr_scheme_id"),
                    elementInfo.get("taxonomy_name").toString(),
                    elementInfo.get("class_name").toString(),
                    elementInfo.get("taxonomy_shortdesc").toString()
            );
            taxonomyList.add(taxonomy);
        }

        results.put("result_data", taxonomyList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /taxonomy/content/{taxonomyId}
     * Description: Reads all elements in the taxonomy identified by {taxonomyId}
     */
    @RequestMapping(method = GET, value = "/taxonomy/content/{taxonomyId}/{abbreviate}")
    @ResponseBody
    public Map<String, Object> readTaxonomyContent(@PathVariable Integer taxonomyId, @PathVariable Integer abbreviate) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> utilResults;
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList taxonomyElementList = new ArrayList(); // this list will be returned in the results Map
        Map<Integer, TaxonomyElement> parentLookup = new HashMap<>(); // this is used to lookup parent PTs when adding children

        // 1. get the core details for the taxonomy
        TaxonomyUtil taxonomyUtil = new TaxonomyUtil(dataSource);
        utilResults = taxonomyUtil.getTaxonomySummary(taxonomyId, abbreviate);
        Taxonomy taxonomy = (Taxonomy) utilResults.get("taxonomy");
        resultData.put("taxonomy_summary", taxonomy);
        resultData.put("custom_form", taxonomy.getCustomForm());

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_elements");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_taxonomy_id", taxonomyId, Types.INTEGER)
                .addValue("_abbreviate", abbreviate, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the md_context json array returned by stored procedure

        // 2. generate a TaxonomyElement object for every row in result set and add to Array List taxonomyElementList
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            TaxonomyElement element = new TaxonomyElement(
                    taxonomyId,
                    (Integer) elementInfo.get("element_id"),
                    (Integer) elementInfo.get("class_id"),
                    (Integer) elementInfo.get("element_parent_id"),
                    elementInfo.get("element_guid").toString(),
                    (Integer) elementInfo.get("element_level"),
                    (Integer) elementInfo.get("element_level_sequence"),
                    elementInfo.get("element_hierarchy_number").toString(),
                    elementInfo.get("element_name").toString(),
                    ""
                    );
            element.setResultSetSequence(resultSetSequence);
            resultSetSequence++;
            element.setParent((Boolean) elementInfo.get("element_is_parent"));
            element.setElementDesc(elementInfo.get("element_shortdesc").toString());

            // 2.1. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

            taxonomyElementList.add(element);

        }

        resultData.put("taxonomy_elements", taxonomyElementList);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns all attributes for the Taxonomy summary md_object identified by taxonomyId.
     * The abbreviate parameter indicates whether an abbreviation scheme should be applied to element names in the taxonomy.
     * It is reserved for future use.
     * @param taxonomyId
     * @param abbreviate
     * @return
     */
    @RequestMapping(method = GET, value = "/taxonomy/summary/{taxonomyId}/{abbreviate}")
    @ResponseBody
    public Map<String, Object> readTaxonomySummary(@PathVariable Integer taxonomyId, @PathVariable Integer abbreviate) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> utilResults;
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map

        TaxonomyUtil taxonomyUtil = new TaxonomyUtil(dataSource);
        utilResults = taxonomyUtil.getTaxonomySummary(taxonomyId, abbreviate);
        Taxonomy taxonomy = (Taxonomy) utilResults.get("taxonomy");
        resultData.put("taxonomy_summary", taxonomy);
        resultData.put("custom_form", taxonomy.getCustomForm());

        // 3. get the associations for the taxonomy
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_class_id", taxonomy.getClassId(), Types.INTEGER)
                .addValue("_object_id", taxonomyId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);
        }

        resultData.put("associations", associationList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns all attributes for the Taxonomy summary md_object identified by objectGUID
     * @param objectGUID
     * @return
     */
    @RequestMapping(method = GET, value = "/taxonomy/summary/guid/{objectGUID}")
    @ResponseBody
    public Map<String, Object> readTaxonomySummaryByGUID(@PathVariable String objectGUID) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> utilResults;

        TaxonomyUtil taxonomyUtil = new TaxonomyUtil(dataSource);
        utilResults = taxonomyUtil.getTaxonomySummaryByGUID(objectGUID);

        results.put("result_data", utilResults.get("taxonomy"));
        results.put("result_status", utilResults.get("result_status"));
        results.put("sql_error", utilResults.get("sql_error"));

        return(results);
    }

    /**
     * Description: saves a new taxonomy in the DL database
     * TODO: path should be changed to /taxonomy/summary
     */
    @RequestMapping(method = POST, value = "/taxonomy/content")
    public Map<String, Object> writeTaxonomyContent(@RequestBody Taxonomy newTaxonomy, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        System.out.println(newTaxonomy.getClassId());
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_taxonomy");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_taxonomy_id", newTaxonomy.getTaxonomyId(), Types.INTEGER)
                .addValue("_class_id", newTaxonomy.getClassId(), Types.INTEGER)
                .addValue("_taxonomy_name",  newTaxonomy.getTaxonomyName(), Types.VARCHAR)
                .addValue("_taxonomy_shortdesc", newTaxonomy.getTaxonomyShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap newID = (LinkedCaseInsensitiveMap) rslt.get(0);
        resultData.put("returnID", newID.get("returnID"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Reads the details for the given taxonomy element
     * URL Path: /taxonomy/element/{taxonomyId}
     */
    @RequestMapping(method = GET, value = "/taxonomy/element/{elementId}")
    @ResponseBody
    public Map<String, Object> readTaxonomyElement(@PathVariable Integer elementId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> utilResults = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // this list is returned in the results Map

        // 1: get the core attributes of the element
        TaxonomyUtil taxonomyUtil = new TaxonomyUtil(dataSource);
        utilResults = taxonomyUtil.getTaxonomyElement(elementId);
        TaxonomyElement taxonomyElement = (TaxonomyElement) utilResults.get("taxonomy_element");

        resultData.put("attributes", taxonomyElement);

        // 2: get the associations for the element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", taxonomyElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", taxonomyElement.getElementId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);
        }
        resultData.put("associations", associationList);

        // 3. get comments for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", taxonomyElement.getElementId(), Types.INTEGER)
                .addValue("_class_id", taxonomyElement.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }
        resultData.put("comments", commentList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/taxonomy/element/guid/{objectGUID}")
    @ResponseBody
    public Map<String, Object> readTaxonomyElementByGUID(@PathVariable String objectGUID) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> utilResults = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // this list is returned in the results Map

        // 1: get the core attributes of the element
        TaxonomyUtil taxonomyUtil = new TaxonomyUtil(dataSource);
        utilResults = taxonomyUtil.getTaxonomyElementByGUID(objectGUID);
        TaxonomyElement taxonomyElement = (TaxonomyElement) utilResults.get("taxonomy_element");

        resultData.put("attributes", taxonomyElement);

        // 2: get the associations for the element
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_class_id", taxonomyElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", taxonomyElement.getElementId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);
        }
        resultData.put("associations", associationList);

        // 3. get comments for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", taxonomyElement.getElementId(), Types.INTEGER)
                .addValue("_class_id", taxonomyElement.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }
        resultData.put("comments", commentList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Writes a new taxonomy element to the DL database.
     * URL Path: /taxonomy/element/
     */
    @RequestMapping(method = POST, value = "/taxonomy/element")
    public Map<String, Object> writeTaxonomyElement(@RequestBody TaxonomyElement newElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // TODO: lookup user id from DlAuthDB using the API Token in Authorization Header
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_taxonomy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_taxonomy_id", newElement.getTaxonomyId(), Types.INTEGER)
                .addValue("_element_id", newElement.getElementId(), Types.INTEGER)
                .addValue("_parent_id",  newElement.getElementParentId(), Types.INTEGER)
                .addValue("_element_guid",  newElement.getElementGUID(), Types.VARCHAR)
                .addValue("_element_name",  newElement.getElementName(), Types.VARCHAR)
                .addValue("_element_shortdesc", newElement.getElementDesc(), Types.VARCHAR)
                .addValue("_custom_form_data", newElement.getCustomFormData(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) rslt.get(0);

        TaxonomyElement element = new TaxonomyElement(
                (Integer) elementInfo.get("taxonomy_id"),
                (Integer) elementInfo.get("element_id"),
                (Integer) elementInfo.get("class_id"),
                (Integer) elementInfo.get("element_parent_id"),
                elementInfo.get("element_guid").toString(),
                (Integer) elementInfo.get("element_level"),
                (Integer) elementInfo.get("element_level_sequence"),
                elementInfo.get("element_hierarchy_number").toString(),
                elementInfo.get("element_name").toString(),
                elementInfo.get("element_shortdesc").toString()
        );

        // add the md_context
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the md_context json array returned by stored procedure
        List<LinkedHashMap> contextList = null;
        try {
            contextList = objectMapper.readValue(elementInfo.get("md_context").toString(), List.class);
            for(LinkedHashMap contextNode: contextList) {
                DlMetaDataContextNode mdContextNode = new DlMetaDataContextNode(
                        Integer.parseInt(contextNode.get("class_id").toString()),
                        contextNode.get("class_name").toString(),
                        Integer.parseInt(contextNode.get("object_id").toString()),
                        contextNode.get("object_name").toString()
                );
                if(contextNode.containsKey("internal_path")) {
                    mdContextNode.setInternalPath(contextNode.get("internal_path").toString());
                }
                else {
                    mdContextNode.setInternalPath(contextNode.get("object_name").toString());
                }
                element.addMdContextNode(mdContextNode);
            }
        }
        catch (Exception e) {
            System.out.println("Exception thrown while parsing md_context JSON array.");
            System.out.println(e);
        }

        //resultData.put("newElement", newID.get("returnID"));
        results.put("result_data", element);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: invokes the stored procedure to move an element within a taxonomy
     */
    @RequestMapping(method = POST, value = "/taxonomy/element/move")
    public Map<String, Object> writeTaxonomyElementMove(@RequestBody MoveElementDefinition moveElementDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_taxonomy_element_move");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_element_id", moveElementDefinition.getElementId(), Types.INTEGER)
                .addValue("_new_parent_element_id", moveElementDefinition.getNewParentElementId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", null);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Writes a new taxonomy element to the DL database.
     * URL Path: /taxonomy/element/
     */
    @RequestMapping(method = POST, value = "/taxonomy/element/customformdata")
    public Map<String, Object> writeTaxonomyElementCustomFormData(@RequestBody CustomFormData customFormData, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // TODO: lookup user id from DlAuthDB using the API Token in Authorization Header
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_taxonomy_element_custom_form_data");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_element_id", customFormData.getElementId(), Types.INTEGER)
                .addValue("_custom_form_data", customFormData.getCustomFormData(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Updates the custom form id for a taxonomy.
     * URL Path: /taxonomy/customform/
     */
    @RequestMapping(method = POST, value = "/taxonomy/customform/{taxonomyId}")
    public Map<String, Object> updateCustomForm(@RequestBody Integer customFormId, @RequestHeader("dlUserId") String userId, @PathVariable Integer taxonomyId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_taxonomy_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_taxonomy_id", taxonomyId, Types.INTEGER)
                .addValue("_custom_form_id", customFormId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Deletes a taxonomy element from the DL database.
     * URL Path: /taxonomy/element/
     */
    @RequestMapping(method = DELETE, value = "/taxonomy/element/{elementId}")
    public Map<String, Object> deleteTaxonomyElement(@PathVariable Integer elementId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // TODO: lookup user id from DlAuthDB using the API Token in Authorization Header
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_taxonomy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_element_id", elementId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Deletes a full taxonomy from the DL database.
     * URL Path: /taxonomy/
     */
    @RequestMapping(method = DELETE, value = "/taxonomy/{taxonomyId}")
    public Map<String, Object> deleteTaxonomy(@PathVariable Integer taxonomyId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_taxonomy");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_taxonomy_id", taxonomyId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: Retrieves the history of changes to the taxonomy element identified by {elementId}.
     * @param elementId
     * @return
     */
    @RequestMapping(method = GET, value = "/taxonomy/element/{elementId}/history")
    public Map<String, Object> readTaxonomyElementHistory(@PathVariable Integer elementId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList archivedElementVersions = new ArrayList();  // this list is returned in the results Map

        // 1: get the element change history
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_taxonomy_element_hist");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_element_id", elementId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("lastChangeUserId", (Integer) out.get("_last_change_user_id"));
        resultData.put("lastChangeUser", out.get("_last_change_user").toString());
        resultData.put("lastChangeTimestamp", out.get("_last_change_ts").toString());

        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;

            TaxonomyElementArchive elementArchive = new TaxonomyElementArchive(
                    (Long) elementInfo.get("element_hist_id"),
                    elementInfo.get("element_name").toString(),
                    elementInfo.get("element_shortdesc").toString(),
                    elementInfo.get("element_hierarchy_number").toString(),
                    elementInfo.get("last_change_ts").toString(),
                    (Integer) elementInfo.get("last_change_user_id"),
                    elementInfo.get("user_login").toString()
            );
            elementArchive.setChangeActionByFlag(elementInfo.get("action_flag").toString());
            archivedElementVersions.add(elementArchive);
        }

        resultData.put("archivedElementVersions", archivedElementVersions);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: this class is used to process a request to move a taxonomy element
     */
    static class MoveElementDefinition {
        private Integer elementId;
        private Integer newParentElementId;

        public MoveElementDefinition() {
            this.elementId = 0;
            this.newParentElementId = 0;
        }

        public MoveElementDefinition(Integer elementId, Integer newParentElementId) {
            this.elementId = elementId;
            this.newParentElementId = newParentElementId;
        }

        public Integer getElementId() {
            return elementId;
        }

        public void setElementId(Integer elementId) {
            this.elementId = elementId;
        }

        public Integer getNewParentElementId() {
            return newParentElementId;
        }

        public void setNewParentElementId(Integer newParentElementId) {
            this.newParentElementId = newParentElementId;
        }
    }

    /**
     * Utility class for receiving custom form data from UI
     */
    static class CustomFormData {
        private Integer elementId;
        private String customFormData;

        public CustomFormData() {
            this.elementId = 0;
            this.customFormData = "";
        }

        public CustomFormData(Integer elementId, String customFormData) {
            this.elementId = elementId;
            this.customFormData = customFormData;
        }

        public Integer getElementId() {
            return elementId;
        }

        public void setElementId(Integer elementId) {
            this.elementId = elementId;
        }

        public String getCustomFormData() {
            return customFormData;
        }

        public void setCustomFormData(String customFormData) {
            this.customFormData = customFormData;
        }
    }
}
