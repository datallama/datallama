/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.apidefinition.ReverseEngineerApiUtil;
import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.ReverseEngineerRunCmn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class ReverseEngineerObjectStoreController {
    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.file.upload}")
    private String uploadFilePath;

    @Value("${dl.security.crypt.key}")
    private String cryptKey;

    /**
     * Executes an online reverse engineer run for the file/object store identified by {objectId}
     * @param objectId
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/fos/reverse-engineer/objectstore/online/{objectId}")
    public Map<String, Object> revEngOnlineObjectStore(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();

        ReverseEngineerObjectStoreUtil revEngUtils = new ReverseEngineerObjectStoreUtil(dataSource, Integer.parseInt(userId));
        RevEngResultCmn revEngResult = revEngUtils.executeOnlineRevEng (objectId, cryptKey);

        results.put("result_status", revEngResult.getResultStatus());
        results.put("result_data", revEngResult);
        return results;
    }

    /**
     * Returns the history of reverse engineer runs for the Object Store identified by {objectId} and {classId}
     *
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/fos/reverse-engineer/history/{objectId}/{classId}")
    public Map<String, Object> getFosRevEngHistory(@PathVariable Integer objectId, @PathVariable Integer classId, @RequestHeader("dlUserId") String userId) {
        Integer resultStatus = 1;
        Map<String, Object> results = new HashMap<>();

        ReverseEngineerObjectStoreUtil revEngUtils = new ReverseEngineerObjectStoreUtil(dataSource, Integer.parseInt(userId)); // TODO: replace with ReverseEngineerCommonUtil
        ArrayList<ReverseEngineerRunCmn> revEngRunHistory = revEngUtils.getRevEngRunHistory(objectId, classId);

        results.put("result_status", resultStatus);
        results.put("result_data", revEngRunHistory);
        return results;
    }

}
