/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.MoCustomFormAssoc;
import io.datallama.api.metamodel.DlMetaDataContextNode;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@CrossOrigin
@RestController
public class
StructuralMetadataController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.security.crypt.key}")
    private String cryptKey;

    /**
     * Reads all Structural Metadata database definitions from the DL database
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/list")
    public Map<String, Object> readSmdDatabaseList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_databases");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ArrayList smdDatabaseList = new ArrayList();

        // generate an SmdDatabaseDefinition object for each row in result set
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdDatabaseDefinition smdDb = new SmdDatabaseDefinition();
            smdDb.setDatabaseId((Integer) elementInfo.get("smd_database_id"));
            smdDb.setClassId((Integer) elementInfo.get("class_id"));
            smdDb.setJdbcDriverFamily(elementInfo.get("jdbc_driver_family").toString());
            smdDb.setLogicalDatabaseName(elementInfo.get("logical_database_name").toString());
            smdDb.setDatabaseName(elementInfo.get("smd_database_name").toString());
            smdDb.setDatabaseShortDesc(elementInfo.get("smd_database_shortdesc").toString());

            smdDatabaseList.add(smdDb);
        }

        results.put("result_data", smdDatabaseList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns details of an SMD database, including the table list and reverse engineering runs
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/detail/{smdDatabaseId}")
    public Map<String, Object> readSmdDatabaseDetail(@PathVariable Integer smdDatabaseId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata smdDb = new StructuralMetadata(dataSource);
        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);

        // 1. retrieve core details for SMD database
        SmdDatabaseDefinition dbDefinition = smdDb.getDatabaseDetail(smdDatabaseId);
        resultData.put("coreDetails", dbDefinition);

        // 2. retrieve the list of schema for the SMD database
        ArrayList<SmdSchemaDefinition> smdDbSchemaList = smdDb.getSmdDbSchemaList(smdDatabaseId);
        resultData.put("schemaList", smdDbSchemaList);

        // 3. retrieve the list of tables/views for the SMD database
        ArrayList<SmdTableViewDefinition> smdDbTableViewList = smdDb.getSmdDbTableViewList(smdDatabaseId);
        resultData.put("tableViewList", smdDbTableViewList);

        // 4. retrieve the list of relationships for the SMD database
        ArrayList<SmdRelationshipDefinition> smdDbRelationshipList = smdDb.getSmdDbRelationshipList(smdDatabaseId);
        resultData.put("relationshipList", smdDbRelationshipList);

        // 5. retrieve the reverse engineer run log
        ArrayList<ReverseEngineerRun> revEngRunLog = revEngUtils.getRevEngRunLog(smdDatabaseId);
        resultData.put("revEngRunLog", revEngRunLog);

        // 6. retrieve the metadata objects associated with the column
        ArrayList<AssociatedObject> associations = smdDb.getAssociatedObjects(dbDefinition.getDatabaseId(), dbDefinition.getClassId());
        resultData.put("associations", associations);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Returns summary information about the SMD database identified by {objectGUID}
     * @param objectGUID
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/guid/{objectGUID}")
    public Map<String, Object> readSmdDatabaseIdsByGUID(@PathVariable String objectGUID) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata smdDb = new StructuralMetadata(dataSource);
        SmdDatabaseDefinition dbDefnSummary = smdDb.getDatabaseSummaryByGUID(objectGUID);

        resultData.put("database_summary", dbDefnSummary);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes the core details of new SMD database definition to the DL database
     * @param databaseDefinition
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/configure")
    public Map<String, Object> writeSmdDatabaseConfiguration(@RequestBody SmdDatabaseDefinition databaseDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_configure_database");

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", databaseDefinition.getDatabaseId(), Types.INTEGER)
                .addValue("_class_id", databaseDefinition.getClassId(), Types.INTEGER)
                .addValue("_logical_database_name", databaseDefinition.getLogicalDatabaseName(), Types.VARCHAR)
                .addValue("_smd_database_name", databaseDefinition.getDatabaseName(), Types.VARCHAR)
                .addValue("_smd_database_shortdesc", databaseDefinition.getDatabaseShortDesc(), Types.VARCHAR)
                .addValue("_jdbc_driver_family", databaseDefinition.getJdbcDriverFamily(), Types.VARCHAR)
                .addValue("_schema_name_list", databaseDefinition.getSchemaNameList(), Types.VARCHAR)
                .addValue("_smd_hostname", databaseDefinition.getHostname(), Types.VARCHAR)
                .addValue("_smd_port", databaseDefinition.getPort(), Types.VARCHAR)
                .addValue("_smd_username", databaseDefinition.getUsername(), Types.VARCHAR)
                .addValue("_smd_password", databaseDefinition.getPassword(), Types.VARCHAR)
                .addValue("_key", cryptKey, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);
        Integer returnId = (Integer) out.get("_return_id");
        resultStatus = (returnId < 0) ? returnId : 1;
        resultData.put("returnId", returnId);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Updates
     * @param databaseDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/details")
    public Map<String, Object> updateSmdDatabaseDetails(@RequestBody SmdDatabaseDefinition databaseDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("update_smd_database_details");

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_smd_database_id", databaseDefinition.getDatabaseId(), Types.INTEGER)
                .addValue("_logical_database_name", databaseDefinition.getLogicalDatabaseName(), Types.VARCHAR)
                .addValue("_smd_database_name", databaseDefinition.getDatabaseName(), Types.VARCHAR)
                .addValue("_smd_database_shortdesc", databaseDefinition.getDatabaseShortDesc(), Types.VARCHAR);
        try{
            jdbcCall.execute(in);
        } catch(org.springframework.dao.DuplicateKeyException e) {
            resultStatus = -1;
            results.put("result_data", resultData);
            results.put("result_status", resultStatus);
            return results;
        }

        resultStatus = 1;
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * Writes a new taxonomy element to the DL database.
     * URL Path: /smd/database/customformdata
     * @param customFormData
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/customformdata")
    public Map<String, Object> writeDatabaseCustomFormData(@RequestBody DlCustomForm customFormData, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_custom_form_data");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_class_id", customFormData.getClassId(), Types.INTEGER)
                .addValue("_object_id", customFormData.getObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", customFormData.getParentClassId(), Types.INTEGER)
                .addValue("_parent_object_id", customFormData.getParentObjectId(), Types.INTEGER)
                .addValue("_custom_form_id", customFormData.getCustomFormId(), Types.INTEGER)
                .addValue("_custom_form_data", customFormData.getFormData(), Types.VARCHAR)
                .addValue("_last_change_user_id", Integer.parseInt(userId), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the full list of SMD column specific singular data sensitivity assignments for the SMD database definition
     * identified by objectId
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/datasensitivity/{objectId}")
    public Map<String, Object> readSmdDbDataSensitivityReport(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        ArrayList<DlDataSensitivityAssignment> smdDbDsReport = structuralMetadata.getSmdDbDataSensitivityReport(objectId);
        resultData.put("ds_report", smdDbDsReport);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Updates connection details for the SMD Database definition identified in the object databaseDefinition.
     * @param databaseDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/connection")
    public Map<String, Object> updateSmdDatabaseConnection(@RequestBody SmdDatabaseDefinition databaseDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("update_smd_database_connection");

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_smd_database_id", databaseDefinition.getDatabaseId(), Types.INTEGER)
                .addValue("_jdbc_driver_family", databaseDefinition.getJdbcDriverFamily(), Types.VARCHAR)
                .addValue("_smd_hostname", databaseDefinition.getHostname(), Types.VARCHAR)
                .addValue("_smd_port", databaseDefinition.getPort(), Types.VARCHAR)
                .addValue("_smd_username", databaseDefinition.getUsername(), Types.VARCHAR)
                .addValue("_schema_name_list", databaseDefinition.getSchemaNameList(), Types.VARCHAR);
        jdbcCall.execute(in);
        resultStatus = 1;

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Creates a new DB schema definition, or updates and existing DB schema definition in the DL DB.
     * The JSON payload must be of the form {schemaId:0, classId:19, databaseId:0, schemaName:"s", schemaShortDesc:"s"}
     * The value of schemaId is 0 for a Create action or > 0 for an Update action.
     * @param schemaDefn
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/schema")
    public Map<String, Object> writeSmdSchemaDetail(@RequestBody SmdSchemaDefinition schemaDefn, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdCrudResult smdCrudResult = structuralMetadata.writeSmdSchema(Integer.parseInt(userId), schemaDefn);

        results.put("result_data", smdCrudResult.getResultData());
        results.put("result_status", smdCrudResult.getResultStatus());
        results.put("sql_error", smdCrudResult.getSqlError());

        return results;
    }

    /**
     * Updates the sort description for the Schema.
     * TODO: merge into the generic POST /smd/database/schema routine
     * @param schemaDefn
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/schema/summary")
    public Map<String, Object> writeSmdSchemaSummary(@RequestBody SmdSchemaDefinition schemaDefn, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdCrudResult smdCrudResult = structuralMetadata.writeSmdSchemaSummary(Integer.parseInt(userId), schemaDefn);

        results.put("result_data", smdCrudResult.getResultData());
        results.put("result_status", smdCrudResult.getResultStatus());
        results.put("sql_error", smdCrudResult.getSqlError());

        return results;
    }

    /**
     * Reads the DB schema definition identified by {objectId} from the DL DB.
     * The JSON payload must be of the form {schemaId:0, classId:19, databaseId:0, schemaName:"s", schemaShortDesc:"s"}
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/schema/{objectId}")
    public Map<String, Object> readSmdSchemaDetail(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdCrudResult smdCrudResult = structuralMetadata.getSmdSchemaDetails(objectId);

        resultData = smdCrudResult.getResultData();
        results.put("result_data", resultData.get("result_data"));
        results.put("result_status", smdCrudResult.getResultStatus());
        results.put("sql_error", smdCrudResult.getSqlError());

        return results;
    }

    /**
     * Reads the full list of SMD column specific singular data sensitivity assignments for the SMD schema definition
     * identified by objectId
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/schema/datasensitivity/{objectId}")
    public Map<String, Object> readSmdSchemaDataSensitivityReport(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        ArrayList<DlDataSensitivityAssignment> smdSchemaDsReport = structuralMetadata.getSmdSchemaDataSensitivityReport(objectId);
        resultData.put("ds_report", smdSchemaDsReport);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Reads the list of schemata for the SMD database definition identified by {objectId}
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/schema/list/{objectId}")
    public Map<String, Object> readSmdDatabaseSchemaList(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata smdUtil = new StructuralMetadata(dataSource);
        ArrayList<SmdSchemaDefinition> smdDbSchemaList = smdUtil.getSmdDbSchemaList(objectId);
        results.put("result_data", smdDbSchemaList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the list of schemata for the SMD database definition identified by {objectId}
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/schema/tableview/list/{objectId}")
    public Map<String, Object> readSmdSchemaTableViewList(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata smdUtil = new StructuralMetadata(dataSource);
        SmdCrudResult smdCrudResult = smdUtil.getSmdSchemaTableViewList(objectId);
        // TODO: check for errors in smdCrudResult
        resultData = smdCrudResult.getResultData();
        results.put("result_data", resultData.get("result_data"));
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns details of the structural metadata table/view identified by {smdTableViewId}. The return result set is
     * a list of SmdColumnDefinition objects
     * @param smdTableViewId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/tableview/{smdTableViewId}")
    public Map<String, Object> readSmdTableViewDetail(@PathVariable Integer smdTableViewId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdTableViewDefinition tableViewDefinition = structuralMetadata.getTableViewDetail(smdTableViewId);
        resultData.put("core_details", tableViewDefinition);

        // 2. retrieve the metadata objects associated with the column
        ArrayList<AssociatedObject> associations = structuralMetadata.getAssociatedObjects(tableViewDefinition.getTableViewId(), tableViewDefinition.getClassId());
        resultData.put("associations", associations);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Read summary information about the structural metadata table/view identified by {smdTableViewId}.
     * The summary does not include the column list, the association list, comments or custom form data.
     * @param smdTableViewId
     * @return Map<String, Object>
     */
    @RequestMapping(method = GET, value = "/smd/database/tableview/summary/{smdTableViewId}")
    public Map<String, Object> readSmdTableViewSummary(@PathVariable Integer smdTableViewId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdTableViewDefinition tableViewDefinition = structuralMetadata.getTableViewSummary(smdTableViewId);

        results.put("result_data", tableViewDefinition);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves changes to a Structural Metadata database definition to the DL database
     * @param tableViewSummary
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/tableview")
    public Map<String, Object> writeSmdTableView(@RequestBody SmdTableViewDefinition tableViewSummary, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdCrudResult smdCrudResult = structuralMetadata.writeSmdTableView(Integer.parseInt(userId), tableViewSummary);

        results.put("result_data", smdCrudResult.getResultData());
        results.put("result_status", smdCrudResult.getResultStatus());
        results.put("sql_error", smdCrudResult.getSqlError());

        return results;
    }

    /**
     * Saves changes to the summary information of the SMD table/view object described by tableViewSummary
     * @param tableViewSummary
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/tableview/summary")
    public Map<String, Object> writeSmdTableViewSummary(@RequestBody SmdTableViewDefinition tableViewSummary, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        structuralMetadata.setTableViewSummary(Integer.parseInt(userId), tableViewSummary.getTableViewId(),
                tableViewSummary.getClassId(), tableViewSummary.getTableViewShortDesc());

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves changes to the summary information of the SMD table/view object described by tableViewSummary
     * @param tableViewSummary
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/tableview/shortdesc")
    public Map<String, Object> writeSmdTableViewShortDesc(@RequestBody SmdTableViewDefinition tableViewSummary, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        structuralMetadata.setTableViewShortDesc(Integer.parseInt(userId), tableViewSummary.getTableViewId(),
                tableViewSummary.getClassId(), tableViewSummary.getTableViewShortDesc());

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Associates an SMD Table/View with a custom form.
     * @param customFormAssoc
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/tableview/customform")
    public Map<String, Object> writeSmdTableViewCustomForm(@RequestBody MoCustomFormAssoc customFormAssoc, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        structuralMetadata.setTableViewCustomForm(Integer.parseInt(userId), customFormAssoc.getObjectId(),
                customFormAssoc.getClassId(), customFormAssoc.getCustomFormId());

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * List all tables/views in the database identified by {smdDatabaseId}
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/tableview/list/{smdDatabaseId}")
    public Map<String, Object> readSmdDbTableViewList(@PathVariable Integer smdDatabaseId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata smdDb = new StructuralMetadata(dataSource);
        ArrayList<SmdTableViewDefinition> smdDbEntityList = smdDb.getSmdDbTableViewList(smdDatabaseId);

        results.put("result_data", smdDbEntityList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads details of the Structural Metadata column identified by {smdColumnId}
     * @param smdColumnId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/column/{smdColumnId}")
    public Map<String, Object> readSmdColumnDetail(@PathVariable Integer smdColumnId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);

        // 1. retrieve the core details of the column
        SmdColumnDefinition columnDefinition = structuralMetadata.getColumnDetail(smdColumnId);
        resultData.put("core_details", columnDefinition);

        // 2. retrieve the metadata objects associated with the column
        ArrayList<AssociatedObject> associations = structuralMetadata.getAssociatedObjects(columnDefinition.getColumnId(), columnDefinition.getClassId());
        resultData.put("associations", associations);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves the short description for a Structural Metadata column to the DL database.
     * @param columnDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/column")
    public Map<String, Object> writeSmdColumn(@RequestBody SmdColumnDefinition columnDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdCrudResult smdCrudResult = structuralMetadata.writeSmdColumn(Integer.parseInt(userId), columnDefinition);

        results.put("result_data", smdCrudResult.getResultData());
        results.put("result_status", smdCrudResult.getResultStatus());
        results.put("sql_error", smdCrudResult.getSqlError());

        return results;
    }

    /**
     * Saves the short description for a Structural Metadata column to the DL database.
     * @param columnDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/column/summary")
    public Map<String, Object> writeSmdColumnSummary(@RequestBody SmdColumnDefinition columnDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        structuralMetadata.setColumnSummary(Integer.parseInt(userId), columnDefinition.getColumnId(), columnDefinition.getColumnShortDesc());

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves the short description for a Structural Metadata column to the DL database.
     * @param columnDataSensitivity
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/column/datasensitivity")
    public Map<String, Object> writeSmdColumnDataSensitivity(@RequestBody ColumnDataSensitivity columnDataSensitivity, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column_data_sensitivity");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", columnDataSensitivity.getObjectId(), Types.INTEGER)
                .addValue("_data_sensitivity_id", columnDataSensitivity.getDlDsTagId(), Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Associates an SMD Table/View with a custom form.
     * @param customFormAssoc
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/column/customform")
    public Map<String, Object> writeSmdColumnCustomForm(@RequestBody MoCustomFormAssoc customFormAssoc, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        structuralMetadata.setColumnCustomForm(Integer.parseInt(userId), customFormAssoc.getObjectId(),
                customFormAssoc.getClassId(), customFormAssoc.getCustomFormId());

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Sets the password for the database credentials used to reverse engineer a database
     * @param dbCredentials
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/password")
    public Map<String, Object> writeSmdDatabasePassword(@RequestBody SmdDbCredentials dbCredentials, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_database_password");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_smd_database_id", dbCredentials.getDatabaseId(), Types.INTEGER)
                .addValue("_password", dbCredentials.getPassword(), Types.VARCHAR)
                .addValue("_key", "pAg7k9N2Cvov2Am8", Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * List all tables/views in the database identified by {smdDatabaseId}
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/relationship/list/{smdDatabaseId}")
    public Map<String, Object> readSmdDbRelationshipList(@PathVariable Integer smdDatabaseId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata smdDb = new StructuralMetadata(dataSource);
        ArrayList<SmdRelationshipDefinition> smdDbEntityList = smdDb.getSmdDbRelationshipList(smdDatabaseId);

        results.put("result_data", smdDbEntityList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Read details of the SMD relationship identified by {smdRelationshipId}
     * @param smdRelationshipId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/database/relationship/{smdRelationshipId}")
    public Map<String, Object> readSmdRelationshipDetail(@PathVariable Integer smdRelationshipId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        SmdRelationshipDefinition relationshipDefinition = structuralMetadata.getRelationshipDetail(smdRelationshipId);

        results.put("result_data", relationshipDefinition);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Write details of the SMD relationship identified in the POST payload.
     * @param smdRelationshipDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/relationship")
    public Map<String, Object> writeSmdRelationshipDetail(@RequestBody SmdRelationshipDefinition smdRelationshipDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // 1. Save core SMD relationship information to DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_database_id", smdRelationshipDefinition.getDatabaseId(), Types.INTEGER)
                .addValue("_schema_id", smdRelationshipDefinition.getSchemaId(), Types.INTEGER)
                .addValue("_class_id", smdRelationshipDefinition.getClassId(), Types.INTEGER)
                .addValue("_create_mode", smdRelationshipDefinition.getCreateMode(), Types.INTEGER)
                .addValue("_reveng_run_id", smdRelationshipDefinition.getRevengRunId(), Types.INTEGER)
                .addValue("_parent_table_view_id", smdRelationshipDefinition.getParentTableViewId(), Types.INTEGER)
                .addValue("_child_table_view_id", smdRelationshipDefinition.getChildTableViewId(), Types.INTEGER)
                .addValue("_relationship_name", smdRelationshipDefinition.getRelationshipName(), Types.VARCHAR)
                .addValue("_relationship_shortdesc", smdRelationshipDefinition.getRelationshipShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        int relationshipId = (int) out.get("_relationship_id");
        resultData.put("returnId", relationshipId);

        // 2. Save each column pair to the DL DB
        ArrayList<SmdRelationshipColumnPair> columnPairs = smdRelationshipDefinition.getColumnPairs();
        for (SmdRelationshipColumnPair columnPair : columnPairs) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_column_pair");
            in = new MapSqlParameterSource()
                    .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                    .addValue("_relationship_id", relationshipId, Types.INTEGER)
                    .addValue("_parent_column_id", columnPair.getParentColumnId(), Types.INTEGER)
                    .addValue("_child_column_id", columnPair.getChildColumnId(), Types.INTEGER)
                    .addValue("_reveng_run_id", smdRelationshipDefinition.getRevengRunId(), Types.INTEGER);
            // TODO: there should be exception handling for an insert failure
            out = jdbcCall.execute(in);
        }

        results.put("result_data",resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     *
     * @param smdRelationshipDefinition
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/database/relationship/summary")
    public Map<String, Object> writeSmdRelationshipSummary(@RequestBody SmdRelationshipDefinition smdRelationshipDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_relationship_id", smdRelationshipDefinition.getRelationshipId(), Types.INTEGER)
                .addValue("_relationship_shortdesc", smdRelationshipDefinition.getRelationshipShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnId", "");

        results.put("result_data",resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/smd/database/relationship/{relationshipId}")
    public Map<String, Object> deleteRelationship(@PathVariable Integer relationshipId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        StructuralMetadata structuralMetadata = new StructuralMetadata(dataSource);
        resultStatus = structuralMetadata.deleteRelationship(relationshipId);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes an SMD Database definition from the DL DB
     * @param databaseId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/smd/database/{databaseId}")
    public Map<String, Object> deleteDatabaseDefinition(@PathVariable Integer databaseId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        //Todo: Put all the deleted database stuff in a separate archive.
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_smd_database");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_smd_database_id", databaseId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * This class is used for updates to the data sensitivity tag assigned to a database column.
     */
    private static class ColumnDataSensitivity {
        private Integer objectId;
        private Integer dlDsTagId;

        public ColumnDataSensitivity() {
            this.objectId = 0;
            this.dlDsTagId = 0;
        }

        public Integer getObjectId() {
            return objectId;
        }

        public void setObjectId(Integer objectId) {
            this.objectId = objectId;
        }

        public Integer getDlDsTagId() {
            return dlDsTagId;
        }

        public void setDlDsTagId(Integer dlDsTagId) {
            this.dlDsTagId = dlDsTagId;
        }
    }

}