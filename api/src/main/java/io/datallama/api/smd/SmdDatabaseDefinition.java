/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class SmdDatabaseDefinition {
    private Integer databaseId = 0;
    private Integer serverId = 0;
    private Integer classId = 0;
    private String jdbcDriverFamily = "";
    private String hostname = "";
    private Integer port = 0;
    private String logicalDatabaseName = "";
    private String databaseName = "";
    private String objectGUID = "";
    private String databaseShortDesc = "";
    private String schemaNameList = "";     // a comma separated list of schema names that will be reverse engineered
    private String username = "";
    private String password = "";
    private Integer tableViewCustomFormId = 0;
    private String tableViewCustomFormName = "";
    private DlCustomForm customForm;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private ArrayList<SmdSchemaDefinition> schemaList;
    private ArrayList<DlDataSensitivityTag> dlDsTagList;

    /**
     * Default constructor.
     */
    public SmdDatabaseDefinition() {
        this.databaseId = 0;
        this.serverId = 0;
        this.classId = 0;
        this.jdbcDriverFamily = "";
        this.hostname = "";
        this.port = 0;
        this.logicalDatabaseName = "";
        this.databaseName = "";
        this.objectGUID = "";
        this.databaseShortDesc = "";
        this.schemaNameList = "";
        this.username = "";
        this.password = "";
        this.tableViewCustomFormId = 0;
        this.tableViewCustomFormName = "";
        this.customForm = new DlCustomForm();
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.schemaList = new ArrayList<SmdSchemaDefinition>();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
    }

    /**
     * Parameterised constructor.
     * @param databaseId
     * @param serverId
     * @param classId
     * @param jdbcDriverFamily
     * @param hostname
     * @param port
     * @param logicalDatabaseName
     * @param databaseName
     * @param databaseShortDesc
     * @param username
     * @param password
     */
    public SmdDatabaseDefinition(Integer databaseId, Integer serverId, Integer classId, String jdbcDriverFamily, String hostname, Integer port,
                                 String logicalDatabaseName, String databaseName, String databaseShortDesc, String username, String password) {
        this.databaseId = databaseId;
        this.serverId = serverId;
        this.classId = classId;
        this.jdbcDriverFamily = jdbcDriverFamily;
        this.hostname = hostname;
        this.port = port;
        this.logicalDatabaseName = logicalDatabaseName;
        this.databaseName = databaseName;
        this.objectGUID = "";
        this.databaseShortDesc = databaseShortDesc;
        this.schemaNameList = "";
        this.username = username;
        this.password = password;
        this.tableViewCustomFormId = 0;
        this.tableViewCustomFormName = "";
        this.customForm = new DlCustomForm();
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.schemaList = new ArrayList<SmdSchemaDefinition>();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
    }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Integer databaseId) {
        this.databaseId = databaseId;
    }

    public String getLogicalDatabaseName() {
        return logicalDatabaseName;
    }

    public void setLogicalDatabaseName(String logicalDatabaseName) {
        this.logicalDatabaseName = logicalDatabaseName;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getDatabaseShortDesc() {
        return databaseShortDesc;
    }

    public void setDatabaseShortDesc(String databaseShortDesc) {
        this.databaseShortDesc = databaseShortDesc;
    }

    public String getSchemaNameList() {
        return schemaNameList;
    }

    public String[] getSchemaNameListAsArray() {
        String[] schemaNameList = this.schemaNameList.split(",");
        return schemaNameList;
    }

    public void setSchemaNameList(String schemaNameList) {
        this.schemaNameList = schemaNameList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getJdbcDriverFamily() {
        return jdbcDriverFamily;
    }

    public void setJdbcDriverFamily(String jdbcDriverFamily) {
        this.jdbcDriverFamily = jdbcDriverFamily;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public String toString() {
        String result = "";

        result = String.format("databaseId: %d, serverId: %d, classId: %d, jdbcDriverFamily: %s, hostname: %s, port: %d, databaseName: %s, databaseShortDesc: %s, databaseSchema: %s, username: %s",
                this.databaseId, this.serverId, this.classId, this.jdbcDriverFamily, this.hostname, this.port, this.databaseName, this.databaseShortDesc, this.schemaNameList, this.username);
        return result;
    }

    public Integer getTableViewCustomFormId() {
        return tableViewCustomFormId;
    }

    public void setTableViewCustomFormId(Integer tableViewCustomFormId) {
        this.tableViewCustomFormId = tableViewCustomFormId;
    }

    public String getTableViewCustomFormName() {
        return tableViewCustomFormName;
    }

    public void setTableViewCustomFormName(String tableViewCustomFormName) {
        this.tableViewCustomFormName = tableViewCustomFormName;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }

    public ArrayList<SmdSchemaDefinition> getSchemaList() {
        return schemaList;
    }

    public void setSchemaList(ArrayList<SmdSchemaDefinition> schemaList) {
        this.schemaList = schemaList;
    }

    public void addSchema(SmdSchemaDefinition schemaDefinition) {
        this.schemaList.add(schemaDefinition);
    }

    public ArrayList<DlDataSensitivityTag> getDlDsTagList() {
        return dlDsTagList;
    }

    public void setDlDsTagList(ArrayList<DlDataSensitivityTag> dlDsTagList) {
        this.dlDsTagList = dlDsTagList;
    }

    public void addDlDsTag(DlDataSensitivityTag dlDsTag) {
        this.dlDsTagList.add(dlDsTag);
    }

}
