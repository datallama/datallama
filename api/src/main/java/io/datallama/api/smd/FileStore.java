/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class FileStore {

    private Integer objectId;
    private Integer classId;
    private Integer abbreviationSchemeId;
    private String objectName;
    private String className;
    private String objectGUID;
    private String objectConfig;
    private String objectShortDesc;
    private Integer customFormId;
    private String customFormName;
    private String schemaDefinition;
    private String formDefinition;
    private Integer createSource;
    private Integer credentialId;
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Default constructor. No parameters.
     */
    public FileStore() {
        this.objectId = 0;
        this.classId = 0;
        this.abbreviationSchemeId = 0;
        this.objectName = "";
        this.className = "";
        this.objectGUID = "";
        this.objectConfig = "{}";
        this.objectShortDesc = "";
        this.customFormId = 0;
        this.customFormName = "";
        this.schemaDefinition = "";
        this.formDefinition = "";
        this.createSource = 0;
        this.credentialId = 0;
    }

    /**
     * Constructor 02. Essential parameters
     * @param objectId
     * @param classId
     * @param abbreviationSchemeId
     * @param objectName
     * @param className
     * @param objectShortDesc
     */
    public FileStore(Integer objectId, Integer classId, Integer abbreviationSchemeId, String objectName, String className, String objectShortDesc) {
        this.objectId = objectId;
        this.classId = classId;
        this.abbreviationSchemeId = abbreviationSchemeId;
        this.objectName = objectName;
        this.className = className;
        this.objectGUID = "";
        this.objectConfig = "{}";
        this.objectShortDesc = objectShortDesc;
        this.customFormId = 0;
        this.customFormName = "";
        this.schemaDefinition = "";
        this.formDefinition = "";
        this.createSource = 0;
        this.credentialId = 0;
    }


    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getAbbreviationSchemeId() {
        return abbreviationSchemeId;
    }

    public void setAbbreviationSchemeId(Integer abbreviationSchemeId) {
        this.abbreviationSchemeId = abbreviationSchemeId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public String getCustomFormName() {
        return customFormName;
    }

    public void setCustomFormName(String customFormName) {
        this.customFormName = customFormName;
    }

    public String getSchemaDefinition() {
        return schemaDefinition;
    }

    public void setSchemaDefinition(String schemaDefinition) {
        this.schemaDefinition = schemaDefinition;
    }

    public String getFormDefinition() {
        return formDefinition;
    }

    public void setFormDefinition(String formDefinition) {
        this.formDefinition = formDefinition;
    }

    public String getObjectConfig() {
        return objectConfig;
    }

    public void setObjectConfig(String objectConfig) {
        this.objectConfig = objectConfig;
    }

    public Integer getCreateSource() {
        return createSource;
    }

    public void setCreateSource(Integer createSource) {
        this.createSource = createSource;
    }

    public Integer getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Integer credentialId) {
        this.credentialId = credentialId;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

}
