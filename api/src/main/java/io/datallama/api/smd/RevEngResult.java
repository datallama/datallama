/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

public class RevEngResult {
    private Integer resultStatus;
    private Integer sqlErrorCode;
    private String sqlState;
    private String sqlErrorMessage;
    private Integer errorNumber;
    private String generalErrorMessage;

    /**
     * Default constructor
     */
    public RevEngResult() {
        this.resultStatus = 1;
        this.sqlErrorCode = 0;
        this.sqlState = "";
        this.sqlErrorMessage = "";
        this.errorNumber = 0;
        this.generalErrorMessage = "";
    }

    public Integer getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Integer resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Integer getSqlErrorCode() {
        return sqlErrorCode;
    }

    public void setSqlErrorCode(Integer sqlErrorCode) {
        this.sqlErrorCode = sqlErrorCode;
    }

    public String getSqlState() {
        return sqlState;
    }

    public void setSqlState(String sqlState) {
        this.sqlState = sqlState;
    }

    public String getSqlErrorMessage() {
        return sqlErrorMessage;
    }

    public void setSqlErrorMessage(String sqlErrorMessage) {
        this.sqlErrorMessage = sqlErrorMessage;
    }

    public Integer getErrorNumber() {
        return errorNumber;
    }

    public void setErrorNumber(Integer errorNumber) {
        this.errorNumber = errorNumber;
    }

    public String getGeneralErrorMessage() {
        return generalErrorMessage;
    }

    public void setGeneralErrorMessage(String generalErrorMessage) {
        this.generalErrorMessage = generalErrorMessage;
    }
}
