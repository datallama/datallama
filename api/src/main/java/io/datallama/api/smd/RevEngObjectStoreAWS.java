package io.datallama.api.smd;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.common.RevEngResultCmn;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;

import software.amazon.awssdk.awscore.exception.AwsErrorDetails;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.util.*;

/**
 * Reverse engineers AWS S3 buckets and directories in the AWS regions specified in the Credential Type 'AWS Access Keys'
 */
public class RevEngObjectStoreAWS implements ReverseEngineerObjectStore {

    private ObjectStoreSpace objectStoreSpace;

    /**
     * Default constructor with no parameters
     */
    public RevEngObjectStoreAWS() {
        this.objectStoreSpace = new ObjectStoreSpace();
    }

    /**
     * Tests that the credentials specified in SmdCredential credential are valid and a connection can be made successfully
     * @param credential
     * @return An { @link SmdCredential} object.
     * @throws Exception
     */
    public SmdCredential testConnection (SmdCredential credential) throws Exception {
        credential.setConnectionTestResult(true);

        // 1. parse the credential detail string to JSON object
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the credential_detail json string to Java LinkedHashMap
        LinkedHashMap credentialDetail = null;
        try {
            credentialDetail = objectMapper.readValue(credential.getCredentialDetail(), LinkedHashMap.class);
        }
        catch (Exception e) {
            credential.setConnectionTestResult(false);
            System.out.println("Exception thrown while parsing credential detail JSON string.");
            System.out.println(e);
        }

        // 2. attempt to connect using the AWS Java 2 SDK S3Client. Any AWS Region can be used
        AwsBasicCredentials credentialsProvider = AwsBasicCredentials.create(credentialDetail.get("access_key_id").toString(), credentialDetail.get("secret_access_key").toString());
        S3Client s3bl = S3Client.builder()
                .region(Region.US_EAST_1)
                .credentialsProvider(StaticCredentialsProvider.create(credentialsProvider))
                .build();

        ListBucketsRequest listBucketsRequest = ListBucketsRequest.builder().build();

        ListBucketsResponse listBucketsResponse = null;
        try {
            listBucketsResponse = s3bl.listBuckets(listBucketsRequest);
        }
        catch (S3Exception s3e) {
            AwsErrorDetails err = s3e.awsErrorDetails();
            credential.setConnectionTestResult(false);
            credential.setConnectionError(String.format("ERROR trying to connect with AWS S3 Client.\nError message: %s.\nError code: %s", s3e.getMessage(), err.errorCode()));
        }
        s3bl.close();

        return credential;
    };

    /**
     * Returns the catalog of reverse engineered AWS S3 buckets
     * @return An {@link ObjectStoreSpace} object.
     * @throws Exception
     */
    public ObjectStoreSpace getReverseEngineerCatalog () throws Exception {
        return this.objectStoreSpace;
    };

    /**
     * Connects to an AWS S3 account and reverse engineers all Buckers or the specified list of buckets
     * @param credential
     * @return A {@link RevEngResultCmn} object that indicates the success/failure status of the reverse engineering run.
     * @throws Exception
     */
    public RevEngResultCmn reverseEngineerObjectStore (SmdCredential credential) throws Exception {
        RevEngResultCmn revEngResult = new RevEngResultCmn();
        List<Region> regionList = Region.regions();
        ArrayList<String> targetRegionList = new ArrayList<String>();
        Map<String, BucketSummary> bucketTracker = new HashMap<>(); // tracks which buckets have been reverse engineered


        // 1. parse the credential detail string to JSON object
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the credential_detail json string to Java LinkedHashMap
        LinkedHashMap credentialDetail = null;
        try {
            credentialDetail = objectMapper.readValue(credential.getCredentialDetail(), LinkedHashMap.class);
            targetRegionList = (ArrayList) credentialDetail.get("region_list");
        }
        catch (Exception e) {
            credential.setConnectionTestResult(false);
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(-203101);
            revEngResult.setGeneralErrorMessage("Exception thrown while parsing credential detail JSON string.");
        }

        // 2. connect to AWS S3 and traverse buckets and directories
        if (revEngResult.getResultStatus() == 1) {
            AwsBasicCredentials credentialsProvider = AwsBasicCredentials.create(credentialDetail.get("access_key_id").toString(), credentialDetail.get("secret_access_key").toString());

            // 2.1. get the full list of buckets across all regions. Use any region for S3Client.
            S3Client s3bl = S3Client.builder()
                    .region(Region.US_EAST_1)
                    .credentialsProvider(StaticCredentialsProvider.create(credentialsProvider))
                    .build();

            ListBucketsRequest listBucketsRequest = ListBucketsRequest.builder().build();

            ListBucketsResponse listBucketsResponse = null;
            List<Bucket> bucketList = null;
            try {
                listBucketsResponse = s3bl.listBuckets(listBucketsRequest);
                bucketList = listBucketsResponse.buckets();
                for (Bucket currentBucket : bucketList) {
                    String currentBucketName = currentBucket.name();
                    BucketSummary currentBucketSummary = new BucketSummary(currentBucketName);
                    bucketTracker.put(currentBucketName, currentBucketSummary);
                }
            }
            catch (S3Exception s3e) {
                AwsErrorDetails err = s3e.awsErrorDetails();
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-203102);
                revEngResult.setGeneralErrorMessage(String.format("ERROR trying to connect with AWS S3 Client.\nError message: %s.\nError code: %s", s3e.getMessage(), err.errorCode()));
            }
            s3bl.close();

            // 2.2. reverse engineer buckets in the full bucket list for each region specified in the credential.
            if (revEngResult.getResultStatus() == 1) {

                for (int i = 0; i < targetRegionList.size(); i++) {

                    String regionName = targetRegionList.get(i);
                    Region targetRegion = null;
                    for (Region thisRegion : regionList) {
                        if (thisRegion.toString().equals(regionName)) {
                            targetRegion = thisRegion;
                            break;
                        }
                    }
                    if (targetRegion != null) {
                        S3Client s3 = S3Client.builder()
                                .region(targetRegion)
                                .credentialsProvider(StaticCredentialsProvider.create(credentialsProvider))
                                .build();

                        for (Bucket currentBucket : bucketList) {
                            String bucketName = currentBucket.name();
                            BucketSummary checkBucket = bucketTracker.get(bucketName);

                            if (checkBucket.getRevEngState() == 0) {
                                try {
                                    ListObjectsRequest listObjects = ListObjectsRequest
                                            .builder()
                                            .bucket(bucketName)
                                            .build();

                                    ListObjectsResponse res = s3.listObjects(listObjects);
                                    List<S3Object> objects = res.contents();

                                    // S3 bucket found so create a new ObjectStoreContainer instance
                                    ObjectStoreContainer objectStoreContainer = new ObjectStoreContainer(bucketName);

                                    checkBucket.setAwsRegion(targetRegion.toString());
                                    checkBucket.setRevEngState(1);

                                    // 2.3. process all directories in the bucket
                                    Map<String, ObjectStoreDirectory> createdOsDirs = new HashMap<>();
                                    String dirname = "";
                                    String basename = "";

                                    for (S3Object myValue : objects) {
                                        String objectName = myValue.key();
                                        if (objectName.endsWith("/")) {

                                            // remove trailing forward slash and split full path into segments
                                            String fullPath = objectName.substring(0, objectName.lastIndexOf('/'));;

                                            if(fullPath.indexOf('/') < 0) {
                                                // this is a top level directory. Add directly to Container
                                                ObjectStoreDirectory newOsDirectory = new ObjectStoreDirectory(fullPath);
                                                objectStoreContainer.addDirectory(newOsDirectory);
                                                createdOsDirs.put(fullPath, newOsDirectory);
                                            }
                                            else {
                                                dirname = fullPath.substring(0, fullPath.lastIndexOf('/'));
                                                basename = fullPath.substring(fullPath.lastIndexOf('/') + 1);

                                                if (createdOsDirs.containsKey(dirname)) {
                                                    ObjectStoreDirectory createdOsDirectory = createdOsDirs.get(dirname);
                                                    ObjectStoreDirectory newOsDirectory = new ObjectStoreDirectory(basename);
                                                    createdOsDirectory.addDirectory(newOsDirectory);
                                                    createdOsDirs.put(fullPath, newOsDirectory);
                                                }
                                            }


                                        }
                                    }

                                    this.objectStoreSpace.addContainer(objectStoreContainer);

                                } catch (S3Exception e) {
                                    // System.err.println(String.format("Unable to process bucket %s. Error Msg: %s", bucketName, e.awsErrorDetails().errorMessage()));
                                    // TODO: while errors are expected due to the perculiarities of AWS S3 bucket endpoint addresses, a more exhaustive exception test should be implemented
                                }
                            }
                        }
                        s3.close();
                    }

                }
            }
        }

        return revEngResult;
    };

    /**
     * This class is used to track the status of "directory structure" reverse engineering for a bucket.
     */
    private static class BucketSummary {
        private String bucketName;
        private String awsRegion;
        private Integer revEngState;    // 0 = not reverse engineered. 1 = reverse engineering completed.

        public BucketSummary(String bucketName) {
            this.bucketName = bucketName;
            this.awsRegion = "";
            this.revEngState = 0;
        }

        public String getBucketName() {
            return bucketName;
        }

        public void setBucketName(String bucketName) {
            this.bucketName = bucketName;
        }

        public String getAwsRegion() {
            return awsRegion;
        }

        public void setAwsRegion(String awsRegion) {
            this.awsRegion = awsRegion;
        }

        public Integer getRevEngState() {
            return revEngState;
        }

        public void setRevEngState(Integer revEngState) {
            this.revEngState = revEngState;
        }
    }

}
