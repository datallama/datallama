/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

public class SmdDbCredentials {
    private Integer databaseId;
    private String hostname;
    private String dbName;
    private Integer dbPort;
    private String username;
    private String password;
    private String jdbcDriverFamily;
    private String sqlError;
    private Boolean connectionTestResult;

    public SmdDbCredentials() {
        this.databaseId = 0;
        this.hostname = "";
        this.dbName = "";
        this.dbPort = 0;
        this.username = "";
        this.password = "";
        this.jdbcDriverFamily = "";
        this.sqlError = "";
        this.connectionTestResult = true;
    }

    public SmdDbCredentials(Integer databaseId, String hostname, String dbName, Integer dbPort,
                            String username, String password, String jdbcDriverFamily) {
        this.databaseId = databaseId;
        this.hostname = hostname;
        this.dbName = dbName;
        this.dbPort = dbPort;
        this.username = username;
        this.password = password;
        this.jdbcDriverFamily = jdbcDriverFamily;
        this.sqlError = "";
        this.connectionTestResult = true;
    }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Integer databaseId) {
        this.databaseId = databaseId;
    }

    public String getHostname() {
        return hostname;
    }

    public String getDbName() {
        return dbName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Integer getDbPort() {
        return dbPort;
    }

    public void setDbPort(Integer dbPort) {
        this.dbPort = dbPort;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJdbcDriverFamily() {
        return jdbcDriverFamily;
    }

    public void setJdbcDriverFamily(String jdbcDriverFamily) {
        this.jdbcDriverFamily = jdbcDriverFamily;
    }

    public String getSqlError() {
        return sqlError;
    }

    public void setSqlError(String sqlError) {
        this.sqlError = sqlError;
    }

    public Boolean getConnectionTestResult() {
        return connectionTestResult;
    }

    public void setConnectionTestResult(Boolean connectionTestResult) {
        this.connectionTestResult = connectionTestResult;
    }

    public Integer getConnectionTestResultAsInteger() {
        return (this.connectionTestResult)?1:0;
    }
}
