/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import io.datallama.api.common.RevEngReconciliation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.web.multipart.MultipartFile;

import schemacrawler.schema.*;
import schemacrawler.schemacrawler.*;
import schemacrawler.tools.integration.serialize.JavaSerializedCatalog;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class ReverseEngineeringController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.file.upload}")
    private String uploadFilePath;

    @Value("${dl.security.crypt.key}")
    private String cryptKey;

    /**
     * Tests that a connection can be made to the database identified by smdDatabaseId using credentials defined for
     * that database in table dl_smd_database
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/reverse-engineer/testconnection/{smdDatabaseId}")
    public Map<String, Object> testSmdDatabaseConnection(@PathVariable Integer smdDatabaseId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // 1. get the credentials for the database
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_database_connection");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_smd_database_id", smdDatabaseId, Types.INTEGER)
                .addValue("_key", cryptKey, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        SmdDbCredentials dbCredentials = new SmdDbCredentials(
                smdDatabaseId,
                (String) out.get("_smd_hostname"),
                (String) out.get("_smd_database_name"),
                (Integer) out.get("_smd_port"),
                (String) out.get("_smd_username"),
                (String) out.get("_smd_password"),
                (String) out.get("_jdbc_driver_family")
        );

        ReverseEngineerUtils revengUtils = new ReverseEngineerUtils(dataSource);
        dbCredentials = revengUtils.testDatabaseConnection(dbCredentials);

        dbCredentials.setPassword("****");
        results.put("result_data", dbCredentials);
        results.put("result_status", dbCredentials.getConnectionTestResultAsInteger());

        return results;
    }

    /**
     * Creates a new reverse engineering run record in the DL DB or updates the status of an existing record.
     * @param userId
     * @param smdDatabaseId
     * @return
     */
    private Integer writeRevEngRun(Integer userId, Integer smdDatabaseId, Integer revengRunId, Integer revengRunStatus,
                                   String generalErrorMessage) {

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_reveng_run");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_smd_database_id", smdDatabaseId, Types.INTEGER)
                .addValue("_reveng_run_id", revengRunId, Types.INTEGER)
                .addValue("_reveng_run_status", revengRunStatus, Types.INTEGER)
                .addValue("_reveng_run_desc", generalErrorMessage, Types.VARCHAR);
        ;
        Map out = jdbcCall.execute(in);
        return (Integer) out.get("_return_id");
    }

    /**
     *
     * @param userId
     * @param smdDatabaseId
     * @param revengRunId
     */
    private void checkRevEngRunStatus(Integer userId, Integer smdDatabaseId, Integer revengRunId) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_reveng_run_status");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_reveng_run_id", revengRunId, Types.INTEGER);
        Map out = jdbcCall.execute(in);
        Integer runResult = (Integer) out.get("_run_result");

        writeRevEngRun(userId, smdDatabaseId, revengRunId, runResult, "Offline import completed for SchemaCrawler serialised catalog.");
    }

    /**
     * Processes a catalogue returned by Schema Crawler and populates SMD objects (schema, tables, views, columns,
     * relationships) in the DL Database.
     * @param catalog
     * @param userId
     * @param smdDatabaseId
     * @param runId
     * @return
     */
    private Map<String, Object> processSchemaCrawlerCatalog(Catalog catalog, String userId, int smdDatabaseId, int runId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;
        Integer descOverflowStatus = 2;

        Integer schemaId = 1;
        SimpleJdbcCall jdbcCall;
        SqlParameterSource in;
        Map out;
        HashMap<String, Integer> tableViewIds = new HashMap<String, Integer>();
        HashMap<Integer, HashMap<String, Integer>> columnIds = new HashMap<>();
        HashMap<String, Integer> schemaIds = new HashMap<String, Integer>();

        // 1. iterate over all SCHEMA in the catalog returned by schema crawler and drill down into tables/views and columns
        for (Schema schema : catalog.getSchemas()) {
            if(catalog.getTables(schema).isEmpty() || schema.getName() != null && (schema.getName().toLowerCase().equals("sys") || schema.getName().toLowerCase().equals("information_schema"))){
                continue;
            }

            String schemaComment = schema.getRemarks();
            if(schemaComment.length() > 1000){
                schemaComment = schemaComment.substring(0, 1000);
                resultData.put(schema.getName(),"Schema description was too long and has been truncated");
            }

            // 1.1. write schema definition to DL DB
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_schema_reveng");
            String schemaName = "Implicit Schema";
            if (schema.getName() != null) {
                schemaName = schema.getName();
            }
            in = new MapSqlParameterSource()
                    .addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_database_id", smdDatabaseId, Types.INTEGER)
                    .addValue("_class_id", 19, Types.INTEGER)       // TODO: ClassId must not be hard coded
                    .addValue("_reveng_run_id", runId, Types.INTEGER)
                    .addValue("_schema_name", schemaName, Types.VARCHAR)
                    .addValue("_schema_shortdesc", schemaComment, Types.VARCHAR);

            out = jdbcCall.execute(in);
            schemaId = (Integer) out.get("_schema_id");
            schemaIds.put(schema.getName(), schemaId);

            // 1.2. iterate over all table/view
            for (Table table : catalog.getTables(schema)) {
                if(table.getColumns().isEmpty()){
                    continue;
                }

                // set the appropriate class id
                // TODO: hard coded classes need to be replaced with a Closs Info Service
                int classId = 5;
                if (table instanceof View) {
                    classId = 6;
                }

                String tableComment = table.getRemarks();
                if (tableComment.length() > 1000) {
                    tableComment = tableComment.substring(0, 1000);
                    resultData.put(table.getName(),"Table description was too long and has been truncated");
                    resultStatus = descOverflowStatus;
                }
                jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view_reveng");
                in = new MapSqlParameterSource()
                        .addValue("_user_id", userId, Types.INTEGER)
                        .addValue("_database_id", smdDatabaseId, Types.INTEGER)
                        .addValue("_schema_id", schemaId, Types.INTEGER)
                        .addValue("_class_id", classId, Types.INTEGER)
                        .addValue("_reveng_run_id", runId, Types.INTEGER)
                        .addValue("_table_view_name", table.getName(), Types.VARCHAR)
                        .addValue("_table_view_shortdesc", tableComment, Types.VARCHAR);

                out = jdbcCall.execute(in);
                Integer tableViewId = (Integer) out.get("_table_view_id");
                tableViewIds.put(table.getFullName(),tableViewId);
                columnIds.put(tableViewId, new HashMap<>());

                // 1.2.1. iterate over all columns in the table/view
                for (Column column : table.getColumns()) {

                    String columnComment = column.getRemarks();
                    if (columnComment.length() > 1000) {
                        columnComment = columnComment.substring(0, 1000);
                        resultData.put(column.getName(),"Column description was too long and has been truncated");
                        resultStatus = descOverflowStatus;
                    }

                    // format the data type display based on datatype
                    String columnDataType = column.getColumnDataType().getName().toLowerCase();
                    String nullStatus = column.isNullable() ? "null" : "not null";
                    String pkMember = column.isPartOfPrimaryKey() ? "PK" : "";
                    if(columnDataType.equals("char") ||
                        columnDataType.equals("character") ||
                        columnDataType.equals("nchar") ||
                        columnDataType.equals("varchar") ||
                        columnDataType.equals("varchar2") ||
                        columnDataType.equals("nvarchar") ||
                        columnDataType.equals("nvarchar2") ||
                        columnDataType.equals("varbinary") ||
                        columnDataType.equals("varying")
                    ) {
                        columnDataType = String.format("%s(%d) %s  %s", columnDataType, column.getSize(),
                                nullStatus, pkMember);
                    }
                    else if(columnDataType.equals("numeric") ||
                        columnDataType.equals("number") ||
                        columnDataType.equals("decimal") ||
                        columnDataType.equals("double")
                    ) {
                        columnDataType = String.format("%s(%d,%d) %s  %s", columnDataType, column.getSize(),
                                column.getDecimalDigits(), nullStatus, pkMember);
                    }
                    else {
                        columnDataType = String.format("%s %s  %s", columnDataType, nullStatus, pkMember);
                    }

                    // write column info to DL DB
                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column_reveng");
                    in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)
                            .addValue("_table_view_id", tableViewId, Types.INTEGER)
                            .addValue("_class_id", 8, Types.INTEGER)
                            .addValue("_reveng_run_id", runId, Types.INTEGER)
                            .addValue("_column_ordinal", column.getOrdinalPosition(), Types.INTEGER)
                            .addValue("_primary_key", column.isPartOfPrimaryKey(), Types.BOOLEAN)
                            .addValue("_column_datatype", columnDataType, Types.VARCHAR)
                            .addValue("_column_name", column.getName(), Types.VARCHAR)
                            .addValue("_column_shortdesc", columnComment, Types.VARCHAR);

                    out = jdbcCall.execute(in);
                    Integer columnId = (Integer) out.get("_column_id");
                    HashMap<String, Integer> temp = columnIds.remove(tableViewId);
                    temp.put(column.getFullName(),columnId);
                    columnIds.put(tableViewId,temp);

                }
            }

        }

        // 2. Process the relationships defined in each schema. Iterate through all tables in the schema - for each table
        //      get the related tables and process the Foreign Keys for each related table.
        for(Schema schema : catalog.getSchemas()) {
            for (Table table : catalog.getTables(schema)) {
                for (Table relatedTable : table.getRelatedTables(TableRelationshipType.child)) {
                    if(schema.getName() != null) {
                        schemaId = schemaIds.get(schema.getName());
                    }

                    // get the SMD Table Id based on table name for both parent and child table.
                    Integer parentTableID = tableViewIds.get(table.getFullName());
                    Integer childTableID = tableViewIds.get(relatedTable.getFullName());

                    Collection<ForeignKey> foreignKeys = table.getForeignKeys();

                    // 2.1. process all foreign key relationships
                    for(ForeignKey foreignKey : foreignKeys) {
                        Integer smdRelationshipId = 0;

                        // 2.1.1. Process the relationship
                        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_reveng");
                        in = new MapSqlParameterSource()
                                .addValue("_user_id", userId, Types.INTEGER)
                                .addValue("_database_id", smdDatabaseId, Types.INTEGER)
                                .addValue("_schema_id", schemaId, Types.INTEGER)
                                .addValue("_class_id", 7, Types.VARCHAR)
                                .addValue("_reveng_run_id", runId, Types.INTEGER)
                                .addValue("_parent_table_view_id", parentTableID, Types.INTEGER)
                                .addValue("_child_table_view_id", childTableID, Types.INTEGER)
                                .addValue("_relationship_name", foreignKey.getSpecificName(), Types.VARCHAR)
                                .addValue("_relationship_shortdesc", "None Set", Types.VARCHAR);

                        out = jdbcCall.execute(in);

                        // save the relationship Id
                        smdRelationshipId = Integer.parseInt(out.get("_relationship_id").toString());

                        // 2.1.2. Process all column pairs for the relationship
                        List<ForeignKeyColumnReference> columnReferences = foreignKey.getColumnReferences();
                        for(ColumnReference columnReference : columnReferences) {
                            Integer parentColumnId = columnIds.get(parentTableID).get(columnReference.getPrimaryKeyColumn().getFullName());
                            Integer childColumnId = columnIds.get(childTableID).get(columnReference.getForeignKeyColumn().getFullName());

                            if( parentColumnId != null && childColumnId != null ) {
                                jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_column_pair");
                                in = new MapSqlParameterSource()
                                        .addValue("_user_id", userId, Types.INTEGER)
                                        .addValue("_relationship_id", smdRelationshipId, Types.INTEGER)
                                        .addValue("_parent_column_id", parentColumnId, Types.INTEGER)
                                        .addValue("_child_column_id", childColumnId, Types.INTEGER)
                                        .addValue("_reveng_run_id", runId, Types.INTEGER);
                                out = jdbcCall.execute(in);
                            }
                        }
                    }

                }
            }
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reverse engineers a database from a serialised java class file produced by the Schema Crawler CLI utility
     * @param smdDatabaseId
     * @param userId
     * @param file
     * @return
     * @throws SQLException
     * @throws SchemaCrawlerException
     */
    @RequestMapping(method = POST, value = "/reverse-engineer/offline/schemacrawler_ser/{smdDatabaseId}",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> dbRevEngOfflineSchemaCrawlerSer(@PathVariable Integer smdDatabaseId, @RequestHeader("dlUserId") String userId,
                                                               @RequestParam("file") MultipartFile file) throws SQLException, SchemaCrawlerException {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        Catalog catalog;

        // 1. save the uploaded serialised Java Class file to the local file system
        try {
            File convertFile = new File(uploadFilePath + file.getOriginalFilename());

            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();

            FileInputStream fin = new FileInputStream(convertFile);
            JavaSerializedCatalog serializedCatalog = new JavaSerializedCatalog(fin);
            catalog = serializedCatalog.getCatalog();
            fin.close();

        } catch(IOException e){
            resultStatus = -201204;
            results.put("result_data", e);
            results.put("result_status", resultStatus);

            return results;
        }

        // 2. create a new reverse engineering run record in the DL DB. The status of a new reverse engineering run is always zero
        Integer newRunId = -1;

        newRunId = writeRevEngRun(Integer.parseInt(userId), smdDatabaseId, newRunId, 0, "Offline reverse engineering running for SchemaCrawler serialised catalog.");

        if (newRunId == -4) {
            resultStatus = -201202;
            results.put("result_data", "The previous reverse engineering run has not been fully reconciled.");
            results.put("result_status", resultStatus);

            return results;
        }
        else if (newRunId < 0) {
            resultStatus = -201203;
            results.put("result_data", "Failed to create a new reveng_run_id in dl_reveng_run. Reason unknown.");
            results.put("result_status", resultStatus);

            return results;
        }

        // 3. process the SchemaCrawler catalog, i.e. write schema, table/view, column etc to DL DB
        results = processSchemaCrawlerCatalog(catalog, userId, smdDatabaseId, newRunId);

        // 4. update the reverse engineering run record with the results.
        Integer returnId = writeRevEngRun(Integer.parseInt(userId), smdDatabaseId, newRunId, (Integer) results.get("result_status"),
                "Reverse engineer run completed - Offline SchemaCrawler.");
        return results;
    }

    /**
     * Reverse engineers a database from a Data Llama Database SMD definition JSON file.
     * TODO: This must be moved to ReverseEngineerUtils
     * @param smdDatabaseId
     * @param userId
     * @param uploadFile
     * @return
     */
    @RequestMapping(method = POST, value = "/reverse-engineer/offline/dldbsmd/json/{smdDatabaseId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> dbRevEngOfflineDlDbSmdJson(@PathVariable Integer smdDatabaseId, @RequestHeader("dlUserId") String userId,
                                                          @RequestParam("file") MultipartFile uploadFile)
    {
        Map<String, Object> results = new HashMap<>();
        ObjectMapper objectMapper = new ObjectMapper();
        RevEngResult revEngResult = new RevEngResult();
        revEngResult.setResultStatus(1);

        // 1. persist the upload file to the local filesystem.
        File jsonFile = null;
        try {
            String jsonFilename = uploadFilePath + uploadFile.getOriginalFilename();
            jsonFile = new File(uploadFilePath + uploadFile.getOriginalFilename());

            FileOutputStream fout = new FileOutputStream(jsonFile);
            fout.write(uploadFile.getBytes());
            fout.close();

        } catch(IOException e) {
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(-200305);
            revEngResult.setGeneralErrorMessage("There was an error when uploading the DlDbSmd JSON file. Reverse engineering run aborted.");
            revEngResult.setSqlErrorMessage(e.getMessage());
        }

        if (jsonFile == null) {
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(-200306);
            revEngResult.setGeneralErrorMessage("There was an error when uploading the DlDbSmd JSON file. Reverse engineering run aborted.");
        }

        // 2. Parse the uploaded JSON file and check that it's a valid DlDbSmd JSON file
        JsonNode rootNode = null;
        if (revEngResult.getResultStatus() == 1) {
            try {
                rootNode = objectMapper.readTree(jsonFile);
            } catch (Exception e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200307);
                revEngResult.setGeneralErrorMessage("Error encountered when trying to parse the DlDbSmd JSON file. Check that the file is a valid JSON file.");
            }
        }

        if (revEngResult.getResultStatus() == 1) {
            String dlJsonTypeName = "";
            JsonNode dlJsonTypeNode = rootNode.get("dl_json_type");

            if (dlJsonTypeNode != null) {
                dlJsonTypeName = dlJsonTypeNode.asText().toUpperCase();
                if(!dlJsonTypeName.equals("DLDBSMD")) {
                    revEngResult.setResultStatus(-1);
                    revEngResult.setErrorNumber(-200308);
                    revEngResult.setGeneralErrorMessage("The uploaded file is not a valid DlDbSmd JSON file. The dl_json_type element is missing or incorrectly formed.");
                }
            }
            else {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200309);
                revEngResult.setGeneralErrorMessage("The uploaded file is not a valid DlDbSmd JSON file. The dl_json_type element is missing.");
            }

        }

        // 3. create a new reverse engineering run record in the DL DB. The status of a new reverse engineering run is always zero
        Integer newRunId = -1;
        if (revEngResult.getResultStatus() == 1) {
            newRunId = writeRevEngRun(Integer.parseInt(userId), smdDatabaseId, newRunId, 0,
                    "Offline reverse engineering running for DlDbSmd JSON file.");
            if (newRunId < -1) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200303);
                revEngResult.setGeneralErrorMessage("Failed to create a new reveng_run_id in dl_reveng_run.");
            }
        }

        // 4. process the nodes in the DL DB SMD JSON file
        if (revEngResult.getResultStatus() == 1) {
            SimpleJdbcCall jdbcCall = null;
            SqlParameterSource in = null;
            Map<String, Object> out = null;

            SmdDatabaseDefinition dbDefn = new SmdDatabaseDefinition();

            // 4.1. process schema nodes
            ArrayNode schemaListNode = (ArrayNode) rootNode.get("schema_list");

            if (schemaListNode != null) {
                Iterator<JsonNode> schemaNodes = schemaListNode.elements();

                // 4.1.1. process schema tables, views and columns
                while (schemaNodes.hasNext()) {
                    JsonNode currentSchemaNode = schemaNodes.next();
                    String schemaName = currentSchemaNode.get("schema_name").asText();

                    if (schemaName == null || schemaName.length() == 0) {
                        revEngResult.setResultStatus(-1);
                        revEngResult.setErrorNumber(-200304);
                        revEngResult.setGeneralErrorMessage("Invalid schema definition in DlDbSmd JSON file. Schema name missing or blank.");
                    }
                    else {
                        // 4.1.1.1. create a schema definition object
                        SmdSchemaDefinition schemaDefn = new SmdSchemaDefinition();
                        schemaDefn.setSchemaName(schemaName);
                        schemaDefn.setSchemaShortDesc(currentSchemaNode.get("shortdesc").asText());

                        // 4.1.1.2. process table/view nodes
                        ArrayNode tableViewListNode = (ArrayNode) currentSchemaNode.get("table_view_list");

                        if (tableViewListNode != null) {
                            Iterator<JsonNode> tableViewListNodes = tableViewListNode.elements();
                            while (tableViewListNodes.hasNext()) {

                                JsonNode currentTableViewNode = tableViewListNodes.next();
                                String tableViewName = currentTableViewNode.get("table_view_name").asText();
                                String tableViewType = currentTableViewNode.get("table_view_type").asText();

                                if (tableViewName == null || tableViewName.length() == 0) {
                                    revEngResult.setResultStatus(-1);
                                    revEngResult.setErrorNumber(-200311);
                                    String errMsg = String.format("Invalid table/view definition in DlDbSmd JSON file in schema. Table/view name is missing or blank in schema %s.", schemaDefn.getSchemaName());
                                    revEngResult.setGeneralErrorMessage(errMsg);
                                }

                                if (revEngResult.getResultStatus() == 1) {
                                    if (tableViewType == null || tableViewType.length() == 0) {
                                        revEngResult.setResultStatus(-1);
                                        revEngResult.setErrorNumber(-200312);
                                        String errMsg = String.format("Invalid table/view definition in DlDbSmd JSON file in schema. Table/view type is missing or blank in schema %s. Must be either 'table' or 'view'.", schemaDefn.getSchemaName());
                                        revEngResult.setGeneralErrorMessage(errMsg);
                                    }
                                }

                                // 4.1.1.3. create the table/view definition
                                if (revEngResult.getResultStatus() == 1) {
                                    SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition();
                                    tableViewDefn.setSchemaName(schemaDefn.getSchemaName());
                                    tableViewDefn.setTableViewName(tableViewName);

                                    if (currentTableViewNode.get("shortdesc") != null)
                                        tableViewDefn.setTableViewShortDesc(currentTableViewNode.get("shortdesc").asText());

                                    String tableViewClass = "DB_TABLE";
                                    if (tableViewType.toUpperCase().equals("VIEW")) {
                                        tableViewClass = "DB_VIEW";
                                    }
                                    tableViewDefn.setClassCode(tableViewClass);

                                    // 4.1.1.4. process column nodes
                                    ArrayNode columnListNode = (ArrayNode) currentTableViewNode.get("column_list");

                                    if (columnListNode != null) {
                                        Iterator<JsonNode> columnListNodes = columnListNode.elements();

                                        while (columnListNodes.hasNext()) {
                                            JsonNode currentColumnNode = columnListNodes.next();

                                            // prepare the datatype string
                                            String dataTypeLength = "";
                                            if (currentColumnNode.get("length").asInt() > 0) {
                                                dataTypeLength = String.format("(%d)", currentColumnNode.get("length").asInt());
                                            } else if (currentColumnNode.get("precision").asInt() > 0 || currentColumnNode.get("scale").asInt() > 0) {
                                                dataTypeLength = String.format("(%d, %d)", currentColumnNode.get("precision").asInt(), currentColumnNode.get("scale").asInt());
                                            }
                                            String columnDataType = String.format("%s%s", currentColumnNode.get("datatype").asText(), dataTypeLength);

                                            SmdColumnDefinition columnDefn = new SmdColumnDefinition();
                                            columnDefn.setColumnName(currentColumnNode.get("column_name").asText());
                                            columnDefn.setColumnOrdinal(currentColumnNode.get("ordinal").asInt());
                                            columnDefn.setColumnDatatype(columnDataType);
                                            columnDefn.setPrimaryKey(currentColumnNode.get("primary_key").asBoolean());
                                            columnDefn.setNullable(currentColumnNode.get("nullable").asBoolean());

                                            if(currentColumnNode.get("shortdesc") != null)
                                                columnDefn.setColumnShortDesc(currentColumnNode.get("shortdesc").asText());

                                            Integer validColDefn = columnDefn.validateMinimal();

                                            if (validColDefn == 1) {
                                                tableViewDefn.addColumnDefinition(columnDefn);
                                            }
                                            else {
                                                revEngResult.setResultStatus(-1);
                                                revEngResult.setErrorNumber(-200314);
                                                String errMsg = String.format("Invalid DlDbSmd JSON file. A column definition for table %s is incorrectly formed.", tableViewName);
                                                revEngResult.setGeneralErrorMessage(errMsg);
                                            }

                                            // exit column processing due to error
                                            if (revEngResult.getResultStatus() != 1)
                                                break;
                                        }

                                    }
                                    else {
                                        revEngResult.setResultStatus(-1);
                                        revEngResult.setErrorNumber(-200313);
                                        String errMsg = String.format("Invalid DlDbSmd JSON file. The column_list array is missing or incorrectly formed in table/view %s.", tableViewName);
                                        revEngResult.setGeneralErrorMessage(errMsg);
                                    }

                                    // add the table/view definition to the schema tableViewList
                                    if (revEngResult.getResultStatus() == 1) {
                                        schemaDefn.addTableView(tableViewDefn);
                                    }
                                }

                                // exit table/view processing due to error
                                if (revEngResult.getResultStatus() != 1)
                                    break;
                            }

                        }
                        else {
                            revEngResult.setResultStatus(-1);
                            revEngResult.setErrorNumber(-200310);
                            String errMsg = String.format("Invalid DlDbSmd JSON file. The table_view_list array is missing or incorrectly formed in schema %s.", schemaDefn.getSchemaName());
                            revEngResult.setGeneralErrorMessage(errMsg);
                        }

                        if (revEngResult.getResultStatus() == 1)
                            dbDefn.addSchema(schemaDefn);

                    } // end schema processing

                    // exit schema processing due to error
                    if (revEngResult.getResultStatus() != 1)
                        break;

                } // end schemaList processing

                // 4.2. process relationshipList
                if (revEngResult.getResultStatus() == 1) {
                    Iterator<JsonNode> schemaRelNodes = schemaListNode.elements();
                    while (schemaRelNodes.hasNext()) {
                        JsonNode currentSchemaNode = schemaRelNodes.next();
                        String schemaName = currentSchemaNode.get("schema_name").asText();

                        ArrayList<SmdRelationshipDefinition> relationshipList = new ArrayList<SmdRelationshipDefinition>();

                        // 4.2.1. process each relationship in the JSON array
                        ArrayNode relationshipListNode = (ArrayNode) currentSchemaNode.get("relationship_list");
                        if (relationshipListNode != null) {
                            Iterator<JsonNode> relationshipListNodes = relationshipListNode.elements();
                            while (relationshipListNodes.hasNext()) {
                                JsonNode currentRelationshipNode = relationshipListNodes.next();
                                String relationshipName = currentRelationshipNode.get("relationship_name").asText();
                                String parentTableName = currentRelationshipNode.get("parent_table_name").asText();
                                String parentTableSchemaName = currentRelationshipNode.get("parent_table_schema_name").asText();
                                String childTableName = currentRelationshipNode.get("parent_table_name").asText();
                                String childTableSchemaName = currentRelationshipNode.get("parent_table_schema_name").asText();

                                if (relationshipName == null || relationshipName.length() == 0) {
                                    revEngResult.setResultStatus(-1);
                                    revEngResult.setErrorNumber(-200315);
                                    String errMsg = String.format("Invalid relationship definition in DlDbSmd JSON file in schema. Relationship name is missing or blank in schema %s.", schemaName);
                                    revEngResult.setGeneralErrorMessage(errMsg);
                                }

                                if (revEngResult.getResultStatus() == 1) {
                                    if (parentTableName == null || parentTableName.length() == 0 || parentTableSchemaName == null || parentTableSchemaName.length() == 0) {
                                        revEngResult.setResultStatus(-1);
                                        revEngResult.setErrorNumber(-200316);
                                        String errMsg = String.format("Invalid relationship definition in DlDbSmd JSON file in schema. Parent table information is missing or blank in schema %s.", schemaName);
                                        revEngResult.setGeneralErrorMessage(errMsg);
                                    }
                                }

                                if (revEngResult.getResultStatus() == 1) {
                                    if (childTableName == null || childTableName.length() == 0 || childTableSchemaName == null || childTableSchemaName.length() == 0) {
                                        revEngResult.setResultStatus(-1);
                                        revEngResult.setErrorNumber(-200317);
                                        String errMsg = String.format("Invalid relationship definition in DlDbSmd JSON file in schema. Child table information is missing or blank in schema %s.", schemaName);
                                        revEngResult.setGeneralErrorMessage(errMsg);
                                    }
                                }

                                // 4.2.2. Create the SmdRelationshipDefinition object
                                if (revEngResult.getResultStatus() == 1) {
                                    SmdRelationshipDefinition smdRelDefn = new SmdRelationshipDefinition();
                                    smdRelDefn.setRelationshipName(relationshipName);
                                    smdRelDefn.setParentTableViewName(parentTableName);
                                    smdRelDefn.setParentTableViewSchemaName(parentTableSchemaName);
                                    smdRelDefn.setChildTableViewName(childTableName);
                                    smdRelDefn.setChildTableViewSchemaName(childTableSchemaName);

                                    // 4.2.2.1. Process the column pairs
                                    ArrayNode columnPairListNode = (ArrayNode) currentRelationshipNode.get("column_pair_list");
                                    if (columnPairListNode != null) {
                                        Iterator<JsonNode> columnPairListNodes = columnPairListNode.elements();
                                        while (columnPairListNodes.hasNext()) {
                                            JsonNode currentColumnPairListNode = columnPairListNodes.next();
                                            String parentColumnName = currentColumnPairListNode.get("parent_column_name").asText();
                                            String childColumnName = currentColumnPairListNode.get("child_column_name").asText();

                                            if (parentColumnName == null || parentColumnName.length() == 0 || childColumnName == null || childColumnName.length() == 0) {
                                                revEngResult.setResultStatus(-1);
                                                revEngResult.setErrorNumber(-200318);
                                                String errMsg = String.format("Invalid relationship column pair definition in DlDbSmd JSON file in schema. Column names are missing or blank in relationship %s.", relationshipName);
                                                revEngResult.setGeneralErrorMessage(errMsg);
                                            }

                                            if (revEngResult.getResultStatus() == 1) {
                                                SmdRelationshipColumnPair columnPair = new SmdRelationshipColumnPair();
                                                columnPair.setParentColumnName(parentColumnName);
                                                columnPair.setChildColumnName(childColumnName);
                                                smdRelDefn.addColumnPair(columnPair);
                                            }
                                        }
                                    }
                                    relationshipList.add(smdRelDefn);
                                } // end of SmdRelationshipDefinition creation
                            }
                        }

                        // 4.3. Add the relationshipList to the appropriate schema definition
                        ArrayList<SmdSchemaDefinition> schemaDefnList = dbDefn.getSchemaList();
                        for (SmdSchemaDefinition schemaDefn : schemaDefnList) {
                            if (schemaDefn.getSchemaName() == schemaName) {
                                schemaDefn.setRelationshipList(relationshipList);
                                break;
                            }
                        }
                    }
                } // end relationshipList processing

            }
            else {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200301);
                revEngResult.setGeneralErrorMessage("Invalid DlDbSmd JSON file. The schema_list array is missing or incorrectly formed.");
            }

            // 5. save the reverse engineered catalog to the DL DB.
            //    TODO: If an error occurs in step 5 then a rollback is required
            //    TODO: stored proc calls should be within try/catch blocks
            //    TODO: this functionality is shared with the Online Reverse Engineer process. Should be rationalised into a single function in ReverseEngineerUtils.java
            if (revEngResult.getResultStatus() == 1) {
                // 5.1. Schema
                ArrayList<SmdSchemaDefinition> schemaList = dbDefn.getSchemaList();
                for (SmdSchemaDefinition schemaDefn : schemaList) {
                    String schemaName = schemaDefn.getSchemaName();
                    Integer schemaId = 0;

                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_schema_reveng");
                    in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)                // passed as method parameter
                            .addValue("_database_id", smdDatabaseId, Types.INTEGER)     // passed as method parameter
                            .addValue("_class_id", 19, Types.INTEGER)        // TODO: replace with class service
                            .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                            .addValue("_schema_name", schemaName, Types.VARCHAR)
                            .addValue("_schema_shortdesc", schemaDefn.getSchemaShortDesc(), Types.VARCHAR);
                    out = jdbcCall.execute(in);
                    schemaId = (Integer) out.get("_schema_id");

                    // 5.2. Tables/Views
                    ArrayList<SmdTableViewDefinition> tableViewList = schemaDefn.getTableViewList();
                    for (SmdTableViewDefinition tableViewDefn : tableViewList ) {
                        String tableViewName = tableViewDefn.getTableViewName();
                        Integer tableViewId = 0;

                        // TODO: replace with class service
                        Integer tableViewClassId = 0;
                        if (tableViewDefn.getClassCode().equals("DB_TABLE"))
                            tableViewClassId = 5;
                        else if (tableViewDefn.getClassCode().equals("DB_VIEW"))
                            tableViewClassId = 6;

                        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view_reveng");
                        in = new MapSqlParameterSource()
                                .addValue("_user_id", userId, Types.INTEGER)                // passed as method parameter
                                .addValue("_database_id", smdDatabaseId, Types.INTEGER)     // passed as method parameter
                                .addValue("_schema_id", schemaId, Types.INTEGER)
                                .addValue("_class_id", tableViewClassId, Types.INTEGER)
                                .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                                .addValue("_table_view_name", tableViewName, Types.VARCHAR)
                                .addValue("_table_view_shortdesc", tableViewDefn.getTableViewShortDesc(), Types.VARCHAR);
                        out = jdbcCall.execute(in);
                        tableViewId = (Integer) out.get("_table_view_id");

                        // 5.3. Columns
                        ArrayList<SmdColumnDefinition> columnList = tableViewDefn.getColumnList();
                        for (SmdColumnDefinition columnDefn : columnList) {
                            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column_reveng");
                            in = new MapSqlParameterSource()
                                    .addValue("_user_id", userId, Types.INTEGER)                // passed as method parameter
                                    .addValue("_table_view_id", tableViewId, Types.INTEGER)
                                    .addValue("_class_id", 8, Types.INTEGER)     // TODO: replace with class service
                                    .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                                    .addValue("_column_ordinal", columnDefn.getColumnOrdinal(), Types.INTEGER)
                                    .addValue("_primary_key", columnDefn.getPrimaryKey(), Types.BOOLEAN)
                                    .addValue("_column_datatype", columnDefn.getColumnDatatype(), Types.VARCHAR)
                                    .addValue("_column_name", columnDefn.getColumnName(), Types.VARCHAR)
                                    .addValue("_column_shortdesc", columnDefn.getColumnShortDesc(), Types.VARCHAR);
                            out = jdbcCall.execute(in);
                        }

                    }
                }

                // 5.4 Relationships
                for (SmdSchemaDefinition schemaDefn : schemaList) {
                    ArrayList<SmdRelationshipDefinition> relationshipList = schemaDefn.getRelationshipList();

                    for (SmdRelationshipDefinition relationshipDefn : relationshipList) {
                        try {
                            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_reveng_02");
                            in = new MapSqlParameterSource()
                                    .addValue("_user_id", userId, Types.INTEGER)              // passed as method parameter
                                    .addValue("_database_id", smdDatabaseId, Types.INTEGER)
                                    .addValue("_schema_name", schemaDefn.getSchemaName(), Types.VARCHAR)
                                    .addValue("_class_id", 7, Types.INTEGER)            // TODO: replace with class service
                                    .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                                    .addValue("_parent_schema_name", relationshipDefn.getParentTableViewSchemaName(), Types.VARCHAR)
                                    .addValue("_parent_table_view_name", relationshipDefn.getParentTableViewName(), Types.VARCHAR)
                                    .addValue("_child_schema_name", relationshipDefn.getChildTableViewSchemaName(), Types.VARCHAR)
                                    .addValue("_child_table_view_name", relationshipDefn.getChildTableViewName(), Types.VARCHAR)
                                    .addValue("_relationship_name", relationshipDefn.getRelationshipName(), Types.VARCHAR)
                                    .addValue("_relationship_shortdesc", relationshipDefn.getRelationshipShortDesc(), Types.VARCHAR);
                            out = jdbcCall.execute(in);

                            Integer relationshipId = (Integer) out.get("_relationship_id");
                            Integer parentTableViewId = (Integer) out.get("_parent_table_view_id");
                            Integer childTableViewId = (Integer) out.get("_child_table_view_id");

                            // If parent table is in another schema and has not yet been reverse engineered then parentTableId == 0
                            // Only process columnPairs if parentTableId > 0
                            if (parentTableViewId > 0) {
                                ArrayList<SmdRelationshipColumnPair> columnPairs = relationshipDefn.getColumnPairs();
                                for (SmdRelationshipColumnPair columnPair : columnPairs ) {
                                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_column_pair_reveng");
                                    in = new MapSqlParameterSource()
                                            .addValue("_user_id", userId, Types.INTEGER)              // passed as method parameter
                                            .addValue("_relationship_id", relationshipId, Types.INTEGER)
                                            .addValue("_parent_table_view_id", parentTableViewId, Types.INTEGER)
                                            .addValue("_parent_column_name", columnPair.getParentColumnName(), Types.VARCHAR)
                                            .addValue("_child_table_view_id", childTableViewId, Types.INTEGER)
                                            .addValue("_child_column_name", columnPair.getChildColumnName(), Types.VARCHAR)
                                            .addValue("_reveng_run_id", newRunId, Types.INTEGER);
                                    out = jdbcCall.execute(in);
                                }
                            }
                        }
                        catch (Exception e) {
                            revEngResult.setErrorNumber(-200204);
                            revEngResult.setGeneralErrorMessage("Error creating foreign key relationship " + relationshipDefn.getRelationshipName());
                            System.out.println(e.getLocalizedMessage());
                        }
                    }

                }

            }

            // 6. update the status of the reverse engineering run record in the DL BD
            if (revEngResult.getResultStatus() == 1) {
                revEngResult.setGeneralErrorMessage("Offline reverse engineer run completed successfully for DlDbSmd JSON file.");
            }
            Integer returnId = writeRevEngRun(Integer.parseInt(userId), smdDatabaseId, newRunId, revEngResult.getResultStatus(),
                    revEngResult.getGeneralErrorMessage());
        }

        results.put("result_status", revEngResult.getResultStatus());
        results.put("result_data", revEngResult);

        return results;
    }

    /**
     * Writes a reverse engineered JSON node to the DL DB. This function is called recursively as the JSON tree is traversed.
     * @param jsonNode
     * @return
     */
    private int processDlDbSmdJsonNodeChildren(JsonNode jsonNode, Integer userId, Integer formatId,
                                               Integer importRunId, Integer parentObjectId) {
        Integer resultStatus = 1;
        Integer newParentObectId = 0;

        Iterator<Map.Entry<String, JsonNode>> nodes = jsonNode.fields();
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_fmt_json_node_load");
        Integer jsonNodeClassId = 12;     // TODO: this should not be hard coded

        while (nodes.hasNext()) {
            Map.Entry<String, JsonNode> entry = nodes.next();
            JsonNode currentNode = entry.getValue();



            if (currentNode.getNodeType() == JsonNodeType.OBJECT || currentNode.getNodeType() == JsonNodeType.ARRAY) {
                processDlDbSmdJsonNodeChildren(currentNode, userId, formatId, importRunId, newParentObectId);
            }
        }

        return resultStatus;
    }

    /**
     * Reverse engineers an RDBMS by connecting directly to it from the Data Llama API. The connection details for the
     * database are retrieved from the DL database using smdDatabaseId. This function utilises native DL RDBMS Reverse Engineering connectors.
     * @param smdDatabaseId
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/reverse-engineer/online/dldbsmd/{smdDatabaseId}")
    public Map<String, Object> dbRevEngOnlineDlDbSmd(@PathVariable Integer smdDatabaseId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();

        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);
        RevEngResult revEngResult = revEngUtils.executeOnlineRevEng (smdDatabaseId, Integer.parseInt(userId), cryptKey);

        results.put("result_status", revEngResult.getResultStatus());
        results.put("result_data", revEngResult);
        return results;
    }

    /**
     * Reads Structural Metadata definitions of tables and views for the database specified by databaseId
     * TODO: move this to StructuralMetadata class
     * @param databaseId The identifier of a database in Data Llama SMD table smd_database
     * @return A list of SmdTableViewDefinition objects
     */
    private ArrayList<SmdTableViewDefinition> getSmdDefinitionForDB (Integer databaseId) {
        Map<String, SmdTableViewDefinition> tableViewDefinitions = new HashMap<>();     // used when adding columns objects to table/view object
        ArrayList<SmdTableViewDefinition> tableViewResults = new ArrayList<SmdTableViewDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_by_db");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER);
        Map<String, Object> smdTablesViews = jdbcCall.execute(in);

        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_column_by_db");
        in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER);
        Map<String, Object> smdColumns = jdbcCall.execute(in);

        // 1. link tables/views and columns into a result set using the utility classes SmdTableViewDefinition and SmdColumnDefinition
        ArrayList tableViewList = (ArrayList) smdTablesViews.get("#result-set-1");
        for(Object listElement:tableViewList) {     // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdTableViewDefinition tableViewDefinition = new SmdTableViewDefinition(
                    (Integer) tableViewInfo.get("smd_table_view_id"),
                    (Integer) tableViewInfo.get("class_id"),
                    (Integer) tableViewInfo.get("smd_schema_id"),
                    tableViewInfo.get("smd_table_view_name").toString());
            tableViewDefinition.setSchemaName(tableViewInfo.get("smd_schema_name").toString());

            tableViewDefinitions.put(tableViewInfo.get("smd_table_view_id").toString(), tableViewDefinition);
        }

        ArrayList columnList = (ArrayList) smdColumns.get("#result-set-1");
        for(Object listElement:columnList) {
            LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdColumnDefinition columnDefinition = new SmdColumnDefinition(
                    (Integer) columnInfo.get("smd_column_id"),
                    columnInfo.get("smd_column_name").toString(),
                    (Integer) columnInfo.get("smd_column_ordinal"),
                    columnInfo.get("smd_column_datatype").toString()
            );

            // get frm the table/view hashmap the table/view definition to which this column belongs
            SmdTableViewDefinition tableViewDefinition = tableViewDefinitions.get(columnInfo.get("smd_table_view_id").toString());
            tableViewDefinition.addColumnDefinition(columnDefinition);

            // replace existing table/view in hashmap with updated table/view definition
            tableViewDefinitions.replace(columnInfo.get("smd_table_view_id").toString(), tableViewDefinition);
        }

        Iterator it = tableViewDefinitions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            tableViewResults.add((SmdTableViewDefinition) pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }

        return tableViewResults;
    }

    /**
     * Returns a reconciliation report for tables/views added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/reverse-engineer/reconcile/{smdDatabaseId}")
    public Map<String, Object> getRevEngDbReconciliation(@PathVariable Integer smdDatabaseId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);

        Map<String, Object> resultsSchema = revEngUtils.getRevEngReconReportSchema(smdDatabaseId);
        Map<String, Object> resultsTV = revEngUtils.getRevEngReconReportTableView(smdDatabaseId);
        Map<String, Object> resultsCol = revEngUtils.getRevEngReconReportColumn(smdDatabaseId);
        Map<String, Object> resultsRel = revEngUtils.getRevEngReconReportRelationship(smdDatabaseId);

        resultData.put("schema", resultsSchema.get("result_data"));
        resultData.put("tableview", resultsTV.get("result_data"));
        resultData.put("column", resultsCol.get("result_data"));
        resultData.put("relationship", resultsRel.get("result_data"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns a reconciliation report for tables/views added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = POST, value = "/reverse-engineer/reconcile/{smdDatabaseId}/tableview")
    @ResponseBody
    public Map<String, Object> reconcileRevEngDeletedTableView(@PathVariable Integer smdDatabaseId, @RequestBody ArrayList<RevEngReconciliation> revEngReconciliationList,
                                                             @RequestHeader("dlUserId") String userId) {
        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);

        // 1. execute the reconciliation action
        revEngUtils.reconcileRevEngDeletedTableView(revEngReconciliationList);

        // 2. retrieve the up-to-date reconciliation report for TableViews
        Map<String, Object> results = revEngUtils.getRevEngReconReportTableView(smdDatabaseId);

        return results;
    }

    /**
     * Returns a reconciliation report for columns added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/reverse-engineer/reconcile/{smdDatabaseId}/column")
    public Map<String, Object> getRevEngColumnsReconciliation(@PathVariable Integer smdDatabaseId) {
        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);
        Map<String, Object> results = revEngUtils.getRevEngReconReportColumn(smdDatabaseId);

        return results;
    }

    /**
     * Returns a reconciliation report for tables/views added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = POST, value = "/reverse-engineer/reconcile/{smdDatabaseId}/column")
    @ResponseBody
    public Map<String, Object> reconcileRevEngDeletedColumn(@PathVariable Integer smdDatabaseId, @RequestBody ArrayList<RevEngReconciliation> revEngReconciliationList,
                                                               @RequestHeader("dlUserId") String userId) {
        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);

        // 1. execute the reconciliation action
        revEngUtils.reconcileRevEngDeletedColumn(revEngReconciliationList);

        // 2. retrieve the up-to-date reconciliation report for TableViews
        Map<String, Object> results = revEngUtils.getRevEngReconReportColumn(smdDatabaseId);

        return results;
    }

    /**
     * Returns a reconciliation report for columns added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = GET, value = "/reverse-engineer/reconcile/{smdDatabaseId}/relationship")
    public Map<String, Object> getRevEngRelationshipReconciliation(@PathVariable Integer smdDatabaseId) {
        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);
        Map<String, Object> results = revEngUtils.getRevEngReconReportRelationship(smdDatabaseId);

        return results;
    }

    /**
     * Returns a reconciliation report for tables/views added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    @RequestMapping(method = POST, value = "/reverse-engineer/reconcile/{smdDatabaseId}/relationship")
    @ResponseBody
    public Map<String, Object> reconcileRevEngDeletedRelationship(@PathVariable Integer smdDatabaseId,
                                        @RequestBody ArrayList<RevEngReconciliation> revEngReconciliationList,
                                        @RequestHeader("dlUserId") String userId) {
        ReverseEngineerUtils revEngUtils = new ReverseEngineerUtils(dataSource);

        // 1. execute the reconciliation action
        revEngUtils.reconcileRevEngDeletedRelationship(revEngReconciliationList);

        // 2. retrieve the up-to-date reconciliation report for TableViews
        Map<String, Object> results = revEngUtils.getRevEngReconReportRelationship(smdDatabaseId);

        return results;
    }

}