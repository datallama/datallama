/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

public class ReverseEngineerConnector {
    private Integer connectorId;
    private Integer classId;
    private Integer credentialTypeId;
    private String connectorName;
    private String connectorVersion;
    private String connectorProvider;
    private String connectorShortDesc;
    private String jarName;
    private String javaClassName;

    /**
     * Default constrcutor with no parameters.
     */
    public ReverseEngineerConnector() {
        this.connectorId = 0;
        this.classId = 0;
        this.credentialTypeId = 0;
        this.connectorName = "";
        this.connectorVersion = "";
        this.connectorProvider = "";
        this.connectorShortDesc = "";
        this.jarName = "";
        this.javaClassName = "";
    }

    /**
     * Constructor with core parameters
     * @param connectorId
     * @param classId
     * @param credentialTypeId
     * @param connectorName
     * @param connectorVersion
     */
    public ReverseEngineerConnector(Integer connectorId, Integer classId, Integer credentialTypeId,
                                    String connectorProvider, String connectorName, String connectorVersion) {
        this.connectorId = connectorId;
        this.classId = classId;
        this.credentialTypeId = credentialTypeId;
        this.connectorProvider = connectorProvider;
        this.connectorName = connectorName;
        this.connectorVersion = connectorVersion;
        this.connectorShortDesc = "";
        this.jarName = "";
        this.javaClassName = "";
    }

    public Integer getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Integer connectorId) {
        this.connectorId = connectorId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getCredentialTypeId() {
        return credentialTypeId;
    }

    public void setCredentialTypeId(Integer credentialTypeId) {
        this.credentialTypeId = credentialTypeId;
    }

    public String getConnectorProvider() {
        return connectorProvider;
    }

    public void setConnectorProvider(String connectorProvider) {
        this.connectorProvider = connectorProvider;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public String getConnectorVersion() {
        return connectorVersion;
    }

    public void setConnectorVersion(String connectorVersion) {
        this.connectorVersion = connectorVersion;
    }

    public String getConnectorShortDesc() {
        return connectorShortDesc;
    }

    public void setConnectorShortDesc(String connectorShortDesc) {
        this.connectorShortDesc = connectorShortDesc;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }
}
