/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.MoClassConstants;
import io.datallama.api.common.RevEngConnector;
import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.ReverseEngineerRunCmn;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for Object Store reverse engineering runs.
 * The class is responsible for instantiating the appropriate Reverse Engineering Connector class, invoking the Interface functions and saving the generated information to the DL DB.
 */
public class ReverseEngineerObjectStoreUtil {
    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;

    /**
     * Default Constructor
     * @param dataSource
     * @param userId
     */
    public ReverseEngineerObjectStoreUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Reverse engineer the structure of one or more object containers by connecting directly to the Object Store platform.
     * @param objectId The objectId of the Object Store Space to be reverse engineered
     * @param cryptKey
     * @return
     */
    public RevEngResultCmn executeOnlineRevEng (Integer objectId, String cryptKey) {
        ReverseEngineerObjectStore revEngInterface = null;
        RevEngConnector revEngConnector = null;
        SmdCredential credential = null;
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // 1. get details of the object store space to be reverse engineered
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_filestore_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_filestore_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        Integer classId = (Integer) out.get("_class_id");
        Integer connectorId = (Integer) out.get("_connector_id");
        Integer credentialId = (Integer) out.get("_credential_id");

        if (connectorId < 0 || credentialId < 0) {
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(-200301);
            revEngResult.setGeneralErrorMessage("One or both of connectorId and credentialId is not set. Check the reverse engineering configuration for the object store.");
        }

        // 2. get details of the reverse engineering connector and the credential
        if (revEngResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_connector");
            in = new MapSqlParameterSource().addValue("_connector_id", connectorId, Types.INTEGER);
            out = jdbcCall.execute(in);

            revEngConnector = new RevEngConnector(
                    connectorId,
                    (Integer) out.get("_class_id"),
                    out.get("_connector_name").toString(),
                    out.get("_connector_version").toString(),
                    out.get("_connector_provider").toString(),
                    out.get("_connector_shortdesc").toString(),
                    (Integer) out.get("_credential_type_id"),
                    (Boolean) out.get("_connector_enabled"),
                    out.get("_jar_name").toString(),
                    out.get("_java_class_name").toString()
            );

            // get the credentials for the Object Store Space
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_credential");
            in = new MapSqlParameterSource()
                    .addValue("_credential_id", credentialId, Types.INTEGER)
                    .addValue("_key", cryptKey, Types.VARCHAR);
            out = jdbcCall.execute(in);

            credential = new SmdCredential(
                    credentialId,
                    (Integer) out.get("_credential_type_id"),
                    out.get("_credential_name").toString(),
                    out.get("_credential_detail").toString()
            );
        }

        // 3. instantiate the reverse engineering connector class based on the configuration retrieved from the DL DB
        if (revEngResult.getResultStatus() == 1) {
            Class revEngInterfaceClass = null;
            Constructor revEngInterfaceConstructor = null;
            Object revEngInterfaceObject = null;

            try {
                revEngInterfaceClass = Class.forName(revEngConnector.getJavaClassName());
                revEngInterfaceConstructor = (Constructor) revEngInterfaceClass.getConstructor();
                revEngInterfaceObject = revEngInterfaceConstructor.newInstance();
                revEngInterface = (ReverseEngineerObjectStore) revEngInterfaceObject;
            }
            catch (ClassNotFoundException | NoSuchMethodException e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200302);
                revEngResult.setGeneralErrorMessage(String.format("Could not instantiate reverse engineering connector. Class %s not found.", revEngConnector.getJavaClassName()));
            } catch (IllegalAccessException e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200303);
                revEngResult.setGeneralErrorMessage(String.format("Could not instantiate reverse engineering connector from Class %s. IllegalAccessException. Refer to DL API log for details.", revEngConnector.getJavaClassName()));
                e.printStackTrace();
            } catch (InstantiationException e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200304);
                revEngResult.setGeneralErrorMessage(String.format("Could not instantiate reverse engineering connector from Class %s. InstantiationException. Refer to DL API log for details.", revEngConnector.getJavaClassName()));
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200305);
                revEngResult.setGeneralErrorMessage(String.format("Could not instantiate reverse engineering connector from Class %s. InvocationTargetException. Refer to DL API log for details.", revEngConnector.getJavaClassName()));
                e.printStackTrace();
            }
        }

        // 4. test the connection (may be redundant)
        try {
            revEngInterface.testConnection(credential);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // 5. execute the reverse engineering run
        if (revEngResult.getResultStatus() == 1) {
            try {
                revEngResult = revEngInterface.reverseEngineerObjectStore(credential);
            }
            catch (Exception e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200310);
                revEngResult.setGeneralErrorMessage("Exception thrown when executing the reverse engineering run");
            }
        }

        // 6. persist the reverse engineered catalog to the DL DB
        if (revEngResult.getResultStatus() == 1) {
            // 6.1. create a new reverse engineer run record
            ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn(dataSource, objectId, moClassConstants.OBJECT_STORE_SPACE);
            revEngRun.setInputSource("Direct connection to object store.");
            revEngResult = revEngRun.generateRevEngRun();

            ObjectStoreSpace objectStoreSpace = null;
            if (revEngResult.getResultStatus() == 1) {
                try {
                    objectStoreSpace = revEngInterface.getReverseEngineerCatalog();
                    objectStoreSpace.setObjectId(objectId);
                    objectStoreSpace.setClassId(classId);

                    // 6.1. persist each container
                    ArrayList<ObjectStoreContainer> containerList = objectStoreSpace.getContainerList();

                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_fos_element_reveng");

                    for(ObjectStoreContainer container : containerList) {
                        in = new MapSqlParameterSource()
                                .addValue("_user_id", userId, Types.INTEGER)
                                .addValue("_class_id", moClassConstants.OBJECT_STORE_CONTAINER, Types.INTEGER)
                                .addValue("_parent_object_id", objectStoreSpace.getObjectId(), Types.INTEGER)
                                .addValue("_parent_class_id", objectStoreSpace.getClassId(), Types.INTEGER)
                                .addValue("_parent_element_object_id", 0, Types.INTEGER)    // An Object Store Container is a top level object so has no parent element
                                .addValue("_element_level", 0, Types.INTEGER)   // element level is 0 for top level elements
                                .addValue("_element_name", container.getContainerName(), Types.VARCHAR)
                                .addValue("_element_shortdesc", container.getContainerShortDesc(), Types.VARCHAR)
                                .addValue("_element_config", "{}", Types.VARCHAR)   // not yet implemented
                                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);

                        out = null;
                        try {
                            out = jdbcCall.execute(in);
                            container.setObjectId((Integer) out.get("_return_id"));
                            container.setClassId(moClassConstants.OBJECT_STORE_CONTAINER);
                        }
                        catch (Exception e) {
                            revEngResult.setResultStatus(-1);
                            revEngResult.setErrorNumber(-200311);
                            revEngResult.setGeneralErrorMessage(e.getMessage());
                        }

                        // 6.2. persist any directories in the container
                        if (revEngResult.getResultStatus() == 1) {
                            ArrayList<ObjectStoreDirectory> directoryList = container.getDirectoryList();

                            for (ObjectStoreDirectory directory : directoryList) {
                                revEngResult = persistObjectStoreDirectory (objectStoreSpace, container.getObjectId(), 1,
                                        directory, revEngRun);
                            }
                        }
                    }

                    // 7. if reverse engineer run was successful, check whether reconciliation is required and update the reverse engineer run as completed.
                    if (revEngResult.getResultStatus() == 1) {
                    // TODO: implement reconciliation check
                    /*
                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_api_reveng_run_status");
                    in = new MapSqlParameterSource()
                            .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);
                    out = jdbcCall.execute(in);
                    Integer runResult = (Integer) out.get("_run_result");
                    String runResultDesc = out.get("_run_result_desc").toString();
                    */

                        revEngResult.setResultStatus(2);
                        revEngResult.setGeneralErrorMessage("Object store reverse engineering run completed successfully.");
                        revEngRun.closeRevEngRun(revEngResult);
                    }

                } catch (Exception e) {
                    revEngResult.setResultStatus(-1);
                    revEngResult.setErrorNumber(-200311);
                    revEngResult.setGeneralErrorMessage("Error when retrieving the reverse engineered catalog.");
                }
            }

        }

        return revEngResult;
    }

    /**
     *
     * @param objectStoreSpace
     * @param parentElementObjectId
     * @param elementLevel
     * @param directory
     * @param revEngRun
     * @return
     */
    private RevEngResultCmn persistObjectStoreDirectory (ObjectStoreSpace objectStoreSpace, Integer parentElementObjectId, Integer elementLevel,
                                                         ObjectStoreDirectory directory, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn persistDirResult = new RevEngResultCmn();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_fos_element_reveng");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", this.userId, Types.INTEGER)
                .addValue("_class_id", moClassConstants.OBJECT_STORE_DIRECTORY, Types.INTEGER)
                .addValue("_parent_object_id", objectStoreSpace.getObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", objectStoreSpace.getClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", parentElementObjectId, Types.INTEGER)    // An Object Store Container is a top level object so has no parent element
                .addValue("_element_level", elementLevel, Types.INTEGER)   // element level is 0 for top level elements
                .addValue("_element_name", directory.getDirectoryName(), Types.VARCHAR)
                .addValue("_element_shortdesc", directory.getDirectoryShortDesc(), Types.VARCHAR)
                .addValue("_element_config", "{}", Types.VARCHAR)   // not yet implemented
                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);

        Map<String, Object> out = null;

        try {
            out = jdbcCall.execute(in);
            directory.setObjectId((Integer) out.get("_return_id"));
            directory.setClassId(moClassConstants.OBJECT_STORE_DIRECTORY);
        }
        catch (Exception e) {
            persistDirResult.setResultStatus(-1);
            persistDirResult.setErrorNumber(-200311);
            persistDirResult.setGeneralErrorMessage(e.getMessage());
        }

        if (persistDirResult.getResultStatus() == 1) {
            ArrayList<ObjectStoreDirectory> directoryList = directory.getDirectoryList();
            for (ObjectStoreDirectory subDirectory : directoryList) {
                if (persistDirResult.getResultStatus() == 1) {
                    persistDirResult = persistObjectStoreDirectory(objectStoreSpace, directory.getObjectId(),
                            elementLevel + 1, subDirectory, revEngRun);
                }
            }
        }

        return persistDirResult;
    }

    /**
     * Returns the history of reverse engineer runs for the metadata object identified by this.objectId and this.classId
     * TODO: this function is a copy & paste from RerverseEngineerApiUtil. It should be moved to a common Bean.
     * @return
     */
    public ArrayList<ReverseEngineerRunCmn> getRevEngRunHistory(Integer objectId, Integer classId) {

        ArrayList<ReverseEngineerRunCmn> revEngRunHistory = new ArrayList<ReverseEngineerRunCmn>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_run_list_cmn");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap revEngRunInfo = (LinkedCaseInsensitiveMap) listElement;
            ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn();
            revEngRun.setRevEngRunId((Integer) revEngRunInfo.get("reveng_run_id"));
            revEngRun.setRevEngRunDateTime(revEngRunInfo.get("run_datetime").toString());
            revEngRun.setRevEngRunResult((Integer) revEngRunInfo.get("run_result"));
            revEngRun.setRevEngRunDesc(revEngRunInfo.get("run_result_desc").toString());
            revEngRun.setInputSource(revEngRunInfo.get("input_source").toString());
            revEngRun.setUserLogin(revEngRunInfo.get("user_login").toString());

            revEngRunHistory.add(revEngRun);
        }

        return revEngRunHistory;
    }

}
