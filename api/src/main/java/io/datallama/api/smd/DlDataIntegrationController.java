/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.*;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class DlDataIntegrationController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.security.crypt.key}")
    private String cryptKey;

    /**
     * Reads the list of all file or object stores metadata objects defined in the DL DB.
     * URL Path: /smd/dataintegration/list
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/dataintegration/list")
    @ResponseBody
    public Map<String, Object> getDataIntegrationSummaryList() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList dataIntegrationList = new ArrayList(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dataintegration_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // 1. create a hierarchy of Array Lists from the simple result set returned by database
        for (Object rowObject : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap rowInfo = (LinkedCaseInsensitiveMap) rowObject;
            DlDataIntegrationSummary dataIntegrationSummary = new DlDataIntegrationSummary(
                    (Integer) rowInfo.get("object_id"),
                    (Integer) rowInfo.get("class_id"),
                    rowInfo.get("object_name").toString()
            );
            dataIntegrationSummary.setClassName(rowInfo.get("class_name").toString());
            dataIntegrationSummary.setObjectShortDesc(rowInfo.get("object_shortdesc").toString());

            dataIntegrationList.add(dataIntegrationSummary);
        }

        results.put("result_data", dataIntegrationList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes a new data integration project to the DL DB
     * URL Path: /smd/dataintegration
     * @param newDataIntegrationSummary
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/dataintegration")
    public Map<String, Object> writeDataIntegrationSummary(@RequestBody DlDataIntegrationSummary newDataIntegrationSummary, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataintegration_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newDataIntegrationSummary.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newDataIntegrationSummary.getClassId(), Types.INTEGER)
                .addValue("_object_name",  newDataIntegrationSummary.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newDataIntegrationSummary.getObjectShortDesc(), Types.VARCHAR)
                .addValue("_object_guid",  newDataIntegrationSummary.getObjectGUID(), Types.VARCHAR)
                .addValue("_object_config",  newDataIntegrationSummary.getObjectConfig(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnID", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads details of the dataIntegration element identified by objectId, including associations, custom form data and comments
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/dataintegration/element/{objectId}")
    @ResponseBody
    public Map<String, Object> readDataIntegrationElement(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // list list is returned in the results Map

        // 1. get the core attributes of the directory
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dataintegration_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlDataIntegrationElement dataIntegrationElement = new DlDataIntegrationElement (
                objectId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_parent_object_id"),
                (Integer) out.get("_parent_class_id"),
                (Integer) out.get("_parent_element_object_id"),
                out.get("_object_name").toString());

        DlMetaDataContext mdContext = new DlMetaDataContext();
        dataIntegrationElement.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        dataIntegrationElement.setElementLevel((Integer) out.get("_element_level"));
        dataIntegrationElement.setElementLevelSequence((Integer) out.get("_element_level_sequence"));
        dataIntegrationElement.setElementHierarchyNumber(out.get("_element_hierarchy_number").toString());
        dataIntegrationElement.setObjectGUID(out.get("_object_guid").toString());
        if (out.get("_object_guid") != null) {
            dataIntegrationElement.setObjectShortDesc(out.get("_object_shortdesc").toString());
        }

        resultData.put("attributes", dataIntegrationElement);

        // 2. get the associations for the dataIntegration element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", dataIntegrationElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", dataIntegrationElement.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ObjectMapper objectMapper = new ObjectMapper();
        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2. add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 3. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", dataIntegrationElement.getClassId(), Types.INTEGER)
                .addValue("_object_id", dataIntegrationElement.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        }
        catch(NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);

        // 4. get the comments for the directory
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", dataIntegrationElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", dataIntegrationElement.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }

        resultData.put("comments", commentList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads details of a dataintegration project including the full element list.
     * @param objectId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/dataintegration/content/{objectId}")
    @ResponseBody
    public Map<String, Object> readDataIntegrationContent(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates the status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList<DlDataIntegrationElement> elementList = new ArrayList<DlDataIntegrationElement>();

        // 1. get the dataIntegration definition summary info
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dataintegration_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        DlDataIntegrationSummary dataIntegrationSummary = new DlDataIntegrationSummary(
                objectId,
                (Integer) out.get("_class_id"),
                out.get("_object_name").toString()
        );
        dataIntegrationSummary.setClassName(out.get("_class_name").toString());
        if (out.get("_object_shortdesc") != null) dataIntegrationSummary.setObjectShortDesc(out.get("_object_shortdesc").toString());
        if (out.get("_object_config") != null) dataIntegrationSummary.setObjectConfig(out.get("_object_config").toString());
        dataIntegrationSummary.setConnectorId((Integer) out.get("_connector_id"));
        dataIntegrationSummary.setCredentialId((Integer) out.get("_credential_id"));

        // 1.1. add the mdContext
        DlMetaDataContext mdContext = new DlMetaDataContext();
        dataIntegrationSummary.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        // 1.2. add the custom form
        dataIntegrationSummary.setCustomFormId((Integer) out.get("_custom_form_id"));
        DlCustomForm customForm = new DlCustomForm(
                dataIntegrationSummary.getCustomFormId(),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        dataIntegrationSummary.setCustomForm(customForm);

        resultData.put("dataintegration_summary", dataIntegrationSummary);

        // 2. get all elements in the dataintegration definition hierarchy
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_dataintegration_elements");
        out = jdbcCall.execute(in);
        ArrayList dbElementsResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;
        for (Object listElement : dbElementsResultList) {
            LinkedCaseInsensitiveMap elementMap = (LinkedCaseInsensitiveMap) listElement;

            String objectDescRevEng = elementMap.get("object_desc_reveng") == null ? "" : elementMap.get("object_desc_reveng").toString();

            // 2.1. instantiate DlDataIntegrationElement object for each row returned from DL DB
            DlDataIntegrationElement dataIntegrationElement = new DlDataIntegrationElement(
                    (Integer) elementMap.get("object_id"),
                    (Integer) elementMap.get("class_id"),
                    dataIntegrationSummary.getObjectId(),
                    dataIntegrationSummary.getClassId(),
                    (Integer) elementMap.get("parent_element_object_id"),
                    elementMap.get("object_name").toString()
            );

            dataIntegrationElement.setElementLevel((Integer) elementMap.get("element_level"));
            dataIntegrationElement.setElementLevelSequence((Integer) elementMap.get("element_level_sequence"));
            dataIntegrationElement.setElementHierarchyNumber(elementMap.get("element_hierarchy_number").toString());
            dataIntegrationElement.setElementIsParent((Boolean) elementMap.get("is_parent"));
            dataIntegrationElement.setObjectGUID(elementMap.get("object_guid").toString());
            dataIntegrationElement.setObjectShortDesc(elementMap.get("object_shortdesc").toString());
            if (elementMap.get("object_desc_reveng") != null)
                dataIntegrationElement.setObjectDescReveng(elementMap.get("object_desc_reveng").toString());
            dataIntegrationElement.setMdContext(mdContext.generateMdContextList(elementMap.get("md_context").toString()));
            dataIntegrationElement.setResultSetSequence(resultSetSequence);
            dataIntegrationElement.determineExpanded();
            dataIntegrationElement.determineVisibility();
            resultSetSequence++;

            elementList.add(dataIntegrationElement);
        }
        resultData.put("dataintegration_elements", elementList);

        // 3. get the associations for the dataIntegration project
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource()
                .addValue("_class_id", dataIntegrationSummary.getClassId(), Types.INTEGER)
                .addValue("_object_id", dataIntegrationSummary.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 3.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 3.2. add the metadata context for the AssociatedObject instance
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 4. get the reverse engineer connector & credential configuration
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_concred_config");
        in = new MapSqlParameterSource()
                .addValue("_object_id", dataIntegrationSummary.getObjectId(), Types.INTEGER)
                .addValue("_class_id", dataIntegrationSummary.getClassId(), Types.INTEGER);

        out = jdbcCall.execute(in);

        RevEngConCredConfig conCredConfig = new RevEngConCredConfig();
        conCredConfig.setObjectId(dataIntegrationSummary.getObjectId());
        conCredConfig.setClassId(dataIntegrationSummary.getClassId());
        conCredConfig.setConnectorId((Integer) out.get("_connector_id"));
        conCredConfig.setConnectorName(out.get("_connector_name").toString());
        conCredConfig.setConnectorShortDesc(out.get("_connector_shortdesc").toString());
        conCredConfig.setCredentialId((Integer) out.get("_credential_id"));
        conCredConfig.setCredentialName(out.get("_credential_name").toString());
        conCredConfig.setCredentialShortDesc(out.get("_credential_shortdesc").toString());

        resultData.put("concred_config", conCredConfig);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes a new DataIntegrationElement to the DL DB
     * @param newDataIntegrationElement
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/dataintegration/element")
    public Map<String, Object> writeDataIntegrationElement(@RequestBody DlDataIntegrationElement newDataIntegrationElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataintegration_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newDataIntegrationElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newDataIntegrationElement.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newDataIntegrationElement.getParentObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", newDataIntegrationElement.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", newDataIntegrationElement.getParentElementObjectId(), Types.INTEGER)
                .addValue("_object_guid",  newDataIntegrationElement.getObjectGUID(), Types.VARCHAR)
                .addValue("_object_name",  newDataIntegrationElement.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newDataIntegrationElement.getObjectShortDesc(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnID", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = POST, value = "/smd/dataintegration/customform")
    public Map<String, Object> writeDataIntegrationCustomFormId(@RequestBody MoCustomFormAssoc customFormAssoc, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataintegration_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", customFormAssoc.getObjectId(), Types.INTEGER)
                .addValue("_class_id", customFormAssoc.getClassId(), Types.INTEGER)
                .addValue("_custom_form_id", customFormAssoc.getCustomFormId(), Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        results.put("result_data", "");
        results.put("result_status", resultStatus);

        return results;
    }

        /**
         * Deletes the dataIntegration summary identified by {objectId}
         * @param objectId
         * @return
         */
    @RequestMapping(method = DELETE, value = "/smd/dataintegration/{objectId}")
    @ResponseBody
    public Map<String, Object> deleteDataIntegrationSummary(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_dataintegration_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes the dataIntegration summary identified by {objectId}
     * @param objectId
     * @return
     */
    @RequestMapping(method = DELETE, value = "/smd/dataintegration/element/{objectId}")
    @ResponseBody
    public Map<String, Object> deleteDataIntegrationElement(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_dataintegration_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Description: invokes the stored procedure to move an element within a dataintegration project hierarchy
     */
    @RequestMapping(method = POST, value = "/smd/dataintegration/element/move")
    public Map<String, Object> writeDataIntegrationElementMove(@RequestBody MoMoveElementDefinition moveElementDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataintegration_element_move");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", moveElementDefinition.getObjectId(), Types.INTEGER)
                .addValue("_new_parent_element_id", moveElementDefinition.getNewParentElementObjectId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", null);
        results.put("result_status", resultStatus);

        return results;
    }

}
