package io.datallama.api.smd;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.common.RevEngResultCmn;
import com.azure.storage.file.datalake.DataLakeFileSystemClient;
import com.azure.storage.file.datalake.DataLakeServiceClient;
import com.azure.storage.file.datalake.DataLakeServiceClientBuilder;
import com.azure.storage.file.datalake.models.FileSystemItem;
import com.azure.storage.file.datalake.models.ListPathsOptions;
import com.azure.storage.file.datalake.models.PathItem;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import java.util.LinkedHashMap;

public class RevEngObjectStoreAzureDLSG2 implements ReverseEngineerObjectStore {

    private ObjectStoreSpace objectStoreSpace;

    /**
     * Default constructor with no parameters
     */
    public RevEngObjectStoreAzureDLSG2() {
        this.objectStoreSpace = new ObjectStoreSpace();
    }

    /**
     * Tests that the credentials specified in SmdCredential credential are valid and a connection can be made successfully
     *
     * @param credential
     * @return An { @link SmdCredential} object.
     * @throws Exception
     */
    public SmdCredential testConnection(SmdCredential credential) throws Exception {
        credential.setConnectionTestResult(true);

        // 1. parse the credential detail string to JSON object
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the credential_detail json string to Java LinkedHashMap
        LinkedHashMap credentialDetail = null;
        try {
            credentialDetail = objectMapper.readValue(credential.getCredentialDetail(), LinkedHashMap.class);
        }
        catch (Exception e) {
            credential.setConnectionTestResult(false);
            System.out.println("Exception thrown while parsing credential detail JSON string.");
            System.out.println(e);
        }

        DataLakeServiceClient dataLakeServiceClient = new DataLakeServiceClientBuilder()
                .endpoint(credentialDetail.get("endpoint").toString())
                .sasToken(credentialDetail.get("sas_token").toString())
                .buildClient();

        try {
            dataLakeServiceClient.listFileSystems().forEach(container -> container.getName());
        }
        catch (Exception e) {
            credential.setConnectionTestResult(false);
            credential.setConnectionError(String.format("ERROR trying to connect to Azure DLSG2 endpoint %s using a SAS Token. Check that the SAS Token is correct.", credentialDetail.get("endpoint").toString()));
        }

        return credential;
    }

    /**
     * Connects to an Azure Data Lake Storage Gen2 account and reverse engineers all Containers in the account
     * @param credential
     * @return A {@link RevEngResultCmn} object that indicates the success/failure status of the reverse engineering run.
     * @throws Exception
     */
    public RevEngResultCmn reverseEngineerObjectStore (SmdCredential credential) throws Exception {
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // 1. parse the credential detail string to JSON object
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the credential_detail json string to Java LinkedHashMap
        LinkedHashMap credentialDetail = null;
        try {
            credentialDetail = objectMapper.readValue(credential.getCredentialDetail(), LinkedHashMap.class);
        }
        catch (Exception e) {
            credential.setConnectionTestResult(false);
            revEngResult.setResultStatus(-1);
            revEngResult.setErrorNumber(-203101);
            revEngResult.setGeneralErrorMessage("Exception thrown while parsing credential detail JSON string.");
        }

        // 2. connect to Azure DLSG2 storage account and traverse containers and directories
        if (revEngResult.getResultStatus() == 1) {
            DataLakeServiceClient dataLakeServiceClient = new DataLakeServiceClientBuilder()
                    .endpoint(credentialDetail.get("endpoint").toString())
                    .sasToken(credentialDetail.get("sas_token").toString())
                    .buildClient();
            try {
                objectStoreSpace.setSpaceName(dataLakeServiceClient.getAccountName());
            } catch (Exception e) {
                credential.setConnectionTestResult(false);
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-203102);
                revEngResult.setGeneralErrorMessage(String.format("ERROR trying to connect to Azure DLSG2 endpoint %s using a SAS Token. Check that the SAS Token is correct.", credentialDetail.get("endpoint").toString()));
            }

            if (revEngResult.getResultStatus() == 1 && dataLakeServiceClient != null) {
                try {
                    dataLakeServiceClient.listFileSystems().forEach(fileSystem -> processContainer(dataLakeServiceClient, fileSystem, objectStoreSpace));
                } catch (Exception e) {
                    revEngResult.setResultStatus(-1);
                    revEngResult.setErrorNumber(-203103);
                    revEngResult.setGeneralErrorMessage(String.format("ERROR trying to connect to Azure DLSG2 endpoint %s using a SAS Token. Check that the SAS Token is correct.", credentialDetail.get("endpoint").toString()));
                }
            }
        }



        return revEngResult;

    }

    /**
     * Returns the catalog of reverse engineered AWS S3 buckets
     * @return An {@link ObjectStoreSpace} object.
     * @throws Exception
     */
    public ObjectStoreSpace getReverseEngineerCatalog () throws Exception {
        return this.objectStoreSpace;
    };

    /**
     * Creates an ObjectStoreContainer object for an Azure container and reverse engineers the directories in it.
     * @param dataLakeServiceClient
     * @param fileSystem
     * @param objectStoreSpace
     */
    private void processContainer(DataLakeServiceClient dataLakeServiceClient, FileSystemItem fileSystem, ObjectStoreSpace objectStoreSpace) {
        String containerName = fileSystem.getName();
        Map<String, ObjectStoreDirectory> createdOsDirs = new HashMap<>();

        // 1. create new container
        ObjectStoreContainer objectStoreContainer = new ObjectStoreContainer();
        objectStoreContainer.setContainerName(containerName);

        // 2. process all directories in the container and add container to space
        DataLakeFileSystemClient dataLakeFileSystemClient = dataLakeServiceClient.getFileSystemClient(containerName);
        ListPathsOptions listPathsOpts = new ListPathsOptions();
        listPathsOpts.setRecursive(true);
        dataLakeFileSystemClient.listPaths(listPathsOpts, Duration.ofMinutes(30)).forEach(path -> processPath(path, objectStoreContainer, createdOsDirs));

        // 3. add container to ObjectStoreSpace
        objectStoreSpace.addContainer(objectStoreContainer);

    }

    /**
     * Creates a new ObjectStoreDirectory object for the final path element and adds it to the parent directory, if one exists.
     * @param path
     * @param objectStoreContainer
     * @param createdOsDirs
     */
    private void processPath(PathItem path, ObjectStoreContainer objectStoreContainer, Map<String, ObjectStoreDirectory> createdOsDirs) {
        // 1. only process directories. The actual objects are not included in the reverse engineer run.
        if (path.isDirectory()) {
            String sPath = path.getName();

            // 1.1. create directories listed in the path if they do not already exist. Else, add to existing ObjectStoreDirectory object.
            // assumes that directory paths are returned in the hierarchy order, which is current behaviour of the Azure Jave SDK call.

            if (sPath.indexOf('/') < 0) {    // this is a top level directory so create it and add it directly to the Container. Track it in the createdOsDirs map
                ObjectStoreDirectory newOsDirectory = new ObjectStoreDirectory(sPath);
                objectStoreContainer.addDirectory(newOsDirectory);
                createdOsDirs.put(sPath, newOsDirectory);
            }
            else {    // this is a subdirectory so add it to the parent ObjectStoreDirectory that has already been created
                String dirname = "";
                String basename = "";

                dirname = sPath.substring(0, sPath.lastIndexOf('/'));
                basename = sPath.substring(sPath.lastIndexOf('/') + 1);

                if (createdOsDirs.containsKey(dirname)) {
                    ObjectStoreDirectory createdOsDirectory = createdOsDirs.get(dirname);
                    ObjectStoreDirectory newOsDirectory = new ObjectStoreDirectory(basename);
                    createdOsDirectory.addDirectory(newOsDirectory);
                    createdOsDirs.put(sPath, newOsDirectory);
                }
            }
        }
    }

}
