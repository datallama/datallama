/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class RevEngMysql implements ReverseEngineerDatabase {
    private SmdDatabaseDefinition dbDefn;

    public RevEngMysql(){
        this.dbDefn = new SmdDatabaseDefinition();
    };

    /**
     * Reverse engineers all tables, views and foreign key relationships in the database specified by dbCredentials.
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public SmdDbCredentials testDatabaseConnection (SmdDbCredentials dbCredentials) throws Exception {
        Boolean classFound = true;
        Connection cnxn;

        // 1. try to load the mysql JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setConnectionTestResult(false);
            dbCredentials.setSqlError("Class not found: com.mysql.jdbc.Driver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:mysql://%s:%d/%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());
            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());

                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
            }
        }

        return dbCredentials;
    };

    /**
     * Reverse engineers all tables, views and relationships in the database identified by dbCredentials.
     * @param dbCredentials
     * @param schemaNameList
     * @return
     * @throws Exception
     */
    public RevEngResult reverseEngineerDatabase (SmdDbCredentials dbCredentials, String[] schemaNameList) throws Exception {
        RevEngResult revEngResult = new RevEngResult();
        Boolean classFound = true;
        Connection cnxn = null;

        // 1. try to load the mysql JDBC driver
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setSqlError("Class not found: com.mysql.jdbc.Driver");
            revEngResult.setErrorNumber(-200199);
            revEngResult.setGeneralErrorMessage("ERROR connecting to database. Class not found: com.mysql.jdbc.Driver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:mysql://%s:%d/%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());
            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());
                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
                revEngResult.setErrorNumber(-200100);
                revEngResult.setGeneralErrorMessage(failureMessage);
            }
        }

        // 3. execute the reverse engineer
        if (dbCredentials.getConnectionTestResult()) {
            dbDefn.setDatabaseName(dbCredentials.getDbName());

            // 3.1. DB Schema. NOTE: MySql does not implement schemas. The database name is the same as the schema name
            SmdSchemaDefinition schemaDefn = new SmdSchemaDefinition();
            schemaDefn.setSchemaName(dbCredentials.getDbName());

            // 3.2 Tables & Views
            String tableviewSql = "select TABLE_NAME, TABLE_TYPE, TABLE_COMMENT from information_schema.TABLES " +
                    "where TABLE_SCHEMA = '" + dbDefn.getDatabaseName() + "' order by TABLE_NAME;";

            try {
                Statement stmt = cnxn.createStatement();
                ResultSet rs = stmt.executeQuery(tableviewSql);
                while(rs.next()) {
                    SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition();
                    tableViewDefn.setSchemaName(schemaDefn.getSchemaName());
                    tableViewDefn.setTableViewName(rs.getString("TABLE_NAME"));
                    tableViewDefn.setTableViewShortDesc(rs.getString("TABLE_COMMENT"));
                    String tableType = rs.getString("TABLE_TYPE");
                    if (tableType.equals("BASE TABLE")) {
                        tableViewDefn.setClassCode("DB_TABLE");
                    } else if (tableType.equals("VIEW")) {
                        tableViewDefn.setClassCode("DB_VIEW");
                    }
                    else {
                        tableViewDefn.setClassCode("DB_TABLE");     // TODO: should probably be 'UNKNOWN'
                    }

                    schemaDefn.addTableView(tableViewDefn);
                }
            }
            catch (SQLException e) {
                revEngResult.setErrorNumber(-200101);
                revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve table and view definitions.");
                e.printStackTrace();
            }

            // 3.3. Columns
            if (revEngResult.getErrorNumber() == 0) {
                for (SmdTableViewDefinition tableViewDefn : schemaDefn.getTableViewList()) {
                    String columnSql = "select TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, " +
                            "CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE, DATETIME_PRECISION, COLUMN_TYPE, COLUMN_COMMENT " +
                            "from INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = '" + dbDefn.getDatabaseName() + "' and TABLE_NAME = '" +
                            tableViewDefn.getTableViewName() + "' order by ordinal_position;";
                    String pkSql = "select tc.CONSTRAINT_NAME, kcu.TABLE_NAME, kcu.COLUMN_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc " +
                            "join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu on kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA " +
                            "and kcu.TABLE_NAME = tc.TABLE_NAME and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME " +
                            "where tc.TABLE_SCHEMA = '" + dbDefn.getDatabaseName() + "' and tc.TABLE_NAME = '" +
                            tableViewDefn.getTableViewName() + "' and tc.CONSTRAINT_TYPE = 'PRIMARY KEY';";

                    try {
                        Statement stmt = cnxn.createStatement();
                        ResultSet rs = stmt.executeQuery(pkSql);
                        HashMap<String, String> pkColumns = new HashMap<String, String>();
                        while (rs.next()) {
                            pkColumns.put(rs.getString("COLUMN_NAME"), "PK");
                        }
                        rs = stmt.executeQuery(columnSql);
                        while (rs.next()) {
                            SmdColumnDefinition columnDefn = new SmdColumnDefinition();
                            columnDefn.setColumnName(rs.getString("COLUMN_NAME"));
                            columnDefn.setTableViewName(rs.getString("TABLE_NAME"));
                            columnDefn.setColumnOrdinal(rs.getInt("ORDINAL_POSITION"));
                            columnDefn.setColumnDatatype(rs.getString("COLUMN_TYPE"));
                            columnDefn.setColumnShortDesc(rs.getString("COLUMN_COMMENT"));
                            if (rs.getString("IS_NULLABLE").equals("NO"))
                                columnDefn.setNullable(false);
                            else
                                columnDefn.setNullable(true);
                            if (pkColumns.containsKey(rs.getString("COLUMN_NAME")))
                                columnDefn.setPrimaryKey(true);

                            tableViewDefn.addColumnDefinition(columnDefn);
                        }

                    }
                    catch (SQLException e) {
                        revEngResult.setErrorNumber(-200102);
                        revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve column definitions.");
                        e.printStackTrace();
                    }
                }
            }

            // 3.4. FK Relationships
            if (revEngResult.getErrorNumber() == 0) {
                String relationshipSql = "select tc.CONSTRAINT_NAME, tc.TABLE_SCHEMA, tc.TABLE_NAME, kcu.COLUMN_NAME, " +
                        "REFERENCED_TABLE_SCHEMA, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME, POSITION_IN_UNIQUE_CONSTRAINT " +
                        "from INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc " +
                        "join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu on kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA " +
                        "and kcu.TABLE_NAME = tc.TABLE_NAME and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME " +
                        "where tc.CONSTRAINT_SCHEMA = '" + dbDefn.getDatabaseName() + "' and tc.constraint_type = 'FOREIGN KEY'" +
                        "order by tc.CONSTRAINT_NAME, POSITION_IN_UNIQUE_CONSTRAINT;";

                try {
                    Statement stmt = cnxn.createStatement();
                    ResultSet rs = stmt.executeQuery(relationshipSql);

                    String previousFKname = "";
                    String currentFKname = "";
                    SmdRelationshipDefinition relDefn = null;

                    // 3.4.1. process FK relationship info in a control break on FK name
                    while (rs.next()) {
                        currentFKname = rs.getString("CONSTRAINT_NAME");

                        if (!currentFKname.equals(previousFKname)) {
                            // add the completed relationship to schema
                            if (relDefn != null)
                                schemaDefn.addRelationship(relDefn);

                            // create new SMD Relationship Definition
                            relDefn = new SmdRelationshipDefinition();
                            relDefn.setRelationshipName(currentFKname);
                            relDefn.setParentTableViewSchemaName(rs.getString("REFERENCED_TABLE_SCHEMA"));
                            relDefn.setParentTableViewName(rs.getString("REFERENCED_TABLE_NAME"));
                            relDefn.setChildTableViewSchemaName(rs.getString("TABLE_SCHEMA"));
                            relDefn.setChildTableViewName(rs.getString("TABLE_NAME"));

                            SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                            relColPair.setParentColumnName(rs.getString("REFERENCED_COLUMN_NAME"));
                            relColPair.setChildColumnName(rs.getString("COLUMN_NAME"));
                            relDefn.addColumnPair(relColPair);
                        }
                        else {
                            SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                            relColPair.setParentColumnName(rs.getString("REFERENCED_COLUMN_NAME"));
                            relColPair.setChildColumnName(rs.getString("COLUMN_NAME"));
                            relDefn.addColumnPair(relColPair);
                        }
                        previousFKname = currentFKname;
                    }
                    // add the completed relationship to schema
                    if (relDefn != null)
                        schemaDefn.addRelationship(relDefn);
                }
                catch (SQLException e) {
                    revEngResult.setErrorNumber(-200103);
                    revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve foreign key definitions.");
                    e.printStackTrace();
                }
            }

            if (revEngResult.getErrorNumber() == 0) {
                dbDefn.addSchema(schemaDefn);
            }
        }

        return revEngResult;
    };

    /**
     * Returns the reverse engineered database catalog
     * @return
     * @throws Exception
     */
    public SmdDatabaseDefinition getReverseEngineerCatalog () throws Exception {
        return dbDefn;
    };

}
