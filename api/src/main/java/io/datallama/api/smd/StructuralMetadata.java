/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class StructuralMetadata {

    private DriverManagerDataSource dataSource;

    /**
     * Default constructor.
     * @param dataSource
     */
    public StructuralMetadata(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Reads details of the SMD Database definition from the DL DB.
     * @param databaseId
     * @return
     */
    public SmdDatabaseDefinition getDatabaseDetail (Integer databaseId) {
        // 1. retrieve the core details for the SMD Database definition
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_database_core_details");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_smd_database_id", databaseId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);

        SmdDatabaseDefinition dbDefinition = new SmdDatabaseDefinition(
                (Integer) rsltMap.get("smd_database_id"),
                (Integer) rsltMap.get("smd_server_id"),
                (Integer) rsltMap.get("class_id"),
                rsltMap.get("jdbc_driver_family").toString(),
                rsltMap.get("smd_hostname").toString(),
                (Integer) rsltMap.get("smd_port"),
                rsltMap.get("logical_database_name").toString(),
                rsltMap.get("smd_database_name").toString(),
                rsltMap.get("smd_database_shortdesc").toString(),
                rsltMap.get("smd_username").toString(),
                rsltMap.get("smd_password").toString()
        );
        dbDefinition.setObjectGUID(rsltMap.get("object_guid").toString());
        dbDefinition.setTableViewCustomFormId((Integer) rsltMap.get("custom_form_id_tv"));
        dbDefinition.setTableViewCustomFormName(rsltMap.get("custom_form_name_tv").toString());
        dbDefinition.setSchemaNameList(rsltMap.get("schema_name_list").toString());

        // 2. get the custom form definition and data for the SMD database definition
        DlCustomForm customForm = new DlCustomForm(
                (Integer) rsltMap.get("custom_form_id"),
                rsltMap.get("custom_form_name").toString(),
                rsltMap.get("schema_definition").toString(),
                rsltMap.get("form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        if (rsltMap.get("custom_form_data") == null)
            customForm.setFormData("{}");
        else
            customForm.setFormData(rsltMap.get("custom_form_data").toString());
        dbDefinition.setCustomForm(customForm);

        // 3. get the list of data sensitivity tags for the SMD database definition
        in = new MapSqlParameterSource().addValue("_object_id", databaseId, Types.INTEGER)
                .addValue("_class_id", dbDefinition.getClassId(), Types.INTEGER);
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_multi");
        Map<String, Object> rslts = jdbcCall.execute(in);

        ArrayList dsTagList = (ArrayList) rslts.get("#result-set-1");

        for (Object listElement:dsTagList) {
            LinkedCaseInsensitiveMap dsTagInfo = (LinkedCaseInsensitiveMap) listElement;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                    (Integer) dsTagInfo.get("data_sensitivity_id"),
                    dsTagInfo.get("data_sensitivity_level").toString(),
                    dsTagInfo.get("data_sensitivity_name").toString()
            );
            dsTag.setCreateMode((Integer) dsTagInfo.get("create_mode"));
            dbDefinition.addDlDsTag(dsTag);
        }

        return dbDefinition;
    }

    /**
     * Reads the SMD Database summary identified by the GUID objectGUID from the DL DB
     * @param objectGUID
     * @return
     */
    public SmdDatabaseDefinition getDatabaseSummaryByGUID (String objectGUID) {

        // 1. retrieve objectId and classID for the SMD database definition using objectGUID
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_database_ids_by_guid");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_guid", objectGUID, Types.VARCHAR);
        // TODO: try/catch blocks required.
        Map<String, Object> out = jdbcCall.execute(in);

        // 2. retrieve the SMD Database Summary
        SmdDatabaseDefinition dbDefn = getDatabaseDetail((Integer) out.get("_object_id"));

        return dbDefn;
    }

    /**
     * Reads Structural Metadata definitions for the schema, tables and views in the database definition identified by databaseId
     * @param databaseId The identifier of a database in Data Llama SMD table smd_database
     * @return A list of SmdTableViewDefinition objects
     */
    public ArrayList<SmdTableViewDefinition> getSmdDbDefinitionAsHierarchy (Integer databaseId) {
        Map<String, SmdTableViewDefinition> tableViewDefinitions = new HashMap<>();
        ArrayList<SmdTableViewDefinition> tableViewResults = new ArrayList<SmdTableViewDefinition>();

        // 1. get all tables/views in the DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_by_db");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER);
        Map<String, Object> smdTablesViews = jdbcCall.execute(in);

        // 2. get all columns in the DB
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_column_by_db");
        in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER);
        Map<String, Object> smdColumns = jdbcCall.execute(in);

        // 3. link tables/views and columns into a result set using the utility classes SmdTableViewDefinition and SmdColumnDefinition
        ArrayList tableViewList = (ArrayList) smdTablesViews.get("#result-set-1");
        for(Object listElement:tableViewList) {
            LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdTableViewDefinition tableViewDefinition = new SmdTableViewDefinition(
                    (Integer) tableViewInfo.get("smd_table_view_id"),
                    (Integer) tableViewInfo.get("class_id"),
                    (Integer) tableViewInfo.get("smd_schema_id"),
                    tableViewInfo.get("smd_table_view_name").toString());
            tableViewDefinition.setClassName(tableViewInfo.get("class_name").toString());
            tableViewDefinition.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
            tableViewDefinition.setSchemaName(tableViewInfo.get("smd_schema_name").toString());

            tableViewDefinitions.put(tableViewInfo.get("smd_table_view_id").toString(), tableViewDefinition);
        }

        ArrayList columnList = (ArrayList) smdColumns.get("#result-set-1");
        for(Object listElement:columnList) {
            LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdColumnDefinition columnDefinition = new SmdColumnDefinition(
                    (Integer) columnInfo.get("smd_column_id"),
                    columnInfo.get("smd_column_name").toString(),
                    (Integer) columnInfo.get("smd_column_ordinal"),
                    columnInfo.get("smd_column_datatype").toString()
            );
            columnDefinition.setClassId((Integer) columnInfo.get("class_id"));

            SmdTableViewDefinition tableViewDefinition = tableViewDefinitions.get(columnInfo.get("smd_table_view_id").toString());
            tableViewDefinition.addColumnDefinition(columnDefinition);

            tableViewDefinitions.replace(columnInfo.get("smd_table_view_id").toString(), tableViewDefinition);
        }

        Iterator it = tableViewDefinitions.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            tableViewResults.add((SmdTableViewDefinition) pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }

        return tableViewResults;
    }

    /**
     * Create a new schema manually in the DL DB.
     * @return
     */
    public SmdCrudResult writeSmdSchema (Integer userId, SmdSchemaDefinition schemaDefn) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();
        SmdSchemaDefinition newSchema = new SmdSchemaDefinition();

        // 1. Create the new SMD TableView in the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_schema");
            SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                    .addValue("_object_id", schemaDefn.getSchemaId(), Types.INTEGER)
                    .addValue("_class_id", schemaDefn.getClassId(), Types.INTEGER)
                    .addValue("_parent_object_id", schemaDefn.getDatabaseId(), Types.INTEGER)
                    .addValue("_schema_name", schemaDefn.getSchemaName(), Types.VARCHAR)
                    .addValue("_schema_shortdesc", schemaDefn.getSchemaShortDesc(), Types.VARCHAR);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            smdCrudResult.setReturnId((Integer) out.get("_return_id"));
        } catch (UncategorizedSQLException e) {
            smdCrudResult.setResultStatus(-1);
            SQLException sqle = e.getSQLException();
            smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
            smdCrudResult.setSqlErrorState(sqle.getSQLState());
            smdCrudResult.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            smdCrudResult.setResultStatus(-1);
            smdCrudResult.setSqlErrorCode(0);
            smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
            smdCrudResult.setSqlErrorMessage(e.getMessage());
        }

        // 2. get details of the new SMD Schema
        if (smdCrudResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_schema");      // TODO: the stored proc should be updated to use OUT params
            in = new MapSqlParameterSource().addValue("_smd_schema_id", smdCrudResult.getReturnId(), Types.INTEGER);

            try {
                Map<String, Object> out = jdbcCall.execute(in);

                ArrayList schemaList = (ArrayList) out.get("#result-set-1");
                LinkedCaseInsensitiveMap schemaInfo = (LinkedCaseInsensitiveMap) schemaList.get(0);

                newSchema.setSchemaId(smdCrudResult.getReturnId());
                newSchema.setClassId((Integer) schemaInfo.get("class_id"));
                newSchema.setSchemaName(schemaInfo.get("smd_schema_name").toString());
                newSchema.setDatabaseId((Integer) schemaInfo.get("smd_database_id"));
                newSchema.setSchemaShortDesc(schemaInfo.get("smd_schema_shortdesc").toString());

                DlMetaDataContext mdContext = new DlMetaDataContext();
                newSchema.setMdContext(mdContext.generateMdContextList(schemaInfo.get("md_context").toString()));

                smdCrudResult.addResultData("schema_summary", newSchema);

            } catch (UncategorizedSQLException e) {
                smdCrudResult.setResultStatus(-1);
                SQLException sqle = e.getSQLException();
                smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
                smdCrudResult.setSqlErrorState(sqle.getSQLState());
                smdCrudResult.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                smdCrudResult.setResultStatus(-1);
                smdCrudResult.setSqlErrorCode(0);
                smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
                smdCrudResult.setSqlErrorMessage(e.getMessage());
            }
        }

        return smdCrudResult;
    }

    /**
     * Updates summary information (short description) for an existing schema.
     * @return
     */
    public SmdCrudResult writeSmdSchemaSummary (Integer userId, SmdSchemaDefinition schemaDefn) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();

        // 1. Create the new SMD TableView in the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_schema_shortdesc");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", schemaDefn.getSchemaId(), Types.INTEGER)
                .addValue("_class_id", schemaDefn.getClassId(), Types.INTEGER)
                .addValue("_object_shortdesc", schemaDefn.getSchemaShortDesc(), Types.VARCHAR);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            smdCrudResult.setReturnId((Integer) out.get("_return_id"));
        } catch (UncategorizedSQLException e) {
            smdCrudResult.setResultStatus(-1);
            SQLException sqle = e.getSQLException();
            smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
            smdCrudResult.setSqlErrorState(sqle.getSQLState());
            smdCrudResult.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            smdCrudResult.setResultStatus(-1);
            smdCrudResult.setSqlErrorCode(0);
            smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
            smdCrudResult.setSqlErrorMessage(e.getMessage());
        }

        return smdCrudResult;
    }

    /**
     * Read details of an SMD Schema definition from the DL DB, including the schema summary infor, the table/view list
     *  and the relationship list.
     * @param objectId
     * @return
     */
    public SmdCrudResult getSmdSchemaDetails(Integer objectId) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();
        SmdSchemaDefinition newSchema = new SmdSchemaDefinition();
        Map<String, Object> resultData = new HashMap<>();
        ArrayList<SmdTableViewDefinition> tableViewList = new ArrayList<SmdTableViewDefinition>();
        ArrayList<SmdRelationshipDefinition> relationshipList = new ArrayList<SmdRelationshipDefinition>();

        // 1. Get the summary information for the DB Schema definition
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_schema");    // TODO: the stored proc should be updated to use OUT params
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_schema_id", objectId, Types.INTEGER);

        try {
            Map<String, Object> out = jdbcCall.execute(in);

            ArrayList rslt = (ArrayList) out.get("#result-set-1");
            LinkedCaseInsensitiveMap rsltMap = (LinkedCaseInsensitiveMap) rslt.get(0);

            newSchema.setSchemaId((Integer) rsltMap.get("smd_schema_id"));
            newSchema.setDatabaseId((Integer) rsltMap.get("smd_database_id"));
            newSchema.setClassId((Integer) rsltMap.get("class_id"));
            newSchema.setSchemaName(rsltMap.get("smd_schema_name").toString());
            newSchema.setObjectGUID(rsltMap.get("object_guid").toString());
            if (rsltMap.get("smd_schema_shortdesc") != null) {
                newSchema.setSchemaShortDesc(rsltMap.get("smd_schema_shortdesc").toString());
            }
            DlMetaDataContext mdContext = new DlMetaDataContext();
            newSchema.setMdContext(mdContext.generateMdContextList(rsltMap.get("md_context").toString()));

            resultData.put("schema_summary", newSchema);
        } catch (UncategorizedSQLException e) {
            smdCrudResult.setResultStatus(-1);
            SQLException sqle = e.getSQLException();
            smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
            smdCrudResult.setSqlErrorState(sqle.getSQLState());
            smdCrudResult.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            smdCrudResult.setResultStatus(-1);
            smdCrudResult.setSqlErrorCode(0);
            smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
            smdCrudResult.setSqlErrorMessage(e.getMessage());
        }

        // 2. get the list of Tables/Views associated with the DB Schema definition
        if (smdCrudResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_by_schema");
            in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);

            try {
                Map<String, Object> out = jdbcCall.execute(in);

                ArrayList rslts = (ArrayList) out.get("#result-set-1");
                for (Object rsltsRow : rslts) {
                    LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) rsltsRow;
                    SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition(
                            (Integer) tableViewInfo.get("smd_table_view_id"),
                            (Integer) tableViewInfo.get("class_id"),
                            (Integer) tableViewInfo.get("smd_schema_id"),
                            tableViewInfo.get("smd_table_view_name").toString()
                    );
                    tableViewDefn.setClassName(tableViewInfo.get("class_name").toString());
                    if (tableViewInfo.get("smd_table_view_shortdesc") != null) {
                        tableViewDefn.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
                    }
                    tableViewDefn.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
                    tableViewDefn.setObjectGUID(tableViewInfo.get("object_guid").toString());

                    tableViewList.add(tableViewDefn);
                }
                resultData.put("table_view_list", tableViewList);

            } catch (UncategorizedSQLException e) {
                smdCrudResult.setResultStatus(-1);
                SQLException sqle = e.getSQLException();
                smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
                smdCrudResult.setSqlErrorState(sqle.getSQLState());
                smdCrudResult.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                smdCrudResult.setResultStatus(-1);
                smdCrudResult.setSqlErrorCode(0);
                smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
                smdCrudResult.setSqlErrorMessage(e.getMessage());
            }

        }

        // 3. get the list of Relationships associated with the DB Schema definition
        if (smdCrudResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_relationship_by_schema");
            in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);

            try {
                Map<String, Object> out = jdbcCall.execute(in);

                ArrayList rslts = (ArrayList) out.get("#result-set-1");
                for (Object rsltsRow : rslts) {
                    LinkedCaseInsensitiveMap relationshipInfo = (LinkedCaseInsensitiveMap) rsltsRow;
                    SmdRelationshipDefinition relationshipDefn = new SmdRelationshipDefinition ();
                    relationshipDefn.setRelationshipId((Integer) relationshipInfo.get("smd_relationship_id"));
                    relationshipDefn.setClassId((Integer) relationshipInfo.get("class_id"));
                    relationshipDefn.setCreateMode((Integer) relationshipInfo.get("create_mode"));
                    relationshipDefn.setRelationshipName(relationshipInfo.get("smd_relationship_name").toString());
                    if (relationshipInfo.get("smd_relationship_shortdesc") != null) {
                        relationshipDefn.setRelationshipShortDesc(relationshipInfo.get("smd_relationship_shortdesc").toString());
                    }
                    relationshipDefn.setParentTableViewName(relationshipInfo.get("parent_table_view_name").toString());
                    relationshipDefn.setChildTableViewName(relationshipInfo.get("child_table_view_name").toString());

                    relationshipList.add(relationshipDefn);
                }
                resultData.put("relationship_list", relationshipList);

            } catch (UncategorizedSQLException e) {
                smdCrudResult.setResultStatus(-1);
                SQLException sqle = e.getSQLException();
                smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
                smdCrudResult.setSqlErrorState(sqle.getSQLState());
                smdCrudResult.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                smdCrudResult.setResultStatus(-1);
                smdCrudResult.setSqlErrorCode(0);
                smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
                smdCrudResult.setSqlErrorMessage(e.getMessage());

            }
        }

        // 4. get the list of data sensitivity tags for the SMD database definition
        if (smdCrudResult.getResultStatus() == 1) {
            in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                    .addValue("_class_id", newSchema.getClassId(), Types.INTEGER);
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_multi");
            Map<String, Object> rslts = jdbcCall.execute(in);

            ArrayList dsTagList = (ArrayList) rslts.get("#result-set-1");

            for (Object listElement:dsTagList) {
                LinkedCaseInsensitiveMap dsTagInfo = (LinkedCaseInsensitiveMap) listElement;
                DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                        (Integer) dsTagInfo.get("data_sensitivity_id"),
                        dsTagInfo.get("data_sensitivity_level").toString(),
                        dsTagInfo.get("data_sensitivity_name").toString()
                );
                dsTag.setCreateMode((Integer) dsTagInfo.get("create_mode"));
                newSchema.addDlDsTag(dsTag);
            }
        }

        smdCrudResult.addResultData("result_data", resultData);
        return smdCrudResult;
    }

    /**
     * Gets the list of all columns in the SMD database definition identified by objectId that have a data sensitivity assignment
     * @param objectId
     * @return
     */
    public ArrayList<DlDataSensitivityAssignment> getSmdSchemaDataSensitivityReport (Integer objectId) {
        ArrayList<DlDataSensitivityAssignment> smdSchemaDsReport = new ArrayList<DlDataSensitivityAssignment>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_smd_schema_report");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> dbResults = jdbcCall.execute(in);

        ArrayList dsReportResults = (ArrayList) dbResults.get("#result-set-1");
        // 1. create a list of DlDataSensitivityAssignment objects from the query results
        for (Object dsReport : dsReportResults) {
            LinkedCaseInsensitiveMap dsReportInfo = (LinkedCaseInsensitiveMap) dsReport;

            // 1.1. create the base DlDataSensitivityAssignment object
            DlDataSensitivityAssignment dsAssignment = new DlDataSensitivityAssignment();
            dsAssignment.setObjectId((Integer) dsReportInfo.get("object_id"));
            dsAssignment.setClassId((Integer) dsReportInfo.get("class_id"));
            dsAssignment.setParentObjectId((Integer) dsReportInfo.get("parent_object_id"));
            dsAssignment.setParentClassId((Integer) dsReportInfo.get("parent_class_id"));
            dsAssignment.setObjectName(dsReportInfo.get("object_name").toString());
            dsAssignment.setObjectShortDesc(dsReportInfo.get("object_shortdesc").toString());

            // 1.2. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            dsAssignment.setMdContext(mdContext.generateMdContextList(dsReportInfo.get("md_context").toString()));

            // 1.3. add the data sensitivity tag - the most important part!
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                    (Integer) dsReportInfo.get("data_sensitivity_id"),
                    (String) dsReportInfo.get("data_sensitivity_level"),
                    (String) dsReportInfo.get("data_sensitivity_name")
            );
            dsAssignment.setDsTag(dsTag);

            smdSchemaDsReport.add(dsAssignment);
        }

        return smdSchemaDsReport;
    }

    /**
     * Read the table/view list for the SMD Schema definition identified by objectId
     * @param objectId
     * @return
     */
    public SmdCrudResult getSmdSchemaTableViewList(Integer objectId) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();
        Map<String, Object> resultData = new HashMap<>();
        ArrayList<SmdTableViewDefinition> tableViewList = new ArrayList<SmdTableViewDefinition>();

        // 1. Get the summary information for the DB Schema definition
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_by_schema");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);

        // 1. get the list of Tables/Views associated with the DB Schema definition
        if (smdCrudResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_by_schema");
            in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);

            try {
                Map<String, Object> out = jdbcCall.execute(in);

                ArrayList rslts = (ArrayList) out.get("#result-set-1");
                for (Object rsltsRow : rslts) {
                    LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) rsltsRow;
                    SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition(
                            (Integer) tableViewInfo.get("smd_table_view_id"),
                            (Integer) tableViewInfo.get("class_id"),
                            (Integer) tableViewInfo.get("smd_schema_id"),
                            tableViewInfo.get("smd_table_view_name").toString()
                    );
                    tableViewDefn.setClassName(tableViewInfo.get("class_name").toString());
                    if (tableViewInfo.get("smd_table_view_shortdesc") != null) {
                        tableViewDefn.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
                    }
                    tableViewDefn.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
                    tableViewDefn.setObjectGUID(tableViewInfo.get("object_guid").toString());

                    tableViewList.add(tableViewDefn);
                }
                resultData.put("table_view_list", tableViewList);

            } catch (UncategorizedSQLException e) {
                smdCrudResult.setResultStatus(-1);
                SQLException sqle = e.getSQLException();
                smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
                smdCrudResult.setSqlErrorState(sqle.getSQLState());
                smdCrudResult.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                smdCrudResult.setResultStatus(-1);
                smdCrudResult.setSqlErrorCode(0);
                smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
                smdCrudResult.setSqlErrorMessage(e.getMessage());
            }

        }

        smdCrudResult.addResultData("result_data", resultData);
        return smdCrudResult;
    }

    /**
     * Gets the list of Schemas for the SMD database definition identified by databaseId
     * @param databaseId
     * @return
     */
    public ArrayList<SmdSchemaDefinition> getSmdDbSchemaList (Integer databaseId) {
        ArrayList<SmdSchemaDefinition> schemaList = new ArrayList<SmdSchemaDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_schema_by_db");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER);
        Map<String, Object> smdSchemas = jdbcCall.execute(in);

        ArrayList schemaResults = (ArrayList) smdSchemas.get("#result-set-1");
        for (Object listElement : schemaResults) {
            LinkedCaseInsensitiveMap schemaInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdSchemaDefinition schemaDefinition = new SmdSchemaDefinition(
                    (Integer) schemaInfo.get("smd_schema_id"),
                    (Integer) schemaInfo.get("class_id"),
                    databaseId,
                    schemaInfo.get("smd_schema_name").toString()
            );
            schemaDefinition.setClassName(schemaInfo.get("class_name").toString());
            schemaDefinition.setSchemaShortDesc(schemaInfo.get("smd_schema_shortdesc").toString());

            schemaList.add(schemaDefinition);
        }
        return schemaList;
    }

    /**
     * Gets the list of all columns in the SMD database definition identified by objectId that have a data sensitivity assignment
     * @param objectId
     * @return
     */
    public ArrayList<DlDataSensitivityAssignment> getSmdDbDataSensitivityReport (Integer objectId) {
        ArrayList<DlDataSensitivityAssignment> smdDbDsReport = new ArrayList<DlDataSensitivityAssignment>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_smd_db_report");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> dbResults = jdbcCall.execute(in);

        ArrayList dsReportResults = (ArrayList) dbResults.get("#result-set-1");
        // 1. create a list of DlDataSensitivityAssignment objects from the query results
        for (Object dsReport : dsReportResults) {
            LinkedCaseInsensitiveMap dsReportInfo = (LinkedCaseInsensitiveMap) dsReport;

            // 1.1. create the base DlDataSensitivityAssignment object
            DlDataSensitivityAssignment dsAssignment = new DlDataSensitivityAssignment();
            dsAssignment.setObjectId((Integer) dsReportInfo.get("object_id"));
            dsAssignment.setClassId((Integer) dsReportInfo.get("class_id"));
            dsAssignment.setParentObjectId((Integer) dsReportInfo.get("parent_object_id"));
            dsAssignment.setParentClassId((Integer) dsReportInfo.get("parent_class_id"));
            dsAssignment.setObjectName(dsReportInfo.get("object_name").toString());
            dsAssignment.setObjectShortDesc(dsReportInfo.get("object_shortdesc").toString());

            // 1.2. add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            dsAssignment.setMdContext(mdContext.generateMdContextList(dsReportInfo.get("md_context").toString()));

            // 1.3. add the data sensitivity tag - the most important part!
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                    (Integer) dsReportInfo.get("data_sensitivity_id"),
                    (String) dsReportInfo.get("data_sensitivity_level"),
                    (String) dsReportInfo.get("data_sensitivity_name")
            );
            dsAssignment.setDsTag(dsTag);

            smdDbDsReport.add(dsAssignment);
        }

        return smdDbDsReport;
    }

    /**
     * Writes a manually defined SMD Table/View definition to the DL Database. This is separate from a reverse engineer run.
     * @param userId
     * @param tableViewDefinition
     * @return
     */
    public SmdCrudResult writeSmdTableView (Integer userId, SmdTableViewDefinition tableViewDefinition) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();
        SmdTableViewDefinition newTableView = new SmdTableViewDefinition();

        // 1. Create the new SMD TableView in the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_database_id", tableViewDefinition.getDatabaseId(), Types.INTEGER)
                .addValue("_schema_id", tableViewDefinition.getSchemaId(), Types.INTEGER)
                .addValue("_table_view_id", tableViewDefinition.getTableViewId(), Types.INTEGER)
                .addValue("_class_id", tableViewDefinition.getClassId(), Types.INTEGER)
                .addValue("_table_view_name", tableViewDefinition.getTableViewName(), Types.VARCHAR)
                .addValue("_table_view_shortdesc", tableViewDefinition.getTableViewShortDesc(), Types.VARCHAR);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            smdCrudResult.setReturnId((Integer) out.get("_return_id"));
        } catch (UncategorizedSQLException e) {
            smdCrudResult.setResultStatus(-1);
            SQLException sqle = e.getSQLException();
            smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
            smdCrudResult.setSqlErrorState(sqle.getSQLState());
            smdCrudResult.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            smdCrudResult.setResultStatus(-1);
            smdCrudResult.setSqlErrorCode(0);
            smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
            smdCrudResult.setSqlErrorMessage(e.getMessage());
        }

        // 2. get details of the new SMD TableView
        if (smdCrudResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view");      // TODO: the stored proc should be updated to use OUT params
            in = new MapSqlParameterSource().addValue("_smd_table_view_id", smdCrudResult.getReturnId(), Types.INTEGER);

            try {
                Map<String, Object> out = jdbcCall.execute(in);

                ArrayList tableViewList = (ArrayList) out.get("#result-set-1");
                LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) tableViewList.get(0);

                newTableView.setTableViewId((Integer) tableViewInfo.get("smd_table_view_id"));
                newTableView.setClassId((Integer) tableViewInfo.get("class_id"));
                newTableView.setClassName(tableViewInfo.get("class_name").toString());
                newTableView.setTableViewName(tableViewInfo.get("smd_table_view_name").toString());
                newTableView.setSchemaId((Integer) tableViewInfo.get("smd_schema_id"));
                newTableView.setSchemaName(tableViewInfo.get("smd_schema_name").toString());
                newTableView.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
                newTableView.setObjectGUID(tableViewInfo.get("object_guid").toString());

                DlMetaDataContext mdContext = new DlMetaDataContext();
                newTableView.setMdContext(mdContext.generateMdContextList(tableViewInfo.get("md_context").toString()));

                smdCrudResult.addResultData("table_view_summary", newTableView);

            } catch (UncategorizedSQLException e) {
                smdCrudResult.setResultStatus(-1);
                SQLException sqle = e.getSQLException();
                smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
                smdCrudResult.setSqlErrorState(sqle.getSQLState());
                smdCrudResult.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                smdCrudResult.setResultStatus(-1);
                smdCrudResult.setSqlErrorCode(0);
                smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
                smdCrudResult.setSqlErrorMessage(e.getMessage());
            }

        }

        return smdCrudResult;
    }

    /**
     * Gets the list of Tables/Views for the SMD database definition identified by databaseId
     * @param databaseId
     * @return
     */
    public ArrayList<SmdTableViewDefinition> getSmdDbTableViewList (Integer databaseId) {
        ArrayList<SmdTableViewDefinition> tableViewResults = new ArrayList<SmdTableViewDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_by_db");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", databaseId, Types.INTEGER);
        Map<String, Object> smdTablesViews = jdbcCall.execute(in);

        ArrayList tableViewList = (ArrayList) smdTablesViews.get("#result-set-1");
        for (Object listElement : tableViewList) {
            LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdTableViewDefinition tableViewDefinition = new SmdTableViewDefinition(
                    (Integer) tableViewInfo.get("smd_table_view_id"),
                    (Integer) tableViewInfo.get("class_id"),
                    (Integer) tableViewInfo.get("smd_schema_id"),
                    tableViewInfo.get("smd_table_view_name").toString());
            tableViewDefinition.setClassName(tableViewInfo.get("class_name").toString());
            tableViewDefinition.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
            tableViewDefinition.setSchemaName(tableViewInfo.get("smd_schema_name").toString());

            tableViewResults.add(tableViewDefinition);
        }
        return tableViewResults;
    }

    /**
     * Gets details of the Table/View entity identified by tableViewId
     * @param tableViewId
     * @return
     */
    public SmdTableViewDefinition getTableViewDetail (Integer tableViewId) {
        // 1. get the core details for the table/view
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_table_view_id", tableViewId, Types.INTEGER);
        Map<String, Object> rslt = jdbcCall.execute(in);

        ArrayList tableViewList = (ArrayList) rslt.get("#result-set-1");
        LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) tableViewList.get(0);

        SmdTableViewDefinition tableViewDefinition = new SmdTableViewDefinition(
                (Integer) tableViewInfo.get("smd_table_view_id"),
                (Integer) tableViewInfo.get("class_id"),
                (Integer) tableViewInfo.get("smd_schema_id"),
                tableViewInfo.get("smd_table_view_name").toString()
        );
        if (tableViewInfo.get("smd_table_view_shortdesc") != null) {
            tableViewDefinition.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
        }
        tableViewDefinition.setClassName(tableViewInfo.get("class_name").toString());
        tableViewDefinition.setSchemaName(tableViewInfo.get("smd_schema_name").toString());
        tableViewDefinition.setObjectGUID(tableViewInfo.get("object_guid").toString());
        tableViewDefinition.setColumnCustomFormId((Integer) tableViewInfo.get("custom_form_id_col"));
        tableViewDefinition.setColumnCustomFormName(tableViewInfo.get("custom_form_name_col").toString());

        // 1.1. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        tableViewDefinition.setMdContext(mdContext.generateMdContextList(tableViewInfo.get("md_context").toString()));

        // 1.2. add the custom form information
        DlCustomForm customForm = new DlCustomForm(
                (Integer) tableViewInfo.get("custom_form_id"),
                tableViewInfo.get("custom_form_name").toString(),
                tableViewInfo.get("schema_definition").toString(),
                tableViewInfo.get("form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        if (tableViewInfo.get("custom_form_data") == null)
            customForm.setFormData("{}");
        else
            customForm.setFormData(tableViewInfo.get("custom_form_data").toString());
        tableViewDefinition.setCustomForm(customForm);

        // 2. get the column list
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_columns");
        rslt = jdbcCall.execute(in);

        ArrayList columnList = (ArrayList) rslt.get("#result-set-1");

        for(Object listElement:columnList) {
            LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdColumnDefinition columnDefinition = new SmdColumnDefinition(
                    (Integer) columnInfo.get("smd_column_id"),
                    columnInfo.get("smd_column_name").toString(),
                    (Integer) columnInfo.get("smd_column_ordinal"),
                    columnInfo.get("smd_column_datatype").toString());
            columnDefinition.setClassId((Integer) columnInfo.get("class_id"));
            if (columnInfo.get("smd_column_shortdesc") != null) {
                columnDefinition.setColumnShortDesc(columnInfo.get("smd_column_shortdesc").toString());
            }
            columnDefinition.setPrimaryKey((Boolean) columnInfo.get("smd_primary_key"));
            // 2.1. add the data sensitivity tag
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag();
            dsTag.setDlDsTagId((Integer) columnInfo.get("data_sensitivity_id"));
            dsTag.setDlDsLevel(columnInfo.get("data_sensitivity_level").toString());
            dsTag.setDlDsName(columnInfo.get("data_sensitivity_name").toString());
            columnDefinition.setDlDsTag(dsTag);

            tableViewDefinition.addColumnDefinition(columnDefinition);
        }

        // 3. get the summary data sensitivity for the table
        in = new MapSqlParameterSource().addValue("_object_id", tableViewId, Types.INTEGER)
                .addValue("_class_id", tableViewDefinition.getClassId(), Types.INTEGER);
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_data_sensitivity_multi");
        rslt = jdbcCall.execute(in);

        ArrayList dsTagList = (ArrayList) rslt.get("#result-set-1");

        for(Object listElement:dsTagList) {
            LinkedCaseInsensitiveMap dsTagInfo = (LinkedCaseInsensitiveMap) listElement;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag(
                    (Integer) dsTagInfo.get("data_sensitivity_id"),
                    dsTagInfo.get("data_sensitivity_level").toString(),
                    dsTagInfo.get("data_sensitivity_name").toString()
            );
            dsTag.setCreateMode((Integer) dsTagInfo.get("create_mode"));
            tableViewDefinition.addDlDsTag(dsTag);
        }

        return tableViewDefinition;
    }

    /**
     * Calls stored procedure read_smd_table_view to get summary information about the table/view identified by tableViewId
     * and returns an {@link SmdTableViewDefinition} object. The information is read
     *
     * @param  tableViewId  Unique identifier of the table/view
     * @return SmdTableViewDefinition
     */
    public SmdTableViewDefinition getTableViewSummary (Integer tableViewId) {
        // 1. get the core details for the table/view
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_table_view_id", tableViewId, Types.INTEGER);
        Map<String, Object> rslt = jdbcCall.execute(in);

        ArrayList tableViewList = (ArrayList) rslt.get("#result-set-1");
        LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) tableViewList.get(0);

        SmdTableViewDefinition tableViewDefinition = new SmdTableViewDefinition(
                (Integer) tableViewInfo.get("smd_table_view_id"),
                (Integer) tableViewInfo.get("class_id"),
                (Integer) tableViewInfo.get("smd_schema_id"),
                tableViewInfo.get("smd_table_view_name").toString()
        );
        tableViewDefinition.setTableViewShortDesc(tableViewInfo.get("smd_table_view_shortdesc").toString());
        tableViewDefinition.setClassName(tableViewInfo.get("class_name").toString());
        tableViewDefinition.setSchemaName(tableViewInfo.get("smd_schema_name").toString());
        tableViewDefinition.setColumnCustomFormId((Integer) tableViewInfo.get("custom_form_id_col"));
        tableViewDefinition.setColumnCustomFormName(tableViewInfo.get("custom_form_name_col").toString());

        // 1.1. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        tableViewDefinition.setMdContext(mdContext.generateMdContextList(tableViewInfo.get("md_context").toString()));

        // 2. get the custom form details
        DlCustomForm customForm = new DlCustomForm(
                (Integer) tableViewInfo.get("custom_form_id"),
                tableViewInfo.get("custom_form_name").toString(),
                tableViewInfo.get("schema_definition").toString(),
                tableViewInfo.get("form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        if (tableViewInfo.get("custom_form_data") == null)
            customForm.setFormData("{}");
        else
            customForm.setFormData(tableViewInfo.get("custom_form_data").toString());
        tableViewDefinition.setCustomForm(customForm);

        return tableViewDefinition;
    }

    /**
     * Updates the short description for an SMD Table/View
     * @param userId
     * @param tableViewId
     * @param tableViewShortDesc
     * @return
     */
    public Integer setTableViewSummary (Integer userId, Integer tableViewId, Integer classId, String tableViewShortDesc) {
        Integer dbResult = 0;
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_database_id", 0, Types.INTEGER)     // not used for this update action
                .addValue("_schema_id", 0, Types.INTEGER)       // not used for this update action
                .addValue("_table_view_id", tableViewId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_table_view_name", "not used", Types.VARCHAR)    // not used for this update action
                .addValue("_table_view_shortdesc", tableViewShortDesc, Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        return dbResult;
    }

    /**
     * Updates the short description for an SMD Table/View
     * @param userId
     * @param tableViewId
     * @param tableViewShortDesc
     * @return
     */
    public Integer setTableViewShortDesc (Integer userId, Integer tableViewId, Integer classId, String tableViewShortDesc) {
        Integer dbResult = 0;
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view_shortdesc");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", tableViewId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_object_shortdesc", tableViewShortDesc, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        dbResult = (Integer) out.get("_return_id");
        return dbResult;
    }

    /**
     * Updates the custom form used for columns in an SMD Table/View definition.
     * @param userId    The userId of the user making the change
     * @param objectId  The objectId of the SMD Database definition for which the custom form will be changed
     * @param classId   The classId of the SMD Database definition for which the custom form will be changed
     * @param customFormId  The customFormId of the new custom form for the SMD Database definition
     * @return
     */
    public Integer setTableViewCustomForm(Integer userId, Integer objectId, Integer classId, Integer customFormId) {
        Integer dbResult = 0;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_custom_form_id", customFormId, Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        return dbResult;
    }

    /**
     * Returns an SmdColumnDefinition object containing the core details of a column
     * @param columnId
     * @return SmdColumnDefinition
     */
    public SmdColumnDefinition getColumnDetail (Integer columnId) {
        // 1. get the core details for the column
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_column");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_column_id", columnId, Types.INTEGER);
        Map<String, Object> rslt = jdbcCall.execute(in);

        ArrayList columnDetails = (ArrayList) rslt.get("#result-set-1");
        LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) columnDetails.get(0);
        DlMetaDataContext mdContext = new DlMetaDataContext();

        // 2. create the columnDefinition object and add core attributes
        SmdColumnDefinition columnDefinition = new SmdColumnDefinition(
                (Integer) columnInfo.get("smd_column_id"),
                columnInfo.get("smd_column_name").toString(),
                (Integer) columnInfo.get("smd_column_ordinal"),
                columnInfo.get("smd_column_datatype").toString()
        );

        columnDefinition.setClassId((Integer) columnInfo.get("class_id"));
        columnDefinition.setParentObjectId((Integer) columnInfo.get("smd_table_view_id"));
        columnDefinition.setParentClassId((Integer) columnInfo.get("parent_class_id"));
        columnDefinition.setPrimaryKey((Boolean) columnInfo.get("smd_primary_key"));
        columnDefinition.setColumnShortDesc(columnInfo.get("smd_column_shortdesc").toString());
        if (columnInfo.get("smd_table_view_name").toString() != null)
            columnDefinition.setTableViewName(columnInfo.get("smd_table_view_name").toString());
        else
            columnDefinition.setTableViewName("");
        if (columnInfo.get("smd_table_view_shortdesc").toString() != null)
            columnDefinition.setTableViewShortDesc(columnInfo.get("smd_table_view_shortdesc").toString());
        else
            columnDefinition.setTableViewShortDesc("");
        columnDefinition.setMdContext(mdContext.generateMdContextList(columnInfo.get("md_context").toString()));

        // 3. add the custom form
        DlCustomForm customForm = new DlCustomForm (
                (Integer) columnInfo.get("custom_form_id"),
                columnInfo.get("custom_form_name").toString(),
                columnInfo.get("schema_definition").toString(),
                columnInfo.get("form_definition").toString()
        );
        customForm.setObjectId(columnDefinition.getColumnId());
        customForm.setClassId(columnDefinition.getClassId());
        customForm.setParentObjectId(columnDefinition.getParentObjectId());
        customForm.setParentClassId(columnDefinition.getParentClassId());

        customForm.prepareSchemaDefinition(dataSource);
        if (columnInfo.get("custom_form_data") == null)
            customForm.setFormData("{}");
        else
            customForm.setFormData(columnInfo.get("custom_form_data").toString());
        columnDefinition.setCustomForm(customForm);

        // 4. add the data sensitivity tag
        DlDataSensitivityTag dlDsTag = new DlDataSensitivityTag();
        dlDsTag.setDlDsTagId((Integer) columnInfo.get("data_sensitivity_id"));
        dlDsTag.setDlDsName(columnInfo.get("data_sensitivity_name").toString());
        dlDsTag.setDlDsLevel(columnInfo.get("data_sensitivity_level").toString());

        columnDefinition.setDlDsTag(dlDsTag);

        return columnDefinition;
    }

    /**
     * Updates the custom form used for columns in an SMD Table/View definition.
     * @param userId
     * @param objectId
     * @param customFormId
     * @return
     */
    public Integer setColumnCustomForm(Integer userId, Integer objectId, Integer classId, Integer customFormId) {
        Integer dbResult = 0;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER)
                .addValue("_custom_form_id", customFormId, Types.VARCHAR);
        Map<String, Object> rslt = jdbcCall.execute(in);

        return dbResult;
    }

    /**
     * Gets the list of Relationshipos for the SMD database definition identified by databaseId
     * @param databaseId
     * @return
     */
    public ArrayList<SmdRelationshipDefinition> getSmdDbRelationshipList(Integer databaseId) {
        ArrayList<SmdRelationshipDefinition> relationshipList = new ArrayList<SmdRelationshipDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_relationship_by_db");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_database_id", databaseId, Types.INTEGER);
        Map<String, Object> dbRelationships = jdbcCall.execute(in);

        ArrayList dbResults = (ArrayList) dbRelationships.get("#result-set-1");
        for (Object listElement : dbResults) {
            LinkedCaseInsensitiveMap relationshipInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdRelationshipDefinition relationshipDefinition = new SmdRelationshipDefinition(
                    (Integer) relationshipInfo.get("smd_relationship_id"),
                    (Integer) relationshipInfo.get("class_id"),
                    (Integer) relationshipInfo.get("create_mode"),
                    relationshipInfo.get("smd_relationship_name").toString(),
                    (Integer) relationshipInfo.get("parent_table_view_id"),
                    (Integer) relationshipInfo.get("parent_class_id"),
                    relationshipInfo.get("parent_table_view_name").toString(),
                    (Integer) relationshipInfo.get("child_table_view_id"),
                    (Integer) relationshipInfo.get("child_class_id"),
                    relationshipInfo.get("child_table_view_name").toString()
            );
            relationshipDefinition.setRelationshipShortDesc(relationshipInfo.get("smd_relationship_shortdesc").toString());

            relationshipList.add(relationshipDefinition);
        }

        return relationshipList;
    }

    /**
     * Reads details of the SMD relationship identified by relationshipId
     * @param relationshipId
     * @return
     */
    public SmdRelationshipDefinition getRelationshipDetail (Integer relationshipId) {
        // TODO: update for multiple column pairs
        // get the core details for the relationship
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_relationship");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_relationship_id", relationshipId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        SmdRelationshipDefinition relationshipDefinition = new SmdRelationshipDefinition();

        // 1. Get relationship attributes
        relationshipDefinition.setRelationshipId(relationshipId);
        relationshipDefinition.setRelationshipName((String) out.get("_smd_relationship_name"));
        relationshipDefinition.setCreateMode((Integer) out.get("_create_mode"));
        relationshipDefinition.setParentTableViewId((Integer) out.get("_parent_table_view_id"));
        relationshipDefinition.setParentTableViewName((String) out.get("_parent_table_view_name"));
        relationshipDefinition.setParentTableViewSchemaName((String) out.get("_parent_schema_name"));
        relationshipDefinition.setChildTableViewId((Integer) out.get("_child_table_view_id"));
        relationshipDefinition.setChildTableViewName((String) out.get("_child_table_view_name"));
        relationshipDefinition.setChildTableViewSchemaName((String) out.get("_child_schema_name"));
        relationshipDefinition.setRelationshipShortDesc((String) out.get("_smd_relationship_shortdesc"));

        // 2. get column pairs for relationship
        ArrayList columnPairList = (ArrayList) out.get("#result-set-1");
        for(Object listElement:columnPairList) {
            LinkedCaseInsensitiveMap columnPairInfo = (LinkedCaseInsensitiveMap) listElement;
            SmdRelationshipColumnPair columnPair = new SmdRelationshipColumnPair(
                    (Integer) columnPairInfo.get("parent_column_id"),
                    columnPairInfo.get("parent_column_name").toString(),
                    columnPairInfo.get("parent_column_datatype").toString(),
                    (Boolean) columnPairInfo.get("parent_column_pk"),
                    (Integer) columnPairInfo.get("child_column_id"),
                    columnPairInfo.get("child_column_name").toString(),
                    columnPairInfo.get("child_column_datatype").toString(),
                    (Boolean) columnPairInfo.get("child_column_pk")
            );

            relationshipDefinition.addColumnPair(columnPair);
        }


        return relationshipDefinition;
    }

    /**
     * Deletes an SMD relationship from the DL DB. Only manually created relationships can be deleted.
     * @param relationshipId
     * @return
     */
    public Integer deleteRelationship (Integer relationshipId) {
        Integer resultStatus = 0;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_smd_relationship");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_smd_relationship_id", relationshipId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        resultStatus = (Integer) out.get("_result_status");

        return resultStatus;
    }

    /**
     * Updates the short description for an SMD Column
     * @param userId
     * @param columnId
     * @param columnShortDesc
     * @return
     */
    public Integer setColumnSummary (Integer userId, Integer columnId, String columnShortDesc) {
        Integer dbResult = 0;
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_column_id", columnId, Types.INTEGER)
                .addValue("_column_shortdesc", columnShortDesc, Types.VARCHAR)
                .addValue("_data_sensitivity_id", 0, Types.INTEGER);
        Map<String, Object> rslt = jdbcCall.execute(in);

        // TODO: should be better error handling here
        return dbResult;
    }

    /**
     * Writes a new SMD column definition to the DL DB using metadata specified via the DL API
     * @param userId
     * @param columnDefinition
     * @return
     */
    public SmdCrudResult writeSmdColumn (Integer userId, SmdColumnDefinition columnDefinition) {
        SmdCrudResult smdCrudResult = new SmdCrudResult();
        SmdColumnDefinition newColumn = new SmdColumnDefinition();

        // 1. Create the new SMD TableView in the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_table_view_id", columnDefinition.getParentObjectId(), Types.INTEGER)
                .addValue("_column_id", columnDefinition.getColumnId(), Types.INTEGER)
                .addValue("_class_id", columnDefinition.getClassId(), Types.INTEGER)
                .addValue("_column_name", columnDefinition.getColumnName(), Types.VARCHAR)
                .addValue("_column_shortdesc", columnDefinition.getColumnShortDesc(), Types.VARCHAR)
                .addValue("_column_ordinal", columnDefinition.getColumnOrdinal(), Types.INTEGER)
                .addValue("_primary_key", columnDefinition.getPrimaryKey(), Types.BOOLEAN)
                .addValue("_column_datatype", columnDefinition.getColumnDatatype(), Types.VARCHAR);

        try {
            Map<String, Object> out = jdbcCall.execute(in);
            smdCrudResult.setReturnId((Integer) out.get("_return_id"));
        } catch (UncategorizedSQLException e) {
            smdCrudResult.setResultStatus(-1);
            SQLException sqle = e.getSQLException();
            smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
            smdCrudResult.setSqlErrorState(sqle.getSQLState());
            smdCrudResult.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            smdCrudResult.setResultStatus(-1);
            smdCrudResult.setSqlErrorCode(0);
            smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
            smdCrudResult.setSqlErrorMessage(e.getMessage());
        }

        // 2. get details of the new SMD TableView
        if (smdCrudResult.getResultStatus() == 1) {
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_column");      // TODO: the stored proc should be updated to use OUT params
            in = new MapSqlParameterSource().addValue("_smd_column_id", smdCrudResult.getReturnId(), Types.INTEGER);

            try {
                Map<String, Object> out = jdbcCall.execute(in);

                ArrayList columnDetails = (ArrayList) out.get("#result-set-1");
                LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) columnDetails.get(0);

                newColumn.setColumnId(smdCrudResult.getReturnId());
                newColumn.setClassId((Integer) columnInfo.get("class_id"));
                newColumn.setParentObjectId((Integer) columnInfo.get("smd_table_view_id"));
                newColumn.setParentClassId((Integer) columnInfo.get("parent_class_id"));
                newColumn.setColumnName(columnInfo.get("smd_column_name").toString());
                newColumn.setColumnShortDesc(columnInfo.get("smd_column_shortdesc").toString());
                newColumn.setPrimaryKey((Boolean) columnInfo.get("smd_primary_key"));

                DlMetaDataContext mdContext = new DlMetaDataContext();
                newColumn.setMdContext(mdContext.generateMdContextList(columnInfo.get("md_context").toString()));

                smdCrudResult.addResultData("column_summary", newColumn);

            } catch (UncategorizedSQLException e) {
                smdCrudResult.setResultStatus(-1);
                SQLException sqle = e.getSQLException();
                smdCrudResult.setSqlErrorCode(sqle.getErrorCode());
                smdCrudResult.setSqlErrorState(sqle.getSQLState());
                smdCrudResult.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                smdCrudResult.setResultStatus(-1);
                smdCrudResult.setSqlErrorCode(0);
                smdCrudResult.setSqlErrorState("Unknown SQL Error State.");
                smdCrudResult.setSqlErrorMessage(e.getMessage());
            }

        }

        return smdCrudResult;
    }

    /**
     * Returns the list of metadata objects associated with the SMD metadata object idenfited by objectId and classId.
     * The metadata objects are returned as an ArrayList of AssociateObject intances.
     * @param objectId
     * @param classId
     * @return
     */
    public ArrayList<AssociatedObject> getAssociatedObjects (Integer objectId, Integer classId) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> rslt = jdbcCall.execute(in);

        ArrayList<AssociatedObject> associations = new ArrayList<>();
        ArrayList dbResultList = (ArrayList) rslt.get("#result-set-1");

        for (Object assocDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the md_context of the associated object
            DlMetaDataContext mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associations.add(assocObject);
        }
        return associations;

    }
}
