/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.DlSqlError;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to return results from a DL DB CRUD action to the invoking Spring Controller function.
 */
public class SmdCrudResult {
    private Integer resultStatus;
    private Integer returnId;
    private Map<String, Object> resultData;
    private DlSqlError sqlError;

    /**
     * Default constructor. No parameters.
     */
    public SmdCrudResult() {
        this.resultStatus = 1;
        this.returnId = 0;
        this.resultData = new HashMap<>();
        this.sqlError = new DlSqlError();
    }

    public Integer getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(Integer resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Integer getReturnId() {
        return returnId;
    }

    public void setReturnId(Integer returnId) {
        this.returnId = returnId;
    }

    public void addResultData(String resultTag, Object resultData) {
        this.resultData.put(resultTag, resultData);
    }

    public Map<String, Object> getResultData() {
        return resultData;
    }

    public void setResultData(Map<String, Object> resultData) {
        this.resultData = resultData;
    }

    public DlSqlError getSqlError() {
        return sqlError;
    }

    public void setSqlError(DlSqlError sqlError) {
        this.sqlError = sqlError;
    }

    public void setSqlErrorCode(Integer sqlErrorCode) {
        this.sqlError.setSqlErrorCode(sqlErrorCode);
    }

    public void setSqlErrorState(String sqlErrorState) {
        this.sqlError.setSqlErrorState(sqlErrorState);
    }

    public void setSqlErrorMessage(String sqlErrorMessage) {
        this.sqlError.setSqlErrorMessage(sqlErrorMessage);
    }
}
