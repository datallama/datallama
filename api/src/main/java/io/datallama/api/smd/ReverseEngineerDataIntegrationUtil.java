/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import io.datallama.api.common.MoClassConstants;
import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.ReverseEngineerRunCmn;
import io.swagger.util.Json;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class ReverseEngineerDataIntegrationUtil {
    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;

    /**
     * Default Constructor with DriverManagerDataSource and userId parameters.
     * @param dataSource
     * @param userId
     */
    public ReverseEngineerDataIntegrationUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Executes an offline reverse engineering run from a Data Llama Data Integration Project SDM specification file identified by dlDiSmdJson.
     * @param objectId
     * @param dlDiSmdJson
     * @return
     */
    public RevEngResultCmn executeOfflineRevEngDlDiJson(Integer objectId, String dlDiSmdJson) {

        DataIntegrationProject diProject = new DataIntegrationProject();
        diProject.setObjectId(objectId);
        diProject.setClassId(moClassConstants.DATA_INTEGRATION_PROJECT);
        ObjectMapper objectMapper = new ObjectMapper();

        // 1. create a new reverse engineer run record
        ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn(dataSource, objectId, moClassConstants.DATA_INTEGRATION_PROJECT);
        Path path = Paths.get(dlDiSmdJson);
        revEngRun.setInputSource(path.getFileName().toString());
        RevEngResultCmn revEngResult = revEngRun.generateRevEngRun();

        // 2. check dlDiSmdJson file is a valid file
        File jsonFile = null;
        if (revEngResult.getResultStatus() == 1) {

            jsonFile = new File(dlDiSmdJson);
            if (!(jsonFile.exists() && !jsonFile.isDirectory())) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(200310);
                revEngResult.setGeneralErrorMessage(String.format("DL Data Integration Project JSON file %s does not exist or is a diretory.", jsonFile));
                System.out.println(String.format("Error: DL Data Integration Project JSON file %s does not exist or is a diretory.", jsonFile));
            }
        }

        // 3. Parse the uploaded JSON file and check that it's a valid DlDbSmd JSON file
        JsonNode rootNode = null;
        if (revEngResult.getResultStatus() == 1 && jsonFile != null) {
            try {
                rootNode = objectMapper.readTree(jsonFile);
            } catch (Exception e) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200407);
                revEngResult.setGeneralErrorMessage("Error encountered when trying to parse the DlDiSmd JSON file. Check that the file is a valid JSON file.");
            }
        }

        if (revEngResult.getResultStatus() == 1) {
            String dlJsonTypeName = "";
            JsonNode dlJsonTypeNode = rootNode.get("dl_json_type");

            if (dlJsonTypeNode != null) {
                dlJsonTypeName = dlJsonTypeNode.asText().toUpperCase();
                if(!dlJsonTypeName.equals("DLDISMD")) {
                    revEngResult.setResultStatus(-1);
                    revEngResult.setErrorNumber(-200408);
                    revEngResult.setGeneralErrorMessage("The uploaded file is not a valid DlDiSmd JSON file. The dl_json_type element is missing or incorrectly formed.");
                }
            }
            else {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200409);
                revEngResult.setGeneralErrorMessage("The uploaded file is not a valid DlDiSmd JSON file. The dl_json_type element is missing.");
            }
        }

        // 4. process the nodes in the DlDiSmd JSON file into a DataIntegrationProject hierarchy
        if (revEngResult.getResultStatus() == 1) {

            // 4.1. Add basic DI project information
            diProject.setProjectName(rootNode.get("project_name").asText());

            // 4.2. process DI jobs that are not contained within a package
            ArrayNode jobListNode = (ArrayNode) rootNode.get("job_list");
            if (jobListNode != null) {
                Iterator<JsonNode> jobNodes = jobListNode.elements();

                // process the top level jobs and recurse into job hierarchies if they have been defined
                while (jobNodes.hasNext()) {
                    JsonNode currentJobNode = jobNodes.next();
                    String jobName = currentJobNode.get("job_name").asText();

                    if (jobName == null || jobName.length() == 0) {
                        revEngResult.setResultStatus(-1);
                        revEngResult.setErrorNumber(-200404);
                        revEngResult.setGeneralErrorMessage("Invalid job definition in DlDiSmd JSON file. Job name is missing or blank.");
                    } else {
                        DataIntegrationJob diJob = new DataIntegrationJob();
                        diJob.setJobName(jobName);
                        diJob.setJobShortDesc(currentJobNode.get("shortdesc").asText());

                        processJobList(diJob, currentJobNode);
                        diProject.addDataIntegrationJob(diJob);
                    }

                }
            }

            // 4.3. process DI packages
            ArrayNode pkgListNode = (ArrayNode) rootNode.get("package_list");
            if (pkgListNode != null) {
                Iterator<JsonNode> pkgNodes = pkgListNode.elements();

                // recurse into the package and job hierarchies if they have been defined
                while (pkgNodes.hasNext()) {
                    JsonNode currentPkgNode = pkgNodes.next();
                    String pkgName = currentPkgNode.get("package_name").asText();

                    if (pkgName == null || pkgName.length() == 0) {
                        revEngResult.setResultStatus(-1);
                        revEngResult.setErrorNumber(-200405);
                        revEngResult.setGeneralErrorMessage("Invalid package definition in DlDiSmd JSON file. Package name is missing or blank.");
                    } else {
                        DataIntegrationPackage diPackage = new DataIntegrationPackage();
                        diPackage.setPackageName(pkgName);
                        diPackage.setPackageShortDesc(currentPkgNode.get("shortdesc").asText());

                        revEngResult = processPackage(diPackage, currentPkgNode);
                        diProject.addDataIntegrationPackage(diPackage);
                    }
                }
            }

        }

        // 5. persist the DataIntegrationProject to the DL DB
        if (revEngResult.getResultStatus() == 1) {
            revEngResult = persistDataIntegrationProject(diProject, revEngRun);
        }

        // 6. update the reverse engineer run details in the DL DB
        if (revEngResult.getResultStatus() == 1) {
            revEngResult.setResultStatus(2);
            revEngResult.setGeneralErrorMessage("Data Integration Project offline reverse engineer run completed successfully.");
            revEngRun.closeRevEngRun(revEngResult); // TODO: catch close action error
        }

        return revEngResult;
    }

    /**
     * Process recursively all sub jobs defined for data integration job, i.e. the entries in the job_list array
     * @param diParentJob
     * @param jobNode
     * @return
     */
    private RevEngResultCmn processJobList (DataIntegrationJob diParentJob, JsonNode jobNode) {
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        ArrayNode jobListNode = (ArrayNode) jobNode.get("job_list");

        if (jobListNode != null) {
            Iterator<JsonNode> jobNodes = jobListNode.elements();

            // process the top level jobs and recurse into job hierarchies if they have been defined
            while (jobNodes.hasNext()) {
                JsonNode currentJobNode = jobNodes.next();
                String jobName = currentJobNode.get("job_name").asText();

                if (jobName == null || jobName.length() == 0) {
                    revEngResult.setResultStatus(-1);
                    revEngResult.setErrorNumber(-200405);
                    revEngResult.setGeneralErrorMessage("Invalid job definition in DlDiSmd JSON file. Job name is missing or blank.");
                } else {
                    DataIntegrationJob diSubJob = new DataIntegrationJob();
                    diSubJob.setJobName(jobName);
                    diSubJob.setJobShortDesc(currentJobNode.get("shortdesc").asText());
                    revEngResult = processJobList(diSubJob, currentJobNode);
                    diParentJob.addDataIntegrationJob(diSubJob);
                }
            }
        }

        return revEngResult;
    }

    /**
     * Process recursively the package and job hierarchies defined in a DI package
     * @param diParentPackage
     * @param packageNode
     * @return
     */
    private RevEngResultCmn processPackage (DataIntegrationPackage diParentPackage, JsonNode packageNode) {
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // 1. process the package job list
        ArrayNode jobListNode = (ArrayNode) packageNode.get("job_list");

        Iterator<JsonNode> jobNodes = jobListNode.elements();
        // process the top level jobs and recurse into job hierarchies if they have been defined
        while (jobNodes.hasNext()) {
            JsonNode currentJobNode = jobNodes.next();
            String jobName = currentJobNode.get("job_name").asText();

            if (jobName == null || jobName.length() == 0) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200404);
                revEngResult.setGeneralErrorMessage("Invalid job definition in DlDiSmd JSON file. Job name is missing or blank.");
            } else {
                DataIntegrationJob diJob = new DataIntegrationJob();
                diJob.setJobName(jobName);
                diJob.setJobShortDesc(currentJobNode.get("shortdesc").asText());

                processJobList(diJob, currentJobNode);
                diParentPackage.addDataIntegrationJob(diJob);
            }
        }

        // 2. process the package package list
        ArrayNode pkgListNode = (ArrayNode) packageNode.get("package_list");
        Iterator<JsonNode> pkgPackageNodes = pkgListNode.elements();

        // recurse into the package and job hierarchies if they have been defined
        while (pkgPackageNodes.hasNext()) {
            JsonNode currentPkgNode = pkgPackageNodes.next();
            String pkgName = currentPkgNode.get("package_name").asText();

            if (pkgName == null || pkgName.length() == 0) {
                revEngResult.setResultStatus(-1);
                revEngResult.setErrorNumber(-200405);
                revEngResult.setGeneralErrorMessage("Invalid package definition in DlDiSmd JSON file. Package name is missing or blank.");
            } else {
                DataIntegrationPackage diPackage = new DataIntegrationPackage();
                diPackage.setPackageName(pkgName);
                diPackage.setPackageShortDesc(currentPkgNode.get("shortdesc").asText());

                revEngResult = processPackage(diPackage, currentPkgNode);

                diParentPackage.addDataIntegrationPackage(diPackage);
            }
        }

        return revEngResult;
    }

    /**
     * Persists a DataIntegrationProject to the DL DB.
     * @param diProject
     * @return
     */
    private RevEngResultCmn persistDataIntegrationProject (DataIntegrationProject diProject, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // 1. persist the list of jobs that are not contained within packages
        ArrayList<DataIntegrationJob> jobList = diProject.getDataIntegrationJobList();
        for (DataIntegrationJob diJob : jobList) {
            persistDataIntegrationJob (diProject, 0, diJob, revEngRun);
        }

        // 2. persist the list of packages in the data integration project
        ArrayList<DataIntegrationPackage> packageList = diProject.getDataIntegrationPackageList();
        for (DataIntegrationPackage diPackage : packageList) {
            persistDataIntegrationPackage(diProject, 0, diPackage, revEngRun);
        }


        return revEngResult;
    }

    /**
     * Persists a data integration package to the DL DB, and recursively persists all
     * @param diProject
     * @param diPackage
     * @param revEngRun
     * @return
     */
    private RevEngResultCmn persistDataIntegrationPackage (DataIntegrationProject diProject, Integer parentElementObjectId,
                                                           DataIntegrationPackage diPackage, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // 1. persist the package
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataintegration_element_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_class_id", MoClassConstants.DATA_INTEGRATION_PACKAGE, Types.INTEGER)
                .addValue("_parent_object_id", diProject.getObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", diProject.getClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", parentElementObjectId, Types.INTEGER)
                .addValue("_object_name", diPackage.getPackageName(), Types.VARCHAR)
                .addValue("_object_shortdesc", diPackage.getPackageShortDesc(), Types.VARCHAR)
                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        Integer newObjectId = (Integer) out.get("_return_id");
        diPackage.setObjectId(newObjectId);
        diPackage.setClassId(MoClassConstants.DATA_INTEGRATION_PACKAGE);

        // 2. persist the job hierarchy for this package
        ArrayList<DataIntegrationJob> diJobList = diPackage.getDataIntegrationJobList();

        for (DataIntegrationJob diJob : diJobList) {
            persistDataIntegrationJob(diProject, diPackage.getObjectId(), diJob, revEngRun);
        }

        return revEngResult;
    }

    /**
     * Persists a Data Integration job to the DL DB and recursively persists all sub-jobs
     * @param diProject
     * @param parentElementObjectId
     * @param diJob
     * @param revEngRun
     * @return
     */
    private RevEngResultCmn persistDataIntegrationJob (DataIntegrationProject diProject, Integer parentElementObjectId,
                                                       DataIntegrationJob diJob, ReverseEngineerRunCmn revEngRun) {
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        // 1. persist this data integration job
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_dataintegration_element_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", userId, Types.INTEGER)
                .addValue("_class_id", MoClassConstants.DATA_INTEGRATION_JOB, Types.INTEGER)
                .addValue("_parent_object_id", diProject.getObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", diProject.getClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", parentElementObjectId, Types.INTEGER)
                .addValue("_object_name", diJob.getJobName(), Types.VARCHAR)
                .addValue("_object_shortdesc", diJob.getJobShortDesc(), Types.VARCHAR)
                .addValue("_reveng_run_id", revEngRun.getRevEngRunId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        Integer newObjectId = (Integer) out.get("_return_id");

        diJob.setObjectId(newObjectId);

        // 2. persist any sub-jobs associated with this job
        for (DataIntegrationJob diSubJob : diJob.getDataIntegrationJobList()) {
            persistDataIntegrationJob(diProject, newObjectId, diSubJob, revEngRun);
        }

        return revEngResult;
    }
}
