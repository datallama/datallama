/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.RevEngReconciliation;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for Database reverse engineering runs. The class is responsible for instantiating the appropriate Reverse Engineering connector class, invoking the Interface functions and saving the generated information to the DL DB.
 */
public class ReverseEngineerUtils {

    private DriverManagerDataSource dataSource;

    /**
     * Constructor
     * @param dataSource
     */
    public ReverseEngineerUtils(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Tests that a connection can be made to a database using the credentials provided
     * TODO: add connection test for all databases covered by SchemaCrawler - postgresql, oracle, db2,
     * @param dbCredentials
     * @return
     */
    public SmdDbCredentials testDatabaseConnection (SmdDbCredentials dbCredentials) {
        RevEngResult revEngResult = new RevEngResult();
        ReverseEngineerDatabase revEngInterface = null;

        // 1. instantiate the appropriate reverse engineering class for the JDBC driver family
        switch (dbCredentials.getJdbcDriverFamily()) {
            case "mysql":
                revEngInterface = new RevEngMysql();
                break;
            case "postgresql":
                revEngInterface = new RevEngPostgresql();
                break;
            case "sqlserver":
                revEngInterface = new RevEngMsSqlServer();
                break;
            case "oracle":
                revEngInterface = new RevEngOracle();
                break;
            default:
                revEngResult.setResultStatus(-200201);
                throw new IllegalStateException("Unexpected value: " + dbCredentials.getJdbcDriverFamily());
        }

        // 2. test the connection
        try {
            dbCredentials = revEngInterface.testDatabaseConnection(dbCredentials);
        }
        catch (Exception e) {
            dbCredentials.setConnectionTestResult(false);
        }

        return dbCredentials;
    }

    /**
     * Execute an online Database reverse engineering run. Currently this uses DL internal DB reverse engineering classes.
     * @param smdDatabaseId
     * @param userId
     * @return
     */
    public RevEngResult executeOnlineRevEng (Integer smdDatabaseId, Integer userId, String cryptKey) {
        RevEngResult revEngResult = new RevEngResult();

        // 1. get the target database connection credentials from the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_database_connection");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_smd_database_id", smdDatabaseId, Types.INTEGER)
                .addValue("_key", cryptKey, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        SmdDbCredentials dbCredentials = new SmdDbCredentials(
                smdDatabaseId,
                (String) out.get("_smd_hostname"),
                (String) out.get("_smd_database_name"),
                (Integer) out.get("_smd_port"),
                (String) out.get("_smd_username"),
                (String) out.get("_smd_password"),
                (String) out.get("_jdbc_driver_family")
        );
        String schemaNameList = out.get("_schema_name_list").toString();
        String[] schemaNames = schemaNameList.trim().split("\\s*,\\s*");

        ReverseEngineerDatabase revEngInterface = null;
        SmdDatabaseDefinition dbDefn = null;

        // 2. instantiate the appropriate reverse engineering class for the JDBC driver family
        switch (dbCredentials.getJdbcDriverFamily()) {
            case "mysql":
                revEngInterface = new RevEngMysql();
                break;
            case "postgresql":
                revEngInterface = new RevEngPostgresql();
                break;
            case "sqlserver":
                revEngInterface = new RevEngMsSqlServer();
                break;
            case "oracle":
                revEngInterface = new RevEngOracle();
                break;
            default:
                revEngResult.setResultStatus(-200201);
                throw new IllegalStateException("Unexpected value: " + dbCredentials.getJdbcDriverFamily());
        }

        // 3. create a new reverse engineering run record in the DL DB
        Integer newRunId = -1;
        revEngResult.setGeneralErrorMessage("Executing reverse engineer run");
        if (revEngResult.getResultStatus() == 1) {
            newRunId = writeRevEngRun(userId, smdDatabaseId, newRunId, revEngResult);
            if (newRunId < -1) {
                revEngResult.setResultStatus(-2);
                revEngResult.setGeneralErrorMessage("Failed to create a new reveng_run_id in dl_reveng_run.");
                revEngResult.setErrorNumber(-201203);
            }
        }

        // 4. execute the reverse engineering run
        //    If an error occurs in step 4 there is no rollback required because no writes have been made to the DL DB.
        if (revEngResult.getResultStatus() == 1) {
            try {
                RevEngResult connectorResult = revEngInterface.reverseEngineerDatabase(dbCredentials, schemaNames);

                if (connectorResult.getErrorNumber() == 0) {
                    dbDefn = revEngInterface.getReverseEngineerCatalog();
                }
                else {
                    revEngResult.setResultStatus(-2);
                    revEngResult.setErrorNumber(connectorResult.getErrorNumber());
                    revEngResult.setGeneralErrorMessage(connectorResult.getGeneralErrorMessage());
                }
            } catch (Exception e) {
                revEngResult.setResultStatus(-2);
                revEngResult.setErrorNumber(-200200);
                revEngResult.setGeneralErrorMessage("ERROR invoking reverse engineering connector for JDBC driver family " + dbCredentials.getJdbcDriverFamily());
                e.printStackTrace();
            }
        }

        // 5. save the reverse engineered catalog to the DL DB.
        //    TODO: If an error occurs in step 5 then a rollback is required
        //    TODO: stored proc calls should be within try/catch blocks
        //    TODO: this functionality is shared with the Offline Reverse Engineer process. Should be rationalised into a single function in ReverseEngineerUtils.java
        if (revEngResult.getResultStatus() == 1) {
            // 5.1. Schema
            ArrayList<SmdSchemaDefinition> schemaList = dbDefn.getSchemaList();
            for (SmdSchemaDefinition schemaDefn : schemaList) {
                String schemaName = schemaDefn.getSchemaName();
                Integer schemaId = 0;

                jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_schema_reveng");
                in = new MapSqlParameterSource()
                        .addValue("_user_id", userId, Types.INTEGER)                // passed as method parameter
                        .addValue("_database_id", smdDatabaseId, Types.INTEGER)     // passed as method parameter
                        .addValue("_class_id", 19, Types.INTEGER)        // TODO: replace with class service
                        .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                        .addValue("_schema_name", schemaName, Types.VARCHAR)
                        .addValue("_schema_shortdesc", schemaDefn.getSchemaShortDesc(), Types.VARCHAR);
                out = jdbcCall.execute(in);
                schemaId = (Integer) out.get("_schema_id");

                // 5.2. Tables/Views
                ArrayList<SmdTableViewDefinition> tableViewList = schemaDefn.getTableViewList();
                for (SmdTableViewDefinition tableViewDefn : tableViewList ) {
                    String tableViewName = tableViewDefn.getTableViewName();
                    Integer tableViewId = 0;

                    // TODO: replace with class service
                    Integer tableViewClassId = 0;
                    if (tableViewDefn.getClassCode().equals("DB_TABLE"))
                        tableViewClassId = 5;
                    else if (tableViewDefn.getClassCode().equals("DB_VIEW"))
                        tableViewClassId = 6;

                    jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_table_view_reveng");
                    in = new MapSqlParameterSource()
                            .addValue("_user_id", userId, Types.INTEGER)                // passed as method parameter
                            .addValue("_database_id", smdDatabaseId, Types.INTEGER)     // passed as method parameter
                            .addValue("_schema_id", schemaId, Types.INTEGER)
                            .addValue("_class_id", tableViewClassId, Types.INTEGER)
                            .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                            .addValue("_table_view_name", tableViewName, Types.VARCHAR)
                            .addValue("_table_view_shortdesc", tableViewDefn.getTableViewShortDesc(), Types.VARCHAR);
                    out = jdbcCall.execute(in);
                    tableViewId = (Integer) out.get("_table_view_id");

                    // 5.3. Columns
                    ArrayList<SmdColumnDefinition> columnList = tableViewDefn.getColumnList();
                    for (SmdColumnDefinition columnDefn : columnList) {
                        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_column_reveng");
                        in = new MapSqlParameterSource()
                                .addValue("_user_id", userId, Types.INTEGER)                // passed as method parameter
                                .addValue("_table_view_id", tableViewId, Types.INTEGER)
                                .addValue("_class_id", 8, Types.INTEGER)     // TODO: replace with class service
                                .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                                .addValue("_column_ordinal", columnDefn.getColumnOrdinal(), Types.INTEGER)
                                .addValue("_primary_key", columnDefn.getPrimaryKey(), Types.BOOLEAN)
                                .addValue("_column_datatype", columnDefn.getColumnDatatype(), Types.VARCHAR)
                                .addValue("_column_name", columnDefn.getColumnName(), Types.VARCHAR)
                                .addValue("_column_shortdesc", columnDefn.getColumnShortDesc(), Types.VARCHAR);
                        out = jdbcCall.execute(in);
                    }

                }
            }

            // 5.4 Relationships
            for (SmdSchemaDefinition schemaDefn : schemaList) {
                ArrayList<SmdRelationshipDefinition> relationshipList = schemaDefn.getRelationshipList();

                for (SmdRelationshipDefinition relationshipDefn : relationshipList) {
                    try {
                        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_reveng_02");
                        in = new MapSqlParameterSource()
                                .addValue("_user_id", userId, Types.INTEGER)              // passed as method parameter
                                .addValue("_database_id", smdDatabaseId, Types.INTEGER)
                                .addValue("_schema_name", schemaDefn.getSchemaName(), Types.VARCHAR)
                                .addValue("_class_id", 7, Types.INTEGER)            // TODO: replace with class service
                                .addValue("_reveng_run_id", newRunId, Types.INTEGER)
                                .addValue("_parent_schema_name", relationshipDefn.getParentTableViewSchemaName(), Types.VARCHAR)
                                .addValue("_parent_table_view_name", relationshipDefn.getParentTableViewName(), Types.VARCHAR)
                                .addValue("_child_schema_name", relationshipDefn.getChildTableViewSchemaName(), Types.VARCHAR)
                                .addValue("_child_table_view_name", relationshipDefn.getChildTableViewName(), Types.VARCHAR)
                                .addValue("_relationship_name", relationshipDefn.getRelationshipName(), Types.VARCHAR)
                                .addValue("_relationship_shortdesc", relationshipDefn.getRelationshipShortDesc(), Types.VARCHAR);
                        out = jdbcCall.execute(in);

                        Integer relationshipId = (Integer) out.get("_relationship_id");
                        Integer parentTableViewId = (Integer) out.get("_parent_table_view_id");
                        Integer childTableViewId = (Integer) out.get("_child_table_view_id");

                        // If parent table is in another schema and has not yet been reverse engineered then parentTableId == 0
                        // Only process columnPairs if parentTableId > 0
                        if (parentTableViewId > 0) {
                            ArrayList<SmdRelationshipColumnPair> columnPairs = relationshipDefn.getColumnPairs();
                            for (SmdRelationshipColumnPair columnPair : columnPairs ) {
                                jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_relationship_column_pair_reveng");
                                in = new MapSqlParameterSource()
                                        .addValue("_user_id", userId, Types.INTEGER)              // passed as method parameter
                                        .addValue("_relationship_id", relationshipId, Types.INTEGER)
                                        .addValue("_parent_table_view_id", parentTableViewId, Types.INTEGER)
                                        .addValue("_parent_column_name", columnPair.getParentColumnName(), Types.VARCHAR)
                                        .addValue("_child_table_view_id", childTableViewId, Types.INTEGER)
                                        .addValue("_child_column_name", columnPair.getChildColumnName(), Types.VARCHAR)
                                        .addValue("_reveng_run_id", newRunId, Types.INTEGER);
                                out = jdbcCall.execute(in);
                            }
                        }
                    }
                    catch (Exception e) {
                        revEngResult.setErrorNumber(-200204);
                        revEngResult.setGeneralErrorMessage("Error creating foreign key relationship " + relationshipDefn.getRelationshipName());
                        System.out.println(e.getLocalizedMessage());
                    }
                }

            }

        }

        // 6. update the status of the reverse engineering run record in the DL BD
        Integer revEngRunResult = revEngResult.getResultStatus();

        if (revEngRunResult == 1) {
            // check whether reconciliation is required
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_reveng_run_status");
            in = new MapSqlParameterSource().addValue("_reveng_run_id", newRunId, Types.INTEGER);
            out = jdbcCall.execute(in);
            revEngRunResult = (Integer) out.get("_run_result");
            revEngResult.setResultStatus(revEngRunResult);
            if (revEngRunResult == 2 && revEngResult.getErrorNumber() >= 0)
                revEngResult.setGeneralErrorMessage("Reverse engineer completed successfully.");
        }
        else {
            revEngResult.setGeneralErrorMessage(String.format("%s\nError Number: %d. SQL Error Code: %d. SQL Error Message: $s", revEngResult.getGeneralErrorMessage(),
                    revEngResult.getErrorNumber(), revEngResult.getSqlErrorCode(), revEngResult.getSqlErrorMessage()));
        }
        Integer returnId = writeRevEngRun(userId, smdDatabaseId, newRunId, revEngResult);

        return revEngResult;
    }

    /**
     * Retrieves from the DL DB the log of reverse engineer runs for the SMD database definition identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    public ArrayList<ReverseEngineerRun> getRevEngRunLog(Integer smdDatabaseId) {
        ArrayList<ReverseEngineerRun> revEngRunLog = new ArrayList<ReverseEngineerRun>();

        // 1. get deleted tables/views
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_database_reveng_run_log");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", smdDatabaseId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        ArrayList dbResults = (ArrayList) out.get("#result-set-1");

        // 1.1. process the deleted tables/views
        for (Object listElement : dbResults) {
            LinkedCaseInsensitiveMap rowInfo = (LinkedCaseInsensitiveMap) listElement;
            ReverseEngineerRun revEngRun = new ReverseEngineerRun(
                    (Integer) rowInfo.get("reveng_run_id"),
                    (Integer) rowInfo.get("run_result"),
                    rowInfo.get("run_result_code").toString(),
                    rowInfo.get("run_datetime").toString(),
                    (Integer) rowInfo.get("user_id"),
                    rowInfo.get("user_login").toString()
            );
            if (rowInfo.get("run_result_desc") != null)
                revEngRun.setRevEngRunDesc(rowInfo.get("run_result_desc").toString());
            revEngRunLog.add(revEngRun);
        }

        return revEngRunLog;
    }

    /**
     * Generates a reconciliation report for schemas added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    public Map<String, Object> getRevEngReconReportSchema(Integer smdDatabaseId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;
        Integer runResult = 0;
        Integer latestRevengRunId = 0;
        ArrayList<RevEngReconciliation> deletedSchema = new ArrayList<RevEngReconciliation>();
        ArrayList<RevEngReconciliation> addedSchema = new ArrayList<RevEngReconciliation>();

        // 1. call the DL DB stored proc for schema reverse engineer report
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_schema_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", smdDatabaseId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        latestRevengRunId = (Integer) out.get("_latest_reveng_run_id");
        ArrayList dbResultDeletedSchema = (ArrayList) out.get("#result-set-1");
        ArrayList dbResultAddedSchema = (ArrayList) out.get("#result-set-2");

        // 1.1. process the deleted schemas
        for (Object listElement : dbResultDeletedSchema) {
            LinkedCaseInsensitiveMap schemaInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) schemaInfo.get("smd_schema_id"),
                    (Integer) schemaInfo.get("class_id"),
                    schemaInfo.get("smd_schema_name").toString(),
                    (Integer) schemaInfo.get("reveng_run_id"),
                    (Integer) schemaInfo.get("reveng_run_count")
            );
            deletedSchema.add(revEngRecon);
        }

        // 1.2. process the added schemas
        for (Object listElement : dbResultAddedSchema) {
            LinkedCaseInsensitiveMap schemaInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) schemaInfo.get("smd_schema_id"),
                    (Integer) schemaInfo.get("class_id"),
                    schemaInfo.get("smd_schema_name").toString(),
                    (Integer) schemaInfo.get("reveng_run_id"),
                    (Integer) schemaInfo.get("reveng_run_count")
            );
            addedSchema.add(revEngRecon);
        }

        // 2. call the DL DB stored proc to get overall reconciliation status for the SMD database definition
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_reveng_run_status");
        in = new MapSqlParameterSource().addValue("_reveng_run_id", latestRevengRunId, Types.INTEGER);
        out = jdbcCall.execute(in);

        runResult = (Integer) out.get("_run_result");

        resultData.put("deleted", deletedSchema);
        resultData.put("added", addedSchema);
        resultData.put("run_result", runResult);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Generates a reconciliation report for tables/views added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    public Map<String, Object> getRevEngReconReportTableView(Integer smdDatabaseId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;
        Integer runResult = 0;
        Integer latestRevengRunId = 0;
        ArrayList<RevEngReconciliation> deletedTV = new ArrayList<RevEngReconciliation>();
        ArrayList<RevEngReconciliation> addedTV = new ArrayList<RevEngReconciliation>();

        // 1. call the DL DB stored proc for tables/views reverse engineer report
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_table_view_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", smdDatabaseId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        latestRevengRunId = (Integer) out.get("_latest_reveng_run_id");
        ArrayList dbResultDeletedTV = (ArrayList) out.get("#result-set-1");
        ArrayList dbResultAddedTV = (ArrayList) out.get("#result-set-2");

        // 1.1. process the deleted tables/views
        for (Object listElement : dbResultDeletedTV) {
            LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) tableViewInfo.get("smd_table_view_id"),
                    (Integer) tableViewInfo.get("class_id"),
                    tableViewInfo.get("smd_schema_name").toString() + "." + tableViewInfo.get("smd_table_view_name").toString(),
                    (Integer) tableViewInfo.get("reveng_run_id"),
                    (Integer) tableViewInfo.get("reveng_run_count")
            );
            deletedTV.add(revEngRecon);
        }

        // 1.2. process the added tables/views
        for (Object listElement : dbResultAddedTV) {
            LinkedCaseInsensitiveMap tableViewInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) tableViewInfo.get("smd_table_view_id"),
                    (Integer) tableViewInfo.get("class_id"),
                    tableViewInfo.get("smd_schema_name").toString() + "." + tableViewInfo.get("smd_table_view_name").toString(),
                    (Integer) tableViewInfo.get("reveng_run_id"),
                    (Integer) tableViewInfo.get("reveng_run_count")
            );
            addedTV.add(revEngRecon);
        }

        // 2. call the DL DB stored proc to get overall reconciliation status for the SMD database definition
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_reveng_run_status");
        in = new MapSqlParameterSource().addValue("_reveng_run_id", latestRevengRunId, Types.INTEGER);
        out = jdbcCall.execute(in);

        runResult = (Integer) out.get("_run_result");

        resultData.put("deleted", deletedTV);
        resultData.put("added", addedTV);
        resultData.put("run_result", runResult);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Executes the reconciliation action selected by the user for each deleted Table/View in the most recent reverse
     * engineer run.
     * @param revEngReconciliationList
     * @return
     */
    public Integer reconcileRevEngDeletedTableView(ArrayList<RevEngReconciliation> revEngReconciliationList) {
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("reconcile_smd_table_view_reveng");
        // iterate over revEngReconciliationList and invoke DL DB stored proc reconcile_smd_table_view_reveng for each object
        for (RevEngReconciliation revEngRecon : revEngReconciliationList) {
            if (revEngRecon.getReconciliationStatus() > 0) {
                SqlParameterSource in = new MapSqlParameterSource()
                        .addValue("_object_id", revEngRecon.getObjectId(), Types.INTEGER)
                        .addValue("_class_id", revEngRecon.getClassId(), Types.INTEGER)
                        .addValue("_target_object_id", revEngRecon.getReconciledObjectId(), Types.INTEGER)
                        .addValue("_target_class_id", revEngRecon.getReconciledClassId(), Types.INTEGER)
                        .addValue("_reconciliation_action", revEngRecon.getReconciliationStatus(), Types.INTEGER);
                Map<String, Object> out = jdbcCall.execute(in);
            }
        }

        return resultStatus;
    }


    /**
     * Generates a reconciliation report for columns added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    public Map<String, Object> getRevEngReconReportColumn(Integer smdDatabaseId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;
        Integer runResult = 0;
        Integer latestRevengRunId = 0;
        ArrayList<RevEngReconciliation> deletedCol = new ArrayList<RevEngReconciliation>();
        ArrayList<RevEngReconciliation> addedCol = new ArrayList<RevEngReconciliation>();

        // 1. call the DL DB stored proc for column reverse engineer report
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_column_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", smdDatabaseId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        latestRevengRunId = (Integer) out.get("_latest_reveng_run_id");
        ArrayList dbResultDeletedCol = (ArrayList) out.get("#result-set-1");
        ArrayList dbResultAddedCol = (ArrayList) out.get("#result-set-2");

        // 1.1. process the deleted columns
        for (Object listElement : dbResultDeletedCol) {
            LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) columnInfo.get("object_id"),
                    (Integer) columnInfo.get("class_id"),
                    columnInfo.get("object_name").toString(),
                    (Integer) columnInfo.get("reveng_run_id"),
                    (Integer) columnInfo.get("reveng_run_count")
            );
            revEngRecon.setParentObjectId((Integer) columnInfo.get("parent_object_id"));
            revEngRecon.setParentClassId((Integer) columnInfo.get("parent_class_id"));
            revEngRecon.setParentObjectName(columnInfo.get("smd_schema_name").toString() + "." + columnInfo.get("parent_object_name").toString());
            deletedCol.add(revEngRecon);
        }

        // 1.2. process the added columns
        for (Object listElement : dbResultAddedCol) {
            LinkedCaseInsensitiveMap columnInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) columnInfo.get("object_id"),
                    (Integer) columnInfo.get("class_id"),
                    columnInfo.get("object_name").toString(),
                    (Integer) columnInfo.get("reveng_run_id"),
                    (Integer) columnInfo.get("reveng_run_count")
            );
            revEngRecon.setParentObjectId((Integer) columnInfo.get("parent_object_id"));
            revEngRecon.setParentClassId((Integer) columnInfo.get("parent_class_id"));
            revEngRecon.setParentObjectName(columnInfo.get("smd_schema_name").toString() + "." + columnInfo.get("parent_object_name").toString());
            addedCol.add(revEngRecon);
        }

        // 2. call the DL DB stored proc to get overall reconciliation status for the SMD database definition
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_reveng_run_status");
        in = new MapSqlParameterSource().addValue("_reveng_run_id", latestRevengRunId, Types.INTEGER);
        out = jdbcCall.execute(in);

        runResult = (Integer) out.get("_run_result");

        resultData.put("deleted", deletedCol);
        resultData.put("added", addedCol);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        resultData.put("run_result", runResult);

        return results;
    }

    /**
     * Executes the reconciliation action selected by the user for each deleted Column in the most recent reverse
     * engineer run.
     * @param revEngReconciliationList
     * @return
     */
    public Integer reconcileRevEngDeletedColumn(ArrayList<RevEngReconciliation> revEngReconciliationList) {
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("reconcile_smd_column_reveng");
        // iterate over revEngReconciliationList and invoke DL DB stored proc reconcile_smd_column_reveng for each object
        for (RevEngReconciliation revEngRecon : revEngReconciliationList) {
            if (revEngRecon.getReconciliationStatus() > 0) {
                SqlParameterSource in = new MapSqlParameterSource()
                        .addValue("_object_id", revEngRecon.getObjectId(), Types.INTEGER)
                        .addValue("_class_id", revEngRecon.getClassId(), Types.INTEGER)
                        .addValue("_target_object_id", revEngRecon.getReconciledObjectId(), Types.INTEGER)
                        .addValue("_target_class_id", revEngRecon.getReconciledClassId(), Types.INTEGER)
                        .addValue("_reconciliation_action", revEngRecon.getReconciliationStatus(), Types.INTEGER);
                Map<String, Object> out = jdbcCall.execute(in);
            }
        }

        return resultStatus;
    }

    /**
     * Generates a reconciliation report for relationships added or deleted in the most recent reverse engineering run
     * for the database identified by smdDatabaseId
     * @param smdDatabaseId
     * @return
     */
    public Map<String, Object> getRevEngReconReportRelationship(Integer smdDatabaseId) {
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;
        Integer runResult = 0;
        Integer latestRevengRunId = 0;
        ArrayList<RevEngReconciliation> deletedRel = new ArrayList<RevEngReconciliation>();
        ArrayList<RevEngReconciliation> addedRel = new ArrayList<RevEngReconciliation>();

        // 1. call the DL DB stored proc for relationship reverse engineer report
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_relationship_reveng");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_database_id", smdDatabaseId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        latestRevengRunId = (Integer) out.get("_latest_reveng_run_id");
        ArrayList dbResultDeletedRel = (ArrayList) out.get("#result-set-1");
        ArrayList dbResultAddedRel = (ArrayList) out.get("#result-set-2");

        // 1.1. process the deleted relationships
        for (Object listElement : dbResultDeletedRel) {
            LinkedCaseInsensitiveMap relInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) relInfo.get("object_id"),
                    (Integer) relInfo.get("class_id"),
                    relInfo.get("smd_schema_name").toString() + "." + relInfo.get("object_name").toString(),
                    (Integer) relInfo.get("reveng_run_id"),
                    (Integer) relInfo.get("reveng_run_count")
            );
            deletedRel.add(revEngRecon);
        }

        // 1.2. process the added relationships
        for (Object listElement : dbResultAddedRel) {
            LinkedCaseInsensitiveMap relInfo = (LinkedCaseInsensitiveMap) listElement;
            RevEngReconciliation revEngRecon = new RevEngReconciliation(
                    (Integer) relInfo.get("object_id"),
                    (Integer) relInfo.get("class_id"),
                    relInfo.get("smd_schema_name").toString() + "." + relInfo.get("object_name").toString(),
                    (Integer) relInfo.get("reveng_run_id"),
                    (Integer) relInfo.get("reveng_run_count")
            );
            addedRel.add(revEngRecon);
        }

        // 2. call the DL DB stored proc to get overall reconciliation status for the SMD database definition
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("check_reveng_run_status");
        in = new MapSqlParameterSource().addValue("_reveng_run_id", latestRevengRunId, Types.INTEGER);
        out = jdbcCall.execute(in);

        runResult = (Integer) out.get("_run_result");

        resultData.put("deleted", deletedRel);
        resultData.put("added", addedRel);
        resultData.put("run_result", runResult);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Executes the reconciliation action selected by the user for each deleted Relationship in the most recent reverse
     * engineer run.
     * @param revEngReconciliationList
     * @return
     */
    public Integer reconcileRevEngDeletedRelationship(ArrayList<RevEngReconciliation> revEngReconciliationList) {
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("reconcile_smd_relationship_reveng");
        // iterate over revEngReconciliationList and invoke DL DB stored proc reconcile_smd_relationship_reveng for each object
        for (RevEngReconciliation revEngRecon : revEngReconciliationList) {
            if (revEngRecon.getReconciliationStatus() > 0) {
                SqlParameterSource in = new MapSqlParameterSource()
                        .addValue("_object_id", revEngRecon.getObjectId(), Types.INTEGER)
                        .addValue("_class_id", revEngRecon.getClassId(), Types.INTEGER)
                        .addValue("_target_object_id", revEngRecon.getReconciledObjectId(), Types.INTEGER)
                        .addValue("_target_class_id", revEngRecon.getReconciledClassId(), Types.INTEGER)
                        .addValue("_reconciliation_action", revEngRecon.getReconciliationStatus(), Types.INTEGER);
                Map<String, Object> out = jdbcCall.execute(in);
            }
        }

        return resultStatus;
    }

    /**
     * Creates a new reverse engineering run record in the DL DB or updates the status of an existing record.
     * @param userId
     * @param smdDatabaseId
     * @return
     */
    private Integer writeRevEngRun(Integer userId, Integer smdDatabaseId, Integer revengRunId, RevEngResult revEngResult) {

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_reveng_run");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", userId, Types.INTEGER)
                .addValue("_smd_database_id", smdDatabaseId, Types.INTEGER)
                .addValue("_reveng_run_id", revengRunId, Types.INTEGER)
                .addValue("_reveng_run_status", revEngResult.getResultStatus(), Types.INTEGER)
                .addValue("_reveng_run_desc", revEngResult.getGeneralErrorMessage(), Types.VARCHAR);
        Map out = jdbcCall.execute(in);
        return (Integer) out.get("_return_id");
    }


}
