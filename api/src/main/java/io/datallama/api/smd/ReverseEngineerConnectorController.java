/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.RevEngConCredConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class ReverseEngineerConnectorController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Returns the list of ReverseEngineerConnector objects configured for the Metadata Object Class identified by {classId}
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/reverse-engineer/connector/list/forclass/{classId}")
    public Map<String, Object> readRevEngConnectorListForClass(@PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_connector_for_class");

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        ArrayList revengConnectorList = new ArrayList();

        // generate an SmdDatabaseDefinition object for each row in result set
        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) listElement;
            ReverseEngineerConnector revengConnector = new ReverseEngineerConnector(
                    (Integer) elementInfo.get("connector_id"),
                    (Integer) elementInfo.get("class_id"),
                    (Integer) elementInfo.get("credential_type_id"),
                    elementInfo.get("connector_provider").toString(),
                    elementInfo.get("connector_name").toString(),
                    elementInfo.get("connector_version").toString()
            );
            revengConnector.setConnectorShortDesc(elementInfo.get("connector_shortdesc").toString());

            revengConnectorList.add(revengConnector);
        }


        results.put("result_data", revengConnectorList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the connector and credential configuration for the metadata object identified by objectId and classId
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/reverse-engineer/concredconfig/{objectId}/{classId}")
    public Map<String, Object> readRevEngConCred(@PathVariable Integer objectId, @PathVariable Integer classId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_concred_config");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        resultData.put("connectorId", (Integer) out.get("_connector_id"));
        resultData.put("connectorName", out.get("_connector_name").toString());
        resultData.put("credentialId", (Integer) out.get("_credential_id"));
        resultData.put("credentialName", out.get("_credential_name").toString());
        resultStatus = (Integer) out.get("_result_id");

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Saves the connector and credential configuration for the metadata object identified by objectId and classId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/reverse-engineer/concredconfig")
    public Map<String, Object> writeRevEngConCred(@RequestBody RevEngConCredConfig revEngConCredConfig, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_reveng_concred_config");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", revEngConCredConfig.getObjectId(), Types.INTEGER)
                .addValue("_class_id", revEngConCredConfig.getClassId(), Types.INTEGER)
                .addValue("_connector_id", revEngConCredConfig.getConnectorId(), Types.INTEGER)
                .addValue("_credential_id", revEngConCredConfig.getCredentialId(), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        resultStatus = (Integer) out.get("_return_id");
        results.put("result_status", resultStatus);

        return results;
    }

}
