/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.util.ArrayList;

/**
 * Models a directory in an Object Store container. Used in the reverse engineering of an Object Store.
 */
public class ObjectStoreDirectory {
    private Integer objectId;
    private Integer classId;
    private String directoryName;
    private String directoryShortDesc;
    private String directoryProperties;
    private ArrayList<ObjectStoreDirectory> directoryList;  // the sub-directory tree under this directory1

    /**
     * Default constructor with no parameters
     */
    public ObjectStoreDirectory() {
        this.objectId = 0;
        this.classId = 0;
        this.directoryName = "";
        this.directoryShortDesc = "";
        this.directoryProperties = "";
        this.directoryList = new ArrayList<ObjectStoreDirectory>();
    }

    /**
     * Constructor 01. Name parameter only.
     * @param directoryName
     */
    public ObjectStoreDirectory(String directoryName) {
        this.objectId = 0;
        this.classId = 0;
        this.directoryName = directoryName;
        this.directoryShortDesc = "";
        this.directoryProperties = "";
        this.directoryList = new ArrayList<ObjectStoreDirectory>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getDirectoryName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }

    public String getDirectoryShortDesc() {
        return directoryShortDesc;
    }

    public void setDirectoryShortDesc(String directoryShortDesc) {
        this.directoryShortDesc = directoryShortDesc;
    }

    public String getDirectoryProperties() {
        return directoryProperties;
    }

    public void setDirectoryProperties(String directoryProperties) {
        this.directoryProperties = directoryProperties;
    }

    public ArrayList<ObjectStoreDirectory> getDirectoryList() {
        return directoryList;
    }

    public void setDirectoryList(ArrayList<ObjectStoreDirectory> directoryList) {
        this.directoryList = directoryList;
    }

    /**
     * Adds a new ObjectStoreDirectory object to directoryList
     * @param directory
     */
    public void addDirectory(ObjectStoreDirectory directory) {
        this.directoryList.add(directory);
    }

}
