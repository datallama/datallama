/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.util.ArrayList;

public class SmdRelationshipDefinition {
    private Integer relationshipId;
    private Integer classId;
    private Integer createMode;
    private String relationshipName;
    private Integer parentTableViewId;
    private Integer parentClassId;
    private String parentTableViewName;
    private String parentTableViewSchemaName;
    private Integer childTableViewId;
    private Integer childClassId;
    private String childTableViewName;
    private String childTableViewSchemaName;
    private Integer databaseId;
    private Integer schemaId;
    private Integer revengRunId;
    private String relationshipShortDesc;
    private ArrayList<SmdRelationshipColumnPair> columnPairs;

    public SmdRelationshipDefinition(){
        this.relationshipId = 0;
        this.classId = 0;
        this.createMode = 0;
        this.relationshipName = "";
        this.parentTableViewId = 0;
        this.parentClassId = 0;
        this.parentTableViewName = "";
        this.parentTableViewSchemaName = "";
        this.childTableViewId = 0;
        this.childClassId = 0;
        this.childTableViewName = "";
        this.childTableViewSchemaName = "";
        this.relationshipShortDesc = "";
        this.columnPairs = new ArrayList<SmdRelationshipColumnPair>();
    }

    public SmdRelationshipDefinition(Integer relationshipId, Integer classId, Integer createMode, String relationshipName,
                                     Integer parentTableViewId, Integer parentClassId, String parentTableViewName,
                                     Integer childTableViewId, Integer childClassId, String childTableViewName) {
        this.relationshipId = relationshipId;
        this.classId = classId;
        this.createMode = createMode;
        this.relationshipName = relationshipName;
        this.parentTableViewId = parentTableViewId;
        this.parentClassId = parentClassId;
        this.parentTableViewName = parentTableViewName;
        this.parentTableViewSchemaName = "";
        this.childTableViewId = childTableViewId;
        this.childClassId = childClassId;
        this.childTableViewName = childTableViewName;
        this.childTableViewSchemaName = "";
        this.relationshipShortDesc = "";
        this.columnPairs = new ArrayList<SmdRelationshipColumnPair>();
    }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Integer databaseId) {
        this.databaseId = databaseId;
    }

    public Integer getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }

    public Integer getRevengRunId() {
        return revengRunId;
    }

    public void setRevengRunId(Integer revengRunId) {
        this.revengRunId = revengRunId;
    }

    public String getRelationshipShortDesc() {
        return relationshipShortDesc;
    }

    public void setRelationshipShortDesc(String relationshipShortDesc) {
        this.relationshipShortDesc = relationshipShortDesc;
    }

    public Integer getCreateMode() {
        return createMode;
    }

    public void setCreateMode(Integer createMode) {
        this.createMode = createMode;
    }

    public Integer getRelationshipId() { return relationshipId; }

    public void setRelationshipId(Integer relationshipId) { this.relationshipId = relationshipId; }

    public Integer getClassId() { return classId; }

    public void setClassId(Integer classId) { this.classId = classId; }

    public String getRelationshipName() { return relationshipName; }

    public void setRelationshipName(String relationshipName) { this.relationshipName = relationshipName; }

    public Integer getParentTableViewId() { return parentTableViewId; }

    public void setParentTableViewId(Integer parentTableViewId) { this.parentTableViewId = parentTableViewId; }

    public Integer getParentClassId() { return parentClassId; }

    public void setParentClassId(Integer parentClassId) { this.parentClassId = parentClassId; }

    public String getParentTableViewName() { return parentTableViewName; }

    public void setParentTableViewName(String parentTableViewName) { this.parentTableViewName = parentTableViewName; }

    public String getParentTableViewSchemaName() {
        return parentTableViewSchemaName;
    }

    public void setParentTableViewSchemaName(String parentTableViewSchemaName) {
        this.parentTableViewSchemaName = parentTableViewSchemaName;
    }

    public Integer getChildTableViewId() { return childTableViewId; }

    public void setChildTableViewId(Integer childTableViewId) { this.childTableViewId = childTableViewId; }

    public Integer getChildClassId() { return childClassId; }

    public void setChildClassId(Integer childClassId) { this.childClassId = childClassId; }

    public String getChildTableViewName() { return childTableViewName; }

    public void setChildTableViewName(String childTableViewName) { this.childTableViewName = childTableViewName; }

    public String getChildTableViewSchemaName() {
        return childTableViewSchemaName;
    }

    public void setChildTableViewSchemaName(String childTableViewSchemaName) {
        this.childTableViewSchemaName = childTableViewSchemaName;
    }

    public ArrayList<SmdRelationshipColumnPair> getColumnPairs() {
        return columnPairs;
    }

    public void setColumnPairs(ArrayList<SmdRelationshipColumnPair> columnPairs) {
        this.columnPairs = columnPairs;
    }

    public void addColumnPair(SmdRelationshipColumnPair columnPair) {
        this.columnPairs.add(columnPair);
    }
}
