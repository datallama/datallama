/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.datallama.api.apidefinition.ReverseEngineerApiUtil;
import io.datallama.api.common.RevEngResultCmn;
import io.datallama.api.common.ReverseEngineerRunCmn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class ReverseEngineerDataIntegrationController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.file.upload}")
    private String uploadFilePath;

    /**
     * Execute an offline reverse engineer using the JSON file format DlDiSmd
     * @param objectId
     * @param userId
     * @param uploadFile
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/dataintegration/reverse-engineer/offline/dldismd/json/{objectId}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Map<String, Object> revEngOfflineDlDiSmdJson(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId,
                                                          @RequestParam("file") MultipartFile uploadFile)
    {
        Map<String, Object> results = new HashMap<>();
        RevEngResultCmn revEngResult = new RevEngResultCmn();

        ObjectMapper objectMapper = new ObjectMapper();
        revEngResult.setResultStatus(1);

        // 1. persist the upload file to the local filesystem.
        String uploadFileName = "";
        if (uploadFilePath.endsWith("/"))
            uploadFileName = String.format("%s%s", uploadFilePath, uploadFile.getOriginalFilename());
        else
            uploadFileName = String.format("%s/%s", uploadFilePath, uploadFile.getOriginalFilename());

        try {
            File convertFile = new File(uploadFileName);

            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(uploadFile.getBytes());
            fout.close();
        } catch (IOException e) {
            Integer resultStatus = -2;

            revEngResult.setResultStatus(resultStatus);
            revEngResult.setErrorNumber(-200404);
            revEngResult.setGeneralErrorMessage(String.format("Failed to save upload file to %s.", uploadFileName));
            results.put("result_data", revEngResult);
            results.put("result_status", resultStatus);
        }

        // 2. Instantiate a ReverseEngineerDataIntegrationUtil object and execute the reverse engineer run
        if (revEngResult.getResultStatus() == 1) {
            ReverseEngineerDataIntegrationUtil revEngUtil = new ReverseEngineerDataIntegrationUtil(dataSource, Integer.parseInt(userId));
            revEngResult = revEngUtil.executeOfflineRevEngDlDiJson(objectId, uploadFileName);
            results.put("result_data", revEngResult);
            results.put("result_status", revEngResult.getResultStatus());
        }

        return results;
    }

    /**
     * Returns the history of reverse engineer runs for the Data Integration Project identified by {objectId} and {classId}
     *
     * @param objectId
     * @param classId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/dataintegration/reverse-engineer/history/{objectId}/{classId}")
    public Map<String, Object> getDataIntegrationRevEngHistory(@PathVariable Integer objectId, @PathVariable Integer classId, @RequestHeader("dlUserId") String userId) {
        Integer resultStatus = 1;
        Map<String, Object> results = new HashMap<>();

        ReverseEngineerCommonUtil revEngUtils = new ReverseEngineerCommonUtil(dataSource, Integer.parseInt(userId));
        ArrayList<ReverseEngineerRunCmn> revEngRunHistory = revEngUtils.getRevEngRunHistory(objectId, classId);

        results.put("result_status", resultStatus);
        results.put("result_data", revEngRunHistory);
        return results;
    }

}
