/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class SmdColumnDefinition {
    private Integer columnId = 0;
    private Integer classId = 0;
    private Integer parentObjectId = 0;
    private Integer parentClassId = 0;
    private String columnName = "";
    private Integer columnOrdinal = 0;
    private Boolean isPrimaryKey = false;
    private Boolean isNullable = false;
    private String columnDatatype = "";
    private String columnShortDesc = "";
    private String tableViewName = "";
    private String tableViewShortDesc = "";     // TODO: tableview details should be returned to UI using SmdTableViewDefinition class
    private ArrayList<DlMetaDataContextNode> mdContext;
    private DlCustomForm customForm;
    private DlDataSensitivityTag dlDsTag;

    /**
     * Default constructor. No parameters.
     */
    public SmdColumnDefinition() {
        this.columnId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.columnName = "";
        this.columnOrdinal = 0;
        this.isPrimaryKey = false;
        this.isNullable = false;
        this.columnDatatype = "";
        this.columnShortDesc = "";
        this.tableViewName = "";
        this.tableViewShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customForm = new DlCustomForm();
        this.dlDsTag = new DlDataSensitivityTag();
    };

    /**
     * Constructure 01. Essential parameters.
     * @param columnId
     * @param columnName
     * @param columnOrdinal
     * @param columnDatatype
     */
    public SmdColumnDefinition(Integer columnId, String columnName, Integer columnOrdinal, String columnDatatype) {
        this.columnId = columnId;
        this.classId = 0;       // TODO: this should be a mandatory argument to the constructor
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.columnName = columnName;
        this.columnOrdinal = columnOrdinal;
        this.isPrimaryKey = false;
        this.isNullable = false;
        this.columnDatatype = columnDatatype;
        this.columnShortDesc = "";
        this.tableViewName = "";
        this.tableViewShortDesc = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customForm = new DlCustomForm();
        this.dlDsTag = new DlDataSensitivityTag();
    }

    public Integer getColumnId() {
        return columnId;
    }

    public void setColumnId(Integer columnId) {
        this.columnId = columnId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getColumnOrdinal() {
        return columnOrdinal;
    }

    public void setColumnOrdinal(Integer columnOrdinal) {
        this.columnOrdinal = columnOrdinal;
    }

    public Boolean getPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(Boolean primaryKey) {
        isPrimaryKey = primaryKey;
    }

    public Boolean getNullable() {
        return isNullable;
    }

    public void setNullable(Boolean nullable) {
        isNullable = nullable;
    }

    public String getColumnDatatype() {
        return columnDatatype;
    }

    public void setColumnDatatype(String columnDatatype) {
        this.columnDatatype = columnDatatype;
    }

    public String getColumnShortDesc() {
        return columnShortDesc;
    }

    public void setColumnShortDesc(String columnShortDesc) {
        this.columnShortDesc = columnShortDesc;
    }

    public String getTableViewName() {
        return tableViewName;
    }

    public void setTableViewName(String tableViewName) {
        this.tableViewName = tableViewName;
    }

    public String getTableViewShortDesc() {
        return tableViewShortDesc;
    }

    public void setTableViewShortDesc(String tableViewShortDesc) {
        this.tableViewShortDesc = tableViewShortDesc;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }

    public DlDataSensitivityTag getDlDsTag() {
        return dlDsTag;
    }

    public void setDlDsTag(DlDataSensitivityTag dlDsTag) {
        this.dlDsTag = dlDsTag;
    }

    public Integer validateMinimal() {
        Integer valid = 1;

        if (this.columnName == null || this.columnName.length() == 0)
            valid = -1;
        if (this.columnOrdinal == null || this.columnOrdinal < 1)
            valid = -1;
        if (this.columnDatatype == null || this.columnDatatype.length() == 0)
            valid = -1;

        return valid;
    }
}
