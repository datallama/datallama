/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.MoClassConstants;
import io.datallama.api.common.ReverseEngineerRunCmn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

public class ReverseEngineerCommonUtil {
    private DriverManagerDataSource dataSource;
    private Integer userId;
    private MoClassConstants moClassConstants;

    /**
     * Constructor
     * @param dataSource
     * @param userId
     */
    public ReverseEngineerCommonUtil(DriverManagerDataSource dataSource, Integer userId) {
        this.dataSource = dataSource;
        this.userId = userId;
        this.moClassConstants = new MoClassConstants();
    }

    /**
     * Returns the history of reverse engineer runs for the metadata object identified by this.objectId and this.classId
     * @param objectId
     * @param classId
     * @return
     */
    public ArrayList<ReverseEngineerRunCmn> getRevEngRunHistory(Integer objectId, Integer classId) {

        ArrayList<ReverseEngineerRunCmn> revEngRunHistory = new ArrayList<ReverseEngineerRunCmn>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_run_list_cmn");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER)
                .addValue("_class_id", classId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) { // add all tables/views into a hashmap that can be used when adding column definitions
            LinkedCaseInsensitiveMap revEngRunInfo = (LinkedCaseInsensitiveMap) listElement;
            ReverseEngineerRunCmn revEngRun = new ReverseEngineerRunCmn();
            revEngRun.setRevEngRunId((Integer) revEngRunInfo.get("reveng_run_id"));
            revEngRun.setRevEngRunDateTime(revEngRunInfo.get("run_datetime").toString());
            revEngRun.setRevEngRunResult((Integer) revEngRunInfo.get("run_result"));
            revEngRun.setRevEngRunDesc(revEngRunInfo.get("run_result_desc").toString());
            revEngRun.setInputSource(revEngRunInfo.get("input_source").toString());
            revEngRun.setUserLogin(revEngRunInfo.get("user_login").toString());

            revEngRunHistory.add(revEngRun);
        }

        return revEngRunHistory;
    }

}
