/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.util.ArrayList;

public class DataIntegrationJob {
    private Integer objectId;
    private Integer classId;
    private String jobName;
    private String jobShortDesc;
    private String jobProperties;
    private ArrayList<DataIntegrationJob> dataIntegrationJobList;

    /**
     * Default constructor with no parameters
     */
    public DataIntegrationJob() {
        this.objectId = 0;
        this.classId = 0;
        this.jobName = "";
        this.jobShortDesc = "";
        this.jobProperties = "";
        this.dataIntegrationJobList = new ArrayList<DataIntegrationJob>();
    }

    /**
     * Constructor 01. Name parameter only
     * @param jobName
     */
    public DataIntegrationJob(String jobName) {
        this.objectId = 0;
        this.classId = 0;
        this.jobName = jobName;
        this.jobShortDesc = "";
        this.jobProperties = "";
        this.dataIntegrationJobList = new ArrayList<DataIntegrationJob>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobShortDesc() {
        return jobShortDesc;
    }

    public void setJobShortDesc(String jobShortDesc) {
        this.jobShortDesc = jobShortDesc;
    }

    public String getJobProperties() {
        return jobProperties;
    }

    public void setJobProperties(String jobProperties) {
        this.jobProperties = jobProperties;
    }

    public ArrayList<DataIntegrationJob> getDataIntegrationJobList() {
        return dataIntegrationJobList;
    }

    public void setDataIntegrationJobList(ArrayList<DataIntegrationJob> dataIntegrationJobList) {
        this.dataIntegrationJobList = dataIntegrationJobList;
    }

    public void addDataIntegrationJob(DataIntegrationJob dataIntegrationJob) {
        this.dataIntegrationJobList.add(dataIntegrationJob);
    }

}
