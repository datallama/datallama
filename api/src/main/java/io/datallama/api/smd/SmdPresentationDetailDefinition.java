/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * TODO: as this is a display artefact it should reside in the AJS code base, not the Java code base
 */
package io.datallama.api.smd;

import io.datallama.api.metamodel.DlMetaDataContextNode;
import java.util.ArrayList;

public class SmdPresentationDetailDefinition {
    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private Integer parentElementObjectId;
    private Integer elementLevel;
    private Integer elementLevelSequence;
    private String elementHierarchyNumber;
    private Boolean elementIsParent;
    private String elementName;
    private String elementShortDesc;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private String elementGUID;
    private Integer elementIsVisible;
    private Integer initialExpandLevel;
    private Boolean elementIsExpanded;
    private Boolean elementIsSelected;
    private Boolean elementIsMatched;
    private Integer resultSetSequence;


    public SmdPresentationDetailDefinition() {
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentElementObjectId = 0;
        this.elementName = "";
        this.elementShortDesc = "";
        this.elementLevel = 0;
        this.elementLevelSequence = 0;
        this.elementHierarchyNumber = "";
        this.elementIsParent = false;
        this.mdContext = new ArrayList();
        this.elementGUID = "";
        this.elementIsVisible = 0;
        this.initialExpandLevel = 2;
        this.elementIsExpanded = false;
        this.elementIsSelected = false;
        this.elementIsMatched = false;
        this.resultSetSequence = 0;
    }

    public SmdPresentationDetailDefinition(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                                           Integer parentElementObjectId, String elementName, String elementShortDesc) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.parentElementObjectId = parentElementObjectId;
        this.elementName = elementName;
        this.elementShortDesc = elementShortDesc;
        this.elementLevel = 0;
        this.elementLevelSequence = 0;
        this.elementHierarchyNumber = "";
        this.elementIsParent = false;
        this.mdContext = new ArrayList();
        this.elementGUID = "";
        this.elementIsVisible = 0;
        this.initialExpandLevel = 2;
        this.elementIsExpanded = false;
        this.elementIsSelected = false;
        this.elementIsMatched = false;
        this.resultSetSequence = 0;
    }

    public SmdPresentationDetailDefinition(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                                           Integer parentElementObjectId, Integer elementLevel, Integer elementLevelSequence,
                                           String elementHierarchyNumber, Boolean elementIsParent, String elementName,
                                           String elementShortDesc, String elementGUID) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.parentElementObjectId = parentElementObjectId;
        this.elementLevel = elementLevel;
        this.elementLevelSequence = elementLevelSequence;
        this.elementHierarchyNumber = elementHierarchyNumber;
        this.elementIsParent = elementIsParent;
        this.elementName = elementName;
        this.elementShortDesc = elementShortDesc;
        this.elementGUID = elementGUID;
        this.mdContext = new ArrayList();
        this.elementIsVisible = 0;
        this.initialExpandLevel = 2;
        this.elementIsExpanded = false;
        this.elementIsSelected = false;
        this.elementIsMatched = false;
        this.resultSetSequence = 0;

        this.determineVisibility();
        this.determineExpanded();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getParentElementObjectId() {
        return parentElementObjectId;
    }

    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.parentElementObjectId = parentElementObjectId;
    }

    public Integer getElementLevel() {
        return elementLevel;
    }

    public void setElementLevel(Integer elementLevel) {
        this.elementLevel = elementLevel;
    }

    public Integer getElementLevelSequence() {
        return elementLevelSequence;
    }

    public void setElementLevelSequence(Integer elementLevelSequence) {
        this.elementLevelSequence = elementLevelSequence;
    }

    public String getElementHierarchyNumber() {
        return elementHierarchyNumber;
    }

    public void setElementHierarchyNumber(String elementHierarchyNumber) {
        this.elementHierarchyNumber = elementHierarchyNumber;
    }

    public Boolean getElementIsParent() {
        return elementIsParent;
    }

    public void setElementIsParent(Boolean elementIsParent) {
        this.elementIsParent = elementIsParent;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public String getElementShortDesc() {
        return elementShortDesc;
    }

    public void setElementShortDesc(String elementShortDesc) {
        this.elementShortDesc = elementShortDesc;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public String getElementGUID() {
        return elementGUID;
    }

    public void setElementGUID(String elementGUID) {
        this.elementGUID = elementGUID;
    }

    public Integer getElementIsVisible() {
        return elementIsVisible;
    }

    public void setElementIsVisible(Integer elementIsVisible) {
        this.elementIsVisible = elementIsVisible;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public Boolean getElementIsExpanded() {
        return elementIsExpanded;
    }

    public void setElementIsExpanded(Boolean elementIsExpanded) {
        this.elementIsExpanded = elementIsExpanded;
    }

    public Boolean getElementIsSelected() {
        return elementIsSelected;
    }

    public void setElementIsSelected(Boolean elementIsSelected) {
        this.elementIsSelected = elementIsSelected;
    }

    public Boolean getElementIsMatched() {
        return elementIsMatched;
    }

    public void setElementIsMatched(Boolean elementIsMatched) {
        this.elementIsMatched = elementIsMatched;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public void determineExpanded() {
        this.elementIsExpanded = this.elementLevel < this.initialExpandLevel;
    }

    public void determineVisibility() {
        this.elementIsVisible = (this.elementLevel <= this.initialExpandLevel) ? 1 : 0;
    }

}
