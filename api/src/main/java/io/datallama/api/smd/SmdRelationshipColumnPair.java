/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

public class SmdRelationshipColumnPair {
    private Integer parentColumnId;
    private String parentColumnName;
    private String parentColumnDatatype;
    private Boolean parentColumnPrimaryKey;
    private Integer childColumnId;
    private String childColumnName;
    private String childColumnDatatype;
    private Boolean childColumnPrimaryKey;

    public SmdRelationshipColumnPair() {
        this.parentColumnId = 0;
        this.parentColumnName = "";
        this.parentColumnDatatype = "";
        this.parentColumnPrimaryKey = false;
        this.childColumnId = 0;
        this.childColumnName = "";
        this.childColumnDatatype = "";
        this.childColumnPrimaryKey = false;
    }

    public SmdRelationshipColumnPair(Integer parentColumnId, String parentColumnName,
                                     Integer childColumnId, String childColumnName)
    {
        this.parentColumnId = parentColumnId;
        this.parentColumnName = parentColumnName;
        this.parentColumnDatatype = "";
        this.parentColumnPrimaryKey = false;
        this.childColumnId = childColumnId;
        this.childColumnName = childColumnName;
        this.childColumnDatatype = "";
        this.childColumnPrimaryKey = false;
    }

    public SmdRelationshipColumnPair(Integer parentColumnId, String parentColumnName, String parentColumnDatatype, Boolean parentColumnPrimaryKey,
                                     Integer childColumnId, String childColumnName, String childColumnDatatype, Boolean childColumnPrimaryKey)
    {
        this.parentColumnId = parentColumnId;
        this.parentColumnName = parentColumnName;
        this.parentColumnDatatype = parentColumnDatatype;
        this.parentColumnPrimaryKey = parentColumnPrimaryKey;
        this.childColumnId = childColumnId;
        this.childColumnName = childColumnName;
        this.childColumnDatatype = childColumnDatatype;
        this.childColumnPrimaryKey = childColumnPrimaryKey;
    }

    public Integer getParentColumnId() {
        return parentColumnId;
    }

    public void setParentColumnId(Integer parentColumnId) {
        this.parentColumnId = parentColumnId;
    }

    public String getParentColumnName() {
        return parentColumnName;
    }

    public void setParentColumnName(String parentColumnName) {
        this.parentColumnName = parentColumnName;
    }

    public Integer getChildColumnId() {
        return childColumnId;
    }

    public void setChildColumnId(Integer childColumnId) {
        this.childColumnId = childColumnId;
    }

    public String getChildColumnName() {
        return childColumnName;
    }

    public void setChildColumnName(String childColumnName) {
        this.childColumnName = childColumnName;
    }

    public String getParentColumnDatatype() {
        return parentColumnDatatype;
    }

    public void setParentColumnDatatype(String parentColumnDatatype) {
        this.parentColumnDatatype = parentColumnDatatype;
    }

    public Boolean getParentColumnPrimaryKey() {
        return parentColumnPrimaryKey;
    }

    public void setParentColumnPrimaryKey(Boolean parentColumnPrimaryKey) {
        this.parentColumnPrimaryKey = parentColumnPrimaryKey;
    }

    public String getChildColumnDatatype() {
        return childColumnDatatype;
    }

    public void setChildColumnDatatype(String childColumnDatatype) {
        this.childColumnDatatype = childColumnDatatype;
    }

    public Boolean getChildColumnPrimaryKey() {
        return childColumnPrimaryKey;
    }

    public void setChildColumnPrimaryKey(Boolean childColumnPrimaryKey) {
        this.childColumnPrimaryKey = childColumnPrimaryKey;
    }
}
