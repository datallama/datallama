/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package io.datallama.api.smd;

import io.datallama.api.common.DlMoClass;
import io.datallama.api.formats.DlFormatClass;
import io.datallama.api.smd.SmdPresentationDefinition;
import io.datallama.api.smd.SmdPresentationDetailDefinition;
import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.DlCustomForm;
import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.metamodel.DlMetaModelClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class SmdPresentationDefinitionController {

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Returns the list of Presentation Definitions from the DL database
     * URL Path: /smd/presentation/list
     *
     * @return Map<String, Object> results
     */
    @RequestMapping(method = GET, value = "/smd/presentation/list")
    public Map<String, Object> listPresentations() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        ArrayList<SmdPresentationDefinition> presentationDefinitionList = new ArrayList<SmdPresentationDefinition>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_presentation_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listObject : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) listObject;
            SmdPresentationDefinition presentationDefinition = new SmdPresentationDefinition(
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("object_name").toString(),
                    objectInfo.get("object_shortdesc").toString()
            );
            presentationDefinition.setClassName(objectInfo.get("class_name").toString());
            presentationDefinition.setObjectTable(objectInfo.get("object_table").toString());
            presentationDefinition.setObjectDetailState(objectInfo.get("object_detail_state").toString());

            // add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            presentationDefinition.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));
            presentationDefinitionList.add(presentationDefinition);
        }

        results.put("result_data", presentationDefinitionList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Returns the list of Presentation Definition Classes from the DL database
     * URL Path: /smd/presentation/list
     *
     * @return Map<String, Object> results
     */
    @RequestMapping(method = GET, value = "/smd/presentation/class/list")
    public Map<String, Object> listPresentationClasses() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList<DlMoClass> presentationClassList = new ArrayList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_presentation_classes");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap classInfo = (LinkedCaseInsensitiveMap) listElement;
            DlMoClass moClass = new DlMoClass(
                    (Integer) classInfo.get("class_id"),
                    classInfo.get("class_name").toString(),
                    classInfo.get("class_shortdesc").toString(),
                    classInfo.get("object_table").toString(),
                    classInfo.get("object_detail_state").toString()
            );
            presentationClassList.add(moClass);
        }

        results.put("result_data", presentationClassList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     *
     */
    @RequestMapping(method = POST, value = "/smd/presentation/summary")
    public Map<String, Object> writePresentation(@RequestBody SmdPresentationDefinition presentationDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_presentation");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", presentationDefinition.getObjectId(), Types.INTEGER)
                .addValue("_class_id", presentationDefinition.getClassId(), Types.INTEGER)
                .addValue("_object_name", presentationDefinition.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc", presentationDefinition.getObjectShortDesc(), Types.VARCHAR)
                .addValue("_object_config", presentationDefinition.getObjectConfig(), Types.VARCHAR);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnId", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /smd/presentation/summary/{objectId}
     *
     */
    @RequestMapping(method = GET, value = "/smd/presentation/summary/{objectId}")
    @ResponseBody
    public Map<String, Object> readPresentation(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates the status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList<SmdPresentationDetailDefinition> presentationDetails = new ArrayList<>(); // this list will be returned in the results Map

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_presentation");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        // 1. generate the presentation definition summary
        SmdPresentationDefinition presentationDefinition = new SmdPresentationDefinition(
                objectId,                               // objectId
                (Integer) out.get("_class_id"),         // classId
                out.get("_object_name").toString(),     // objectName
                out.get("_object_shortdesc").toString() // objectShortDesc
        );
        presentationDefinition.setClassName(out.get("_class_name").toString());
        presentationDefinition.setObjectTable(out.get("_object_table").toString());
        presentationDefinition.setObjectDetailState(out.get("_object_detail_state").toString());

        // 1.1. add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        presentationDefinition.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        // 1.2. add the custom form definition
        DlCustomForm customForm = new DlCustomForm(
                (Integer) out.get("_custom_form_id"),
                out.get("_custom_form_name").toString(),
                out.get("_schema_definition").toString(),
                out.get("_form_definition").toString()
        );
        customForm.prepareSchemaDefinition(dataSource);
        presentationDefinition.setCustomForm(customForm);


        resultData.put("presentation", presentationDefinition);

        // 2. generate a presentation detail object for every row in result set and add to Array List presentationDetails
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_presentation_details");
        out = jdbcCall.execute(in);
        ArrayList dbElementsResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;
        for (Object listElement : dbElementsResultList) {
            LinkedCaseInsensitiveMap presentationDetailInfo = (LinkedCaseInsensitiveMap) listElement;

            //make the presentation detail
            SmdPresentationDetailDefinition presentationDetail = new SmdPresentationDetailDefinition(
                    (Integer) presentationDetailInfo.get("object_id"),
                    (Integer) presentationDetailInfo.get("class_id"),
                    (Integer) presentationDetailInfo.get("parent_object_id"),
                    (Integer) presentationDetailInfo.get("parent_class_id"),
                    (Integer) presentationDetailInfo.get("parent_element_object_id"),
                    (Integer) presentationDetailInfo.get("element_level"),
                    (Integer) presentationDetailInfo.get("element_level_sequence"),
                    presentationDetailInfo.get("element_hierarchy_number").toString(),
                    (Boolean) presentationDetailInfo.get("is_parent"),
                    presentationDetailInfo.get("element_name").toString(),
                    presentationDetailInfo.get("element_shortdesc").toString(),
                    presentationDetailInfo.get("element_guid").toString()
            );
            presentationDetail.setResultSetSequence(resultSetSequence);
            resultSetSequence++;

            // add the md_context
            presentationDetail.setMdContext(mdContext.generateMdContextList(presentationDetailInfo.get("md_context").toString()));

            presentationDetails.add(presentationDetail);
        }

        resultData.put("presentation_details", presentationDetails);

        // 3: get the associations for the presentation detail
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource()
                .addValue("_class_id", presentationDefinition.getClassId(), Types.INTEGER)
                .addValue("_object_id", presentationDefinition.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context for the AssociatedObject instance
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);


        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /smd/presentation/detail/{objectId}
     *
     */
    @RequestMapping(method = GET, value = "/smd/presentation/detail/{objectId}")
    @ResponseBody
    public Map<String, Object> readPresentationDetail(@PathVariable Integer objectId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates the status of database results.

        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // list list is returned in the results Map

        // 1: get the core attributes of the presentation detail
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_presentation_detail");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", objectId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        LinkedCaseInsensitiveMap detailInfo = (LinkedCaseInsensitiveMap) dbResultList.get(0);
        SmdPresentationDetailDefinition detail = new SmdPresentationDetailDefinition(
                (Integer) detailInfo.get("object_id"),
                (Integer) detailInfo.get("class_id"),
                (Integer) detailInfo.get("parent_object_id"),
                (Integer) detailInfo.get("parent_class_id"),
                (Integer) detailInfo.get("parent_element_object_id"),
                (Integer) detailInfo.get("element_level"),
                (Integer) detailInfo.get("element_level_sequence"),
                detailInfo.get("element_hierarchy_number").toString(),
                (Boolean) detailInfo.get("is_parent"),
                detailInfo.get("element_name").toString(),
                detailInfo.get("element_shortdesc").toString(),
                detailInfo.get("element_guid").toString()
        );

        // 1.1: add md_context for presentation detail
        DlMetaDataContext mdContext = new DlMetaDataContext();
        detail.setMdContext(mdContext.generateMdContextList(detailInfo.get("md_context").toString()));

        resultData.put("attributes", detail);

        // 2: get the associations for the element
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", detail.getClassId(), Types.INTEGER)
                .addValue("_object_id", detail.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1: create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2: add the metadata context for the AssociatedObject instance
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 3. Add the custom form data -- read_custom_form_data
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_custom_form_data");
        in = new MapSqlParameterSource().addValue("_class_id", detail.getClassId(), Types.INTEGER)
                .addValue("_object_id", detail.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);

        DlCustomForm customForm = new DlCustomForm();
        try {
            customForm.setCustomFormId((Integer) out.get("_custom_form_id"));
            customForm.setFormData(out.get("_custom_form_data").toString());
        }
        catch(NullPointerException e) {
            customForm.setCustomFormId(-1); // -1 indicates that no custom data exists for the JSON node
            customForm.setFormData("{}");
        }

        resultData.put("custom_form", customForm);


        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * URL Path: /smd/presentation/detail
     *
     */
    @RequestMapping(method = POST, value = "/smd/presentation/detail")
    @ResponseBody
    public Map<String, Object> writePresentationDetail(@RequestBody SmdPresentationDetailDefinition newElement, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_presentation_detail");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newElement.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newElement.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newElement.getParentObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", newElement.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id", newElement.getParentElementObjectId(), Types.INTEGER)
                .addValue("_element_name", newElement.getElementName(), Types.VARCHAR)
                .addValue("_element_shortdesc", newElement.getElementShortDesc(), Types.VARCHAR)
                .addValue("_import_run_id", null, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList rslt = (ArrayList) out.get("#result-set-1");
        LinkedCaseInsensitiveMap elementInfo = (LinkedCaseInsensitiveMap) rslt.get(0);

        // instantiate an object to return to invoker
        SmdPresentationDetailDefinition element = new SmdPresentationDetailDefinition(
                (Integer) elementInfo.get("object_id"),
                (Integer) elementInfo.get("class_id"),
                (Integer) elementInfo.get("parent_object_id"),
                (Integer) elementInfo.get("parent_class_id"),
                (Integer) elementInfo.get("parent_element_object_id"),
                (Integer) elementInfo.get("element_level"),
                (Integer) elementInfo.get("element_level_sequence"),
                elementInfo.get("element_hierarchy_number").toString(),
                (Boolean) elementInfo.get("is_parent"),
                elementInfo.get("element_name").toString(),
                elementInfo.get("element_shortdesc").toString(),
                elementInfo.get("element_guid").toString()
        );

        // add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        element.setMdContext(mdContext.generateMdContextList(elementInfo.get("md_context").toString()));

        results.put("result_data", element);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/smd/presentation/{objectId}")
    public Map<String, Object> deletePresentation(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_smd_presentation");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = DELETE, value = "/smd/presentation/detail/{objectId}")
    public Map<String, Object> deletePresentationDetail(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_smd_presentation_detail");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }
}