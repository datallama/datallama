/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.CredentialType;

public class SmdCredential {
    private Integer credentialId;
    private Integer credentialTypeId;
    private String credentialName;
    private String credentialShortDesc;
    private String credentialDetail;
    private CredentialType credentialType;
    private Boolean connectionTestResult;
    private String connectionError;


    /**
     * Default constructor with no parameters.
     */
    public SmdCredential() {
        this.credentialId = 0;
        this.credentialTypeId = 0;
        this.credentialName = "";
        this.credentialDetail = "";
        this.credentialShortDesc = "";
        this.credentialType = new CredentialType();
        this.connectionTestResult = false;
        this.connectionError = "";
    }

    /**
     * Constructor with essential parameters.
     * @param credentialId
     * @param credentialTypeId
     * @param credentialName
     * @param credentialDetail
     */
    public SmdCredential(Integer credentialId, Integer credentialTypeId, String credentialName, String credentialDetail) {
        this.credentialId = credentialId;
        this.credentialTypeId = credentialTypeId;
        this.credentialName = credentialName;
        this.credentialDetail = credentialDetail;
        this.credentialShortDesc = "None Specified";
        this.credentialType = new CredentialType();
        this.connectionTestResult = false;
        this.connectionError = "";
    }

    public Integer getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Integer credentialId) {
        this.credentialId = credentialId;
    }

    public Integer getCredentialTypeId() {
        return credentialTypeId;
    }

    public void setCredentialTypeId(Integer credentialTypeId) {
        this.credentialTypeId = credentialTypeId;
    }

    public String getCredentialName() {
        return credentialName;
    }

    public void setCredentialName(String credentialName) {
        this.credentialName = credentialName;
    }

    public String getCredentialShortDesc() {
        return credentialShortDesc;
    }

    public void setCredentialShortDesc(String credentialShortDesc) {
        this.credentialShortDesc = credentialShortDesc;
    }

    public String getCredentialDetail() {
        return credentialDetail;
    }

    public void setCredentialDetail(String credentialDetail) {
        this.credentialDetail = credentialDetail;
    }

    public CredentialType getCredentialType() {
        return credentialType;
    }

    public void setCredentialType(CredentialType credentialType) {
        this.credentialType = credentialType;
    }

    public Boolean getConnectionTestResult() {
        return connectionTestResult;
    }

    public void setConnectionTestResult(Boolean connectionTestResult) {
        this.connectionTestResult = connectionTestResult;
    }

    public String getConnectionError() {
        return connectionError;
    }

    public void setConnectionError(String connectionError) {
        this.connectionError = connectionError;
    }
}
