/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.util.ArrayList;

/**
 * An Object Store Space is a collection of Object Store Container. It might be a logical collection or it might correspond to a cloud storage account.
 * Used in the reverse engineering of an Object Store.
 */
public class ObjectStoreSpace {
    private Integer objectId;
    private Integer classId;
    private String spaceName;
    private String spaceShortDesc;
    private ArrayList<ObjectStoreContainer> containerList;

    /**
     * Default constructor with no parameters.
     */
    public ObjectStoreSpace() {
        this.objectId = 0;
        this.classId = 0;
        this.spaceName = "";
        this.spaceShortDesc = "";
        this.containerList = new ArrayList<ObjectStoreContainer>();
    }

    /**
     * Constructor with name parameter only.
     * @param spaceName
     */
    public ObjectStoreSpace(String spaceName) {
        this.objectId = 0;
        this.classId = 0;
        this.spaceName = spaceName;
        this.spaceShortDesc = "";
        this.containerList = new ArrayList<ObjectStoreContainer>();
    }

    /**
     * Constructor with name, objectId and classId parameters
     * @param objectId
     * @param classId
     * @param spaceName
     */
    public ObjectStoreSpace(Integer objectId, Integer classId, String spaceName) {
        this.objectId = objectId;
        this.classId = classId;
        this.spaceName = spaceName;
        this.spaceShortDesc = "";
        this.containerList = new ArrayList<ObjectStoreContainer>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getSpaceShortDesc() {
        return spaceShortDesc;
    }

    public void setSpaceShortDesc(String spaceShortDesc) {
        this.spaceShortDesc = spaceShortDesc;
    }

    public ArrayList<ObjectStoreContainer> getContainerList() {
        return containerList;
    }

    public void setContainerList(ArrayList<ObjectStoreContainer> containerList) {
        this.containerList = containerList;
    }

    /**
     * Adds a new ObjectStoreContainer object to containerList
     * @param container
     */
    public void addContainer(ObjectStoreContainer container) {
        this.containerList.add(container);
    }

}
