/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.util.ArrayList;

public class DataIntegrationProject {
    private Integer objectId;
    private Integer classId;
    private String projectName;
    private String projectShortDesc;
    private String projectProperties;
    private ArrayList<DataIntegrationPackage> dataIntegrationPackageList;
    private ArrayList<DataIntegrationJob> dataIntegrationJobList;


    /**
     * Default constructor with no parameters.
     */
    public DataIntegrationProject() {
        this.objectId = 0;
        this.classId = 0;
        this.projectName = "";
        this.projectShortDesc = "";
        this.projectProperties = "";
        this.dataIntegrationPackageList = new ArrayList<DataIntegrationPackage>();
        this.dataIntegrationJobList = new ArrayList<DataIntegrationJob>();
    }

    /**
     * Constructor with name parameter only
     * @param projectName
     */
    public DataIntegrationProject(String projectName) {
        this.objectId = 0;
        this.classId = 0;
        this.projectName = projectName;
        this.projectShortDesc = "";
        this.projectProperties = "";
        this.dataIntegrationPackageList = new ArrayList<DataIntegrationPackage>();
        this.dataIntegrationJobList = new ArrayList<DataIntegrationJob>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectShortDesc() {
        return projectShortDesc;
    }

    public void setProjectShortDesc(String projectShortDesc) {
        this.projectShortDesc = projectShortDesc;
    }

    public String getProjectProperties() {
        return projectProperties;
    }

    public void setProjectProperties(String projectProperties) {
        this.projectProperties = projectProperties;
    }

    public ArrayList<DataIntegrationPackage> getDataIntegrationPackageList() {
        return dataIntegrationPackageList;
    }

    public void setDataIntegrationPackageList(ArrayList<DataIntegrationPackage> dataIntegrationPackageList) {
        this.dataIntegrationPackageList = dataIntegrationPackageList;
    }

    public void addDataIntegrationPackage(DataIntegrationPackage dataIntegrationPackage) {
        this.dataIntegrationPackageList.add(dataIntegrationPackage);
    }

    public ArrayList<DataIntegrationJob> getDataIntegrationJobList() {
        return dataIntegrationJobList;
    }

    public void setDataIntegrationJobList(ArrayList<DataIntegrationJob> dataIntegrationJobList) {
        this.dataIntegrationJobList = dataIntegrationJobList;
    }

    public void addDataIntegrationJob(DataIntegrationJob dataIntegrationJob) {
        this.dataIntegrationJobList.add(dataIntegrationJob);
    }

}
