/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class RevEngMsSqlServer implements ReverseEngineerDatabase {
    private SmdDatabaseDefinition dbDefn;

    public RevEngMsSqlServer(){
        this.dbDefn = new SmdDatabaseDefinition();
    };

    /**
     * Reverse engineers all tables, views and foreign key relationships in the database specified by dbCredentials.
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public SmdDbCredentials testDatabaseConnection (SmdDbCredentials dbCredentials) throws Exception {
        Boolean classFound = true;
        Connection cnxn;

        // 1. try to load the mysql JDBC driver
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setConnectionTestResult(false);
            dbCredentials.setSqlError("Class not found: com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:sqlserver://%s:%d;databaseName=%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());

            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());

                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
            }
        }

        return dbCredentials;
    };

    /**
     * Reverse engineers all tables, views and relationships in the database identified by dbCredentials
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public RevEngResult reverseEngineerDatabase (SmdDbCredentials dbCredentials, String[] schemaNameList) throws Exception {
        RevEngResult revEngResult = new RevEngResult();
        Boolean classFound = true;
        Connection cnxn = null;

        // 1. try to load the mysql JDBC driver
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setConnectionTestResult(false);
            dbCredentials.setSqlError("Class not found: com.microsoft.sqlserver.jdbc.SQLServerDriver");
            revEngResult.setErrorNumber(-200199);
            revEngResult.setGeneralErrorMessage("ERROR connecting to database. Class not found: com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:sqlserver://%s:%d;databaseName=%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());
            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());
                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
                revEngResult.setErrorNumber(-200198);
                revEngResult.setGeneralErrorMessage(String.format("ERROR connecting to database. JDBC URL: %s. Error %s.", jdbcURL, failureMessage));
            }
        }

        // 3. execute the reverse engineer
        if (dbCredentials.getConnectionTestResult()) {
            dbDefn.setDatabaseName(dbCredentials.getDbName());
            SmdSchemaDefinition schemaDefn = null;

            // 3.1. Iterate the schema name list and process each schema for Tables/Views
            for (String schemaName : schemaNameList) {
                schemaDefn = new SmdSchemaDefinition();
                schemaDefn.setSchemaName(schemaName);

                // 3.2 Tables & Views
                String tableviewCommentSql = "select objname as table_name, value as table_comment from fn_listextendedproperty( 'MS_Description', " +
                        "'schema', '" + schemaName + "', 'table',  default, null, default) union " +
                        "select objname as table_name, value as table_comment from fn_listextendedproperty( 'MS_Description', " +
                        "'schema', '" + schemaName + "', 'view',  default, null, default);";
                Map<String, String> tableViewComments = new HashMap<>();

                String tableviewSql = "select table_name, table_type from information_schema.tables " +
                        "where table_catalog = '" + dbDefn.getDatabaseName() + "' " +
                        "and table_schema = '" + schemaDefn.getSchemaName() + "' order by table_name;";

                try {
                    // 3.2.1. get table comments
                    Statement stmt = cnxn.createStatement();
                    ResultSet rs = stmt.executeQuery(tableviewCommentSql);
                    while(rs.next()) {
                        String key = schemaDefn.getSchemaName() + "." + rs.getString("table_name");
                        String comment = rs.getString("table_comment");
                        if (comment != null) {
                            tableViewComments.put(key, comment);
                        }
                    }
                    rs.close();

                    // 3.2.2. get table/view definitions
                    rs = stmt.executeQuery(tableviewSql);
                    while(rs.next()) {
                        String key = schemaDefn.getSchemaName() + "." + rs.getString("table_name");
                        String comment = "";

                        if (tableViewComments.containsKey(key)) {
                            comment = tableViewComments.get(key);
                        }

                        SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition();
                        tableViewDefn.setSchemaName(schemaDefn.getSchemaName());
                        tableViewDefn.setTableViewName(rs.getString("table_name"));
                        tableViewDefn.setTableViewShortDesc(comment);
                        String tableType = rs.getString("table_type");
                        if (tableType.equals("BASE TABLE")) {
                            tableViewDefn.setClassCode("DB_TABLE");
                        } else if (tableType.equals("VIEW")) {
                            tableViewDefn.setClassCode("DB_VIEW");
                        }
                        else {
                            tableViewDefn.setClassCode("DB_TABLE");     // TODO: should probably be 'UNKNOWN'
                        }
                        schemaDefn.addTableView(tableViewDefn);
                    }

                }
                catch (SQLException e) {
                    revEngResult.setErrorNumber(-200101);
                    revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve table and view definitions.");
                    e.printStackTrace();
                }

                // 3.3. Columns
                if (revEngResult.getErrorNumber() == 0) {
                    for (SmdTableViewDefinition tableViewDefn : schemaDefn.getTableViewList()) {
                        String columnSql = "select table_name, column_name, ordinal_position, column_default, is_nullable, data_type, " +
                                "character_maximum_length, numeric_precision, numeric_scale, datetime_precision " +
                                "from information_schema.columns where table_schema = '" + schemaName + "' " +
                                "and table_name = '" + tableViewDefn.getTableViewName() + "' order by ordinal_position;";

                        String pkSql = "select tc.constraint_name, kcu.table_name, kcu.column_name from information_schema.table_constraints tc " +
                                "join information_schema.key_column_usage kcu on kcu.table_schema = tc.table_schema " +
                                "and kcu.table_name = tc.table_name and kcu.constraint_name = tc.constraint_name " +
                                "where tc.table_schema = '" + schemaName + "' and tc.table_name = '" +
                                tableViewDefn.getTableViewName() + "' and tc.constraint_type = 'PRIMARY KEY';";

                        String commentSql = "select objname as column_name, value as column_comment " +
                                "from fn_listextendedproperty('MS_Description', 'schema', '" + schemaName + "'," +
                                "'table',  '" + tableViewDefn.getTableViewName() + "', 'column', default);";

                        try {
                            // 3.3.1. get primary key information
                            Statement stmt = cnxn.createStatement();

                            ResultSet rs = stmt.executeQuery(pkSql);
                            HashMap<String, String> pkColumns = new HashMap<String, String>();
                            while (rs.next()) {
                                pkColumns.put(rs.getString("column_name"), "PK");
                            }

                            // 3.3.2. get column comments
                            rs = stmt.executeQuery(commentSql);
                            HashMap<String, String> colComments = new HashMap<String, String>();
                            while (rs.next()) {
                                colComments.put(rs.getString("column_name"), rs.getString("column_comment"));
                            }

                            // 3.3.3. get column definition
                            rs = stmt.executeQuery(columnSql);
                            while (rs.next()) {
                                SmdColumnDefinition columnDefn = new SmdColumnDefinition();

                                // generate datatype description
                                String datatype = rs.getString("data_type");
                                if (datatype.equals("numeric") && rs.getInt("numeric_precision") > 0) {
                                    datatype = String.format("%s (%d,%d)", datatype, rs.getInt("numeric_precision"), rs.getInt("numeric_scale"));
                                }
                                if (rs.getInt("character_maximum_length") > 0) {
                                    datatype = String.format("%s (%d)", datatype, rs.getInt("character_maximum_length"));
                                }

                                columnDefn.setColumnName(rs.getString("column_name"));
                                columnDefn.setTableViewName(rs.getString("table_name"));
                                columnDefn.setColumnOrdinal(rs.getInt("ordinal_position"));
                                columnDefn.setColumnDatatype(datatype);
                                if (rs.getString("is_nullable").equals("NO"))
                                    columnDefn.setNullable(false);
                                else
                                    columnDefn.setNullable(true);
                                if (pkColumns.containsKey(rs.getString("column_name")))
                                    columnDefn.setPrimaryKey(true);

                                if (colComments.containsKey(rs.getString("column_name")))
                                    columnDefn.setColumnShortDesc(colComments.get(rs.getString("column_name")));

                                tableViewDefn.addColumnDefinition(columnDefn);
                            }

                        }
                        catch (SQLException e) {
                            revEngResult.setErrorNumber(-200102);
                            revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve column definitions for table " + tableViewDefn.getTableViewName());
                            e.printStackTrace();
                        }
                    }
                }


                // 3.4. FK Relationships
                if (revEngResult.getErrorNumber() == 0) {
                    String relationshipSql = "select KCU1.CONSTRAINT_NAME as 'fk_constraint_name', " +
                            "KCU1.TABLE_NAME as 'child_table_name', KCU1.COLUMN_NAME as 'child_column_name', " +
                            "KCU1.ORDINAL_POSITION AS 'child_column_ordinal', KCU2.TABLE_SCHEMA as 'parent_table_schema', KCU2.TABLE_NAME as 'parent_table_name', " +
                            "KCU2.COLUMN_NAME AS 'parent_column_name', KCU2.ORDINAL_POSITION as 'parent_column_ordinal' " +
                            "from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC " +
                            "join INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU1 on KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG " +
                            "and KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA and KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME " +
                            "join INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU2 on KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG " +
                            "and KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA and KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME " +
                            "and KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION " +
                            "where RC.CONSTRAINT_SCHEMA = '" + schemaName + "' order by fk_constraint_name, child_column_ordinal;";

                    try {
                        Statement stmt = cnxn.createStatement();
                        ResultSet rs = stmt.executeQuery(relationshipSql);

                        String previousFKname = "";
                        String currentFKname = "";
                        SmdRelationshipDefinition relDefn = null;

                        // 3.4.1. process FK relationship info in a control break on FK name
                        while (rs.next()) {
                            currentFKname = rs.getString("fk_constraint_name");

                            if (!currentFKname.equals(previousFKname)) {
                                // add the completed relationship to schema
                                if (relDefn != null)
                                    schemaDefn.addRelationship(relDefn);

                                // create new SMD Relationship Definition
                                relDefn = new SmdRelationshipDefinition();
                                relDefn.setRelationshipName(currentFKname);
                                relDefn.setParentTableViewSchemaName(rs.getString("parent_table_schema"));
                                relDefn.setParentTableViewName(rs.getString("parent_table_name"));
                                relDefn.setChildTableViewSchemaName(schemaName);
                                relDefn.setChildTableViewName(rs.getString("child_table_name"));

                                SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                                relColPair.setParentColumnName(rs.getString("parent_column_name"));
                                relColPair.setChildColumnName(rs.getString("child_column_name"));
                                relDefn.addColumnPair(relColPair);
                            }
                            else {
                                SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                                relColPair.setParentColumnName(rs.getString("parent_column_name"));
                                relColPair.setChildColumnName(rs.getString("child_column_name"));
                                relDefn.addColumnPair(relColPair);
                            }
                            previousFKname = currentFKname;
                        }
                        // add the completed relationship to schema
                        if (relDefn != null)
                            schemaDefn.addRelationship(relDefn);
                    }
                    catch (SQLException e) {
                        revEngResult.setErrorNumber(-200103);
                        revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve foreign key definitions.");
                        e.printStackTrace();
                    }
                }

                if (revEngResult.getErrorNumber() == 0) {
                    dbDefn.addSchema(schemaDefn);
                }

            }
        }

        return revEngResult;
    };

    /**
     * Returns the reverse engineered database catalog
     * @return
     * @throws Exception
     */
    public SmdDatabaseDefinition getReverseEngineerCatalog () throws Exception {
        return dbDefn;
    };

}
