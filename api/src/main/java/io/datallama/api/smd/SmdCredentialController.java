/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.common.CredentialType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
public class SmdCredentialController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.security.crypt.key}")
    private String cryptKey;

    /**
     * Reads the list of credential types defined in the DL DB
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/credential/type_list")
    public Map<String, Object> readCredentialTypeList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList<CredentialType> credentialTypeList = new ArrayList<CredentialType>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_credential_type_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object dbResult : dbResultList) {
            LinkedCaseInsensitiveMap credentialProviderInfo = (LinkedCaseInsensitiveMap) dbResult;
            CredentialType credentialType = new CredentialType(
                    (Integer) credentialProviderInfo.get("credential_type_id"),
                    credentialProviderInfo.get("credential_type_name").toString(),
                    credentialProviderInfo.get("credential_type_shortdesc").toString()
            );
            credentialType.setCredentialTemplate(credentialProviderInfo.get("credential_template").toString());
            credentialType.setSchemaDefinition(credentialProviderInfo.get("schema_definition").toString());
            credentialType.setFormDefinition(credentialProviderInfo.get("form_definition").toString());
            credentialTypeList.add(credentialType);
        }

        results.put("result_data", credentialTypeList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the list of credentials defined in the DL DB. Credentials are used to connect to implementations for reverse engineering.
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/credential/list")
    public Map<String, Object> readCredentialList() {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList<SmdCredential> credentialList = new ArrayList<SmdCredential>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_credential_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object dbResult : dbResultList) {
            LinkedCaseInsensitiveMap credentialInfo = (LinkedCaseInsensitiveMap) dbResult;
            SmdCredential smdCredential = new SmdCredential();
            smdCredential.setCredentialId((Integer) credentialInfo.get("credential_id"));
            smdCredential.setCredentialTypeId((Integer) credentialInfo.get("credential_type_id"));
            smdCredential.setCredentialName(credentialInfo.get("credential_name").toString());
            smdCredential.setCredentialShortDesc(credentialInfo.get("credential_shortdesc").toString());

            CredentialType credentialType = new CredentialType();
            credentialType.setCredentialTypeId((Integer) credentialInfo.get("credential_type_id"));
            credentialType.setCredentialTypeName(credentialInfo.get("credential_type_name").toString());

            smdCredential.setCredentialType(credentialType);

            credentialList.add(smdCredential);
        }

        results.put("result_data", credentialList);
        results.put("result_status", resultStatus);

        return results;
    }

    @RequestMapping(method = GET, value = "/smd/credential/list/forconnector/{connectorId}")
    public Map<String, Object> readRevEngCredentialListForConnector(@PathVariable Integer connectorId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        ArrayList<SmdCredential> credentialList = new ArrayList<SmdCredential>();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_credential_list_for_connector");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_connector_id", connectorId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object dbResult : dbResultList) {
            LinkedCaseInsensitiveMap credentialInfo = (LinkedCaseInsensitiveMap) dbResult;
            SmdCredential smdCredential = new SmdCredential();
            smdCredential.setCredentialId((Integer) credentialInfo.get("credential_id"));
            smdCredential.setCredentialTypeId((Integer) credentialInfo.get("credential_type_id"));
            smdCredential.setCredentialName(credentialInfo.get("credential_name").toString());
            smdCredential.setCredentialShortDesc(credentialInfo.get("credential_shortdesc").toString());

            credentialList.add(smdCredential);
        }

        results.put("result_data", credentialList);
        results.put("result_status", resultStatus);

        return results;

    }


    /**
     * Writes details of a new credential into the DL DB
     * @param smdCredential
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/credential")
    public Map<String, Object> writeCredential(@RequestBody SmdCredential smdCredential, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        Integer newFosCredentialId = -1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_credential");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_credential_id", newFosCredentialId, Types.INTEGER)
                .addValue("_credential_name", smdCredential.getCredentialName(), Types.VARCHAR)
                .addValue("_credential_shortdesc", smdCredential.getCredentialShortDesc(), Types.VARCHAR)
                .addValue("_credential_type_id", smdCredential.getCredentialTypeId(), Types.INTEGER)
                .addValue("_credential_detail", smdCredential.getCredentialDetail(), Types.VARCHAR)
                .addValue("_key", cryptKey, Types.VARCHAR)
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        Integer returnId = (Integer) out.get("_return_id");

        if (returnId < 0) {
            resultStatus = returnId;
        }

        resultData.put("returnId", returnId);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Updates summary information (i.e. short description) in the DL DB for the credential identified by credentialId
     * @param smdCredential
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/smd/credential/summary")
    public Map<String, Object> writeCredentialSummary(@RequestBody SmdCredential smdCredential, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_credential_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_credential_id", smdCredential.getCredentialId(), Types.INTEGER)
                .addValue("_credential_shortdesc", smdCredential.getCredentialShortDesc(), Types.VARCHAR)
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * Reads summary information for the credential identified by {credentialId}.
     * @param credentialId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/credential/{credentialId}")
    public Map<String, Object> readCredential(@PathVariable Integer credentialId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Map<String, Object> visibleCredentialDetail = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // 1. get the credential from the DL DB
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_credential");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_credential_id", credentialId, Types.INTEGER)
                .addValue("_key", cryptKey, Types.VARCHAR);
        Map<String, Object> out = jdbcCall.execute(in);

        // 1.1. parse the JSON data for credential details and visible fields
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the credential_detail json string to Java LinkedHashMap
        LinkedHashMap<String, Object> credentialDetail = null;
        ArrayList<String> visibleFields = null;

        try {
            credentialDetail = objectMapper.readValue(out.get("_credential_detail").toString(), LinkedHashMap.class);
            visibleFields = objectMapper.readValue(out.get("_visible_fields").toString(), ArrayList.class);
        }
        catch (Exception e) {
            resultStatus = -1;
            System.out.println(e.getMessage());
        }

        // 2. collate information about credential
        if (resultStatus == 1) {
            // 2.1. iterate over credential detail
            for (String visibleField : visibleFields) {
                if (credentialDetail.containsKey(visibleField)) {
                    visibleCredentialDetail.put(visibleField, credentialDetail.get(visibleField));
                }
            }
            resultData.put("visibleCredentialDetail", visibleCredentialDetail);
            resultData.put("credentialId", credentialId);
            resultData.put("credentialTypeId", (Integer) out.get("_credential_type_id"));
            resultData.put("credentialName", out.get("_credential_name").toString());
            resultData.put("credentialShortDesc", out.get("_credential_shortdesc").toString());
            resultData.put("credentialTypeName", out.get("_credential_type_name").toString());
            resultData.put("credentialTypeShortDesc", out.get("_credential_type_shortdesc").toString());
        }

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Runs a connection test for the credential specified by {credentialId}
     * @param credentialId
     * @return
     */
    @RequestMapping(method = GET, value = "/smd/credential/{credentialId}/testconnection")
    public Map<String, Object> testCredentialConnection(@PathVariable Integer credentialId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        // TODO: implement generic credential connection test. This might be complicated if credential types can be shared by multiple connectors.

        results.put("result_status", resultStatus);
        return results;
    }

}
