/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class SmdTableViewDefinition {
    private Integer tableViewId;
    private Integer classId;
    private Integer databaseId;
    private Integer schemaId;
    private String className;
    private String classCode;
    private String tableViewName;
    private String objectGUID;
    private String schemaName;
    private String tableViewShortDesc;
    private Integer columnCount;
    private ArrayList<SmdColumnDefinition> columnList;
    private Integer columnCustomFormId = 0;
    private String columnCustomFormName = "";
    private ArrayList<DlMetaDataContextNode> mdContext;
    private DlCustomForm customForm;
    private ArrayList<DlDataSensitivityTag> dlDsTagList;

    /**
     * Default constructor. No parameters.
     */
    public SmdTableViewDefinition() {
        this.tableViewId = 0;
        this.classId = 0;
        this.schemaId = 0;
        this.databaseId = 0;
        this.className = "";
        this.classCode = "";
        this.tableViewName = "";
        this.objectGUID = "";
        this.schemaName = "";
        this.tableViewShortDesc = "";
        this.columnCount = 0;
        this.columnList = new ArrayList<SmdColumnDefinition>();
        this.columnCustomFormId = 0;
        this.columnCustomFormName = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customForm = new DlCustomForm();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
    }

    /**
     * Constructor 01. Parameters for core members.
     * @param tableViewId
     * @param classId
     * @param schemaId
     * @param tableViewName
     */
    public SmdTableViewDefinition(Integer tableViewId, Integer classId, Integer schemaId, String tableViewName) {
        this.tableViewId = tableViewId;
        this.classId = classId;
        this.databaseId = 0;
        this.schemaId = schemaId;
        this.className = "";
        this.classCode = "";
        this.tableViewName = tableViewName;
        this.objectGUID = "";
        this.schemaName = "";
        this.tableViewShortDesc = "";
        this.columnCount = 0;
        this.columnList = new ArrayList<SmdColumnDefinition>();
        this.columnCustomFormId = 0;
        this.columnCustomFormName = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customForm = new DlCustomForm();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
    }

    public Integer getTableViewId() {
        return this.tableViewId;
    }

    public void setTableViewId(Integer tableViewId) {
        this.tableViewId = tableViewId;
    }

    public Integer getClassId() { return classId; }

    public void setClassId(Integer classId) { this.classId = classId; }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Integer databaseId) {
        this.databaseId = databaseId;
    }

    public Integer getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getTableViewName() {
        return this.tableViewName;
    }

    public void setTableViewName(String tableViewName) {
        this.tableViewName = tableViewName;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableViewShortDesc() {
        return tableViewShortDesc;
    }

    public void setTableViewShortDesc(String tableViewShortDesc) {
        this.tableViewShortDesc = tableViewShortDesc;
    }

    public Integer getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(Integer columnCount) {
        this.columnCount = columnCount;
    }

    public ArrayList<SmdColumnDefinition> getColumnList() {
        return this.columnList;
    }

    public void setColumnList(ArrayList<SmdColumnDefinition> columnList) {
        this.columnList = columnList;
    }

    public void addColumnDefinition(SmdColumnDefinition columnDefinition) {
        this.columnList.add(columnDefinition);
    }

    public Integer getColumnCustomFormId() {
        return columnCustomFormId;
    }

    public void setColumnCustomFormId(Integer columnCustomFormId) {
        this.columnCustomFormId = columnCustomFormId;
    }

    public String getColumnCustomFormName() {
        return columnCustomFormName;
    }

    public void setColumnCustomFormName(String columnCustomFormName) {
        this.columnCustomFormName = columnCustomFormName;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public ArrayList<DlDataSensitivityTag> getDlDsTagList() {
        return dlDsTagList;
    }

    public void setDlDsTagList(ArrayList<DlDataSensitivityTag> dlDsTagList) {
        this.dlDsTagList = dlDsTagList;
    }

    public void addDlDsTag(DlDataSensitivityTag dlDsTag) {
        this.dlDsTagList.add(dlDsTag);
    }
}
