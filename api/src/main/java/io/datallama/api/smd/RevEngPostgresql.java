/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class RevEngPostgresql implements ReverseEngineerDatabase {
    private SmdDatabaseDefinition dbDefn;

    public RevEngPostgresql(){
        this.dbDefn = new SmdDatabaseDefinition();
    };

    /**
     * Reverse engineers all tables, views and foreign key relationships in the database specified by dbCredentials.
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public SmdDbCredentials testDatabaseConnection (SmdDbCredentials dbCredentials) throws Exception {
        Boolean classFound = true;
        Connection cnxn;

        // 1. try to load the mysql JDBC driver
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setConnectionTestResult(false);
            dbCredentials.setSqlError("Class not found: org.postgresql.Driver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:postgresql://%s:%d/%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());

            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());

                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
            }
        }

        return dbCredentials;
    };

    /**
     * Reverse engineers all tables, views and relationships in the database identified by dbCredentials
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public RevEngResult reverseEngineerDatabase (SmdDbCredentials dbCredentials, String[] schemaNameList) throws Exception {
        RevEngResult revEngResult = new RevEngResult();
        Boolean classFound = true;
        Connection cnxn = null;

        // 1. try to load the mysql JDBC driver
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setSqlError("Class not found: org.postgresql.Driver");
            revEngResult.setErrorNumber(-200199);
            revEngResult.setGeneralErrorMessage("ERROR connecting to database. Class not found: org.postgresql.Driver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:postgresql://%s:%d/%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());
            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());
                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
                revEngResult.setErrorNumber(-200100);
                revEngResult.setGeneralErrorMessage(failureMessage);
            }
        }

        // 3. execute the reverse engineer
        if (dbCredentials.getConnectionTestResult()) {
            dbDefn.setDatabaseName(dbCredentials.getDbName());
            SmdSchemaDefinition schemaDefn = null;

            // 3.1. Iterate the schema name list and process each schema for Tables/Views
            for (String schemaName : schemaNameList) {
                schemaDefn = new SmdSchemaDefinition();
                schemaDefn.setSchemaName(schemaName);

                // 3.2 Tables & Views
                String tableviewCommentSql = "select distinct pgn.nspname, t.table_name, pg_catalog.obj_description(pgc.oid, 'pg_class') as table_comment " +
                        "from information_schema.tables t inner join pg_catalog.pg_class pgc on t.table_name = pgc.relname " +
                        "inner join pg_catalog.pg_namespace pgn on pgc.relnamespace = pgn.oid where pgn.nspname = '" + schemaDefn.getSchemaName() + "';";
                Map<String, String> tableViewComments = new HashMap<>();

                String tableviewSql = "select table_name, table_type from information_schema.tables " +
                        "where table_catalog = '" + dbDefn.getDatabaseName() + "' " +
                        "and table_schema = '" + schemaDefn.getSchemaName() + "' order by table_name;";

                try {
                    // 3.2.1. get table comments
                    Statement stmt = cnxn.createStatement();
                    ResultSet rs = stmt.executeQuery(tableviewCommentSql);
                    while (rs.next()) {
                        String key = schemaDefn.getSchemaName() + "." + rs.getString("table_name");
                        String comment = rs.getString("table_comment");
                        if (comment != null) {
                            tableViewComments.put(key, comment);
                        }
                    }
                    rs.close();

                    // 3.2.2. get table/view definitions
                    rs = stmt.executeQuery(tableviewSql);
                    while (rs.next()) {
                        String key = schemaDefn.getSchemaName() + "." + rs.getString("table_name");
                        String comment = "";

                        if (tableViewComments.containsKey(key)) {
                            comment = tableViewComments.get(key);
                        }

                        SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition();
                        tableViewDefn.setSchemaName(schemaDefn.getSchemaName());
                        tableViewDefn.setTableViewName(rs.getString("table_name"));
                        tableViewDefn.setTableViewShortDesc(comment);
                        String tableType = rs.getString("table_type");
                        if (tableType.equals("BASE TABLE")) {
                            tableViewDefn.setClassCode("DB_TABLE");
                        } else if (tableType.equals("VIEW")) {
                            tableViewDefn.setClassCode("DB_VIEW");
                        } else {
                            tableViewDefn.setClassCode("DB_TABLE");     // TODO: should probably be 'UNKNOWN'
                        }
                        schemaDefn.addTableView(tableViewDefn);
                    }
                }
                catch (SQLException e) {
                    revEngResult.setErrorNumber(-200101);
                    revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve table and view definitions.");
                    e.printStackTrace();
                }

                // 3.3. Columns
                if (revEngResult.getErrorNumber() == 0) {
                    for (SmdTableViewDefinition tableViewDefn : schemaDefn.getTableViewList()) {
                        String columnSql = "select table_name, column_name, ordinal_position, column_default, is_nullable, data_type, " +
                                "character_maximum_length, numeric_precision, numeric_scale, datetime_precision " +
                                "from information_schema.columns where table_schema = '" + schemaName + "' " +
                                "and table_name = '" + tableViewDefn.getTableViewName() + "' order by ordinal_position;";

                        String pkSql = "select tc.constraint_name, kcu.table_name, kcu.column_name from information_schema.table_constraints tc " +
                                "join information_schema.key_column_usage kcu on kcu.table_schema = tc.table_schema " +
                                "and kcu.table_name = tc.table_name and kcu.constraint_name = tc.constraint_name " +
                                "where tc.table_schema = '" + schemaName + "' and tc.table_name = '" +
                                tableViewDefn.getTableViewName() + "' and tc.constraint_type = 'PRIMARY KEY';";

                        String commentSql = "select attname as column_name, col_description(attrelid, attnum) as description " +
                                "from pg_attribute where attrelid = '" + schemaName + "." + tableViewDefn.getTableViewName() + "'::regclass " +
                                "and attnum > 0 and not attisdropped and col_description(attrelid, attnum) is not null";

                        try {
                            Statement stmt = cnxn.createStatement();

                            ResultSet rs = stmt.executeQuery(pkSql);
                            HashMap<String, String> pkColumns = new HashMap<String, String>();
                            while (rs.next()) {
                                pkColumns.put(rs.getString("column_name"), "PK");
                            }

                            rs = stmt.executeQuery(commentSql);
                            HashMap<String, String> colComments = new HashMap<String, String>();
                            while (rs.next()) {
                                colComments.put(rs.getString("column_name"), rs.getString("description"));
                            }

                            rs = stmt.executeQuery(columnSql);
                            while (rs.next()) {
                                SmdColumnDefinition columnDefn = new SmdColumnDefinition();

                                // generate datatype description
                                String datatype = rs.getString("data_type");
                                if (datatype.equals("numeric") && rs.getInt("numeric_precision") > 0) {
                                    datatype = String.format("%s (%d,%d)", datatype, rs.getInt("numeric_precision"), rs.getInt("numeric_scale"));
                                }
                                if (rs.getInt("character_maximum_length") > 0) {
                                    datatype = String.format("%s (%d)", datatype, rs.getInt("character_maximum_length"));
                                }

                                columnDefn.setColumnName(rs.getString("column_name"));
                                columnDefn.setTableViewName(rs.getString("table_name"));
                                columnDefn.setColumnOrdinal(rs.getInt("ordinal_position"));
                                columnDefn.setColumnDatatype(datatype);
                                if (rs.getString("is_nullable").equals("NO"))
                                    columnDefn.setNullable(false);
                                else
                                    columnDefn.setNullable(true);
                                if (pkColumns.containsKey(rs.getString("column_name")))
                                    columnDefn.setPrimaryKey(true);

                                if (colComments.containsKey(rs.getString("column_name")))
                                    columnDefn.setColumnShortDesc(colComments.get(rs.getString("column_name")));

                                tableViewDefn.addColumnDefinition(columnDefn);
                            }

                        }
                        catch (SQLException e) {
                            revEngResult.setErrorNumber(-200102);
                            revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve column definitions for table " + tableViewDefn.getTableViewName());
                            e.printStackTrace();
                        }
                    }
                }

                // 3.4. FK Relationships
                if (revEngResult.getErrorNumber() == 0) {
                    String relationshipSql = "select constraint_catalog, constraint_schema, constraint_name, table_catalog, table_schema, table_name " +
                            "from information_schema.table_constraints where constraint_catalog = '"+ dbDefn.getDatabaseName() + "' " +
                            "and constraint_schema = '" + schemaName + "' and constraint_type = 'FOREIGN KEY';";

                    // The SQL command template for retrieving column pairs for an RK relationship.
                    // pgc.conname = relationship name, pgn.nspname = schema name
                    String relColumnsSql = "select conrelid::regclass as source_table, sc.attname as source_column, " +
                            "confrelid::regclass as target_table, tc.attname as target_column " +
                            "from (select pgc.conname, pgn.nspname, pgc.contype, pgc.conrelid, pgc.confrelid, " +
                            "unnest(pgc.conkey) as conkey, unnest(pgc.confkey) as confkey " +
                            "from pg_constraint pgc join pg_namespace pgn on pgc.connamespace = pgn.oid " +
                            "where pgc.conname = '%s' and pgn.nspname = '%s') sub " +
                            "join pg_attribute as sc on sc.attrelid = conrelid and sc.attnum = conkey " +
                            "join pg_attribute as tc on tc.attrelid = confrelid and tc.attnum = confkey;";

                    // 3.4.1. Process the set of FK relationships in the current schema
                    try {
                        Statement stmt = cnxn.createStatement();
                        ResultSet rs = stmt.executeQuery(relationshipSql);

                        String fkName = "";
                        SmdRelationshipDefinition relDefn = null;

                        while (rs.next()) {
                            fkName = rs.getString("constraint_name");

                            // create new SMD Relationship Definition
                            relDefn = new SmdRelationshipDefinition();
                            relDefn.setRelationshipName(fkName);

                            // get the set of column pairs for this FK relationship.
                            //  The
                            Statement stmt02 = cnxn.createStatement();
                            ResultSet rs02 = stmt02.executeQuery(String.format(relColumnsSql, fkName, schemaName));

                            while (rs02.next()) {
                                String[] sourceTablePath = rs02.getString("source_table").split("\\.");
                                String[] targetTablePath = rs02.getString("target_table").split("\\.");
                                relDefn.setChildTableViewSchemaName(sourceTablePath[0]);
                                relDefn.setChildTableViewName(sourceTablePath[1]);
                                relDefn.setParentTableViewSchemaName(targetTablePath[0]);
                                relDefn.setParentTableViewName(targetTablePath[1]);

                                SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                                relColPair.setParentColumnName(rs02.getString("target_column"));
                                relColPair.setChildColumnName(rs02.getString("source_column"));
                                relDefn.addColumnPair(relColPair);
                            }
                            // add the completed relationship to schema
                            if (relDefn != null) {
                                schemaDefn.addRelationship(relDefn);
                            }
                        }
                    }
                    catch (SQLException e) {
                        revEngResult.setErrorNumber(-200103);
                        revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve foreign key definitions.");
                        e.printStackTrace();
                    }
                }

                if (revEngResult.getErrorNumber() == 0) {
                    dbDefn.addSchema(schemaDefn);
                }

            }
        }

        return revEngResult;
    };

    /**
     * Returns the reverse engineered database catalog
     * @return
     * @throws Exception
     */
    public SmdDatabaseDefinition getReverseEngineerCatalog () throws Exception {
        return dbDefn;
    };

}
