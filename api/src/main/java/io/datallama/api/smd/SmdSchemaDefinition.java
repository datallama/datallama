/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class SmdSchemaDefinition {
    private Integer schemaId;
    private Integer classId;
    private Integer databaseId;
    private String schemaName;
    private String className;
    private String objectGUID;
    private String schemaShortDesc;
    private ArrayList<SmdTableViewDefinition> tableViewList;
    private ArrayList<SmdRelationshipDefinition> relationshipList;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private DlCustomForm customForm;
    private ArrayList<DlDataSensitivityTag> dlDsTagList;

    public SmdSchemaDefinition() {
        this.schemaId = 0;
        this.classId = 0;
        this.databaseId = 0;
        this.schemaName = "";
        this.className = "";
        this.objectGUID = "";
        this.schemaShortDesc = "";
        this.tableViewList = new ArrayList<SmdTableViewDefinition>();
        this.relationshipList = new ArrayList<SmdRelationshipDefinition>();
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customForm = new DlCustomForm();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
    }

    public SmdSchemaDefinition(Integer schemaId, Integer classId, Integer databaseId, String schemaName) {
        this.schemaId = schemaId;
        this.classId = classId;
        this.databaseId = databaseId;
        this.schemaName = schemaName;
        this.className = "";
        this.objectGUID = "";
        this.schemaShortDesc = "";
        this.tableViewList = new ArrayList<SmdTableViewDefinition>();
        this.relationshipList = new ArrayList<SmdRelationshipDefinition>();
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.customForm = new DlCustomForm();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
    }

    public Integer getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Integer schemaId) {
        this.schemaId = schemaId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Integer databaseId) {
        this.databaseId = databaseId;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getSchemaShortDesc() {
        return schemaShortDesc;
    }

    public void setSchemaShortDesc(String schemaShortDesc) {
        this.schemaShortDesc = schemaShortDesc;
    }

    public ArrayList<SmdTableViewDefinition> getTableViewList() {
        return tableViewList;
    }

    public void setTableViewList(ArrayList<SmdTableViewDefinition> tableViewList) {
        this.tableViewList = tableViewList;
    }

    public void addTableView(SmdTableViewDefinition tableViewDefinition) {
        this.tableViewList.add(tableViewDefinition);
    }

    public ArrayList<SmdRelationshipDefinition> getRelationshipList() {
        return relationshipList;
    }

    public void setRelationshipList(ArrayList<SmdRelationshipDefinition> relationshipList) {
        this.relationshipList = relationshipList;
    }

    public void addRelationship(SmdRelationshipDefinition relationshipDefinition) {
        this.relationshipList.add(relationshipDefinition);
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }

    public ArrayList<DlDataSensitivityTag> getDlDsTagList() {
        return dlDsTagList;
    }

    public void setDlDsTagList(ArrayList<DlDataSensitivityTag> dlDsTagList) {
        this.dlDsTagList = dlDsTagList;
    }

    public void addDlDsTag(DlDataSensitivityTag dlDsTag) {
        this.dlDsTagList.add(dlDsTag);
    }
}
