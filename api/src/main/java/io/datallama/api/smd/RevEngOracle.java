/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class RevEngOracle implements ReverseEngineerDatabase {
    private SmdDatabaseDefinition dbDefn;

    public RevEngOracle(){
        this.dbDefn = new SmdDatabaseDefinition();
    };

    /**
     * Reverse engineers all tables, views and foreign key relationships in the database specified by dbCredentials.
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public SmdDbCredentials testDatabaseConnection (SmdDbCredentials dbCredentials) throws Exception {
        Boolean classFound = true;
        Connection cnxn;

        // 1. try to load the oracle JDBC driver
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setConnectionTestResult(false);
            dbCredentials.setSqlError("Class not found: oracle.jdbc.driver.OracleDriver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:oracle:thin:@%s:%d/%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());

            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());

                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
            }
        }

        return dbCredentials;
    };

    /**
     * Reverse engineers all tables, views and relationships in the database identified by dbCredentials
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public RevEngResult reverseEngineerDatabase (SmdDbCredentials dbCredentials, String[] schemaNameList) throws Exception {
        RevEngResult revEngResult = new RevEngResult();
        Boolean classFound = true;
        Connection cnxn = null;

        // 1. connect to the Oracle database
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        }
        catch (ClassNotFoundException e) {
            classFound = false;
            dbCredentials.setConnectionTestResult(false);
            dbCredentials.setSqlError("Class not found: oracle.jdbc.driver.OracleDriver");
        }

        // 2. driver loaded so try to connect
        if (classFound) {
            String jdbcURL = String.format("jdbc:oracle:thin:@%s:%d/%s", dbCredentials.getHostname(), dbCredentials.getDbPort(), dbCredentials.getDbName());

            // try to connect to the target MySQL database. If connection fails the invoking function will catch the exception.
            try {
                cnxn = DriverManager.getConnection(jdbcURL, dbCredentials.getUsername(), dbCredentials.getPassword());
                dbCredentials.setConnectionTestResult(true);
            }
            catch (SQLException e) {
                String failureMessage = String.format("Message: %s, SQL State: %s, Cause: %s", e.getMessage(), e.getSQLState(), e.getCause());

                dbCredentials.setConnectionTestResult(false);
                dbCredentials.setSqlError(failureMessage);
            }
        }


        // 3. execute the reverse engineer
        if (dbCredentials.getConnectionTestResult()) {
            dbDefn.setDatabaseName(dbCredentials.getDbName());
            SmdSchemaDefinition schemaDefn = null;

            // 3.1. Iterate the schema name list and process each schema for Tables/Views
            for (String schemaName : schemaNameList) {
                schemaDefn = new SmdSchemaDefinition();
                schemaDefn.setSchemaName(schemaName);

                // 3.2 Tables & Views
                String tableviewSql = "select table_name, comments from all_tab_comments where owner = '" + schemaName + "'";
                String viewSql = "select view_name from all_views where owner = '" + schemaName + "'";
                Map<String, String> viewList = new HashMap<>();

                try {
                    // 3.2.1. get view list
                    Statement stmt = cnxn.createStatement();
                    ResultSet rs = stmt.executeQuery(viewSql);
                    while (rs.next()) {
                        String key = rs.getString("view_name");
                        viewList.put(key, key);
                    }
                    rs.close();

                    // 3.2.2. get table/view definitions with comments
                    rs = stmt.executeQuery(tableviewSql);
                    while (rs.next()) {
                        String tableViewName = rs.getString("table_name");

                        String tableViewClass = "DB_TABLE";
                        if (viewList.containsKey(tableViewName)) {
                            tableViewClass = "DB_VIEW";
                        }

                        SmdTableViewDefinition tableViewDefn = new SmdTableViewDefinition();
                        tableViewDefn.setSchemaName(schemaDefn.getSchemaName());
                        tableViewDefn.setTableViewName(rs.getString("table_name"));
                        tableViewDefn.setTableViewShortDesc(rs.getString("comments"));
                        tableViewDefn.setClassCode(tableViewClass);

                        schemaDefn.addTableView(tableViewDefn);
                    }
                }
                catch (SQLException e) {
                    revEngResult.setErrorNumber(-200101);
                    revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve table and view definitions.");
                    e.printStackTrace();
                }

                // 3.3. Columns
                if (revEngResult.getErrorNumber() == 0) {
                    for (SmdTableViewDefinition tableViewDefn : schemaDefn.getTableViewList()) {
                        String columnSql = "select k.table_name, k.column_name, k.column_id, k.data_type, k.nullable, " +
                                "k.data_scale, k.data_precision, k.data_length, c.comments " +
                                "from all_tab_cols k left outer join all_col_comments c on " +
                                "k.table_name = c.table_name and k.column_name = c.table_name " +
                                "where k.table_name = '" + tableViewDefn.getTableViewName() + "' " +
                                "and k.owner = '" + schemaName + "' order by k.column_id";

                        String pkSql = "select a1.constraint_name, a2.column_name, a2.position from all_constraints a1 " +
                                "join all_cons_columns a2 on a1.constraint_name = a2.constraint_name " +
                                "where a1.owner = '" + schemaName + "' and a1.table_name = '" + tableViewDefn.getTableViewName() + "' " +
                                "and a1.constraint_type = 'P' order by a2.position";

                        try {
                            Statement stmt = cnxn.createStatement();

                            ResultSet rs = stmt.executeQuery(pkSql);
                            HashMap<String, String> pkColumns = new HashMap<String, String>();
                            while (rs.next()) {
                                pkColumns.put(rs.getString("column_name"), "PK");
                            }

                            rs = stmt.executeQuery(columnSql);
                            while (rs.next()) {
                                SmdColumnDefinition columnDefn = new SmdColumnDefinition();

                                // generate datatype description
                                String datatype = rs.getString("data_type");
                                String datatypeSuffix = "";
                                Integer precision = rs.getInt("data_precision");
                                Integer scale = rs.getInt("data_scale");
                                Integer length = rs.getInt("data_length");
                                if (precision > 0) {
                                    if (scale > 0)
                                            datatypeSuffix = String.format("(%d,%d)", precision, scale);
                                    else
                                        datatypeSuffix = String.format("(%d)", precision);
                                }
                                else if (length > 0) {
                                    datatypeSuffix = String.format("(%d)", length);
                                }
                                datatype += datatypeSuffix;

                                columnDefn.setColumnName(rs.getString("column_name"));
                                columnDefn.setTableViewName(rs.getString("table_name"));
                                columnDefn.setColumnOrdinal(rs.getInt("column_id"));
                                columnDefn.setColumnDatatype(datatype);
                                if (rs.getString("nullable").toUpperCase().equals("N"))
                                    columnDefn.setNullable(false);
                                else
                                    columnDefn.setNullable(true);
                                if (pkColumns.containsKey(rs.getString("column_name")))
                                    columnDefn.setPrimaryKey(true);

                                columnDefn.setColumnShortDesc(rs.getString("comments"));

                                tableViewDefn.addColumnDefinition(columnDefn);
                            }

                        }
                        catch (SQLException e) {
                            revEngResult.setErrorNumber(-200102);
                            revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve column definitions for table " + tableViewDefn.getTableViewName());
                            e.printStackTrace();
                        }
                    }
                }

                // 3.4. FK Relationships
                if (revEngResult.getErrorNumber() == 0) {
                    String relationshipSql = "select ac.constraint_name, ac.owner as child_schema, ac.table_name as child_table, " +
                            "ccol.column_name as child_column, ccol.position as child_col_pos, pcol.owner as parent_schema, " +
                            "pcol.TABLE_NAME as parent_table, pcol.COLUMN_NAME as parent_column, pcol.position as parent_col_pos " +
                            "from all_constraints ac join all_cons_columns ccol on ac.CONSTRAINT_NAME = ccol.constraint_name " +
                            "join all_cons_columns pcol on ac.R_CONSTRAINT_NAME = pcol.CONSTRAINT_NAME and pcol.position = ccol.position " +
                            "where ac.owner = '" + schemaName + "' and ac.constraint_type = 'R' order by ac.constraint_name, ccol.position";


                    // 3.4.1. Process the set of FK relationships in the current schema
                    try {
                        Statement stmt = cnxn.createStatement();
                        ResultSet rs = stmt.executeQuery(relationshipSql);

                        String previousFKname = "";
                        String currentFKname = "";
                        SmdRelationshipDefinition relDefn = null;

                        // process FK relationship info in a control break on FK name
                        while (rs.next()) {
                            currentFKname = rs.getString("CONSTRAINT_NAME");

                            if (!currentFKname.equals(previousFKname)) {
                                // add the completed relationship to schema
                                if (relDefn != null)
                                    schemaDefn.addRelationship(relDefn);

                                // create new SMD Relationship Definition
                                relDefn = new SmdRelationshipDefinition();
                                relDefn.setRelationshipName(currentFKname);
                                relDefn.setParentTableViewSchemaName(rs.getString("PARENT_SCHEMA"));
                                relDefn.setParentTableViewName(rs.getString("PARENT_TABLE"));
                                relDefn.setChildTableViewSchemaName(rs.getString("CHILD_SCHEMA"));
                                relDefn.setChildTableViewName(rs.getString("CHILD_TABLE"));

                                SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                                relColPair.setParentColumnName(rs.getString("PARENT_COLUMN"));
                                relColPair.setChildColumnName(rs.getString("CHILD_COLUMN"));
                                relDefn.addColumnPair(relColPair);
                            }
                            else {
                                SmdRelationshipColumnPair relColPair = new SmdRelationshipColumnPair();
                                relColPair.setParentColumnName(rs.getString("PARENT_COLUMN"));
                                relColPair.setChildColumnName(rs.getString("CHILD_COLUMN"));
                                relDefn.addColumnPair(relColPair);
                            }
                            previousFKname = currentFKname;
                        }
                        // add the completed relationship to schema
                        if (relDefn != null)
                            schemaDefn.addRelationship(relDefn);
                    }
                    catch (SQLException e) {
                        revEngResult.setErrorNumber(-200103);
                        revEngResult.setGeneralErrorMessage("ERROR executing SQL statement to retrieve foreign key definitions.");
                        revEngResult.setSqlErrorCode(e.getErrorCode());
                        revEngResult.setSqlErrorMessage(e.getMessage());
                        e.printStackTrace();
                    }
                }

                if (revEngResult.getErrorNumber() == 0) {
                    dbDefn.addSchema(schemaDefn);
                }

            }
        }

        return revEngResult;
    };

    /**
     * Returns the reverse engineered database catalog
     * @return
     * @throws Exception
     */
    public SmdDatabaseDefinition getReverseEngineerCatalog () throws Exception {
        return dbDefn;
    };

}
