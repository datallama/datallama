/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import java.util.ArrayList;

/**
 * Models a container in an Object Store container. Used in the reverse engineering of an Object Store.
 */
public class ObjectStoreContainer {
    private Integer objectId;
    private Integer classId;
    private String containerName;
    private String containerShortDesc;
    private String containerProperties;
    private ArrayList<ObjectStoreDirectory> directoryList;

    /**
     * Default constructor with no parameters.
     */
    public ObjectStoreContainer() {
        this.objectId = 0;
        this.classId = 0;
        this.containerName = "";
        this.containerShortDesc = "";
        this.containerProperties = "";
        this.directoryList = new ArrayList<ObjectStoreDirectory>();
    }

    /**
     * Constructor with name parameter only
     * @param containerName
     */
    public ObjectStoreContainer(String containerName) {
        this.objectId = 0;
        this.classId = 0;
        this.containerName = containerName;
        this.containerShortDesc = "";
        this.containerProperties = "";
        this.directoryList = new ArrayList<ObjectStoreDirectory>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getContainerShortDesc() {
        return containerShortDesc;
    }

    public void setContainerShortDesc(String containerShortDesc) {
        this.containerShortDesc = containerShortDesc;
    }

    public String getContainerProperties() {
        return containerProperties;
    }

    public void setContainerProperties(String containerProperties) {
        this.containerProperties = containerProperties;
    }

    public ArrayList<ObjectStoreDirectory> getDirectoryList() {
        return directoryList;
    }

    public void setDirectoryList(ArrayList<ObjectStoreDirectory> directoryList) {
        this.directoryList = directoryList;
    }

    /**
     * Adds a new ObjectStoreDirectory object to directoryList
     * @param directory
     */
    public void addDirectory(ObjectStoreDirectory directory) {
        this.directoryList.add(directory);
    }
}
