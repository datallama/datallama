/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.association.AssociatedObject;
import io.datallama.api.common.*;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
public class SmdFileStoreController {

    @Autowired
    private DriverManagerDataSource dataSource;

    @Value("${dl.security.crypt.key}")
    private String cryptKey;

    /**
     * Returns the list of MO Classes
     * URL Path: /filestore/class/list
     *
     * @return Map<String, Object> results
     */
    @RequestMapping(method = GET, value = "/filestore/class/list")
    public Map<String, Object> listFileObjectStoreClasses() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList<DlMoClass> presentationClassList = new ArrayList();

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_fileobjectstore_classes");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object listElement : dbResultList) {
            LinkedCaseInsensitiveMap classInfo = (LinkedCaseInsensitiveMap) listElement;
            DlMoClass moClass = new DlMoClass(
                    (Integer) classInfo.get("class_id"),
                    classInfo.get("class_name").toString(),
                    classInfo.get("class_shortdesc").toString(),
                    classInfo.get("object_table").toString(),
                    classInfo.get("object_detail_state").toString()
            );
            presentationClassList.add(moClass);
        }

        results.put("result_data", presentationClassList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the list of all file or object stores metadata objects defined in the DL DB.
     * URL Path: /filestore/list
     */
    @RequestMapping(method = GET, value = "/filestore/list")
    @ResponseBody
    public Map<String, Object> listFileStores() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList filestoreList = new ArrayList(); // this list will be returned in the results Map

        // 1. call the DL DB stored proc to read all File/Object store top level md_objects
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_fos_list");
        Map<String, Object> out = jdbcCall.execute();
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        // 2. create a new Filestore object for each row in result set
        // TODO: The should be a parent class for Filestore and ObjectStoreSpace that can be used in ArrayLists etc
        for (Object listDirectory : dbResultList) {
            LinkedCaseInsensitiveMap directoryInfo = (LinkedCaseInsensitiveMap) listDirectory;
            FileStore filestore = new FileStore(
                    (Integer) directoryInfo.get("object_id"),
                    (Integer) directoryInfo.get("class_id"),
                    0,
                    directoryInfo.get("object_name").toString(),
                    directoryInfo.get("class_name").toString(),
                    directoryInfo.get("object_shortdesc").toString()
            );
            filestore.setObjectGUID(directoryInfo.get("object_guid").toString());

            filestoreList.add(filestore);
        }

        results.put("result_data", filestoreList);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads all directories in the file or object store identified by {filestoreId}
     * URL Path: /filestore/content/{filestoreId}/{abbreviate}
     */
    @RequestMapping(method = GET, value = "/filestore/content/{filestoreId}/{abbreviate}")
    @ResponseBody
    public Map<String, Object> readFileStoreContent(@PathVariable Integer filestoreId, @PathVariable Integer abbreviate) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.

        ArrayList filestoreDirectoryList = new ArrayList(); // this list will be returned in the results Map
        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        Map<Integer, FileStoreDirectory> parentLookup = new HashMap<>(); // this is used to lookup parent PTs when adding children

        // 1. get summary info for the file/object store
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_object_id", filestoreId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        FileStore filestore = new FileStore(
                filestoreId,
                (Integer) out.get("_class_id"),
                0,
                out.get("_object_name").toString(),
                out.get("_class_name").toString(),
                out.get("_object_shortdesc").toString()
        );
        filestore.setObjectGUID(out.get("_object_guid").toString());
        filestore.setCustomFormId((Integer) out.get("_custom_form_id"));
        filestore.setCustomFormName(out.get("_custom_form_name").toString());
        filestore.setSchemaDefinition(out.get("_schema_definition").toString());
        filestore.setFormDefinition(out.get("_form_definition").toString());

        // add the md_context
        DlMetaDataContext mdContext = new DlMetaDataContext();
        filestore.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        resultData.put("filestore_summary", filestore);

        // 2. get the reverse engineer connector & credential configuration
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_reveng_concred_config");
        in = new MapSqlParameterSource()
                .addValue("_object_id", filestoreId, Types.INTEGER)
                .addValue("_class_id", filestore.getClassId(), Types.INTEGER);

        out = jdbcCall.execute(in);

        RevEngConCredConfig conCredConfig = new RevEngConCredConfig();
        conCredConfig.setObjectId(filestoreId);
        conCredConfig.setClassId(filestore.getClassId());
        conCredConfig.setConnectorId((Integer) out.get("_connector_id"));
        conCredConfig.setConnectorName(out.get("_connector_name").toString());
        conCredConfig.setConnectorShortDesc(out.get("_connector_shortdesc").toString());
        conCredConfig.setCredentialId((Integer) out.get("_credential_id"));
        conCredConfig.setCredentialName(out.get("_credential_name").toString());
        conCredConfig.setCredentialShortDesc(out.get("_credential_shortdesc").toString());

        resultData.put("concred_config", conCredConfig);

        // 3. read the list of directories/containers for the file/object store
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_elements");
        in = new MapSqlParameterSource()
                .addValue("_object_id", filestoreId, Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        Integer resultSetSequence = 0;
        ObjectMapper objectMapper = new ObjectMapper();  // used to parse the md_context json array returned by stored procedure

        // 3.1. generate a FileStoreDirectory object for every row in result set and add to Array List filestoreDirectoryList
        for (Object listDirectory : dbResultList) {
            LinkedCaseInsensitiveMap directoryInfo = (LinkedCaseInsensitiveMap) listDirectory;
            FileStoreDirectory directory = new FileStoreDirectory(
                    filestoreId,
                    (Integer) directoryInfo.get("object_id"),
                    (Integer) directoryInfo.get("class_id"),
                    (Integer) directoryInfo.get("parent_element_object_id"),
                    directoryInfo.get("object_guid").toString(),
                    (Integer) directoryInfo.get("element_level"),
                    (Integer) directoryInfo.get("element_level_sequence"),
                    directoryInfo.get("element_hierarchy_number").toString(),
                    directoryInfo.get("object_name").toString(),
                    directoryInfo.get("object_shortdesc").toString()
            );
            directory.setResultSetSequence(resultSetSequence);
            resultSetSequence++;
            directory.setParent((Boolean) directoryInfo.get("element_is_parent"));

            // 3.2. add the md_context
            mdContext = new DlMetaDataContext();
            directory.setMdContext(mdContext.generateMdContextList(directoryInfo.get("md_context").toString()));

            filestoreDirectoryList.add(directory);

        }

        resultData.put("filestore_directories", filestoreDirectoryList);

        // third get the associations for the directory
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", filestore.getClassId(), Types.INTEGER)
                .addValue("_object_id", filestore.getObjectId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // first, create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // second, add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * saves a new file or object store in the DL DB
     */
    @RequestMapping(method = POST, value = "/filestore/summary")
    public Map<String, Object> writeFileStoreContent(@RequestBody FileStore newFileStore, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_summary");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newFileStore.getObjectId(), Types.INTEGER)
                .addValue("_class_id", newFileStore.getClassId(), Types.INTEGER)
                .addValue("_object_name",  newFileStore.getObjectName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newFileStore.getObjectShortDesc(), Types.VARCHAR)
                .addValue("_object_guid", newFileStore.getObjectGUID(), Types.VARCHAR)
                .addValue("_object_config", newFileStore.getObjectConfig(), Types.VARCHAR)
                .addValue("_defn_method", newFileStore.getCreateSource(), Types.TINYINT);

        Map<String, Object> out = jdbcCall.execute(in);
        resultData.put("returnID", (Integer) out.get("_return_id"));

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Reads the details for the given filestore directory
     * URL Path: /filestore/directory/{directoryId}
     */
    @RequestMapping(method = GET, value = "/filestore/directory/{directoryId}")
    @ResponseBody
    public Map<String, Object> readFileStoreDirectory(@PathVariable Integer directoryId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1; // this indicates status of database results.
        ArrayList associationList = new ArrayList();  // this list is returned in the results Map
        ArrayList commentList = new ArrayList();      // this list is returned in the results Map

        // 1. get the core attributes of the directory
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource().addValue("_object_id", directoryId, Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        FileStoreDirectory directory = new FileStoreDirectory(
                (Integer) out.get("_parent_object_id"),
                directoryId,
                (Integer) out.get("_class_id"),
                (Integer) out.get("_parent_element_object_id"),
                out.get("_object_guid").toString(),
                (Integer) out.get("_element_level"),
                (Integer) out.get("_element_level_sequence"),
                out.get("_element_hierarchy_number").toString(),
                out.get("_object_name").toString(),
                out.get("_object_shortdesc").toString());

        directory.setCustomFormData(out.get("_custom_form_data").toString());

        DlMetaDataContext mdContext = new DlMetaDataContext();
        directory.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));

        resultData.put("attributes", directory);

        // 1.1. get the data sensitivity overview for the directory
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element_ds_overview");
        in = new MapSqlParameterSource().addValue("_object_id", directoryId, Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dsOverview = (ArrayList) out.get("#result-set-1");
        for(Object listElement:dsOverview) {
            LinkedCaseInsensitiveMap dsTagInfo = (LinkedCaseInsensitiveMap) listElement;
            DlDataSensitivityTag dsTag = new DlDataSensitivityTag (
                    (Integer) dsTagInfo.get("data_sensitivity_id"),
                    dsTagInfo.get("data_sensitivity_level").toString(),
                    dsTagInfo.get("data_sensitivity_name").toString()
            );
            dsTag.setCreateMode((Integer) dsTagInfo.get("create_mode"));
            directory.addDlDsOverviewTag(dsTag);
        }

        // 2. get the associations for the directory
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_association");
        in = new MapSqlParameterSource().addValue("_class_id", directory.getClassId(), Types.INTEGER)
                .addValue("_object_id", directory.getDirectoryId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        ArrayList dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object assocDetails : dbResultList) {

            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) assocDetails;
            // 2.1. create the AssociatedObject instance
            AssociatedObject assocObject = new AssociatedObject(
                    (Integer) objectInfo.get("assoc_id"),
                    (Integer) objectInfo.get("target_object_id"),
                    (Integer) objectInfo.get("target_class_id"),
                    objectInfo.get("target_object_name").toString(),
                    objectInfo.get("target_class_name").toString(),
                    objectInfo.get("assoc_shortdesc").toString(),
                    objectInfo.get("assoc_type_name").toString(),
                    objectInfo.get("verb_phrase").toString()
            );

            // 2.2. add the metadata context
            mdContext = new DlMetaDataContext();
            assocObject.setMdContext(mdContext.generateMdContextList(objectInfo.get("md_context").toString()));

            associationList.add(assocObject);

        }
        resultData.put("associations", associationList);

        // 3. get the comments for the directory
        jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_comments_by_object");
        in = new MapSqlParameterSource()
                .addValue("_object_id", directory.getDirectoryId(), Types.INTEGER)
                .addValue("_class_id", directory.getClassId(), Types.INTEGER);
        out = jdbcCall.execute(in);
        dbResultList = (ArrayList) out.get("#result-set-1");

        for (Object commentDetails : dbResultList) {
            LinkedCaseInsensitiveMap objectInfo = (LinkedCaseInsensitiveMap) commentDetails;
            MoComment commentObject = new MoComment(
                    (Integer) objectInfo.get("user_id"),
                    (Integer) objectInfo.get("comment_id"),
                    (Integer) objectInfo.get("object_id"),
                    (Integer) objectInfo.get("class_id"),
                    objectInfo.get("last_change_ts").toString(),
                    objectInfo.get("comment_text").toString()
            );
            commentObject.setUsername(objectInfo.get("user_login").toString());
            commentList.add(commentObject);
        }

        resultData.put("comments", commentList);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes a new file or object store container or directory to the DL database.
     * URL Path: /filestore/directory/
     */
    @RequestMapping(method = POST, value = "/filestore/directory")
    public Map<String, Object> writeFileStoreDirectory(@RequestBody FileStoreDirectory newDirectory, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.
        DlSqlError sqlError = new DlSqlError();
        FileStoreDirectory directory = new FileStoreDirectory();
        Integer returnId = 0;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", newDirectory.getDirectoryId(), Types.INTEGER)
                .addValue("_class_id", newDirectory.getClassId(), Types.INTEGER)
                .addValue("_parent_object_id", newDirectory.getFilestoreId(), Types.INTEGER)
                .addValue("_parent_class_id", newDirectory.getParentClassId(), Types.INTEGER)
                .addValue("_parent_element_object_id",  newDirectory.getDirectoryParentId(), Types.INTEGER)
                .addValue("_object_guid",  newDirectory.getDirectoryGUID(), Types.VARCHAR)
                .addValue("_object_name",  newDirectory.getDirectoryName(), Types.VARCHAR)
                .addValue("_object_shortdesc", newDirectory.getDirectoryDesc(), Types.VARCHAR)
                .addValue("_element_type", null, Types.VARCHAR)
                .addValue("_object_config", null, Types.VARCHAR);

        Map<String, Object> out = null;

        try {
            out = jdbcCall.execute(in);
            returnId = (Integer) out.get("_return_id");
        }
        catch (UncategorizedSQLException e) {
            resultStatus = -1;
            SQLException sqle = e.getSQLException();
            sqlError.setSqlErrorCode(sqle.getErrorCode());
            sqlError.setSqlErrorState(sqle.getSQLState());
            sqlError.setSqlErrorMessage(sqle.getMessage());
        } catch (Exception e) {
            resultStatus = -1;
            sqlError.setSqlErrorCode(0);
            sqlError.setSqlErrorState("Unknown SQL Error State.");
            sqlError.setSqlErrorMessage(e.getMessage());
        }

        Integer newObjectId = 0;
        if (resultStatus == 1) {
            newObjectId = (Integer) out.get("_return_id");

            // get details of the new directory
            jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_element");
            in = new MapSqlParameterSource()
                    .addValue("_object_id", newObjectId, Types.INTEGER);

            try {
                out = jdbcCall.execute(in);
            } catch (UncategorizedSQLException e) {
                resultStatus = -1;
                SQLException sqle = e.getSQLException();
                sqlError.setSqlErrorCode(sqle.getErrorCode());
                sqlError.setSqlErrorState(sqle.getSQLState());
                sqlError.setSqlErrorMessage(sqle.getMessage());
            } catch (Exception e) {
                resultStatus = -1;
                sqlError.setSqlErrorCode(0);
                sqlError.setSqlErrorState("Unknown.");
                sqlError.setSqlErrorMessage(e.getMessage());
            }
        }

        if (resultStatus == 1) {
            directory = new FileStoreDirectory(
                    (Integer) out.get("_parent_object_id"),
                    newObjectId,
                    (Integer) out.get("_class_id"),
                    (Integer) out.get("_parent_element_object_id"),
                    out.get("_object_guid").toString(),
                    (Integer) out.get("_element_level"),
                    (Integer) out.get("_element_level_sequence"),
                    out.get("_element_hierarchy_number").toString(),
                    out.get("_object_name").toString(),
                    out.get("_object_shortdesc").toString()
            );

            // add the md_context
            DlMetaDataContext mdContext = new DlMetaDataContext();
            directory.setMdContext(mdContext.generateMdContextList(out.get("_md_context").toString()));
        }

        results.put("result_data", directory);
        results.put("result_status", resultStatus);
        results.put("sql_error", sqlError);

        return results;
    }

    /**
     * invokes the stored procedure to move an directory within a filestore
     */
    @RequestMapping(method = POST, value = "/filestore/directory/move")
    public Map<String, Object> writeFileStoreDirectoryMove(@RequestBody MoveDirectoryDefinition moveDirectoryDefinition, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_element_move");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", moveDirectoryDefinition.getDirectoryId(), Types.INTEGER)
                .addValue("_new_parent_element_id", moveDirectoryDefinition.getNewParentDirectoryId(), Types.INTEGER);
        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", null);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Writes a new filestore directory to the DL database.
     * URL Path: /filestore/directory/
     */
    @RequestMapping(method = POST, value = "/filestore/directory/customformdata")
    public Map<String, Object> writeFileStoreDirectoryCustomFormData(@RequestBody DlCustomForm customForm, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_custom_form_data");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_class_id", customForm.getClassId(), Types.INTEGER)
                .addValue("_object_id", customForm.getObjectId(), Types.INTEGER)
                .addValue("_parent_class_id", customForm.getParentClassId(), Types.INTEGER)
                .addValue("_parent_object_id", customForm.getParentObjectId(), Types.INTEGER)
                .addValue("_custom_form_id", customForm.getCustomFormId(), Types.INTEGER)
                .addValue("_custom_form_data", customForm.getFormData(), Types.VARCHAR)
                .addValue("_last_change_user_id", Integer.parseInt(userId), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Updates the custom form id for a filestore.
     * URL Path: /filestore/customform/
     */
    @RequestMapping(method = POST, value = "/filestore/customform")
    public Map<String, Object> updateCustomForm(@RequestBody FileStore fileStore, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_smd_hierarchy_custom_form_id");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", fileStore.getObjectId(), Types.INTEGER)
                .addValue("_class_id", fileStore.getClassId(), Types.INTEGER)
                .addValue("_custom_form_id", fileStore.getCustomFormId(), Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes a filestore directory from the DL database.
     * URL Path: /filestore/directory/
     */
    @RequestMapping(method = DELETE, value = "/filestore/directory/{objectId}")
    public Map<String, Object> deleteFileStoreDirectory(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        // TODO: lookup user id from DlAuthDB using the API Token in Authorization Header
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_smd_hierarchy_element");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Deletes a full filestore from the DL database.
     * URL Path: /filestore/
     */
    @RequestMapping(method = DELETE, value = "/filestore/{objectId}")
    public Map<String, Object> deleteFileStore(@PathVariable Integer objectId, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("delete_smd_hierarchy_summary");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER)
                .addValue("_object_id", objectId, Types.INTEGER);

        jdbcCall.execute(in);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Create a new file or object store credential.
     * @param fosCredential
     * @param userId
     * @return
     */
    @RequestMapping(method = POST, value = "/filestore/credential")
    public Map<String, Object> writeFosCredential(@RequestBody FosCredential fosCredential, @RequestHeader("dlUserId") String userId) {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;
        Integer newFosCredentialId = -1;

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("write_credential");
        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("_credential_id", newFosCredentialId, Types.INTEGER)
                .addValue("_credential_name", fosCredential.getCredentialName(), Types.VARCHAR)
                .addValue("_credential_shortdesc", fosCredential.getCredentialShortDesc(), Types.VARCHAR)
                .addValue("_credential_detail", fosCredential.getCredentialDetailJson(), Types.VARCHAR)
                .addValue("_key", cryptKey, Types.VARCHAR)
                .addValue("_user_id", Integer.parseInt(userId), Types.INTEGER);

        Map<String, Object> out = jdbcCall.execute(in);
        Integer returnId = (Integer) out.get("_run_result");

        if (returnId < 0) {
            resultStatus = -1;
        }

        resultData.put("returnId", returnId);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }


    /**
     * Utility class used to process a request to move a filestore directory
     */
    static class MoveDirectoryDefinition {
        private Integer directoryId;
        private Integer newParentDirectoryId;

        public MoveDirectoryDefinition() {
            this.directoryId = 0;
            this.newParentDirectoryId = 0;
        }

        public MoveDirectoryDefinition(Integer directoryId, Integer newParentDirectoryId) {
            this.directoryId = directoryId;
            this.newParentDirectoryId = newParentDirectoryId;
        }

        public Integer getDirectoryId() {
            return directoryId;
        }

        public void setDirectoryId(Integer directoryId) {
            this.directoryId = directoryId;
        }

        public Integer getNewParentDirectoryId() {
            return newParentDirectoryId;
        }

        public void setNewParentDirectoryId(Integer newParentDirectoryId) {
            this.newParentDirectoryId = newParentDirectoryId;
        }
    }

    /**
     * Utility class for receiving custom form data from UI
     */
    static class CustomFormData {
        private Integer directoryId;
        private String customFormData;

        public CustomFormData() {
            this.directoryId = 0;
            this.customFormData = "";
        }

        public CustomFormData(Integer directoryId, String customFormData) {
            this.directoryId = directoryId;
            this.customFormData = customFormData;
        }

        public Integer getDirectoryId() {
            return directoryId;
        }

        public void setDirectoryId(Integer directoryId) {
            this.directoryId = directoryId;
        }

        public String getCustomFormData() {
            return customFormData;
        }

        public void setCustomFormData(String customFormData) {
            this.customFormData = customFormData;
        }
    }

    /**
     * Utility class for receiving a File/Object Store credential definition from the UI
     */
    static class FosCredential {
        private Integer cloudProviderId;
        private String credentialName;
        private String credentialShortDesc;
        private String credentialDetailJson;

        public FosCredential() {
            this.cloudProviderId = 0;
            this.credentialName = "";
            this.credentialShortDesc = "";
            this.credentialDetailJson = "";
        }

        public Integer getCloudProviderId() {
            return cloudProviderId;
        }

        public void setCloudProviderId(Integer cloudProviderId) {
            this.cloudProviderId = cloudProviderId;
        }

        public String getCredentialName() {
            return credentialName;
        }

        public void setCredentialName(String credentialName) {
            this.credentialName = credentialName;
        }

        public String getCredentialShortDesc() {
            return credentialShortDesc;
        }

        public void setCredentialShortDesc(String credentialShortDesc) {
            this.credentialShortDesc = credentialShortDesc;
        }

        public String getCredentialDetailJson() {
            return credentialDetailJson;
        }

        public void setCredentialDetailJson(String credentialDetailJson) {
            this.credentialDetailJson = credentialDetailJson;
        }
    }

}
