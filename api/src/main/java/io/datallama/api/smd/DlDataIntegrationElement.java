/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlDataIntegrationElement {
    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private Integer parentElementObjectId;
    private Integer elementLevel;
    private Integer elementLevelSequence;
    private String elementHierarchyNumber;
    private Boolean elementIsParent;
    private String objectName;
    private String objectShortDesc;
    private String objectDescReveng;
    private String objectGUID;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private Integer elementIsVisible;
    private Integer initialExpandLevel;
    private Boolean elementIsExpanded;
    private Boolean elementIsSelected;
    private Boolean elementIsMatched;
    private Integer resultSetSequence;

    /**
     * Default constructor with no parameters
     */
    public DlDataIntegrationElement() {
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.parentElementObjectId = 0;
        this.elementLevel = 0;
        this.elementLevelSequence = 0;
        this.elementHierarchyNumber = "";
        this.elementIsParent = false;
        this.objectName = "";
        this.objectShortDesc = "";
        this.objectDescReveng = "";
        this.objectGUID = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.elementIsVisible = 0;
        this.initialExpandLevel = 2;
        this.elementIsExpanded = false;
        this.elementIsSelected = false;
        this.elementIsMatched = false;
        this.resultSetSequence = 0;
    }

    /**
     * Constructor 02 with parameters for core members
     *
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param parentElementObjectId
     * @param objectName
     */
    public DlDataIntegrationElement(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                                    Integer parentElementObjectId, String objectName) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.parentElementObjectId = parentElementObjectId;
        this.elementLevel = 0;
        this.elementLevelSequence = 0;
        this.elementHierarchyNumber = "";
        this.elementIsParent = false;
        this.objectName = objectName;
        this.objectShortDesc = "";
        this.objectDescReveng = "";
        this.objectGUID = "";
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.elementIsVisible = 0;
        this.initialExpandLevel = 2;
        this.elementIsExpanded = false;
        this.elementIsSelected = false;
        this.elementIsMatched = false;
        this.resultSetSequence = 0;

    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getParentElementObjectId() {
        return parentElementObjectId;
    }

    public void setParentElementObjectId(Integer parentElementObjectId) {
        this.parentElementObjectId = parentElementObjectId;
    }

    public Integer getElementLevel() {
        return elementLevel;
    }

    public void setElementLevel(Integer elementLevel) {
        this.elementLevel = elementLevel;
    }

    public Integer getElementLevelSequence() {
        return elementLevelSequence;
    }

    public void setElementLevelSequence(Integer elementLevelSequence) {
        this.elementLevelSequence = elementLevelSequence;
    }

    public String getElementHierarchyNumber() {
        return elementHierarchyNumber;
    }

    public void setElementHierarchyNumber(String elementHierarchyNumber) {
        this.elementHierarchyNumber = elementHierarchyNumber;
    }

    public Boolean getElementIsParent() {
        return elementIsParent;
    }

    public void setElementIsParent(Boolean elementIsParent) {
        this.elementIsParent = elementIsParent;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public String getObjectDescReveng() {
        return objectDescReveng;
    }

    public void setObjectDescReveng(String objectDescReveng) {
        this.objectDescReveng = objectDescReveng;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public Integer getElementIsVisible() {
        return elementIsVisible;
    }

    public void setElementIsVisible(Integer elementIsVisible) {
        this.elementIsVisible = elementIsVisible;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public Boolean getElementIsExpanded() {
        return elementIsExpanded;
    }

    public void setElementIsExpanded(Boolean elementIsExpanded) {
        this.elementIsExpanded = elementIsExpanded;
    }

    public Boolean getElementIsSelected() {
        return elementIsSelected;
    }

    public void setElementIsSelected(Boolean elementIsSelected) {
        this.elementIsSelected = elementIsSelected;
    }

    public Boolean getElementIsMatched() {
        return elementIsMatched;
    }

    public void setElementIsMatched(Boolean elementIsMatched) {
        this.elementIsMatched = elementIsMatched;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public void determineExpanded() {
        this.elementIsExpanded = (this.elementLevel < this.initialExpandLevel) ? true : false;
    }

    public void determineVisibility() {
        this.elementIsVisible = (this.elementLevel <= this.initialExpandLevel) ? 1 : 0;
    }

}
