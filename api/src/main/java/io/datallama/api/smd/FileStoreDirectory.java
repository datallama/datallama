/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class FileStoreDirectory {

    private Integer directoryId;
    private Integer classId;
    private Integer filestoreId;
    private Integer parentClassId;
    private Integer directoryParentId;
    private Integer directoryLevel;
    private Integer directoryLevelSequence;
    private String directoryGUID;
    private String directoryHierarchyNumber;
    private String directoryName;
    private String directoryDesc;
    private String customFormData;
    private String directoryFullPathName;
    private Integer directoryVisible;
    private Integer initialExpandLevel;
    private String indentCss;
    private Boolean expanded;
    private Boolean parent;
    private Integer resultSetSequence;
    private Integer directorySelected;
    private Boolean directoryMatched;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private ArrayList<DlDataSensitivityTag> dlDsTagList;
    private ArrayList<DlDataSensitivityTag> dlDsOverview;

    /**
     * Default constructor with no parameters.
     */
    public FileStoreDirectory() {
        this.directoryId = 0;
        this.classId = 0;
        this.filestoreId = 0;
        this.parentClassId = 0;
        this.directoryParentId = 0;
        this.directoryLevel = 0;
        this.directoryLevelSequence = 0;
        this.directoryGUID = "";
        this.directoryHierarchyNumber = "";
        this.directoryName = "";
        this.directoryDesc = "";
        this.customFormData = "";
        this.directoryVisible = 0;
        this.initialExpandLevel = 0;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.directorySelected = 0;
        this.directoryMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
        this.dlDsOverview = new ArrayList<DlDataSensitivityTag>();
    }

    public FileStoreDirectory(Integer filestoreId, Integer directoryId, Integer classId, Integer directoryParentId,
                              String directoryGUID, Integer directoryLevel, Integer directoryLevelSequence,
                              String directoryHierarchyNumber, String directoryName, String directoryDesc) {
        this.filestoreId = filestoreId;
        this.parentClassId = 0;
        this.directoryId = directoryId;
        this.classId = classId;
        this.directoryParentId = directoryParentId;
        this.directoryLevel = directoryLevel;
        this.directoryLevelSequence = directoryLevelSequence;
        this.directoryGUID = directoryGUID;
        this.directoryHierarchyNumber = directoryHierarchyNumber;
        this.directoryName = directoryName;
        this.directoryDesc = directoryDesc;
        this.customFormData = "";
        this.directoryVisible = 0;
        this.initialExpandLevel = 2;
        this.indentCss = "";
        this.expanded = false;
        this.parent = false;
        this.resultSetSequence = 0;
        this.directorySelected = 0;
        this.directoryMatched = false;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.dlDsTagList = new ArrayList<DlDataSensitivityTag>();
        this.dlDsOverview = new ArrayList<DlDataSensitivityTag>();

        this.determineVisibility();
        this.determineExpanded();
        this.generateIndentCss();
    }

    public Integer getDirectoryId() {
        return directoryId;
    }
    public Integer getObjectId() {
        return directoryId;
    }

    public void setDirectoryId(Integer directoryId) {
        this.directoryId = directoryId;
    }
    public void setObjectId(Integer objectId) {
        this.directoryId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getFilestoreId() {
        return filestoreId;
    }
    public Integer getParentObjectId() {
        return filestoreId;
    }

    public void setFilestoreId(Integer filestoreId) {
        this.filestoreId = filestoreId;
    }
    public void setParentObjectId(Integer parentObjectId) {
        this.filestoreId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public Integer getDirectoryParentId() {
        return directoryParentId;
    }
    public Integer getElementParentObjectId() {
        return directoryParentId;
    }

    public void setDirectoryParentId(Integer directoryParentId) {
        this.directoryParentId = directoryParentId;
    }
    public void setElementParentObjectId(Integer elementParentObjectId) {
        this.directoryParentId = elementParentObjectId;
    }

    public Integer getDirectoryLevel() {
        return directoryLevel;
    }

    public void setDirectoryLevel(Integer directoryLevel) {
        this.directoryLevel = directoryLevel;
    }

    public Integer getDirectoryLevelSequence() {
        return directoryLevelSequence;
    }

    public void setDirectoryLevelSequence(Integer directoryLevelSequence) {
        this.directoryLevelSequence = directoryLevelSequence;
    }

    public String getDirectoryGUID() {
        return directoryGUID;
    }
    public String getObjectGUID() {
        return directoryGUID;
    }

    public void setDirectoryGUID(String directoryGUID) {
        this.directoryGUID = directoryGUID;
    }
    public void setObjectGUID(String objectGUID) {
        this.directoryGUID = objectGUID;
    }

    public String getDirectoryHierarchyNumber() {
        return directoryHierarchyNumber;
    }

    public void setDirectoryHierarchyNumber(String directoryHierarchyNumber) {
        this.directoryHierarchyNumber = directoryHierarchyNumber;
    }

    public String getDirectoryName() {
        return directoryName;
    }
    public String getObjectName() {
        return directoryName;
    }

    public void setDirectoryName(String directoryName) {
        this.directoryName = directoryName;
    }
    public void setObjectName(String objectName) {
        this.directoryName = objectName;
    }

    public String getDirectoryDesc() {
        return directoryDesc;
    }
    public String getObjectShortDesc() {
        return directoryDesc;
    }

    public void setDirectoryDesc(String directoryDesc) {
        this.directoryDesc = directoryDesc;
    }
    public void setObjectShortDesc(String objectShortDesc) {
        this.directoryDesc = objectShortDesc;
    }

    public String getCustomFormData() {
        return customFormData;
    }

    public void setCustomFormData(String customFormData) {
        this.customFormData = customFormData;
    }

    public String getDirectoryFullPathName() {
        return directoryFullPathName;
    }

    public void setDirectoryFullPathName(String directoryFullPathName) {
        this.directoryFullPathName = directoryFullPathName;
    }

    public Integer getDirectoryVisible() {
        return directoryVisible;
    }

    public void setDirectoryVisible(Integer directoryVisible) {
        this.directoryVisible = directoryVisible;
    }

    public Integer getInitialExpandLevel() {
        return initialExpandLevel;
    }

    public void setInitialExpandLevel(Integer initialExpandLevel) {
        this.initialExpandLevel = initialExpandLevel;
    }

    public String getIndentCss() {
        return indentCss;
    }

    public void setIndentCss(String indentCss) {
        this.indentCss = indentCss;
    }

    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }

    public Boolean getParent() {
        return parent;
    }

    public void setParent(Boolean parent) {
        this.parent = parent;
    }

    public Integer getResultSetSequence() {
        return resultSetSequence;
    }

    public void setResultSetSequence(Integer resultSetSequence) {
        this.resultSetSequence = resultSetSequence;
    }

    public Integer getDirectorySelected() {
        return directorySelected;
    }

    public void setDirectorySelected(Integer directorySelected) {
        this.directorySelected = directorySelected;
    }

    public Boolean getDirectoryMatched() {
        return directoryMatched;
    }

    public void setDirectoryMatched(Boolean directoryMatched) {
        this.directoryMatched = directoryMatched;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }
    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }
    public void addMdContextNode(DlMetaDataContextNode mdContextNode) {
        this.mdContext.add(mdContextNode);
    }

    public void determineVisibility() {
        directoryVisible = (directoryLevel <= initialExpandLevel) ? 1 : 0;
    }
    public void determineExpanded() {
        this.expanded = directoryLevel < initialExpandLevel;
    }
    public void generateIndentCss() {
        this.indentCss = "{'margin-left':" + Integer.toString(directoryLevel * 15) + ";}";
    }

    public ArrayList<DlDataSensitivityTag> getDlDsTagList() {
        return this.dlDsTagList;
    }
    public void setDlDsTagList(ArrayList<DlDataSensitivityTag> dlDsTagList) {
        this.dlDsTagList = dlDsTagList;
    }
    public void addDlDsTag(DlDataSensitivityTag dlDsTag) {
        this.dlDsTagList.add(dlDsTag);
    }

    public ArrayList<DlDataSensitivityTag> getDlDsOverview() {
        return this.dlDsOverview;
    }
    public void setDlDsOverview(ArrayList<DlDataSensitivityTag> dlDsTagList) {
        this.dlDsOverview = dlDsOverview;
    }
    public void addDlDsOverviewTag(DlDataSensitivityTag dlDsTag) {
        this.dlDsOverview.add(dlDsTag);
    }

}
