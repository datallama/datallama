/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

public interface ReverseEngineerDataIntegration {

    /**
     * Reverse engineers all packages and jobs in the database specified by dbCredentials.
     * @param credential
     * @return
     * @throws Exception
     */
    public SmdCredential testDatabaseConnection (SmdCredential credential) throws Exception;

    /**
     * Reverse engineers all packages and jobs in the dataIntegration project identified by credential.
     * This is an Online reverse engineer which connects programmatically to a data integration platform
     * @param credential
     * @return
     * @throws Exception
     */
    public RevEngResult reverseEngineerDataIntegration (SmdCredential credential) throws Exception;

    /**
     * Returns the reverse engineered database catalog
     * @return
     * @throws Exception
     */
    public SmdDatabaseDefinition getReverseEngineerCatalog () throws Exception;

}
