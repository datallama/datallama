/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import io.datallama.api.common.DlCustomForm;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;

public class DlDataIntegrationSummary {
    private Integer objectId;
    private Integer classId;
    private String className;
    private String objectName;
    private String objectGUID;
    private String objectShortDesc;
    private String objectConfig;
    private Integer customFormId;
    private DlCustomForm customForm;
    private Integer connectorId;
    private Integer credentialId;
    private ArrayList<DlMetaDataContextNode> mdContext;

    /**
     * Default constructor with no parameters.
     */
    public DlDataIntegrationSummary() {
        this.objectId = 0;
        this.classId = 0;
        this.className = "";
        this.objectName = "";
        this.objectGUID = "";
        this.objectShortDesc = "";
        this.objectConfig = "{}";
        this.customFormId = 0;
        this.customForm = new DlCustomForm();
        this.connectorId = 0;
        this.credentialId = 0;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    /**
     * Constructor 02, with essential parameters.
     * @param objectId
     * @param classId
     * @param objectName
     */
    public DlDataIntegrationSummary(Integer objectId, Integer classId, String objectName) {
        this.objectId = objectId;
        this.classId = classId;
        this.className = "";
        this.objectName = objectName;
        this.objectGUID = "";
        this.objectShortDesc = "";
        this.objectConfig = "{}";
        this.customFormId = 0;
        this.customForm = new DlCustomForm();
        this.credentialId = 0;
        this.connectorId = 0;
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectGUID() {
        return objectGUID;
    }

    public void setObjectGUID(String objectGUID) {
        this.objectGUID = objectGUID;
    }

    public String getObjectShortDesc() {
        return objectShortDesc;
    }

    public void setObjectShortDesc(String objectShortDesc) {
        this.objectShortDesc = objectShortDesc;
    }

    public String getObjectConfig() {
        return objectConfig;
    }

    public void setObjectConfig(String objectConfig) {
        this.objectConfig = objectConfig;
    }

    public Integer getCustomFormId() {
        return customFormId;
    }

    public void setCustomFormId(Integer customFormId) {
        this.customFormId = customFormId;
    }

    public DlCustomForm getCustomForm() {
        return customForm;
    }

    public void setCustomForm(DlCustomForm customForm) {
        this.customForm = customForm;
    }

    public Integer getConnectorId() {
        return connectorId;
    }

    public void setConnectorId(Integer connectorId) {
        this.connectorId = connectorId;
    }

    public Integer getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Integer credentialId) {
        this.credentialId = credentialId;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }
}
