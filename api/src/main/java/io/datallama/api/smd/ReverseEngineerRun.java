/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

public class ReverseEngineerRun {
    private Integer revEngRunId;
    private Integer revEngRunResult;
    private String revEngRunCode;
    private String revEngRunDesc;
    private String revEngRunDt;
    private Integer userId;
    private String userLogin;

    /**
     * Default Constructor. No parameters
     */
    public ReverseEngineerRun() {
        this.revEngRunId = 0;
        this.revEngRunResult = 0;
        this.revEngRunCode = "";
        this.revEngRunDesc = "";
        this.revEngRunDt = "";
        this.userId = 0;
        this.userLogin = "";
    }

    /**
     * Paramatized Constructor 01 - all members specified except revEngRunDescription
     * @param revEngRunId
     * @param revEngRunResult
     * @param revEngRunCode
     * @param revEngRunDt
     * @param userId
     * @param userLogin
     */
    public ReverseEngineerRun(Integer revEngRunId, Integer revEngRunResult, String revEngRunCode,
                              String revEngRunDt, Integer userId, String userLogin) {
        this.revEngRunId = revEngRunId;
        this.revEngRunResult = revEngRunResult;
        this.revEngRunCode = revEngRunCode;
        this.revEngRunDesc = "";
        this.revEngRunDt = revEngRunDt;
        this.userId = userId;
        this.userLogin = userLogin;
    }

    public Integer getRevEngRunId() {
        return revEngRunId;
    }

    public void setRevEngRunId(Integer revEngRunId) {
        this.revEngRunId = revEngRunId;
    }

    public Integer getRevEngRunResult() {
        return revEngRunResult;
    }

    public void setRevEngRunResult(Integer revEngRunResult) {
        this.revEngRunResult = revEngRunResult;
    }

    public String getRevEngRunCode() {
        return revEngRunCode;
    }

    public void setRevEngRunCode(String revEngRunCode) {
        this.revEngRunCode = revEngRunCode;
    }

    public String getRevEngRunDesc() {
        return revEngRunDesc;
    }

    public void setRevEngRunDesc(String revEngRunDesc) {
        this.revEngRunDesc = revEngRunDesc;
    }

    public String getRevEngRunDt() {
        return revEngRunDt;
    }

    public void setRevEngRunDateTime(String revEngRunDt) {
        this.revEngRunDt = revEngRunDt;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
}
