/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.smd;

import com.fasterxml.jackson.databind.JsonNode;
import io.datallama.api.smd.SmdDbCredentials;

public interface ReverseEngineerDatabase {

    /**
     * Reverse engineers all tables, views and foreign key relationships in the database specified by dbCredentials.
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public SmdDbCredentials testDatabaseConnection (SmdDbCredentials dbCredentials) throws Exception;

    /**
     * Reverse engineers all tables, views and relationships in the database identified by dbCredentials
     * @param dbCredentials
     * @return
     * @throws Exception
     */
    public RevEngResult reverseEngineerDatabase (SmdDbCredentials dbCredentials, String[] schemaNameList) throws Exception;

    /**
     * Returns the reverse engineered database catalog
     * @return
     * @throws Exception
     */
    public SmdDatabaseDefinition getReverseEngineerCatalog () throws Exception;
}
