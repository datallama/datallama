/*
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.smd;

import io.datallama.api.common.DlDataSensitivityTag;
import io.datallama.api.metamodel.DlMetaModelObject;

/**
 * Models the assignment of a data sensitivity tag to a metadata object (md_object).
 * If the md_object has multiple data sensitivity tag assignments then multiple DlDataSensitivityAssignment objects
 * will be created in the data sensitivity report.
 */
// TODO: This class should be moved to the common package
public class DlDataSensitivityAssignment extends DlMetaModelObject {

    private DlDataSensitivityTag dsTag;

    public DlDataSensitivityAssignment() {
        this.dsTag = new DlDataSensitivityTag();
    }

    public DlDataSensitivityTag getDsTag() {
        return dsTag;
    }

    public void setDsTag(DlDataSensitivityTag dsTag) {
        this.dsTag = dsTag;
    }
}
