/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.search;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.datallama.api.metamodel.DlMetaDataContext;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * The SearchController class is used to search and manage the OpenSearch service
 */
@CrossOrigin
@RestController
public class SearchServiceController {

    @Value("${dl.srch.url.base}")
    private String srchUrlBase;

    @Value("${dl.srch.username}")
    private String srchUsername;

    @Value("${dl.srch.password}")
    private String srchPassword;

    @Autowired
    private RestTemplate sslRestTemplate;

    @Autowired
    private DriverManagerDataSource dataSource;

    /**
     * Rebuilds the "core" index
     * @return
     */
    @RequestMapping(method = PUT, value = "/search/indexes/rebuild/core")
    public Map<String, Object> rebuildSearchIndexes() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        SearchIndexObject srchNdxObj = new SearchIndexObject();
        ResponseEntity<Object> respEntity = null;

        // 1. recreate the core index
        // 1.1. generate authentication headers
        String deleteUrl = srchUrlBase + "/core";
        HttpHeaders headers = createBasicAuthHeader(srchUsername, srchPassword);
        headers.setContentType(MediaType.APPLICATION_JSON);

        // 1.2. delete existing index
        HttpEntity<String> requestEntity = new HttpEntity<>(headers);
        try {
            respEntity = sslRestTemplate.exchange(deleteUrl, HttpMethod.DELETE, requestEntity, Object.class);
        }
        catch (HttpClientErrorException e) {
            // TODO: this exception should be thrown because the index does not exist. Double check that this is the cause
            resultStatus = -2;
        }

        // 1.3. create new index named "core"
        String requestBodyData = "";
        String createIndexUrl = srchUrlBase + "/core";
        if (resultStatus == 1) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                requestBodyData = objectMapper.writeValueAsString(srchNdxObj.generateSrchIndxMappingDefn());
            } catch (JsonProcessingException e) {
                System.out.println("Failed to serialise Search Index Mapping Definition object as JSON.");
                resultStatus = -3;
            }
        }

        requestEntity = new HttpEntity<>(requestBodyData, headers);
        if (resultStatus == 1) {
            respEntity = sslRestTemplate.exchange(createIndexUrl, HttpMethod.PUT, requestEntity, Object.class);
        }

        // 2.0. populate the new index
        String bulkRequestBody = "";
        ObjectMapper objectMapper = new ObjectMapper();
        DlMetaDataContext mdContextBuilder = new DlMetaDataContext();

        // 2.1. populate search index with SMD database, schema and table/view md_object
        if (resultStatus == 1) {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_db_objects_01");
            Map<String, Object> queryResultSet = jdbcCall.execute();

            ArrayList smdDbObjects = (ArrayList) queryResultSet.get("#result-set-1");
            for (Object listElement : smdDbObjects) {
                LinkedCaseInsensitiveMap smdDbObject = (LinkedCaseInsensitiveMap) listElement;

                String srchNdxObjStr = String.format("{\"objectId\":%d,\"classId\":%d,\"parentObjectId\":%d,\"parentClassId\":%d,\"objectName\":\"%s\",\"objectShortdesc\":\"%s\",\"elementType\":\"%s\",\"mdContext\":\"%s\",\"dataSensitivityId\":%d}\n",
                        (Integer) smdDbObject.get("object_id"),
                        (Integer) smdDbObject.get("class_id"),
                        (Integer) smdDbObject.get("parent_object_id"),
                        (Integer) smdDbObject.get("parent_class_id"),
                        smdDbObject.get("object_name").toString(),
                        smdDbObject.get("object_shortdesc").toString().replace("\"", "\\\"").replace("\n", " "),
                        smdDbObject.get("element_type").toString(),
                        smdDbObject.get("md_context").toString().replace("\"", "\\\""),
                        (Integer) smdDbObject.get("data_sensitivity_id")
                );

                String bulkMetadata = String.format("{\"index\":{\"_id\":\"%s_%s\"}}\n", smdDbObject.get("object_id").toString(), smdDbObject.get("class_id").toString());
                //Map<String, Object> bulkMetadata = Map.of("index", Map.of("_id", srchObjId));
                bulkRequestBody += bulkMetadata;
                bulkRequestBody += srchNdxObjStr;
            }
            String bulkWriteUrl = srchUrlBase + "/core/_bulk";
            requestEntity = new HttpEntity<>(bulkRequestBody, headers);
            if (resultStatus == 1) {
                respEntity = sslRestTemplate.exchange(bulkWriteUrl, HttpMethod.POST, requestEntity, Object.class);
            }
        }

        // 2.2. populate search index with SMD column md_object
        if (resultStatus == 1) {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_db_objects_02");
            Map<String, Object> queryResultSet = jdbcCall.execute();

            ArrayList smdDbObjects = (ArrayList) queryResultSet.get("#result-set-1");
            for (Object listElement : smdDbObjects) {
                LinkedCaseInsensitiveMap smdDbObject = (LinkedCaseInsensitiveMap) listElement;

                String srchNdxObjStr = String.format("{\"objectId\":%d,\"classId\":%d,\"parentObjectId\":%d,\"parentClassId\":%d,\"objectName\":\"%s\",\"objectShortdesc\":\"%s\",\"elementType\":\"%s\",\"mdContext\":\"%s\",\"dataSensitivityId\":%d}\n",
                        (Integer) smdDbObject.get("object_id"),
                        (Integer) smdDbObject.get("class_id"),
                        (Integer) smdDbObject.get("parent_object_id"),
                        (Integer) smdDbObject.get("parent_class_id"),
                        smdDbObject.get("object_name").toString(),
                        smdDbObject.get("object_shortdesc").toString().replace("\"", "\\\"").replace("\n", " "),
                        smdDbObject.get("element_type").toString(),
                        smdDbObject.get("md_context").toString().replace("\"", "\\\""),
                        (Integer) smdDbObject.get("data_sensitivity_id")
                );

                String bulkMetadata = String.format("{\"index\":{\"_id\":\"%s_%s\"}}\n", smdDbObject.get("object_id").toString(), smdDbObject.get("class_id").toString());
                bulkRequestBody += bulkMetadata;
                bulkRequestBody += srchNdxObjStr;
            }
            String bulkWriteUrl = srchUrlBase + "/core/_bulk";
            requestEntity = new HttpEntity<>(bulkRequestBody, headers);
            if (resultStatus == 1) {
                respEntity = sslRestTemplate.exchange(bulkWriteUrl, HttpMethod.POST, requestEntity, Object.class);
            }
        }

        // 2.3. populate search index with smd hierarchy md_objects
        if (resultStatus == 1) {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_smd_hierarchy_objects_srch");
            Map<String, Object> queryResultSet = jdbcCall.execute();

            ArrayList smdDbObjects = (ArrayList) queryResultSet.get("#result-set-1");
            for (Object listElement : smdDbObjects) {
                LinkedCaseInsensitiveMap smdDbObject = (LinkedCaseInsensitiveMap) listElement;

                String srchNdxObjStr = String.format("{\"objectId\":%d,\"classId\":%d,\"parentObjectId\":%d,\"parentClassId\":%d,\"objectName\":\"%s\",\"objectShortdesc\":\"%s\",\"elementType\":\"%s\",\"mdContext\":\"%s\",\"dataSensitivityId\":%d}\n",
                        (Integer) smdDbObject.get("object_id"),
                        (Integer) smdDbObject.get("class_id"),
                        (Integer) smdDbObject.get("parent_object_id"),
                        (Integer) smdDbObject.get("parent_class_id"),
                        smdDbObject.get("object_name").toString(),
                        smdDbObject.get("object_shortdesc").toString().replace("\"", "\\\"").replace("\n", " "),
                        smdDbObject.get("element_type").toString(),
                        smdDbObject.get("md_context").toString().replace("\"", "\\\""),
                        (Integer) smdDbObject.get("data_sensitivity_id")
                );

                String bulkMetadata = String.format("{\"index\":{\"_id\":\"%s_%s\"}}\n", smdDbObject.get("object_id").toString(), smdDbObject.get("class_id").toString());
                bulkRequestBody += bulkMetadata;
                bulkRequestBody += srchNdxObjStr;
            }
            String bulkWriteUrl = srchUrlBase + "/core/_bulk";
            requestEntity = new HttpEntity<>(bulkRequestBody, headers);
            if (resultStatus == 1) {
                respEntity = sslRestTemplate.exchange(bulkWriteUrl, HttpMethod.POST, requestEntity, Object.class);
            }
        }

        // 2.4. populate search index with business ontology md_objects
        if (resultStatus == 1) {
            SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("read_ontology_objects_srch");
            Map<String, Object> queryResultSet = jdbcCall.execute();

            ArrayList smdDbObjects = (ArrayList) queryResultSet.get("#result-set-1");
            for (Object listElement : smdDbObjects) {
                LinkedCaseInsensitiveMap smdDbObject = (LinkedCaseInsensitiveMap) listElement;

                String srchNdxObjStr = String.format("{\"objectId\":%d,\"classId\":%d,\"parentObjectId\":%d,\"parentClassId\":%d,\"objectName\":\"%s\",\"objectShortdesc\":\"%s\",\"elementType\":\"%s\",\"mdContext\":\"%s\",\"dataSensitivityId\":%d}\n",
                        (Integer) smdDbObject.get("object_id"),
                        (Integer) smdDbObject.get("class_id"),
                        (Integer) smdDbObject.get("parent_object_id"),
                        (Integer) smdDbObject.get("parent_class_id"),
                        smdDbObject.get("object_name").toString(),
                        smdDbObject.get("object_shortdesc").toString().replace("\"", "\\\"").replace("\n", " "),
                        smdDbObject.get("element_type").toString(),
                        smdDbObject.get("md_context").toString().replace("\"", "\\\""),
                        (Integer) smdDbObject.get("data_sensitivity_id")
                );

                String bulkMetadata = String.format("{\"index\":{\"_id\":\"%s_%s\"}}\n", smdDbObject.get("object_id").toString(), smdDbObject.get("class_id").toString());
                bulkRequestBody += bulkMetadata;
                bulkRequestBody += srchNdxObjStr;
            }
            String bulkWriteUrl = srchUrlBase + "/core/_bulk";
            requestEntity = new HttpEntity<>(bulkRequestBody, headers);
            if (resultStatus == 1) {
                respEntity = sslRestTemplate.exchange(bulkWriteUrl, HttpMethod.POST, requestEntity, Object.class);
            }
        }

        resultData.put("bulk_write_response", respEntity);
        results.put("result_data", resultData);
        results.put("result_status", resultStatus);
        return results;
    }

    /**
     * This method demonstrates how to connect to OpenSearch using TLS.
     * Note that basic authentication is still required.
     * @return
     */
    @RequestMapping(method = GET, value = "/search/database01")
    public Map<String, Object> xmplOpenSearchGet01() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        ObjectMapper objectMapper = new ObjectMapper();

        DlSrchExprBuilder srchExprBldr = new DlSrchExprBuilder();
        DlSrchBoolExpr srchBoolExpr = new DlSrchBoolExpr();
        srchBoolExpr.addMustExpr(srchExprBldr.getWildcardExpr("objectName", "*email*", true));
        ArrayList<Integer> classFilter = new ArrayList<Integer>();
        classFilter.add(8);
        classFilter.add(73);
        srchBoolExpr.addMustExpr(srchExprBldr.getTermsExpr("classId", classFilter));

        srchBoolExpr.addMustNotExpr(srchExprBldr.getWildcardExpr("elementType", "int*", true));
        srchBoolExpr.addMustNotExpr(srchExprBldr.getWildcardExpr("elementType", "decimal*", true));
        srchBoolExpr.addMustNotExpr(srchExprBldr.getWildcardExpr("elementType", "numeric*", true));
        srchBoolExpr.addMustNotExpr(srchExprBldr.getWildcardExpr("elementType", "bool*", true));
        srchBoolExpr.addMustNotExpr(srchExprBldr.getWildcardExpr("elementType", "binary*", true));
        srchBoolExpr.addMustNotExpr(srchExprBldr.getWildcardExpr("elementType", "varbinary*", true));

        Map<String, Object> queryDefn = srchExprBldr.getBoolQueryDefn(100, srchBoolExpr);

        String requestBodyData = "";
        try {
            requestBodyData = objectMapper.writeValueAsString(queryDefn);
        } catch (JsonProcessingException e) {
            System.out.println("Failed to serialise queryDefn object as JSON.");
        }

        String srchUrl = srchUrlBase + "/core/_search";

        HttpHeaders headers = createBasicAuthHeader(srchUsername, srchPassword);
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(requestBodyData, headers);
        ResponseEntity<Object> respEntity = sslRestTemplate.exchange(srchUrl, HttpMethod.POST, requestEntity, Object.class);

        resultData.put("searchResults", respEntity);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * THIS WORKS! However, it is a hack because the HTTP Method is set to POST even though the endpoint is a GET.
     * The hack is necessary because Spring RestTemplate by default will not send a request body with a GET call.
     * Refer to https://thegeekyasian.com/http-get-request-with-a-request-body-using-resttemplate/ for work around.
     *
     * It also uses basicAuthentication over HTTP which is not secure!
     *
     * @return
     */
    @RequestMapping(method = GET, value = "/search/database02")
    public Map<String, Object> xmplOpenSearchGet02() {
        Map<String, Object> results = new HashMap<>();
        Map<String, Object> resultData = new HashMap<>();
        Integer resultStatus = 1;   // this indicates status of database results.

        String srchUrl = srchUrlBase + "/databases/_search";
        String requestBodyData = "{\"size\":500,\"query\":{\"bool\":{\"must\":[{\"wildcard\":{\"object_name\":{\"value\":\"*cust*\",\"case_insensitive\":true}}},{\"terms\":{\"class_id\":[5,6]}}]}}}";

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate restTemplate = restTemplateBuilder.basicAuthentication("admin", "admin").build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(requestBodyData, headers);
        ResponseEntity<Object> respEntity = restTemplate.exchange(srchUrl, HttpMethod.POST, requestEntity, Object.class);

        resultData.put("searchResults", respEntity);

        results.put("result_data", resultData);
        results.put("result_status", resultStatus);

        return results;
    }

    /**
     * Generates the HTTP basic authentication header that can be used to authenticate against the OpenSearch API
     * @param username
     * @param password
     * @return
     */
    private HttpHeaders createBasicAuthHeader(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }

}
