package io.datallama.api.search;

import java.util.ArrayList;

public class DlSrchBoolExpr {
    private ArrayList<Object> must;
    private ArrayList<Object> must_not;
    private ArrayList<Object> should;

    /**
     * Default Constructor. No params.
     */
    public DlSrchBoolExpr() {
        this.must = new ArrayList<Object>();
        this.must_not = new ArrayList<Object>();
        this.should = new ArrayList<Object>();
    }

    public ArrayList<Object> getMust() {
        return must;
    }

    public void setMust(ArrayList<Object> must) {
        this.must = must;
    }

    public void addMustExpr(Object mustExpr) {
        this.must.add(mustExpr);
    }
    public ArrayList<Object> getMust_not() {
        return must_not;
    }

    public void setMust_not(ArrayList<Object> must_not) {
        this.must_not = must_not;
    }

    public void addMustNotExpr(Object mustNotExpr) {
        this.must_not.add(mustNotExpr);
    }

    public ArrayList<Object> getShould() {
        return should;
    }

    public void setShould(ArrayList<Object> should) {
        this.should = should;
    }

    public void addShouldExpr(Object shouldExpr) {
        this.should.add(shouldExpr);
    }

}
