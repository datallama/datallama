/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.datallama.api.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DlSrchExprBuilder {

    /**
     * Default Constructor: no parameters
     */
    public DlSrchExprBuilder() {
    }

    /**
     * Generates and returns an OpenSearch Boolean Query definition
     * @param resultCountLimit
     * @param srchBoolExpr
     * @return
     */
    public Map<String, Object> getBoolQueryDefn (Integer resultCountLimit, DlSrchBoolExpr srchBoolExpr) {
        Map<String, Object> boolQueryDefn = new HashMap<>();
        boolQueryDefn.put("size", resultCountLimit);

        Map<String, Object> boolExpr = new HashMap<>();
        boolExpr.put("bool", srchBoolExpr);
        boolQueryDefn.put("query", boolExpr);

        return boolQueryDefn;
    }

    public Map<String, Object> getWildcardExpr (String fieldName, String wildcardValue, Boolean caseInsensitive) {
        Map<String, Object> wildcardExpr = new HashMap<>();
        Map<String, Object> fieldNameExpr = new HashMap<>();
        Map<String, Object> valueExpr = new HashMap<>();
        valueExpr.put("value", wildcardValue);
        valueExpr.put("case_insensitive", caseInsensitive);
        fieldNameExpr.put(fieldName, valueExpr);
        wildcardExpr.put("wildcard", fieldNameExpr);

        return wildcardExpr;
    }

    public Map<String, Object> getTermsExpr (String fieldName, ArrayList terms) {
        Map<String, Object> termsExpr = new HashMap<>();
        Map<String, Object> valueExpr = new HashMap<>();
        valueExpr.put(fieldName, terms);
        termsExpr.put("terms", valueExpr);

        return termsExpr;
    }

}
