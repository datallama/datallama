/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.datallama.api.search;

import io.datallama.api.metamodel.DlMetaDataContext;
import io.datallama.api.metamodel.DlMetaDataContextNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The SearchIndexObject class is used to add an object to an OpenSearch index
 */
public class SearchIndexObject {
    private Integer objectId;
    private Integer classId;
    private Integer parentObjectId;
    private Integer parentClassId;
    private String objectName;
    private String objectShortdesc;
    private String elementType;
    private DlMetaDataContext mdContextBuilder;
    private ArrayList<DlMetaDataContextNode> mdContext;
    private Integer dataSensitivityId;

    /**
     * Default constructor. No parameters.
     */
    public SearchIndexObject() {
        this.objectId = 0;
        this.classId = 0;
        this.parentObjectId = 0;
        this.parentClassId = 0;
        this.objectName = "";
        this.objectShortdesc = "";
        this.mdContextBuilder = new DlMetaDataContext();
        this.mdContext = new ArrayList<DlMetaDataContextNode>();
        this.dataSensitivityId = 0;
        this.elementType = "";
    }

    /**
     * Constructor 02. All members defined as parameters, with mdContext parameter passed as a String
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param objectName
     * @param objectShortdesc
     * @param mdContext
     * @param dataSensitivityId
     * @param elementType
     */
    public SearchIndexObject(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                             String objectName, String objectShortdesc, String mdContext, Integer dataSensitivityId,
                             String elementType) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.objectName = objectName;
        this.objectShortdesc = objectShortdesc;
        this.dataSensitivityId = dataSensitivityId;
        this.elementType = elementType;

        this.mdContextBuilder = new DlMetaDataContext();

        this.mdContext = this.mdContextBuilder.generateMdContextList(mdContext);
    }

    /**
     * Constructor 02. All members defined as parameters, with mdContext parameter passed as an ArrayList<DlMetaDataContextNode>
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param objectName
     * @param objectShortdesc
     * @param mdContext
     * @param dataSensitivityId
     * @param elementType
     */
    public SearchIndexObject(Integer objectId, Integer classId, Integer parentObjectId, Integer parentClassId,
                             String objectName, String objectShortdesc, ArrayList<DlMetaDataContextNode> mdContext,
                             Integer dataSensitivityId, String elementType) {
        this.objectId = objectId;
        this.classId = classId;
        this.parentObjectId = parentObjectId;
        this.parentClassId = parentClassId;
        this.objectName = objectName;
        this.objectShortdesc = objectShortdesc;
        this.dataSensitivityId = dataSensitivityId;
        this.elementType = elementType;
        this.mdContext = mdContext;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getParentObjectId() {
        return parentObjectId;
    }

    public void setParentObjectId(Integer parentObjectId) {
        this.parentObjectId = parentObjectId;
    }

    public Integer getParentClassId() {
        return parentClassId;
    }

    public void setParentClassId(Integer parentClassId) {
        this.parentClassId = parentClassId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getObjectShortdesc() {
        return objectShortdesc;
    }

    public void setObjectShortdesc(String objectShortdesc) {
        this.objectShortdesc = objectShortdesc;
    }

    public ArrayList<DlMetaDataContextNode> getMdContext() {
        return mdContext;
    }

    public void setMdContext(ArrayList<DlMetaDataContextNode> mdContext) {
        this.mdContext = mdContext;
    }

    public Integer getDataSensitivityId() {
        return dataSensitivityId;
    }

    public void setDataSensitivityId(Integer dataSensitivityId) {
        this.dataSensitivityId = dataSensitivityId;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }

    public Map<String, Object> generateSrchIndxMappingDefn() {
        Map<String, Object> srchIndxObjMappings = new HashMap<>();
        Map<String, Object> properties = new HashMap<>();

        properties.put("objectId", Map.of("type", "integer"));
        properties.put("classId", Map.of("type", "integer"));
        properties.put("parentObjectId", Map.of("type", "integer"));
        properties.put("parentClassId", Map.of("type", "integer"));
        properties.put("objectName", Map.of("type", "text"));
        properties.put("objectShortdesc", Map.of("type", "text"));
        properties.put("elementType", Map.of("type", "text"));
        properties.put("mdContext", Map.of("type", "text"));
        properties.put("dataSensitivityId", Map.of("type", "integer"));

        srchIndxObjMappings.put("mappings", Map.of("properties", properties));

        return srchIndxObjMappings;
    }

}
