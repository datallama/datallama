#!/usr/bin/bash

cat <<EOF
---------------------------
RUN DATE (UTC)
$(date --utc)
---------------------------
EOF

#option set to fail if variables are unset
set -o nounset
set -o xtrace
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"}/lib.sh

usage(){
cat <<EOF
USAGE:

rebuild_containers.sh [ --proxy --proxy-ip=IP --proxy-port=PORT ] [ --container-name { ui | api | db } ]
rebuild_containers.sh [ -P -i IP -p PORT ] [ -c { ui | api | db } ]

EOF
}

TEMP=$(getopt -o c:C:Pi:p:h \
    --long container-name:,https-ip,proxy,proxy-ip:,proxy-port:,help \
    -n `basename $0` -- "$@")

if [ $? != 0 ] ; then usage >&2 ; exit 1 ; fi

eval set -- "$TEMP"
while true ; do
        case "$1" in
                -c|--container-name) CONTAINER_NAME="${2}" ; shift 2;;
                -C|--https-ip)       HTTPS_CERT_IP="${2}" ;  shift 2;;
                -P|--proxy)          PROXY_TRUE="true" ;     shift ;;
                -i|--proxy-ip)       PROXY_IP="${2}" ;       shift 2;;
                -p|--proxy-port)     PROXY_PORT="${2}" ;     shift 2;;
		-h|--help)           usage &&                exit 1 ;;
                --) shift ; break ;;
		*) usage && exit 1 ;;
        esac
done

CONTAINER_NAME=${CONTAINER_NAME:-all}
VALID_CONTAINERS=("api" "db" "ui" "all")

if [[ " ${VALID_CONTAINERS[*]} " =~ " ${CONTAINER_NAME} " ]]; then
	rebuild $CONTAINER_NAME
else
	echo "invalid container name ${CONTAINER_NAME}; cannot build " && exit 1;
fi

#${COMPOSE_BINARY} exec ui ./fixcerts.sh '192.168.122.xxx'
${DOCKER_BINARY} cp datallama_ui_1:/usr/local/apache2/conf/server-ca.crt .

# uncomment below to push image
#${DOCKER_BINARY} login --username datallama docker.io
#${DOCKER_BINARY} push
