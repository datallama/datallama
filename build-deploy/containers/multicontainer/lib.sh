#!/usr/bin/env bash
VER=14
TEMPLATE_VER=13
INSTANCE_USER='fedora'
INSTANCE_NAME="dl-dev-05-lnx-DLMVP-109-${VER}"
DL_HOST="dl${VER}"
SSH_CONFIG="$HOME/.ssh/config"

function build_instance(){
    aws --profile datallama ec2 run-instances \
        --launch-template="LaunchTemplateId=lt-070df7ff756ed85c5,Version=${TEMPLATE_VER}" \
        --tag-specifications="ResourceType=instance,Tags=[{Key=Name,Value=${INSTANCE_NAME}}]"
}

function confirm_instance(){
    aws --profile datallama ec2 describe-instances | \
            jq ".Reservations[].Instances[] | select(.Tags[].Value==\"${INSTANCE_NAME}\" and .Tags[].Key==\"Name\") | inside(.)"
}

function get_instance_dnsname(){
    aws --profile datallama ec2 describe-instances | \
            jq ".Reservations[].Instances[] | select(.Tags[].Value==\"${INSTANCE_NAME}\" and .Tags[].Key==\"Name\") | .NetworkInterfaces[].Association.PublicDnsName" --raw-output
}

function ssh_config_fixer(){
   $(grep -q "${DL_HOST}" $SSH_CONFIG)
   [[ $? -eq 0 ]] && {
      sed -i "/Host ${DL_HOST}/,/Host /s/Hostname.*/Hostname $(get_instance_dnsname)/" $SSH_CONFIG
      sed -i "/Host ${DL_HOST}/,/Host /s/User.*/User ${INSTANCE_USER}/" $SSH_CONFIG
   } || {
      cat >>$SSH_CONFIG <<EOF
Host ${DL_HOST}
       User ${INSTANCE_USER}
       Hostname $(get_instance_dnsname)
       IdentityFile $HOME/.ssh/id_rsa
EOF
   }
cat ~/.ssh/config
}

function install_docker_dnf(){
    #now incorporated into lauch template so will always run on initial start of ec2 instance
    sudo dnf --assumeyes config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    sudo dnf --assumeyes install docker-ce docker-ce-cli containerd.io docker-compose git vim
    sudo grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
    sudo usermod -aG docker $USER
    sudo systemctl enable docker
    sudo systemctl start docker
    sudo reboot
    docker run hello-world
}

function deploy_package(){
   cd ..
   tar czvf dl${VER}.tar.gz --exclude 'datallama/.git' datallama
   scp dl${VER}.tar.gz dl${VER}:~/
   ssh dl${VER} "tar xzvf dl${VER}.tar.gz; cd datallama; docker-compose up --build --detach"
   echo "login with: docker exec -it datallama_allinone_1 bash"
   #TODO work in progress - the next command needs to be done inside the container
   #ssh dl${VER} 'docker exec datallama_allinone_1 "./init.sh"'
   cd datallama
}

#[[ `confirm_instance` != 'true' ]] && build_instance
#deploy_package
#[[ `confirm_instance` == 'true' ]] && ssh_config_fixer || echo "no ssh/config changes as it doesn't exist"

#compose binary
export BUILDAH_FORMAT=docker
COMPOSE_BINARY='podman-compose'
DOCKER_BINARY='podman'

CONTAINER_VERSION='beta-2.7.2'
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd ../../.. && pwd )"

cat <<EOF
SCRIPT_DIR=$SCRIPT_DIR
PROJECT_DIR=$PROJECT_DIR
EOF

function rebuild(){
   CONTAINER_NAME=$1
   if [[ ${CONTAINER_NAME} != "all" ]] ; then
      echo "rebuilding ${CONTAINER_NAME}"
      docker rm --force datallama_${CONTAINER_NAME}_1 && docker rmi datallama/${CONTAINER_NAME}:${CONTAINER_VERSION}
      build
      ${COMPOSE_BINARY} --file ${PROJECT_DIR}/docker-compose.yaml up --detach ${CONTAINER_NAME} 

   else
      echo "rebuilding ${CONTAINER_NAME}"
      ${COMPOSE_BINARY} --file ${PROJECT_DIR}/docker-compose.yaml down
      build
      ${COMPOSE_BINARY} --file ${PROJECT_DIR}/docker-compose.yaml up --detach
   fi
}

function build(){
   PROXY_TRUE=false
   DOCKER_BINARY=docker
   CONTAINER_PREFIX=$1
   PROJECT_DIR=$2
   CONTAINER_VERSION=$3
   if [[ $PROXY_TRUE == "true"  ]]; then
      proxy_ip=${PROXY_IP:-host.containers.internal}
      proxy_port=${PROXY_PORT:-3128}
#      ${COMPOSE_BINARY} build \
#         --build-arg proxy=true \
#	 --build-arg proxy_ip=${proxy_ip:?"must have proxy ip value if proxy used"} \
#	 --build-arg proxy_port=${proxy_port:?"must have proxy port if proxy used"} \
#	 --no-cache $CONTAINER_NAME
#
cat <<EOF
	${DOCKER_BINARY} build -f ${PROJECT_DIR}/api/Dockerfile --format docker -t datallama/api:beta-${CONTAINER_VERSION} \
		--no-cache --build-arg volume --build-arg proxy=true \
		--build-arg proxy_ip=${proxy_ip:?"must have proxy ip value if proxy used"} \
		--build-arg proxy_port=${proxy_port:?"must have proxy port if proxy used"} ${PROJECT_DIR}/api

	${DOCKER_BINARY} build -f ${PROJECT_DIR}/ui/Dockerfile --format docker -t datallama/ui:beta-${CONTAINER_VERSION} \
		--no-cache --build-arg volume --build-arg proxy=true \
		--build-arg proxy_ip=${proxy_ip:?"must have proxy ip value if proxy used"} \
		--build-arg proxy_port=${proxy_port:?"must have proxy port if proxy used"} ${PROJECT_DIR}/ui

	${DOCKER_BINARY} build -f ${PROJECT_DIR}/db/Dockerfile --format docker -t datallama/db:beta-${CONTAINER_VERSION} \
		--no-cache --build-arg MYSQL_ROOT_PASSWORD=secret --build-arg volume --build-arg proxy=true \
		--build-arg proxy_ip=${proxy_ip:?"must have proxy ip value if proxy used"} \
		--build-arg proxy_port=${proxy_port:?"must have proxy port if proxy used"} ${PROJECT_DIR}/db
EOF
   else
      #${COMPOSE_BINARY} --file ${PROJECT_DIR}/docker-compose.yaml build --format docker $CONTAINER_NAME
      #${COMPOSE_BINARY} --file ${PROJECT_DIR}/docker-compose.yaml build $CONTAINER_NAME

      ${DOCKER_BINARY} build \
	      -f ${PROJECT_DIR}/api/Dockerfile \
	      -t ${TAG_PREFIX}/api:${CONTAINER_VERSION} \
	      ${PROJECT_DIR}/api

      ${DOCKER_BINARY} build \
	      -f ${PROJECT_DIR}/ui/Dockerfile \
	      -t ${TAG_PREFIX}/ui:${CONTAINER_VERSION} \
	      ${PROJECT_DIR}/ui

      ${DOCKER_BINARY} build \
	      -f ${PROJECT_DIR}/db/Dockerfile \
	      -t ${TAG_PREFIX}/db:${CONTAINER_VERSION} \
              --build-arg MYSQL_ROOT_PASSWORD=secret \
	      ${PROJECT_DIR}/db

   fi
}

