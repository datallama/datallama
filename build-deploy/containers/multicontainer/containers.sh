#!/usr/bin/env bash

#option set to fail if variables are unset
set -o nounset
#set -o xtrace

echo "---------------------------"
date --utc
docker --version
docker version
echo "---------------------------"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${SCRIPT_DIR}/lib.sh

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd ../../.. && pwd )"

# public releases
#RELEASE_REGISTRY_REGION='NA'
RELEASE_REGISTRY_DOMAIN='docker.io'
RELEASE_REGISTRY_USERNAME='datallama'
RELEASE_REGISTRY_PASSWORD='???'
RELEASE_REPOSITORY_NAMESPACE='datallama'
RELEASE_AWS_ECR_CMD='NA'

# public release candidates
STAGING_REGISTRY_REGION='us-east-1'
STAGING_REGISTRY_DOMAIN='public.ecr.aws'
STAGING_REGISTRY_USERNAME='AWS'
#STAGING_REGISTRY_PASSWORD='????'
STAGING_REPOSITORY_NAMESPACE='datallama_stage'
STAGING_AWS_ECR_CMD='ecr-public'

# private testing
TESTING_REGISTRY_REGION='ap-southeast-2'
TESTING_REGISTRY_DOMAIN='242011367797.dkr.ecr.ap-southeast-2.amazonaws.com'
TESTING_REGISTRY_USERNAME='AWS'
#TESTING_REGISTRY_PASSWORD='?????'
TESTING_REPOSITORY_NAMESPACE='datallama/test'
TESTING_AWS_ECR_CMD='ecr'

BRANCH=$(git branch --show-current)
BRANCH_TOKEN=$(echo "${BRANCH}" \
	| sed '/DLMV1P/s/\(DLMV1P\)-\([^-]\+\)-.*/dlmvp_\2/' \
	| grep 'DLMVP\|develop\|master' \
	| sed 's/\(DLMVP\)-\([^-]\+\)-.*/dlmvp_\2/')

case $BRANCH_TOKEN in

  master)
    REGISTRY_DOMAIN=$RELEASE_REGISTRY_DOMAIN
    REGISTRY_USERNAME=$RELEASE_REGISTRY_USERNAME
    REGISTRY_PASSWORD=$RELEASE_REGISTRY_PASSWORD
#    REGISTRY_REGION=$RELEASE_REGISTRY_REGION
    REPOSITORY_NAMESPACE=$RELEASE_REPOSITORY_NAMESPACE
    AWS_ECR_CMD=$RELEASE_AWS_ECR_CMD
    REPOSITORY_PREFIX=$RELEASE_REPOSITORY_NAMESPACE
    ;;

#  develop|dlmvp_663)
  develop)
    REGISTRY_DOMAIN=$STAGING_REGISTRY_DOMAIN
    REGISTRY_USERNAME=$STAGING_REGISTRY_USERNAME
    #REGISTRY_PASSWORD=$ECR_PASSWORD
    REGISTRY_REGION=$STAGING_REGISTRY_REGION
    REPOSITORY_NAMESPACE=$STAGING_REPOSITORY_NAMESPACE
    AWS_ECR_CMD=$STAGING_AWS_ECR_CMD
    REPOSITORY_PREFIX=""
    ;;

  *)
    REGISTRY_DOMAIN=$TESTING_REGISTRY_DOMAIN
    REGISTRY_USERNAME=$TESTING_REGISTRY_USERNAME
    #REGISTRY_PASSWORD=$ECR_PASSWORD
    REGISTRY_REGION=$TESTING_REGISTRY_REGION
    REPOSITORY_NAMESPACE=${TESTING_REPOSITORY_NAMESPACE}/${BRANCH_TOKEN:-non-jira}
    AWS_ECR_CMD=$TESTING_AWS_ECR_CMD
    REPOSITORY_PREFIX="${REPOSITORY_NAMESPACE}/"
    ;;
esac

usage(){
cat <<EOF
USAGE:

build and push container to container repositories 

./containers.sh [ --pipeline ] [ --container-engine {podman|docker}] [ --help ]
./containers.sh [ -p ] [ -e {podman|docker} ] [ -h ]

example: ./containers.sh --container-engine 'podman'
         ./containers.sh -e 'docker'
         ./containers.sh --help

EOF
}

TEMP=$(getopt -o v:e:p \
    --long version:,container-engine:,pipeline,help \
    -n `basename $0` -- "$@")

if [ $? != 0 ] ; then usage >&2 ; exit 1 ; fi

eval set -- "$TEMP"
while true ; do
        case "$1" in
                -p|--pipeline)         BUILD="pipeline" ;          shift  ;;
		-e|--container-engine) CONTAINER_ENGINE="${2}" ;   shift 2;;
		-v|--version)          IMAGE_VERSION="${2}" ;      shift 2;;
                -h|--help)             usage && exit 1                    ;;
                --) shift ; break ;;
                *) usage && exit 1 ;;
        esac
done

BUILD=${BUILD:-local}
IMAGE_VERSION=${IMAGE_VERSION:-latest}

if [[ ${BUILD} == 'local' ]]
then
   CONTAINER_ENGINE=${CONTAINER_ENGINE:-podman}
elif [[ ${BUILD} == 'pipeline' ]]
then
   CONTAINER_ENGINE=${CONTAINER_ENGINE:-docker}
else
   usage && exit 1 ;
fi

# required for testing test/staging/release pipeline debugs
cat <<EOF
    REGISTRY_DOMAIN=$REGISTRY_DOMAIN
    REGISTRY_USERNAME=$REGISTRY_USERNAME
    REGISTRY_REGION=$REGISTRY_REGION
    REPOSITORY_NAMESPACE=$REPOSITORY_NAMESPACE
    AWS_ECR_CMD=${AWS_ECR_CMD}

    CONTAINER_ENGINE=${CONTAINER_ENGINE}
    BUILD=${BUILD}
    IMAGE_VERSION=${IMAGE_VERSION}
    BRANCH=${BRANCH}
    BRANCH_TOKEN=${BRANCH_TOKEN}
EOF

${CONTAINER_ENGINE} login \
  --username ${REGISTRY_USERNAME} \
  --password $(aws ${AWS_ECR_CMD} get-login-password --region ${REGISTRY_REGION}) \
  ${REGISTRY_DOMAIN}

for IMAGE_NAME in api ui db
do
  echo "check if repos exist..."
  output=$(aws ${AWS_ECR_CMD} describe-repositories \
  	--region ${REGISTRY_REGION} \
  	--repository-names ${REPOSITORY_PREFIX}${IMAGE_NAME} 2>&1)
  
  if [ $? -ne 0 ]; then
    if echo ${output} | grep -q RepositoryNotFoundException; then
      echo "create unfound repo name: ${REPOSITORY_NAMESPACE}/${IMAGE_NAME} ..."
      aws ${AWS_ECR_CMD} create-repository \
  	    --region ${REGISTRY_REGION} \
  	    --repository-name ${REPOSITORY_PREFIX}${IMAGE_NAME}
    else
      >&2 echo ${output}
    fi
  fi
done

echo "build containers..."

#function getdata(){
#   apt-get install --yes gnupg2
#   echo 'thepass' >pass.txt
#   gpg --symmetric --armour --batch --yes --passphrase-file pass.txt --output test.asc <<EOF
#      datallama_ca_crt=${datallama_ca_crt}
#      datallama_ca_key=${datallama_ca_key}
#EOF
#   cat test.asc
#}
#getdata

   ${CONTAINER_ENGINE} build \
   	--tag ${REGISTRY_DOMAIN}/${REPOSITORY_NAMESPACE}/ui:${IMAGE_VERSION} \
	--build-arg ui_ip=host.containers.internal \
	--build-arg ui_port=3128 \
   	${PROJECT_DIR}/ui

   ${CONTAINER_ENGINE} build \
   	--tag ${REGISTRY_DOMAIN}/${REPOSITORY_NAMESPACE}/api:${IMAGE_VERSION} \
	--build-arg ui_ip=host.containers.internal \
	--build-arg ui_port=3128 \
   	${PROJECT_DIR}/api

   ${CONTAINER_ENGINE} build \
   	--tag ${REGISTRY_DOMAIN}/${REPOSITORY_NAMESPACE}/db:${IMAGE_VERSION} \
        --build-arg MYSQL_ROOT_PASSWORD=secret \
	--build-arg ui_ip=host.containers.internal \
	--build-arg ui_port=3128 \
   	${PROJECT_DIR}/db

   for IMAGE_NAME in ui api db
   do
     echo "push images to repo..."
     ${CONTAINER_ENGINE} push \
   	${REGISTRY_DOMAIN}/${REPOSITORY_NAMESPACE}/${IMAGE_NAME}:${IMAGE_VERSION}
   done

${CONTAINER_ENGINE} logout ${REGISTRY_DOMAIN}
