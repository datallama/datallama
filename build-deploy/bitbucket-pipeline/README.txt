The top level script for the datallama bitbucket repository CICD pipeline is bitbucket-pipelines.yml in the top level directory of the repository.

This directory contains scripts that support the datallama repository bitbucket pipeline, such as the Dockerfile for the container image that supports the pipeline

These scripts have not been tested as at 14/03/2024.
