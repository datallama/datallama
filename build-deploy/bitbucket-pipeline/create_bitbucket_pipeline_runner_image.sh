#!/usr/bin/bash
INDEX=0.3
CNTNR_CMD=docker
AWS_REGION=ap-southeast-2

${CNTNR_CMD} build --tag datallama/bitbucket_pipeline:${INDEX} .
aws ecr-public get-login-password --region ${AWS_REGION} | ${CNTNR_CMD} login --username AWS --password-stdin 242011367797.dkr.ecr.ap-southeast-2.amazonaws.com
${CNTNR_CMD} push datallama/bitbucket_pipeline:${INDEX} 242011367797.dkr.ecr.ap-southeast-2.amazonaws.com/datallama_stage/bitbucket_pipeline:${INDEX}
