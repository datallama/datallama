#!/usr/bin/python
# 
# deploys a single task 
#

import os, os.path, shutil, json, subprocess, platform
from optparse import OptionParser

from glob import glob

class DlApiBuildDeploy(object):

    osPlatform = ""
    srcBase = ""
    apiBaseDir = ""
    mvnCmd = ""


    '''
        Constructure initialises the source base directory 
    '''
    def __init__(self):
        self.srcBase = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
        self.osPlatform = platform.system()
        print("srcBase = %s" % self.srcBase)
        print("osPlatform = %s" % self.osPlatform)

    '''
        readEnvConfig reads the Database environment settings either from the file specific in argument sEnvFileName or,
        if sEnvFileName is DEFAULT, from the file default_env.json which is assumed to be in the same dir as the execution script.  
    '''
    def readEnvConfig(self, sEnvFileName):
        exitStatus = 0

        if (sEnvFileName.upper() == "DEFAULT"):
            sFileNameFullPath = self.srcBase + "/build-deploy/bin/default_env.json"
        else:
            sFileNameFullPath = sEnvFileName

        if not(os.path.exists(sFileNameFullPath)):
            print("ERROR: can't open enviroment file %s." % sFileNameFullPath)
            exitStatus = 120

        if exitStatus == 0:
            fh = open(sFileNameFullPath, "r")
            jsonDoc = json.load(fh)
            fh.close()

            try:
                self.apiBaseDir = jsonDoc["apibasedir"]
            except KeyError:
                print("ERROR: can't load all API enviroment settings from file %s." % sFileNameFullPath)
                exitStatus = 122

        if exitStatus == 0:
            # check that the mvn utility is available
            self.mvnCmd = "mvn"
            if self.osPlatform == "Windows":
                self.mvnCmd = "mvn.cmd"
            cmd = [self.mvnCmd, "--version"]
            try:
                output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
            except OSError as exc:
                print("ERROR: could not execute command %s" % cmd[0])
                print(exc)
                exitStatus = 121

        if exitStatus == 0:
            print("found environment file and maven utility")

        return exitStatus

    '''
        apiBuild invokes maven to build the API source source code into a self executable jar file 
    '''
    def apiBuild(self):
        exitStatus = 0

        os.chdir(self.srcBase + "/api")

        # check whether the API base directory path exists in the default location
        # or in the location specified in the env file
        print("setting up the API runtime directory structure")
        if len(self.apiBaseDir) == 0:
            self.apiBaseDir = ["/opt", "/datallama", "/api"]

        if self.osPlatform == "Windows":
            checkDir = "C:" + self.apiBaseDir[0]
        else:
            checkDir = self.apiBaseDir[0]

        if not(os.path.exists(checkDir)):
            os.mkdir(checkDir)

        checkDir = checkDir + self.apiBaseDir[1]
        if not(os.path.exists(checkDir)):
            os.mkdir(checkDir)

        checkDir = checkDir + self.apiBaseDir[2]
        if os.path.exists(checkDir):
            shutil.rmtree(checkDir)
        os.mkdir(checkDir)
        os.mkdir(checkDir + "/log")


        cmd = [self.mvnCmd, "clean"]
        try:
            print("running mvn clean ..."),
            output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
        except OSError as exc:
            print("ERROR: failed while calling mvn clean")
            print(exc)
            exitStatus = 131
        except subprocess.CalledProcessError as exc:
            print("ERROR: maven returned a non-zero result")
            print(exc)
            exitStatus = 135

        if exitStatus == 0:
            print("mvn clean succeeded.")
            print("running mvn package ..."),

            cmd = [self.mvnCmd, "package"]
            try:
                output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
            except OSError as exc:
                print("ERROR: failed when executing mvn package")
                print(exc)
                exitStatus = 132
            except subprocess.CalledProcessError as exc:
                print("ERROR: maven returned a non-zero result")
                print(exc)
                exitStatus = 133


        if exitStatus == 0:
            print("mvn packege succeeded. API built! Deploying to runtime directory")
            # copy api-0.1.jar from target dir to api base dir as dl-api.jar
            sourceFile = self.srcBase + "/api/target/api-0.1.jar"
            targetFile = "".join(self.apiBaseDir) + "/dl-api.jar"

            if self.osPlatform == "Windows":
                targetFile = "C:" + targetFile

            try:
                shutil.copyfile(sourceFile, targetFile)
            except OSError as exc:
                print("ERROR: failed to copy API jar file to runtime directory")
                print(exc)
                exitStatus = 134

            sourceFile = self.srcBase + "/build-deploy/bin/dl-api-ctl."
            targetFile = "".join(self.apiBaseDir) + "/"

            if self.osPlatform == "Windows":
                sourceFile = sourceFile + "bat"
                targetFile = "C:" + targetFile
            else:
                sourceFile = sourceFile + "sh"

            try:
                shutil.copyfile(sourceFile, targetFile)
            except OSError as exc:
                print("ERROR: failed to copy API jar file to runtime directory")
                print(exc)
                exitStatus = 134

        return exitStatus

    '''
        fullDbDeploy builds all DL database objects into the database 
    '''
    def apiDeploy(self):
        exitStatus = 0

        return exitStatus



# call as stand alone script
if __name__ == "__main__":

    # parse command line options
    optParser = OptionParser(description='Builds the Data Llama API from java source code and deploys it to the API runtime directory structure.')
    optParser.add_option("--env", dest="environment", help="specify a run environment for the Data Llama API")

    apiBuildDeploy = DlApiBuildDeploy()

    # parse environment option is provided
    envFile = "default"
    (options, args) = optParser.parse_args()
    if options.environment:
        envFile = options.environment

    # read configuration file
    result = apiBuildDeploy.readEnvConfig(envFile)

    # build the API from source code
    if result == 0:
        result = apiBuildDeploy.apiBuild()

    if result > 0:
        print("\nERRORS: one or more errors encountered during API build and deploy")
    else:
        print("\nSUCCESS: no errors encountered during API build and deploy")
