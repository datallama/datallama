#!/bin/bash

TARGET_DIR=/var/www/html/dldev03
rm -Rf ${TARGET_DIR}/*

cp -R /home/richard/src/datallama/ui/* ${TARGET_DIR}/

cp /home/richard/cfg/dldeploy/dataLlamaConfig.js ${TARGET_DIR}/js/dataLlamaConfig.js

