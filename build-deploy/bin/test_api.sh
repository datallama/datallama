#!/usr/bin/env bash
[ $# == 1 ] && URL=$1 || URL='home/overview/home.overview'

TOKEN=$(curl -X GET 'http://localhost:8080/auth/login02?dlUser=rupert&dlPwd=rupert' \
  -is \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Accept-Language: en-US,en;q=0.5' \
  -H 'Accept: application/json, text/plain, */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Host: localhost:8080' \
  -H 'Origin: http://localhost:81' \
  -H 'Pragma: no-cache' \
  -H 'Referer: http://localhost:81/' | \
  tail -n1 | jq .resultData.authToken --raw-output)

curl -X GET "http://localhost:8080/${URL}?dlAuth=${TOKEN}" \
 --compressed \
 -H 'Accept-Language: en-US,en;q=0.5' \
 -H 'Accept: application/json, text/plain, */*' \
 -H 'Cache-Control: no-cache' \
 -H 'Connection: keep-alive' \
 -H 'Origin: http://localhost:81' \
 -H 'Pragma: no-cache' \
 -H 'Referer: http://localhost:81/' \
