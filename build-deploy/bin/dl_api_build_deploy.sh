#!/bin/bash

SRC_BASE=$HOME/src/datallama
TARGET_DIR=/opt/datallama/api

cd ${SRC_BASE}/api
mvn clean
if [ $? -gt 0 ]; then
	echo "ERROR: mvn clean failed."
	exit 101
fi

mvn package
if [ $? -gt 0 ]; then
	echo "ERROR: mvn package failed."
	exit 102
fi

echo -e "\n* * * * * * * * * * * *\nMaven build succeeded.\n* * * * * * * * * * * *\n"

# deploy the executable jar
${TARGET_DIR}/dlapictl.sh stop
cp ${SRC_BASE}/api/target/api-0.1.jar ${TARGET_DIR}/dl-api.jar
${TARGET_DIR}/dlapictl.sh start

exit 0


