/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for managing the diagrams.
 *
 */
function DiagramService($location, $uibModal, notify, HttpService, UtilityService) {
    this.canvas = null;             // the Draw2d canvas that the diagram will be displayed on.
    this.scope = null;              // the scope of the diagram controller.

    this.currentDiagram = {         // the current diagram
        id: 0,                      // the id of the diagram.
        name: "",                   // the name of the diagram.
        properties: {},             // the diagram properties.
        entities: [],               // the entities in the diagram (each element has all the information to display a diagram entity).
        connectors: [],             // the connectors in the diagram (each element has all the information to display a diagram connector).
        entityDeletes: [],          // the entities that will deleted from the diagram with the next save (only the id is saved).
        connectorDeletes: [],       // the connectors that will deleted from the diagram with the next save (only the id is saved).
        allEntities: [],            // all defined entities with information about their inclusion in the current diagram.
        allConnectors: [],          // all defined connectors with information about their inclusion in the current diagram.
        customData: {}              // the diagram specific custom data.
    };

    this.response = {
        result_status: 0,
        result_data: {}
    }

    this.newDiagram = {};           // the definition for a new Diagram
    this.closeAfterSave = false;    // whether or not to close the diagram after saving.
    this.selectedDiagramId = 0;     // the id of the diagram that was selected to be opened.
    this.zoomRate = 0.9             // the rate at which the zoom buttons zoom (small = faster)

    this.notifyTemplate = 'views/common/notify.html';
    notify.config({duration: 2000});

    // Diagram specific call backs
    this.newEntity = null;              // these must be defined in each diagram controller and must accept a definition
    this.newConnector = null;           // and return the specific draw2d object relevant for that diagram.
    this.customConfig = null;           // this is for any custom config that must happen after retrieving the content from the DB.

    // API URLS
    this.getDiagramContentURL = "";     // the API call that returns the diagram content. The result data must be the same form as this.currentDiagram.
    this.getAllEntitiesURL = "";        // the API call that returns all the defined entities that could be added to the diagram.
    this.postDiagramContentURL = "";    // the API call to save the diagram content.
    this.deleteDiagramURL = "";         // the API call to delete the diagram.
    this.createDiagramURL = "";         // the API call to create a new diagram.
    this.getEntityDetailURL = "";       // the API call to get additional details about the entity.

    this.locationAfterClose = "";          // the location state that will be activated after a diagram is closed.

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      Functions that can be invoked from an DL Diagram Controller
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    this.initScopeVariables = function() {
        this.scope.saveMdObject = this.saveMdObject;
        this.scope.checkClose = this.checkClose;
        this.scope.saveDiagram = this.saveDiagram;
        this.scope.closeMdObject = this.closeDiagram;
        this.scope.confirmDelete = this.confirmDelete;
        this.scope.openDiagram = this.openDiagram;
        this.scope.readDiagramFromDb = this.readDiagramFromDb;
        this.scope.deleteMdObject = this.deleteDiagram;
        this.scope.createDiagramInDb = this.createDiagramInDb;
        this.scope.createDiagram = this.createDiagram;
        this.scope.setSelectedDiagramId = this.setSelectedDiagramId;
        this.scope.updateSelectedObjects = this.updateSelectedObjects;
        this.scope.setViewMode = this.setViewMode;
        this.scope.getAllEntities = this.getAllEntities;
    };

    /**
     *
     */
    this.readDiagramFromDb = function() { //read the diagram definition from the database and configure the current diagram.
        const GET_URL = this.getDiagramContentURL + this.selectedDiagramId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        this.currentDiagram = response.data.result_data;
                        this.currentDiagram.entityDeletes = [];
                        this.currentDiagram.connectorDeletes = [];
                        this.scope.currentDiagram.id = this.currentDiagram.id;
                        this.scope.currentDiagram.name = this.currentDiagram.name;
                        this.scope.currentDiagram.customData = this.currentDiagram.customData;
                        let properties = {};
                        try {
                            properties = JSON.parse(this.currentDiagram.properties);
                        }
                        catch {
                            console.log("Warning: The diagram properties (" + this.currentDiagram.properties + ") are not valid.")
                        }
                        this.currentDiagram.properties = properties;
                        this.scope.currentDiagram.properties = this.currentDiagram.properties;

                        if(this.currentDiagram.properties.allEntities){
                            try{
                                this.getAllEntities();
                            }
                            catch{
                                console.log("Warning: Selection table entities not initialised.")
                            }
                        }

                        this.loadDiagram();

                        if(this.currentDiagram.properties.customConfig){
                            try {
                                this.customConfig();
                            }
                            catch {
                                console.log("Warning: The function customConfig() has not been initialised.");
                            }
                        }
                    }.bind(this)
                }
            ]
        );
    }.bind(this);

    /**
     *
     */
    this.getAllEntities = function() {
        this.scope.updateGetAllEntitiesURL();
        const GET_URL = this.getAllEntitiesURL;
        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        this.currentDiagram.allEntities = response.data.result_data.allEntities;
                        this.scope.allEntities = this.currentDiagram.allEntities;
                        this.currentDiagram.allConnectors = response.data.result_data.allConnectors;
                    }.bind(this)
                }
            ]
        );
    }.bind(this);

    /**
     * Creates entities and connectors on a diagram canvas when the diagram is opened.
     * @function loadDiagram
     */
    this.loadDiagram = function() {

        // 1. fix the scrolling bug
        this.scope.canvas.setScrollArea(window);

        // 2. create entities
        const tempEntities = this.currentDiagram.entities;
        this.currentDiagram.entities = [];
        for (let i = 0; i < tempEntities.length; i++) {
            const entity = tempEntities[i];
            this.addEntity(entity);
        }

        // 3. create connectors
        const tempConnectors = this.currentDiagram.connectors;
        this.currentDiagram.connectors = [];
        for (let i = 0; i < tempConnectors.length; i++) {
            const cnct = tempConnectors[i];

            const startIds = cnct.cnctStartPort.split(":");
            const cnctStartNtt = startIds[0]+":"+startIds[1];
            const endIds = cnct.cnctEndPort.split(":");
            const cnctEndNtt = endIds[0]+":"+endIds[1];

            let description = "None specified";
            if(cnct.cnctDesc && cnct.cnctDesc.length>0){
                description = cnct.cnctDesc;
            }

            let vertices = {};
            try{
                vertices = JSON.parse(cnct.cnctVertices);
            }
            catch{
                console.log("Warning. The connection vertices ("+ cnct.cnctVertices +") are not valid.")
            }
            const connector = $.extend(cnct,{
                cnctId : cnct.cnctId,
                classId : cnct.classId,
                cnctName : cnct.cnctName,
                cnctDesc : description,
                cnctStartNtt: cnctStartNtt,
                cnctEndNtt: cnctEndNtt,
                cnctVertices: vertices
            })
            this.addConnector(connector);
        }

        // 4. set the Z order
        tempEntities.sort(function (a,b) { return(a.z-b.z)});
        tempConnectors.sort(function (a,b) { return(a.z-b.z)});
        while(tempConnectors.length + tempEntities.length > 0){
            if(tempConnectors.length === 0 || tempEntities.length > 0 && tempEntities[0].z < tempConnectors[0].z){
                const nttDetails = tempEntities.shift();
                const nttObject = this.canvas.getFigure(nttDetails.classId+":"+nttDetails.nttId);
                nttObject.toFront();
            }
            else{
                const cnctDetails = tempConnectors.shift();
                const cnctObject = this.canvas.getLine(cnctDetails.classId+":"+cnctDetails.cnctId);
                cnctObject.toFront();
            }
        }

        // 5. create entity groups
        this.made = [];
        if(this.currentDiagram.properties.groups){
            for(let i = 0; i < this.currentDiagram.properties.groups.length; i++){
                const group = this.currentDiagram.properties.groups[i];
                if(!this.checkGroupMade(group.id)){
                    const newGroupObject = this.makeGroup(group.elements)
                    this.made.push({
                        id: group.id,
                        newId: newGroupObject.id
                    });
                }
            }
        }

        // 6. reset deletes arrays
        this.currentDiagram.connectorDeletes = [];
        this.currentDiagram.entityDeletes = [];
        this.scope.dirtyCanvas = false;
    }.bind(this);

    //returns true if the group with id has already been made and false otherwise
    this.checkGroupMade = function(id){
        for(let i = 0; i < this.made.length; i++){
            if(this.made[i].id === id){
                return true;
            }
        }
        return false;
    }

    //Checks if the element is a figure or group that has been made. If it's a group that has already been made then the
    //id passed in is not the id that is used in the canvas so returns the new id. If it's a group that hasn't been made
    //yet return -1.
    this.checkElementId = function(id){
        for(let i = 0; i < this.made.length; i++){
            if(this.made[i].id === id){
                return this.made[i].newId;
            }
        }
        const figures = this.canvas.getFigures().data;
        for(let i = 0; i < figures.length; i++){
            if(figures[i].id === id){
                return id;
            }
        }
        return -1;
    }

    //A recursive function that creates the groups from the data stored in the diagram properties.
    this.makeGroup = function(elements){
        const selection = new draw2d.util.ArrayList;
        for(let i = 0; i < elements.length; i++){
            const canvasId = this.checkElementId(elements[i])
            if(canvasId !== -1){
                selection.push(this.canvas.getFigure(canvasId));
            }
            else{
                let newGroupElements = [];
                for(let j = 0; j < this.currentDiagram.properties.groups.length; j++){
                    const newGroup = this.currentDiagram.properties.groups[j];
                    if(newGroup.id === elements[i]){
                        newGroupElements = newGroup.elements;
                        break;
                    }
                }
                const newGroupObject = this.makeGroup(newGroupElements)
                this.made.push({
                    id: elements[i],
                    newId: newGroupObject.id
                });
                selection.push(newGroupObject);

            }
        }
        //Todo: this is a bit dodgy because I am just assuming that the new group is the latest figure. There should be
        // some way of getting the group from the command return
        this.canvas.getCommandStack().execute(new draw2d.command.CommandGroup(this.canvas, selection));
        return(this.canvas.getFigures().data[0]);
    }

    /**
     * Description: saves all entities and connectors in the diagram
     */
    this.saveDiagram = function() {
        const entities = [];
        const connectors = [];

        if (this.currentDiagram.id === 0) {
            notify({
                message: "There is no open diagram. Nothing to save.",
                classes: "alert-warning",
                templateUrl: this.notifyTemplate
            });
            return;
        }

        // 1. get all the entities and groups in the diagram
        const groups = [];
        this.canvas.getFigures().each( function(i, currentEntity) {
            if(currentEntity.cssClass !== "draw2d_shape_composite_Group") {
                const entityDetails = {
                    classId: currentEntity.classId,
                    nttId: currentEntity.nttId,
                    x: currentEntity.x,
                    y: currentEntity.y,
                    z: currentEntity.getZOrder(),
                    width: currentEntity.width,
                    height: currentEntity.height,
                    colorDefn: currentEntity.colorDefn,
                    colorSource: currentEntity.colorSource
                }
                entities.push(entityDetails)
            }
            else {
                const elements = [];
                for(let i = 0; i < currentEntity.assignedFigures.data.length; i++){
                    elements.push(currentEntity.assignedFigures.data[i].id);
                }
                groups.push({
                    id: currentEntity.id,
                    elements: elements
                })
            }
        });

        // get all the connectors
        this.canvas.getLines().each( function (i, currentConnector){
            const verticesOrig = currentConnector.getVertices().data;
            const verticesNew = [];
            for (let j = 0; j<verticesOrig.length; j++){
                const newVertex = {};
                newVertex.x = parseInt(verticesOrig[j].x, 10);
                newVertex.y = parseInt(verticesOrig[j].y, 10);
                verticesNew.push(newVertex);
            }
            const connectorDetails = {
                classId: currentConnector.classId,
                cnctId: currentConnector.cnctId,
                cnctName: currentConnector.cnctName,
                cnctDesc: currentConnector.cnctDesc,
                cnctStartPort: currentConnector.sourcePort.id,
                cnctEndPort: currentConnector.targetPort.id,
                cnctVertices: verticesNew,
                z: currentConnector.getZOrder()
            }
            connectors.push(connectorDetails);
        });

        this.currentDiagram.properties.groups = groups;
        const diagramUpdates = {
            "diagramId":this.currentDiagram.id,
            "properties":this.currentDiagram.properties,
            "entities":entities,
            "connectors":connectors,
            "entityDeletes": this.currentDiagram.entityDeletes,
            "connectorDeletes": this.currentDiagram.connectorDeletes
        };

        // save the diagram to the database
        const POST_URL = this.postDiagramContentURL;
        HttpService.call(POST_URL, "POST", diagramUpdates, null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Diagram saved.",
                            classes: "alert-success",
                            templateUrl: this.notifyTemplate
                        });
                        if(this.closeAfterSave){
                            this.closeAfterSave = false;
                            this.closeDiagram();
                        }
                        this.scope.dirtyCanvas = false;
                    }.bind(this)
                }
            ]
        );
    }.bind(this);

    /**
     * Description: This function is called when the user clicks the Close command on the Diagram menu.
     *  If the canvas is dirty it prompts the user to confirm closing the diagram
     */
    this.checkClose = function() {
        if(this.scope.dirtyCanvas){
            this.confirmClose();
        }
        else{
            this.closeDiagram();
        }
    }.bind(this);

    /**
     * Description: prompts the to save changes before closing or confirm that changes are to be discarded.
     */
    this.confirmClose = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/confirm_close.html'),
            controller: 'ConfirmCloseController',
            size: 'sm',
            scope: this.scope
        });
    }.bind(this);

    /**
     * Description: closes the diagram and resets all variables
     */
    this.closeDiagram = function() {
        this.currentDiagram.id = 0;
        this.scope.currentDiagram.id = 0;
        this.currentDiagram.name = "";
        this.scope.currentDiagram.name = "";
        this.currentDiagram.properties = {};
        this.scope.currentDiagram.properties = {};
        this.currentDiagram.connectors = [];
        this.currentDiagram.entities = [];
        this.currentDiagram.entityDeletes = [];
        this.currentDiagram.connectorDeletes = [];
        this.currentDiagram.allEntities = [];
        this.scope.allEntities = [];
        this.currentDiagram.allConnectors = [];
        this.canvas.clear();
        this.scope.dirtyCanvas = false;
        this.scope.viewMode = 1;

        if (this.locationAfterClose != null && this.locationAfterClose.length > 0)
            $location.path(this.locationAfterClose);

    }.bind(this);

    this.saveMdObject = function(){
        this.closeAfterSave = true;
        this.saveDiagram();
    }.bind(this);

    this.confirmDelete = function() {
        if(this.currentDiagram.id>0){
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: this.scope
            });
        }
        else{
            notify({ message: "There is no open diagram. Nothing to delete.",
                classes: "alert-warning",
                templateUrl: this.notifyTemplate
            });
        }
    }.bind(this);

    /**
     * Description: opens the Common Open Diagram dialog.
     */
    this.openDiagram = function() {
        if (this.currentDiagram.id > 0) {
            const msg = "Please close the current diagram before opening a new one.";
            notify({
                message: msg,
                classes: "alert-warning",
                templateUrl: this.notifyTemplate
            });
        }
        else{
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_open_diagram.html'),
                controller: 'CmnOpenDiagramController',
                size: 'lg',
                scope: this.scope
            });
        }
    }.bind(this);

    /**
     * Deletes a diagram.
     */
    this.deleteDiagram = function() {
        const DELETE_URL = this.deleteDiagramURL + this.currentDiagram.id;
        HttpService.call(DELETE_URL,"DELETE",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Diagram was successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: this.notifyTemplate
                        });
                        this.scope.dirtyCanvas = false;
                        this.scope.goBack();
                    }.bind(this)
                }
            ]
        );

    }.bind(this);

    this.createDiagram = function() {
        if(this.currentDiagram.id>0){
            const msg = "Please close the current diagram before creating a new one.";
            notify({
                message: msg,
                classes: "alert-warning",
                templateUrl: this.notifyTemplate
            });
        }
        else{
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_create_diagram.html'),
                controller: 'CmnCreateDiagramController',
                size: 'md',
                scope: this.scope
            });
        }
    }.bind(this);

    this.createDiagramInDb = function() {
        const POST_URL = this.createDiagramURL;

        HttpService.call(POST_URL,"POST",this.newDiagram,null,
            [
                {
                    status:1,
                    callback: function(response){
                        this.currentDiagram.id = response.data.result_data.returnID;
                        this.currentDiagram.properties = this.newDiagram.diagramProperties;
                        this.currentDiagram.name = this.newDiagram.diagramName;
                        this.scope.currentDiagram.name = this.newDiagram.diagramName;
                        this.scope.currentDiagram.id = this.currentDiagram.id;
                        this.scope.currentDiagram.properties = this.newDiagram.diagramProperties;
                        this.getAllEntities();
                    }.bind(this)
                }
            ]
        );

    }.bind(this);

    /**
     * Updates the diagram based on the selection the user has made from available entities.
     * @function updateSelectedObjects
     */
    this.updateSelectedObjects = function(){
        this.scope.viewMode = 1;
        const updates = [];
        let updated = 0;

        for(let i = 0; i < this.currentDiagram.allEntities.length; i++) {
            const currentObject = this.currentDiagram.allEntities[i];

            if (currentObject.selected !== currentObject.selection_change) {
                if (currentObject.selected === 0 && currentObject.selection_change === 1) {
                    updates.push(currentObject);
                }
                else if (currentObject.selected === 1 && currentObject.selection_change === 0) {
                    // remove the object from the canvas
                    const objectDiagramId = currentObject.class_id + ":" + currentObject.object_id;
                    const objectFigure = this.canvas.getFigure(objectDiagramId);
                    //update connectors
                    for(let i = 0; i < this.currentDiagram.connectors.length; i++){
                        if(this.currentDiagram.connectors[i].cnctStartNtt === objectDiagramId || this.currentDiagram.connectors[i].cnctEndNtt === objectDiagramId){
                            const toBeRemovedConnector = this.canvas.getLine(this.currentDiagram.connectors[i].classId+":"+this.currentDiagram.connectors[i].cnctId);
                            this.canvas.remove(toBeRemovedConnector);
                            //The connector's on removed call back is now called.
                            i--;
                        }
                    }
                    this.canvas.remove(objectFigure);
                    //The entity's on removed call back is now called.
                }
                currentObject.selected = currentObject.selection_change;
                this.currentDiagram.allEntities[i] = currentObject;
            }
        }

        for(let i = 0; i < updates.length; i++) {
            const currentObject = updates[i];
            const x = (10 * (i+1)) % 1000;
            const y = (10*(i+1)) % 1000;
            let entity = {
                x : x,
                y : y,
                nttId : currentObject.object_id,
                classId : currentObject.class_id,
                nttName : currentObject.object_name,
                nttDesc : currentObject.object_shortdesc,
                colorDefn : '{"bgColor":"#FFFFFF"}',
                colorSource: 2
            }

            // call the DL API to get details of the entity.
            if(this.getEntityDetailURL.length > 0){
                const GET_URL = this.getEntityDetailURL + currentObject.object_id;

                HttpService.call(GET_URL, "GET", null, null,
                    [
                        {
                            status: 1,
                            callback: function(response){
                                entity = $.extend(entity, response.data.result_data.core_details);

                                this.addEntity(entity);

                                updated++;
                                if(updated === updates.length){
                                    this.autoAddConnectors();
                                }
                            }.bind(this)
                        }
                    ]
                );
            }
            else{
                this.addEntity(entity);
                updated++;
                if(updated === updates.length){
                    this.autoAddConnectors();
                }
            }
        }
    }.bind(this);

    this.setViewMode = function(viewMode){
        this.scope.viewMode = viewMode;
    }.bind(this);

    this.autoAddConnectors = function() {
        for(let j = 0; j < this.currentDiagram.allConnectors.length; j++){
            if(this.currentDiagram.allConnectors[j].inclusionStatus === false){
                let start = false;
                let end = false;
                for(let k = 0; k < this.currentDiagram.entities.length; k++){
                    if(!start && parseInt(this.currentDiagram.allConnectors[j].cnctStartNtt.split(":")[1],10) === this.currentDiagram.entities[k].nttId){
                        start = true;
                    }
                    if(!end && parseInt(this.currentDiagram.allConnectors[j].cnctEndNtt.split(":")[1],10) === this.currentDiagram.entities[k].nttId){
                        end = true;
                    }
                    if(start && end){
                        this.currentDiagram.allConnectors[j].inclusionStatus = true;
                        this.addConnector(this.currentDiagram.allConnectors[j]);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Adds and entity to the diagram canvas and sets the on_removed event handler.
     * @function addEntity
     * @param entity
     */
    this.addEntity = function(entity) {
        const ntt = this.newEntity(entity);

        ntt.on(
            "removed",
            function(){
                this.scope.dirtyCanvas = true;
                for(let i = 0; i < this.currentDiagram.entities.length; i++){
                    if(this.currentDiagram.entities[i].nttId === entity.nttId){
                        this.currentDiagram.entities.splice(i,1);
                        break;
                    }
                }
                this.currentDiagram.entityDeletes.push(entity.nttId);
            }.bind(this)
        );

        this.scope.dirtyCanvas = true;

        for(let i = 0; i < this.currentDiagram.entityDeletes.length; i++){
            if(this.currentDiagram.entityDeletes[i] === entity.nttId){
                this.currentDiagram.entityDeletes.splice(i,1);
                break;
            }
        }

        this.currentDiagram.entities.push(entity);
        this.canvas.add(ntt);
    };

    /**
     * Description: creates connectors between entities that are associated
     * @param connector
     */
    this.addConnector = function(connector) {
        const cnct = this.newConnector(connector);

        let startNtt = this.canvas.getFigure(connector.cnctStartNtt);
        let endNtt = this.canvas.getFigure(connector.cnctEndNtt);

        if (startNtt != null && endNtt != null) {
            cnct.setSource(startNtt.getHybridPort(0));
            cnct.setTarget(endNtt.getHybridPort(0));

            // add the vertices
            if (connector.cnctVertices && connector.cnctVertices.length>2) {
                const vertices = new draw2d.util.ArrayList();
                for (let j = 0; j < connector.cnctVertices.length; j++) {
                    const currentVertex = connector.cnctVertices[j];
                    const newPoint = new draw2d.geo.Point(parseInt(currentVertex.x, 10), parseInt(currentVertex.y, 10))
                    if(newPoint.x >= 0 && newPoint.y >= 0){
                        vertices.add(newPoint);
                    }
                }
                if(vertices.data.length > 2){
                    cnct.setVertices(vertices);
                }
            }

            cnct.on(
                "removed",
                function(){
                    this.scope.dirtyCanvas = true;
                    for(let i = 0; i < this.currentDiagram.connectors.length; i++){
                        if(this.currentDiagram.connectors[i].cnctId === connector.cnctId){
                            this.currentDiagram.connectors.splice(i,1);
                            break;
                        }
                    }
                    if(this.currentDiagram.properties.allEntities){
                        for(let i = 0; i < this.currentDiagram.allConnectors.length; i++){
                            if(this.currentDiagram.allConnectors[i].cnctId === connector.cnctId){
                                this.currentDiagram.allConnectors[i].inclusionStatus = false;
                                break;
                            }
                        }
                    }
                    this.currentDiagram.connectorDeletes.push(connector.cnctId);
                }.bind(this)
            );

            this.scope.dirtyCanvas = true;
            for(let i = 0; i < this.currentDiagram.connectorDeletes.length; i++){
                if(this.currentDiagram.connectorDeletes[i] === connector.cnctId){
                    this.currentDiagram.connectorDeletes.splice(i,1);
                    break;
                }
            }
            this.currentDiagram.connectors.push(connector);

            this.canvas.add(cnct);
        }

    };

    this.zoomIn = function(){
      this.canvas.setZoom(this.canvas.zoomFactor * this.zoomRate)
    };
    this.zoomOut = function(){
      this.canvas.setZoom(this.canvas.zoomFactor / this.zoomRate)
    };
    this.zoomReset = function(){
      this.canvas.setZoom(1)
    };

    /////////////////////////
    // Getters and Setters //
    /////////////////////////
    this.setCanvas = function(canvas) {
        this.canvas = canvas;
    };

    this.setScope = function(scope) {
        this.scope = scope;
    };

    this.setNewEntity = function(newEntity) {
        this.newEntity = newEntity;
    };

    this.setNewConnector = function(newConnector) {
        this.newConnector = newConnector;
    };

    this.setGetDiagramContentURL = function(getDiagramContentURL) {
        this.getDiagramContentURL = getDiagramContentURL;
    };

    this.setGetAllEntitiesURL = function(getAllEntitiesURL) {
        this.getAllEntitiesURL = getAllEntitiesURL;
    };

    this.setPostDiagramContentURL = function(postDiagramContentURL) {
        this.postDiagramContentURL = postDiagramContentURL;
    };

    this.setCreateDiagramURL = function(createDiagramURL) {
        this.createDiagramURL = createDiagramURL;
    };

    this.setDeleteDiagramURL = function(deleteDiagramURL) {
        this.deleteDiagramURL = deleteDiagramURL;
    };

    this.setGetEntityDetailURL = function(getEntityDetailURL) {
        this.getEntityDetailURL = getEntityDetailURL;
    };

    this.setNewDiagram = function(newDiagram) {
        this.newDiagram = newDiagram;
    };

    this.setCustomConfig = function(customConfig) {
        this.customConfig = customConfig;
    }

    this.setLocationAfterClose = function(locationAfterClose) {
        this.locationAfterClose = locationAfterClose;
    }

    this.setSelectedDiagramId = function(selectedDiagramId) {
        this.selectedDiagramId = selectedDiagramId;
    }.bind(this);

}

/* * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service  *
 * * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('DiagramService', DiagramService);
