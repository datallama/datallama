/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for making standard HTTP calls.
 *
 */
function HttpService($http, $location, LogoutService, notify) {

    this.defaultHeaders = {};
    this.configured = false;
    this.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: makes and HTTP REST call based on the configuration passed in parameters
     * @param url
     * @param method
     * @param postData
     * @param additionalHeaders
     * @param successCallbacks
     */
    this.call = function(url, method, postData, additionalHeaders, successCallbacks) {

        if(!this.configured){
            this.eatCookies();
        }

        if(postData === null || method.toUpperCase() !== "POST") postData={};
        let headers = additionalHeaders;
        if(headers === null) headers = {};

        $.extend(headers,this.defaultHeaders);

        $http({
            method: method,
            url: url,
            headers: headers,
            data: postData
        }).then(
            function successCallback(response) {
                const resultStatus = response.data.result_status;
                let handled = false;

                for(let i = 0; i < successCallbacks.length; i++){
                    if(successCallbacks[i].status === resultStatus){
                        successCallbacks[i].callback(response);
                        handled = true
                        break
                    }
                }
                if(!handled) {
                    if (resultStatus === -10) {
                        this.setUserId(null);
                        LogoutService.logout("TIMEOUT");
                    }
                    else {
                        let msg = "No data was returned from the database. ";
                        msg += "The API call URL was " + url + ". ";
                        msg += "The API result status was " + resultStatus + ".";
                        notify({
                            message: msg,
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: this.notifyTemplate
                        });
                    }
                }
            }.bind(this),
            function errorCallback(response) {
                if(response.status === 401) {
                    this.setUserId(null);
                    LogoutService.logout("TIMEOUT");
                }
                else if (response.status === 403) {
                    let msg = "You do not have the appropriate privilege level to complete this action.";
                    notify({
                        message: msg,
                        classes: "alert-warning",
                        duration: 5000,
                        templateUrl: this.notifyTemplate
                    });
                }
                else {
                    let errorMessage = "";
                    try{
                        errorMessage = "The API call " + response.config.method + " " + response.config.url + " failed with error '" + response.data.error + "'.";
                        errorMessage += " The error message was: " + response.data.message;
                    }catch{
                        errorMessage = "The API call failed with no response"
                    }
                    notify({
                        message: errorMessage,
                        classes: "alert-danger",
                        duration: 30000,
                        templateUrl: this.notifyTemplate
                    });
                }
            }.bind(this)
        );
    }

    /**
     * Description: sets the userId of the logged in user in the HTTP header dlUserId
     * @param userId
     */
    this.setUserId = function (userId) {
        this.defaultHeaders = {
            'dlUserId': userId
        }
        this.configured = userId != null;
    }

    this.eatCookies = function() {
        const cookies = document.cookie.split(";");
        for(let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i];
            const keyVal = cookie.split("=");
            if(keyVal[0] === "user_id") {
                this.setUserId(keyVal[1]);
                break;
            }
        }
    }
}
/* * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service  *
 * * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('HttpService', HttpService);