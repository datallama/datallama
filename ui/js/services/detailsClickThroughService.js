/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for managing click through to the detail state.
 *              When clicking through to the DETAIL state you use the class
 *              table to determine the state name but sometimes multiple
 *              class types have the same state (for example taxonomy definitions
 *              and taxonomy elements) therefore you have to build different
 *              invocation params depending on if it is a main class or a
 *              sub class.
 *
 */
function DetailsClickThroughService(notify, MoClassService, $state, HttpService) {

    this.clickThrough = function(objectId, classId, parentObjectId, parentClassId, stateParams){
        window.scrollTo(0,0);
        const targetMoClass = MoClassService.getMoClassByClassId(classId);
        const stateName = targetMoClass.objectDetailState;
        if(stateName && stateName !== "none") {

            let GET_URL = "";

            switch(classId) {
                //Data landscape objects
                case MoClassService.getMoClassIdByCode("INTERNAL_DATA_STORE"):
                case MoClassService.getMoClassIdByCode("DATA_FLOW"):
                case MoClassService.getMoClassIdByCode("EXTERNAL_DATA_STORE"):
                case MoClassService.getMoClassIdByCode("INBOUND_DATA_FEED"):
                case MoClassService.getMoClassIdByCode("OUTBOUND_DATA_FEED"):
                case MoClassService.getMoClassIdByCode("ETL_PLATFORM"):
                case MoClassService.getMoClassIdByCode("DATA_STREAM_PROCESSOR"):
                case MoClassService.getMoClassIdByCode("DATA_VISUALISATION_APPLICATION"):
                case MoClassService.getMoClassIdByCode("DATA_REPORTING_APPLICATION"):
                    $state.go(stateName,
                        {
                            invocationParams: {
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{}
                            }
                        });
                    break;

                //Diagrams
                case MoClassService.getMoClassIdByCode("DSDF_DIAGRAM"):     //3: DSDF Diagram
                case MoClassService.getMoClassIdByCode("PDM_DIAGRAM"):      //4: PDM Diagram
                case MoClassService.getMoClassIdByCode("ONTOLOGY_DIAGRAM"): //42: PDM Diagram
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("DB_TABLE"):
                case MoClassService.getMoClassIdByCode("DB_VIEW"):
                case MoClassService.getMoClassIdByCode("DB_TABLE_COLUMN"):
                case MoClassService.getMoClassIdByCode("DB_VIEW_COLUMN"):
                case MoClassService.getMoClassIdByCode("DB_SCHEMA"):
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("TAXONOMY_DEFINITION")://9: taxonomy definitions
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams: {
                                    elementId:0,
                                    tedActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                                }
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("TAXONOMY_ELEMENT"): //10: taxonomy element
                    GET_URL = apiHost + "/taxonomy/element/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.taxonomyElementSCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("JSON_MESSAGE_FORMAT"): //11: JSON Message Format
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{
                                    jsonNodeId:0,
                                    activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                                }
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("JSON_MESSAGE_NODE"): //12: JSON node
                    GET_URL = apiHost + "/format/json/content/node/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.jsonNodeSCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("RELATIONAL_DATABASE"): //13: Relational data base
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{}
                            }
                        });
                    break;

                // Ontology Lists
                case MoClassService.getMoClassIdByCode("LIST_CDG"): //14: List (CDG)
                case MoClassService.getMoClassIdByCode("CATALOGUE"): //29: Catalogue
                case MoClassService.getMoClassIdByCode("GLOSSARY"): //30: Glossary
                case MoClassService.getMoClassIdByCode("THESAURUS"): //31: Thesaurus
                case MoClassService.getMoClassIdByCode("DICTIONARY"): //32: Dictionary
                case MoClassService.getMoClassIdByCode("ATTRIBUTE_DOMAIN"): //33: Attribute Domain
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{elementId:0, ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("LIST_CDG_ELEMENT"): //15: List (CDG) Element
                    GET_URL = apiHost + "/ontology/listcdg/element/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.listCdgElementSCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("GHMF"): //16: GHMF
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{elementId:0, ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("GHMF_ELEMENT"): //17: GHMF element
                    GET_URL = apiHost + "/format/ghmf/content/element/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.listGhmfElementSCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("XML_MESSAGE_FORMAT"): //36: XML message format
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{elementId:0, ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("XML_ELEMENT"): //37: XML element
                    GET_URL = apiHost + "/format/xml/content/element/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.xmlElementSCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("REST_API"): //39: API definition
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("REST_API_PATH_SEGMENT"): //40: API path segment
                case MoClassService.getMoClassIdByCode("REST_API_METHOD"):       //41: API path segment
                case MoClassService.getMoClassIdByCode("REST_API_RESPONSE"):     //45: API path segment
                    GET_URL = apiHost + "/data-models/apidefinition/rest/element/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.apiPathSCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("FILE_STORE"): // 34: file store
                case MoClassService.getMoClassIdByCode("OBJECT_STORE_SPACE"): // 75: file store
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{elementId:0, ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1} }
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("DIRECTORY"): // 35: directory
                    GET_URL = apiHost + "/filestore/directory/" + objectId;
                    HttpService.call(GET_URL,"GET",null,null,
                        [
                            {
                                status:1,
                                callback: function(response){
                                    this.directorySCB(response, objectId, stateName);
                                }.bind(this)
                            }
                        ]
                    );
                    break;

                case MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION"): //46: association
                    $state.go(stateName,
                        {
                            invocationParams:{
                                stateName:stateName,
                                objectId:objectId,
                                classId:classId,
                                stateParams:{}
                            }
                        });
                    break;

                case MoClassService.getMoClassIdByCode("DELIMITED_FLATFILE"):
                    break;
                default:
                    this.showNoDetailStateWarning(targetMoClass);
            }
        }
        else {  // The targetMoClass classCode does not exist in the switch statement.
            this.showNoDetailStateWarning(targetMoClass);
        }
    }

    /**
     * Description: an alert message is displayed if a click through action for the specified class has not been defined.
     * @param targetMoClass
     */
    this.showNoDetailStateWarning = function(targetMoClass){
        let message = "";
        if(targetMoClass && targetMoClass.className && targetMoClass.className.length > 0) {
            message = "Currently there is no details view for meta data objects of type " + targetMoClass.className;
        }
        else{
            message = "Currently there is no details view for the selected object"
        }
        notify({
            message: message,
            classes: "alert-warning",
            duration: 2000,
            templateUrl: 'views/common/notify.html'
        });
    }

    /**
     * Description: executes the state transition for taxonomy or taxonomy element after retrieving parentObjectId info
     * @param response
     * @param objectId
     * @param stateName
     */
    this.taxonomyElementSCB = function(response, objectId, stateName) {
        const mainClassId = MoClassService.getMoClassIdByCode("TAXONOMY_DEFINITION");
        const childClassId = MoClassService.getMoClassIdByCode("TAXONOMY_ELEMENT");
        $state.go(stateName,
            {
                invocationParams: {
                    stateName:stateName,
                    objectId:objectId,
                    classId:childClassId,
                    parentObjectId: response.data.result_data.attributes.taxonomyId,
                    parentClassId: mainClassId,
                    stateParams:{
                        elementId:objectId,
                        tedActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

    /**
     * Description: executes the state transition for JSON format or JSON element after retrieving parentObjectId info
     * @param response
     * @param objectId
     * @param stateName
     */
    this.jsonNodeSCB = function(response, objectId, stateName){
        const mainClassId = MoClassService.getMoClassIdByCode("JSON_MESSAGE_FORMAT");
        const childClassId = MoClassService.getMoClassIdByCode("JSON_NODE");
        $state.go(stateName,
            {
                invocationParams:{
                    stateName: stateName,
                    objectId: objectId,
                    classId: childClassId,
                    parentObjectId: response.data.result_data.attributes.formatId,
                    parentClassId: mainClassId,
                    stateParams: {
                        jsonNodeId: objectId,
                        activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

    /**
     * Description: executes the state transition for Ontology List or List element after retrieving parentObjectId info
     * @param response
     * @param objectId
     * @param stateName
     */
    this.listCdgElementSCB = function(response, objectId, stateName){
        const mainClassId = MoClassService.getMoClassIdByCode("LIST_CDG");
        const childClassId = MoClassService.getMoClassIdByCode("LIST_CDG_ELEMENT");
        $state.go(stateName,
            {
                invocationParams: {
                    stateName: stateName,
                    objectId: objectId,
                    classId: childClassId,
                    parentObjectId: response.data.result_data.attributes.listCdgId,
                    parentClassId: mainClassId,
                    stateParams: {
                        elementId: objectId,
                        ledActiveTab: {attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

    /**
     * Description: executes the state transition for GHMF element after retrieving parentObjectId info
     * @param response
     * @param objectId
     * @param stateName
     */
    this.listGhmfElementSCB = function(response, objectId, stateName){
        const mainClassId = MoClassService.getMoClassIdByCode("GHMF");
        const childClassId = MoClassService.getMoClassIdByCode("GHMF_ELEMENT");
        $state.go(stateName,
            {
                invocationParams: {
                    stateName: stateName,
                    objectId: objectId,
                    classId: childClassId,
                    parentObjectId: response.data.result_data.attributes.formatId,
                    parentClassId: mainClassId,
                    stateParams: {
                        elementId: objectId,
                        ledActiveTab: {attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

    this.xmlElementSCB = function(response, objectId, stateName){
        const mainClassId = MoClassService.getMoClassIdByCode("XML_MESSAGE_FORMAT");
        const childClassId = MoClassService.getMoClassIdByCode("XML_ELEMENT");
        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:childClassId,
                    parentObjectId: response.data.result_data.attributes.parentObjectId,
                    parentClassId: mainClassId,
                    stateParams:{
                        elementId:objectId,
                        ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

    this.apiPathSCB = function(response, objectId, stateName){
        const mainClassId = MoClassService.getMoClassIdByCode("REST_API");
        const childClassId = MoClassService.getMoClassIdByCode("REST_API_PATH_SEGMENT");
        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:childClassId,
                    parentObjectId: response.data.result_data.attributes.parentObjectId,
                    parentClassId: mainClassId,
                    stateParams:{
                        elementId:objectId,
                        ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

    /**
     * Description: executes the state transition for a directory after retrieving parentObjectId info
     * @param response
     * @param objectId
     * @param stateName
     */
    this.directorySCB = function(response, objectId, stateName){
        const mainClassId = MoClassService.getMoClassIdByCode("FILE_STORE");
        const childClassId = MoClassService.getMoClassIdByCode("DIRECTORY");
        $state.go(stateName,
            {
                invocationParams: {
                    stateName: stateName,
                    objectId: objectId,
                    classId: childClassId,
                    parentObjectId: response.data.result_data.attributes.filestoreId,
                    parentClassId: mainClassId,
                    stateParams: {
                        elementId:objectId,
                        ledActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}
                    }
                }
            }
        );
    }

}

/* * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service  *
 * * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('DetailsClickThroughService', DetailsClickThroughService);
