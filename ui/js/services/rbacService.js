/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for managing the RBAC configuration for the logged in user.
 *
 */
function RbacService() {

    this.groupPermissions = {};
    this.userId = 0;
    this.configuredGP = false;
    this.configuredUID = false;

    this.setGroupPermissions = function(groupPermissions) {
        this.groupPermissions = groupPermissions;
        this.configuredGP = groupPermissions!=null;
    }

    this.setUserId = function(userId) {
        this.userId = userId;
        this.configuredUID = userId!=null;
    }

    this.getUserId = function(){
        return this.userId;
    }

    this.eatCookies = function(){
        const cookies = document.cookie.replace(/\s*/g, "").split(";");
        for(let i = 0; i<cookies.length; i++){
            const cookie = cookies[i];
            const keyVal = cookie.split("=");
            if(!this.configuredUID && keyVal[0] === "user_id"){
                this.setUserId(keyVal[1]);
            }
            else if(!this.configuredGP && keyVal[0] === "group_perms"){
                this.setGroupPermissions(JSON.parse(keyVal[1]));
            }
        }
    }

    this.checkRbac = function(module, action_category, mdObjectOwner) {
        if(!this.configuredGP || !this.configuredUID){
            this.eatCookies();
        }
        if(this.configuredGP && this.configuredUID){
            const rbacPath = "this.groupPermissions." + module + "_" + action_category;
            const accessGranted = eval(rbacPath);
            return !(accessGranted);    // the inverse of accessGranted must be returned because AngularJS only provides ng-disabled
        }
    }

}

/** * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service *
 ** * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('RbacService', RbacService);
