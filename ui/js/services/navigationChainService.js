/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for managing the goBack feature via a navigation chain.
 */
function NavigationChainService($state) {

    this.navigationChain = [];

    this.storeCurrentState = function(currentStateSpec) {
        this.navigationChain.push(currentStateSpec);
    };

    this.retrievePreviousState = function() {
        var previousState = this.navigationChain[this.navigationChain.length - 1];
        return previousState;
    }

    this.goBack = function() {
        var previousState = this.navigationChain[this.navigationChain.length - 1];
        this.navigationChain.pop();
        $state.go(previousState.stateName, {invocationParams:previousState});
    }
}

/** * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service *
 ** * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('NavigationChainService', NavigationChainService);
