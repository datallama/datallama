/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for logging users out.
 *
 */
function LogoutService(RbacService, $http, $location, notify) {

    /**
     * Description: ends the current user session and navigates to the login view
     */
    this.logout = function(logoutInitiation) {
        // Get the logout configuration that is defined in javascript file dataLlamaConfig.js
        let logoutURL = "";
        for (let i = 0; i < authConfig.length; i++) {
            if (authConfig[i].authMethod === authMethod) {
                logoutURL = authConfig[i].logoutURL;
            }
        }

        RbacService.setUserId(null);
        RbacService.setGroupPermissions(null);

        let logoutMessage = "You have been logged out of Data Llama.";
        let notifyLevel = "alert-success";

        if (logoutInitiation == "TIMEOUT") {
            logoutMessage = "Your session has timed out. Please login again.";
            notifyLevel = "alert-warning";
        }
        if(authMethod === "dlint") {
            $http.post(logoutURL).then(
                function successCallback() {
                    notify({
                        message: logoutMessage,
                        classes: notifyLevel,
                        duration: 2000,
                        templateUrl: 'views/common/notify.html'
                    });
                    $location.path("logins");
                },
                function errorCallback() {
                    notify({
                        message: "There was a error while trying to log you out. The cause of the error is unknown.",
                        classes: "alert-danger",
                        duration: 2000,
                        templateUrl: 'views/common/notify.html'
                    });
                }
            );
        }
        else {
            window.location.href = logoutURL;
        }
    }

}

/** * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service *
 ** * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('LogoutService', LogoutService);
