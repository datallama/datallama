/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The DataSensitivityService stores the mapping between data sensitivity levels from the DL DB and CSS Classes.
 *
 */
function DataSensitivityService() {
    this.dsListSpecific = {
        "none":"dl-ds-none", "low":"dl-ds-low-1", "medium":"dl-ds-medium-1", "high":"dl-ds-high-1", "critical":"dl-ds-critical-1"
    };
    this.dsListAggregate = {
        "none":"dl-ds-none", "low":"dl-ds-low-2", "medium":"dl-ds-medium-2", "high":"dl-ds-high-2", "critical":"dl-ds-critical-2"
    };

    /**
     * Returns the CSS Class for the specified data sensitivity level
     * @param dataSensitivityLevel
     * @returns dataSensitivityLevelCssClass
     */
    this.getCssClassForLevel = function(dataSensitivityLevel) {
        return this.dsListSpecific[dataSensitivityLevel];
    }

    /**
     * Returns the CSS Class for the specified data sensitivity level
     * @param dataSensitivityLevel
     * @returns dataSensitivityLevelCssClass
     */
    this.getAggregateCssClassForLevel = function(dataSensitivityLevel) {
        return this.dsListAggregate[dataSensitivityLevel];
    }

    /**
     * Returns the CSS Class for the specified data sensitivity level
     * @param dataSensitivityLevel
     * @returns dataSensitivityLevelCssClass
     */
    this.getCssClassLvlCmode = function(dataSensitivityLevel, createMode) {
        let cssClass = "";
        if (createMode == 10 ) {    // specific DS assignment
            cssClass = this.dsListSpecific[dataSensitivityLevel];
        }
        else if(createMode == 20 ) {    // aggregate DS assignment
            cssClass = this.dsListAggregate[dataSensitivityLevel];
        }
        else
            cssClass = this.dsListSpecific["none"];

        return cssClass;
    }

    /**
     * Returns the full list of data sensitivity levels and corresponding CSS Classes.
     * @returns {*|{high: string, critical: string, low: string, none: string, medium: string}}
     */
    this.getDataSensitivityList = function() {
        return this.dsListSpecific;
    }

}

/** * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service *
 ** * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('DataSensitivityService', DataSensitivityService);
