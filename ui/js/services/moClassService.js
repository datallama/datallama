/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The MoClassService stores details of the metadata object classes defined in Data Llama.
 *  The structure of the MoClass objects stored in the moClassList matches the DlMoClass Java class, as follows:
 *  { classId: 0, className: "", classCode: "", classShortDesc: "",
 *    customFormId: 0, customFormName: "", objectTable: "", objectDetailState: "" }
 *
 */
function MoClassService() {

    this.moClassList = [];

    /**
     * Adds a metadata object class definition to the moClassList array
     * @param moClass
     */
    this.addMoClass = function(moClass) {
        this.moClassList.push(moClass);
    };

    /**
     * Returns the classId corresponding to the Class Code provided in argument moClassCode
     * @param moClassCode
     * @returns {number}
     */
    this.getMoClassIdByCode = function(moClassCode) {
        let classId = 0;
        for (let i = 0; i < this.moClassList.length; i++) {
            if (this.moClassList[i].classCode == moClassCode) {
                classId = this.moClassList[i].classId;
                break;
            }
        }
        return classId;
    }

    /**
     * Returns the classCode corresponding to the classId provided in argument moClassCode
     * @param moClassCode
     * @returns {number}
     */
    this.getMoClassCodeById = function(moClassId) {
        let classCode = "";
        for (let i = 0; i < this.moClassList.length; i++) {
            if (this.moClassList[i].classId == moClassId) {
                classCode = this.moClassList[i].classCode;
                break;
            }
        }
        return classCode;
    }

    /**
     * Returns a metadata class object for the class identified by classId
     * @param classId
     * @returns {{}}
     */
    this.getMoClassByClassId = function(classId) {
        let moClass = {};
        for (let i = 0; i < this.moClassList.length; i++) {
            if (this.moClassList[i].classId == classId) {
                moClass = this.moClassList[i];
                break;
            }
        }
        return moClass;
    }

    /**
     * Returns a metadata class object for the class identified by classId
     * @param classId
     * @returns {{}}
     */
    this.getMoClassByCode = function(classCode) {
        let moClass = {};
        for (let i = 0; i < this.moClassList.length; i++) {
            if (this.moClassList[i].classCode == classCode) {
                moClass = this.moClassList[i];
                break;
            }
        }
        return moClass;
    }

}

/** * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service *
 ** * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('MoClassService', MoClassService);
