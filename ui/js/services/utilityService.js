/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: provides utility functions commonly used throughout AJS controllers
 */
function UtilityService() {

    this.compressHierarchNumber = function(hierarchyNumber) {
        let hna = hierarchyNumber.split('.');
        let hn = "";
        for (let i = 0; i < hna.length; i++) {
            hn = hn + parseInt(hna[i]) + ".";
        }
        return hn.substr(0, hn.length - 1);

    }

    this.getReleaseTemplateUrl = function(templateUrl) {
        const releaseNumber = releaseConfig.releaseNumber.replaceAll('.', '');
        const releaseTemplateUrl = templateUrl.substr(0, templateUrl.length - 5) + releaseNumber + ".html";
        return releaseTemplateUrl;
    }
}

/** * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as a Service *
 ** * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .service('UtilityService', UtilityService);
