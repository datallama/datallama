"use strict";

d2.directive("draw2dCanvas", ["$window","$parse", "$timeout",
function($window, $parse, $timeout){

	return {
		restrict: 'E,A',
		link: function(scope, element, attrs, controller) {

			// provide the scope properties and override the defaults with the user settings
			scope.editor = $.extend(true, {
				canvas: {
					onDrop: function(droppedDomNode, x, y, shiftKey, ctrlKey){}
				},
				palette:{
					figures: []
				},
				state:{
					dirty  : false,
					canUndo: false,
					canRedo: false
				},
				selection:{
					className:null,
					figure:null,
					attr:null
				}
			},
			scope.editor);

			// init the Draw2D Canvas with the given user settings and overriden hooks
			scope.canvas = new draw2d.Canvas(element.attr("id"), scope.editor.canvas.width, scope.editor.canvas.height);
			scope.canvas.setScrollArea("#" + element.attr("id"));
			scope.canvas.onDrop = $.proxy(scope.editor.canvas.onDrop, scope.canvas);
			scope.canvas.paper.canvas.style.background = "#FFFFFF";

			// added by RJS
			scope.canvas.on("dblclick", function(x, dblClickDetails){
				if (scope.canvasDblClick != null) {
					scope.canvasDblClick(dblClickDetails);
                }
				else {
					console.log("Warning: No canvasDblClick function defined in view controller");
				}
			});
			// added by JJS. This is to make sure the canvas has been created before the diagram service.
			scope.initDiagram();


			// update the scope model with the current state of the CommandStack
			var stack = scope.canvas.getCommandStack();
			stack.addEventListener(function(event){
				$timeout(function(){
					scope.editor.state.canUndo= stack.canUndo();
					scope.editor.state.canRedo= stack.canRedo();
				},0);
			});

			// Update the selection in the model and Databinding Draw2D -> Angular
			var changeCallback = function(emitter, attribute){
				$timeout(function(){
					if(scope.editor.selection.attr !== null){
						scope.editor.selection.attr[attribute] = emitter.attr(attribute);
					}
				},0);
			};

			scope.canvas.on("select", function(canvas, figure){
				$timeout(function(){
					if(figure !== null){
						scope.editor.selection.className = figure.NAME;
						scope.editor.selection.attr = figure.userData;
					}
					else {
						scope.editor.selection.className = null;
						scope.editor.selection.attr = null;
					}

					// unregister and register the attr listener to the new figure
					// TODO: fix the off & on functions
					//if(scope.editor.selection.figure !== null) {scope.editor.selection.figure.off("change", changeCallback);}

					scope.editor.selection.figure = figure;

					//if(scope.editor.selection.figure !== null){scope.editor.selection.figure.on("change", changeCallback);}
				},0);
			});

			// configure the newConnectionCreator edit policy to use scope function newConnectionCreator() in the AJS controller
			if (scope.newConnectionCreator != null) {
				scope.canvas.installEditPolicy( new draw2d.policy.connection.DragConnectionCreatePolicy({
					createConnection: scope.newConnectionCreator
				}));
            }
			else {
				console.log("Warning. $scope.newConnectionCreator is undefined. Using default connection creator");
			}


			// Databinding: Angular UI -> Draw2D
			// it is neccessary to call the related setter of the draw2d object. "Normal" Angular
			// Databinding didn't work for draw2d yet
			scope.$watchCollection("editor.selection.attr", function(newValues, oldValues){

				if(oldValues !== null && scope.editor.selection.figure!=null){
					// for performance reason we post only changed attributes to the draw2d figure
					var changes = draw2d.util.JSON.diff(newValues, oldValues);
					scope.editor.selection.figure.attr(changes);
				}

			});

			// push the canvas function to the scope for ng-action access
			scope.editor.undo = $.proxy(stack.undo,stack);
			scope.editor.redo = $.proxy(stack.redo,stack);
			scope.editor["delete"] = $.proxy(function(){
				var node = this.getCurrentSelection();
				var command= new draw2d.command.CommandDelete(node);
				this.getCommandStack().execute(command);
				},scope.canvas);
			scope.editor.load = $.proxy(function(json){
				scope.canvas.clear();
				var reader = new draw2d.io.json.Reader();
				reader.unmarshal(scope.canvas, json);
			},scope.canvas);


			/*
			 * Description: this function will reload a previously opened diagram after the user returns to the canvas from another view
			 */
			var watch = scope.$watch(
				function() {
					return element.children().length;
				},
 				function() {
					// Wait for templates to render
					scope.$evalAsync(function() {
						// Finally, directives are evaluated and templates are rendered here
						var children = element.children();
						if (scope.reloadDiagram != null) {
							scope.reloadDiagram();
						}
					});
            });

		}
	};

}]);

