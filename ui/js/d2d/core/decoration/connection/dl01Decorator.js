
/*****************************************
 *   Library is under GPL License (GPL)
 *   Copyright (c) 2012 Andreas Herz
 ****************************************/
/**
 * @class draw2d.decoration.connection.CircleDecorator
 *
 * See the example:
 *
 *     @example preview small frame
 *
 *     // create and add two nodes which contains Ports (In and OUT)
 *     //
 *     var start = new draw2d.shape.node.Start();
 *     var end   = new draw2d.shape.node.End();

 *     // ...add it to the canvas
 *     canvas.add( start, 50,50);
 *     canvas.add( end, 230,80);
 *
 *     // Create a Connection and connect the Start and End node
 *     //
 *     var c = new draw2d.Connection();
 *
 *     // toggle from ManhattenRouter to DirectRouter to show the rotation of decorations
 *     c.setRouter(new draw2d.layout.connection.DirectRouter());
 *
 *     // Set the endpoint decorations for the connection
 *     //
 *     c.setSourceDecorator(new draw2d.decoration.connection.CircleDecorator());
 *     c.setTargetDecorator(new draw2d.decoration.connection.CircleDecorator());
 *     // Connect the endpoints with the start and end port
 *     //
 *     c.setSource(start.getOutputPort(0));
 *     c.setTarget(end.getInputPort(0));
 *
 *     // and finally add the connection to the canvas
 *     canvas.add(c);
 *
 * @inheritable
 * @author Andreas Herz
 * @extend draw2d.decoration.connection.Decorator
 */
draw2d.decoration.connection.dl01Decorator = draw2d.decoration.connection.Decorator.extend({

	NAME : "draw2d.decoration.connection.dl01Decorator",

	/**
	 * @constructor
	 *
	 * @param {Number} [width] the width of the arrow
	 * @param {Number} [height] the height of the arrow
	 */
    init: function(width, height)
    {
        this._super( width, height);
    },

	/**
	 * Draw a filled circle decoration.
     *
	 * @param {Raphael} paper the raphael paper object for the paint operation
	 **/
	paint: function(paper)
	{
        var st = paper.set();

        //console.log("inside dl01Decorator");
        //console.log(this);

		//st.push(paper.circle(20, 0, this.width/2));
        //st.push(paper.rect(10, -10, 20, 20));
        //st.push(paper.rect(10, -5, 10, 10));

        var p = ["M", this.width + 5, " ", -(this.height/2),		// Go to the top center..
	                    //"L", this.width  , " ", 0,			// ...draw line to the right middle
	                    "L", this.width + 5, " ", this.height/2,	// ...bottom center...
	                    "L", 10, " ", 0,						// ...left middle...
	                    //"L", this.width/2, " ", -this.height/2,  // and close the path
	                    "Z"].join("");
		st.push(
	        paper.path(p)
		);

        //console.log("rendering dl01Decorator");
        //console.log(p);
        st.attr( {fill:"#999999", stroke:this.color.hash()} );


		return st;
	}
});


