
/*****************************************
 *   Library is under GPL License (GPL)
 *   Copyright (c) 2012 Andreas Herz
 ****************************************/
/**
 * @class draw2d.decoration.connection.DlIdef1xTargetDecorator
 *
 * @inheritable
 * @author Richard Scrivener
 * @extend draw2d.decoration.connection.Decorator
 */
draw2d.decoration.connection.DlIdef1xTargetDecorator = draw2d.decoration.connection.Decorator.extend({

	NAME : "draw2d.decoration.connection.DlIdef1xTargetDecorator",

	/**
	 * @constructor 
	 * 
	 * @param {Number} [width] the width of the arrow
	 * @param {Number} [height] the height of the arrow
	 */
    init: function(width, height)
    {   
        this._super( width, height);
    },

	/**
	 * Draw a filled circle decoration.
     *
	 * @param {Raphael} paper the raphael paper object for the paint operation 
	 **/
	paint: function(paper)
	{
        var st = paper.set();
        
		st.push(paper.circle(this.width/2, 0, this.width/2));
        st.attr({fill:this.backgroundColor.hash(), stroke:this.color.hash()});
		
		return st;
	}
});






