DlCrowManyDecorator = draw2d.decoration.connection.Decorator.extend({

    NAME : "DlCrowManyDecorator",

    /**
     * @constructor
     *
     * @param {Number} [width] the width of the arrow
     * @param {Number} [height] the height of the arrow
     */
    init: function(width, height)
    {
        this._super( width, height);
    },

    /**
     * @param {Raphael} paper the raphael paper object for the paint operation
     **/
    paint: function(paper)
    {
        const w = this.width;
        const h = this.height;
        const set = paper.set();
        const path = "M 0,0 L "+w+",0 M "+w/4+",0 L "+w+","+h/2+" M "+w/4+",0 L "+w+","+-h/2+" M "+w/4+","+h/2+" L "+w/4+","+-h/2;
        set.push(paper.path(path));
        return set;
    }
});