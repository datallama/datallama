/**
 * Data Llama 
 * The DlSmdColumn class models a Structural Metadata (SMD) column for Physical Data Model diagrams.
 * An SMD Column is a representation of a column in a database table of view.
 * The structure of the columnDefinition parameter object is as follows:
 * {
 *   column_id: integer,
 *   text: string,
 *   fontColor: string,
 *   resizeable: boolean,
 *   stroke:0,
 *   padding:{left:10, top:-2, bottom:0}
 * }
 */
var DlSmdColumn = draw2d.shape.basic.Label.extend({

	NAME: "DlSmdColumn",

    init : function(columnDefinition, showColumnDetailsCallback)
    {
    	this.columnId = columnDefinition.columnId;
        this.classId = 8;	// TODO: this value should be returned from the DB stored procedure
        this.columnName = columnDefinition.text;
        this.columnDatatype - columnDefinition.columnDatatype;
		this.isCollapsible = true;

		this._super($.extend(columnDefinition));

		/*
		var columnDetails = function(){
			console.log("Showing details for column " + this.columnName);
		}.bind(this);
		*/

		//this.on("click",columnDetails);

        /**
         * Definition: call the showEntityDetailsCallback function that was passed to this.init
         */
        var columnClick = function(){
            console.log("click message from inside column " + this.columnName);
            showColumnDetailsCallback(this.columnId);
        }.bind(this);

        this.on("click",columnClick);

    }

});
