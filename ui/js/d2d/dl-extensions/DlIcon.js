DlIcon = draw2d.shape.icon.Icon.extend({
    NAME : "DlIcon",

    init: function(attr, setter, getter ) {
        this._super(
            $.extend({},attr),
            $.extend({
                iconPath: this.setIconPath
            },setter),
            $.extend({
                iconPath: this.getIconPath
            },getter)
        );
    },

    createSet: function() {
        return this.canvas.paper.path(this.iconPath);
    },

    setIconPath: function(iconPath) {
        this.iconPath = iconPath;
    },

    getIconPath: function () {
        return(this.iconPath);
    }
});