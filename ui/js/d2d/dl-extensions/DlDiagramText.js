DlDiagramText = draw2d.shape.basic.Text.extend({
    NAME : "DlDiagramText",

    init: function(attr, setter, getter ) {
        this._super(
            $.extend({},attr),
            $.extend({
                nttId: this.setNttId,
                classId: this.setClassId,
                colorDefn: this.setColorDefn,
                colorSource: this.setColorSource,
                hAlign: this.setHAlign,
                vAlign: this.setVAlign,
                z: this.setZ
            },setter),
            $.extend({
                nttId: this.setNttId,
                classId: this.setClassId,
                colorDefn: this.getColorDefn,
                colorSource: this.getColorSource,
                hAlign: this.getHAlign,
                vAlign: this.getVAlign,
                z: this.getZ
            },getter)
        );

        this.deleteable = true;
        this.id = String(this.classId) + ':' + String(this.nttId);
        try{
            this.setColorDefn(JSON.parse(attr.colorDefn));
        } catch{
            console.log("WARNING! Invalid color definition");
        }
        this.setWidth(attr.width-8);

    },

    updateText: function(){
        switch (this.hAlign) {
            case "start":
                this.svgNodes[0].attr({"text-anchor":"start", x:(5)});
                break;
            case "end":
                this.svgNodes[0].attr({"text-anchor":"end", x:(this.width-5)});
                break;
            default:
                this.svgNodes[0].attr({"text-anchor":"middle", x:(this.width/2)});
        }
    },

    repaint: function(attributes){
        if (this.repaintBlocked === true || this.shape === null){
            return;
        }

        //repaint the shape
        this._super(attributes);
        this.updateText();

        return this;
    },

    setNttId: function(nttId){
        this.nttId = nttId;
    },

    getNttId: function(){
        return(this.nttId);
    },

    setClassId: function(classId){
        this.classId = classId;
    },

    getClassId: function(){
        return(this.classId);
    },

    setColorDefn: function(colorDefn) {
        this.colorDefn = colorDefn;
        this.setBackgroundColor(this.colorDefn.bgColor);
        this.setColor(this.colorDefn.strokeColor);
    },

    getColorDefn: function() {
        return(this.colorDefn);
    },

    setColorSource: function(colourSource) {
        this.colorSource = colourSource;
    },

    getColorSource: function() {
        return(this.colorSource);
    },

    setHAlign: function(hAlign){
        this.hAlign = hAlign;
    },

    getHAlign: function(){
        return(this.hAlign);
    },

    setVAlign: function(vAlign){
        this.vAlign = vAlign;
    },

    getVAlign: function(){
        return(this.vAlign);
    },

    setZ: function(z){
        this.z = z;
    },

    getZ: function(){
        return(this.z);
    }
});