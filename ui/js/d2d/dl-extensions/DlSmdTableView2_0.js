/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Service for managing the diagrams.
 *
 */
// TODO: Should probably add this.classId and make this.id without using attr.classId and attr.tableViewId.
// TODO: Should probably put the onRemoved call back inside create table element to avoid using attr.deleteCallback.
DlSmdTableView2_0 = draw2d.shape.node.Node.extend({

    NAME: "DlSmdTableView2_0",

    init: function(attr, setter, getter) {
        this.rows = null;
        this.pad = 10;
        this.collapsed = false;
        this.colorSource = 0; //this is used when defining custom color definitions but hasn't been implemented for for this object
        this.columnList = attr.columnList;
        this.tableViewName = attr.tableViewName;
        this.tableViewId = attr.tableViewId;
        this.schemaId = attr.schemaId;
        this.classId = attr.classId;
        this.deleteable = false;
        this.id = String(attr.classId) + ':' + String(attr.tableViewId);
        this.nttId = attr.tableViewId;

        this._super(
            $.extend({}, attr),
            $.extend({
                colorDefn: this.setColorDefn,
                collapsed: this.setCollapsed,
                columnList: this.setColumnList,
                tableViewName: this.setTableViewName,
                tableViewId: this.setTableViewId,
                schemaId: this.setSchemaId,
                headerClickCallback: this.setHeaderClickCallback,
                rowClickCallback: this.setRowClickCallback,
                deleteCallback: this.setDeleteCallback,
                nttId: this.setNttId,
                classId: this.setClassId
            }, setter),
            $.extend({
                colorDefn: this.getColorDefn,
                collapsed: this.getCollapsed,
                columnList: this.getColumnList,
                tableViewName: this.getTableViewName,
                tableViewId: this.getTableViewId,
                schemaId: this.getSchemaId,
                headerClickCallback: this.getHeaderClickCallback,
                rowClickCallback: this.getRowClickCallback,
                deleteCallback: this.getDeleteCallback,
                nttId: this.getNttId,
                classId: this.getClassId
            }, getter)
        );

        this.installEditPolicy(new draw2d.policy.figure.AntSelectionFeedbackPolicy());

        this.inputLocator  = new DlCollapsibleInputLocator();
        this.outputLocator = new DlCollapsibleOutputLocator();
        const portBgColor = new draw2d.util.Color("#ABCAF2");

        const inputPort = this.createPort("hybrid",  this.inputLocator);
        inputPort.setConnectionAnchor(new DlCrowFanConnectionAnchor(inputPort));
        //inputPort.setConnectionAnchor(new DlCrowConnectionAnchor(inputPort));
        inputPort.bgColor = portBgColor;
        inputPort.width = 5;
        inputPort.height = 5;
        const inputPortId = this.id + ":p1";
        inputPort.setId(inputPortId);
        inputPort.setName(inputPortId);

        const outputPort = this.createPort("hybrid",  this.outputLocator);
        outputPort.setConnectionAnchor(new DlCrowFanConnectionAnchor(outputPort));
        //outputPort.setConnectionAnchor(new DlCrowConnectionAnchor(outputPort));
        outputPort.bgColor = portBgColor;
        outputPort.width = 5;
        outputPort.height = 5;
        const outputPortId = this.id + ":p2";
        outputPort.setId(outputPortId);
        outputPort.setName(outputPortId);
    },

    repaint: function(attributes) {
        if (this.repaintBlocked===true || this.shape === null){
            return;
        }

        //Update the shape's new position and size
        attributes= attributes || {};
        attributes.x = this.getAbsoluteX();
        attributes.y = this.getAbsoluteY();
        attributes.width = this.getWidth();
        attributes.height = this.getHeight();

        //repaint the text
        this.rows.forEach((row,index)=>this.updateRows(row,index));

        //repaint the shape
        this._super(attributes);

        return this;
    },

    toFront: function(figure) {
        this._super(figure);
        this.rows.forEach((row)=>row.toFront());
    },

    toBack: function(figure) {
        this.rows.forEach((row)=>row.toBack());
        this._super(figure);
    },

    //This is inherited and must be overridden to change the behaviour.
    createShapeElement: function() {
        return(this.createTableElement());
    },

    createTableElement: function() {
        const box = this.canvas.paper.rect(this.getX(),this.getY(),this.getWidth(),this.getHeight());
        this.rows = this.createTableRows();
        box.attr({stroke: "#737373", fill: "#FFFFFF"});

        return box;
    },

    expandCollapse: function(collapsed){
        this.collapsed = collapsed;

        const rowClickCallback = function(index){
            this.rowClickCallback(this.columnList[index].columnId);
        }.bind(this);

        const mid = this.getY()+this.makeEven(this.getHeight())/2;

        if(this.collapsed){
            for(let i = 2; i<this.rows.length; i++){
                this.rows[i].attr({opacity:0}).toBack();
                this.rows[i].unclick();
            }
            this.setMinHeight(this.rows[0].attrs.height+30);
            this.setHeight(this.minHeight);
            this.setY(mid-this.makeEven(this.getHeight())/2);
        }
        else{
            let minHeight = 22+7;
            for(let i = 2; i<this.rows.length; i++){
                this.rows[i].attr({opacity:1}).toFront();
                $(this.rows[i].node).bind("contextmenu", function(){
                    rowClickCallback(i-2);
                });
                minHeight+=this.rows[i].getBBox(true).height;
            }
            this.setMinHeight(minHeight);
            this.setY(mid-this.makeEven(this.getHeight())/2);
        }
    },

    makeEven: function(val) {
      if(val%2 === 1){
          return val+1;
      }
      return val;
    },

    //Todo: make font-size and text-anchor variables
    createTableRows: function() {

        const rowClickCallback = function(index){
            this.rowClickCallback(this.columnList[index].columnId);
        }.bind(this);

        const headerClickCallback = function(){
            this.headerClickCallback(this.tableViewId);
        }.bind(this);

        const toggle = function() {
            this.expandCollapse(!this.collapsed);

        }.bind(this);

        const headerBackGround = this.canvas.paper.rect();

        const header = this.canvas.paper.text(0, 0, this.tableViewName);
        const tableViewId = this.tableViewId;
        header.attr({"font-size":12, "text-anchor":"start"});
        $(header.node).bind("contextmenu", function(){
            headerClickCallback(tableViewId);
        });

        const columns = [headerBackGround, header];
        let minWidth = header.getBBox(true).width;
        let minHeight = 22 + 7; //This will be the height of the header background

        for (let i = 0; i < this.columnList.length; i++) {
            const column = this.columnList[i];
            const newColumn = this.canvas.paper.text(0, 0, column.columnName);
            newColumn.attr({"font-size":12,"text-anchor":"start"});
            $(newColumn.node).bind("contextmenu", function(){
                rowClickCallback(i);
            });
            const bBox = newColumn.getBBox(true);
            minWidth = Math.max(minWidth,bBox.width);
            minHeight+=bBox.height;
            columns.push(newColumn);
        }
        this.setMinWidth(minWidth+2*this.pad);
        this.setMinHeight(minHeight);

        headerBackGround.dblclick(toggle);
        header.dblclick(toggle);

        return (columns);
    },

    //Todo: make colours and buffer variables(NOTE: the default attr call has been copied and pasted in the toggle method)
    updateRows: function(row, index) {
        switch (index) {
            case 0: //The header background
                row.attr({x:this.x,y:this.y,width:this.width,height:22,stroke:"#737373",fill:"#737373"});
                break;
            case 1: //The header text
                row.attr({x:(this.x+this.pad),y:(this.y+11),fill:"#FFFFFF"});
                break;
            default: //The row text
                row.attr({x:(this.x+this.pad),y:(this.y+14*(index-1)+18),fill:"#303030"});
        }
    },

    setCanvas: function( canvas ) {
        // remove the rows
        if(canvas===null && this.rows!==null){
            this.rows.forEach(row=>row.remove());
            this.rows=null;
        }

        this._super(canvas);
    },

    setCollapsed: function(collapsed){
        this.collapsed = collapsed;
    },

    getCollapsed: function(){
        return(this.collapsed);
    },

    setColorDefn: function(colorDefn) {
        this.colorDefn = colorDefn;
    },

    getColorDefn: function() {
        return(this.colorDefn);
    },

    setColumnList: function(columnList) {
        this.columnList = columnList;
    },

    getColumnList: function() {
        return(this.columnList);
    },

    setTableViewName: function(tableViewName) {
        this.tableViewName = tableViewName;
    },

    getTableViewName: function() {
        return(this.tableViewName);
    },

    setTableViewId: function(tableViewId) {
        this.tableViewId = tableViewId;
    },

    getTableViewId: function() {
        return(this.tableViewId);
    },

    setSchemaId: function(schemaId) {
        this.schemaId = schemaId;
    },

    getSchemaId: function() {
        return(this.schemaId);
    },

    setHeaderClickCallback: function(headerClickCallback) {
        this.headerClickCallback = headerClickCallback;
    },

    getHeaderClickCallback: function() {
        return(this.headerClickCallback);
    },

    setRowClickCallback: function(rowClickCallback) {
        this.rowClickCallback = rowClickCallback;
    },

    getRowClickCallback: function() {
        return(this.rowClickCallback);
    },

    setDeleteCallback: function(deleteCallback) {
        this.deleteCallback = deleteCallback;
    },

    getDeleteCallback: function() {
        return(this.deleteCallback);
    },

    setNttId: function(nttId) {
        this.nttId = nttId;
    },

    getNttId: function() {
        return(this.nttId);
    },

    setClassId: function(classId) {
        this.classId = classId;
    },

    getClassId: function() {
        return(this.classId);
    }
});
