DlDiagramConnector = draw2d.Connection.extend({

    NAME: "DlDiagramConnector",

    init: function(attr,setter,getter) {
        this._super(
            $.extend({
                stroke:2,
                outlineStroke:0,
                outlineColor:"#303030",
                color:"999999",
                targetDecorator: new draw2d.decoration.connection.dl01Decorator()
            },attr),
            $.extend({
                cnctId: this.setCnctId,
                classId: this.setClassId,
                cnctName: this.setCnctName,
                cnctDesc: this.setCnctDesc
            },setter),
            $.extend({
                cnctId: this.getCnctId,
                classId: this.getClassId,
                cnctName: this.getCnctName,
                cnctDesc: this.getCnctDesc
            },getter)
        );

        this.deleteable = false;
        this.id = String(this.classId) + ':' + String(this.cnctId);
    },

    setCnctId: function(cnctId){
        this.cnctId = cnctId;
    },

    getCnctId: function(){
        return(this.cnctId);
    },

    setClassId: function(classId){
        this.classId = classId;
    },

    getClassId: function(){
        return(this.classId);
    },

    setCnctDesc: function(cnctDesc){
        this.cnctDesc = cnctDesc;
    },

    getCnctDesc: function(){
        return(this.cnctDesc);
    },

    setCnctName: function(cnctName){
        this.cnctName = cnctName;
    },

    getCnctName: function(){
        return(this.cnctName);
    }
});
