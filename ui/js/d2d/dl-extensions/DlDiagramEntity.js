DlDiagramEntity = draw2d.shape.node.Node.extend({

    NAME: "DlDiagramEntity",

    init: function(attr,setter,getter) {
        this.title = null;

        this._super(
            $.extend({
                minWidth: 40,
                minHeight: 60
            },attr),
            $.extend({
                nttId: this.setNttId,
                classId: this.setClassId,
                nttName: this.setNttName,
                nttDesc: this.setNttDesc,
                colorDefn: this.setColorDefn,
                colorSource: this.setColorSource,
                collapsed: this.setCollapsed,
                iconSvgString: this.setIconSvgString,
                backgroundColor: this.setBackgroundColor,
                iconColor: this.setIconColor
            },setter),
            $.extend({
                nttId: this.getNttId,
                classId: this.getClassId,
                nttName: this.getNttName,
                nttDesc: this.getNttDesc,
                colorDefn: this.getColorDefn,
                colorSource: this.getColorSource,
                collapsed: this.getCollapsed,
                iconSvgString: this.getIconSvgString,
                backgroundColor: this.getBackgroundColor,
                iconColor: this.getIconColor
            },getter)
        );

        this.deleteable = false;
        this.id = String(this.classId) + ':' + String(this.nttId);
        try{
            this.colorDefn = JSON.parse(this.colorDefn);
        } catch{
            console.log("WARNING! Invalid color definition");
        }

        const portBgColor = new draw2d.util.Color("#ABCAF2");

        const port = this.createPort("hybrid",  new draw2d.layout.locator.CenterLocator);
        port.setConnectionAnchor(new draw2d.layout.anchor.FanConnectionAnchor(port));
        port.bgColor = portBgColor;
        port.width = 5;
        port.height = 5;
        const portId = this.id + ":p";
        port.setId(portId);
        port.setName(portId);
    },

    repaint: function(attributes) {
        if (this.repaintBlocked===true || this.shape === null){
            return;
        }

        //Update the shape's new position and size
        attributes = attributes || {};
        attributes.x = this.getAbsoluteX();
        attributes.y = this.getAbsoluteY();
        attributes.width = this.getWidth();
        attributes.height = this.getHeight();

        this.title.attr({x:this.x+this.width/2,y:this.y+10});

        //repaint the shape
        this._super(attributes);

        return this;
    },

    toFront: function(figure) {
        this._super(figure);
        this.title.toFront();
    },

    toBack: function(figure) {
        this.title.toBack();
        this._super(figure);
    },

    //This is inherited and must be overridden to change the behaviour.
    createShapeElement: function() {
        return(this.createEntity());
    },

    createEntity: function() {
        const box = this.canvas.paper.rect(this.getX(),this.getY(),this.getWidth(),this.getHeight());
        box.attr({stroke: "#737373", fill: this.colorDefn.bgColor});
        this.title = this.canvas.paper.text(0,0,this.nttName);
        this.setMinWidth(Math.max(this.minWidth,this.title.getBBox(true).width+20));

        return box;
    },

    setCanvas: function( canvas ) {
        // remove additional stuff here
        if(this.title != null){
            this.title.remove()
            this.title=null;
        }
        this._super(canvas);
    },

    setNttId: function(nttId){
        this.nttId = nttId;
    },

    getNttId: function(){
        return(this.nttId);
    },

    setClassId: function(classId){
        this.classId = classId;
    },

    getClassId: function(){
        return(this.classId);
    },

    setNttDesc: function(nttDesc){
        this.nttDesc = nttDesc;
    },

    getNttDesc: function(){
        return(this.nttDesc);
    },

    setNttName: function(nttName){
        this.nttName = nttName;
    },

    getNttName: function(){
        return(this.nttName);
    },

    setCollapsed: function(collapsed){
        this.collapsed = collapsed;
    },

    getCollapsed: function(){
        return(this.collapsed);
    },

    setIconSvgString: function(iconSvgString) {
        this.iconSvgString = iconSvgString;
    },

    getIconSvgString: function() {
        return(this.iconSvgString);
    },

    setColorDefn: function(colorDefn) {
        this.colorDefn = colorDefn;
    },

    getColorDefn: function() {
        return(this.colorDefn);
    },

    setColorSource: function(colourSource) {
        this.colorSource = colourSource;
    },

    getColorSource: function() {
        return(this.colorSource);
    },

    setBackgroundColor: function(backgroundColor) {
        this.colorDefn.bgColor = backgroundColor;
        const attributes = {
            fill: backgroundColor
        };
        this.repaint(attributes);
    },

    getBackgroundColor: function() {
        return(this.colorDefn.bgColor);
    },

    setIconColor: function(iconColor) {
        this.colorDefn.iconColor = iconColor;
        this.getChildren().data[0].setBackgroundColor(iconColor);
    },

    getIconColor: function() {
        return(this.colorDefn.iconColor);
    }
});
