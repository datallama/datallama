const DlInsideBottomLeftLocator = draw2d.layout.locator.Locator.extend({
    init: function(attr)
    {
        this._super();
        this.margin = (attr && ( "margin" in attr)) ? attr.margin : -18;
    },

    relocate: function(index, target)
    {
        var parent = target.getParent();
        var boundingBox = parent.getBoundingBox();

        // I made a wrong decision in the port handling: anchor point
        // is in the center and not topLeft. Now I must correct this flaw here, and there, and...
        // shit happens.
        var offset = (parent instanceof draw2d.Port) ? boundingBox.h/2 : 0;

        if(target instanceof draw2d.Port){
            target.setPosition(0,(boundingBox.h/2)-offset);
        }
        else{
            var targetBoundingBox = target.getBoundingBox();
            // target.setPosition(y, x) -- original: target.setPosition(-targetBoundingBox.w - this.margin, (boundingBox.h/2)-(targetBoundingBox.h/2)-offset);
            target.setPosition(-targetBoundingBox.w - this.margin, (boundingBox.h)-(targetBoundingBox.h + 4));
        }
    }
});