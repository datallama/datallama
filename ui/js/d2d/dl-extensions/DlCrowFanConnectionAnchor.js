
DlCrowFanConnectionAnchor = draw2d.layout.anchor.ConnectionAnchor.extend({

	NAME : "DlCrowFanConnectionAnchor",

	/**
	 * @constructor
	 * 
	 * @param {draw2d.Figure} owner the figure to use for the anchor calculation
	 * @param {Number} [separation] the separation or fan distance between the concurrent/conflicting anchors 
	 */
	init: function(owner, separation)
    {
		this._super(owner);
		
		if( separation ){
		    this.separation = parseInt(separation);
		}
		else{
		    this.separation = 10;
		}
	},

	/**
	 * @method
	 * 
	 * Returns the location where the Connection should be anchored in
	 * absolute coordinates. The anchor may use the given reference
	 * Point to calculate this location.
	 * 
	 * @param {draw2d.geo.Point} reference The reference Point in absolute coordinates
     * @param {draw2d.Connection} inquiringConnection the connection who ask for the location.
     *
	 * @return {draw2d.geo.Point} The anchor's location
	 */
	getLocation: function(reference, inquiringConnection)
    {
        const box = this.getBox();
        box.x = box.x - 20;
        box.y = box.y - 20;
        box.w = box.w + 40;
        box.h = box.h + 40;

	    var r = new draw2d.geo.Rectangle(0,0);
        r.setBounds(box);
        r.translate(-1, -1);
        r.resize(1, 1);

        var center = r.getCenter();

        if (r.isEmpty() || (reference.x === center.x && reference.y === center.y)){
            return center; // This avoids divide-by-zero
        }

        // translate the center if required
        //
        var s = inquiringConnection.getSource();
        var t = inquiringConnection.getTarget();
        var lines = this.getOwner().getConnections().clone();
        lines.grep(function(other){
            return (other.getTarget() === t && other.getSource() === s) ||(other.getTarget() === s && other.getSource() === t);
        });
        var index= lines.indexOf(inquiringConnection)+1;
        var position = center.getPosition(reference);
        var ray;
        if (position == draw2d.geo.PositionConstants.SOUTH || position == draw2d.geo.PositionConstants.EAST){
            ray = new draw2d.geo.Point(reference.x - center.x, reference.y - center.y);
        }
        else{
            ray = new draw2d.geo.Point(center.x - reference.x, center.y - reference.y);
        }
        var length = Math.sqrt(ray.x * ray.x + ray.y * ray.y);
        if(index<=2){
            length *= 1.5;
        }
        var xSeparation = this.separation * ray.x / length;
        var ySeparation = this.separation * ray.y / length;
        if (index % 2 === 0){
            center = new draw2d.geo.Point(center.x + (index / 2) * (-1 * ySeparation), center.y + (index / 2) * xSeparation);
        }
        else{
            center = new draw2d.geo.Point(center.x + (index / 2) * ySeparation, center.y + (index / 2) * (-1 * xSeparation));
        }

        var intersections= box.intersectionWithLine(center, reference);
        // perfect - one intersection mean that the shifted center point is inside the bounding box and has only one intersection with it.
        //
        switch(intersections.getSize()){
            case 0:
                // calculate the edge of the bounding box which is nearest to the reference point
                var v = box.getVertices();
                var first = v.first();
                first.distance = reference.getDistance(first);
                return this.fixCorners(v.asArray().reduce(function(previous, current){
                    current.distance= reference.getDistance(current);
                    return current.distance<previous.distance?current:previous;
                }), box, inquiringConnection);
            case 1:
                return(this.fixCorners(intersections.get(0), box, inquiringConnection));
            case 2:
                // get the nearest of these points
                var p0= intersections.get(0); 
                var p1= intersections.get(1); 
                var p0diff = reference.getDistance(p0);
                var p1diff = reference.getDistance(p1);
                if(p0diff<p1diff){
                    return (this.fixCorners(p0, box, inquiringConnection));
                }
                return (this.fixCorners(p1, box, inquiringConnection));
        }
        
        // we have 0 or 2 intersections with the bounding box. This means the shifted 
        // calculate the intersection if the new "center" with the bounding box of the 
        // shape (if any exists)
        
	},

    /**
     * @param point: the anchor point of the connection
     * @param box: the box with padding = 20 on all side
     * @param inquiringConnection
     */
    fixCorners: function(point, box, inquiringConnection){
        const p = 30;
        const source = this.getOwner().getId().toString() === inquiringConnection.getSource().getId().toString();

	    if(point.x-box.x === 0){ //The point is on the left side of the box
            if(point.y < box.y + p){ //The point is too high
                point.y = box.y + p;
            }
            else if(point.y > box.y+box.h-p){ //The point is too low
                point.y = box.y+box.h-p;
            }

            if(inquiringConnection.NAME === "DlPdmDiagramConnector"){
                if(source) {
                    inquiringConnection.setSourceSide(0);
                }
                else{
                    inquiringConnection.setTargetSide(0);
                }
            }
        }
        else if(point.x-box.x === box.w){ //The point is on the right side of the box
            if(point.y < box.y + p){ //The point is too high
                point.y = box.y + p;
            }
            else if(point.y > box.y+box.h-p){ //The point is too low
                point.y = box.y+box.h-p;
            }

            if(inquiringConnection.NAME === "DlPdmDiagramConnector"){
                if(source) {
                    inquiringConnection.setSourceSide(180);
                }
                else{
                    inquiringConnection.setTargetSide(180);
                }
            }
        }
        else if(point.y-box.y === 0){ //The point is on the top of the box
            if(point.x < box.x + p){ //The point is too far left
                point.x = box.x + p;
            }
            else if(point.x > box.x+box.w-p){ //The point is too far right
                point.x = box.x+box.w-p;
            }

            if(inquiringConnection.NAME === "DlPdmDiagramConnector"){
                if(source) {
                    inquiringConnection.setSourceSide(90);
                }
                else{
                    inquiringConnection.setTargetSide(90);
                }
            }
        }
        else if(point.y-box.y === box.h){ //The point is on the bottom of the box
            if(point.x < box.x + p){ //The point is too far left
                point.x = box.x + p;
            }
            else if(point.x > box.x+box.w-p){ //The point is too far right
                point.x = box.x+box.w-p;
            }

            if(inquiringConnection.NAME === "DlPdmDiagramConnector"){
                if(source) {
                    inquiringConnection.setSourceSide(270);
                }
                else{
                    inquiringConnection.setTargetSide(270);
                }
            }
        }
        return point;
    },
	
	/**
	 * Returns the bounds of this Anchor's owner. Subclasses can
	 * override this method to adjust the box. Maybe you return the box
	 * of the port parent (the parent figure)
	 * 
	 * @return {draw2d.geo.Rectangle} The bounds of this Anchor's owner
	 */
	getBox: function()
    {
		return this.getOwner().getParent().getBoundingBox();
	},

	/**
	 * @method
	 * 
     * Returns the reference point for this anchor in absolute coordinates. This might be used
     * by another anchor to determine its own location.
	 * 
     * @param {draw2d.Connection} inquiringConnection the connection who ask for the location.
     *
	 * @return {draw2d.geo.Point} The bounds of this Anchor's owner
	 */
	getReferencePoint: function(inquiringConnection)
    {
		return this.getBox().getCenter();
	}
});
