/**
 * Data Llama 
 * The DlSmdTableView class models a Structural Metadata (SMD) table of view for Physical Data Model diagrams.
 * An SMD TableView is a representation of a database table or view.
 * The structure of parameter tableViewDefinition is:
 * {
 *   tableViewId: number,
 *   classId: number,
 *   tableViewName: string,
 *   x: number,
 *   y: number,
 *   columnList: [{<columnDefinition>}]
 * }
 * For the structure of a columnDefinition object refer to script DlSmdColumm.js
 */

var DlSmdTableView = draw2d.shape.layout.VerticalLayout.extend({

	NAME: "DlSmdTableView",

    init : function(tableViewDefinition, showEntityDetailsCallback, showColumnDetailsCallback)
    {
        this.inputLocator  = new DlCollapsibleInputLocator();
        this.outputLocator = new DlCollapsibleOutputLocator();
		var portBGcolor = new draw2d.util.Color("#ABCAF2");

		// Override the drag-and-drop connection action
		/*scope.canvas.installEditPolicy( new draw2d.policy.connection.DragConnectionCreatePolicy({
			createConnection: scope.newConnectionCreator
		}));*/

		var coords = {x: tableViewDefinition.x, y: tableViewDefinition.y};
        this._super($.extend({bgColor:"#FFFFFF", color:"#737373", stroke:1, radius:2, gap:5}, coords));

        var diagramId = String(tableViewDefinition.classId) + ':' + String(tableViewDefinition.tableViewId);
        this.id = diagramId;

        this.header = new draw2d.shape.layout.HorizontalLayout({
            stroke: 0,
            radius: 0,
            bgColor: "#737373"
        });

        var label = null;
        this.header.add(label = new draw2d.shape.basic.Label({
            text:tableViewDefinition.tableViewName,
            fontColor:"#FFFFFF",
            stroke:0,
            fontSize:12,
            fontFamily:"Verdana",
            padding:{left:10, right:10}
        }));

		/**
		 * Definition: collapses the table object so that only the header is visible
		 */
        var toggle = function(){
			this.getChildren().each( function(i, child) {
				if (child.isCollapsible != null) {
					if (child.isCollapsible == true) {
						child.setVisible(!child.isVisible());
					}
				}
			});
        }.bind(this);


		// setup the collapse/expand behaviour
        label.on("dblclick",toggle);
        this.header.on("dblclick",toggle);

        
		/**
		 * Definition: call the showEntityDetailsCallback function that was passed to this.init
		 */
        var headerClick = function(){
            showEntityDetailsCallback(tableViewDefinition.tableViewId);
        }

        label.on("click",headerClick);
        this.header.on("click",headerClick);

        var inputPort = this.createPort("hybrid",  this.inputLocator);
		inputPort.setConnectionAnchor(new draw2d.layout.anchor.FanConnectionAnchor(inputPort));
		inputPort.bgColor = portBGcolor;
		inputPort.width = 5;
		inputPort.height = 5;
        var inputPortId = diagramId + ":p1";
        inputPort.setId(inputPortId);
        inputPort.setName(inputPortId);

        var outputPort = this.createPort("hybrid",  this.outputLocator);
		outputPort.setConnectionAnchor(new draw2d.layout.anchor.FanConnectionAnchor(outputPort));
		outputPort.bgColor = portBGcolor;
		outputPort.width = 5;
		outputPort.height = 5;
        var outputPortId = diagramId + ":p2";
        outputPort.setId(outputPortId);
        outputPort.setName(outputPortId);

        this.add(this.header);

		// add the column definitions to the table/view
		var columnLabelAttributes = {
		    columnId:0,
            text:"not set",
            fontColor:"#303030",
            resizeable:true,
            stroke:0,
            padding:{left:10, top:-2, bottom:0}
		};

		for (var i = 0; i < tableViewDefinition.columnList.length; i++) {

            columnLabelAttributes.columnId = tableViewDefinition.columnList[i].columnId;
			columnLabelAttributes.text = tableViewDefinition.columnList[i].columnName;
			var newColumnObj = new DlSmdColumn(columnLabelAttributes, showColumnDetailsCallback);

			this.add(newColumnObj);
		}
        // add a footer buffer
        this.add(new draw2d.shape.basic.Label({text:"", fontColor:"#303030", resizeable:true, stroke:0 }));

    }
});
