DlPdmDiagramConnector = draw2d.Connection.extend({

    NAME: "DlPdmDiagramConnector",

    init: function(attr,setter,getter) {
        const dlTargetDecorator = new DlCrowManyDecorator(20,20);
        const dlSourceDecorator = new DlCrowOneDecorator(20,20);
        //dlTargetDecorator.setBackgroundColor(new draw2d.util.Color("#737373"));
        this._super(
            $.extend({
                stroke:1,
                outlineStroke:0,
                outlineColor:"#303030",
                color: new draw2d.util.Color("#737373"),
                targetDecorator: dlTargetDecorator,
                sourceDecorator: dlSourceDecorator
            },attr),
            $.extend({
                cnctId: this.setCnctId,
                classId: this.setClassId,
                cnctName: this.setCnctName,
                cnctDesc: this.setCnctDesc,
                parentColumnId: this.setParentColumnId,
                childColumnId: this.setChildColumnId,
                parentTableViewId: this.setParentTableViewId,
                childTableViewId: this.setChildTableViewId,
                sourceSide: this.setSourceSide,
                targetSide: this.setTargetSide
            },setter),
            $.extend({
                cnctId: this.getCnctId,
                classId: this.getClassId,
                cnctName: this.getCnctName,
                cnctDesc: this.getCnctDesc,
                parentColumnId: this.getParentColumnId,
                childColumnId: this.getChildColumnId,
                parentTableViewId: this.getParentTableViewId,
                childTableViewId: this.getChildTableViewId,
                sourceSide: this.getSourceSide,
                targetSide: this.getTargetSide
            },getter)
        );

        this.deleteable = false;
        this.id = String(this.classId) + ':' + String(this.cnctId);
    },

    repaint: function(attributes)
    {
        if(this.repaintBlocked===true || this.shape===null){
            return;
        }

        if(this.sourcePort===null || this.targetPort===null){
            return;
        }

        this._super(attributes);

        // paint the decorator if any exists
        //
        if(this.targetDecorator!==null && this.targetDecoratorNode===null){
            this.targetDecoratorNode= this.targetDecorator.paint(this.getCanvas().paper);
        }

        if(this.sourceDecorator!==null && this.sourceDecoratorNode===null){
            this.sourceDecoratorNode= this.sourceDecorator.paint(this.getCanvas().paper);
        }

        var _this = this;

        // translate/transform the SOURCE decoration on the start of the connection and rotate depending on the side
        // of the table/view (north, east, south, west) to which the source decorator is connected
        if(this.sourceDecoratorNode!==null){
            var start = this.getVertices().first();
            this.sourceDecoratorNode.transform("r" + this.getSourceSide() + "," + start.x + "," + start.y + "t" + start.x + "," + start.y);
            // propagate the color and the opacity to the decoration as well
            this.sourceDecoratorNode.attr({"stroke":"#"+this.lineColor.hex(), opacity:this.alpha});
            this.sourceDecoratorNode.forEach(function(shape){
                shape.node.setAttribute("class",_this.cssClass!==null?_this.cssClass:"");
            });
        }

        // translate/transform the TARGET decoration on the end of the connection and rotate depending on the side
        // of the table/view (north, east, south, west) to which the target decorator is connected
        if(this.targetDecoratorNode!==null){
            var end = this.getVertices().last();
            this.targetDecoratorNode.transform("r" + this.getTargetSide() + "," + end.x + "," + end.y + "t" + end.x + "," + end.y);
            this.targetDecoratorNode.attr({"stroke":"#"+this.lineColor.hex(), opacity:this.alpha});
            this.targetDecoratorNode.forEach(function(shape){
                shape.node.setAttribute("class",_this.cssClass!==null?_this.cssClass:"");
            });
        }
    },

    setCnctId: function(cnctId){
        this.cnctId = cnctId;
    },

    getCnctId: function(){
        return(this.cnctId);
    },

    setClassId: function(classId){
        this.classId = classId;
    },

    getClassId: function(){
        return(this.classId);
    },

    setCnctDesc: function(cnctDesc){
        this.cnctDesc = cnctDesc;
    },

    getCnctDesc: function(){
        return(this.cnctDesc);
    },

    setCnctName: function(cnctName){
        this.cnctName = cnctName;
    },

    getCnctName: function(){
        return(this.cnctName);
    },

    setParentColumnId: function(parentColumnId){
        this.parentColumnId = parentColumnId;
    },

    getParentColumnId: function(){
        return(this.parentColumnId);
    },

    setChildColumnId: function(childColumnId){
        this.childColumnId = childColumnId;
    },

    getChildColumnId: function(){
        return(this.childColumnId);
    },

    setParentTableViewId: function(parentTableViewId){
        this.parentTableViewId = parentTableViewId;
    },

    getParentTableViewId: function(){
        return(this.parentTableViewId);
    },

    setChildTableViewId: function(childTableViewId){
        this.childTableViewId = childTableViewId;
    },

    getChildTableViewId: function(){
        return(this.childTableViewId);
    },

    /**
     * This function is redundant but left for backwards compatibility
     * @param sourceSide
     */
    setSourceSide: function(sourceSide){
        this.sourceSide = sourceSide;
    },

    /**
     * Returns the side of the table/view (North,East,South,West) on which the connector SOURCE is located.
     * @returns {number}
     */
    getSourceSide: function(){
        let sourceSide = 0;
        const west_x = this.sourcePort.parent.x;
        const east_x = this.sourcePort.parent.x + this.sourcePort.parent.width;
        const north_y = this.sourcePort.parent.y;
        const south_y = this.sourcePort.parent.y + this.sourcePort.parent.height;

        const me_x = this.vertices.data[0].x;
        const me_y = this.vertices.data[0].y;

        if (me_x > east_x) {
            sourceSide = 180;
        }
        else if (me_x < west_x) {
            sourceSide = 0;
        }
        else {
            // calculate north/south positioning
            if (me_y <  north_y) {
                sourceSide = 90;
            }
            else {
                sourceSide = 270;
            }
        }
        return(sourceSide);
    },

    /**
     * This function is redundant but left for backwards compatibility
     * @param sourceSide
     */
    setTargetSide: function(targetSide){
        this.targetSide = targetSide;
    },

    /**
     * Returns the side of the table/view (North,East,South,West) on which the connector TARGET is located.
     * @returns {number}
     */
    getTargetSide: function() {
        let targetSide = 0;
        const west_x = this.targetPort.parent.x;
        const east_x = this.targetPort.parent.x + this.targetPort.parent.width;
        const north_y = this.targetPort.parent.y;
        const south_y = this.targetPort.parent.y + this.targetPort.parent.height;

        const me_x = this.vertices.data[this.vertices.data.length - 1].x;
        const me_y = this.vertices.data[this.vertices.data.length - 1].y;

        if (me_x > east_x) {
            targetSide = 180;
        }
        else if (me_x < west_x) {
            targetSide = 0;
        }
        else {
            // calculate north/south positioning
            if (me_y <  north_y) {
                targetSide = 90;
            }
            else {
                targetSide = 270;
            }
        }
        return(targetSide);
    }
});
