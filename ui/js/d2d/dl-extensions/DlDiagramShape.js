DlDiagramShape = draw2d.shape.node.Node.extend({
    NAME : "DlDiagramShape",

    init: function(attr, setter, getter ) {
        this.displayShape = null;
        this.displayText = null;
        this._super(
            $.extend({},attr),
            $.extend({
                nttId: this.setNttId,
                classId: this.setClassId,
                shapeSvgString: this.setShapeSvgString,
                svgWidth: this.setSvgWidth,
                svgHeight: this.setSvgHeight,
                colorDefn: this.setColorDefn,
                colorSource: this.setColorSource,
                text: this.setText,
                nttName: this.setNttName,
                nttDesc: this.setNttDesc,
                hAlign: this.setHAlign,
                vAlign: this.setVAlign,
                z: this.setZ,
                ctClassId: this.setCtClassId,
                ctObjectId: this.setCtObjectId,
                ctObjectName: this.setCtObjectName
            },setter),
            $.extend({
                nttId: this.setNttId,
                classId: this.setClassId,
                shapeSvgString: this.getShapeSvgString,
                svgWidth: this.getSvgWidth,
                svgHeight: this.getSvgHeight,
                colorDefn: this.getColorDefn,
                colorSource: this.getColorSource,
                text: this.getText,
                nttName: this.getNttName,
                nttDesc: this.getNttDesc,
                hAlign: this.getHAlign,
                vAlign: this.getVAlign,
                z: this.getZ,
                ctClassId: this.getCtClassId,
                ctObjectId: this.getCtObjectId,
                ctObjectName: this.getCtObjectName
            },getter)
        );

        this.deleteable = true;
        this.id = String(this.classId) + ':' + String(this.nttId);
        try{
            this.colorDefn = JSON.parse(this.colorDefn);
        } catch{
            console.log("WARNING! Invalid color definition");
        }

        const portBgColor = new draw2d.util.Color("#ABCAF2");

        const port = this.createPort("hybrid",  new draw2d.layout.locator.CenterLocator);
        port.setConnectionAnchor(new draw2d.layout.anchor.FanConnectionAnchor(port));
        port.bgColor = portBgColor;
        port.width = 5;
        port.height = 5;
        const portId = this.id + ":p";
        port.setId(portId);
        port.setName(portId);
    },

    createShapeElement: function() {
        const box = this.canvas.paper.rect(this.getX(),this.getY(),this.getWidth(),this.getHeight());
        box.attr({stroke: null, fill: null});
        this.displayShape = this.canvas.paper.path(this.shapeSvgString);
        this.displayText = this.canvas.paper.text(0,0,this.text);
        this.displayText.attr({"font-size":12,"text-anchor":this.hAlign});

        this.setMinWidth(this.displayText.getBBox(true).width + 20);
        this.setMinHeight(this.displayText.getBBox(true).height + 10);

        this.updateDisplayText();
        this.colorDisplayShape();
        this.transformDisplayShape();
        return box;
    },

    repaint: function(attributes) {
        if (this.repaintBlocked === true || this.shape === null){
            return;
        }

        //Update the shape's new position and size
        attributes= attributes || {};
        attributes.x = this.getAbsoluteX();
        attributes.y = this.getAbsoluteY();
        attributes.width = this.getWidth();
        attributes.height = this.getHeight();

        //repaint the display shape
        this.transformDisplayShape();

        //update the display text
        this.updateDisplayText();

        //repaint the shape
        this._super(attributes);

        return this;
    },

    toFront: function(figure) {
        this.displayShape.toFront();
        this.displayText.toFront();
        this._super(figure);
    },

    toBack: function(figure) {
        this._super(figure);
        this.displayText.toBack();
        this.displayShape.toBack();
    },

    updateDisplayText: function() {
        let vOffset = 0;
        if(this.displayText.getBBox(true).height/2 > (this.getHeight()*(this.vAlign/100) - 5)){
            vOffset = 5 + this.displayText.getBBox(true).height/2 - this.getHeight()*(this.vAlign/100);
        } else if(this.displayText.getBBox(true).height/2 > (this.getHeight()*(1-(this.vAlign/100))-5)){
            vOffset = 5 + this.displayText.getBBox(true).height/2 - this.getHeight()*(1-(this.vAlign/100));
            vOffset = -vOffset;
        }
        switch (this.hAlign) {
            case "start":
                this.displayText.attr({x:(this.x+10),y:(this.y+this.height*(this.vAlign/100)+vOffset)});
                break;
            case "end":
                this.displayText.attr({x:(this.x+this.width-10),y:(this.y+this.height*(this.vAlign/100)+vOffset)});
                break;
            default:
                this.displayText.attr({x:(this.x+this.width/2),y:(this.y+this.height*(this.vAlign/100)+vOffset)});
        }

    },

    updateText: function() {
        switch (this.hAlign) {
            case "start":
                this.displayText.attr({text:this.text, "text-anchor":"start"});
                break;
            case "end":
                this.displayText.attr({text:this.text, "text-anchor":"end"});
                break;
            default:
                this.displayText.attr({text:this.text, "text-anchor":"middle"});
        }
        this.setMinWidth(this.displayText.getBBox(true).width + 20);
        this.setMinHeight(this.displayText.getBBox(true).height + 10);

        this.updateDisplayText();
    },

    transformDisplayShape: function(){
        this.displayShape.transform("T"+(this.getAbsoluteX()+(this.getWidth()-this.getSvgWidth())/2)+","+(this.getAbsoluteY()+(this.getHeight()-this.getSvgHeight())/2)+" S"+(this.getWidth()/this.getSvgWidth())+","+(this.getHeight()/this.getSvgHeight()));
    },

    colorDisplayShape: function(){
        try{
            this.displayShape.attr({"fill":this.colorDefn.bgColor, "stroke":this.colorDefn.strokeColor});
        } catch {
            //This is a quick and dirty fix to an error I get while initialising the object.
        }
    },

    setCanvas: function( canvas ) {
        // remove the display shape
        if(canvas === null && this.displayShape!==null){
            this.displayShape.remove();
            this.displayShape = null;
            this.displayText.remove();
            this.displayText = null;
        }

        this._super(canvas);
    },

    setShapeSvgString: function(shapeSvgString) {
        this.shapeSvgString = shapeSvgString;
    },

    getShapeSvgString: function () {
        return(this.shapeSvgString);
    },

    setSvgWidth: function(svgWidth) {
        this.svgWidth = svgWidth;
    },

    getSvgWidth: function() {
        return(this.svgWidth);
    },

    setSvgHeight: function(svgHeight) {
        this.svgHeight = svgHeight;
    },

    getSvgHeight: function() {
        return(this.svgHeight);
    },

    setNttId: function(nttId){
        this.nttId = nttId;
    },

    getNttId: function(){
        return(this.nttId);
    },

    setClassId: function(classId){
        this.classId = classId;
    },

    getClassId: function(){
        return(this.classId);
    },

    setColorDefn: function(colorDefn) {
        this.colorDefn = colorDefn;
        this.colorDisplayShape();
    },

    getColorDefn: function() {
        return(this.colorDefn);
    },

    setColorSource: function(colourSource) {
        this.colorSource = colourSource;
    },

    getColorSource: function() {
        return(this.colorSource);
    },

    setText: function(text) {
        this.text = text;
    },

    getText: function() {
        return(this.text)
    },

    setNttDesc: function(nttDesc){
        this.nttDesc = nttDesc;
    },

    getNttDesc: function(){
        return(this.nttDesc);
    },

    setNttName: function(nttName){
        this.nttName = nttName;
    },

    getNttName: function(){
        return(this.nttName);
    },

    setHAlign: function(hAlign){
        this.hAlign = hAlign;
    },

    getHAlign: function(){
        return(this.hAlign);
    },

    setVAlign: function(vAlign){
        this.vAlign = vAlign;
    },

    getVAlign: function(){
        return(this.vAlign);
    },

    setZ: function(z){
        this.z = z;
    },

    getZ: function(){
        return(this.z);
    },

    setCtClassId: function(ctClassId){
        this.ctClassId = ctClassId;
    },

    getCtClassId: function(){
        return(this.ctClassId);
    },

    setCtObjectId: function(ctObjectId){
        this.ctObjectId = ctObjectId;
    },

    getCtObjectId: function(){
        return(this.ctObjectId);
    },

    setCtObjectName: function(ctObjectName){
        this.ctObjectName = ctObjectName;
    },

    getCtObjectName: function(){
        return(this.ctObjectName);
    }
});