DlDiagramArrow = draw2d.Connection.extend({

    NAME: "DlDiagramArrow",

    init: function(attr,setter,getter) {
        this._super(
            $.extend({
                stroke:2,
                outlineStroke:0,
                outlineColor:"#303030",
                color:"999999",
                targetDecorator: new draw2d.decoration.connection.ArrowDecorator()
            },attr),
            $.extend({
                cnctId: this.setCnctId,
                classId: this.setClassId,
                cnctName: this.setCnctName,
                cnctDesc: this.setCnctDesc,
                clickThroughMdContext: this.setClickThroughMdContext,
                z: this.setZ
            },setter),
            $.extend({
                cnctId: this.getCnctId,
                classId: this.getClassId,
                cnctName: this.getCnctName,
                cnctDesc: this.getCnctDesc,
                clickThroughMdContext: this.getClickThroughMdContext,
                z: this.getZ
            },getter)
        );

        this.deleteable = true;
        this.id = String(this.classId) + ':' + String(this.cnctId);

        if(this.clickThroughMdContext == null){
            this.clickThroughMdContext = {};
        }
        else{
            try{
                this.clickThroughMdContext = JSON.parse(this.clickThroughMdContext);
            } catch{
                console.log("WARNING! Invalid click through MD context");
            }
        }
    },

    setCnctId: function(cnctId){
        this.cnctId = cnctId;
    },

    getCnctId: function(){
        return(this.cnctId);
    },

    setClassId: function(classId){
        this.classId = classId;
    },

    getClassId: function(){
        return(this.classId);
    },

    setCnctDesc: function(cnctDesc){
        this.cnctDesc = cnctDesc;
    },

    getCnctDesc: function(){
        return(this.cnctDesc);
    },

    setCnctName: function(cnctName){
        this.cnctName = cnctName;
    },

    getCnctName: function(){
        return(this.cnctName);
    },

    setClickThroughMdContext: function(clickThroughMdContext){
        this.clickThroughMdContext = clickThroughMdContext;
    },

    getClickThroughMdContext: function(){
        return(this.clickThroughMdContext);
    },

    setZ: function(z){
        this.z = z;
    },

    getZ: function(){
        return(this.z);
    }
});
