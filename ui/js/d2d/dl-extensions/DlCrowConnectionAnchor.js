DlCrowConnectionAnchor = draw2d.layout.anchor.ConnectionAnchor.extend({

    NAME: "DlCrowConnectionAnchor",

    init: function(owner)
    {
        this._super(owner);

    },

    getLocation: function(reference, inquiringConnection)
    {
        const thisPortIds = this.getOwner().getId().split(":");
        let source = true;
        let otherPortIds;
        if(this.getOwner().getId().toString() === inquiringConnection.getSource().getId().toString()){
            otherPortIds = inquiringConnection.getTarget().getId().split(":");
        }else{
            otherPortIds = inquiringConnection.getSource().getId().split(":");
            source = false;
        }

        const thisTable = this.getOwner().getCanvas().getFigure(thisPortIds[0]+":"+thisPortIds[1]);
        const otherTable = this.getOwner().getCanvas().getFigure(otherPortIds[0]+":"+otherPortIds[1]);

        let offset = 15;

        if(thisTable.getAbsoluteX() < otherTable.getAbsoluteX()+(otherTable.getWidth()/2)){
            if(inquiringConnection.NAME === "DlPdmDiagramConnector"){
                if(source){
                    inquiringConnection.setSourceSide(180);
                }
                else{
                    inquiringConnection.setTargetSide(180);
                    offset *= -1;
                }
            }
            return new draw2d.geo.Point(thisTable.getAbsoluteX()+thisTable.getWidth()+20, thisTable.getAbsoluteY()+thisTable.getHeight()/2 + offset);
        }
        else{
            if(inquiringConnection.NAME === "DlPdmDiagramConnector"){
                if(source){
                    inquiringConnection.setSourceSide(0);
                }
                else{
                    inquiringConnection.setTargetSide(0);
                    offset *= -1;
                }
            }
            return new draw2d.geo.Point(thisTable.getAbsoluteX()-20, thisTable.getAbsoluteY()+thisTable.getHeight()/2 + offset);
        }

    }

});