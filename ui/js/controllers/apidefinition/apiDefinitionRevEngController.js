/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the API Definition Reverse Engineer modal dialog
 * TODO: The only API reverse engineering currently supported is REST from OpenAPI3. Update so that multiple specifications can be supported.
 */
function ApiDefinitionRevEngController ($scope, $stateParams, NavigationChainService, DTOptionsBuilder,
                                        notify, RbacService, HttpService) {
    const UNRECONCILED = 0;
    const MATCHING = 1;
    const MATCHED = 2;
    const DELETED = 3;

    $scope.gridData = [];
    $scope.latestRevEngRun = {"revEngRunId": 0, "revEngRunResult": 0};
    $scope.reconcileReport = {};
    $scope.valid = true;
    $scope.spinnerLoading = "";
    $scope.invocationParams = {};
    $scope.config = {'reconciliationRequired': false, 'btnlock': false, 'matchmode': false};

    $scope.matchElement = {source: {objectName: "None Selected"}, target: {}};
    
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('language', {'sEmptyTable': 'No previous reverse engineer runs'} )
        .withOption('order', [[0, 'desc']]);

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('language', {'sEmptyTable': 'No elements requiring reconciliation'} );

    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: Initialises the controller.
     */
    $scope.initApiDefinitionRevEng = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.getRevEngRunHistory();
        $scope.getRevEngReconcileReport();
    }

    /**
     * Description: retrieves the reverse engineer run history for this API.
     */
    $scope.getRevEngRunHistory = function() {
        const GET_URL = apiHost + "/apidefinition/rest/reverse-engineer/history/" + $scope.invocationParams.parentObjectId + "/" + $scope.invocationParams.parentClassId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                        if($scope.gridData.length > 0) {
                            let latestRevEngRun = $scope.gridData[$scope.gridData.length - 1];
                            $scope.latestRevEngRun.revEngRunId = latestRevEngRun.revEngRunId;
                            $scope.latestRevEngRun.revEngRunResult = latestRevEngRun.revEngRunResult;
                            if ($scope.latestRevEngRun.revEngRunResult == 1) $scope.config.reconciliationRequired = true;
                        }
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        notify({
                            message: "There was an error when trying to retrieve the reverse engineer run history.",
                            classes: "alert-danger",
                            duration: 5000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: retrieves the reconciliation report for the latest reverse engineer run.
     */
    $scope.getRevEngReconcileReport = function() {
        const GET_URL = apiHost + "/apidefinition/rest/reverse-engineer/reconcile/" + $scope.invocationParams.parentObjectId + "/" + $scope.invocationParams.parentClassId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        if (response.data.result_data.latest_run_result == 1) {
                            $scope.reconcileReport = response.data.result_data;
                        }
                        else {
                            $scope.reconcileReport = {"deleted":[], "added":[]};
                        }
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        notify({
                            message: "There was an error when trying to retrieve the reverse engineer reconciliation report.",
                            classes: "alert-danger",
                            duration: 5000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: executes a reconciliation for any deleted elements that have reconciliationStatus == MATCHED or DELETED
     */
    $scope.reconcileRevEngRun = function() {
        let reconcileList = [];
        let status = 0;

        for (let i = 0; i < $scope.reconcileReport.deleted.length; i++) {
            let deletedElement = $scope.reconcileReport.deleted[i];
            if (deletedElement.reconciliationStatus == 2 || deletedElement.reconciliationStatus == 3) {
                reconcileList.push(deletedElement);
                status++;
            }
        }

        if (status > 0) {
            const POST_URL = apiHost + "/apidefinition/rest/reverse-engineer/reconcile/" + $scope.invocationParams.parentObjectId + "/" + $scope.invocationParams.parentClassId;

            HttpService.call(POST_URL, "POST", reconcileList, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.processReconcileResult(response, 1);
                        }
                    },
                    {
                        status: 2,
                        callback: function(response){
                            $scope.processReconcileResult(response, 2);
                        }
                    },
                    {
                        status: -1,
                        callback: function(response) {
                            notify({
                                message: "There was an error while reconciling the API reverse engineer run.",
                                classes: "alert-danger",
                                duration: 5000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: Processes the result of a reverse engineer reconciliation action
     * @param response
     */
    $scope.processReconcileResult = function(response, resultStatus) {
        let msg = "";
        if (resultStatus == 1) {
            msg = "The reverse engineer reconciliation action completed successfully. Further reconciliation is required.";
            $scope.config.reconciliationRequired = true;
        }
        else if (resultStatus == 2) {
            msg = "The reverse engineer reconciliation action completed successfully. The reverse engineer run if fully reconciled.";
            $scope.config.reconciliationRequired = false;
        }

        $scope.getRevEngRunHistory();

        notify({
            message: msg,
            classes: "alert-success",
            duration: 5000,
            templateUrl: $scope.notifyTemplate
        });

        $scope.getRevEngReconcileReport();
    }

    /**
     * Description: Check that the file selected by the user is valid.
     */
    $scope.checkValid = function(){
        if(document.getElementById('inputFile') && (document.getElementById('inputFile')).files[0]){
            const inputFile = (document.getElementById('inputFile')).files[0];
            if(inputFile && inputFile.name){
                $scope.valid = true;
                $scope.executeRevEngRun(inputFile);
            }
        }
        else {
            $scope.valid = false;
        }

    }

    /**
     * Description: executes the reverse engineer run by invoking the DL API.
     */
    $scope.executeRevEngRun = function(inputFile) {
        $scope.setSpinnerLoading(true);
        const formData = new FormData();
        formData.append('file', inputFile);

        const POST_URL = apiHost + "/apidefinition/rest/reverse-engineer/" + $scope.invocationParams.parentObjectId;
        const headers = {'Content-Type': undefined};

        HttpService.call(POST_URL, "POST", formData, headers,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.getRevEngRunHistory();
                        $scope.getRevEngReconcileReport();
                        $scope.setSpinnerLoading(false);
                        notify({
                            message: "The reverse engineer run completed. RECONCILIATION is required for one or more deleted elements.",
                            classes: "alert-warning",
                            duration: 5000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: 2,
                    callback: function(response){
                        $scope.getRevEngRunHistory();
                        $scope.setSpinnerLoading(false);
                        notify({
                            message: "The reverse engineer run completed. No reconciliation is required.",
                            classes: "alert-success",
                            duration: 5000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        $scope.reverseEngineerRunFailure(response);
                        $scope.setSpinnerLoading(false);
                    }
                }
            ]
        );

    }

    /**
     * Description: Notify the user the the reverse engineering run failed.
     * @param response
     */
    $scope.reverseEngineerRunFailure = function(response) {
        $uibModalInstance.close();
        $scope.setSpinnerLoading("");

        let msg = "Error: " + response.data.result_data.generalErrorMessage + ". Error number: " + response.data.result_data.errorNumber;

        notify({
            message: msg,
            classes: "alert-danger",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: turns the spinner on/off and
     * @param value
     */
    $scope.setSpinnerLoading = function(value){
        $scope.spinnerLoading = value ? "sk-loading" : "";
        $scope.config.btnlock = value;
    }

    /**
     * Description: prepares a deleted element to be matched with an added TableView for reconciliation
     * @param deletedElement
     */
    $scope.matchToAdded = function (deletedElement) {
        $scope.matchElement.source = deletedElement;
        deletedElement.reconciliationStatus = MATCHING;
        $scope.config.matchmode = true;
    }

    /**
     * Description: associates an added element with a deleted element
     * @param addedColumn
     */
    $scope.matchToDeleted = function (addedColumn) {
        $scope.matchElement.source.reconciledObjectId = addedColumn.objectId;
        $scope.matchElement.source.reconciledClassId = addedColumn.classId;
        $scope.matchElement.source.reconciledObjectName = addedColumn.parentObjectName + "." + addedColumn.objectName;
        $scope.matchElement.source.reconciliationStatus = MATCHED;
        $scope.config.matchmode = false;
    }

    /**
     * Description: reset migration matching for identified deleted element
     * @param deletedElement
     */
    $scope.clearMatch = function (deletedElement) {
        $scope.matchElement.source = {objectName: "None Selected"};
        deletedElement.reconciledObjectId = 0;
        deletedElement.reconciledClassId = 0;
        deletedElement.reconciledObjectName = "";
        deletedElement.reconciliationStatus = UNRECONCILED;
        $scope.config.matchmode = false;
    }

    /**
     * Description: prepares a deleted Column to be matched with an added Column for reconciliation
     * @param deletedElement
     */
    $scope.acceptDeleted = function (deletedElement) {
        $scope.matchElement.source = {objectName: "None Seleted"};
        deletedElement.reconciliationStatus = DELETED;
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ApiDefinitionRevEngController', ApiDefinitionRevEngController);
