/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: controller for the create_rest_element.html modal dialog
 */
function CreateRestElementController ($scope, $uibModalInstance) {
    $scope.newElementSpec = {
        name: "",
        type: $scope.newElementConfig.elementType,      // $scope.newElementConfig is defined in the parent Controller
        shortdesc: "",
        httpMethod: {httpMethodName: "None"},
        responseCode: 0,
        topLevelElement: $scope.newElementConfig.topLevelElement
    };

    $scope.validation = {
        name: true,
        type: true
    }

    $scope.httpMethodList = [
        {httpMethodName: "CONNECT"},
        {httpMethodName: "DELETE"},
        {httpMethodName: "GET"},
        {httpMethodName: "HEAD"},
        {httpMethodName: "OPTIONS"},
        {httpMethodName: "PATCH"},
        {httpMethodName: "POST"},
        {httpMethodName: "PUT"},
        {httpMethodName: "TRACE"}
    ];

    /**
     * Saves the new element in the DL database by invoking $scope.saveNewElement which is defined in the parent controller
     */
    $scope.create = function () {
        $scope.saveNewElement($scope.newElementSpec);    // this function is in the controller that invoked the modal dialog
        $uibModalInstance.close();
    };

    /**
     * Check that all mandatory fields have been populated.
     */
    $scope.checkValid = function(){
        if ($scope.newElementSpec.type == "path") {
            $scope.validation.name = !!($scope.newElementSpec.name && $scope.newElementSpec.name.length > 0);
        }
        else
            $scope.validation.name = true;

        $scope.validation.type = !!($scope.newElementSpec.type && $scope.newElementSpec.type.length > 0);

        if($scope.validation.name && $scope.validation.type){
            $scope.create();
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.setState = function(elementType) {

        switch (elementType) {
            case "path":
                break;
            case "method":
                $scope.newElementSpec.topLevelElement=false
                $scope.newElementSpec.name = "";
                break;
            case "response":
                $scope.newElementSpec.topLevelElement=false
                $scope.newElementSpec.name = "";
                break;
        }

    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateRestElementController', CreateRestElementController);
