/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Format Summary view.
 *
 */
function apiRestSummaryController($scope, $state, $stateParams, $window, $uibModal, NavigationChainService, notify,
                                  DTOptionsBuilder, HttpService, RbacService, MoClassService, DetailsClickThroughService, UtilityService) {
    $scope.summary = {
        objectId: 0,
        classId: 0,
        className: "",
        parentObjectId: 0,
        parentClassId: 0,
        objectName: "",
        objectShortDesc: ""
    };
    $scope.summaryUndo = {};
    $scope.associatedObjects = [];

    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Format Associations'},
        ]);

    $scope.disable = {
        coreDetails: true,
        associations: true
    }

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialisation function for the controller
     */
    $scope.initSummary = function() {
        $scope.perms.create = !RbacService.checkRbac('dmf','create');
        $scope.perms.update = !RbacService.checkRbac('dmf','update');
        $scope.perms.delete = !RbacService.checkRbac('dmf','delete');
        $scope.perms.publish = !RbacService.checkRbac('dmf','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.summary.objectId = $scope.invocationParams.objectId;
        $scope.summary.classId = $scope.invocationParams.classId;
        $scope.summary.abbreviation = ($scope.summary.abbreviation === 1) ? 0 : 1;
        $scope.readSummary();
    }

    $scope.readSummary = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/apidefinition/rest/content/' + $scope.summary.objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.summary.objectName = response.data.result_data.api_summary.objectName;
                        $scope.summary.objectShortDesc = response.data.result_data.api_summary.objectShortDesc;
                        $scope.summary.className = response.data.result_data.api_summary.className;
                        $scope.associatedObjects = response.data.result_data.associations;
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: enables fields in selected node display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.formatSummary, $scope.formatSummaryUndo);

            $scope.disable.coreDetails = false;
        }
        else if (dlIboxName === "associations") {
            $scope.disable.associations = false;
        }
    }

    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            $scope.saveSummaryCoreDetails();
            $scope.disable.coreDetails = true;
        }
        else if (dlIboxName === "associations") {
            $scope.disable.associations = true;
        }
    }

    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.formatSummaryUndo, $scope.formatSummary);
            $scope.disable.coreDetails = true;
        }
        else if (dlIboxName === "associations") {
            $scope.disable.associations = true;
        }
    }

    /**
     * Description: saves a updated XML node core details to the database
     */
    $scope.saveSummaryCoreDetails = function() {
        const summaryUpdate = {
            objectId: $scope.summary.objectId,
            classId: $scope.summary.classId,
            objectName: $scope.summary.objectName,
            objectShortDesc: $scope.summary.objectShortDesc
        };
        $scope.coreDetailsDisabled = true;

        const POST_URL = apiHost + "/apidefinition/summary";
        HttpService.call(POST_URL, "POST", summaryUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The summary was updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: deletes the format definition after the user confirms the delete action
     */
    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/apidefinition/" + $scope.summary.objectId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "API successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.newAssociation = function() {

        NavigationChainService.storeCurrentState($scope.invocationParams);

        const invocationParams = {
            stateName:"associations.new_association",
            objectId:$scope.summary.objectId,
            classId:$scope.summary.classId,
            stateParams:{
                objectClass:$scope.summary.className,
                objectContext:$scope.summary.objectName
            }
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: opens the clicks through to the object details state for the object defined by class id and object id
     */
    $scope.clickThrough = function(objectId, classId) {
        NavigationChainService.storeCurrentState($scope.invocationParams);
        DetailsClickThroughService.clickThrough(objectId, classId);
    }

    /**
     * Description: opens the file store detail state for the API
     */
    $scope.viewDetails = function() {
        NavigationChainService.storeCurrentState($scope.invocationParams);

        const invocationParams = {
            stateName: "smd.api_rest_detail",
            objectId: 0,
            classId: 0,
            parentObjectId: $scope.summary.objectId,
            parentClassId: $scope.summary.classId,
            stateParams:{activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('apiRestSummaryController', apiRestSummaryController);
