/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Message Formats List
 */
function DataSensitivityFullReportController($scope, $rootScope, $state, $window, $uibModal, NavigationChainService, notify,
                                         DTOptionsBuilder, DTColumnBuilder, RbacService, HttpService, UtilityService,
                                         DataSensitivityService) {
    $scope.fullDataSensitivityReport = [];
    $scope.comment = {};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'csv', title: 'DataSensitivityTagList'}
        ])
        .withOption('aaSorting', []);

    $scope.dsLevelCode = {'low':'01', 'medium':'02', 'high':'03', 'critical':'04'};
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initDataSensitivityFullReport = function () {
        $scope.readFullDsReport();
    }

    /**
     * Description: reads the full list of data sensitivity tags from the DL DB
     */
    $scope.readFullDsReport = function() {
        const GET_URL = apiHost + '/common/datasensitivity/report/full';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.fullDataSensitivityReport = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Returns the CSS Class for the specified data sensitivity by invoking the DataSensitivityService.
     * @param dataSensitivityLevel
     * @returns {*}
     */
    $scope.getDataSensitivityCssClass = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DataSensitivityFullReportController', DataSensitivityFullReportController);
