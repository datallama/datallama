/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/**
 * Description: modal dialog for selecting a single data sensitivity tag to be assigned to an md_object
 * TODO: change name to CmnAssignDataSensitivitySingleConstructor
 */
function CmnAssignDataSensitivitySingle ($scope, $uibModalInstance, HttpService, DataSensitivityService, currentDsTagId) {
    $scope.currentDsTagId = angular.copy(currentDsTagId);
    $scope.selectedDsTag = {dlDsTagId:0};
    $scope.dataSensitivityTagList = [];

    $scope.validation = {
        name: true,
        class: true
    }

    /**
     * Description: initialises the CmnAssignDataSensitivitySingle by retrieving the list of data sensitivity tags from the DL DB.
     */
    $scope.initAssignDataSensitivitySingle = function () {
        const GET_URL = apiHost + "/common/datasensitivity/tag/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.dataSensitivityTagList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: Returns the CSS Class for the specified data sensitivity by invoking the DataSensitivityService.
     * @param dataSensitivityLevel
     * @returns {*}
     */
    $scope.getDsCssClass = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: update $scope.selectedDsTag every time the user clicks on an option button.
     * @param selectedDsTag
     */
    $scope.setSelectedDsTag = function(selectedDsTag) {
        $scope.selectedDsTag = selectedDsTag;
    }

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.assign = function () {
        if($scope.selectedDsTag.dlDsTagId == 0) {
            $scope.selectedDsTag = {
                dlDsTagId: 0,
                dlDsLevel: "none",
                dlDsName: "None",
                dlDsShortDesc: "None"
            }
        }
        $scope.updateDataSensitivityTag($scope.selectedDsTag);
        $uibModalInstance.close();

    };

    /**
     * Description: closes the dialog without creating a new File/Object Store
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnAssignDataSensitivitySingle', CmnAssignDataSensitivitySingle);
