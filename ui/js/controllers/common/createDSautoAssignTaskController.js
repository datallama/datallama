/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Message Formats List
 */
function CreateDsAutoAssignTaskController($scope, $rootScope, $state, $window, $uibModal, NavigationChainService, notify,
                                         DTOptionsBuilder, DTColumnBuilder, RbacService, HttpService, UtilityService,
                                         DataSensitivityService) {
    $scope.dsAutoAssignTaskDetails = {
        taskName: "", taskShortDesc: "", dsTag: {}, manualExec: true,
        objNameIncluded: "", objNameExcluded: "", datatypeIncluded: "",
        classCollection: {tvcol: false, presff: false, msgff: false, ffff: false}
    };
    $scope.dsTagList = [];

    $scope.perms = {create: false, update: false, delete: false, publish: false, configure: false};
    $scope.validation = {taskName: true, dsTag: true, objNameIncluded: true, datatype: true, classCollection: true};
    $scope.createDisabled = false;

    $scope.editingDisabled01 = true;
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('aaSorting', []);

    $scope.dsLevelCode = {'low':'01', 'medium':'02', 'high':'03', 'critical':'04'};
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initCreateDsAutoAssignTask = function () {
        $scope.perms.create = !RbacService.checkRbac('ont','create');
        $scope.perms.update = !RbacService.checkRbac('ont','update');
        $scope.perms.delete = !RbacService.checkRbac('ont','delete');
        $scope.perms.publish = !RbacService.checkRbac('ont','publish');
        $scope.perms.publish = !RbacService.checkRbac('ont','configure');

        $scope.readDsTagList();
    }

    /**
     * Description: reads the full list of data sensitivity tags from the DL DB
     */
    $scope.readDsTagList = function() {
        const GET_URL = apiHost + '/common/datasensitivity/tag/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        let dsTagList = response.data.result_data;

                        $scope.dsTagList = [];

                        // add the elementSelected attribute to each tag
                        for (var i = 0; i < dsTagList.length; i++) {
                            let dsTag = dsTagList[i];
                            dsTag.elementSelected = 0;
                            dsTag.dsLevelCode = $scope.dsLevelCode[dsTag.dlDsLevel];
                            $scope.dsTagList.push(dsTag);
                        }
                    }
                }
            ]
        );
    }

    /**
     * Returns the CSS Class for the specified data sensitivity by invoking the DataSensitivityService.
     * @param dataSensitivityLevel
     * @returns {*}
     */
    $scope.getDataSensitivityCssClass = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteMdObjectType) {
        let check = 0;

        // if the mdObject type == 'element', check that an element is selected
        if($scope.selectedElement == null || $scope.selectedElement.dlDsTagId <= 0) {
            check = 1;
            const errorMessage = "You have not selected a data sensitivity tag.";
            notify({ message: errorMessage,
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate});
        }

        if (check == 0) {
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/data_sensitivity_confirm_delete_tag.html'),
                controller: 'DataSensitivityConfirmDeleteTagController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.editingDisabled01 = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Validate the users input for mandatory fields.
     * If validation passes, invoke $scope.saveNewAutoAssignTask()
     */
    $scope.validateInput = function() {

        let valid = 0;
        $scope.validation = {taskName: true, dsTag: true, objNameIncluded: true, datatype: true, classCollection: true};

        // 1.1. validate all mandatory fields
        if ($scope.dsAutoAssignTaskDetails.taskName.length === 0) {
            $scope.validation.taskName = false;
            valid++;
        }

        if (!$scope.dsAutoAssignTaskDetails.dsTag.hasOwnProperty("dlDsTagId")) {
            $scope.validation.dsTag = false;
            valid++;
        }

        if ($scope.dsAutoAssignTaskDetails.objNameIncluded.length === 0) {
            $scope.validation.objNameIncluded = false;
            valid++;
        }

        if ($scope.dsAutoAssignTaskDetails.datatypeIncluded.length === 0) {
            $scope.validation.datatype = false;
            valid++;
        }

        ccValid = false;
        for(var x in $scope.dsAutoAssignTaskDetails.classCollection) {
            if($scope.dsAutoAssignTaskDetails.classCollection[x])
                ccValid = true;
        }
        $scope.validation.classCollection = ccValid;

        if (valid == 0)
            $scope.saveNewAutoAssignTask();
    }

    /**
     * Description: Save new data sensitivity tag to the DL DB
     */
    $scope.saveNewAutoAssignTask = function () {
        let classList = {
            class_id_list: [],
            selection_method: "collection",
            class_collections: []
        }
        if ($scope.dsAutoAssignTaskDetails.classCollection.tvcol) classList.class_collections.push("TVCOL");
        if ($scope.dsAutoAssignTaskDetails.classCollection.presff) classList.class_collections.push("PRESFF");
        if ($scope.dsAutoAssignTaskDetails.classCollection.msgff) classList.class_collections.push("MSGFF");
        if ($scope.dsAutoAssignTaskDetails.classCollection.ffff) classList.class_collections.push("FFFF");

        let newDsAutoAssignTask = {
            taskId: 0,
            taskName: $scope.dsAutoAssignTaskDetails.taskName,
            taskShortDesc: $scope.dsAutoAssignTaskDetails.taskShortDesc,
            dsTag: $scope.dsAutoAssignTaskDetails.dsTag,
            classList: classList,
            invocationMode: 0,
            objectNameMatchInclude: $scope.dsAutoAssignTaskDetails.objNameIncluded,
            objectNameMatchExclude: $scope.dsAutoAssignTaskDetails.objNameExcluded,
            datatypeMatch: $scope.dsAutoAssignTaskDetails.datatypeIncluded,
            scheduleDefn: "{}"
        };

        newDsAutoAssignTask.invocationMode = ($scope.dsAutoAssignTaskDetails.manualExec) ? 1 : 2;

        const POST_URL = apiHost + "/common/datasensitivity/autoassigntask";

        HttpService.call(POST_URL, "POST", newDsAutoAssignTask, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "New data sensitivity auto assignment task saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                },
                {
                    status: -1062,
                    callback: function(response) {
                        notify({
                            message: "A data sensitivity auto assignment task with the same name already exists.",
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: Deletes the currently selected data sensitivity auto assignment task after confirmation from user.
     */
    $scope.deleteDSautoAssignTask = function () {
        const DELETE_URL = apiHost + "/common/datasensitivity/tag/" + $scope.selectedElement.dlDsTagId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {

                        notify({
                            message: "Data sensitivity tag details successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readDsTagList();
                    }
                }
            ]
        );

    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateDsAutoAssignTaskController', CreateDsAutoAssignTaskController);
