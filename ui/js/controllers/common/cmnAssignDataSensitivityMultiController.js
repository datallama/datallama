/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Description: modal dialog for selecting multiple data sensitivity tags to be assigned to an md_object
 *  The structure of each selected tag object is {dlDsTagId:0}
 */
function CmnAssignDataSensitivityMultiController ($scope, $uibModalInstance, HttpService, DataSensitivityService,
                                                  currentDsTagList) {
    $scope.selectedDsTags = [];
    $scope.dataSensitivityTagList = [];

    $scope.validation = {
        name: true,
        class: true
    }

    /**
     * Description: initialises the CmnAssignDataSensitivitySingle by retrieving the list of data sensitivity tags from the DL DB.
     */
    $scope.initAssignDataSensitivityMulti = function () {
        $scope.selectedDsTags = angular.copy(currentDsTagList);

        const GET_URL = apiHost + "/common/datasensitivity/tag/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.dataSensitivityTagList = response.data.result_data;
                        $scope.setExistingSelectedDsTags();
                    }
                }
            ]
        );
    }

    /**
     * Description: Sets DS tags in the full list to selected and maybe disabled based on the existing DS tag
     *  assignments listed in $scope.selectedDsTags
     */
    $scope.setExistingSelectedDsTags = function() {

        // 1. add selected and createMode elements to all DS tags.
        for (let k = 0; k < $scope.dataSensitivityTagList.length; k++) {
            $scope.dataSensitivityTagList[k].selected = 0;
            $scope.dataSensitivityTagList[k].createMode = 0;
        }

        // 2. check DS tags that are already defined in the DL DB for this md object
        for (let i = 0; i < $scope.dataSensitivityTagList.length; i++) {
            // check if tag is in $scope.selectedDsTags
            for (let j = 0; j < $scope.selectedDsTags.length; j++) {
                if ($scope.dataSensitivityTagList[i].dlDsTagId == $scope.selectedDsTags[j].dlDsTagId) {
                    $scope.dataSensitivityTagList[i].selected = 1;
                    $scope.dataSensitivityTagList[i].createMode = $scope.selectedDsTags[j].createMode;
                    break;
                }
            }
        }
    }

    /**
     * Description: Returns the CSS Class for the specified data sensitivity by invoking the DataSensitivityService.
     * @param dataSensitivityLevel
     * @returns {*}
     */
    $scope.getDsCssClass = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: update $scope.selectedDsTag every time the user clicks on an option button.
     * @param selectedDsTags
     */
    $scope.updateDsTagSelection = function(selectedDsTag) {
        selectedDsTag.createMode = 10;

        // 1. toggle status of selectedDsTag in the $scope.selectedDsTags array
        let tagFound = 0;
        for (let i = 0; i < $scope.selectedDsTags.length; i++) {
            if ($scope.selectedDsTags[i].dlDsTagId == selectedDsTag.dlDsTagId) {
                //$scope.selectedDsTags.splice(i, 1);
                $scope.selectedDsTags[i].createMode = $scope.selectedDsTags[i].createMode * -1;
                tagFound = 1;
                break;
            }
        }
        if (tagFound == 0) {
            $scope.selectedDsTags.push(selectedDsTag);
        }
    }

    /**
     * Description: invokes the saveDataSensitivityMulti function in parent scope passing the array of selected
     *  data sensitivity tags as parameter
     */
    $scope.assign = function () {
        $scope.saveDataSensitivityMulti($scope.selectedDsTags);
        $uibModalInstance.close();

    };

    /**
     * Description: closes the dialog without creating a new File/Object Store
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnAssignDataSensitivityMultiController', CmnAssignDataSensitivityMultiController);
