/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for opening data model diagrams
 */
function CmnSelectOntologyListController ($scope, $uibModalInstance, notify, DTOptionsBuilder, HttpService) {
    $scope.ontologyLists = [];
    $scope.selectedOntologyList = {
        listCdgId: 0,
        listCdgName: ""
    }

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    /**
     * Description: get the list of diagrams from the DB
     */
    $scope.initSelectOntologyList = function () {

        const GET_URL = apiHost + "/ontology/listcdg/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.ontologyLists = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: set the customFormId and customFormName based on the custom form selected in the table
     */
    $scope.setSelectedOntologyList = function (listCdgId, classId, listCdgName) {
        $scope.selectedOntologyList.listCdgId = listCdgId;
        $scope.selectedOntologyList.classId = classId;
        $scope.selectedOntologyList.listCdgName = listCdgName;
        $scope.selectedOntologyList.enumAssocShortDesc = "";
    }

    /**
     * Description:
     */
    $scope.ok = function () {
        if($scope.selectedOntologyList.listCdgId > 0){
            // send selected form details to parent
            $scope.saveSelectedOntologyList($scope.selectedOntologyList);
            $uibModalInstance.close();
        } else{
            const msg = "Please select an Ontology List.";
            notify({
                message: msg,
                classes: "alert-warning",
                templateUrl: $scope.notifyTemplate
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnSelectOntologyListController', CmnSelectOntologyListController);
