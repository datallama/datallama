/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Description: Controller for Data Sensitivity Auto Assignment Task Detail view
 */

function DsAutoAssignTaskDetailController($scope, $window, $state, $stateParams, $uibModal, notify,
                                         DTOptionsBuilder, DTColumnBuilder, RbacService, HttpService,
                                         NavigationChainService, DataSensitivityService) {
    $scope.dsAutoAssignTaskId = 0;
    $scope.dsAutoAssignTaskDetails = {
        taskId: 0, taskName: "", taskShortDesc: "", dsTag: {},
        objectNameMatchInclude: "", objectNameMatchExclude: "", datatypeMatch: "",
        classList: {class_id_list: [], selection_method: "", class_collections: []},
        manualExec: true,
        classCollectionDisplay: {tvcol: false, presff: false, msgff: false, ffff: false},
        overwrite: false
    };
    $scope.dsAutoAssignTaskDetailsUndo = {};
    $scope.dsTagList = [];
    $scope.editingDisabled = true;
    $scope.validation = {taskName: true, dsTag: true, objNameIncluded: true, datatype: true, classCollection: true};
    $scope.perms = {};

    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initDsAutoAssignTaskDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('ont','create');
        $scope.perms.update = !RbacService.checkRbac('ont','update');
        $scope.perms.delete = !RbacService.checkRbac('ont','delete');
        $scope.perms.publish = !RbacService.checkRbac('ont','publish');
        $scope.perms.publish = !RbacService.checkRbac('ont','configure');

        $scope.dsAutoAssignTaskId = $stateParams.invocationParams.objectId;
        $scope.dsAutoAssignTaskDetails.taskId = $stateParams.invocationParams.objectId;
        $scope.readDsAutoAssignTaskDetail();
    }

    /**
     * Description: reads details of the data sensitivity auto assignment task identified by dsAutoAssignTaskId
     */
    $scope.readDsAutoAssignTaskDetail = function() {
        const GET_URL = apiHost + '/common/datasensitivity/autoassigntask/' + $scope.dsAutoAssignTaskId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.processTaskDetailResults(response.data.result_data);
                    }
                }
            ]
        );

    }

    /**
     * Description: Processes results from the API call /common/datasensitivity/autoassigntask/
     * @param resultData
     */
    $scope.processTaskDetailResults = function(resultData) {
        // 1. populate the dsAutoAssignTaskDetails object with results
        $scope.dsAutoAssignTaskDetails.dsTag = resultData.dsAutoAssignTask.dsTag;
        $scope.dsAutoAssignTaskDetails.taskName = resultData.dsAutoAssignTask.taskName;
        $scope.dsAutoAssignTaskDetails.taskShortDesc = resultData.dsAutoAssignTask.taskShortDesc;
        $scope.dsAutoAssignTaskDetails.invocationMode = resultData.dsAutoAssignTask.invocationMode;
        if ($scope.dsAutoAssignTaskDetails.invocationMode == 2)
            $scope.dsAutoAssignTaskDetails.manualExec = false;
        $scope.dsAutoAssignTaskDetails.objectNameMatchInclude = resultData.dsAutoAssignTask.objectNameMatchInclude;
        $scope.dsAutoAssignTaskDetails.objectNameMatchExclude = resultData.dsAutoAssignTask.objectNameMatchExclude;
        $scope.dsAutoAssignTaskDetails.datatypeMatch = resultData.dsAutoAssignTask.datatypeMatch;
        $scope.dsAutoAssignTaskDetails.classList = resultData.dsAutoAssignTask.classList;
        if ($scope.dsAutoAssignTaskDetails.classList.class_collections.includes("TVCOL"))
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.tvcol = true;
        if ($scope.dsAutoAssignTaskDetails.classList.class_collections.includes("PRESFF"))
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.presff = true;
        if ($scope.dsAutoAssignTaskDetails.classList.class_collections.includes("MSGFF"))
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.msgff = true;
        if ($scope.dsAutoAssignTaskDetails.classList.class_collections.includes("FFFF"))
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.ffff = true;
        $scope.dsAutoAssignTaskDetails.overwrite = resultData.dsAutoAssignTask.overwrite;

        $scope.dsTagList = resultData.dsTagList;
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "task_details") {
            // capture all field values for undo of edit
            angular.copy($scope.dsAutoAssignTaskDetails, $scope.dsAutoAssignTaskDetailsUndo);
            $scope.editingDisabled = false;
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        let valid = 0;
        if (dlIboxToolsName === "task_details") {
            valid = $scope.validateInput();
        }
        return valid;
    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "task_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.dsAutoAssignTaskDetailsUndo, $scope.dsAutoAssignTaskDetails);
            $scope.editingDisabled = true;
            $scope.validation = {taskName: true, dsTag: true, objNameIncluded: true, datatype: true, classCollection: true};
        }
    }

    /**
     * Validate the users input for mandatory fields.
     * If validation passes, invoke $scope.saveNewAutoAssignTask()
     */
    $scope.validateInput = function() {
        let valid= 0;
        $scope.validation = {taskName: true, dsTag: true, objNameIncluded: true, datatype: true, classCollection: true};

        // 1.1. validate all mandatory fields
        if ($scope.dsAutoAssignTaskDetails.taskName.length === 0) {
            $scope.validation.taskName = false;
            valid++;
        }

        if (!$scope.dsAutoAssignTaskDetails.dsTag.hasOwnProperty("dlDsTagId")) {
            $scope.validation.dsTag = false;
            valid++;
        }

        if ($scope.dsAutoAssignTaskDetails.objectNameMatchInclude.length === 0) {
            $scope.validation.objNameIncluded = false;
            valid++;
        }

        if ($scope.dsAutoAssignTaskDetails.datatypeMatch.length === 0) {
            $scope.validation.datatype = false;
            valid++;
        }

        $scope.validation.classCollection = $scope.dsAutoAssignTaskDetails.classCollectionDisplay.tvcol ||
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.presff ||
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.msgff ||
            $scope.dsAutoAssignTaskDetails.classCollectionDisplay.ffff;

        if ($scope.validation.classCollection == false)
            valid++;

        if (valid == 0) {
            $scope.updateAutoAssignTask();
        }

        return valid;
    }

    /**
     * Description: Saves updates to the data sensitivity auto assignment task
     */
    $scope.updateAutoAssignTask = function () {
        $scope.editingDisabled = true;

        let classList = {
            class_id_list: [],
            selection_method: "collection",
            class_collections: []
        }
        if ($scope.dsAutoAssignTaskDetails.classCollectionDisplay.tvcol) classList.class_collections.push("TVCOL");
        if ($scope.dsAutoAssignTaskDetails.classCollectionDisplay.presff) classList.class_collections.push("PRESFF");
        if ($scope.dsAutoAssignTaskDetails.classCollectionDisplay.msgff) classList.class_collections.push("MSGFF");
        if ($scope.dsAutoAssignTaskDetails.classCollectionDisplay.ffff) classList.class_collections.push("FFFF");

        $scope.dsAutoAssignTaskDetails.invocationMode = $scope.dsAutoAssignTaskDetails.manualExec ? 1 : 2;

        let newDsAutoAssignTask = {
            taskId: $scope.dsAutoAssignTaskId,
            taskName: $scope.dsAutoAssignTaskDetails.taskName,
            taskShortDesc: $scope.dsAutoAssignTaskDetails.taskShortDesc,
            dsTag: $scope.dsAutoAssignTaskDetails.dsTag,
            classList: classList,
            invocationMode: $scope.dsAutoAssignTaskDetails.invocationMode,
            objectNameMatchInclude: $scope.dsAutoAssignTaskDetails.objectNameMatchInclude,
            objectNameMatchExclude: $scope.dsAutoAssignTaskDetails.objectNameMatchExclude,
            datatypeMatch: $scope.dsAutoAssignTaskDetails.datatypeMatch,
            overwrite: $scope.dsAutoAssignTaskDetails.overwrite,
            scheduleDefn: "{}"
        };

        const POST_URL = apiHost + "/common/datasensitivity/autoassigntask";

        HttpService.call(POST_URL, "POST", newDsAutoAssignTask, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Changes to the data sensitivity auto assignment task saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1062,
                    callback: function(response) {
                        notify({
                            message: "A data sensitivity auto assignment task with the same name already exists.",
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: runs the Data Sensitivity Auto Assignment Task in response to user action.
     */
    $scope.executeDsAutoAssignTask = function() {
        const POST_URL = apiHost + "/common/datasensitivity/runautoassign/" + $scope.dsAutoAssignTaskId;

        HttpService.call(POST_URL, "POST", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The data sensitivity auto assignment task was executed successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        notify({
                            message: response.result_data.error_details,
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsAutoAssignTaskDetailController', DsAutoAssignTaskDetailController);
