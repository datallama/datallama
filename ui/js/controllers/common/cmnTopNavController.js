/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Controller for actions initiated from the common Top Navigation bar
 */
function CmnTopNavController($scope, $rootScope, $location, $uibModal, notify,
                             MoClassService, HttpService, LogoutService, UtilityService) {
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: loads core reference data used by the Data Llama app
     */
    $scope.initCmnTopNav = function() {
        $scope.loadMoClassService();
    }

    /**
     * Description: loads the constant identifiers for all metadata object classes as AngularJS constants
     */
    $scope.loadMoClassService = function() {
        const GET_URL = apiHost + '/ref/class/constants';
        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.successCallback(response);
                    }
                }
            ]
        );
    }

    $scope.successCallback = function(response){
        const classList = response.data.result_data;
        for (let i = 0; i < classList.length; i++) {
            MoClassService.addMoClass(classList[i]);
        }
    }

    /**
     * Description: logs out the user from Data Llama
     */
    $scope.logout = function () {
        HttpService.setUserId(null);
        LogoutService.logout("USERACTION");
    }

    /**
     * Description: opens the change password modal dialog
     */
    $scope.openChangePasswordDialog = function () {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/change_password.html'),
            controller: 'CmnChangePasswordController',
            size: 'md',
            scope: $scope
        });

    }
}
/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnTopNavController', CmnTopNavController);
