/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for opening data model diagrams
 */
function CmnOpenDiagramController ($scope, $uibModalInstance, $uibModal, notify,
                                   DTOptionsBuilder, HttpService) {

    $scope.diagramSelectedDiagram = {
        diagramId: 0,
        diagramName: ""
    }

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
    ;

    /**
     * Description: get the list of diagrams from the DB
     */
    $scope.initDmOpenDiagram = function () {
        $scope.diagramList = [];

        const GET_URL = apiHost + "/data-models/diagram/list/" + $scope.dmDiagramClass.classId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.diagramList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: set the diagramId and diagramName for diagram selected in table
     */
    $scope.setSelectedDiagram = function (diagramId, diagramName) {
        $scope.diagramSelectedDiagram.diagramId = diagramId;
        $scope.diagramSelectedDiagram.diagramName = diagramName;
    }

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.ok = function () {
        if($scope.diagramSelectedDiagram.diagramId>0){
            $scope.setSelectedDiagramId($scope.diagramSelectedDiagram.diagramId)
            $uibModalInstance.close();
            $scope.readDiagramFromDb();
        } else{
            const msg = "Please select a diagram to open.";
            notify({
                message: msg,
                classes: "alert-warning",
                templateUrl: $scope.notifyTemplate
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnOpenDiagramController', CmnOpenDiagramController);
