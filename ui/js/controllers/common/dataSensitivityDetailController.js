/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * Description: Controller for Data Sensitivity Detail view, which includes the following tabs:
 * 1. Full Data Sensitivity Report
 * 2. Data Sensitivity Tags
 * 3. Data Sensitivity Auto Assignment Tasks
 */
function DataSensitivityDetailController($scope, $window, $state, $stateParams, $uibModal, notify,
                                         DTOptionsBuilder, DTColumnBuilder, RbacService, HttpService, UtilityService,
                                         NavigationChainService, DataSensitivityService) {
    $scope.fullDataSensitivityReport = [];
    $scope.dataSensitivityTagList = [];
    $scope.selectedElement = {dlDsTagId: 0, dlDsTagName: "", dlDsTagShortDesc: "", dlDsTagSelected: false};
    $scope.selectedElementUndo = {};

    $scope.dsAutoAssignTasks = [];

    $scope.activeTab = {dsfullrpt: true, dstags: false, dstasks: false, activeIndex: 1};
    $scope.comment = {};
    $scope.perms = {create: false, update: false, delete: false, publish: false, configure: false};

    $scope.editingDisabled01 = true;
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'csv', title: 'DataSensitivityTagList'}
        ])
        .withOption('aaSorting', []);
    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    $scope.dsLevelCode = {'low':'01', 'medium':'02', 'high':'03', 'critical':'04'};
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initDataSensitivityDetail = function () {

        if ($stateParams.invocationParams.hasOwnProperty("stateParams")) {
            if($stateParams.invocationParams.stateParams.hasOwnProperty("activeTab")) {
                switch ($stateParams.invocationParams.stateParams.activeTab) {
                    case 1:
                        $scope.activeTab = {dsfullrpt: true, dstags: false, dstasks: false, activeIndex: 1};
                        break;
                    case 2:
                        $scope.activeTab = {dsfullrpt: false, dstags: true, dstasks: false, activeIndex: 2};
                        break;
                    case 3:
                        $scope.activeTab = {dsfullrpt: false, dstags: false, dstasks: true, activeIndex: 3};
                        break;
                }
            }
        }

        $scope.perms.create = !RbacService.checkRbac('ont','create');
        $scope.perms.update = !RbacService.checkRbac('ont','update');
        $scope.perms.delete = !RbacService.checkRbac('ont','delete');
        $scope.perms.publish = !RbacService.checkRbac('ont','publish');
        $scope.perms.publish = !RbacService.checkRbac('ont','configure');

        $scope.readDsTagList();
        $scope.readFullDsReport();
        $scope.readDsAutoAssignTaskList();
    }

    /**
     * Description: reads the full list of data sensitivity tags via the DL API
     */
    $scope.readFullDsReport = function() {
        const GET_URL = apiHost + '/common/datasensitivity/report/full';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.fullDataSensitivityReport = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: reads the full list of data sensitivity tags via the DL API
     */
    $scope.readDsTagList = function() {
        const GET_URL = apiHost + '/common/datasensitivity/tag/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        let dsTagList = response.data.result_data;

                        $scope.dataSensitivityTagList = [];

                        // add the elementSelected attribute to each tag
                        for (var i = 0; i < dsTagList.length; i++) {
                            let dsTag = dsTagList[i];
                            dsTag.elementSelected = 0;
                            dsTag.dsLevelCode = $scope.dsLevelCode[dsTag.dlDsLevel];
                            $scope.dataSensitivityTagList.push(dsTag);
                        }
                    }
                }
            ]
        );
    }

    /**
     * Description: reads the list of data sensitivity auto assignment tasks via the DL API
     */
    $scope.readDsAutoAssignTaskList = function() {
        const GET_URL = apiHost + '/common/datasensitivity/autoassigntask/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.dsAutoAssignTasks = response.data.result_data.dlDsAutoAssignmentTaskList;
                    }
                }
            ]
        );

    }

    /**
     * Returns the CSS Class for the specified data sensitivity by invoking the DataSensitivityService.
     * @param dataSensitivityLevel
     * @returns {*}
     */
    $scope.getDataSensitivityCssClass = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: display details of the data sensitivity tag it is clicked in the list view.
     * @param selectedElement
     */
    $scope.showElementDetails = function(selectedElement) {

        // 1. reset the selected element flag. Number of tags will always be small so just iterate full list
        for (let i = 0; i < $scope.dataSensitivityTagList.length; i++) {
            if ($scope.dataSensitivityTagList[i].elementSelected == 1)
                $scope.dataSensitivityTagList[i].elementSelected = 0;
            if ($scope.dataSensitivityTagList[i].dlDsTagId == selectedElement.dlDsTagId)
                $scope.dataSensitivityTagList[i].elementSelected = 1;
        }

        $scope.resetEditing();
        $window.scrollTo(0, 0);

        // 2. set selected element.
        $scope.selectedElement = selectedElement;

    }

    /**
     * Description: checks whether an element in the hierarchy has been selected. If parameter silent is true then notification is not provided
     * @param silent
     * @returns {boolean}
     */
    $scope.checkElementSelected = function(silent) {
        if ($scope.selectedElement == null || $scope.selectedElement.dlDsTagId <= 0) {
            if(silent !== true){
                notify({
                    message: "No element selected.",
                    classes: "alert-danger",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            return false;
        }
        return true;
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteMdObjectType) {
        let check = 0;

        // if the mdObject type == 'element', check that an element is selected
        if($scope.selectedElement == null || $scope.selectedElement.dlDsTagId <= 0) {
            check = 1;
            const errorMessage = "You have not selected a data sensitivity tag.";
            notify({ message: errorMessage,
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate});
        }

        if (check == 0) {
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/data_sensitivity_confirm_delete_tag.html'),
                controller: 'DataSensitivityConfirmDeleteTagController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.editingDisabled01 = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.selectedElement, $scope.selectedElementUndo);
            $scope.editingDisabled01 = false;
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            $scope.editingDisabled01 = true;
            $scope.saveDlDsTagDetails();
        }
    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.selectedElementUndo, $scope.selectedElement);
            $scope.editingDisabled01 = true;
        }
    }

    /**
     * Saves the selected element to the DL DB
     */
    $scope.saveDlDsTagDetails = function() {
        const POST_URL = apiHost + "/common/datasensitivity/tag";

        HttpService.call(POST_URL, "POST", $scope.selectedElement, null,
            [
                {
                    status: 1,
                    callback: function(response) {

                        notify({
                            message: "Data sensitivity tag details successfully saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: Show the Create Data Sensitivity Tag dialog
     */
    $scope.openCreateDataSensitivityTagDialog = function () {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/common/create_data_sensitivity_tag.html'),
            controller: 'CreateDataSensitivityTagController',
            size: 'lg',
            scope: $scope
        });
    };

    /**
     * Description: Save new data sensitivity tag to the DL DB
     */
    $scope.saveNewDataSensitivityTag = function (newDsTag) {
        const POST_URL = apiHost + "/common/datasensitivity/tag";

        HttpService.call(POST_URL, "POST", newDsTag, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.readDsTagList();

                        notify({
                            message: "New data sensitivity tag saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1062,
                    callback: function(response) {
                        notify({
                            message: "A data sensitivity tag with that name already exists.",
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: Deletes the currently selected data sensitivity tag after confirmation from user.
     */
    $scope.deleteDStag = function () {
        console.log("Deleting DS Tag identified by " + $scope.selectedElement.dlDsTagId);
        const DELETE_URL = apiHost + "/common/datasensitivity/tag/" + $scope.selectedElement.dlDsTagId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {

                        notify({
                            message: "Data sensitivity tag details successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readDsTagList();
                    }
                }
            ]
        );

    }

    /**
     * Description: transition to a new state via the NavigationChainService
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        let currentState = {
            stateName:'taxonomies.data_sensitivity_tag_list',
            objectId:0,
            classId:0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: {}
        }
        NavigationChainService.storeCurrentState(currentState);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "detail":
                stateName = 'taxonomies.ds_auto_assign_task_detail';
                currentState.stateParams = {activeTab: 3};
                break;
            case "create":
                stateName = 'taxonomies.create_ds_auto_assign_task';
                currentState.stateParams = {activeTab: 3};
                break;
            default:
                stateName = 'taxonomies.data_sensitivity_tag_list';
                break;
        }

        $state.go(stateName,
            {
                invocationParams: {
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DataSensitivityDetailController', DataSensitivityDetailController);
