/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Controller for the common user login screen
 */
function CmnLoginController($http, $scope, $rootScope, $location, notify, RbacService, HttpService) {
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.username = "";
    $scope.password = "";

    $scope.login = function () {

        const authCredentials = {
            username: $scope.username,
            password: $scope.password
        };
        const POST_URL = apiHost + '/auth/login02';

        $http.post(POST_URL, authCredentials).then(
            function successCallback(response) {
                $scope.resultStatus = response.data.resultStatus;

                if ($scope.resultStatus === 1) {
                    HttpService.setUserId(response.data.userId);
                    RbacService.setUserId(response.data.userId);
                    RbacService.setGroupPermissions(response.data.groupPermissions);
                    notify.closeAll();
                    $location.path("home/overview");

                } else if ($scope.resultStatus === -10) {
                    notify({
                        message: "Login failed. The username/password combination you have provided is incorrect.",
                        classes: "alert-danger",
                        duration: 2000,
                        templateUrl: $scope.notifyTemplate
                    });
                } else {
                    notify({
                        message: "There was a error when trying to authenticate your credentials. The cause of the error is unknown.",
                        classes: "alert-warning",
                        duration: 2000,
                        templateUrl: $scope.notifyTemplate
                    });
                }
            },
            function errorCallback(response) {
                if(response.status === 401){
                    notify({
                        message: "Login failed. The username/password combination you have provided is incorrect.",
                        classes: "alert-danger",
                        duration: 2000,
                        templateUrl: $scope.notifyTemplate
                    });
                    $location.path("logins");
                }
                else if(response.status === 403){
                    notify({
                        message: "Login failed. Not authorised! That doesn't even make sense.",
                        classes: "alert-danger",
                        duration: 2000,
                        templateUrl: $scope.notifyTemplate
                    });
                    $location.path("logins");
                }
                else{
                    let responseError = "none provided";
                    let responseMessage = "none provided";
                    if (response.data != null) {
                        responseError = response.data.error;
                        responseMessage = response.data.message;
                    }
                    let errorMessage = "The API call " + apiHost + "/auth/login02 failed with error '" + responseError + "'.";
                    errorMessage += " The error message was: " + responseMessage;
                    notify({
                        message: errorMessage,
                        classes: "alert-danger",
                        duration: 30000,
                        templateUrl: $scope.notifyTemplate
                    });
                }
            }
        );
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnLoginController', CmnLoginController);
