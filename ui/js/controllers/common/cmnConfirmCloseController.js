/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: confirm close MdObject with unsaved changes
 */

function CmnConfirmCloseController ($scope, $uibModalInstance) {

    $scope.save = function() {
        // the saveMdObject() function must be defined in the Controller that invoked the confirm_close.html dialog
        $scope.saveMdObject();
        $uibModalInstance.close();

    }

    $scope.closeConfirmed = function() {
        // the closeMdObject() function must be defined in the Controller that invoked the confirm_close.html dialog
        $scope.closeMdObject();
        $uibModalInstance.close();

    }

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ConfirmCloseController', CmnConfirmCloseController);

