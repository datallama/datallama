/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating data model diagrams
 */
function CmnChangePasswordController ($scope, $rootScope, $uibModalInstance, notify, HttpService) {
    $scope.userPasswordChange = {
        currentPassword: "",
        newPassword: "",
        repeatNewPassword: ""
    };
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: sends the password change request to the API
     */
    $scope.save = function () {
        // check that the new password and repeat new password are identical
        if ($scope.userPasswordChange.newPassword === $scope.userPasswordChange.repeatNewPassword) {
            $uibModalInstance.close();

            // save new smd database core details to DL database
            const POST_URL = apiHost + '/auth/changepassword';

            HttpService.call(POST_URL,"POST",$scope.userPasswordChange,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            notify({
                                message: "Password successfully changed",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );
        }
        else {
            notify({ message: "The new password and repeat new password do not match.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate});
        }

    };

    /**
     * Description: cancels the user action and closes the overlay modal dialog
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}
/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CmnChangePasswordController', CmnChangePasswordController);