/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function MoClassListController($scope, $rootScope, $state, $stateParams, NavigationChainService, RbacService, notify, DTOptionsBuilder, HttpService) {

    $scope.gridData01 = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'User Group Assignment'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};

    /**
     * Description: Initialises the MoClassListController
     */
    $scope.initMoClasList = function () {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.readMoClassList();
    }

    /**
     * Description: reads the list of metadata object classes from the DL database
     */
    $scope.readMoClassList = function () {
        const GET_URL = apiHost + '/admin/class/list/';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData01 = response.data.result_data.classList;
                    }
                }
            ]
        );
    }

    /**
     * Description: click through to class detail view
     */
    $scope.clickThrough = function (objectId, classId) {
        const currentStateSpec = {
            stateName:"admin.mo_class_list",
            objectId:0,
            classId:0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"admin.mo_class_detail",
            objectId:objectId,
            classId:classId,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MoClassListController', MoClassListController);
