/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function UserDetailController($http, $scope, $rootScope, $state, $stateParams, $uibModal, notify,
                              NavigationChainService, RbacService, DTOptionsBuilder, HttpService, UtilityService) {

    $scope.userDetails = {core: {}, groupAssignment: []};
    $scope.userId = 0;
    $scope.editingDisabled = {core: true, groupAssignment: true};
    $scope.userDetailsCoreUndo = {};
    $scope.userDetailsGroupAssignmentUndo = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'User Group Assignment'}
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};

    /**
     * Description: Initialises the UserDetailController
     */
    $scope.initUserDetail = function () {
        $scope.perms.create = !RbacService.checkRbac('user','create');
        $scope.perms.update = !RbacService.checkRbac('user','update');
        $scope.perms.delete = !RbacService.checkRbac('user','delete');
        $scope.perms.publish = !RbacService.checkRbac('user','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.userId = $scope.invocationParams.objectId;

        $scope.readUserDetails();
    }

    /**
     * Description: Reads user core details and group assignments from the DL database
     */
    $scope.readUserDetails = function() {
        const GET_URL = apiHost + '/admin/auth/user/' + $scope.userId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.userDetails = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Descriptions: toggles the disable status of the editing icon set
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "user_details") {
            angular.copy($scope.userDetails.core, $scope.userDetailsCoreUndo);
            $scope.editingDisabled.core = false;
        }
        else if (dlIboxName === "group_assignment") {
            angular.copy($scope.userDetails.groupAssignment, $scope.userDetailsGroupAssignmentUndo);
            $scope.editingDisabled.groupAssignment = false;
        }
    }

    /**
     * Description: saves edits to DL database by invoking relevant save function
     * @param dlIboxName
     */
    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "user_details") {
            $scope.editingDisabled.core = true;
            $scope.saveUserCoreDetails();
        }
        else if (dlIboxName === "group_assignment") {
            // validate that at least one group has been assigned
            let validate = 0;
            for (let i = 0; i < $scope.userDetails.groupAssignment.length; i++) {
                if ($scope.userDetails.groupAssignment[i].assigned)
                    validate++;
            }

            if (validate === 0) {
                angular.copy($scope.userDetailsGroupAssignmentUndo, $scope.userDetails.groupAssignment);
                notify({
                    message: "The user must be assigned to at least one group.\nThe user's group assignment has been reset to the previous state.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.saveUserGroupAssignment();
            }
        }
    }

    /**
     * Description: cancels edits and rolls back to pre-edit values
     * @param dlIboxName
     */
    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName === "user_details") {
            angular.copy($scope.userDetailsCoreUndo, $scope.userDetails.core);
            $scope.editingDisabled.core = true;
        }
        else if (dlIboxName === "group_assignment") {
            angular.copy($scope.userDetailsGroupAssignmentUndo, $scope.userDetails.groupAssignment);
            $scope.editingDisabled.groupAssignment = true;
        }
    }

    /**
     * Description: saves user core details to the DL database
     */
    $scope.saveUserCoreDetails = function() {

        const POST_URL = apiHost + '/admin/auth/user';

        HttpService.call(POST_URL,"POST",$scope.userDetails.core,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "User details updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: saves user group assignment configuration to the DL database
     */
    $scope.saveUserGroupAssignment = function() {
        const POST_URL = apiHost + '/admin/auth/user/grouplist';

        const userGroupList = {
            userId: $scope.userDetails.core.userId,
            groupList: $scope.userDetails.groupAssignment
        }

        HttpService.call(POST_URL,"POST",userGroupList,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.editingDisabled.groupAssignment = true;
                        let message = "User group assignment updated successfully.";
                        if(RbacService.getUserId() === $scope.userDetails.core.userId){
                           message = "User group assignment updated successfully. Please log out and back in to view changes."
                        }
                        notify({
                            message: message,
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    $scope.openResetPasswordDialog = function() {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/admin/reset_user_password.html'),
            controller: 'ResetUserPasswordController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: saves new password for currently selected user
     * @param newPassword
     */
    $scope.userPasswordReset = function(newPassword) {
        $scope.editingDisabled.core = false;

        const POST_URL = apiHost + '/admin/auth/user/passwordreset';

        const userPasswordResetDetails = {
            userId: $scope.userId,
            newPassword: newPassword
        }

        HttpService.call(POST_URL,"POST",userPasswordResetDetails,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "User details updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: returns to the previous state in the Navigation Chain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('UserDetailController', UserDetailController);

