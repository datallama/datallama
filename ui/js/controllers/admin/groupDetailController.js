/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function GroupDetailController($scope, $rootScope, $state, $stateParams, notify, NavigationChainService,
                               RbacService, DTOptionsBuilder, HttpService) {

    $scope.groupDetails = {core: {}, groupPermissions: []};
    $scope.groupId = 0;
    $scope.editingDisabled = {core: true, groupPermissions: true};
    $scope.groupDetailsCoreUndo = {};
    $scope.groupDetailsGroupPermissionsUndo = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'User Group Assignment'}
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};

    /**
     * Initialises the UserDetailController
     */
    $scope.initGroupDetail = function () {
        $scope.perms.create = !RbacService.checkRbac('user','create');
        $scope.perms.update = !RbacService.checkRbac('user','update');
        $scope.perms.delete = !RbacService.checkRbac('user','delete');
        $scope.perms.publish = !RbacService.checkRbac('user','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.groupId = $scope.invocationParams.objectId;

        $scope.readGroupDetails();
    }

    /**
     * Description: Reads user core details and group assignments from the DL database
     */
    $scope.readGroupDetails = function() {
        const GET_URL = apiHost + '/admin/auth/group/' + $scope.groupId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.groupDetails.core.groupId = response.data.result_data.group.groupId;
                        $scope.groupDetails.core.groupName = response.data.result_data.group.groupName;
                        $scope.groupDetails.core.groupShortDesc = response.data.result_data.group.groupShortDesc;
                        $scope.groupDetails.groupPermissions = response.data.result_data.group.groupPermissions;
                    }
                }
            ]
        );

    }

    /**
     * Descriptions: toggles the disable status of the editing icon set
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "group_details") {
            angular.copy($scope.groupDetails.core, $scope.groupDetailsCoreUndo);
            $scope.editingDisabled.core = false;
        }
        else if (dlIboxName === "group_permissions") {
            angular.copy($scope.groupDetails.groupPermissions, $scope.groupDetailsGroupPermissionsUndo);
            $scope.editingDisabled.groupPermissions = false;
        }
    }

    /**
     * Description: saves edits to DL database by invoking relevant save function
     * @param dlIboxName
     */
    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "group_details") {
            $scope.editingDisabled.core = true;
            $scope.saveGroupCoreDetails();
        }
        else if (dlIboxName === "group_permissions") {
            $scope.editingDisabled.groupPermissions = true;
            $scope.saveGroupPermissions();
        }
    }

    /**
     * Description: cancels edits and rolls back to pre-edit values
     * @param dlIboxName
     */
    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName === "group_details") {
            angular.copy($scope.groupDetailsCoreUndo, $scope.groupDetails.core);
            $scope.editingDisabled.core = true;
        }
        else if (dlIboxName === "group_permissions") {
            angular.copy($scope.groupDetailsGroupPermissionsUndo, $scope.groupDetails.groupPermissions);
            $scope.editingDisabled.groupAssignment = true;
        }
    }

    /**
     * Description: saves group core details to the DL database
     */
    $scope.saveGroupCoreDetails = function() {

        const POST_URL = apiHost + '/admin/auth/group';

        HttpService.call(POST_URL,"POST",$scope.groupDetails.core,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Group core details updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves group permissions configuration to the DL database
     */
    $scope.saveGroupPermissions = function() {

        const POST_URL = apiHost + '/admin/auth/group/permissions';

        const groupUpdates = {
            groupId: $scope.groupDetails.core.groupId,
            groupName: $scope.groupDetails.core.groupName,
            groupShortDesc: $scope.groupDetails.core.groupShortDesc,
            groupPermissions: $scope.groupDetails.groupPermissions
        }

        HttpService.call(POST_URL,"POST",groupUpdates,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Group permissions configuration updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }


    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param action_category
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: returns to the previous state in the Navigation Chain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('GroupDetailController', GroupDetailController);

