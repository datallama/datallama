/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new taxonomy element
 */
function CreateGroupController ($scope, $uibModalInstance) {
    console.log($scope);

    $scope.newGroupDetails = {
        groupId:-1,
        groupName:"",
        groupShortDesc:""
    };

    $scope.validation = {
        groupName: true
    }
    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.save = function () {
        console.log($scope.newGroupDetails);
        let valid = 0;
        // validate required fields
        if ($scope.newGroupDetails.groupName.length === 0) {
            $scope.validation.groupName = false;
            valid++;
        }

        if (valid === 0) {
            $scope.saveNewGroup($scope.newGroupDetails);   // this function is defined in usersGroupsListController
            $uibModalInstance.close();
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateGroupController', CreateGroupController);
