/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new taxonomy element
 */
function CreateUserController ($scope, $uibModalInstance) {
    $scope.newUserDetails = {
        userId:-1,
        username:"",
        firstname:"",
        lastname:"",
        emailAddress:"",
        enabled:true,
        initialPassword:""
    };

    $scope.validation = {
        username: true,
        firstname: true,
        lastname: true,
        initialPassword: true
    }
    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.save = function () {

        let valid = 0;
        // validate required fields
        if ($scope.newUserDetails.username.length === 0) {
            $scope.validation.username = false;
            valid++;
        }
        if ($scope.newUserDetails.firstname.length === 0) {
            $scope.validation.firstname = false;
            valid++;
        }
        if ($scope.newUserDetails.lastname.length === 0) {
            $scope.validation.lastname = false;
            valid++;
        }
        if ($scope.newUserDetails.password.length === 0) {
            $scope.validation.initialPassword = false;
            valid++;
        }

        if (valid === 0) {
            $scope.saveNewUser($scope.newUserDetails);   // this function is defined in usersGroupsListController
            $uibModalInstance.close();
        }


    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateUserController', CreateUserController);
