/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function MoClassDetailController($scope, $rootScope, $state, $stateParams, $uibModal, notify, NavigationChainService,
                                 RbacService, DTOptionsBuilder, HttpService, UtilityService) {
    $scope.moClassDetails = {};
    $scope.moClassDetailsUndo = {};

    $scope.editingDisabled = {core: true};
    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.initMoClassDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('refc','create');
        $scope.perms.update = !RbacService.checkRbac('refc','update');
        $scope.perms.delete = !RbacService.checkRbac('refc','delete');
        $scope.perms.publish = !RbacService.checkRbac('refc','publish');

        $scope.readMoClassDetail();
    }

    /**
     * Description: reads the list of metadata object classes from the DL database
     */
    $scope.readMoClassDetail = function () {
        const GET_URL = apiHost + '/admin/class/detail/' + $scope.invocationParams.objectId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.moClassDetails = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Descriptions: toggles the disable status of the editing icon set
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "class_details") {
            angular.copy($scope.moClassDetails, $scope.moClassDetailsUndo);
            $scope.editingDisabled.core = false;
        }
    }

    /**
     * Description: saves edits to DL database by invoking relevant save function
     * @param dlIboxName
     */
    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "class_details") {
            $scope.editingDisabled.core = true;
            $scope.saveMoClassDetails();
        }
    }

    /**
     * Description: cancels edits and rolls back to pre-edit values
     * @param dlIboxName
     */
    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName == "class_details") {
            angular.copy($scope.moClassDetailsUndo, $scope.moClassDetails);
            $scope.editingDisabled.core = true;
        }
    }

    /**
     * Description: opens the Select Custom Form dialog
     */
    $scope.showCustomFormSelectDialog = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'lg',
            scope: $scope
        });

    };

    $scope.saveSelectedCustomForm = function (selectedCustomForm) {
        $scope.moClassDetails.customFormId = selectedCustomForm.customFormId;
        $scope.moClassDetails.customFormName = selectedCustomForm.customFormName;
    }

    /**
     * Description: saves update details for metadata object classes from the DL database
     */
    $scope.saveMoClassDetails = function () {
        const POST_URL = apiHost + '/admin/class/detail/';

        HttpService.call(POST_URL,"POST",$scope.moClassDetails,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "MO Class details updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: returns to the previous state in the Navigation Chain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}


/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MoClassDetailController', MoClassDetailController);
