/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new taxonomy element
 */
function CreateAssociationTypeController ($scope, $uibModalInstance) {
    $scope.validation = {
        name: true,
        sourcePhrase: true,
        targetPhrase: true
    }

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.create = function () {

        $scope.validation.name = !!($scope.assocTypeName && $scope.assocTypeName.length > 0);
        $scope.validation.sourcePhrase = !!($scope.sourcePhrase && $scope.sourcePhrase.length > 0);
        $scope.validation.targetPhrase = !!($scope.targetPhrase && $scope.targetPhrase.length > 0);
        if($scope.validation.name && $scope.validation.sourcePhrase && $scope.validation.targetPhrase){
            const newAssociationType = {
                assocTypeId: $scope.assocTypeId,
                assocTypeName: $scope.assocTypeName,
                assocTypeShortDesc: $scope.assocTypeShortDesc,
                sourcePhrase: $scope.sourcePhrase,
                targetPhrase: $scope.targetPhrase
            };

            $scope.createAssociationType(newAssociationType);
            $uibModalInstance.close();
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateAssociationTypeController', CreateAssociationTypeController);
