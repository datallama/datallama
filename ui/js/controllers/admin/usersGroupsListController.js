/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function UsersGroupsListController($http, $scope, $rootScope, $state, $stateParams, $uibModal, notify,
                                   NavigationChainService, RbacService, DTOptionsBuilder, HttpService, UtilityService) {

    $scope.userList = [];
    $scope.groupList = [];

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DataLlamaUserGroupReport'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;

    /**
     * Description: Initialises the controller
     */
    $scope.initUsersGroupsList = function () {
        $scope.readUserGroupData();
    }

    /**
     * Description: retrieves user and group information from the Data Llama database
     */
    $scope.readUserGroupData = function () {
        const GET_URL = apiHost + '/admin/auth/list_users_groups';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.userList = response.data.result_data.users;
                        $scope.groupList = response.data.result_data.groups;
                    }
                }
            ]
        );
    }

    /**
     * Description: Opens the Create Taxonomy modal dialog box
     */
    $scope.openCreateUserDialog = function() {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/admin/create_user.html'),
            controller: 'CreateUserController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Opens the Create Taxonomy modal dialog box
     */
    $scope.openCreateGroupDialog = function() {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/admin/create_group.html'),
            controller: 'CreateGroupController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: saves details of a new user in the DL database.
     * The structure of newUserDetails is the same as the Java class io.datallama.api.security.DlUser
     * @param newUserDetails
     */
    $scope.saveNewUser = function (newUserDetails) {
        const POST_URL = apiHost + '/admin/auth/user';
        console.log(newUserDetails);

        HttpService.call(POST_URL, "POST", newUserDetails, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "New user created successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readUserGroupData();
                    }
                }
            ]
        );
    }

    /**
     * Description: saves details of a new group to the DL database.
     * The structure of newGroupDetails is the same as the Java class io.datallama.api.security.DlGroup but without the assigned member
     * @param newGroupDetails
     */
    $scope.saveNewGroup = function (newGroupDetails) {
        const POST_URL = apiHost + '/admin/auth/group';

        HttpService.call(POST_URL, "POST", newGroupDetails, null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "New user created successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readUserGroupData();
                    }
                }
            ]
        );
    }
    /**
     * Description: click through to taxonomy detail
     */
    $scope.clickThrough = function (objectId, classId) {
        const currentStateSpec = {
            stateName:"admin.users_groups_list",
            objectId:0,
            classId:0,
            stateParams:$scope.stateParams
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        if (classId === -10) {  // click through to user details
            const invocationParams = {
                stateName:"admin.user_detail",
                objectId:objectId,
                classId:classId,
                stateParams:{}
            };
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
        else {
            const invocationParams = {
                stateName:"admin.group_detail",
                objectId:objectId,
                classId:classId,
                stateParams:{}
            };
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
    }

    /**
     * Description: return to previous state in the Navigation Chain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('UsersGroupsListController', UsersGroupsListController);

