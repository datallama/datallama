/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Custom Form Designer
 */
function CustomFormDesignerController($scope, $stateParams, $uibModal, notify, HttpService, NavigationChainService,
                                      DTOptionsBuilder, RbacService, UtilityService) {
    $scope.invocationParams = {};
    $scope.customFormId = $stateParams.invocationParams.objectId;      // the taxonomyId is specified in the page URL are retrieved with $stateParams
    $scope.formDefinition = {};
    $scope.formDefinitionUndo = {};
    $scope.customFormJsonDisabled = true;
    $scope.enumAssoc = [];

    $scope.modelData = {};

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    /**
     * Initialises the view page on load
     */
    $scope.initCustomFormDesigner = function() {
        $scope.perms.create = !RbacService.checkRbac('custform','create');
        $scope.perms.update = !RbacService.checkRbac('custform','update');
        $scope.perms.delete = !RbacService.checkRbac('custform','delete');
        $scope.perms.publish = !RbacService.checkRbac('custform','publish');

        $scope.invocationParams = $stateParams.invocationParams;

        $scope.readCustomFormContent();
    }

    /**
     * Description: call API to read the custom form content
     */
    $scope.readCustomFormContent = function () {

        const GET_URL = apiHost + '/admin/customform/content/' + $scope.customFormId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.formDefinition.customFormId = response.data.core_details.customFormId;
                        $scope.formDefinition.classId = response.data.core_details.classId;
                        $scope.formDefinition.customFormName = response.data.core_details.customFormName;
                        $scope.formDefinition.customFormShortDesc = response.data.core_details.customFormShortDesc;
                        $scope.formDefinition.schemaDefinition = JSON.parse(response.data.core_details.schemaDefinition);
                        $scope.formDefinition.formDefinition = JSON.parse(response.data.core_details.formDefinition);

                        $scope.formDefinition.schemaString = JSON.stringify($scope.formDefinition.schemaDefinition,null,"  ");
                        $scope.formDefinition.formString = JSON.stringify($scope.formDefinition.formDefinition, null, "  ");

                        $scope.enumAssoc = response.data.enum_assoc;

                        $scope.renderForm();
                    }
                }
            ]
        );
    }

    /**
     * Description: reload the previous view in the navigation chain.
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: enables the custom form JSON definition textareas for editing
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "custom_form_json") {
            // capture json defintions for undo of edit
            angular.copy($scope.formDefinition, $scope.formDefinitionUndo)
            $scope.customFormJsonDisabled = false;
        }
    }

    /**
     * Description: saves changes made to custom form JSON definitions to the DL database
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "custom_form_json") {
            $scope.customFormJsonDisabled = true;

            $scope.formDefinition.schemaDefinition = JSON.parse($scope.formDefinition.schemaString);
            $scope.formDefinition.formDefinition = JSON.parse($scope.formDefinition.formString);
            const updateCustomForm = {
                customFormId: $scope.formDefinition.customFormId,
                schemaDefinition: JSON.stringify($scope.formDefinition.schemaDefinition),
                formDefinition: JSON.stringify($scope.formDefinition.formDefinition)
            }

            // make the REST POST call to save the overview page content to the database
            const POST_URL = apiHost + "/admin/customform/content/detail";

            HttpService.call(POST_URL, "POST", updateCustomForm, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            notify({
                                message: "Changes saved.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: disables the custom form JSON definition textareas to prevent editing
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "custom_form_json") {
            // reset all fields to the pre-edit state
            angular.copy($scope.formDefinitionUndo, $scope.formDefinition);
            $scope.customFormJsonDisabled = true;
        }
    }

    $scope.pretty = function(){
        return typeof $scope.modelData === 'string' ? $scope.modelData : angular.toJson($scope.modelData);
    };

    /**
     * Description: This is a dummy submit form action for the purposes of the Form Design views
     * @param ngform
     * @param modelData
     */
    $scope.submitForm = function(ngform, modelData) {
        $scope.schemaJson.properties.name.readonly = false;
        $scope.$broadcast('schemaFormRedraw');
    };

    /**
     * Description: renders the custom form from the Schema Definition and Form Definition JSON objects
     */
    $scope.renderForm = function() {
        let result = 0;

/*        try {
            $scope.schemaJson = JSON.parse($scope.formDefinition.schemaString);
        }
        catch (e) {
            notify({
                message: "There is an error in the Schema Definition JSON. Please check and correct",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
            result++;
            console.log("error in the schema definition JSON.");
            console.log(e);
        }
*/

        try {
            $scope.formJson = JSON.parse($scope.formDefinition.formString);
        }
        catch (e) {
            notify({
                message: "There is an error in the Form Definition JSON. Please check and correct",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
            result++;
        }

        if (result === 0) {
            const GET_URL = apiHost + "/common/customform/" + $scope.customFormId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response) {
                            console.log(response);
                            $scope.schemaJson = response.data.result_data.schemaDefinitionJson;
                        }
                    },
                ]
            );
            $scope.$broadcast('schemaFormRedraw');
        }

    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectOntologyListDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_ontology_list.html'),
            controller: 'CmnSelectOntologyListController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: saves to the DL DB the association between the selected Ontology List and the current Custom Form
     * @param selectedOntologyList
     */
    $scope.saveSelectedOntologyList = function(selectedOntologyList) {
        let postData = {
            enumAssocId: 0,
            customFormId: $scope.customFormId,
            assocObjectId: selectedOntologyList.listCdgId,
            assocClassId: selectedOntologyList.classId,
            enumAssocShortDesc: selectedOntologyList.enumAssocShortDesc
        };

        const POST_URL = apiHost + "/common/customform/enumassoc";

        HttpService.call(POST_URL, "POST", postData, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "new association created successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });

                        $scope.refreshEnumAssociations();
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        notify({
                            message: response.data.result_data.errorMsg,
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: refreshes the list of enumeration associations.
     */
    $scope.refreshEnumAssociations = function() {
        const GET_URL = apiHost + "/common/customform/enumassoc/" + $scope.customFormId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.enumAssoc = response.data.result_data;
                    }
                }
            ]
        );
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CustomFormDesignerController', CustomFormDesignerController);
