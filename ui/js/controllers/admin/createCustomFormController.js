/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new taxonomy element
 */
function CreateCustomFormController ($scope, $uibModalInstance) {
    $scope.customFormName = "";
    $scope.customFormShortDesc = "";

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.create = function () {

        // TODO: input validation
        const newCustomForm = {
            customFormId: -1,
            customFormName: $scope.customFormName,
            customFormShortDesc: $scope.customFormShortDesc,
            classId: 20
        };

        $scope.createCustomForm(newCustomForm);    // this function must be defined in the controller that opens the dialog.
        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateCustomFormController', CreateCustomFormController);
