/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Association Type List view
 */
function AssociationTypeListController($scope, $uibModal, notify, DTOptionsBuilder, HttpService, UtilityService) {
    $scope.gridData = [];
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.stateParams = {};
    $scope.update = false;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'}
        ]
    );

    $scope.initAssociationTypeList = function() {
        $scope.readGridData();
    };

    $scope.readGridData = function() {
        const GET_URL = apiHost + '/association/types';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }

    $scope.openCreateAssociationTypeDialog = function() {

        $scope.assocTypeName = "";
        $scope.assocTypeShortDesc = "";
        $scope.sourcePhrase = "";
        $scope.targetPhrase = "";
        $scope.assocTypeId = -1;
        $scope.update = false;

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/admin/create_association_type.html'),
            controller: 'CreateAssociationTypeController',
            size: 'md',
            scope: $scope
        });

    }

    $scope.createAssociationType = function(newAssociationType) {
        const POST_URL = apiHost + "/admin/assoctype/content";

        HttpService.call(POST_URL,"POST",newAssociationType,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.readGridData();

                        let message = "New association type saved.";
                        if($scope.update){
                            message = "Association type updated"
                        }
                        notify({
                            message: message,
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    $scope.editAssociationType = function(association){
        $scope.assocTypeName = association.assocTypeName;
        $scope.assocTypeShortDesc = association.assocTypeShortDesc;
        $scope.sourcePhrase = association.sourcePhrase;
        $scope.targetPhrase = association.targetPhrase;
        $scope.assocTypeId = association.assocTypeId;
        $scope.update = true;

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/admin/create_association_type.html'),
            controller: 'CreateAssociationTypeController',
            size: 'md',
            scope: $scope
        });
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('AssociationTypeListController', AssociationTypeListController);

