/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function CustomFormListController($scope, $rootScope, $location, $state, $stateParams, $uibModal, notify,
                                  NavigationChainService, DTOptionsBuilder, HttpService, UtilityService) {
    $scope.gridData = [];
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'}
        ]
    );


    /**
     * Description: initialisation function for the controller
     */
    $scope.initCustomFormList = function() {
        $scope.readGridData();
    };

    /**
     * Description: reads the taxonomy list to populate the grid view
     */
    $scope.readGridData = function() {
        const GET_URL = apiHost + '/admin/customform/list';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: Opens the Create Taxonomy modal dialog box
     */
    $scope.openCreateCustomFormDialog = function() {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/admin/create_custom_form.html'),
            controller: 'CreateCustomFormController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: write a new taxonomy to the database based on details entered in the modal dialog
     * @param newCustomForm
     */
    $scope.createCustomForm = function(newCustomForm) {
        const POST_URL = apiHost + "/admin/customform/content";

        HttpService.call(POST_URL,"POST",newCustomForm,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.readGridData();

                        notify({
                            message: "New custom form saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: click through to taxonomy detail
     */
    $scope.clickThrough = function (objectId, classId) {
        const currentStateSpec = {
            stateName:"admin.custom_form_list",
            objectId:$scope.invocationParams.objectId,
            classId:$scope.invocationParams.classId,
            stateParams:$scope.stateParams
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"admin.custom_form_designer",
            objectId:objectId,
            classId:classId,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CustomFormListController', CustomFormListController);

