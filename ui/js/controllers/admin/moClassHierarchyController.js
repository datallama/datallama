/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the mo_class_hierarchy Detail view.
 *  The structure of $scope.selectedElement is:
 *  {classHierId: 0, elementId: 0, classId: 0, elementParentId: 0, elementLevel: 0, elementLevelSequence: 0, elementGUID: "",
 *   elementHierarchyNumber: "", elementName: "", elementDesc: "", elementFullPathName: ""}
 *  IMPORTANT: the invocationParams structure for classHierDetail must be as follows:
 *   {}
 */
function MoClassHierarchyController($scope, $rootScope, $location, $state, $stateParams, $window, $document, $anchorScroll,
                                  $uibModal, $timeout, notify, NavigationChainService, RbacService, HttpService,
                                  MoClassService, DTOptionsBuilder, DTColumnBuilder, UtilityService) {
    $scope.classHierElements = [];   // this array holds all the elements returned from the database
    $scope.classHierElementsTableView = [];      // this array is used to display on the table view of the classHier

    $scope.previousResultSetSequence = 0;
    $scope.selectedElement = {};
    $scope.selectedElementDisplay = {};
    $scope.elementAttributesUndo = {elementName: "", elementDesc: ""};

    $scope.customForm = {};

    $scope.config = {visibilityLevel: 4, searchPanelHidden: true, abbreviation: 0, endnodes: false, tableViewInitialised: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveElement = {moveMode: false, moveTargetElement: null};

    $scope.changeHistoryVisible = false;

    $scope.tedEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;
    $scope.tedActiveTab = {};

    $scope.customDTButtons = [];
    const exportButton = {
        text:"export",
        action:function(e, dt, node, config){
            alert('Export functionality is being implemented.');
        }
    };
    $scope.customDTButtons.push(exportButton);
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notSortable()
    ];

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.spinnerLoading = "";

    //When calling deleteMdObject() are we deleting the classHier (or just one of the elements)?
    $scope.deleteclassHier = false;

    /**
     * Description: initialisation function for the controller
     */
    $scope.initMoClassHierarchy = function() {

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.resetSelectedElementDisplay();

        $scope.readClassHierElements();
    }

    /**
     * Description: Open a dialog to select a custom form for the classHier
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *              delete the custom form data in the classHier elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Massive Database Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form data for <b>ALL</b> elements of this classHier will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/classHier/customform/" + $scope.classHierId;

        HttpService.call(POST_URL, "POST", selectedCustomForm.customFormId, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        notify({
                            message: "Custom form Id successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.customForm = selectedCustomForm;
                        $scope.resetSelectedElementDisplay();
                        $scope.readClassHierElements();
                    }
                }
            ]
        );
    }

    /**
     * Description: reads from the DL database all elements for the classHier identified by $scope.classHierId
     */
    $scope.readClassHierElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + "/ref/class/hierarchy";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processClassHierElementsResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: processes class hierarchy elements in the API response
     */
    $scope.processClassHierElementsResponse = function(response) {
        console.log(response.data.result_data);
        $scope.spinnerLoading = "";

        for (let i = 0; i < response.data.result_data.length; i++) {
            let currentClassElement = response.data.result_data[i];
            currentClassElement.resultSetSequence = i;
            currentClassElement.classMatched = 0;
            currentClassElement.classVisible = 1;
            currentClassElement.expanded = 1;
            currentClassElement.selected = 0;

            $scope.classHierElements.push(currentClassElement);
        }
        $scope.refreshSelectedElement();
    }


    /**
     * Description: refreshes the selected element status if elementId is specified in a navigation transition
     */
    $scope.refreshSelectedElement = function() {
        const refreshElementId = $scope.selectedElement.classId;

        if(refreshElementId > 0) {
            for (let i = 0; i < $scope.classHierElements.length; i++ ) {
                if ($scope.classHierElements[i].classId === refreshElementId) {
                    $scope.selectedElement = $scope.classHierElements[i];
                    break;
                }
            }
            $scope.loadSelectedElement(refreshElementId);
        }
    }

    /**
     * Enables editable input fields for the Ibox identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        if (dlIboxName === "ted") {
            if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
                notify({
                    message: "You have not selected an element. No edits can be made.",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                // make a copy of current state element core_details
                $scope.elementAttributesUndo.elementName = $scope.selectedElementDisplay.elementName;
                $scope.elementAttributesUndo.elementDesc = $scope.selectedElementDisplay.elementDesc;

                $scope.tedEditingDisabled = false;

                // reset the cfrm edit action
                $scope.cfrmEditingDisabled = true;
            }
        }
        else if (dlIboxName === "cfrm") {
            if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
                notify({
                    message: "You have not selected an element. No edits can be made.",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.enableCustomFormFields();
                $scope.cfrmEditingDisabled = false;

                // reset the ted edit action
                $scope.elementAttributesUndo = {elementName: "", elementDesc: ""};
                $scope.tedEditingDisabled = true;
            }
        }
    }

    /**
     * Saves to the DL DB the changes made in the Ibox identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;

            if ($scope.elementAttributesUndo.elementName == $scope.selectedElementDisplay.elementName &&
                $scope.elementAttributesUndo.elementDesc == $scope.selectedElementDisplay.elementDesc) {
                // nothing to do
            }
            else {
                $scope.saveElementCoreDetails();
            }
        }
        else if (dlIboxName === "cfrm") {
            $scope.cfrmEditingDisabled = true;
            $scope.saveElementCustomFormData();
        }
    }

    /**
     * Disables editable input fields for the Ibox identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            // reset all fields to the pre-edit state
            $scope.selectedElementDisplay.elementName = $scope.elementAttributesUndo.elementName;
            $scope.selectedElementDisplay.elementDesc = $scope.elementAttributesUndo.elementDesc;
            $scope.elementAttributesUndo = {elementName: "", elementDesc: ""};
        }
        else if (dlIboxName === "cfrm") {
            $scope.disableCustomFormFields();
            $scope.cfrmEditingDisabled = true;
        }

    }

    /**
     * Resets all elements in the selectedElementDisplay object to default values
     */
    $scope.resetSelectedElementDisplay = function() {
        $scope.selectedElementDisplay = {
            classId: 0,
            classParentId: 0,
            classLevel: 0,
            classLevelSequence: 0,
            classHierarchyNumber: "No class has been selected.",
            className: "No class has been selected.",
            classShortDesc: "No class has been selected.",
            customFormId: 0,
            customFormName: "No class has been selected.",
            parent: false,
            abstract: false,
            classMatched: 0,
            classVisible: 0,
            expanded: false,
            selected: 0,
        }
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: collapses the whole classHier down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.classHierElements.length; i++) {
            if ($scope.classHierElements[i].classLevel > 0) {
                $scope.classHierElements[i].classVisible = 0;
            }
            $scope.classHierElements[i].expanded = false;
        }
    };

    /**
     * Description: expands every element in the classHier
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.classHierElements.length; i++) {
            $scope.classHierElements[i].classVisible = 1;
            $scope.classHierElements[i].expanded = true;
        }
    };

    /**
     * Description: expands the classHier to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.classHierElements.length; i++) {
            if ($scope.classHierElements[i].classLevel < $scope.config.visibilityLevel - 1) {
                $scope.classHierElements[i].classVisible = 1;
                $scope.classHierElements[i].expanded = true;
            }
            else if ($scope.classHierElements[i].classLevel === $scope.config.visibilityLevel - 1) {
                $scope.classHierElements[i].classVisible = 1;
                $scope.classHierElements[i].expanded = false;
            }
            else {
                $scope.classHierElements[i].classVisible = 0;
                $scope.classHierElements[i].expanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param element
     */
    $scope.toggleElementExpansion = function(element) {
        const setVisibility = (element.expanded) ? 0 : 1;
        const ehnMatch = element.classHierarchyNumber + ".";

        if (element.expanded) { // collapse all levels below this element
            for(let i = 0; i < $scope.classHierElements.length; i++) {
                if ($scope.classHierElements[i].classHierarchyNumber.startsWith(ehnMatch)
                    && $scope.classHierElements[i].classLevel > element.classLevel){
                    $scope.classHierElements[i].classVisible = 0;
                    $scope.classHierElements[i].expanded = false;
                    $scope.classHierElements[i].selected = false;
                }
                if ($scope.classHierElements[i].classId === element.classId)
                    $scope.classHierElements[i].expanded = false;
            }
        }
        else {  // expand all levels below this element
            for(let i = 0; i < $scope.classHierElements.length; i++) {
                if ($scope.classHierElements[i].classHierarchyNumber.startsWith(ehnMatch)
                    && $scope.classHierElements[i].classLevel === element.classLevel + 1){
                    $scope.classHierElements[i].classVisible = 1;
                }
                if ($scope.classHierElements[i].classId === element.classId)
                    $scope.classHierElements[i].expanded = true;
            }
        }
    }

    /**
     * Description: display details of a classHier element when it is clicked in the hierarchy view
     * @param selectedElement
     * TODO: need to find a way to shift focus to Attributes tab
     */
    $scope.showElementDetails = function(selectedElement) {
        $scope.classHierElements[$scope.previousResultSetSequence].selected = 0;

        $scope.selectedElement = $scope.classHierElements[selectedElement.resultSetSequence];
        $scope.classHierElements[$scope.selectedElement.resultSetSequence].selected = 1;
        $scope.previousResultSetSequence = $scope.selectedElement.resultSetSequence;

        $scope.resetTedEditing();
        $window.scrollTo(0, 0); // scroll to top so that element details form is visible

        const GET_URL = apiHost + '/ref/class/detail/' + $scope.selectedElement.classId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processElementDetailsResponse(response);
                    }
                }
            ]
        );

    }

    /**
     * Description: process the details returned from the DL database for the selected element
     * @param response
     */
    $scope.processElementDetailsResponse = function(response) {
        $scope.resetSelectedElementDisplay();
        $scope.selectedElementDisplay.className = response.data.result_data.className;
        $scope.selectedElementDisplay.classShortDesc = response.data.result_data.classShortDesc;
        $scope.selectedElementDisplay.customFormId = response.data.result_data.customFormId;
        $scope.selectedElementDisplay.customFormName = response.data.result_data.customFormName;
        $scope.selectedElementDisplay.classHierarchyNumber = response.data.result_data.classHierarchyNumber;
        $scope.selectedElementDisplay.parent = response.data.result_data.parent;
        $scope.selectedElementDisplay.abstract = response.data.result_data.abstract;

        let hierarchyNumberArray = $scope.selectedElementDisplay.classHierarchyNumber.split(".");
        let trimmedHierarchyNumber = "";
        for (let i = 0; i < hierarchyNumberArray.length; i++) {
            trimmedHierarchyNumber = trimmedHierarchyNumber + parseInt(hierarchyNumberArray[i]) + ".";
        }
        $scope.selectedElementDisplay.classHierarchyNumber = trimmedHierarchyNumber.substr(0, trimmedHierarchyNumber.length -1);

    }

    /**
     * Description: loads the element details for classHier element identified by param elementId
     * @param elementId
     */
    $scope.loadSelectedElement = function(elementId) {
        // TODO: this function is not currently used but is reserved for future use.
        const GET_URL = apiHost + '/classHier/element/' + elementId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.selectedElementDisplay = response.data.result_data.attributes;
                        $scope.selectedElementAssociations = response.data.result_data.associations;

                        let hierarchyNumberArray = $scope.selectedElementDisplay.elementHierarchyNumber.split(".");
                        let trimmedHierarchyNumber = "";
                        for (let i = 0; i < hierarchyNumberArray.length; i++) {
                            trimmedHierarchyNumber = trimmedHierarchyNumber + parseInt(hierarchyNumberArray[i]) + ".";
                        }
                        $scope.selectedElementDisplay.elementHierarchyNumber = trimmedHierarchyNumber.substr(0, trimmedHierarchyNumber.length -1);
                    }
                }
            ]
        );

    }

    /**
     * Description: searches all elements using keyword
     */
    $scope.executeSearch = function() {
        // 1. clear the previous search
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.classHierElements[$scope.search.matchList[i].resultSetSequence].classMatched = false;
        }
        $scope.search.matchList = [];

        // expand all elements so matches can be displayed
        $scope.expandAll();

        const regexp = new RegExp($scope.search.keyword, "gi");
        for(let i = 0; i < $scope.classHierElements.length; i++) {
            const result = $scope.classHierElements[i].className.match(regexp);
            if (result != null) {
                const matchedElement = {
                    elementId: $scope.classHierElements[i].classId,
                    resultSetSequence: $scope.classHierElements[i].resultSetSequence,
                    elementName: $scope.classHierElements[i].className
                }

                $scope.search.matchList.push(matchedElement);
                $scope.classHierElements[i].classMatched = true;
            }
        }
        notify.config({duration: 2000});
        const message = "The Search returned " + $scope.search.matchList.length + " matches.";
        notify({
            message: message,
            classes: "alert-success",
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: clears all search results and collapes the hierarchy to 3 levels
     */
    $scope.clearSearch = function() {
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.classHierElements[$scope.search.matchList[i].resultSetSequence].classMatched = false;
        }
        $scope.search.matchList = [];
        $scope.search.currentViewIndex = null;
        $scope.search.keyword = "";

        $scope.config.visibilityLevel = 4;
        $scope.expandToLevel();
    }

    /**
     * Description: scrolls through the search results, up or down, bringing each element into focus
     */
    $scope.scrollSearchResults = function(direction) {
        if ($scope.search.matchList.length > 0){
            if (direction === 'down') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = 0;
                else if ($scope.search.currentViewIndex === $scope.search.matchList.length - 1)
                    $scope.search.currentViewIndex = 0;
                else
                    $scope.search.currentViewIndex++;
            }
            else if (direction === 'up') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else if ($scope.search.currentViewIndex === 0)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else
                    $scope.search.currentViewIndex--;
            }
            $location.hash($scope.search.matchList[$scope.search.currentViewIndex].elementId);
            $anchorScroll();
        }
    }

    $scope.resetTedEditing = function () {
        $scope.tedEditingDisabled = true;
    }

    /**
     * Description: Clears the SpinnerLoading overlay
     */
    $scope.clearSpinnerLoading = function () {
        $scope.spinnerLoading = "";
    };

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MoClassHierarchyController', MoClassHierarchyController);


