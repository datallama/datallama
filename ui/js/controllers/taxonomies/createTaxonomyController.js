/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new taxonomy element
 */
function CreateTaxonomyController ($scope, $http, $uibModalInstance, MoClassService) {
    $scope.newTaxonomy = {};
    $scope.taxonomyName = "";
    $scope.taxonomyShortDesc = "";

    $scope.validation = {
        name: true
    }

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.create = function () {

        // validate that taxonomy name has been specified
        if ($scope.taxonomyName.length == 0 || $scope.taxonomyName === null) {
            $scope.validation.name = false;
        }

        if ($scope.validation.name) {
            const newTaxonomy = {
                taxonomyId: -1,
                taxonomyName: $scope.taxonomyName,
                taxonomyShortDesc: $scope.taxonomyShortDesc,
                classId: MoClassService.getMoClassIdByCode('TAXONOMY_DEFINITION')
            };

            $scope.createTaxonomy(newTaxonomy);    // this function must be defined in the controller that opens the dialog.
            $uibModalInstance.close();
        }
    };

    /**
     * Description: cancels the create taxonomy action and closes the dialog
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

};

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateTaxonomyController', CreateTaxonomyController);
