/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy List view
 */
function TaxonomiesListController($http, $scope, $rootScope, $location, $state, $stateParams, $uibModal, notify,
                                  NavigationChainService, HttpService, DTOptionsBuilder, UtilityService) {
    $scope.gridData = [];
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},
        ]);

    /**
     * Description: initialisation function for the controller
     */
    $scope.initTaxonomiesList = function() {
        $scope.readGridData();
    };

    /**
     * Description: reads the taxonomy list to populate the grid view
     */
    $scope.readGridData = function() {
        const GET_URL = apiHost + '/taxonomy/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processGridDataResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description:
     * @param response
     */
    $scope.processGridDataResponse = function(response) {
        $scope.gridData = response.data.result_data;
    }

    /**
     * Description: Opens the Create Taxonomy modal dialog box
     */
    $scope.openCreateTaxonomyDialog = function() {

        var modalInstance = $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/taxonomies/create_taxonomy.html'),
            controller: 'CreateTaxonomyController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: write a new taxonomy to the database based on details entered in the modal dialog
     * @param newTaxonomy
     */
    $scope.createTaxonomy = function(newTaxonomy) {
        const POST_URL = apiHost + "/taxonomy/content";

        HttpService.call(POST_URL, "POST", newTaxonomy, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({ message: "New taxonomy saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate});
                        $scope.readGridData();
                    }
                }
            ]
        );
    }

    /**
     * Description: click through to taxonomy detail
     */
    $scope.clickThrough = function (objectId, classId, mode) {
        const currentStateSpec = {
            stateName:"taxonomies.list",
            objectId:$scope.invocationParams.objectId,
            classId:$scope.invocationParams.classId,
            parentObjectId:0,
            parentClassId:0,
            stateParams:$scope.stateParams
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {
            stateName:"taxonomies.detail",
            objectId:0,
            classId:0,
            parentObjectId:objectId,
            parentClassId:classId,
            stateParams:{
                tedActiveTab:{attributes:true, associations:false, comments:false, customform:false, more:false, activeIndex:1}
            }
        };
        if(mode === "summary"){
            invocationParams = {
                stateName:"taxonomies.taxonomy_summary",
                objectId:objectId,
                classId:classId,
                parentObjectId:0,
                parentClassId:0,
                stateParams:{}
            };
        }


        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

};

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('TaxonomiesListController', TaxonomiesListController);

