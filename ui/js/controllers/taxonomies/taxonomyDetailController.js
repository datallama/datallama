/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy Detail view.
 *  The structure of $scope.selectedElement is:
 *  {taxonomyId: 0, elementId: 0, classId: 0, elementParentId: 0, elementLevel: 0, elementLevelSequence: 0, elementGUID: "",
 *   elementHierarchyNumber: "", elementName: "", elementDesc: "", elementFullPathName: ""}
 *  IMPORTANT: the invocationParams structure for taxonomyDetail must be as follows:
 *   {}
 */
function TaxonomyDetailController($scope, $rootScope, $location, $state, $stateParams, $window, $document, $anchorScroll,
                                  $uibModal, $timeout, notify, NavigationChainService, RbacService, HttpService,
                                  MoClassService, DTOptionsBuilder, DTColumnBuilder, UtilityService) {
    $scope.taxonomyElements = [];   // this array holds all the elements returned from the database
    $scope.taxonomyElementsTableView = [];      // this array is used to display on the table view of the taxonomy

    $scope.taxonomyId = 0;      // the taxonomyId is specified in the page URL are retrieved with $stateParams
    $scope.classId = 0;
    $scope.taxonomy_summary = {};
    $scope.selectedElement = {};
    $scope.selectedElementDisplay = {};
    $scope.selectedElementChangeHistory = [];
    $scope.elementAttributesUndo = {elementName: "", elementDesc: ""};

    $scope.previousResultSetSequence = 0;
    $scope.selectedElementAssociations = [];
    $scope.newElement = {};
    $scope.newElementId = 0;
    $scope.returnId = 0;
    $scope.newElementConfig = {topLevelElement: true};

    $scope.customForm = {};

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false, tableViewInitialised: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveElement = {moveMode: false, moveTargetElement: null};

    $scope.changeHistoryVisible = false;

    $scope.tedEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;
    $scope.tedActiveTab = {};

    $scope.customDTButtons = [];
    const exportButton = {
        text:"export",
        action:function(e, dt, node, config){
            alert('Export functionality is being implemented.');
        }
    };
    $scope.customDTButtons.push(exportButton);
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notSortable()
    ];

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.spinnerLoading = "";

    //When calling deleteMdObject() are we deleting the taxonomy (or just one of the elements)?
    $scope.deleteTaxonomy = false;

    /**
     * Description: initialisation function for the controller
     */
    $scope.initTaxonomyDetail = function() {

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.resetSelectedElementDisplay();
        $scope.taxonomyId = $scope.invocationParams.parentObjectId;
        $scope.classId = $scope.invocationParams.parentClassId;
        $scope.selectedElement.elementId = $scope.invocationParams.objectId;
        $scope.selectedElement.classId = $scope.invocationParams.classId;

        if (angular.equals($scope.invocationParams.stateParams, {})) {
            $scope.tedActiveTab = {attributes:true, associations:false, comments:false, customform:false, more:false, activeIndex:1};
        }
        else {
            $scope.tedActiveTab = $scope.invocationParams.stateParams.tedActiveTab;
        }

        $scope.readTaxonomyElements();
    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *              delete the custom form data in the taxonomy elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Massive Database Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form data for <b>ALL</b> elements of this taxonomy will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/taxonomy/customform/" + $scope.taxonomyId;

        HttpService.call(POST_URL, "POST", selectedCustomForm.customFormId, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        notify({
                            message: "Custom form Id successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.customForm = selectedCustomForm;
                        $scope.resetSelectedElementDisplay();
                        $scope.readTaxonomyElements();
                    }
                }
            ]
        );
    }

    /**
     * Description: reads from the DL database all elements for the taxonomy identified by $scope.taxonomyId
     */
    $scope.readTaxonomyElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/taxonomy/content/' + $scope.taxonomyId + "/" + $scope.config.abbreviation;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processTaxonomyElementsResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: processes Taxonomy Elements in response from the DL database
     */
    $scope.processTaxonomyElementsResponse = function(response) {
        $scope.spinnerLoading = "";

        $scope.taxonomy_summary.taxonomyId = response.data.result_data.taxonomy_summary.taxonomyId;
        $scope.taxonomy_summary.abbreviationSchemeId = response.data.result_data.taxonomy_summary.abbreviationSchemeId;
        $scope.taxonomy_summary.classId = response.data.result_data.taxonomy_summary.classId;
        $scope.taxonomy_summary.taxonomyName = response.data.result_data.taxonomy_summary.taxonomyName;
        $scope.taxonomy_summary.className = response.data.result_data.taxonomy_summary.className;
        $scope.taxonomy_summary.taxonomyShortDesc = response.data.result_data.taxonomy_summary.taxonomyShortDesc;

        $scope.customForm.customFormId = response.data.result_data.custom_form.customFormId;
        $scope.customForm.customFormName = response.data.result_data.custom_form.customFormName;
        $scope.customForm.schemaDefinition = response.data.result_data.custom_form.schemaDefinitionJson;
        $scope.customForm.formDefinition = response.data.result_data.custom_form.formDefinition;
        $scope.disableCustomFormFields();
        try{
            $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
        }catch (e) {
            console.log("Warning! The custom form definition could not be parsed properly")
        }
        $scope.customForm.formData = {};

        $scope.taxonomyElements = response.data.result_data.taxonomy_elements;
        $scope.refreshSelectedElement();
    }

    /**
     * Description: copies the taxonomy Elements array to the taxonomy Elements table view array
     */
    $scope.refreshElementsTableView = function() {
        if ($scope.config.tableViewInitialised == false) {
            $scope.spinnerLoading = "sk-loading";
            $timeout(function(){
                angular.copy($scope.taxonomyElements, $scope.taxonomyElementsTableView);
                $scope.config.tableViewInitialised = true;
            },0,true);
            $timeout(function(){
                $scope.spinnerLoading = "";
            },0,true);
        }
    }


    /**
     * Description: refreshes the selected element status if elementId is specified in a navigation transition
     */
    $scope.refreshSelectedElement = function() {
        const refreshElementId = $scope.selectedElement.elementId;

        if(refreshElementId > 0) {
            for (let i = 0; i < $scope.taxonomyElements.length; i++ ) {
                if ($scope.taxonomyElements[i].elementId === refreshElementId) {
                    $scope.selectedElement = $scope.taxonomyElements[i];
                    break;
                }
            }
            $scope.loadSelectedElement(refreshElementId);
        }
    }

    /**
     * Enables editable input fields for the Ibox identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        if (dlIboxName === "ted") {
            if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
                notify({
                    message: "You have not selected an element. No edits can be made.",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                // make a copy of current state element core_details
                $scope.elementAttributesUndo.elementName = $scope.selectedElementDisplay.elementName;
                $scope.elementAttributesUndo.elementDesc = $scope.selectedElementDisplay.elementDesc;

                $scope.tedEditingDisabled = false;

                // reset the cfrm edit action
                $scope.cfrmEditingDisabled = true;
            }
        }
        else if (dlIboxName === "cfrm") {
            if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
                notify({
                    message: "You have not selected an element. No edits can be made.",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.enableCustomFormFields();
                $scope.cfrmEditingDisabled = false;

                // reset the ted edit action
                $scope.elementAttributesUndo = {elementName: "", elementDesc: ""};
                $scope.tedEditingDisabled = true;
            }
        }
    }

    /**
     * Saves to the DL DB the changes made in the Ibox identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;

            if ($scope.elementAttributesUndo.elementName == $scope.selectedElementDisplay.elementName &&
               $scope.elementAttributesUndo.elementDesc == $scope.selectedElementDisplay.elementDesc) {
                // nothing to do
            }
            else {
                $scope.saveElementCoreDetails();
            }
        }
        else if (dlIboxName === "cfrm") {
            $scope.cfrmEditingDisabled = true;
            $scope.saveElementCustomFormData();
        }
    }

    /**
     * Disables editable input fields for the Ibox identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            // reset all fields to the pre-edit state
            $scope.selectedElementDisplay.elementName = $scope.elementAttributesUndo.elementName;
            $scope.selectedElementDisplay.elementDesc = $scope.elementAttributesUndo.elementDesc;
            $scope.elementAttributesUndo = {elementName: "", elementDesc: ""};
        }
        else if (dlIboxName === "cfrm") {
            $scope.disableCustomFormFields();
            $scope.cfrmEditingDisabled = true;
        }

    }

    /**
     * Resets all elements in the selectedElementDisplay object to default values
     */
    $scope.resetSelectedElementDisplay = function() {
        $scope.selectedElementDisplay = {
            elementId: 0,
            classId: 0,
            elementParentId: 0,
            elementLevel: 0,
            elementLevelSequence: 0,
            elementLevelGUID: "No element has been selected",
            elementHierarchyNumber: "No element has been selected.",
            elementName: "No element has been selected.",
            elementDesc: "No element has been selected.",
            elementFullPathName: "No element has been selected.",
            elementVisible: 0,
            initialExpandLevel: 0,
            indentCss: "",
            expanded: false,
            parent: false,
            resultSetSequence: 0,
            elementSelected: 0,
            mdContext: []
        }
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    $scope.moveLastToTheBeginning = function () {
        const a = $scope.taxonomyElements.pop();
        $scope.taxonomyElements.splice(0,0, a);
    };

    /**
     * Description: opens the New Taxonomy Element modal dialog with default field values
     */
    $scope.initiateNewElement = function() {
        $scope.newElementConfig.topLevelElement = ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0);

        $scope.newElement = {name: "", desc: ""};

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/taxonomies/create_taxonomy_element.html'),
            controller: 'CreateTaxonomyElementController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: collapses the whole taxonomy down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.taxonomyElements.length; i++) {
            if ($scope.taxonomyElements[i].elementLevel > 0) {
                $scope.taxonomyElements[i].elementVisible = 0;
            }
            $scope.taxonomyElements[i].expanded = false;
        }
    };

    /**
     * Description: makes visible the single element identified by elementId
     */
    $scope.displayNewElement = function (elementId) {
        let searchElementId = elementId;
        let iterationCount = 1;

        while (searchElementId > 0 && iterationCount < 100) {
            for(let i = 0; i < $scope.taxonomyElements.length; i++) {
                if($scope.taxonomyElements[i].elementId === searchElementId) {
                    if ($scope.taxonomyElements[i].elementVisible === 1) {
                        searchElementId = 0;
                    }
                    else {
                        $scope.taxonomyElements[i].elementVisible = 1;
                        searchElementId =  $scope.taxonomyElements[i].elementParentId;
                    }
                    break;
                }
            }
            iterationCount++;
        }
    }

    /**
     * Description: expands every element in the taxonomy
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.taxonomyElements.length; i++) {
            $scope.taxonomyElements[i].elementVisible = 1;
            $scope.taxonomyElements[i].expanded = true;
        }
    };

    /**
     * Description: expands the taxonomy to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.taxonomyElements.length; i++) {
            if ($scope.taxonomyElements[i].elementLevel < $scope.config.visibilityLevel - 1) {
                $scope.taxonomyElements[i].elementVisible = 1;
                $scope.taxonomyElements[i].expanded = true;
            }
            else if ($scope.taxonomyElements[i].elementLevel === $scope.config.visibilityLevel - 1) {
                $scope.taxonomyElements[i].elementVisible = 1;
                $scope.taxonomyElements[i].expanded = false;
            }
            else {
                $scope.taxonomyElements[i].elementVisible = 0;
                $scope.taxonomyElements[i].expanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param element
     */
    $scope.toggleElementExpansion = function(element) {
        const setVisibility = (element.expanded) ? 0 : 1;
        const ehnMatch = element.elementHierarchyNumber + ".";

        if (element.expanded) { // collapse all levels below this element
            for(let i = 0; i < $scope.taxonomyElements.length; i++) {
                if ($scope.taxonomyElements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.taxonomyElements[i].elementLevel > element.elementLevel){
                    $scope.taxonomyElements[i].elementVisible = 0;
                    $scope.taxonomyElements[i].expanded = false;
                }
                if ($scope.taxonomyElements[i].elementId === element.elementId)
                    $scope.taxonomyElements[i].expanded = false;
            }
        }
        else {  // expand all levels below this element
            for(let i = 0; i < $scope.taxonomyElements.length; i++) {
                if ($scope.taxonomyElements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.taxonomyElements[i].elementLevel === element.elementLevel + 1){
                    $scope.taxonomyElements[i].elementVisible = 1;
                }
                if ($scope.taxonomyElements[i].elementId === element.elementId)
                    $scope.taxonomyElements[i].expanded = true;
            }
        }
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.taxonomyElementsTableView = [];
        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.taxonomyElements.length; i++) {
                if ($scope.taxonomyElements[i].parent === false) {
                    endnodes.push($scope.taxonomyElements[i]);
                }
            }
            angular.copy(endnodes, $scope.taxonomyElementsTableView);
        }
        else {
            angular.copy($scope.taxonomyElements, $scope.taxonomyElementsTableView);
        }
    }

    /**
     * Description: saves a new taxonomy element to the DL database and splices it into the $scope.taxonomyElements array
     */
    $scope.saveNewElement = function() {
        $scope.newElementId--;
        $scope.newElementParent = {parentElementIdL: 0, parentResultSetSequence: 0}

        if ($scope.newElementConfig.topLevelElement === true) {
            $scope.newElementParent.parentElementId = 0;
            $scope.newElementParent.parentResultSetSequence = $scope.taxonomyElements.length;
        }
        else {
            $scope.newElementParent.parentElementId = $scope.selectedElement.elementId;
            $scope.newElementParent.parentResultSetSequence = $scope.selectedElement.resultSetSequence;
        }

        const newElement = {
            taxonomyId: $scope.taxonomyId,
            elementId: $scope.newElementId,
            elementParentId: $scope.newElementParent.parentElementId,
            elementName: $scope.newElement.name,
            elementDesc: $scope.newElement.desc,
            customFormData: null
        };

        const POST_URL = apiHost + "/taxonomy/element";

        HttpService.call(POST_URL, "POST", newElement, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processNewElementResponse(response);
                    }
                }
            ]
        );
    }

    $scope.processNewElementResponse = function(response) {
        let savedElement = response.data.result_data;
        savedElement.elementVisible = 1;

        // splice the new element into the $scope.taxonomyElements array at the end of the level_sequence based on parent element
        let i = 0;
        let newResultSetSequence = 0;
        if ($scope.newElementConfig.topLevelElement === false) {
            newResultSetSequence = $scope.newElementParent.parentResultSetSequence + 1;
            for (i = newResultSetSequence; i < $scope.taxonomyElements.length; i++ ) {
                if ($scope.taxonomyElements[i].elementLevel > $scope.selectedElement.elementLevel) {
                    newResultSetSequence++;
                }
                else {
                    break;
                }
            }
            $scope.taxonomyElements.splice(newResultSetSequence, 0, savedElement);

            // update all resultSetSequence values after saved new element
            for (i = newResultSetSequence; i < $scope.taxonomyElements.length; i++) {
                $scope.taxonomyElements[i].resultSetSequence = i;
            }
        }
        else {
            newResultSetSequence = $scope.newElementParent.parentResultSetSequence;
            savedElement.resultSetSequence = $scope.taxonomyElements.length;
            $scope.taxonomyElements.push(savedElement);
        }

        notify({ message: "New taxonomy element saved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate});

    }

    /**
     * Description: saves the core details of an updated element to the DL database
     */
    $scope.saveElementCoreDetails = function() {
        // update the name in selected element
        $scope.taxonomyElements[$scope.selectedElement.resultSetSequence].elementName = $scope.selectedElementDisplay.elementName;

        $scope.coreDetailsDisabled = true;

        const updatedElement = {
            taxonomyId: $scope.taxonomyId,
            elementId: $scope.selectedElement.elementId,
            elementParentId: $scope.selectedElement.elementParentId,
            elementGUID: $scope.selectedElementDisplay.elementGUID,
            elementName: $scope.selectedElementDisplay.elementName,
            elementDesc: $scope.selectedElementDisplay.elementDesc
        };

        // make the REST POST call to save the data store summary info to the database
        const POST_URL = apiHost + "/taxonomy/element";

        HttpService.call(POST_URL, "POST", updatedElement, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        // notify successful update
                        notify.config({duration: 2000});
                        notify({ message: "Updates to taxonomy element saved.",
                            classes: "alert-success",
                            templateUrl: $scope.notifyTemplate});
                    }
                }
            ]
        );
    }

    /**
     * Description: saves to the DL DB the custom form data for selected element
     */
    $scope.saveElementCustomFormData = function() {

        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedElement.elementId,
            classId: $scope.selectedElement.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

        $scope.disableCustomFormFields();
        $scope.cfrmEditingDisabled = true;
    }


    /**
     * Description; reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedElement != null) {
            const GET_URL = apiHost + '/comment/' + $scope.selectedElement.elementId + "/" + $scope.selectedElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response) {
                            $scope.processElementCommentsResponse(response);
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: processes element comments returned from DL database
     * @param response
     */
    $scope.processElementCommentsResponse = function(response) {
        $scope.comment.commentList = response.data.result_data;
    }

    $scope.toggleCommentPanel = function(){
        if($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedElement.elementId,
                classId: $scope.selectedElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL, "POST", newComment, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );

        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a taxonomy element when it is clicked in the hierarchy view
     * @param selectedElement
     * TODO: need to find a way to shift focus to Attributes tab
     */
    $scope.showElementDetails = function(selectedElement) {
        $scope.disableCustomFormFields();
        $scope.changeHistoryVisible = false;
        $scope.selectedElementChangeHistory = [];

        // NOTE: an element must be selected before Move Mode can be enabled, so this function assumes $scope.selectedElement will never be null
        if ($scope.moveElement.moveMode) {
            if (selectedElement.elementHierarchyNumber.startsWith($scope.selectedElement.elementHierarchyNumber)) {
                notify({ message: "You cannot move an element to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.moveElement.moveTargetElement = selectedElement;
                $scope.setActiveTab(5);
            }
        }
        else {
            $scope.taxonomyElements[$scope.previousResultSetSequence].elementSelected = 0;

            $scope.selectedElement = $scope.taxonomyElements[selectedElement.resultSetSequence];
            $scope.taxonomyElements[$scope.selectedElement.resultSetSequence].elementSelected = 1;
            $scope.previousResultSetSequence = $scope.selectedElement.resultSetSequence;

            $scope.resetTedEditing();
            $window.scrollTo(0, 0); // scroll to top so that element details form is visible

            const GET_URL = apiHost + '/taxonomy/element/' + $scope.selectedElement.elementId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            $scope.processElementDetailsResponse(response);
                        }
                    }
                ]
            );
        }

    }

    /**
     * Description: process the details returned from the DL database for the selected element
     * @param response
     */
    $scope.processElementDetailsResponse = function(response) {
        $scope.selectedElementDisplay = response.data.result_data.attributes;
        $scope.customForm.formData = JSON.parse($scope.selectedElementDisplay.customFormData);
        $scope.selectedElementAssociations = response.data.result_data.associations;
        $scope.comment.commentList = response.data.result_data.comments;

        let hierarchyNumberArray = $scope.selectedElementDisplay.elementHierarchyNumber.split(".");
        let trimmedHierarchyNumber = "";
        for (let i = 0; i < hierarchyNumberArray.length; i++) {
            trimmedHierarchyNumber = trimmedHierarchyNumber + parseInt(hierarchyNumberArray[i]) + ".";
        }
        $scope.selectedElementDisplay.elementHierarchyNumber = trimmedHierarchyNumber.substr(0, trimmedHierarchyNumber.length -1);

    }

    /**
     * Description: shows details of the md-context node in a modal dialog
     * TODO: this implementation needs to be completed
     * @param contextNode
     */
    $scope.showMdContextNodeDetails = function(contextNode) {
        console.log(contextNode);
    }

    /**
     * Description: loads the element details for taxonomy element identified by param elementId
     * @param elementId
     */
    $scope.loadSelectedElement = function(elementId) {
        // TODO: this code can be rationalised with the same code in showElementDetails
        const GET_URL = apiHost + '/taxonomy/element/' + elementId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.selectedElementDisplay = response.data.result_data.attributes;
                        $scope.selectedElementAssociations = response.data.result_data.associations;

                        let hierarchyNumberArray = $scope.selectedElementDisplay.elementHierarchyNumber.split(".");
                        let trimmedHierarchyNumber = "";
                        for (let i = 0; i < hierarchyNumberArray.length; i++) {
                            trimmedHierarchyNumber = trimmedHierarchyNumber + parseInt(hierarchyNumberArray[i]) + ".";
                        }
                        $scope.selectedElementDisplay.elementHierarchyNumber = trimmedHierarchyNumber.substr(0, trimmedHierarchyNumber.length -1);
                    }
                }
            ]
        );

    }

    /**
     * Description: searches all elements using keyword
     */
    $scope.executeSearch = function() {
        // first, clear the previous search
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.taxonomyElements[$scope.search.matchList[i].resultSetSequence].elementMatched = false;
        }
        $scope.search.matchList = [];

        // expand all elements so matches can be displayed
        $scope.expandAll();

        const regexp = new RegExp($scope.search.keyword, "gi");
        for(let i = 0; i < $scope.taxonomyElements.length; i++) {
            const result = $scope.taxonomyElements[i].elementName.match(regexp);
            if (result != null) {
                const matchedElement = {
                    elementId: $scope.taxonomyElements[i].elementId,
                    resultSetSequence: $scope.taxonomyElements[i].resultSetSequence,
                    elementName: $scope.taxonomyElements[i].elementName
                }

                $scope.search.matchList.push(matchedElement);
                $scope.taxonomyElements[i].elementMatched = true;
            }
        }
        notify.config({duration: 2000});
        const message = "The Search returned " + $scope.search.matchList.length + " matches.";
        notify({
            message: message,
            classes: "alert-success",
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: clears all search results and collapes the hierarchy to 3 levels
     */
    $scope.clearSearch = function() {
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.taxonomyElements[$scope.search.matchList[i].resultSetSequence].elementMatched = false;
        }
        $scope.search.matchList = [];
        $scope.search.currentViewIndex = null;
        $scope.search.keyword = "";

        $scope.config.visibilityLevel = 3;
        $scope.expandToLevel();
    }

    /**
     * Description: scrolls through the search results, up or down, bringing each element into focus
     */
    $scope.scrollSearchResults = function(direction) {
        if ($scope.search.matchList.length > 0){
            if (direction === 'down') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = 0;
                else if ($scope.search.currentViewIndex === $scope.search.matchList.length - 1)
                    $scope.search.currentViewIndex = 0;
                else
                    $scope.search.currentViewIndex++;
            }
            else if (direction === 'up') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else if ($scope.search.currentViewIndex === 0)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else
                    $scope.search.currentViewIndex--;
            }
            $location.hash($scope.search.matchList[$scope.search.currentViewIndex].elementId);
            $anchorScroll();
        }
    }

    /**
     * Description: toggles the abbreviation setting between 0 (off) and 1 (on)
     * TODO: eventually the user will be able to select abbreviation schemes from this view, which is why it isn't a boolean
     */
    $scope.toggleAbbreviation = function() {
        $scope.config.abbreviation = ($scope.config.abbreviation === 1) ? 0 : 1;
        $scope.readTaxonomyElements();
    }

    /**
     * Description: sets "move element mode" so that the user can select the new parent element
     */
    $scope.toggleMoveMode =  function() {
        if($scope.selectedElement == null || $scope.selectedElement.elementId <= 0){
            notify({
                message: "You have not selected an element.",
                classes: "alert-danger",
                duration: 30000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else{
            $scope.moveElement.moveMode = !$scope.moveElement.moveMode;
            $scope.moveElement.moveTargetElement = null;
        }
    }

    /**
     * Description: moves the initial selected element to the selected target parent element.
     *  It's complicated!
     */
    $scope.executeMove = function() {
        const moveElementDefinition = {
            elementId: $scope.selectedElement.elementId,
            newParentElementId: $scope.moveElement.moveTargetElement.elementId
        };

        const POST_URL = apiHost + "/taxonomy/element/move";

        HttpService.call(POST_URL, "POST", moveElementDefinition, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processMoveElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: post-processing after the move element action has responded
     */
    $scope.processMoveElementResponse = function() {
        notify({ message: "Taxonomy element successfully moved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate});
        $scope.moveElement.moveMode = false;
        $scope.moveElement.moveTargetElement = null;
        $scope.readTaxonomyElements();

    }
    /**
     * Description: moves the initial selected element to the selected target parent element.
     *  It's complicated!
     */
    $scope.executeMove = function() {
        const moveElementDefinition = {
            elementId: $scope.selectedElement.elementId,
            newParentElementId: $scope.moveElement.moveTargetElement.elementId
        };

        const POST_URL = apiHost + "/taxonomy/element/move";

        HttpService.call(POST_URL, "POST", moveElementDefinition, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processMoveElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: post-processing after the move element action has responded
     */
    $scope.processMoveElementResponse = function() {
        notify({ message: "Taxonomy element successfully moved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate});
        $scope.moveElement.moveMode = false;
        $scope.moveElement.moveTargetElement = null;
        $scope.readTaxonomyElements();

    }

    /**
     * No longer used?
     */
    $scope.flattenTaxonomy = function() {
        $scope.flattenedTaxonomy = [];

        for(let i = 0; i < $scope.taxonomyElements.length; i++) {
            const newFlattenedElement = {
                elementId: $scope.taxonomyElements[i].elementId,
                elementFullPathName: $scope.taxonomyElements[i].elementFullPathName,
                childrenCount: $scope.taxonomyElements[i].children.length
            }
            $scope.flattenedTaxonomy.push(newFlattenedElement);

            if($scope.taxonomyElements[i].children.length > 0) {
                $scope.recurseFlattenTaxonomy($scope.taxonomyElements[i].children);
            }
        }
    }

    $scope.recurseFlattenTaxonomy = function(elementChildren) {
        for(let j = 0; j < elementChildren.length; j++) {
            const newFlattenedElement = {
                elementId: elementChildren[j].elementId,
                elementFullPathName: elementChildren[j].elementFullPathName,
                childrenCount: elementChildren[j].children.length
            }
            $scope.flattenedTaxonomy.push(newFlattenedElement);

            if(elementChildren[j].children.length > 0) {
                $scope.recurseFlattenTaxonomy(elementChildren[j].children);
            }
        }
    }

    /**
     * Description: opens the Object Association modal dialog
     */
    $scope.newAssociation = function() {
        if($scope.selectedElement && $scope.selectedElement.elementId>0){
            const currentStateSpec = {
                stateName:"taxonomies.detail",
                objectId:$scope.selectedElement.elementId,
                classId:$scope.selectedElement.classId,
                parentObjectId:$scope.taxonomyId,
                parentClassId:$scope.invocationParams.classId,
                stateParams:{
                    tedActiveTab:{attributes:false, associations:true, comments:false, more:false, activeIndex:2}
                }
            };

            NavigationChainService.storeCurrentState(currentStateSpec);

            const invocationParams = {
                stateName:"associations.new_association",
                objectId:$scope.selectedElement.elementId,
                classId:$scope.selectedElement.classId,
                parentObjectId:0,
                parentClassId:0,
                stateParams:{
                    objectClass:"Taxonomy Element",
                    objectContext:$scope.selectedElementDisplay.elementFullPathName
                }
            };
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
        else{
            notify({
                message: "Please select an element first.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
    }

    /**
     * Description: Opens the full view of associated objects
     */
    $scope.openFullAssociationView = function() {
        const currentStateSpec = {
            stateName:"taxonomies.detail",
            objectId:$scope.selectedElement.elementId,
            classId:$scope.selectedElement.classId,
            parentObjectId:$scope.taxonomyId,
            parentClassId:$scope.invocationParams.classId,
            stateParams:{
                tedActiveTab:{attributes:false, associations:true, comments:false, more:false, activeIndex:2}
            }
        };
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName: "associations.taxonomy_fullview",
            objectId: $scope.taxonomyId,
            classId: $stateParams.invocationParams.classId,
            stateParams: {}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});

    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.tedActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
                break;
            case 2:
                $scope.tedActiveTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "more":false, "activeIndex":2};
                break;
            case 3:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "more":false, "activeIndex":3};
                break;
            case 4:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "more":false, "activeIndex":4};
                break;
            case 5:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":false, "more":true, "activeIndex":5};
                break;
        }
    }

    $scope.resetTedEditing = function () {
        $scope.tedEditingDisabled = true;
    }

    /**
     * Description: Clears the SpinnerLoading overlay
     */
    $scope.clearSpinnerLoading = function () {
        $scope.spinnerLoading = "";
    };

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: opens the confirm delete modal dialog
     * @param deleteTaxonomy
     */
    $scope.confirmDelete = function (deleteTaxonomy) {
        $scope.deleteTaxonomy = deleteTaxonomy;
        if(!$scope.deleteTaxonomy && ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0)) {
            const errorMessage = "You have not selected an element.";
            notify({ message: errorMessage,
                classes: "alert-danger",
                duration: 30000,
                templateUrl: $scope.notifyTemplate});
        }
        else{
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: Delete the selected taxonomy element
     */
    $scope.deleteMdObject = function() {
        if($scope.deleteTaxonomy){
            const DELETE_URL = apiHost + "/taxonomy/" + $scope.taxonomyId;
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            notify({
                                message: "Taxonomy was deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.goBack();
                        }
                    }
                ]
            );

        }
        else{
            const DELETE_URL = apiHost + "/taxonomy/element/" + $scope.selectedElement.elementId;
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            notify({ message: "Taxonomy element was deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate});
                            $scope.moveElement.moveMode = false;
                            $scope.moveElement.moveTargetElement = null;
                            $scope.readTaxonomyElements();
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: shows the Change History table in the More tab and retrieves from the DL DB the change
     *  history for the selected element.
     */
    $scope.showChangeHistory = function() {
        $scope.changeHistoryVisible = true;

        const GET_URL = apiHost + "/taxonomy/element/" + $scope.selectedElement.elementId + "/history";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.selectedElementChangeHistory = response.data.result_data.archivedElementVersions;
                    }
                }
            ]
        );
    }

    /**
     * Description: Hides the Change History table in the More tab and resets change history data array
     */
    $scope.hideChangeHistory = function() {
        $scope.changeHistoryVisible = false;
        $scope.selectedElementChangeHistory = [];
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: click through to taxonomy detail
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId) {
        var currentStateSpec = {
            stateName:"taxonomies.detail",
            objectId:$scope.selectedElement.elementId,
            classId:$scope.selectedElement.classId,
            parentObjectId:$scope.taxonomyId,
            parentClassId:$scope.invocationParams.classId,
            stateParams:{
                tedActiveTab:{attributes:false, associations:true, comments:false, more:false, activeIndex:2}
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {stateName: "", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};
        let moClass = MoClassService.getMoClassByClassId(classId);
        invocationParams.stateName = moClass.objectDetailState;
        invocationParams.objectId = objectId;
        invocationParams.classId = classId;
        invocationParams.parentObjectId = parentObjectId;
        invocationParams.parentClassId = parentClassId;
        invocationParams.stateParams = {};

        if (invocationParams.stateName.length > 0) {
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('TaxonomyDetailController', TaxonomyDetailController);


