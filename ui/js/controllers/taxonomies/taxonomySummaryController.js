/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomy Summary view.
 *
 */
function taxonomySummaryController($scope, $state, $stateParams, $window, $uibModal, NavigationChainService,
                                notify, DTOptionsBuilder, HttpService, RbacService, MoClassService, UtilityService) {
    $scope.taxonomySummary = {
        taxonomyId: 0,
        classId: 0,
        className: "",
        parentObjectId: 0,
        parentClassId: 0,
        taxonomyName: "",
        taxonomyShortDesc: "",
        abbreviation: 0
    };
    $scope.customForm = {};
    $scope.associatedObjects = [];
    $scope.disable = {
        coreDetails: true,
        associations: true
    };

    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Taxonomy Associations'},
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    $scope.initTaxonomySummary = function() {
        $scope.perms.create = !RbacService.checkRbac('ont','create');
        $scope.perms.update = !RbacService.checkRbac('ont','update');
        $scope.perms.delete = !RbacService.checkRbac('ont','delete');
        $scope.perms.publish = !RbacService.checkRbac('ont','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.taxonomySummary.taxonomyId = $scope.invocationParams.objectId;
        $scope.taxonomySummary.classId = $scope.invocationParams.classId;
        $scope.taxonomySummary.abbreviation = ($scope.taxonomySummary.abbreviation === 1) ? 0 : 1;
        $scope.readTaxonomySummary();

    }

    /**
     * Description: retrieves via the DL API the summary information for the taxonomy md_object identified by
     *  $scope.taxonomySummary.taxonomyId
     */
    $scope.readTaxonomySummary = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/taxonomy/summary/' + $scope.taxonomySummary.taxonomyId + '/' +$scope.taxonomySummary.abbreviation;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.taxonomySummary.objectId = response.data.result_data.taxonomy_summary.objectId;
                        $scope.taxonomySummary.classId = response.data.result_data.taxonomy_summary.classId;
                        $scope.taxonomySummary.parentObjectId = response.data.result_data.taxonomy_summary.parentObjectId;
                        $scope.taxonomySummary.parentClassId = response.data.result_data.taxonomy_summary.parentClassId;
                        $scope.taxonomySummary.taxonomyName = response.data.result_data.taxonomy_summary.taxonomyName;
                        $scope.taxonomySummary.taxonomyShortDesc = response.data.result_data.taxonomy_summary.taxonomyShortDesc;
                        $scope.taxonomySummary.className = response.data.result_data.taxonomy_summary.className;
                        $scope.taxonomySummary.objectGUID = response.data.result_data.taxonomy_summary.objectGUID;
                        $scope.customForm = response.data.result_data.taxonomy_summary.customForm;
                        $scope.associatedObjects = response.data.result_data.associations;
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    $scope.dlIboxEdit = function(dlIboxName) {
        switch(dlIboxName){
            case "core_details":
                $scope.disable.coreDetails = false;
                break;
            case "associations":
                $scope.disable.associations = false;
                break;
        }
    }

    $scope.dlIboxSave = function(dlIboxName) {
        switch(dlIboxName){
            case "core_details":
                $scope.disable.coreDetails = true;
                $scope.saveTaxonomyCoreDetails();
                break;
            case "associations":
                $scope.disable.associations = true;
                break;
        }
    }

    $scope.dlIboxCancel = function(dlIboxName) {
        switch(dlIboxName){
            case "core_details":
                $scope.disable.coreDetails = true;
                break;
            case "associations":
                $scope.disable.associations = true;
                break;
        }
    }

    /**
     * Description: manages view transitions in response to customer click actions
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {

        NavigationChainService.storeCurrentState($scope.invocationParams);

        let invocationParams = {
            stateName: "",
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        switch (mode)  {
            case "detail":
                var moClass = MoClassService.getMoClassByClassId(MoClassService.getMoClassIdByCode("TAXONOMY_ELEMENT"));
                invocationParams.stateName = moClass.objectDetailState;
                invocationParams.parentObjectId = $scope.taxonomySummary.taxonomyId;
                invocationParams.parentClassId = $scope.taxonomySummary.classId;
                break;

            case "assoc":
                var moClass = MoClassService.getMoClassByClassId(classId);
                invocationParams.stateName = moClass.objectDetailState;

                invocationParams.stateName = moClass.objectDetailState;
                invocationParams.objectId = objectId;
                invocationParams.classId = classId;
                invocationParams.parentObjectId = parentObjectId;
                invocationParams.parentClassId = parentClassId;
                break;
        }

        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: Saves summary information for a taxonomy to the DL DB the
     */
    $scope.saveTaxonomyCoreDetails = function() {

        const POST_URL = apiHost + "/taxonomy/content";

        HttpService.call(POST_URL, "POST", $scope.taxonomySummary, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The taxonomy summary was updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description:
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description:
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description:
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: prompts the user to confirm a delete action for a taxonomy
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: deletes the Taxonomy MD Object identified by $scope.taxonomySummary.taxonomyId
     *  This function is invoked from the ConfirmDeleteController (the views/dl-common/cmn_confirm_delete.html view)
     */
    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/taxonomy/" + $scope.taxonomySummary.taxonomyId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Taxonomy successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.newAssociation = function() {
        // execute the navigation transition
        const currentStateSpec = $scope.invocationParams;

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"associations.new_association",
            objectId:$scope.taxonomySummary.taxonomyId,
            classId:$scope.taxonomySummary.classId,
            stateParams:{
                objectClass: "Taxonomy Definition",
                objectContext: $scope.taxonomySummary.taxonomyName
            }
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('taxonomySummaryController', taxonomySummaryController);
