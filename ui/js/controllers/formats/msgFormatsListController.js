/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Message Formats List
 */
function MsgFormatsListController($scope, $rootScope, $location, $state, $uibModal, NavigationChainService, notify,
                                  DTOptionsBuilder, HttpService, MoClassService, UtilityService) {
    $scope.gridData = [];


    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'FormatList'},
            {extend: 'pdf', title: 'FormatList'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initMsgFormatsList = function () {
        $scope.readGridData();
    }

    /**
     * Click through to the details view for the selected message format
     * @param objectId
     * @param classId
     * @param objectDetailState
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {
        const currentStateSpec = {
            stateName:"smd.msg_fmt",
            objectId:0,
            classId:0,
            parentObjectId:0,
            parentClassId:0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let targetState = "";
        if (mode == 'detail') {
            let moClass = MoClassService.getMoClassByClassId(parentClassId);
            switch (moClass.classCode) {
                case 'JSON_MESSAGE_FORMAT':
                    targetState = 'smd.fmt_json_detail';
                    break;
                case 'GHMF':
                    targetState = 'smd.fmt_ghmf_detail';
                    break;
                case 'XML_MESSAGE_FORMAT':
                    targetState = 'smd.fmt_xml_detail';
                    break;
                case 'DELIMITED_FLATFILE':
                    targetState = 'smd.fmt_flatfile_detail';
                    break;
                case 'FIXED_WIDTH_FLATFILE':
                    targetState = 'smd.fmt_flatfile_detail';
                    break;
            }
        }
        else if (mode == 'summary') {
            targetState = "smd.fmt_summary";
        }

        const invocationParams = {
            stateName: targetState,
            objectId: objectId,
            classId: classId,
            parentObjectId: parentObjectId,
            parentClassId: parentClassId,
            stateParams: { activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1} }
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: opens the create_message_format modal dialog
     */
    $scope.openCreateMessageFormatsDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/create_msg_format.html'),
            controller: 'CreateFormatController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: creates a new message format in table dl_format
     */
    $scope.createMessageFormat = function(newMessageFormat) {

        const POST_URL = apiHost + '/format/summary';

        HttpService.call(POST_URL, "POST", newMessageFormat, null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.readGridData();
                        notify({
                            message: "New message format saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        let exception = response.data.result_data.exception;
                        let msg = "Exception Class: " + exception.exceptionClass + ". Exception Message: " + exception.exceptionMessage + ".";
                        notify({
                            message: msg,
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description:
     */
    $scope.readGridData = function() {
        const GET_URL = apiHost + '/format/list';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MsgFormatsListController', MsgFormatsListController);
