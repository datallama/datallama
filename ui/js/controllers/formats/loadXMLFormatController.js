/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The controller for the UIB modal dialogue where you can load a XML file
 *              and turn it into a XML message format in the DL data base
 */
function LoadXMLFormatController ($uibModalInstance, $scope, HttpService) {

    $scope.valid = true;
    $scope.spinnerLoading = "";
    $scope.disableButtons = false;
    $scope.setSpinnerLoading = function(value){
        $scope.spinnerLoading = value;
        $scope.disableButtons = (value !== "");
    }

    $scope.checkValid = function(){
        $scope.valid = false;
        if(document.getElementById('inputFile') && (document.getElementById('inputFile')).files[0]){
            const inputFile = (document.getElementById('inputFile')).files[0];
            if(inputFile && inputFile.name && inputFile.name.split(".").length === 2 && inputFile.name.split(".")[1] === "xml"){
                $scope.valid = true;
                $scope.load(inputFile);
            }
        }
    }
    $scope.load = function (inputFile) {
        $scope.setSpinnerLoading("sk-loading");
        const formData = new FormData();
        formData.append('file', inputFile);
        const POST_URL = apiHost + "/format/reverse-engineer/xml/" + $scope.formatId;
        const headers = {'Content-Type': undefined};
        HttpService.call(POST_URL,"POST",formData,headers,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.loadSCB()
                    }
                    //todo: I think there is a good chance that this
                    //      might fail so we should include a callback
                    //      for that situation.
                }
            ]
        );
    };

    $scope.loadSCB = function(){
        $scope.setSpinnerLoading("");
        $scope.readXmlElements();
        $uibModalInstance.close();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('LoadXMLFormatController', LoadXMLFormatController);
