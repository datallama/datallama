/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new flat file format element
 */
function CreateFlatFileElementController ($scope, $uibModalInstance) {

    $scope.validation = {
        elementName: true,
        datatype: true,
        fixedWidthLength: true
    }

    $scope.datatypeList = ['string', 'numeric', 'boolean', 'datetime'];

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.create = function () {
        let valid = 0;
        $scope.validation = {elementName: true, datatype: true, fixedWidthLength: true}

        // validate required fields
        if ($scope.newElement.name.length === 0) {
            $scope.validation.elementName = false;
            valid++;
        }
        if ($scope.newElement.datatype == "none_selected" || $scope.newElement.datatype.length === 0) {
            $scope.validation.datatype = false;
            valid++;
        }

        let fixedWidthLength = parseInt($scope.newElement.fixedWidthLength);
        if (isNaN(fixedWidthLength)) {
            $scope.validation.fixedWidthLength = false;
            valid++;
        }
        else {
            $scope.newElement.fixedWidthLength = fixedWidthLength;
        }

        if (valid === 0) {
            $scope.saveNewElement();
            $uibModalInstance.close();
        }

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateFlatFileElementController', CreateFlatFileElementController);
