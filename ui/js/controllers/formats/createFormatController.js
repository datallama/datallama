/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function CreateFormatController ($scope, $rootScope, $uibModalInstance, HttpService) {
    $scope.messageFormatClassList = [];

    $scope.messageFormat = {
        formatName: "",
        formatShortDesc: "",
        classId: 0
    };
    $scope.selectedMessageFormatClass = {};
    $scope.validation = {
        name: true,
        format: true
    }
    /**
     * Description: initiates the New Format dialog by retrieving the list of message format classes
     */
    $scope.initCreateFormat = function () {

        const GET_URL = apiHost + "/format/class/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.messageFormatClassList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: checks that the user input for the new format is valid.
     */
    $scope.checkValid = function(){
        $scope.validation.name = !!($scope.messageFormat.formatName && $scope.messageFormat.formatName.length > 0);
        $scope.validation.format = !!($scope.selectedMessageFormatClass && $scope.selectedMessageFormatClass.classId > 0);

        if($scope.validation.name && $scope.validation.format){
            $scope.create();
        }
    }

    /**
     * Description: prepares the POST object for a new message format and invokes the createMessageFormat function of the parent controller
     */
    $scope.create = function () {

        const newMessageFormat = {
            formatId: 0,
            formatName: $scope.messageFormat.formatName,
            formatShortDesc: $scope.messageFormat.formatShortDesc,
            classId: $scope.selectedMessageFormatClass.classId,
            createSource: 1
        };

        $scope.createMessageFormat(newMessageFormat);    // this function must be defined in the controller that opens the dialog.
        $uibModalInstance.close();

    };

    /**
     * Description: Cancels the create new format action
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateFormatController', CreateFormatController);
