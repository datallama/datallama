
/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Format JSON Detail view.
 *
 * The structure of the jsonNode object stored in $scope.jsonNodes, $scope.selectedJsonNode and $scope.selectedJsonNodeDisplay is:
 * {formatId: 0, jsonNodeId: 0, classId: 0, parentObjectId: 0, jsonNodeLevel: 0, jsonNodeLevelSequence: 0, jsonNodeType: "s",
 *  jsonNodeGUID: "s", jsonNodeHierarchyNumber: "s", jsonNodeName: "s", jsonNodeShortDesc: "s", jsonNodeFullPathName: "s",
 *  jsonNodeVisible: 0, initialExpandLevel: 0, indentCss": "s", expanded: true, parent: "s", resultSetSequence: 0,
 *  jsonNodeSelected: 0, jsonNodeMatched: false, mdContext: [{classID: 0 className": "s", objectID": 1, objectName: "s", internalPath: "s"}] }
 */
function FmtJsonDetailController($scope, $rootScope, $location, $state, $stateParams, $window, $uibModal, notify,
                                 NavigationChainService, DTOptionsBuilder, DTColumnBuilder, HttpService, RbacService,
                                 MoClassService, UtilityService) {
    $scope.jsonNodes = [];   // this array holds the hierarchy data to be displayed
    $scope.jsonNodesTableView = [];
    $scope.formatId = 0;      // the formatId is specified in the page URL are retrieved with $stateParams
    $scope.jsonSummary = {};
    $scope.selectedJsonNode = null;
    $scope.selectedJsonNodeDisplay = {};
    $scope.selectedJsonNodeDisplayUndo = {};
    $scope.selectedNodeAssociations = [];
    $scope.previousResultSetSequence = 0;
    $scope.newJsonNode = {};
    $scope.newJsonNodeId = 0;
    $scope.newJsonNodeConfig = {topLevelElement: true};
    $scope.jsonFormatExport = {};

    $scope.jsonNodeTypes = [
        {value: "object", nodeType: "object"},
        {value: "array", nodeType: "array"},
        {value: "boolean", nodeType: "boolean"},
        {value: "integer", nodeType: "integer"},
        {value: "number", nodeType: "number"},
        {value: "string", nodeType: "string"}
    ];
    $scope.selectedNodeType = $scope.jsonNodeTypes[3];

    $scope.customForm = {};
    $scope.customFormFields = [];

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false, exportDisplay: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveJsonNode = {moveMode: false, moveTargetJsonNode: null, moveToTop: {selected: false, disabled: false}};
    $scope.deleteMdObjectType = "not_set";

    $scope.tedEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;
    $scope.activeDetailTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notVisible(),
        DTColumnBuilder.newColumn(1).notVisible(),
        DTColumnBuilder.newColumn(2).notVisible(),
        DTColumnBuilder.newColumn(3).notSortable()
    ];

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    $scope.exportFileName = "jsonFormatExport_" + yyyy + "-" + mm + "-" + dd;

    $scope.dtOptions03 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv', title: $scope.exportFileName}
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialisation function for the controller
     */
    $scope.initFmtJsonDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.resetSelectedNodeDisplay();
        $scope.formatId = $scope.invocationParams.parentObjectId;
        $scope.selectedJsonNode = {
            jsonNodeId: 0,
            classId: 0
        }

        $scope.readJsonNodes();

        if($scope.invocationParams.objectId > 0) {
            $scope.activeDetailTab = $scope.invocationParams.stateParams.activeDetailTab;
            $scope.loadSelectedJsonNode($scope.invocationParams.objectId);
        }
    }

    /**
     * Reads all json nodes from the DL database for the json format identified by $scope.formatId
     */
    $scope.readJsonNodes = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/format/json/content/' + $scope.formatId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.jsonSummary = response.data.result_data.format_summary;
                        // Parse the JSON values for custom form
                        $scope.customForm.customFormId = $scope.jsonSummary.customForm.customFormId;
                        $scope.customForm.customFormName = $scope.jsonSummary.customForm.customFormName;
                        $scope.customForm.customFormShortDesc = $scope.jsonSummary.customForm.customFormShortDesc;
                        //$scope.customForm.schemaDefinition = JSON.parse($scope.jsonSummary.customForm.schemaDefinition);
                        $scope.customForm.schemaDefinition = $scope.jsonSummary.customForm.schemaDefinitionJson;
                        $scope.customForm.formDefinition = JSON.parse($scope.jsonSummary.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse('{}');
                        $scope.disableCustomFormFields();

                        $scope.jsonNodes = response.data.result_data.json_nodes;

                        // set selected json node if navigation transition specifies objectId
                        if ($scope.invocationParams.objectId > 0) {
                            for (let i = 0; i < $scope.jsonNodes.length; i++) {
                                if ($scope.jsonNodes[i].jsonNodeId == $scope.invocationParams.objectId) {
                                    $scope.selectedJsonNode = $scope.jsonNodes[i];
                                    $scope.jsonNodes[i].jsonNodeSelected = 1;
                                    $scope.previousResultSetSequence = $scope.selectedJsonNode.resultSetSequence;
                                    break;
                                }
                            }
                        }
                        angular.copy($scope.jsonNodes, $scope.jsonNodesTableView);
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.jsonNodes.length; i++) {
                if ($scope.jsonNodes[i].parent === false) {
                    endnodes.push($scope.jsonNodes[i]);
                }
            }
            angular.copy(endnodes, $scope.jsonNodesTableView);
        }
        else {
            angular.copy($scope.jsonNodes, $scope.jsonNodesTableView);
        }
    }

    $scope.openLoadJSONMessageFormatsDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/load_json_msg_format.html'),
            controller: 'LoadJSONFormatController',
            size: 'md',
            scope: $scope
        });

    }

    $scope.toggleCommentPanel = function(){
        if($scope.selectedJsonNode == null || $scope.selectedJsonNode.elementId <= 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden
        }
    }

    /**
     * Description: enables fields in selected node display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        let elementSelected = true;
        if (dlIboxName === "ted") {
            if ($scope.selectedJsonNode == null || $scope.selectedJsonNode.jsonNodeId <= 0) {
                elementSelected = false;
            }
            else {
                // copy all summary fields to undo object
                angular.copy($scope.selectedJsonNodeDisplay, $scope.selectedJsonNodeDisplayUndo);
                $scope.tedEditingDisabled = false;
            }

        }
        else if (dlIboxName === "cfrm") {
            if ($scope.selectedJsonNode == null || $scope.selectedJsonNode.jsonNodeId <= 0) {
                elementSelected = false;
            }
            else {
                $scope.enableCustomFormFields();
                $scope.cfrmEditingDisabled = false;
            }
        }

        if (!elementSelected) {
            notify({
                message: "You have not selected an element. No edits can be made.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
    }

    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            $scope.saveJsonNodeCoreDetails();
        }
        else if (dlIboxName === "cfrm") {
            $scope.cfrmEditingDisabled = true;
            $scope.disableCustomFormFields();
            $scope.saveSelectedCustomFormData();
        }
    }

    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;

            // reset all fields to the pre-edit state
            angular.copy($scope.selectedJsonNodeDisplayUndo, $scope.selectedJsonNodeDisplay);
        }
        else if (dlIboxName === "cfrm") {
            $scope.disableCustomFormFields();
            $scope.cfrmEditingDisabled = true;
        }
    }

    /**
     * Description: resets variable $scope.selectedJsonNode to default values
     */
    $scope.resetSelectedNodeDisplay = function() {
        $scope.selectedJsonNodeDisplay = {
            jsonNodeId: 0,
            classId: 0,
            jsonNodeParentId: 0,
            jsonNodeLevel: 0,
            jsonNodeLevelSequence: 0,
            jsonNodeLevelGUID: "No json node has been selected",
            jsonNodeHierarchyNumber: "No json node has been selected.",
            jsonNodeName: "No json node has been selected.",
            jsonNodeDesc: "No json node has been selected.",
            jsonNodeFullPathName: "No json node has been selected.",
            jsonNodeVisible: 0,
            initialExpandLevel: 0,
            indentCss: "",
            expanded: false,
            parent: false,
            resultSetSequence: 0,
            jsonNodeSelected: 0,
            mdContext: []
        }

    }


    $scope.remove = function(scope) {
        scope.remove();
    };
    $scope.toggle = function(scope) {
        scope.toggle();
    };
    $scope.moveLastToTheBeginning = function () {
        const a = $scope.data.pop();
        $scope.data.splice(0,0, a);
    };

    /**
     * Description: Initialises the input fields for the create_json_node.html dialog
     */
    $scope.initiateNewJsonNode = function() {
        // TODO: Bug! needs to take into account the "top level" checkbox setting
        $scope.newJsonNodeConfig.topLevelElement = $scope.selectedJsonNode == null;

        $scope.newJsonNode = {name: "", desc: "", type: ""};

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/create_json_node.html'),
            controller: 'CreateJsonNodeController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: collapses the whole taxonomy down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.jsonNodes.length; i++) {
            if ($scope.jsonNodes[i].jsonNodeLevel > 0) {
                $scope.jsonNodes[i].jsonNodeVisible = 0;
            }
            $scope.jsonNodes[i].expanded = false;
        }
    };

    /**
     * Description: expands every element in the taxonomy
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.jsonNodes.length; i++) {
            $scope.jsonNodes[i].jsonNodeVisible = 1;
            $scope.jsonNodes[i].expanded = true;
        }
    };

    /**
     * Description: expands the taxonomy to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.jsonNodes.length; i++) {
            if ($scope.jsonNodes[i].jsonNodeLevel < $scope.config.visibilityLevel - 1) {
                $scope.jsonNodes[i].jsonNodeVisible = 1;
                $scope.jsonNodes[i].expanded = true;
            }
            else if ($scope.jsonNodes[i].jsonNodeLevel === $scope.config.visibilityLevel - 1) {
                $scope.jsonNodes[i].jsonNodeVisible = 1;
                $scope.jsonNodes[i].expanded = false;
            }
            else {
                $scope.jsonNodes[i].jsonNodeVisible = 0;
                $scope.jsonNodes[i].expanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param jsonNode
     */
    $scope.toggleJsonNodeExpansion = function(jsonNode) {
        const setVisibility = (jsonNode.expanded) ? 0 : 1;
        const ehnMatch = jsonNode.jsonNodeHierarchyNumber + ".";

        if (jsonNode.expanded) { // collapse all levels below this element
            for(let i = 0; i < $scope.jsonNodes.length; i++) {
                if ($scope.jsonNodes[i].jsonNodeHierarchyNumber.startsWith(ehnMatch)
                    && $scope.jsonNodes[i].jsonNodeLevel > jsonNode.jsonNodeLevel){
                    $scope.jsonNodes[i].jsonNodeVisible = 0;
                    $scope.jsonNodes[i].expanded = false;
                }
                if ($scope.jsonNodes[i].jsonNodeId === jsonNode.jsonNodeId)
                    $scope.jsonNodes[i].expanded = false;
            }
        }
        else {  // expand all levels below this element
            for(let i = 0; i < $scope.jsonNodes.length; i++) {
                if ($scope.jsonNodes[i].jsonNodeHierarchyNumber.startsWith(ehnMatch)
                    && $scope.jsonNodes[i].jsonNodeLevel === jsonNode.jsonNodeLevel + 1){
                    $scope.jsonNodes[i].jsonNodeVisible = 1;
                }
                if ($scope.jsonNodes[i].jsonNodeId === jsonNode.jsonNodeId)
                    $scope.jsonNodes[i].expanded = true;
            }
        }
    }

    /**
     * Description: sets "move element mode" so that the user can select the new parent element
     */
    $scope.toggleMoveMode =  function() {
        if($scope.selectedJsonNode == null || $scope.selectedJsonNode.jsonNodeId <= 0) {
            notify({
                message: "No element selected.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.moveJsonNode.moveMode = !$scope.moveJsonNode.moveMode;
            $scope.moveJsonNode.moveToTop.selected = false;
            if ($scope.selectedJsonNode.jsonNodeLevel == 0) $scope.moveJsonNode.moveToTop.disabled = true;
            $scope.moveJsonNode.moveTargetJsonNode = null;
        }
    }

    /**
     * Description: moves the initial selected element to the selected target parent element.
     *  It's complicated!
     */
    $scope.executeMove = function() {
        let newParentObjectId = 0;
        if (!$scope.moveJsonNode.moveToTop.selected)
            newParentObjectId = $scope.moveJsonNode.moveTargetJsonNode.jsonNodeId;

        const moveElementDefinition = {
            objectId: $scope.selectedJsonNode.jsonNodeId,
            newParentObjectId: newParentObjectId
        };

        const POST_URL = apiHost + "/format/json/content/node/move";

        HttpService.call(POST_URL, "POST", moveElementDefinition, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "JSON element successfully moved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.moveJsonNode.moveMode = false;
                        $scope.moveJsonNode.moveTargetElement = null;
                        $scope.readJsonNodes();
                    }
                }
            ]
        );
    }

    /**
     * Description: saves a new json node to the DL database and splices it into the $scope.taxonomyElements array
     */
    $scope.saveNewJsonNode = function(newJsonNodeType) {
        $scope.newJsonNodeId = 0;
        let parentObjectId = 0;
        let parentResultSetSequence = 0;

        if ($scope.newJsonNodeConfig.topLevelElement === true) {
            parentObjectId = 0;
            parentResultSetSequence = $scope.jsonNodes.length;
        }
        else {
            parentObjectId = $scope.selectedJsonNode.jsonNodeId;
            parentResultSetSequence = $scope.selectedJsonNode.resultSetSequence;
        }

        const classId = MoClassService.getMoClassIdByCode('JSON_NODE');
        const parentClassId = MoClassService.getMoClassIdByCode('JSON_MESSAGE_FORMAT');
        const newNode = {
            formatId: $scope.formatId,
            parentClassId: parentClassId,
            jsonNodeId: $scope.newJsonNodeId,
            classId: classId,
            parentElementObjectId: parentObjectId,
            jsonNodeName: $scope.newJsonNode.name,
            jsonNodeType: $scope.newJsonNode.type.jsonNodeTypeName,
            jsonNodeShortDesc: $scope.newJsonNode.desc,
            jsonNodeFullPathName: "not set"
        };

        const POST_URL = apiHost + "/format/json/content/node";

        HttpService.call(POST_URL, "POST", newNode, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.saveNewJsonNodeSCB(response, parentResultSetSequence);
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the JSON format definition view after a new node has been saved to the DL DB
     * @param response
     * @param parentResultSetSequence
     */
    $scope.saveNewJsonNodeSCB = function(response, parentResultSetSequence){
        const savedJsonNode = response.data.result_data;
        savedJsonNode.jsonNodeVisible = 1;

        // The new json node will be spliced into the $scope.jsonNodes array at the end of the level based on parent element
        let i = 0;
        let newResultSetSequence = 0;

        if (savedJsonNode.jsonNodeLevel > 0) {  // new element is NOT a top level element
            newResultSetSequence = parentResultSetSequence + 1;
            for (i = newResultSetSequence; i < $scope.jsonNodes.length; i++ ) {
                if ($scope.jsonNodes[i].jsonNodeLevel > $scope.selectedJsonNode.jsonNodeLevel) {
                    newResultSetSequence++;
                }
                else {
                    break;
                }
            }
            $scope.jsonNodes.splice(newResultSetSequence, 0, savedJsonNode);

            // update all resultSetSequence values after saved new element
            for (let i = newResultSetSequence; i < $scope.jsonNodes.length; i++) {
                $scope.jsonNodes[i].resultSetSequence = i;
            }

            // if node is not a top level level node, update new parent node to be a parent
            if(typeof parentResultSetSequence !== "undefined")
                $scope.jsonNodes[parentResultSetSequence].parent = true;
        }
        else {
            savedJsonNode.resultSetSequence = $scope.jsonNodes.length;
            $scope.jsonNodes.push(savedJsonNode);
        }

        notify({
            message: "New JSON element saved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });
    }

    /**
     * Description: saves to the DL DB updates made to the core details of a JSON node
     */
    $scope.saveJsonNodeCoreDetails = function() {
        $scope.coreDetailsDisabled = true;

        const POST_URL = apiHost + "/format/json/content/node";

        HttpService.call(POST_URL,"POST", $scope.selectedJsonNodeDisplay, null,
            [
                {
                    status:1,
                    callback: function(response){
                        // update the selected json node with any values changed in the LHS display panel
                        $scope.selectedJsonNode.jsonNodeName = $scope.selectedJsonNodeDisplay.jsonNodeName;
                        $scope.selectedJsonNode.jsonNodeShortDesc = $scope.selectedJsonNodeDisplay.jsonNodeShortDesc;
                        $scope.selectedJsonNode.jsonNodeType = $scope.selectedJsonNodeDisplay.jsonNodeType;

                        notify({
                            message: "Changes were successfully saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: searches all elements using keyword
     */
    $scope.executeSearch = function() {
        // 1. clear the previous search
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.jsonNodes[$scope.search.matchList[i].resultSetSequence].jsonNodeMatched = false;
        }
        $scope.search.matchList = [];

        // expand all elements so matches can be displayed
        $scope.expandAll();

        const regexp = new RegExp($scope.search.keyword, "gi"); // regexp search is global and case insensitive
        for(let i = 0; i < $scope.jsonNodes.length; i++) {
            const result = $scope.jsonNodes[i].jsonNodeName.match(regexp);
            if (result != null) {
                const matchedElement = {
                    jsonNodeId: $scope.jsonNodes[i].jsonNodeId,
                    resultSetSequence: $scope.jsonNodes[i].resultSetSequence,
                    elementName: $scope.jsonNodes[i].jsonNodeName
                }

                $scope.search.matchList.push(matchedElement);
                $scope.jsonNodes[i].jsonNodeMatched = true;
            }
        }
        notify.config({duration: 5000});
        const message = "The Search returned " + $scope.search.matchList.length + " matches.";
        notify({
            message: message,
            classes: "alert-success",
            templateUrl: $scope.notifyTemplate
        });
    }

    /**
     * Description: scrolls through the search results, up or down, bringing each element into focus.
     *  The parameter 'direction' indicates whether the search is up or down
     * @param direction
     */
    $scope.scrollSearchResults = function(direction) {
        if ($scope.search.matchList.length > 0){
            if (direction === 'down') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = 0;
                else if ($scope.search.currentViewIndex === $scope.search.matchList.length - 1)
                    $scope.search.currentViewIndex = 0;
                else
                    $scope.search.currentViewIndex++;
            }
            else if (direction === 'up') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else if ($scope.search.currentViewIndex === 0)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else
                    $scope.search.currentViewIndex--;
            }
            $location.hash($scope.search.matchList[$scope.search.currentViewIndex].jsonNodeId);
            $anchorScroll();
        }
    }

    /**
     * Description: clears all search results and collapes the hierarchy to 3 levels
     */
    $scope.clearSearch = function() {
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.jsonNodes[$scope.search.matchList[i].resultSetSequence].jsonNodeMatched = false;
        }
        $scope.search.matchList = [];
        $scope.search.currentViewIndex = null;
        $scope.search.keyword = "";

        $scope.config.visibilityLevel = 3;
        $scope.expandToLevel();
    }

    /**
     * Description; reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedJsonNode != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedJsonNode.jsonNodeId + "/" + $scope.selectedJsonNode.classId;

            HttpService.call(GET_URL,"GET",null,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedJsonNode == null) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedJsonNode.jsonNodeId,
                classId: $scope.selectedJsonNode.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL, "POST", newComment, null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a taxonomy element when it is clicked in the hierarchy view
     * @param selectedJsonNode
     */
    $scope.showJsonNodeDetails = function(selectedJsonNode) {

        if ($scope.moveJsonNode.moveMode) {
            if (selectedJsonNode.jsonNodeHierarchyNumber.startsWith($scope.selectedJsonNode.jsonNodeHierarchyNumber)) {
                notify({ message: "You cannot move an element to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.moveJsonNode.moveTargetJsonNode = selectedJsonNode;
                $scope.setActiveTab(4);
            }
        }
        else {
            $scope.jsonNodes[$scope.previousResultSetSequence].jsonNodeSelected = 0;

            $scope.selectedJsonNode = $scope.jsonNodes[selectedJsonNode.resultSetSequence];
            $scope.jsonNodes[$scope.selectedJsonNode.resultSetSequence].jsonNodeSelected = 1;
            $scope.previousResultSetSequence = $scope.selectedJsonNode.resultSetSequence;

            $scope.resetEditing();
            $window.scrollTo(0, 0);

            const GET_URL = apiHost + '/format/json/content/node/' + $scope.selectedJsonNode.jsonNodeId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.processSelectedJsonNodeResponse(response);
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: loads the selected JSON Node when returning to the view from a Go Back action
     * @param jsonNodeId
     */
    $scope.loadSelectedJsonNode = function(jsonNodeId) {

        // 2. get full details of the node from the DL database
        const GET_URL = apiHost + '/format/json/content/node/' + jsonNodeId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processSelectedJsonNodeResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: loads the response from the DL database into scope variables for display in the Details panel
     * @param response
     */
    $scope.processSelectedJsonNodeResponse = function(response) {
        $scope.selectedJsonNodeDisplay = response.data.result_data.attributes;
        $scope.selectedJsonNodeDisplay.jsonNodeHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedJsonNodeDisplay.jsonNodeHierarchyNumber);
        $scope.selectedNodeAssociations = response.data.result_data.associations;
        $scope.customForm.formData = JSON.parse(response.data.result_data.custom_form.formData);
        $scope.readElementComments();
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: checks whether the logged in user has permissions on the module and actionCategory specified in parameters
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteMdObjectType) {
        $scope.deleteMdObjectType = deleteMdObjectType;
        let check = 0;

        // if the mdObject type == 'element', check that an element is selected
        if ($scope.deleteMdObjectType == 'element') {
            if($scope.selectedJsonNode == null || $scope.selectedJsonNode.jsonNodeId <= 0) {
                check = 1;
                const errorMessage = "You have not selected an element.";
                notify({ message: errorMessage,
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
        }

        if (check == 0) {
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: calls the DL API to delete the select JSON node or the entire JSON format.
     */
    $scope.deleteMdObject = function () {
        let DELETE_URL = apiHost;
        switch ($scope.deleteMdObjectType) {
            case 'element':
                DELETE_URL = DELETE_URL + "/format/json/content/node/" + $scope.selectedJsonNode.jsonNodeId;
                break;
            case 'format':
                DELETE_URL = DELETE_URL + "/format/" + $scope.formatId;
                break;
            default:
                DELETE_URL = "not_set";
                break;
        }

        if (DELETE_URL != "not_set") {
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            notify({
                                message: "JSON " + $scope.deleteMdObjectType + " successfully deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });

                            if ($scope.deleteMdObjectType == 'element')
                                $scope.readJsonNodes();
                            else if ($scope.deleteMdObjectType == 'format')
                                $scope.goBack();
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.activeDetailTab = {"attributes":true, "associations":false, "comments":false, "activeIndex":1};
                break;
            case 2:
                $scope.activeDetailTab = {"attributes":false, "associations":true, "comments":false, "activeIndex":2};
                break;
            case 3:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":true, "activeIndex":3};
                break;
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.tedEditingDisabled = true;
        $scope.cfrmEditingDisabled = true;
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
        $scope.disableCustomFormFields();
    }

    /**
     * @param silent
     * @returns {boolean}
     */
    $scope.checkElementSelected = function(silent) {
        if ($scope.selectedJsonNode == null || $scope.selectedJsonNode.jsonNodeId <= 0) {
            if(silent !== true){
                notify({
                    message: "No element selected.",
                    classes: "alert-danger",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            return false;
        }
        return true;
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form attributes for this table/view and all columns " +
                "that inherit the custom form definition will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: This function is invoked when the Change Custom Form warning dialog is closed by the OK button.
     */
    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Open a dialog to select a custom form for the JSON definition
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/format/customform/";
        const postValues = {
            "objectId": $scope.formatId,
            "classId": $scope.jsonSummary.classId,
            "customFormId": selectedCustomForm.customFormId
        };

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form association successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.updateCustomFormDefinition(selectedCustomForm.customFormId);
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the custom form definition after it has been change by the user.
     * @param customFormId
     */
    $scope.updateCustomFormDefinition = function(customFormId) {
        const GET_URL = apiHost + "/common/customform/" + customFormId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.customForm = response.data.result_data;
                        $scope.customForm.schemaDefinition = JSON.parse($scope.customForm.schemaDefinition);
                        $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data to the DL DB for the selected json node
     */
    $scope.saveSelectedCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedJsonNode.jsonNodeId,
            classId: $scope.selectedJsonNode.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: displays the format export view and prepares the export data
     */
    $scope.generateFormatExport = function() {
        $scope.config.exportDisplay = true;

        // 1. prepare the custom form fields array
        $scope.customFormFields = [];
        for (const propName in $scope.customForm.schemaDefinition.properties) {
            const cfrmFieldDefn = {fieldName: propName, fieldTitle: $scope.customForm.schemaDefinition.properties[propName].title}
            $scope.customFormFields.push(cfrmFieldDefn);
        }

        // 2. get the export data
        const GET_URL = apiHost + '/format/json/' + $scope.formatId + "/export";
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.jsonFormatExport = response.data.result_data;
                    }
                }
            ]
        );

    }

    /**
     * Description: returns the value for the custom form field specified in cfrmField.fieldName
     * @param cfrmField
     * @param customFormData
     * @returns {string}
     */
    $scope.getCustomFormFieldValue = function(cfrmField, customFormData) {
        let displayValue = "";
        const customFormDataJson = JSON.parse(customFormData);

        for (const fieldName in customFormDataJson) {
            if (fieldName == cfrmField.fieldName) {
                displayValue = customFormDataJson[fieldName];
            }
        }
        return displayValue;
    }

    /**
     * Description: closes the export view and resets the export data array.
     */
    $scope.closeExportView = function() {
        $scope.jsonFormatExport = [];
        $scope.config.exportDisplay = false;
    }

    /**
     * Description: Click through to the details view for the selected message format
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {

        if(mode !== "default" && mode !== "fullAssoc" && !$scope.checkElementSelected()){
            return;
        }
        const currentStateSpec = {
            stateName: $scope.invocationParams.stateName,
            objectId: $scope.selectedJsonNode.jsonNodeId,
            classId: $scope.selectedJsonNode.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            stateParams:{
                activeDetailTab: $scope.activeDetailTab
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "fullAssoc":
                stateName = "associations.mdobject_fullview";
                stateParams = {
                    objectClass: "JSON FORMAT",
                    objectContext: ""
                }
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass: "JSON Node",
                    objectContext: $scope.selectedJsonNode.mdContext[1].internalPath
                };
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('FmtJsonDetailController', FmtJsonDetailController);


