/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the General Hierarchical Message Format (GHMF) Detail view.
 *  The structure of $scope.selectedElement is:
 *  {formatId: 0, objectId: 0, classId: 0, elementParentId: 0, elementLevel: 0, elementLevelSequence: 0, elementGUID: "",
 *   elementHierarchyNumber: "", elementName: "", elementDesc: "", elementFullPathName: ""}
 */
function FmtGhmfDetailController($scope, $rootScope, $location, $state, $stateParams, $window, $anchorScroll, $uibModal, notify,
                                 NavigationChainService, DTOptionsBuilder, DTColumnBuilder, RbacService, MoClassService,
                                 HttpService, UtilityService) {
    $scope.ghmfElements = [];   // this array holds all the elements returned from the database
    $scope.ghmfElementsTableView = [];      // this array is used to display on the table view of the format elements

    $scope.formatId = 0;      // the formatId is specified in the page URL are retrieved with $stateParams
    $scope.formatSummary = {};
    $scope.selectedElement = null;
    $scope.selectedElementDisplay = {};
    $scope.selectedElementDisplayUndo = {};
    $scope.previousResultSetSequence = 0;
    $scope.selectedElementAssociations = [];
    $scope.newElement = {};
    $scope.newElementId = 0;
    $scope.returnId = 0;
    $scope.newElementConfig = {topLevelElement: true};

    $scope.customForm = {};
    $scope.customFormFields = [];

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveElement = {moveMode: false, moveTargetElement: null, moveToTop: {selected: false, disabled: false}};
    $scope.deleteMdObjectType = "not_set";

    $scope.tedEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;

    $scope.tedActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'GHMF Elements'}
        ]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notVisible(),
        DTColumnBuilder.newColumn(1).notVisible(),
        DTColumnBuilder.newColumn(2).notVisible(),
        DTColumnBuilder.newColumn(3).notSortable()
    ];

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.spinnerLoading = "";


    /**
     * Description: initialisation function for the controller
     */
    $scope.initGhmfDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.resetSelectedElementDisplay();
        $scope.formatId = $scope.invocationParams.parentObjectId;

        $scope.selectedElement = {
            formatId: 0,
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            elementTypeId: 0,
            elementLevel: 0,
            elementLevelSequence: 0,
            elementGUID: "",
            elementHierarchyNumber: "",
            elementName: "",
            elementDesc: ""
        };

        $scope.readElements();

        if($scope.invocationParams.objectId > 0) {
            $scope.tedActiveTab = $scope.invocationParams.stateParams.tedActiveTab;
            $scope.loadSelectedElement($scope.invocationParams.objectId);
        }
    }

    /**
     * Description: reads from the DL database all elements for the GHMF format identified by $scope.formatId
     */
    $scope.readElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/format/ghmf/content/' + $scope.formatId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.formatSummary = response.data.result_data.format_summary;
                        $scope.ghmfElements = response.data.result_data.elements;

                        // Parse the JSON values for custom form
                        $scope.customForm.customFormId = $scope.formatSummary.customForm.customFormId;
                        $scope.customForm.customFormName = $scope.formatSummary.customForm.customFormName;
                        $scope.customForm.customFormShortDesc = $scope.formatSummary.customForm.customFormShortDesc;
                        $scope.customForm.schemaDefinition = $scope.formatSummary.customForm.schemaDefinitionJson;
                        $scope.customForm.formDefinition = JSON.parse($scope.formatSummary.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse('{}');

                        // set selected json node if navigation transition specifies objectId
                        if ($scope.invocationParams.objectId > 0) {
                            for (let i = 0; i < $scope.ghmfElements.length; i++) {
                                if ($scope.ghmfElements[i].objectId == $scope.invocationParams.objectId) {
                                    $scope.selectedElement = $scope.ghmfElements[i];
                                    $scope.ghmfElements[i].elementSelected = 1;
                                    $scope.previousResultSetSequence = $scope.selectedElement.resultSetSequence;
                                    break;
                                }
                            }
                        }
                        angular.copy($scope.ghmfElements, $scope.ghmfElementsTableView);
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.ghmfElements.length; i++) {
                if ($scope.ghmfElements[i].parent === false) {
                    endnodes.push($scope.ghmfElements[i]);
                }
            }
            angular.copy(endnodes, $scope.ghmfElementsTableView);
        }
        else {
            angular.copy($scope.ghmfElements, $scope.ghmfElementsTableView);
        }
    }

    $scope.toggleCommentPanel = function(){
        if($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden
        }
    }

    /**
     * Description: enables editing for the iBox in the LHS details panel identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "You have not selected an element. No edits can be made.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else if (dlIboxName === "ted") {
            // copy all summary fields to undo object
            angular.copy($scope.selectedElementDisplay, $scope.selectedElementDisplayUndo);
            $scope.tedEditingDisabled = false;
        }
        else if (dlIboxName === "cfrm") {
            $scope.enableCustomFormFields();
            $scope.cfrmEditingDisabled = false;
        }

    }

    /**
     * Description: saves changes made in the LHS detail panel
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            $scope.saveElementCoreDetails();
        }
        else if (dlIboxName === "cfrm") {
            $scope.cfrmEditingDisabled = true;
            $scope.disableCustomFormFields();
            $scope.saveSelectedCustomFormData();
        }
    }

    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            // reset all fields to the pre-edit state
            angular.copy($scope.selectedElementDisplayUndo, $scope.selectedElementDisplay);
        }
        else if (dlIboxName === "cfrm") {
            $scope.disableCustomFormFields();
            $scope.cfrmEditingDisabled = true;
        }

    }


    $scope.resetSelectedElementDisplay = function() {
        $scope.selectedElementDisplay = {
            elementId: 0,
            classId: 0,
            elementParentId: 0,
            elementLevel: 0,
            elementLevelSequence: 0,
            elementLevelGUID: "No element has been selected",
            elementHierarchyNumber: "No element has been selected.",
            elementName: "No element has been selected.",
            elementDesc: "No element has been selected.",
            elementFullPathName: "No element has been selected.",
            elementVisible: 0,
            initialExpandLevel: 0,
            indentCss: "",
            expanded: false,
            parent: false,
            resultSetSequence: 0,
            elementSelected: 0,
            mdContext: []
        }
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: checks whether the logged in user has permissions on the module and actionCategory specified in parameters
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    $scope.moveLastToTheBeginning = function () {
        const a = $scope.ghmfElements.pop();
        $scope.ghmfElements.splice(0,0, a);
    };

    /**
     * Description: opens the New Format Element modal dialog with default field values
     */
    $scope.initiateNewElement = function() {
        $scope.newElementConfig.topLevelElement = $scope.selectedElement == null;

        $scope.newElement = {name: "", desc: ""};

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/create_ghmf_element.html'),
            controller: 'CreateGhmfElementController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: opens the confirm delete modal dialog. The parameter deleteMdObject
     * @param deleteMdObjectType
     */
    $scope.confirmDelete = function (deleteMdObjectType) {
        $scope.deleteMdObjectType = deleteMdObjectType;
        let check = 0;

        // if the mdObject type == 'element', check that an element is selected
        if ($scope.deleteMdObjectType == 'element') {
            if($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
                check = 1;
                const errorMessage = "You have not selected an element.";
                notify({ message: errorMessage,
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate});
            }
        }

        if (check == 0) {
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: invokes the DL API to delete the selected GHMF element or the entire GHMF format definition.
     */
    $scope.deleteMdObject = function () {

        let DELETE_URL = apiHost;
        switch ($scope.deleteMdObjectType) {
            case 'element':
                DELETE_URL = DELETE_URL + "/format/ghmf/content/element/" + $scope.selectedElement.objectId;
                break;
            case 'format':
                DELETE_URL = DELETE_URL + "/format/" + $scope.formatId;
                break;
            default:
                DELETE_URL = "not_set";
                break;
        }

        if (DELETE_URL != "not_set") {
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            notify({
                                message: "GHMF " + $scope.deleteMdObjectType + " successfully deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            if ($scope.deleteMdObjectType == 'element')
                                $scope.readElements();
                            else if ($scope.deleteMdObjectType == 'format')
                                $scope.goBack();
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: collapses the whole format hierarchy down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.ghmfElements.length; i++) {
            if ($scope.ghmfElements[i].elementLevel > 0) {
                $scope.ghmfElements[i].elementVisible = 0;
            }
            $scope.ghmfElements[i].expanded = false;
        }
    };

    /**
     * Description: makes visible the single element identified by elementId
     */
    $scope.displayNewElement = function (elementId) {
        let searchElementId = elementId;
        let iterationCount = 1;

        while (searchElementId > 0 && iterationCount < 100) {
            for(let i = 0; i < $scope.ghmfElements.length; i++) {
                if($scope.ghmfElements[i].elementId === searchElementId) {
                    if ($scope.ghmfElements[i].elementVisible === 1) {
                        searchElementId = 0;
                    }
                    else {
                        $scope.ghmfElements[i].elementVisible = 1;
                        searchElementId =  $scope.ghmfElements[i].elementParentId;
                    }
                    break;
                }
            }
            iterationCount++;
        }
    }

    /**
     * Description: expands every element in the message format hierarchy
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.ghmfElements.length; i++) {
            $scope.ghmfElements[i].elementVisible = 1;
            $scope.ghmfElements[i].expanded = true;
        }
    };

    /**
     * Description: expands the message format hierarchy to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.ghmfElements.length; i++) {
            if ($scope.ghmfElements[i].elementLevel < $scope.config.visibilityLevel - 1) {
                $scope.ghmfElements[i].elementVisible = 1;
                $scope.ghmfElements[i].expanded = true;
            }
            else if ($scope.ghmfElements[i].elementLevel === $scope.config.visibilityLevel - 1) {
                $scope.ghmfElements[i].elementVisible = 1;
                $scope.ghmfElements[i].expanded = false;
            }
            else {
                $scope.ghmfElements[i].elementVisible = 0;
                $scope.ghmfElements[i].expanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param element
     */
    $scope.toggleElementExpansion = function(element) {
        const ehnMatch = element.elementHierarchyNumber + ".";

        if (element.expanded) { // collapse all levels below this element
            for(let i = 0; i < $scope.ghmfElements.length; i++) {
                if ($scope.ghmfElements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.ghmfElements[i].elementLevel > element.elementLevel){
                    $scope.ghmfElements[i].elementVisible = 0;
                    $scope.ghmfElements[i].expanded = false;
                }
                if ($scope.ghmfElements[i].objectId === element.objectId)
                    $scope.ghmfElements[i].expanded = false;
            }
        }
        else {  // expand all levels below this element
            for(let i = 0; i < $scope.ghmfElements.length; i++) {
                if ($scope.ghmfElements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.ghmfElements[i].elementLevel === element.elementLevel + 1){
                    $scope.ghmfElements[i].elementVisible = 1;
                }
                if ($scope.ghmfElements[i].objectId === element.objectId)
                    $scope.ghmfElements[i].expanded = true;
            }
        }
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.ghmfElementsTableView = [];
        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.ghmfElements.length; i++) {
                if ($scope.ghmfElements[i].parent === false) {
                    endnodes.push($scope.ghmfElements[i]);
                }
            }
            angular.copy(endnodes, $scope.ghmfElementsTableView);
        }
        else {
            angular.copy($scope.ghmfElements, $scope.ghmfElementsTableView);
        }
    }

    /**
     * Description: saves a new format element to the DL database and splices it into the $scope.taxonomyElements array
     */
    $scope.saveNewElement = function() {
        let parentElementId;
        let parentResultSetSequence = 0;

        if ($scope.newElementConfig.topLevelElement === true) {
            parentElementId = 0;
            parentResultSetSequence = $scope.ghmfElements.length;
        }
        else {
            parentElementId = $scope.selectedElement.objectId;
            parentResultSetSequence = $scope.selectedElement.resultSetSequence;
        }

        const newElement = {
            parentObjectId: $scope.formatId,
            parentClassId: 16,
            objectId: $scope.newElementId,
            classId: 17,
            parentElementObjectId: parentElementId,
            elementName: $scope.newElement.name,
            elementType: "string",
            elementDesc: $scope.newElement.desc
        };

        const POST_URL = apiHost + "/format/ghmf/content/element";

        HttpService.call(POST_URL, "POST", newElement, null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.saveNewElementSCB(response, parentResultSetSequence);
                    }
                }
            ]
        );

    }

    /**
     * Description: update the GHMF hierarchy display after saving a new element to the DL DB
     * @param response
     * @param parentResultSetSequence
     */
    $scope.saveNewElementSCB = function(response, parentResultSetSequence){
        const savedElement = response.data.result_data;
        savedElement.elementVisible = 1;

        // splice the new element into the $scope.taxonomyElements array at the end of the level_sequence based on parent element
        let i = 0;
        let newResultSetSequence = 0;
        if ($scope.newElementConfig.topLevelElement === false) {
            newResultSetSequence = parentResultSetSequence + 1;
            for (i = newResultSetSequence; i < $scope.ghmfElements.length; i++ ) {
                if ($scope.ghmfElements[i].elementLevel > $scope.selectedElement.elementLevel) {
                    newResultSetSequence++;
                }
                else {
                    break;
                }
            }
            $scope.ghmfElements.splice(newResultSetSequence, 0, savedElement);

            // update all resultSetSequence values after saved new element
            for (i = newResultSetSequence; i < $scope.ghmfElements.length; i++) {
                $scope.ghmfElements[i].resultSetSequence = i;
            }

            // update new parent node to be a parent
            $scope.ghmfElements[parentResultSetSequence].parent = true;
        }
        else {
            newResultSetSequence = parentResultSetSequence;
            savedElement.resultSetSequence = $scope.ghmfElements.length;
            $scope.ghmfElements.push(savedElement);
        }

        notify({
            message: "New message format element saved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });
    }

    /**
     * Description: saves the core details of an updated element to the DL database
     */
    $scope.saveElementCoreDetails = function() {
        // update the name in selected element
        $scope.ghmfElements[$scope.selectedElement.resultSetSequence].elementName = $scope.selectedElementDisplay.elementName;

        $scope.coreDetailsDisabled = true;

        const updatedElement = {
            parentObjectId: $scope.formatId,
            parentClassId: 16,
            objectId: $scope.selectedElement.objectId,
            classId: 17,
            parentElementObjectId: $scope.selectedElement.parentObjectId,
            elementName: $scope.selectedElement.elementName,
            elementType: "string",
            elementDesc: $scope.selectedElementDisplay.elementDesc
        };


        // make the REST POST call to save the data store summary info to the database
        const POST_URL = apiHost + "/format/ghmf/content/element";

        HttpService.call(POST_URL, "POST", updatedElement, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Updates to taxonomy element saved.",
                            duration: 2000,
                            classes: "alert-success",
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description; reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedElement.objectId + "/" + $scope.selectedElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedElement == null) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedElement.objectId,
                classId: $scope.selectedElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL,"POST",newComment,null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a taxonomy element when it is clicked in the hierarchy view
     * @param selectedElement
     */
    $scope.showElementDetails = function(selectedElement) {

        // NOTE: an element must be selected before Move Mode can be enabled, so this function assumes $scope.selectedElement will never be null
        if ($scope.moveElement.moveMode) {
            if (selectedElement.elementHierarchyNumber.startsWith($scope.selectedElement.elementHierarchyNumber)) {
                notify({
                    message: "You cannon move an element to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.moveElement.moveTargetElement = selectedElement;
                $scope.setActiveTab(5);
            }
        }
        else {
            $scope.ghmfElements[$scope.previousResultSetSequence].elementSelected = 0;

            $scope.selectedElement = $scope.ghmfElements[selectedElement.resultSetSequence];
            if ($scope.selectedElement.elementLevel == 0)
                $scope.moveElement.moveToTop.disabled = true
            else
                $scope.moveElement.moveToTop.disabled = false
            $scope.ghmfElements[$scope.selectedElement.resultSetSequence].elementSelected = 1;
            $scope.previousResultSetSequence = $scope.selectedElement.resultSetSequence;

            $scope.resetEditing();
            $window.scrollTo(0, 0); // scroll to top so that element details form is visible

            const GET_URL = apiHost + '/format/ghmf/content/element/' + $scope.selectedElement.objectId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.processSelectedElementResponse(response);
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: loads the element details for taxonomy element identified by param elementId
     * @param elementId
     */
    $scope.loadSelectedElement = function(elementId) {
        const GET_URL = apiHost + '/format/ghmf/content/element/' + $scope.invocationParams.objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processSelectedElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: processes the element details returned by API to support display in the view
     * @param response
     */
    $scope.processSelectedElementResponse = function(response) {
        $scope.selectedElementDisplay = response.data.result_data.attributes;
        $scope.selectedElementDisplay.elementHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedElementDisplay.elementHierarchyNumber);

        $scope.selectedElementAssociations = response.data.result_data.associations;
        $scope.comment.commentList = response.data.result_data.comments;
        $scope.customForm.formData = JSON.parse(response.data.result_data.custom_form.formData);
    }

    /**
     * Description: searches all elements using keyword
     */
    $scope.executeSearch = function() {
        // first, clear the previous search
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.ghmfElements[$scope.search.matchList[i].resultSetSequence].elementMatched = false;
        }
        $scope.search.matchList = [];

        // expand all elements so matches can be displayed
        $scope.expandAll();

        const regexp = new RegExp($scope.search.keyword, "gi");
        for(let i = 0; i < $scope.ghmfElements.length; i++) {
            const result = $scope.ghmfElements[i].elementName.match(regexp);
            if (result != null) {
                const matchedElement = {
                    elementId: $scope.ghmfElements[i].elementId,
                    resultSetSequence: $scope.ghmfElements[i].resultSetSequence,
                    elementName: $scope.ghmfElements[i].elementName
                }

                $scope.search.matchList.push(matchedElement);
                $scope.ghmfElements[i].elementMatched = true;
            }
        }
        notify.config({duration: 2000});
        const message = "The Search returned " + $scope.search.matchList.length + " matches.";
        notify({
            message: message,
            classes: "alert-success",
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: clears all search results and collapes the hierarchy to 3 levels
     */
    $scope.clearSearch = function() {
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.ghmfElements[$scope.search.matchList[i].resultSetSequence].elementMatched = false;
        }
        $scope.search.matchList = [];
        $scope.search.currentViewIndex = null;
        $scope.search.keyword = "";

        $scope.config.visibilityLevel = 3;
        $scope.expandToLevel();
    }

    /**
     * Description: scrolls through the search results, up or down, bringing each element into focus
     */
    $scope.scrollSearchResults = function(direction) {
        if ($scope.search.matchList.length > 0){
            if (direction === 'down') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = 0;
                else if ($scope.search.currentViewIndex === $scope.search.matchList.length - 1)
                    $scope.search.currentViewIndex = 0;
                else
                    $scope.search.currentViewIndex++;
            }
            else if (direction === 'up') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else if ($scope.search.currentViewIndex === 0)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else
                    $scope.search.currentViewIndex--;
            }
            $location.hash($scope.search.matchList[$scope.search.currentViewIndex].elementId);
            $anchorScroll();
        }
    }

    /**
     * Description: toggles the abbreviation setting between 0 (off) and 1 (on)
     * TODO: eventually the user will be able to select abbreviation schemes from this view, which is why it isn't a boolean
     */
    $scope.toggleAbbreviation = function() {
        $scope.config.abbreviation = ($scope.config.abbreviation === 1) ? 0 : 1;
        $scope.readElements();
    }

    /**
     * Description: sets "move element mode" so that the user can select the new parent element
     */
    $scope.toggleMoveMode =  function() {
        if($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "No element selected.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.moveElement.moveMode = !$scope.moveElement.moveMode;
            $scope.moveElement.moveToTop.selected = false;
            if ($scope.selectedElement.elementLevel == 0) $scope.moveElement.moveToTop.disabled = true;
            $scope.moveElement.moveTargetElement = null;
        }
    }

    /**
     * Description: moves the initial selected element to the selected target parent element.
     *  It's complicated!
     */
    $scope.executeMove = function() {
        let newParentObjectId = 0;
        if (!$scope.moveElement.moveToTop.selected)
            newParentObjectId = $scope.moveElement.moveTargetElement.objectId;

        const moveElementDefinition = {
            objectId: $scope.selectedElement.objectId,
            newParentObjectId: newParentObjectId
        };

        const POST_URL = apiHost + "/format/ghmf/content/element/move";

        HttpService.call(POST_URL, "POST", moveElementDefinition, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Format element successfully moved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.moveElement.moveMode = false;
                        $scope.moveElement.moveTargetElement = null;
                        $scope.readElements();
                    }
                }
            ]
        );
    }

    /**
     * Description:
     * @param silent
     * @returns {boolean}
     */
    $scope.checkElementSelected = function(silent) {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            if(silent !== true){
                notify({
                    message: "No element selected",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            return false;
        }
    return true;
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.tedActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
                break;
            case 2:
                $scope.tedActiveTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "more":false, "activeIndex":2};
                break;
            case 3:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "more":false, "activeIndex":3};
                break;
            case 4:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "more":false, "activeIndex":4};
                break;
            case 5:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":false, "more":true, "activeIndex":5};
                break;
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.tedEditingDisabled = true;
        $scope.cfrmEditingDisabled = true;
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
        $scope.disableCustomFormFields();
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form attributes for this table/view and all columns " +
                "that inherit the custom form definition will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: This function is invoked when the Change Custom Form warning dialog is closed by the OK button.
     */
    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Open a dialog to select a custom form for the JSON definition
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/format/customform/";
        const postValues = {
            "objectId": $scope.formatId,
            "classId": $scope.invocationParams.parentClassId,
            "customFormId": selectedCustomForm.customFormId
        };

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form association successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.updateCustomFormDefinition(selectedCustomForm.customFormId);
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the custom form definition after it has been change by the user.
     * @param customFormId
     */
    $scope.updateCustomFormDefinition = function(customFormId) {
        const GET_URL = apiHost + "/common/customform/" + customFormId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.customForm = response.data.result_data;
                        $scope.customForm.schemaDefinition = JSON.parse($scope.customForm.schemaDefinition);
                        $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data to the DL DB for the selected json node
     */
    $scope.saveSelectedCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedElement.objectId,
            classId: $scope.selectedElement.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: clears the data loading spinner.
     */
    $scope.clearSpinnerLoading = function () {
        $scope.spinnerLoading = "";
    };

    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {

        if(mode !== "default" && mode !== "fullAssoc" && !$scope.checkElementSelected()){
            return;
        }

        const currentStateSpec = {
            stateName:"smd.fmt_ghmf_detail",
            objectId: $scope.selectedElement.objectId,
            classId: $scope.selectedElement.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            stateParams: {tedActiveTab: $scope.tedActiveTab}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "fullAssoc":
                stateName = "associations.mdobject_fullview";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass: "GHMF Element",
                    objectContext: $scope.selectedElementDisplay.mdContext[1].internalPath
                };
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('FmtGhmfDetailController', FmtGhmfDetailController);
