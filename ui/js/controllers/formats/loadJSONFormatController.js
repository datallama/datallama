/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The controller for the UIB modal dialogue where you can load a JSON file
 *              and turn it into a JSON message format in the DL data base
 */
function LoadJSONFormatController ($uibModalInstance, $scope, HttpService, notify) {

    $scope.valid = true;
    $scope.spinnerLoading = "";
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.disableButtons = false;
    $scope.setSpinnerLoading = function(value){
        $scope.spinnerLoading = value;
        $scope.disableButtons = (value !== "");
    }

    /**
     * Description: Check that the file selected by the user is valid.
     */
    $scope.checkValid = function(){
        $scope.valid = false;
        if(document.getElementById('inputFile') && (document.getElementById('inputFile')).files[0]){
            const inputFile = (document.getElementById('inputFile')).files[0];
            if(inputFile && inputFile.name && inputFile.name.split(".").length === 2 && inputFile.name.split(".")[1] === "json"){
                $scope.valid = true;
                $scope.load(inputFile);
            }
        }
    }

    /**
     * Description: initiates the reverse engineering for a JSON file selected by the user.
     * @param inputFile
     */
    $scope.load = function (inputFile) {
        $scope.setSpinnerLoading("sk-loading");
        const formData = new FormData();
        formData.append('file', inputFile);

        const POST_URL = apiHost + "/format/reverse-engineer/json/" + $scope.formatId;
        const headers = {'Content-Type': undefined};

        HttpService.call(POST_URL, "POST", formData, headers,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.loadSCB()
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        $scope.reverseEngineerRunFailure(response);
                    }
                }
            ]
        );
    };

    /**
     * Description: display all JSON nodes after a successful reverse engineering run.
     */
    $scope.loadSCB = function() {
        $scope.setSpinnerLoading("");
        $scope.resetSelectedNodeDisplay();
        $scope.readJsonNodes();
        $uibModalInstance.close();
    }

    /**
     * Description: Notify the user the the reverse engineering run failed.
     * @param response
     */
    $scope.reverseEngineerRunFailure = function(response) {
        $uibModalInstance.close();
        $scope.setSpinnerLoading("");

        notify({
            message: response.data.result_data,
            classes: "alert-warning",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     *
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('LoadJSONFormatController', LoadJSONFormatController);
