/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function CreateXmlNodeController ($scope, $uibModalInstance, notify, MoClassService) {
    $scope.validation = {
        name: true
    }

    /**
     * Description: checks that all user inputs are valid and invokes the saveNewXmlElement() function in parent AJS Controller
     */
    $scope.checkValid = function() {
        let valid = true;
        if (!($scope.newXmlNode.name && $scope.newXmlNode.name.length > 0)) {
            $scope.validation.name = false;
            valid = false;
        }

        // validate conditions for XML Attributes
        if ($scope.newXmlNode.attribute) {
            $scope.newXmlNode.classId = MoClassService.getMoClassIdByCode('XML_ATTRIBUTE');
            $scope.newXmlNode.type = "attribute";
            if (!($scope.selectedXmlElement && $scope.selectedXmlElement.classId == MoClassService.getMoClassIdByCode('XML_ELEMENT') )) {
                valid = false;
            }
        }
        else {
            $scope.newXmlNode.classId = MoClassService.getMoClassIdByCode('XML_ELEMENT');
        }

        if (valid) {
            $scope.saveNewXmlElement();
            $uibModalInstance.close();
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateXmlNodeController', CreateXmlNodeController);
