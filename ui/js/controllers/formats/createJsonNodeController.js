/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function CreateJsonNodeController ($scope, $uibModalInstance) {
    $scope.jsonNodeType = [
        {jsonNodeTypeName: "object"},
        {jsonNodeTypeName: "array"},
        {jsonNodeTypeName: "boolean"},
        {jsonNodeTypeName: "integer"},
        {jsonNodeTypeName: "number"},
        {jsonNodeTypeName: "string"}
    ];
    $scope.newJsonNodeType = null;
    $scope.validation = {
        name: true,
        type: true
    }

    $scope.create = function () {
        // first: create the new data model diagram in the database. NOTE that $scope.newDiagram is defined in invoking controller
        // TODO: validation - check that all fields have been specified

        $scope.saveNewJsonNode();    // this function must be defined in the controller that opens the dialog.
        $uibModalInstance.close();

    };

    $scope.checkValid = function(){
        $scope.validation.name = !!($scope.newJsonNode.name && $scope.newJsonNode.name.length > 0);
        $scope.validation.type = !!($scope.newJsonNode.type && $scope.newJsonNode.type.jsonNodeTypeName && $scope.newJsonNode.type.jsonNodeTypeName.length > 0);

        if($scope.validation.name && $scope.validation.type){
            $scope.create();
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateJsonNodeController', CreateJsonNodeController);
