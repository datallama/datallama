/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Format XML Detail view.
 *
 * The structure of the xmlElement object stored in $scope.xmlElements, $scope.selectedXmlElement is:
 * {formatId: 0, xmlElementId: 0, classId: 0, parentObjectId: 0, xmlElementLevel: 0, xmlElementLevelSequence: 0, xmlElementType: "s",
 *  xmlElementGUID: "s", xmlElementHierarchyNumber: "s", xmlElementName: "s", xmlElementShortDesc: "s", xmlElementFullPathName: "s",
 *  xmlElementVisible: 0, initialExpandLevel: 0, indentCss": "s", expanded: true, parent: "s", resultSetSequence: 0,
 *  xmlElementSelected: 0, xmlElementMatched: false, mdContext: [{classID: 0 className": "s", objectID": 1, objectName: "s", internalPath: "s"}] }
 */
function FmtXmlDetailController($scope, $rootScope, $location, $state, $stateParams, $window, $uibModal, notify,
                                NavigationChainService, DTOptionsBuilder, DTColumnBuilder, HttpService, RbacService,
                                MoClassService, UtilityService) {
    $scope.xmlElements = [];   // this array holds the hierarchy data to be displayed
    $scope.xmlElementsTableView = [];
    $scope.formatId = 0;      // the formatId is specified in the page URL are retrieved with $stateParams
    $scope.classId = 0;
    $scope.xmlSummary = {};
    $scope.selectedXmlElement = null;
    $scope.selectedXmlElementDisplay = {};
    $scope.selectedXmlElementDisplayUndo = {};
    $scope.selectedNodeAssociations = [];
    $scope.previousResultSetSequence = 0;
    $scope.newXmlNode = {
        name: "",
        desc: "",
        topLevelElement : true,
        attribute: false
    };
    $scope.xmlNodeTypeDisplay = "Element";
    $scope.deleteMdObjectType = "not_set";
    $scope.loadObjectId = 0;

    $scope.customForm = {};
    $scope.customFormFields = [];

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveXmlElement = {moveMode: false, moveTargetXmlElement: null};

    $scope.tedEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;
    $scope.activeDetailTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Xml Elements'},
        ]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notVisible(),
        DTColumnBuilder.newColumn(1).notVisible(),
        DTColumnBuilder.newColumn(2).notVisible(),
        DTColumnBuilder.newColumn(3).notSortable()
    ];

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialisation function for the controller
     */
    $scope.initFmtXmlDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.formatId = $scope.invocationParams.parentObjectId;
        $scope.classId = $scope.invocationParams.parentClassId;
        $scope.loadObjectId = $scope.invocationParams.objectId;
        $scope.activeDetailTab = $scope.invocationParams.stateParams.activeDetailTab;
        $scope.selectedXmlElement = {
            objectId:0,
            classId: 0
        }

        $scope.readXmlElements();
    }

    /**
     * Reads all xml nodes from the DL database for the xml format identified by $scope.formatId
     */
    $scope.readXmlElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/format/xml/content/' + $scope.formatId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.xmlSummary = response.data.result_data.format_summary;
                        $scope.customForm = $scope.xmlSummary.customForm;
                        $scope.customForm.schemaDefinition = $scope.xmlSummary.customForm.schemaDefinitionJson;
                        $scope.customForm.formDefinition = JSON.parse($scope.xmlSummary.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse('{}');
                        $scope.disableCustomFormFields();

                        $scope.xmlElements = response.data.result_data.xml_flat_list;

                        // set selected element if required
                        if ($scope.invocationParams.objectId > 0) {
                            for (let i = 0; i < $scope.xmlElements.length; i++) {
                                if ($scope.xmlElements[i].objectId == $scope.invocationParams.objectId) {
                                    $scope.selectedXmlElement = $scope.xmlElements[i];
                                    $scope.showXmlElementDetails($scope.selectedXmlElement);
                                }
                            }
                        }

                        angular.copy($scope.xmlElements, $scope.xmlElementsTableView);
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }
    
    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.xmlElements.length; i++) {
                if ($scope.xmlElements[i].xmlElementIsParent === false) {
                    endnodes.push($scope.xmlElements[i]);
                }
            }
            angular.copy(endnodes, $scope.xmlElementsTableView);
        }
        else {
            angular.copy($scope.xmlElements, $scope.xmlElementsTableView);
        }
    }

    $scope.openLoadXMLMessageFormatsDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/load_xml_msg_format.html'),
            controller: 'LoadXMLFormatController',
            size: 'md',
            scope: $scope
        });

    }

    $scope.toggleCommentPanel = function(){
        if($scope.selectedXmlElement == null || $scope.selectedXmlElement.elementId <= 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden
        }
    }

    /**
     * Description: enables fields in selected node display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {

        if ($scope.selectedXmlElement == null || $scope.selectedXmlElement.elementId <= 0) {
            notify({
                message: "You have not selected an XML element/attribute. No edits can be made.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            if (dlIboxName === "ted") {
                // copy all summary fields to undo object
                angular.copy($scope.selectedXmlElementDisplay, $scope.selectedXmlElementDisplayUndo);
                $scope.tedEditingDisabled = false;
            }
            else if (dlIboxName === "cfrm") {
                $scope.enableCustomFormFields();
                $scope.cfrmEditingDisabled = false;
            }
        }

    }

    /**
     * Description: saves changes made in the LHS detail panel
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            $scope.saveXmlElementCoreDetails();
        }
        else if (dlIboxName === "cfrm") {
            $scope.cfrmEditingDisabled = true;
            $scope.disableCustomFormFields();
            $scope.saveSelectedCustomFormData();
        }
    }

    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            angular.copy($scope.selectedXmlElementDisplayUndo, $scope.selectedXmlElementDisplay);
            $scope.tedEditingDisabled = true;
        }
        else if (dlIboxName === "cfrm") {
            $scope.disableCustomFormFields();
            $scope.cfrmEditingDisabled = true;
        }
    }

    $scope.remove = function(scope) {
        scope.remove();
    };
    $scope.toggle = function(scope) {
        scope.toggle();
    };
    $scope.moveLastToTheBeginning = function () {
        const a = $scope.data.pop();
        $scope.data.splice(0,0, a);
    };

    /**
     * Description: Opens the New XML Element/Attribute dialog or alerts user that an Attribute cannot have a child element
     */
    $scope.initiateNewXmlElement = function() {
        $scope.newXmlNode = {
            classId: 0,
            name: "",
            desc: "",
            topLevelElement : false,
            attribute: false,
            type: "simple"
        };

        let openDialog = true;
        if ($scope.selectedXmlElement.objectId == 0)
            $scope.newXmlNode.topLevelElement = true;
        else if ($scope.selectedXmlElement.classId == MoClassService.getMoClassIdByCode('XML_ATTRIBUTE')) {
            openDialog = false;
            notify({
                message: "You cannot add a child node to an XML Attribute.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }

        if (openDialog) {
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/create_xml_node.html'),
                controller: 'CreateXmlNodeController',
                size: 'md',
                scope: $scope
            });
        }
    };

    /**
     * Description: collapses the whole taxonomy down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.xmlElements.length; i++) {
            if ($scope.xmlElements[i].xmlElementLevel > 0) {
                $scope.xmlElements[i].xmlElementVisible = 0;
            }
            $scope.xmlElements[i].expanded = false;
        }
    };

    /**
     * Description: expands every element in the taxonomy
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.xmlElements.length; i++) {
            $scope.xmlElements[i].xmlElementVisible = 1;
            $scope.xmlElements[i].expanded = true;
        }
    };

    /**
     * Description: expands the taxonomy to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.xmlElements.length; i++) {
            if ($scope.xmlElements[i].xmlElementLevel < $scope.config.visibilityLevel - 1) {
                $scope.xmlElements[i].xmlElementVisible = 1;
                $scope.xmlElements[i].expanded = true;
            }
            else if ($scope.xmlElements[i].xmlElementLevel === $scope.config.visibilityLevel - 1) {
                $scope.xmlElements[i].xmlElementVisible = 1;
                $scope.xmlElements[i].expanded = false;
            }
            else {
                $scope.xmlElements[i].xmlElementVisible = 0;
                $scope.xmlElements[i].expanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param xmlElement
     */
    $scope.toggleXmlElementExpansion = function(xmlElement) {
        if(xmlElement.isAttribute) return;
        const ehnMatch = xmlElement.xmlElementHierarchyNumber + ".";

        if (xmlElement.expanded) { // collapse all levels below this element
            for(let i = 0; i < $scope.xmlElements.length; i++) {
                if ($scope.xmlElements[i].xmlElementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.xmlElements[i].xmlElementLevel > xmlElement.xmlElementLevel){
                    $scope.xmlElements[i].xmlElementVisible = 0;
                    $scope.xmlElements[i].expanded = false;
                }
                if ($scope.xmlElements[i].objectId === xmlElement.objectId)
                    $scope.xmlElements[i].expanded = false;
            }
        }
        else {  // expand all levels below this element
            for (let i = 0; i < $scope.xmlElements.length; i++) {
                if ($scope.xmlElements[i].xmlElementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.xmlElements[i].xmlElementLevel === xmlElement.xmlElementLevel + 1){
                    $scope.xmlElements[i].xmlElementVisible = 1;
                }
                if ($scope.xmlElements[i].objectId === xmlElement.objectId)
                    $scope.xmlElements[i].expanded = true;
            }
        }
    }

    /**
     * Description: saves a new xml element to the DL database and splices it into the $scope.xmlElements array
     */
    $scope.saveNewXmlElement = function() {

        let parentElementObjectId = 0;
        if(!$scope.newXmlNode.topLevelElement) {
            parentElementObjectId = $scope.selectedXmlElement.objectId;
            $scope.loadObjectId = $scope.selectedXmlElement.objectId;
        }

        const newElement = {
            objectId: 0,
            classId: $scope.newXmlNode.classId,
            parentObjectId: $scope.formatId,
            parentClassId: $scope.classId,
            parentElementObjectId: parentElementObjectId,
            xmlElementType: $scope.newXmlNode.type,
            xmlElementName: $scope.newXmlNode.name,
            xmlElementShortDesc: $scope.newXmlNode.desc
        };

        const POST_URL = apiHost + "/format/xml/content/element";

        HttpService.call(POST_URL, "POST", newElement, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "New XML element or attribute saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readXmlElements();
                    }
                }
            ]
        );
    }

    /**
     * Description: saves a updated XML node core details to the database
     */
    $scope.saveXmlElementCoreDetails = function() {
        $scope.coreDetailsDisabled = true;
        $scope.loadObjectId = $scope.selectedXmlElementDisplay.objectId;

        const POST_URL = apiHost + "/format/xml/content/element";

        HttpService.call(POST_URL, "POST", $scope.selectedXmlElementDisplay, null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Changes were successfully saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readXmlElements();
                    }
                }
            ]
        );
    }

    /**
     * Description; reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedXmlElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedXmlElement.objectId + "/" + $scope.selectedXmlElement.classId;

            HttpService.call(GET_URL,"GET",null,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedXmlElement == null) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedXmlElement.objectId,
                classId: $scope.selectedXmlElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL, "POST", newComment, null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
             }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a taxonomy element when it is clicked in the hierarchy view
     * @param selectedXmlElement
     */
    $scope.showXmlElementDetails = function(selectedXmlElement) {

        if ($scope.moveXmlElement.moveMode) {
            if (selectedXmlElement.xmlElementHierarchyNumber.startsWith($scope.selectedXmlElement.elementHierarchyNumber)) {
                notify({ message: "You cannot move an element to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.moveXmlElement.moveTargetXmlElement = selectedXmlElement;
                $scope.setActiveTab(4);
            }
        }
        else {
            $scope.xmlElements[$scope.previousResultSetSequence].xmlElementSelected = 0;

            $scope.selectedXmlElement = $scope.xmlElements[selectedXmlElement.resultSetSequence];
            $scope.xmlElements[$scope.selectedXmlElement.resultSetSequence].xmlElementSelected = 1;
            $scope.previousResultSetSequence = $scope.selectedXmlElement.resultSetSequence;

            $scope.resetEditing();
            $window.scrollTo(0, 0);

            let GET_URL = apiHost + '/format/xml/content/element/' + $scope.selectedXmlElement.objectId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.selectedXmlElementDisplay = response.data.result_data.attributes;
                            $scope.selectedXmlElementDisplay.xmlElementHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedXmlElementDisplay.xmlElementHierarchyNumber);
                            if ($scope.selectedXmlElement.attribute)
                                $scope.xmlNodeTypeDisplay = "Attribute";
                            else
                                $scope.xmlNodeTypeDisplay = "Element";
                            $scope.selectedNodeAssociations = response.data.result_data.associations;
                            $scope.customForm.formData = JSON.parse(response.data.result_data.custom_form.formData);
                            $scope.readElementComments();
                        }
                    }
                ]
             );
        }
    }

    /**
     * Description: loads the element details for taxonomy element identified by param elementId
     * @param xmlElementId
     */
    $scope.loadSelectedXmlElement = function(xmlElementId) {
        // TODO: this code can be rationalised with the same code in showXmlElementDetails
        const GET_URL = apiHost + '/format/xml/content/element/' + xmlElementId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.selectedXmlElementDisplay = response.data.result_data.attributes;
                        $scope.selectedNodeAssociations = response.data.result_data.associations;
                        //Todo: need to add comments
                        //Highlight the selected element in the hierarchy view.
                        for(let i = 0; i < $scope.xmlElements.length; i++){
                            if($scope.xmlElements[i].classId === $scope.selectedXmlElement.classId && $scope.xmlElements[i].objectId === $scope.selectedXmlElement.objectId) {
                                $scope.xmlElements[i].xmlElementSelected = 1;
                                $scope.selectedXmlElement = $scope.xmlElements[i];
                                break;
                            }
                        }
                    }
                }
            ]
        );
    }

    /**
     * Description: NOT CURRENTLY USED
     *  loads the response from the DL database into scope variables for display in the Details panel
     * @param response
     */
    $scope.processSelectedElementResponse = function(response) {
        $scope.selectedXmlElementDisplay = response.data.result_data.attributes;
        $scope.selectedXmlElementDisplay.elementHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedXmlElementDisplay.elementHierarchyNumber);
        $scope.selectedNodeAssociations = response.data.result_data.associations;
        $scope.customForm.formData = JSON.parse(response.data.result_data.custom_form.formData);
        $scope.readElementComments();
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: checks whether the logged in user has permissions on the module and actionCategory specified in parameters
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteMdObjectType) {
        $scope.deleteMdObjectType = deleteMdObjectType;
        let check = 0;

        // if the mdObject type == 'element', check that an element is selected
        if ($scope.deleteMdObjectType == 'element') {
            if($scope.selectedXmlElement == null || $scope.selectedXmlElement.elementId <= 0) {
                check = 1;
                const errorMessage = "You have not selected an element.";
                notify({ message: errorMessage,
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
        }

        if (check == 0) {
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: calls the DL API to delete the select JSON node or the entire JSON format.
     */
    $scope.deleteMdObject = function () {
        let DELETE_URL = apiHost;
        switch ($scope.deleteMdObjectType) {
            case 'element':
                DELETE_URL = DELETE_URL + "/format/xml/content/element/" + $scope.selectedXmlElement.objectId;
                break;
            case 'format':
                DELETE_URL = DELETE_URL + "/format/" + $scope.formatId;
                break;
            default:
                DELETE_URL = "not_set";
                break;
        }

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "XML " + $scope.deleteMdObjectType + " successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });

                        if ($scope.deleteMdObjectType == 'element')
                            $scope.readXmlElements();
                        else if ($scope.deleteMdObjectType == 'format')
                            $scope.goBack();
                    }
                }
            ]
        );
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.activeDetailTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
                break;
            case 2:
                $scope.activeDetailTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "more":false, "activeIndex":2};
                break;
            case 3:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "more":false, "activeIndex":3};
                break;
            case 4:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "more":false, "activeIndex":4};
                break;
            case 5:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":false, "customform":false, "more":true, "activeIndex":5};
                break;
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.tedEditingDisabled = true;
        $scope.cfrmEditingDisabled = true;
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
        $scope.disableCustomFormFields();
    }

    $scope.checkElementSelected = function(silent) {
        if ($scope.selectedXmlElement == null || $scope.selectedXmlElement.objectId <= 0) {
            if(silent !== true){
                notify({
                    message: "No element selected",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            return false;
        }
        return true;
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form attributes for this table/view and all columns " +
                "that inherit the custom form definition will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: This function is invoked when the Change Custom Form warning dialog is closed by the OK button.
     */
    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Open a dialog to select a custom form for the JSON definition
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/format/customform/";
        const postValues = {
            "objectId": $scope.formatId,
            "classId": $scope.classId,
            "customFormId": selectedCustomForm.customFormId
        };

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form association successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.updateCustomFormDefinition(selectedCustomForm.customFormId);
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the custom form definition after it has been change by the user.
     * @param customFormId
     */
    $scope.updateCustomFormDefinition = function(customFormId) {
        const GET_URL = apiHost + "/common/customform/" + customFormId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.customForm = response.data.result_data;
                        $scope.customForm.schemaDefinition = JSON.parse($scope.customForm.schemaDefinition);
                        $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data to the DL DB for the selected json node
     */
    $scope.saveSelectedCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedXmlElement.objectId,
            classId: $scope.selectedXmlElement.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: Click through to the details view for the selected message format
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {

        if(mode !== "default" && mode !== "fullAssoc" && !$scope.checkElementSelected()){
            return;
        }

        const currentStateSpec = {
            stateName: "smd.fmt_xml_detail",
            objectId: $scope.selectedXmlElement.objectId,
            classId: $scope.selectedXmlElement.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            stateParams: {activeDetailTab: $scope.activeDetailTab}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "fullAssoc":
                stateName = "associations.mdobject_fullview";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams = {
                    objectClass:"XML Element",
                    objectContext:$scope.selectedXmlElement.mdContext[1].internalPath
                };
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('FmtXmlDetailController', FmtXmlDetailController);


