/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Format Summary view.
 *
 */
function FormatSummaryController($scope, $state, $stateParams, $window, $uibModal, NavigationChainService,
                                notify, DTOptionsBuilder, HttpService, RbacService, MoClassService, UtilityService) {
    $scope.formatSummary = {
        objectId: 0,
        classId: 0,
        className: "",
        classCode: "",
        parentObjectId: 0,
        parentClassId: 0,
        formatName: "",
        formatShortDesc: "",
        objectConfig: {}
    };
    $scope.formatSummaryUndo = {};

    $scope.customForm = {};
    $scope.classConfig = {};
    $scope.associatedObjects = [];

    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Format Associations'},
        ]);

    $scope.editingDisabled = true;
    $scope.associationsDisabled = true;

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialisation function for the controller
     */
    $scope.initFormatSummary = function() {
        $scope.perms.create = !RbacService.checkRbac('dmf','create');
        $scope.perms.update = !RbacService.checkRbac('dmf','update');
        $scope.perms.delete = !RbacService.checkRbac('dmf','delete');
        $scope.perms.publish = !RbacService.checkRbac('dmf','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.formatSummary.objectId = $scope.invocationParams.objectId;
        $scope.formatSummary.classId = $scope.invocationParams.classId;

        $scope.readFormatSummary();
    }

    /**
     * Reads all xml nodes from the DL database for the xml format identified by $scope.formatId
     */
    $scope.readFormatSummary = function() {

        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/format/' + $scope.formatSummary.objectId + '/' + $scope.formatSummary.classId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.formatSummary = response.data.result_data.format_summary;
                        $scope.formatSummary.classCode = MoClassService.getMoClassByClassId($scope.formatSummary.classId).classCode;
                        $scope.formatSummary.objectConfig = JSON.parse($scope.formatSummary.objectConfig);

                        $scope.classConfig = response.data.result_data.format_summary.classConfig;
                        // parse the class config angularSchemaForm definitions
                        try {
                            $scope.classConfig.configSchemaDefn = JSON.parse($scope.classConfig.configSchemaDefn);
                            $scope.classConfig.configFormDefn = JSON.parse($scope.classConfig.configFormDefn);
                            $scope.classConfig.classConfigData = JSON.parse($scope.classConfig.classConfigData);
                        } catch (e) {
                            console.log("Warning! The class configuration could not be parsed properly");
                        }
                        // parse the custom form angularSchemaForm definitions
                        $scope.customForm = response.data.result_data.format_summary.customForm;
                        try {
                            $scope.customForm.formData = JSON.parse($scope.customForm.formData);
                            $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
                            $scope.customForm.schemaDefinition = $scope.customForm.schemaDefinitionJson;
                            //$scope.disableCustomFormFields();
                        } catch (e) {
                            console.log("Warning! The custom form definition could not be parsed properly");
                        }

                        $scope.associatedObjects = response.data.result_data.associations;
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: enables fields in selected node display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.formatSummary, $scope.formatSummaryUndo);
            $scope.editingDisabled = false;
        }
        else if (dlIboxName === "associations") {
            $scope.associationsDisabled = false;
        }
    }

    /**
     * Description: saves to the DL DB any changes made to format core details. Note: associations are saved via the
     *  New Association view.
     * @param dlIboxName
     */
    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            $scope.saveFormatSummaryCoreDetails();
            $scope.editingDisabled = true;
        }
        else if (dlIboxName === "associations") {
            $scope.associationsDisabled = true;
        }
    }

    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.formatSummaryUndo, $scope.formatSummary);
            $scope.editingDisabled = true;
        }
        else if (dlIboxName === "associations") {
            $scope.associationsDisabled = true;
        }
    }

    /**
     * Description: saves a updated XML node core details to the database
     */
    $scope.saveFormatSummaryCoreDetails = function() {
        const formatSummaryUpdate = {
            formatId: $scope.formatSummary.formatId,
            classId: $scope.formatSummary.classId,
            formatName: $scope.formatSummary.formatName,
            formatShortDesc: $scope.formatSummary.formatShortDesc,
            objectConfig: JSON.stringify($scope.formatSummary.objectConfig),
            createSource: 1
        };
        $scope.coreDetailsDisabled = true;

        const POST_URL = apiHost + "/format/summary";
        HttpService.call(POST_URL, "POST", formatSummaryUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The format summary was updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        let exception = response.data.result_data.exception;
                        let msg = "Exception Class: " + exception.exceptionClass + ". Exception Message: " + exception.exceptionMessage + ".";
                        notify({
                            message: msg,
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description; reads all comments for the selected element
     */
    $scope.readComments = function() {
        if ($scope.selectedXmlElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedXmlElement.objectId + "/" + $scope.selectedXmlElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedXmlElement == null) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedXmlElement.objectId,
                classId: $scope.selectedXmlElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL,"POST",newComment,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: deletes the format definition after the user confirms the delete action
     */
    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/format/" + $scope.formatSummary.objectId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Format successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.newAssociation = function() {

        NavigationChainService.storeCurrentState($scope.invocationParams);

        const invocationParams = {
            stateName:"associations.new_association",
            objectId:$scope.formatSummary.formatId,
            classId:$scope.formatSummary.classId,
            stateParams:{
                objectClass:$scope.formatSummary.className,
                objectContext:$scope.formatSummary.formatName
            }
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: transitions to either a format detail view or an associated objects view.
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, transitionMode) {
        // execute the navigation transition
        const currentStateSpec = {
            stateName: "smd.fmt_summary",
            objectId: $scope.formatSummary.formatId,
            classId: $scope.formatSummary.classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let targetState = "";
        let invocationParams = {
            stateName: targetState,
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        if (transitionMode == "DETAIL") {
            let moClass = MoClassService.getMoClassByClassId($scope.formatSummary.classId);
            switch (moClass.classCode) {
                case 'JSON_MESSAGE_FORMAT':
                    targetState = 'smd.fmt_json_detail';
                    break;
                case 'GHMF':
                    targetState = 'smd.fmt_ghmf_detail';
                    break;
                case 'XML_MESSAGE_FORMAT':
                    targetState = 'smd.fmt_xml_detail';
                    break;
                case 'DELIMITED_FLATFILE':
                case 'FIXED_WIDTH_FLATFILE':
                    targetState = 'smd.fmt_flatfile_detail';
                    break;
            }
            invocationParams.stateName = targetState;
            invocationParams.parentObjectId = $scope.formatSummary.formatId;
            invocationParams.parentClassId = $scope.formatSummary.classId;
        }
        else if (transitionMode == "ASSOC") {
            let moClass = MoClassService.getMoClassByClassId(classId);
            invocationParams.stateName = moClass.objectDetailState;
            invocationParams.objectId = objectId;
            invocationParams.classId = classId;
            invocationParams.parentObjectId = parentObjectId;
            invocationParams.parentClassId = parentClassId;
        }
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('FormatSummaryController', FormatSummaryController);
