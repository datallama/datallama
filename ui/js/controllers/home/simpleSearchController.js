/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Thie view lists all meatadata objects stored in Data Llama
 */
function SimpleSearchController ($scope, $rootScope, $state, $stateParams, $location, notify,
                                 DTOptionsBuilder, NavigationChainService, HttpService, MoClassService) {
    $scope.gridData = [];
    $scope.searchParams = {
        keyword: "",
        objectName: true,
        objectNameExact: false,
        objectShortDesc: true,
        taxonomy: true,
        messageFormat: true,
        landscapeObjects: true,
        dataFlows: true,
        dataBases: true,
        diagrams: true,
        endNodesOnly: true
    };
    $scope.config = {filterPanelHidden: true};
    $scope.assocShortDesc = "";

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'}
        ]);

    $scope.invokingState = $stateParams.invokingState;
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Initialisation function for the MmObjectAssociationController.
     */
    $scope.initSimpleSearch = function () {
        $scope.invokingState = $stateParams.invokingState;
        $scope.invocationParams = $stateParams.invocationParams;

        try {
            $scope.searchParams = $scope.invocationParams.stateParams.searchParams;
            $scope.executeSearch();
        }
        catch(e) {
            $scope.resetSearchParams();
         }
    }

    /**
     * Executes the search when the seach button is clicked
     */
    $scope.executeSearch = function() {
        const keyword = String($scope.searchParams.keyword);
        if (keyword.length > 0) {
            $scope.spinnerLoading = "sk-loading";
            const POST_URL = apiHost + "/home/search/filtered";

            HttpService.call(POST_URL, "POST", $scope.searchParams, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.spinnerLoading = "";
                            $scope.gridData = response.data.result_data;
                            if ($scope.gridData.length == 0) {
                                notify({
                                    message: "No rows returned for you search",
                                    classes: "alert-success",
                                    duration: 1000,
                                    templateUrl: $scope.notifyTemplate
                                });
                            }
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: Clears the current search
     */
    $scope.clearSearch = function() {
        $scope.resetSearchParams();
        $scope.gridData = [];
    }

    /**
     * Description: toggles visibility of the Search Filters Panel
     */
    $scope.toggleFilterPanel = function() {
        $scope.config.filterPanelHidden = !$scope.config.filterPanelHidden
    }

    /**
     * Description: toggles the meta class selection on/off when meta class name is clicked on the Search Filter Classes drop down
     * @param paramName
     */
    $scope.toggleSearchParams = function(paramName) {
        let cmd = "$scope.searchParams." + paramName + " = !($scope.searchParams." + paramName + ")";
        eval(cmd);
    }

    /**
     * Navigate to the default view for the metadata object clicked by the user
     * @param objectId
     * @param classId
     * @param mdContext
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mdContext) {
        const currentStateSpec = {
            stateName:"home.simple_search",
            objectId:0,
            classId:0,
            parentObjectId:0,
            parentClassId:0,
            stateParams:{searchParams:$scope.searchParams}};

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {stateName: "", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};
        let moClass = MoClassService.getMoClassByClassId(classId);
        invocationParams.stateName = moClass.objectDetailState;
        invocationParams.objectId = objectId;
        invocationParams.classId = classId;
        invocationParams.parentObjectId = parentObjectId;
        invocationParams.parentClassId = parentClassId;
        invocationParams.stateParams = {};

        if (invocationParams.stateName.length > 0) {
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }

    }

    /**
     * Description: return to the Data Store Summary view
     */
    $scope.goBack = function() {
        console.log($scope.invokingState);
        //$state.go($scope.invokingState.stateName, $scope.invokingState.stateParams);
    }

    /**
     * Description: reset the Search Parameters to default values
     */
    $scope.resetSearchParams = function () {
        $scope.searchParams = {
            keyword: "",
            objectName: true,
            objectNameExact: false,
            objectShortDesc: true,
            taxonomy: true,
            messageFormat: true,
            landscapeObjects: true,
            dataFlows: true,
            dataBases: true,
            diagrams: true,
            endNodesOnly: true
        };
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SimpleSearchController', SimpleSearchController);

