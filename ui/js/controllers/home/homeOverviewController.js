/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomies Overview view
 */
function HomeOverviewController($rootScope, $scope, $state, $stateParams, $location, $compile,
                                NavigationChainService, notify, HttpService, RbacService) {

    $scope.editOverviewDesc = false;
    $scope.homeOverviewText = "";
    $scope.homeOverviewTextUndo = "";
    $scope.overviewDescReadOnly = null;

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.initHomeOverview = function() {
        $scope.perms.create = !RbacService.checkRbac('home','create');
        $scope.perms.update = !RbacService.checkRbac('home','update');
        $scope.perms.delete = !RbacService.checkRbac('home','delete');
        $scope.perms.publish = !RbacService.checkRbac('home','publish');

        $scope.overviewDescReadOnly = document.getElementById("overviewDescReadOnly");

        const GET_URL = apiHost + '/home/overview/' + $state.current.name.replace(/\./g, "_");
        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.initHomeOverviewSCB(response);
                    }
                }
            ]
        );

    }

    $scope.initHomeOverviewSCB = function(response){
        $scope.homeOverviewText = response.data.result_data.pageContent;
        let overviewDescReadOnly = $scope.overviewDescReadOnly;
        overviewDescReadOnly.innerHTML = $scope.homeOverviewText;
        $compile(overviewDescReadOnly)($scope);
    }

    $scope.goBack = function() {
        console.log("Ok, OK!! We're going back");
    }

    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "overview_desc") {
            $scope.homeOverviewTextUndo = $scope.homeOverviewText;
            $scope.editOverviewDesc = true;
        }
    }

    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "overview_desc") {
            $scope.editOverviewDesc = false;

            $scope.overviewPageContent = {
                overviewId: 0,  // not used in stored proc at present
                stateName: $state.current.name,
                pageContent: $scope.homeOverviewText
            }

            // make the REST POST call to save the overview page content to the database
            const POST_URL = apiHost + "/home/overview";

            HttpService.call(POST_URL,"POST",$scope.overviewPageContent,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.dlIboxSaveSCB(response);
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: update both read-write and read-only content variables after data successfully saved to DL database
     * @param response
     */
    $scope.dlIboxSaveSCB = function(response){
        $scope.homeOverviewText = $scope.overviewPageContent.pageContent;
        $scope.overviewDescReadOnly.innerHTML = $scope.homeOverviewText;
        $compile($scope.overviewDescReadOnly)($scope);

        notify({
            message: "Changes saved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });
    }

    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "overview_desc") {
            $scope.homeOverviewText = $scope.homeOverviewTextUndo;
            $scope.overviewDescReadOnly.innerHTML = $scope.homeOverviewText;
            $scope.editOverviewDesc = false;
        }
    }

    /**
     * Navigate to the view identified by state targetState
     * @param targetState
     */
    $scope.clickThrough = function(targetState) {
        const currentStateSpec = {
            stateName:"home.overview",
            objectId:0,
            classId:0,
            stateParams:{}};
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {  // data store
            stateName: targetState,
            objectId: 0,
            classId: 0,
            stateParams: ""};
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});

    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('HomeOverviewController', HomeOverviewController);
