/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating data model diagrams
 */
function PdmNewRelationshipController ($scope, $uibModalInstance) {
    $scope.parentColumn = null;
    $scope.childColumn = null;

    $scope.validation = {
        name: true,
        columnPairs: true
    }

    /**
     * Description: validates user input and $scope.addNewRelationship() in parent controller if validation succeeds.
     *  Variable $scope.newRelationshipDetails is declared in the parent controller.
     */
    $scope.ok = function () {
        let valid = true;
        if($scope.newRelationshipDetails.relationshipName && $scope.newRelationshipDetails.relationshipName.length > 0){
            $scope.validation.name = true;
        }
        else{
            $scope.validation.name = false;
            valid = false;
        }

        if($scope.newRelationshipDetails.columnPairs && $scope.newRelationshipDetails.columnPairs.length > 0){
            $scope.validation.columnPairs = true;
        }
        else{
            $scope.validation.columnPairs = false;
            valid = false;
        }

        if(valid){
            $scope.saveRelationshipToDb();
            $uibModalInstance.close();
        }

    }

    /**
     * Description: Called when the user cancels the New Relationship action
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.deleteTemporaryConnector();
    };

    /**
     * Description: adds a new column pair to the $scope.newRelationshipDetails.columnPairs array.
     *  Checks that the column pair does not already exist in the array.
     */
    $scope.addColumnPair = function() {
        if ($scope.parentColumn != null && $scope.childColumn != null) {
            const newColumnPair = {
                parentColumnId: $scope.parentColumn.columnId,
                parentColumnName:  $scope.parentColumn.columnName,
                childColumnId:  $scope.childColumn.columnId,
                childColumnName:  $scope.childColumn.columnName
            };

            // Check whether the column pair as already been added.
            let check = 0;
            for(let i = 0; i < $scope.newRelationshipDetails.columnPairs.length; i++) {
                let currentColumnPair = $scope.newRelationshipDetails.columnPairs[i];
                if (currentColumnPair.parentColumnId == newColumnPair.parentColumnId && currentColumnPair.childColumnId == newColumnPair.childColumnId) {
                    check = 1;
                }
            }

            if (check == 0) {
                $scope.newRelationshipDetails.columnPairs.push(newColumnPair);
            }
        }
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('PdmNewRelationshipController', PdmNewRelationshipController);
