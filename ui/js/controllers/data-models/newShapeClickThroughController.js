/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function newShapeClickThroughController ($scope, $rootScope, $state, $stateParams, $window, NavigationChainService, notify, DTOptionsBuilder, HttpService) {
    $scope.gridData = [];
    $scope.selectedObject = {};
    $scope.searchParams = {
        keyword: "",
        objectName: true,
        objectNameExact: false,
        objectShortDesc: true,
        taxonomy: true,
        messageFormat: true,
        landscapeObjects: true,
        dataFlows: true,
        dataBases: true,
        diagrams: true,
        endNodesOnly: true
    };
    $scope.config = {
        filterPanelHidden: true
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Initialisation function for the MmObjectAssociationController.
     */
    $scope.initNewShapeClickThrough = function () {
        $scope.invocationParams = $stateParams.invocationParams;

        $scope.selectedObject.objectId = 0;
        $scope.selectedObject.classId = 0;
    }

    /**
     * Description: Executes the search when the search button is clicked
     */
    $scope.executeSearch = function() {
        const keyword = String($scope.searchParams.keyword);
        if (keyword.length > 0) {
            const POST_URL = apiHost + "/home/search/filtered";

            HttpService.call(POST_URL, "POST", $scope.searchParams, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.gridData = response.data.result_data;
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: Clears the current search
     */
    $scope.clearSearch = function() {
        $scope.searchParams.keyword = "";
        $scope.gridData = [];
    }

    /**
     * Description: toggles visibility of the search filter panel
     */
    $scope.toggleFilterPanel = function() {
        $scope.config.filterPanelHidden = !$scope.config.filterPanelHidden
    }

    /**
     * Description: toggles the meta class selection on/off when meta class name is clicked on the Search Filter Classes drop down
     * @param paramName
     */
    $scope.toggleSearchParams = function(paramName) {
        let cmd = "$scope.searchParams." + paramName + " = !($scope.searchParams." + paramName + ")";
        eval(cmd);
    }

    $scope.checkValid = function() {
        if($scope.selectedObject.classId > 0 && $scope.selectedObject.objectId > 0){
            $scope.saveClickThrough();
        }
        else{
            notify({
                message: "Please select and object for the click through",
                classes: "alert-warning",
                templateUrl: $scope.notifyTemplate
            });
        }
    }

    $scope.saveClickThrough = function () {
        const POST_URL = apiHost + "/data-models/shape/ct";

        const clickThrough = {
            objectId: $scope.invocationParams.objectId,
            ctClassId: $scope.selectedObject.classId,
            ctObjectId: $scope.selectedObject.objectId
        }

        HttpService.call(POST_URL, "POST", clickThrough, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Click Through successfully saved",
                            classes: "alert-success",
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.selectClickThrough = function(selectedObject){
        $scope.selectedObject.selectionChange = 0;
        $scope.selectedObject = selectedObject;
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('newShapeClickThroughController', newShapeClickThroughController);

