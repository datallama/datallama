/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for Physical Data Model (PDM) diagram
 */
function PdmDiagramController($scope, $rootScope, $uibModal, $location, $state, $stateParams, $compile, notify,
                              NavigationChainService, DTOptionsBuilder, DiagramService, HttpService,
                              DetailsClickThroughService, RbacService, UtilityService) {
    $scope.dirtyCanvas = false;
    $scope.currentDiagram = {
        id: 0,
        classId: 0,
        name: "",
        shortDesc: "",
        properties: {}
    }
    $scope.diagramFullDesc = "";
    $scope.diagramFullDescUndo = "";
    $scope.diagramFullDescReadOnly = null;

    $scope.currentDatabaseId = 0;
    $scope.viewMode = 1;
    $scope.editingDisabled = {core: true, fulldesc: true, tblvw: true, clmn: true, rlshp: true};
    $scope.currentDiagramUndo = {};

    $scope.detailsPanelUndo = {tblvwDesc: "", clmnDesc: "", rlshpDesc: ""};
    $scope.edsSize = 3;

    $scope.activeTab = {tableView: false, column: false, relationship: false, diagram: true, activeIndex: 4};
    $scope.perms = {create: false, update: false, delete: false, publish: false, configure: false};

    class DlRelationship {
        cnctId = 0;
        relationshipDiagramId = "";
        classId = 0;
        cnctName = "";
        cnctDesc = "";
        parentTableViewId = 0;
        parentClassId = 0;
        parentColumnId = 0;
        parentTableViewName = "";
        childTableViewId = 0;
        childClassId = 0;
        childColumnId = 0;
        childTableViewName = "";
        cnctStartNtt = "";
        cnctEndNtt = "";
        cnctVertices = {};
        inclusionStatus = 0;

        constructor(cnctId, classId, cnctName, cnctDesc, parentTableViewId, parentClassId, parentTableViewName,
                    childTableViewId, childClassId, childTableViewName, cnctStartNtt, cnctEndNtt, cnctVertices, inclusionStatus) {
            this.cnctId = cnctId;
            this.classId = classId;
            this.cnctName = cnctName;
            this.cnctDesc = cnctDesc;
            this.parentTableViewId = parentTableViewId;
            this.parentClassId = parentClassId;
            this.parentTableViewName = parentTableViewName;
            this.childTableViewId = childTableViewId;
            this.childClassId = childClassId;
            this.childTableViewName = childTableViewName ;
            this.cnctStartNtt = cnctStartNtt;
            this.cnctEndNtt = cnctEndNtt;
            this.cnctVertices = cnctVertices;
            this.inclusionStatus = inclusionStatus;

            this.relationshipDiagramId = classId + ":" + cnctId;
        }
    }

    // the columnPairs array stores columnPair objects with structure {parentColumnId:0, childColumnId:0}
    $scope.newRelationshipDetails = {
        relationshipName: "",
        relationshipShortDesc: "",
        databaseId: 0,
        schemaId: 0,
        classId: 7,
        createMode: 30,
        revengRunId: 0,
        parentTableViewId: 0,
        parentColumnId: 0,
        childTableViewId: 0,
        childColumnId: 0,
        parentTable: "",
        parentColumn: "",
        childTable: "",
        childColumn: "",
        columnPairs: []
    };

    $scope.selectedTableView = {
        tableViewId:0,
        classId:0,
        tableViewName:"None selected",
        tableViewShortDesc:"None selected",
        columnCount:0,
        columnList:[]
    };
    $scope.selectedColumn = {
        columnId:0,
        columnName:"None selected",
        columnOrdinal:0,
        columnDatatype:"N/A",
        columnShortDesc:"None selected",
        tableViewName:"None selected"
    };
    $scope.selectedRelationship = {
        relationshipId:0,
        classId:0,
        relationshipName:"None selected",
        parentTableViewId:0,
        parentColumnId:0,
        parentClassId:0,
        parentTableViewName:"None selected",
        parentColumnName:"None selected",
        childTableViewId:0,
        childColumnId:0,
        childClassId:0,
        childTableViewName:"None selected",
        childColumnName:"None selected",
        relationshipShortDesc:"None defined"
    };

    $scope.tempId = 0;

    //used when creating a new diagram
    $scope.newDiagram = {
        diagramId : -1,
        classId : 4,
        diagramName : "",
        diagramDesc : "",
        diagramProperties : {smdDatabaseId:0, smdDatabaseName:"", allEntities:true, customConfig:false}
    };

    //used when creating a new diagram
    $scope.dmDiagramClass = {
        classId: 4,
        className: "Physical Data Model"
    };

    $scope.notifyTemplate = 'views/common/notify.html';

    //used for the selection view
    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Physical Data Model Entities'},
        ]);

    /**
     * Description: Initialises the pdmDiagramController
     */
    $scope.initDiagram = function() {
        // 1. configure RBAC permissions
        $scope.perms.create = !RbacService.checkRbac('dmf','create');
        $scope.perms.update = !RbacService.checkRbac('dmf','update');
        $scope.perms.delete = !RbacService.checkRbac('dmf','delete');
        $scope.perms.publish = !RbacService.checkRbac('dmf','publish');
        $scope.perms.publish = !RbacService.checkRbac('dmf','configure');

        $scope.currentDiagram.id = $stateParams.invocationParams.objectId;
        $scope.diagramFullDescReadOnly = document.getElementById("fullDescReadOnly");

        // 2. process state parameters
        const stateParams = $stateParams.invocationParams.stateParams;
        if (stateParams.entityDetailsSidebar !== undefined) {
            $scope.entityDetailsSidebar = stateParams.entityDetailsSidebar;
        }
        if (stateParams.activeTab !== undefined) {
            $scope.activeTab = stateParams.activeTab;
        }


        // 3. configure the Diagram Service
        DiagramService.setSelectedDiagramId($scope.currentDiagram.id);
        DiagramService.setGetDiagramContentURL(apiHost + "/data-models/pdm/diagram/content/");
        DiagramService.setPostDiagramContentURL(apiHost + "/data-models/pdm/diagram/content");
        DiagramService.setCreateDiagramURL(apiHost + "/data-models/diagram/create");
        DiagramService.setDeleteDiagramURL(apiHost + "/data-models/diagram/");
        DiagramService.setGetEntityDetailURL(apiHost + "/smd/database/tableview/");
        DiagramService.setLocationAfterClose("/datamodels/pdm_diagrams_list");
        DiagramService.setNewDiagram($scope.newDiagram);
        DiagramService.setNewEntity($scope.newEntity);
        DiagramService.setNewConnector($scope.newConnector);
        DiagramService.setCanvas($scope.canvas);
        DiagramService.setScope($scope);

        // init scope variables that will be handled by the diagram service
        DiagramService.initScopeVariables();

        // Load the diagram
        DiagramService.readDiagramFromDb();

        // get the full description for the diagram
        // TODO: intergrate into the DiagramService
        $scope.readDiagramFullDesc();

    }

    $scope.updateGetAllEntitiesURL = function() {
        DiagramService.setGetAllEntitiesURL(apiHost + "/data-models/pdm/diagram/" + $scope.currentDiagram.id + "/database/"+ $scope.currentDiagram.properties.smdDatabaseId);
    }

    $scope.zoomIn = function(){
      DiagramService.zoomIn()
    }
    $scope.zoomOut = function(){
      DiagramService.zoomOut()
    }
    $scope.zoomReset = function(){
      DiagramService.zoomReset()
    }

    /**
     * Adds an table/view entity to a PDM diagram
     * @function newEntity
     * @param entityDetails
     * @returns {DlSmdTableView2_0}
     */
    $scope.newEntity = function(entityDetails) {
        // add the entity figure id
        entityDetails.id = String(entityDetails.classId) + ":" + String(entityDetails.nttId);
        const attributes = $.extend({headerClickCallback: $scope.showEntityDetails, rowClickCallback: $scope.showColumnDetails}, entityDetails);
        return(new DlSmdTableView2_0(attributes));
    };

    $scope.canvasDblClick = function(){
        //this is a quick and dirty fix to avoid the warning from
        //canvas.js when double clicking to collapse a table.
    }

    $scope.newConnector = function(connectorDetails){
        const newConnector = new DlPdmDiagramConnector(connectorDetails)
        newConnector.on(
            "contextmenu",
            function(){
            const smdRelationshipId = newConnector.id.split(":")[1];
            $scope.showRelationshipDetails(smdRelationshipId);
        });
        return(newConnector);
    };

    /**
     * Description: enables editing for the panel identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            angular.copy($scope.currentDiagram, $scope.currentDiagramUndo);
            $scope.editingDisabled.core = false;
        }
        else if (dlIboxName === "full_desc") {
            angular.copy($scope.diagramFullDesc, $scope.diagramFullDescUndo);
            $scope.editingDisabled.fulldesc = false;
        }
        else if (dlIboxName === "tblvw") {
            if ($scope.selectedTableView.tableViewId === 0) {
                notify({
                    message: "No table or view selected in the diagram.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.editingDisabled.tblvw = false;
            }
        }
        else if (dlIboxName === "clmn") {
            if ($scope.selectedColumn.columnId === 0) {
                notify({
                    message: "No column selected in the diagram.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.editingDisabled.clmn = false;
            }
        }
        else if (dlIboxName === "rlshp") {
            if ($scope.selectedRelationship.relationshipId === 0) {
                notify({
                    message: "No column selected in the diagram.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            else {
                $scope.editingDisabled.rlshp = false;
            }
        }
    }

    /**
     * Description: saves changes made in the panel identified by dlIboxName
     * @param dlIboxName
     */
    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            $scope.editingDisabled.core = true;
            $scope.saveDiagramSummary();
        }
        else if (dlIboxName === "full_desc") {
            $scope.editingDisabled.fulldesc = true;
            $scope.diagramFullDescReadOnly.innerHTML = $scope.diagramFullDesc;
            $compile($scope.diagramFullDescReadOnly)($scope);
            $scope.saveDiagramFullDesc();
        }
        else if (dlIboxName === "tblvw") {
            $scope.editingDisabled.tblvw = true;
            $scope.saveTableViewShortDesc();
        }
        else if (dlIboxName === "clmn") {
            $scope.editingDisabled.clmn = true;
            $scope.saveColumnShortDesc();
        }
        else if (dlIboxName === "rlshp") {
            $scope.editingDisabled.rlshp = true;
            $scope.saveRelationshipShortDesc();
        }
    }

    /**
     * Description: cancels changes made in the panel identified by dlIboxName and reverts to Undo values
     * @param dlIboxName
     */
    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            $scope.editingDisabled.core = true;
            angular.copy($scope.currentDiagramUndo, $scope.currentDiagram);
        }
        else if (dlIboxName === "full_desc") {
            $scope.editingDisabled.fulldesc = true;
            angular.copy($scope.diagramFullDescUndo, $scope.diagramFullDesc);
        }
        else if (dlIboxName === "tblvw") {
            $scope.editingDisabled.tblvw = true;
        }
        else if (dlIboxName === "clmn") {
            $scope.editingDisabled.clmn = true;
        }
        else if (dlIboxName === "rlshp") {
            $scope.editingDisabled.rlshp = true;
        }
    }

    /**
     * Description: reads the fulls description for the diagram from the DL database
     * TODO: move this to the diagramService
     */
    $scope.readDiagramFullDesc = function () {
        const GET_URL = apiHost + '/common/fulldescription/' + $scope.currentDiagram.id + "/" + $scope.currentDiagram.classId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.readDiagramFullDescSCB(response);
                    }
                }
            ]
        );
    }

    $scope.readDiagramFullDescSCB = function(response){
        $scope.diagramFullDesc = response.data.result_data.fullDescription;
        $scope.diagramFullDescReadOnly.innerHTML = $scope.diagramFullDesc;
        $compile($scope.diagramFullDescReadOnly)($scope);
    }

    /**
     * Description: saves the summary info (name, short description) for a PDM diagram to the DL database
     * TODO: move this to the diagramService
     */
    $scope.saveDiagramSummary = function () {
        const POST_URL = apiHost + '/data-models/diagram/summary';

        const diagramUpdates = {
            diagramId: $scope.currentDiagram.id,
            classId: 4,     // TODO: fix this!
            diagramName: $scope.currentDiagram.name,
            diagramShortDesc: $scope.currentDiagram.shortDesc,
            diagramProperties: {}
        }

        HttpService.call(POST_URL,"POST",diagramUpdates,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "User details updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );


    }

    /**
     * Description: saves the full description for the diagram, as defined by the user, to the DL database.
     */
    $scope.saveDiagramFullDesc = function () {
        const POST_URL = apiHost + '/common/fulldescription/';

        const fullDescUpdate = {
            objectId: $scope.currentDiagram.id,
            classId: $scope.currentDiagram.classId,
            fullDescription: $scope.diagramFullDesc
        };

        HttpService.call(POST_URL,"POST",fullDescUpdate,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Full description successfully saved",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the updated table/view short description to the DL database
     */
    $scope.saveTableViewShortDesc = function () {
        const POST_URL = apiHost + "/smd/database/tableview/summary";

        const tableViewUpdates = {
            "tableViewId":$scope.selectedTableView.tableViewId,
            "tableViewShortDesc":$scope.selectedTableView.tableViewShortDesc
        };

        HttpService.call(POST_URL,"POST",tableViewUpdates,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Table/View short description updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: saves the updated table/view short description to the DL database
     */
    $scope.saveColumnShortDesc = function () {
        const POST_URL = apiHost + "/smd/database/column/summary";
        const columnUpdates = {
            "columnId":$scope.selectedColumn.columnId,
            "columnShortDesc":$scope.selectedColumn.columnShortDesc
        };

        HttpService.call(POST_URL,"POST",columnUpdates,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Column short description updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the updated table/view short description to the DL database
     */
    $scope.saveRelationshipShortDesc = function () {
        const POST_URL = apiHost + "/smd/database/relationship/summary";
        const relationshipUpdates = {
            "relationshipId":$scope.selectedRelationship.relationshipId,
            "relationshipShortDesc":$scope.selectedRelationship.relationshipShortDesc
        };

        HttpService.call(POST_URL,"POST",relationshipUpdates,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Relationship short description updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: move backwards along the Navigation Chain, i.e. to previously visited view
     */
    $scope.goBack = function() {
        if($scope.dirtyCanvas){
            const msg = "Please close or save the current diagram before going back.";
            notify({
                message: msg,
                classes: "alert-warning",
                templateUrl: $scope.notifyTemplate
            });
        }
        else{
            NavigationChainService.goBack();
        }
    }

    /**
     * Description: Displays details of the selected Table/View in the details panel.
     * @param tableViewId
     */
	$scope.showEntityDetails = function (tableViewId) {
        const GET_URL = apiHost + '/smd/database/tableview/summary/' + tableViewId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.selectedTableView = response.data.result_data;
                        $scope.resetSelectedColumn();
                        $scope.resetSelectedRelationship();
                        $scope.entityDetailsSidebar = 1;
                        $scope.setActiveTab(1);
                    }
                }
            ]
        );

    };

    /**
     * Description: Displays details of the selected column in the details panel
     * @param columnId
     */
    $scope.showColumnDetails = function (columnId) {
        const GET_URL = apiHost + '/smd/database/column/' + columnId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.selectedColumn = response.data.result_data.core_details;
                        $scope.selectedTableView.tableViewName = response.data.result_data.core_details.tableViewName;
                        $scope.selectedTableView.tableViewShortDesc = response.data.result_data.core_details.tableViewShortDesc;
                        $scope.entityDetailsSidebar = 1;
                        $scope.setActiveTab(2);
                    }
                }
            ]
        );
    };

    /**
     * Description: Displays details of the selected SMD Relationship in the Details Panel
     * @param relationshipId
     */
    $scope.showRelationshipDetails = function(relationshipId){
        const GET_URL = apiHost + '/smd/database/relationship/' + relationshipId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.selectedRelationship = response.data.result_data;
                        $scope.resetSelectedTableView();
                        $scope.resetSelectedColumn();
                        $scope.entityDetailsSidebar = 1;
                        $scope.setActiveTab(3);
                    }
                }
            ]
        );
    }

    /*
    * Description: Creates a new connection between PDM table/view entities from user manual action
    */
    $scope.newConnectionCreator = function () {
        $scope.tempId -= 1;

        //This is a temporary connector and will be replaced after the
        //relationship has been saved to the database.
        const tempConnector = new draw2d.Connection({id: $scope.tempId});
        tempConnector.on(
            "added",
            function(){

                const parentPortId = $scope.canvas.getLine($scope.tempId).sourcePort.id;
                const parentId = parentPortId.substr(0,parentPortId.length - 3);
                const parentTable = $scope.canvas.getFigure(parentId);

                // initialise the newRelationshipDetails object.
                $scope.newRelationshipDetails.databaseId=$scope.currentDiagram.properties.smdDatabaseId;
                $scope.newRelationshipDetails.parentTable = parentTable.tableViewName;
                $scope.newRelationshipDetails.parentTableViewId = parentTable.tableViewId;
                $scope.newRelationshipDetails.schemaId = parentTable.schemaId;
                $scope.parentColumnList = parentTable.columnList;

                const childPortId = $scope.canvas.getLine($scope.tempId).targetPort.id;
                const childId = childPortId.substr(0,childPortId.length - 3);
                const childTable = $scope.canvas.getFigure(childId);
                $scope.newRelationshipDetails.childTable = childTable.tableViewName;
                $scope.newRelationshipDetails.childTableViewId = childTable.tableViewId;
                $scope.childColumnList = childTable.columnList;

                $scope.newRelationshipDetails.relationshipName = parentTable.tableViewName + "->" + childTable.tableViewName;
                $scope.newRelationshipDetails.columnPairs = [];
                $scope.showNewRelationshipDialog();
            }
        );
        return tempConnector;
    }

    /**
     * invokes the DL API to save the SMD relationship to the DL database. Invoked from the New Relationship Dialog SAVE button.
     * @function saveRelationshipToDb
     */
    $scope.saveRelationshipToDb = function(){
        const POST_URL = apiHost + '/smd/database/relationship';

        HttpService.call(POST_URL, "POST", $scope.newRelationshipDetails, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.saveRelationshipToDbSCB(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: Processes the response from the DL API for the /smd/database/relationship invocation.
     *  The temporary connector identified by $scope.tempId is removed and replaced by a properly formed
     *  SMD Relationship connector.
     * @param response
     */
    $scope.saveRelationshipToDbSCB = function(response){
        const cnctId = response.data.result_data.returnId

        const tempConnection = $scope.canvas.getLine($scope.tempId);
        const sourceIds = tempConnection.sourcePort.id.split(":");
        const targetIds = tempConnection.targetPort.id.split(":");
        const startNtt = sourceIds[0] + ":" + sourceIds[1];
        const endNtt = targetIds[0] + ":" + targetIds[1];
        const parentClassId = sourceIds[0];
        const childClassId = targetIds[0];

        const newRelationship = new DlRelationship(
            cnctId,
            $scope.newRelationshipDetails.classId,
            $scope.newRelationshipDetails.relationshipName,
            $scope.newRelationshipDetails.relationshipShortDesc,
            $scope.newRelationshipDetails.parentTableViewId,
            parentClassId,
            $scope.newRelationshipDetails.parentTable,
            $scope.newRelationshipDetails.childTableViewId,
            childClassId,
            $scope.newRelationshipDetails.childTable,
            startNtt,
            endNtt,
            {},
            1
        );
        newRelationship.parentColumnId = $scope.newRelationshipDetails.parentColumnId;
        newRelationship.childColumnId = $scope.newRelationshipDetails.childColumnId;

        DiagramService.addConnector(newRelationship);
        $scope.canvas.remove(tempConnection);
    }

    /**
     * Description: Deletes a temporary connection when the New Relationship actions fails or is cancelled by the user
     *  in the PDM New Relationship dialog.
     */
    $scope.deleteTemporaryConnector = function() {
        const tempConnection = $scope.canvas.getLine($scope.tempId);
        $scope.canvas.remove(tempConnection);
    }

    /**
     * Description: opens the pdm_new_relationship.html dialog when the uses manually creates a connector between to
     *  table/view objects on the diagram.
     */
    $scope.showNewRelationshipDialog = function () {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/data-models/pdm_new_relationship.html'),
            controller: 'PdmNewRelationshipController',
            size: 'lg',
            keyboard: false,
            scope: $scope
        });

    };

    /**
     * Description: displays the div that contains the full description view
     */
    $scope.toggleDiagramFullDescription = function (action) {
        if (action === 'show')
            $scope.setViewMode(3);
        else if (action === 'hide')
            $scope.setViewMode(1);
    }

    /**
     * Description: Show the Select SMD Database dialoag.
     */
    $scope.pdmSelectSmdDatabase = function () {

        if ($scope.currentDiagram.properties.smdDatabaseId > 0) {
            const msg = "A database has already been selected for this diagram: " + $scope.currentDiagram.properties.smdDatabaseName;
            notify({
                message: msg,
                classes: "alert-warning",
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            // if a database is selected in the dialog then $scope.readSmdDatabaseDetails() is called
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/smd_select_database.html'),
                controller: 'SmdSelectDatabaseController',
                size: 'lg',
                scope: $scope
            });
        }

    };

    $scope.resetSelectedTableView = function(){
        $scope.selectedTableView = {
            tableViewId:0,
            classId:0,
            tableViewName:"None selected",
            tableViewShortDesc:"None selected",
            columnCount:0,
            columnList:[]
        }
    }

    $scope.resetSelectedColumn = function(){
        $scope.selectedColumn = {
            columnId:0,
            columnName:"None selected",
            columnOrdinal:0,
            columnDatatype:"N/A",
            columnShortDesc:"None selected",
            tableViewName:"None selected"
        };
    }

    $scope.resetSelectedRelationship = function(){
        $scope.selectedRelationship = {
            relationshipId:0,
            classId:0,
            relationshipName:"None selected",
            parentTableViewId:0,
            parentColumnId:0,
            parentClassId:0,
            parentTableViewName:"None selected",
            parentColumnName:"None selected",
            childTableViewId:0,
            childColumnId:0,
            childClassId:0,
            childTableViewName:"None selected",
            childColumnName:"None selected",
            relationshipShortDesc:"None selected"
        };
    }

    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.activeTab = {"tableView":true, "column":false, "relationship":false, "diagram":false, "activeIndex":1};
                break;
            case 2:
                $scope.activeTab = {"tableView":false, "column":true, "relationship":false, "diagram":false, "activeIndex":2};
                break;
            case 3:
                $scope.activeTab = {"tableView":false, "column":false, "relationship":true, "diagram":false, "activeIndex":3};
                break;
            case 4:
                $scope.activeTab = {"tableView":false, "column":false, "relationship":false, "diagram":true, "activeIndex":4};
                break;
        }
    }

    /**
     * Description: increases or decreases the width of the Entity Details Panel
     * @param action
     */
    $scope.resizeEntityDetailsSidebar = function (action) {
        if (action === 'plus' && $scope.edsSize < 5)
            $scope.edsSize++;
        else if(action === 'minus' && $scope.edsSize > 3)
            $scope.edsSize--;
    }

    $scope.expandCollapseAll = function (collapsed){
        const tables = $scope.canvas.getFigures().data;
        for(let i = 0; i < tables.length; i++){
            tables[i].expandCollapse(collapsed);
        }
    }

    /**
     * Stores current state in the NavigationChainService and transitions to the smd.table_view_detail state
     * for entity identified by objectId and classId
     * @param objectId
     * @param classId
     */
    $scope.clickThrough = function (objectId, classId) {

        if (objectId > 0) {
            const currentStateSpec = {
                stateName:"datamodels.pdm_diagrams_view",
                objectId:$scope.currentDiagram.id,
                classId:$scope.currentDiagram.classId,
                stateParams: {
                    entityDetailsSidebar: 1,
                    activeTab: {tableView: false, column: false, relationship: false, diagram: true, activeIndex: 4}
                }
            };

            NavigationChainService.storeCurrentState(currentStateSpec);

            DetailsClickThroughService.clickThrough(objectId, classId);
        }
    }

}


/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('PdmDiagramController', PdmDiagramController);
