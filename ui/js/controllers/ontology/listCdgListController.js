/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the List CDG list view
 */
function ListCdgListController($scope, $state, $rootScope, $location, $uibModal, notify,
                               NavigationChainService, DTOptionsBuilder, HttpService, UtilityService) {
    $scope.gridData = [];
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'CDG-Lists'},
            {extend: 'pdf', title: 'CDG-Lists'},
        ]);

    /**
     * Description: initialisation function for the controller
     */
    $scope.initListCdgList = function() {
        $scope.readListCdgList();
    };

    /**
     * Description: reads the list of List CDG Metadata Objects from the DL database
     */
    $scope.readListCdgList = function() {
        const GET_URL = apiHost + '/ontology/listcdg/list';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the modal dialog that lets the user specify details for a new List CDG element
     */
    $scope.openCreateListCdgDialog = function() {
        const modalInstance = $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/list_cdg_new_list.html'),
            controller: 'ListCdgNewListController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: write a new taxonomy to the database based on details entered in the modal dialog
     * @param newList
     */
    $scope.createListCdg = function(newList) {
        const POST_URL = apiHost + "/ontology/listcdg";

        HttpService.call(POST_URL,"POST",newList,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.readListCdgList();
                        notify({
                            message: "New list saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: click through to list cdg detail view
     */
    $scope.clickThrough = function (objectId, classId, mode) {
        const currentStateSpec = {
            stateName:"taxonomies.list_cdg_list",
            objectId:0,
            classId:0,
            parentObjectId:0,
            parentClassId:0,
            stateParams: {}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {
            stateName:"taxonomies.list_cdg_detail",
            objectId:0,
            classId:0,
            parentObjectId:objectId,
            parentClassId:classId,
            stateParams:{
                tedActiveTab:{attributes:true, associations:false, comments:false, customform:false, more:false, activeIndex:1}
            }
        };
        if(mode === "summary"){
            invocationParams = {
                stateName:"taxonomies.list_cdg_summary",
                objectId:objectId,
                classId:classId,
                parentObjectId:0,
                parentClassId:0,
                stateParams:{}
            };
        }
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    };
}

function ListCdgNewListController ($scope, $location, $uibModalInstance) {
    $scope.newListDetails =  {};
    $scope.listTypeList = [
        {classId: 29, typeName: "Catalogue"},
        {classId: 30, typeName: "Glossary"},
        {classId: 31, typeName: "Thesaurus"},
        {classId: 32, typeName: "Dictionary"},
        {classId: 33, typeName: "Attribute"}
    ];
    $scope.validation = {
        listType: true,
        listName: true
    };

    /**
     * Description: create a new list (CDG) when user clicks OK.
     */
    $scope.ok = function (result) {
        let valid = true;

        if($scope.newListDetails.name && $scope.newListDetails.name.length > 0){
            $scope.validation.listName = true;
        }
        else{
            $scope.validation.listName = false;
            valid = false;
        }

        if($scope.newListDetails.listType && $scope.newListDetails.listType.classId > 0){
            $scope.validation.listType = true;
        }
        else{
            $scope.validation.listType = false;
            valid = false;
        }
        if(valid){
            let desc = null;
            if($scope.newListDetails.description){
                desc = $scope.newListDetails.description;
            }

            const newList = {
                listCdgId : -1,
                classId: $scope.newListDetails.listType.classId,
                listCdgName : $scope.newListDetails.name,
                className: $scope.newListDetails.listType.typeName,
                listCdgShortDesc : desc
            };
            $scope.createListCdg(newList);
            $uibModalInstance.close();
        }
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}
/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ListCdgListController', ListCdgListController)
    .controller('ListCdgNewListController', ListCdgNewListController);
