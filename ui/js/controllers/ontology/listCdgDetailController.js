/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the List Detail view.
 *  The structure of $scope.selectedElement is:
 *  {listId: 0, elementId: 0, classId: 0, elementName: "", elementShortDesc: "", elementSelected: 0}
 */
function ListCdgDetailController($scope, $rootScope, $state, $stateParams, $window, $uibModal, $compile, notify,
                                 NavigationChainService, RbacService,DTOptionsBuilder, DTColumnBuilder,
                                 MoClassService, HttpService, UtilityService) {
    $scope.listCdgElements = [];   // this array holds all the elements returned from the database

    $scope.listCdgId = 0;      // the listCdgId is specified in the page URL are retrieved with $stateParams
    $scope.list_cdg_summary = {};
    $scope.selectedElement = {};
    $scope.selectedElementDisplay = {};
    $scope.selectedElementAssociations = [];

    $scope.newElement = {};
    $scope.newElementId = 0;
    $scope.returnId = 0;
    $scope.newElementConfig = {topLevelElement: true};

    $scope.customForm = {};

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};

    $scope.ledEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;
    $scope.editFullDesc = false;
    $scope.fullDesc = {text: ""};
    $scope.fullDescReadOnly = "";
    $scope.ledActiveTab = {attributes:true, associations:false, comments:false, customform:false, more:false, activeIndex:1};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
        ]);

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.spinnerLoading = "";

    $scope.deleteList = false;

    /**
     * Description: initialisation function for the controller
     */
    $scope.initListCdgDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.resetSelectedElementDisplay();
        $scope.listCdgId = $scope.invocationParams.parentObjectId;
        $scope.classId = $scope.invocationParams.parentClassId;
        $scope.selectedElement.elementId = $scope.invocationParams.objectId;
        $scope.selectedElement.classId = $scope.invocationParams.classId;

        if (angular.equals($scope.invocationParams.stateParams, {})) {
            $scope.ledActiveTab = {attributes:true, associations:false, comments:false, customform:false, more:false, activeIndex:1};
        }
        else {
            $scope.ledActiveTab = $scope.invocationParams.stateParams.ledActiveTab;
        }
        $scope.readListCdgElements();
    }

    /**
     * Description: reads from the DL database all elements for the list identified by $scope.listCdgId
     */
    $scope.readListCdgElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/ontology/listcdg/content/' + $scope.listCdgId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.spinnerLoading = "";
                        $scope.list_cdg_summary = response.data.result_data.list_cdg_summary;
                        $scope.customForm = response.data.result_data.custom_form;
                        try{
                            $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
                            $scope.customForm.schemaDefinition = $scope.customForm.schemaDefinitionJson;
                            $scope.disableCustomFormFields();
                        }catch (e) {
                            console.log("Warning! The custom form definition could not be parsed properly");
                        }
                        $scope.listCdgElements = response.data.result_data.list_cdg_elements;
                        $scope.refreshSelectedElement();
                    }
                }
            ]
        );
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *              delete the custom form data in the list elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Massive Database Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form data for <b>ALL</b> elements of this list will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/ontology/listcdg/customform/" + $scope.listCdgId;

        HttpService.call(POST_URL, "POST", selectedCustomForm.customFormId, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form selection successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.customForm = selectedCustomForm;
                        $scope.resetSelectedElementDisplay();
                        $scope.readListCdgElements();
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteList) {
        $scope.deleteList = deleteList;
        if(!$scope.deleteList && ($scope.selectedElement == null || $scope.selectedElement.elementId === 0)) {
            const errorMessage = "You have not selected an element.";
            notify({
                message: errorMessage,
                classes: "alert-danger",
                duration: 30000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else{
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: lets the user delete either a full list of just a list element
     */
    $scope.deleteMdObject = function () {
        if($scope.deleteList){
            const DELETE_URL = apiHost + "/ontology/listcdg/" + $scope.listCdgId;
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            notify({
                                message: "List was deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.goBack();
                        }
                    }
                ]
            );

        }
        else{
            const DELETE_URL = apiHost + "/ontology/listcdg/element/" + $scope.selectedElement.elementId;
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            notify({
                                message: "List CDG element was deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            //Todo: any clean up such as reloading elements etc...
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: opens the New List CDG Element modal dialog with default field values
     */
    $scope.initiateNewElement = function() {
        $scope.newElementConfig.topLevelElement = ($scope.selectedElement == null || $scope.selectedElement.elementId === 0);

        $scope.newElement = {name: "", desc: ""};

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/create_list_cdg_element.html'),
            controller: 'CreateListCdgElementController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: saves a new Lits cdg element to the DL database and splices it into the $scope.listCdgElements array
     */
    $scope.saveNewElement = function() {
        $scope.newElementId--;

        const newElement = {
            listCdgId: $scope.listCdgId,
            elementId: $scope.newElementId,
            classId: MoClassService.getMoClassIdByCode('LIST_CDG_ELEMENT'),
            elementName: $scope.newElement.name,
            elementShortDesc: $scope.newElement.desc,
            customFormData: JSON.stringify({})
        };

        const POST_URL = apiHost + "/ontology/listcdg/element";

        HttpService.call(POST_URL, "POST", newElement, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.readListCdgElements();
                        notify({
                            message: "New list element created successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the core details of an updated element to the DL database
     */
    $scope.saveElementCoreDetails = function() {
        $scope.coreDetailsDisabled = true;

        const updatedElement = {
            listCdgId: $scope.listCdgId,
            elementId: $scope.selectedElement.elementId,
            classId: $scope.selectedElement.classId,
            elementName: $scope.selectedElementDisplay.elementName,
            elementShortDesc: $scope.selectedElementDisplay.elementShortDesc
        };

        // make the REST POST call to save the data store summary info to the database
        const POST_URL = apiHost + "/ontology/listcdg/element";

        HttpService.call(POST_URL, "POST", updatedElement, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.readListCdgElements();
                        notify({
                            message: "Updates to list element saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: refreshes the selected element status if elementId is specified in a navigation transition
     */
    $scope.refreshSelectedElement = function() {
        const refreshElementId = $scope.selectedElement.elementId;

        if(refreshElementId > 0) {
            for (let i = 0; i < $scope.listCdgElements.length; i++ ) {
                if ($scope.listCdgElements[i].elementId === refreshElementId) {
                    $scope.selectedElement = $scope.listCdgElements[i];
                    break;
                }
            }
            $scope.showElementDetails($scope.selectedElement, 2);
        }
    }

    /**
     * Descriptions: toggles the disable status of the editing icon set
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        switch (dlIboxName) {
            case "led":
                if ($scope.selectedElement === null || $scope.selectedElement.elementId === 0) {
                    let errorMessage = "You have not selected an element. No edits can be made.";
                    notify({
                        message: errorMessage,
                        classes: "alert-danger",
                        duration: 2000,
                        templateUrl: $scope.notifyTemplate
                    });
                }
                else {
                    $scope.ledEditingDisabled = false;
                    $scope.editFullDesc = true;
                }
                break;

            case "cfrm":
                if ($scope.selectedElement === null || $scope.selectedElement.elementId === 0) {
                    const errorMessage = "You have not selected an element. No edits can be made.";
                    notify({
                        message: errorMessage,
                        classes: "alert-danger",
                        duration: 30000,
                        templateUrl: $scope.notifyTemplate
                    });
                }
                else {
                    $scope.enableCustomFormFields();
                    $scope.cfrmEditingDisabled = false;
                }
                break;
        }
    }

    /**
     * Description: saves edits to DL database by invoking relevant save function
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        switch (dlIboxName) {
            case "led":
                $scope.ledEditingDisabled = true;
                $scope.editFullDesc = false;
                $scope.saveElementCoreDetails();
                $scope.saveFullDesc();
                break;
            case "cfrm":
                $scope.cfrmEditingDisabled = true;
                $scope.saveElementCustomFormData();
                break;
        }
    }

    /**
     * Description: cancels edits and rolls back to pre-edit values
     * @param dlIboxName
     */
    $scope.dlIboxParentCancel = function(dlIboxName) {
        switch (dlIboxName) {
            case "led":
                $scope.ledEditingDisabled = true;
                $scope.editFullDesc = false;
                break;
            case "cfrm":
                $scope.disableCustomFormFields();
                $scope.cfrmEditingDisabled = true;
                break
        }

    }

    /**
     * Description: saves to the DL DB the input field values selected in the Custom Form
     */
    $scope.saveElementCustomFormData = function(){

        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedElement.elementId,
            classId: $scope.selectedElement.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

        $scope.disableCustomFormFields();
        $scope.cfrmEditingDisabled = true;
    }

    /**
     * Description: reads the full description for the list element
     */
    $scope.readFullDesc = function () {
        const GET_URL = apiHost + '/common/fulldescription/' + $scope.selectedElement.elementId + "/" + $scope.selectedElement.classId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.processFullDesc(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: populates the Full Description read-only display and summernote editor with full description HTML
     * @param response
     */
    $scope.processFullDesc = function(response) {
        $scope.fullDesc.text = response.data.result_data.fullDescription;
        const fullDescReadOnly = document.getElementById("fullDescReadOnly");
        fullDescReadOnly.innerHTML = $scope.fullDesc.text;
    }

    /**
     * Description: saves the full description for the list element
     */
    $scope.saveFullDesc = function() {
        const POST_URL = apiHost + '/common/fulldescription/';

        const fullDescUpdate = {
            objectId: $scope.selectedElement.elementId,
            classId: $scope.selectedElement.classId,
            fullDescription: $scope.fullDesc.text
        };

        HttpService.call(POST_URL, "POST", fullDescUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Full description successfully saved",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description:
     */
    $scope.resetSelectedElementDisplay = function() {
        $scope.selectedElementDisplay = {
            elementId: 0,
            classId: 0,
            elementName: "No element has been selected.",
            elementShortDesc: "No element has been selected.",
            elementSelected: 0,
            mdContext: []
        }
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }


    /**
     * Description; reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedElement.elementId + "/" + $scope.selectedElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedElement == null || $scope.selectedElement.elementId === 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedElement.elementId,
                classId: $scope.selectedElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL, "POST", newComment, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a lst element when it is clicked in the list view.
     *  selectionMode specifies whether the selectedElement is set by a user click action (1) or a state transition (2)
     * @param selectedElement
     * @param selectionMode
     */
    $scope.showElementDetails = function(selectedElement, selectionMode) {

        if (selectionMode == 1) {   // user has clicked element name
            $scope.selectedElement = selectedElement;
        }

        $scope.disableCustomFormFields();

        for (let i = 0; i < $scope.listCdgElements.length; i++) {
            if ($scope.listCdgElements[i].elementId === $scope.selectedElement.elementId)
                $scope.listCdgElements[i].elementSelected = 1;
            else {
                $scope.listCdgElements[i].elementSelected = 0;
            }
        }

        $scope.resetLedEditing();
        $window.scrollTo(0, 0); // scroll to top so that element details form is visible

        $scope.readFullDesc();

        // get details of selected element
        const GET_URL = apiHost + '/ontology/listcdg/element/' + $scope.selectedElement.elementId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.selectedElementDisplay = response.data.result_data.attributes;
                        $scope.customForm.formData = JSON.parse($scope.selectedElementDisplay.customFormData);
                        $scope.selectedElementAssociations = response.data.result_data.associations;
                        $scope.comment.commentList = response.data.result_data.comments;
                    }
                }
            ]
        );
    }

    /**
     * Description: shows details of the md-context node in a modal dialog
     * TODO: this implementation needs to be completed
     * @param contextNode
     */
    $scope.showMdContextNodeDetails = function(contextNode) {
        console.log(contextNode);
    }

    /**
     * Description: toggles the abbreviation setting between 0 (off) and 1 (on)
     * TODO: eventually the user will be able to select abbreviation schemes from this view, which is why it isn't a boolean
     */
    $scope.toggleAbbreviation = function() {
        $scope.config.abbreviation = ($scope.config.abbreviation === 1) ? 0 : 1;
        $scope.readListElements();
    }

    $scope.toggleCommentPanel = function(){
        if($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden
        }
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.ledActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "activeIndex":1};
                break;
            case 2:
                $scope.ledActiveTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "activeIndex":2};
                break;
            case 3:
                $scope.ledActiveTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "activeIndex":3};
                break;
            case 4:
                $scope.ledActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "activeIndex":4};
                break;
        }
    }

    $scope.resetLedEditing = function () {
        $scope.ledEditingDisabled = true;
    }

    /**
     * Description: Clears the SpinnerLoading overlay
     */
    $scope.clearSpinnerLoading = function () {
        $scope.spinnerLoading = "";
    };

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    $scope.checkElementSelected = function(silent) {
        if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
            if(silent !== true){
                notify({
                    message: "No element selected",
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            return false;
        }
        return true;
    }

    /**
     * Description: click through to taxonomy detail
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {
        if(mode !== "default" && mode !== "fullAssoc" && !$scope.checkElementSelected()){
            return;
        }

        const currentStateSpec = {
            stateName:"taxonomies.list_cdg_detail",
            objectId:$scope.selectedElement.elementId,
            classId:$scope.selectedElement.classId,
            parentObjectId:$scope.listCdgId,
            parentClassId:$scope.invocationParams.parentClassId,
            stateParams:{
                ledActiveTab:$scope.ledActiveTab
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "fullAssoc":
                /*
                 * Todo: think about using the same full assoc view as for the formats.
                 * Todo: actually build the full assoc view if you decide not to use to use the format's view.
                 */
                stateName = "associations.list_cdg_fullview";
                stateParams = {}
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:"List (CDG) Element",
                    objectContext:$scope.selectedElementDisplay.elementName
                };
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ListCdgDetailController', ListCdgDetailController);


