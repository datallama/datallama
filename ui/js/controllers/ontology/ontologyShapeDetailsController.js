/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Overlay controller
 */
function ontologyShapeDetailsController ($scope, $uibModalInstance) {

    $scope.validation = {
        name: true
    };

    $scope.clickThroughDefined = ($scope.selectedShape.ctClassId > 0 && $scope.selectedShape.ctObjectId > 0);

    $scope.clickThroughButton = "Add Click Through";
    if($scope.selectedShape.ctClassId > 0 || $scope.selectedShape.ctObjectId > 0){
        $scope.clickThroughButton = "Change Click Through";
    }

    $scope.ok = function () {
        let valid = true;

        if($scope.selectedShape.nttName && $scope.selectedShape.nttName.length > 0){
            $scope.validation.name = true;
        }
        else{
            $scope.validation.name = false;
            valid = false;
        }
        if(valid){
            const shapeDetails = {
                objectId : $scope.selectedShape.nttId,
                name : $scope.selectedShape.nttName,
                shortDesc : $scope.selectedShape.nttDesc,
                shapeTemplateId: 0,
                isText: false,
                text: $scope.selectedShape.text,
                hAlign: $scope.selectedShape.hAlign,
                vAlign: $scope.selectedShape.vAlign
            }
            $scope.updateShape(shapeDetails);
            $uibModalInstance.close();
        }
    };

    $scope.preCheckClickThrough = function() {
        $uibModalInstance.close()
        $scope.checkClickThrough();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ontologyShapeDetailsController', ontologyShapeDetailsController);