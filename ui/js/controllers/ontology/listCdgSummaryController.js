/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the List Summary view.
 *
 */
function listCdgSummaryController($scope, $state, $stateParams, $window, $uibModal, NavigationChainService,
                                notify, DTOptionsBuilder, HttpService, RbacService, MoClassService, UtilityService) {
    $scope.listSummary = {
        listCdgId: 0,
        classId: 0,
        className: "",
        listCdgName: "",
        listCdgShortDesc: ""
    };

    $scope.associatedObjects = [];
    $scope.disable = {
        coreDetails: true,
        associations: true
    };

    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'List Associations'},
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    $scope.initListSummary = function() {
        $scope.perms.create = !RbacService.checkRbac('ont','create');
        $scope.perms.update = !RbacService.checkRbac('ont','update');
        $scope.perms.delete = !RbacService.checkRbac('ont','delete');
        $scope.perms.publish = !RbacService.checkRbac('ont','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.listSummary.listCdgId = $scope.invocationParams.objectId;
        $scope.listSummary.classId = $scope.invocationParams.classId;
        $scope.readListSummary();

    }

    $scope.readListSummary = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/ontology/listcdg/content/' + $scope.listSummary.listCdgId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.listSummary = response.data.result_data.list_cdg_summary;
                        $scope.associatedObjects = response.data.result_data.associations;
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    $scope.dlIboxParentEdit = function(dlIboxName) {
        switch(dlIboxName){
            case "core_details":
                $scope.disable.coreDetails = false;
                break;
            case "associations":
                $scope.disable.associations = false;
                break;
        }
    }

    $scope.dlIboxParentSave = function(dlIboxName) {
        switch(dlIboxName){
            case "core_details":
                $scope.disable.coreDetails = true;
                $scope.saveListCoreDetails();
                break;
            case "associations":
                $scope.disable.associations = true;
                break;
        }
    }

    $scope.dlIboxParentCancel = function(dlIboxName) {
        switch(dlIboxName){
            case "core_details":
                $scope.disable.coreDetails = true;
                break;
            case "associations":
                $scope.disable.associations = true;
                break;
        }
    }

    /**
     * Description: opens the Object Association modal dialog
     */
    $scope.clickThrough = function() {

        NavigationChainService.storeCurrentState($scope.invocationParams);

        const moClass = MoClassService.getMoClassByClassId(MoClassService.getMoClassIdByCode("LIST_CDG_ELEMENT"));

        const invocationParams = {
            stateName: moClass.objectDetailState,
            objectId: 0,
            classId: 0,
            parentObjectId: $scope.listSummary.listCdgId,
            parentClassId: $scope.listSummary.classId,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    $scope.saveListCoreDetails = function() {

        const POST_URL = apiHost + "/ontology/listcdg";

        HttpService.call(POST_URL, "POST", $scope.listSummary, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The list summary was updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
        console.log("save core details");
    }

    $scope.readComments = function() {
        if ($scope.selectedXmlElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedXmlElement.objectId + "/" + $scope.selectedXmlElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/ontology/listcdg/" + $scope.listSummary.listCdgId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "List successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.newAssociation = function() {
        // execute the navigation transition
        const currentStateSpec = $scope.invocationParams;

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"associations.new_association",
            objectId:$scope.listSummary.listCdgId,
            classId:$scope.listSummary.classId,
            stateParams:{
                objectClass: "List (CDG)",
                objectContext: $scope.listSummary.listCdgName
            }
        };
        console.log()
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('listCdgSummaryController', listCdgSummaryController);
