/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for adding new shape.
 *
 */
function ontologyNewShapeController ($scope, $uibModalInstance, items) {
    $scope.newShapeDetails =  {
        name: null,
        description: null,
        shapeTemplate: {
            templateId: null,
            templateName: null,
            templateDesc: null,
            templateSVG: null
        }
    };
    $scope.validation = {
        template: true,
        name: true
    };

    $scope.ok = function (result) {
        let valid = true;

        if($scope.newShapeDetails.name && $scope.newShapeDetails.name.length > 0){
            $scope.validation.name = true;
        }
        else{
            $scope.validation.name = false;
            valid = false;
        }

        if($scope.newShapeDetails.shapeTemplate && $scope.newShapeDetails.shapeTemplate.templateId > 0){
            $scope.validation.template = true;
        }
        else{
            $scope.validation.template = false;
            valid = false;
        }
        if(valid){
            let desc = null;
            if($scope.newShapeDetails.description){
                desc = $scope.newShapeDetails.description;
            }
            const newShape = {
                objectId : -1,
                name : $scope.newShapeDetails.name,
                shortDesc : desc,
                shapeTemplateId: $scope.newShapeDetails.shapeTemplate.templateId
            }
            $scope.addShape(items.x, items.y, newShape);
            $uibModalInstance.close();
        }
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ontologyNewShapeController', ontologyNewShapeController);