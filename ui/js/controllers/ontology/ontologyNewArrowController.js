/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for adding new connections.
 *
 */
function ontologyNewArrowController ($scope, $location, $uibModalInstance) {
    $scope.validation = {
        arrowName: true
    };

    /**
     * Description: create a new arrow in the data base.
     */
    $scope.ok = function () {
        let valid = true;

        if($scope.newArrowDetails.name && $scope.newArrowDetails.name.length > 0){
            $scope.validation.arrowName = true;
        }
        else{
            $scope.validation.arrowName = false;
            valid = false;
        }

        if(valid){
            $scope.addArrow($scope.newArrowDetails);
            $uibModalInstance.close();
        }
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ontologyNewArrowController', ontologyNewArrowController);