/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Ontology Diagrams view
 */
function ontologyDiagramController($scope, $rootScope, $stateParams, $state, $location, $uibModal, notify,
								   NavigationChainService, DTOptionsBuilder, DiagramService, HttpService, MoClassService,
								   RbacService, DetailsClickThroughService, UtilityService) {

	$scope.dirtyCanvas = false;
	$scope.tempId = 0;

	$scope.dmDiagramClass = {
		classId: MoClassService.getMoClassIdByCode("ONTOLOGY_DIAGRAM"),
		className: "Ontology"
	};

	$scope.currentDiagram = {
		name: "",
		id: 0,
		properties: {},
		customData: {
			processors: []
		}
	}

	$scope.newDiagram = {
		diagramId : -1,
		classId : $scope.dmDiagramClass.classId,
		diagramName : "",
		diagramDesc : "",
		diagramProperties : {
			"colorDefs":[
				{
					"id":"default",
					"colorDef":{
						"bgColor":"#FFFFFF"
					}
				}
			],
			"groups":[],
			allEntities:false, customConfig:true
		}
	};

	$scope.selectedShapes = [];
	$scope.shapeTemplateList = [];
	$scope.palletSidebar = true;
	$scope.currentTemplate = 0;
	$scope.shapeEditingDisabled = true;
	$scope.updatedShapes = [];

	$scope.nodesVisible = (RbacService.checkRbac('ont', 'update', 0) || RbacService.checkRbac('ont', 'create', 0));

    $scope.notifyTemplate = 'views/common/notify.html';

	/**
	 * Description: initialising function for the controller. For diagram controllers the init function is called by the
	 * Draw2d canvas.
	 */
	$scope.initDiagram = function() {
		$scope.currentDiagram.id = $stateParams.invocationParams.objectId;
		$scope.getShapeTemplates();
		//Configure the Diagram Service
		DiagramService.setSelectedDiagramId($scope.currentDiagram.id);
		DiagramService.setGetDiagramContentURL(apiHost + "/ontology/diagram/content/");
		DiagramService.setPostDiagramContentURL(apiHost + "/ontology/diagram/content");
		DiagramService.setCreateDiagramURL(apiHost + "/data-models/diagram/create");
		DiagramService.setDeleteDiagramURL(apiHost + "/data-models/diagram/");
		DiagramService.setNewDiagram($scope.newDiagram);
		DiagramService.setNewEntity($scope.newEntity);
		DiagramService.setNewConnector($scope.newConnector);
		DiagramService.setCanvas($scope.canvas);
		DiagramService.setScope($scope);
		DiagramService.setCustomConfig($scope.config)
		//init scope variables that will be handled by the diagram service
		DiagramService.initScopeVariables();
		//Load the diagram
		DiagramService.readDiagramFromDb();
	}

	$scope.config = function(){
		$scope.toggleNodes();
	}

  $scope.zoomIn = function(){
    DiagramService.zoomIn()
  }
  $scope.zoomOut = function(){
    DiagramService.zoomOut()
  }
  $scope.zoomReset = function(){
    DiagramService.zoomReset()
  }

	$scope.saveOntDiagram = function(){
		const POST_URL = apiHost + '/data-models/shape/align';

		for(let i = 0; i <$scope.updatedShapes.length; i++){
			const shapeDetails = {
				objectId : $scope.updatedShapes[i].nttId,
				hAlign : $scope.updatedShapes[i].hAlign,
				vAlign : $scope.updatedShapes[i].vAlign
			}
			HttpService.call(POST_URL,"POST",shapeDetails,null,
				[
					{
						status:1,
						callback: function(response){
							if(i === $scope.updatedShapes.length){
								$scope.updatedShapes = [];
							}
						}
					}
				]
			);
		}

		$scope.saveDiagram();
	}

	$scope.newEntity = function(entityDetails){

		let newEntity;
		if(entityDetails.isText){
			newEntity = new DlDiagramText(entityDetails);
			newEntity.installEditor(new draw2d.ui.LabelInplaceEditor({
				//for when updating the text.
				onCommit: $.proxy(function(value){
					const shapeDetails = {
						objectId : entityDetails.nttId,
						name : "",
						shortDesc : null,
						shapeTemplateId: 1, //This is the id for the text template
						isText: true,
						text: value,
						hAlign: entityDetails.hAlign,
						vAlign: entityDetails.vAlign
					}
					$scope.updateShape(shapeDetails);
				},this)
			}));
		}
		else{
			newEntity = new DlDiagramShape(entityDetails);
			newEntity.on(
				"contextmenu",
				function(){
					if($scope.dirtyCanvas){
						const msg = "There are unsaved changes. Please save the diagram before viewing details";
						notify({
							message: msg,
							classes: "alert-warning",
							templateUrl: $scope.notifyTemplate
						});
					}
					else{
						$scope.selectedShape = newEntity;
						$scope.shapeEditingDisabled = true;
						$uibModal.open({
							templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_shape_details.html'),
							controller: 'ontologyShapeDetailsController',
							size: 'lg',
							scope: $scope
						});
					}
				}
			);
		}
		return(newEntity);
	};

	$scope.newConnector = function(connectorDetails){
		const newConnector = new DlDiagramArrow(connectorDetails)
		newConnector.on(
			"contextmenu",
			function(){
				if($scope.dirtyCanvas){
					const msg = "There are unsaved changes. Please save the diagram before viewing details";
					notify({
						message: msg,
						classes: "alert-warning",
						templateUrl: $scope.notifyTemplate
					});
				}
				else{
					$scope.selectedShape = newConnector;
					$scope.shapeEditingDisabled = true;
					$uibModal.open({
						templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_arrow_details.html'),
						controller: 'ontologyArrowDetailsController',
						size: 'lg',
						scope: $scope
					});
				}
			}
		);
		return(newConnector);
	};

	$scope.newConnectionCreator = function () {
		$scope.tempId -= 1;

		//This is a temporary connector and will be replaced after the
		//final arrow has been saved to the database.
		const tempConnector = new draw2d.Connection({id: $scope.tempId});
		tempConnector.on(
			"added",
			function(){
				const objectId = -1;
				const sourceClassId = tempConnector.sourcePort.id.split(":")[0];
				const sourceNttId = tempConnector.sourcePort.id.split(":")[1];
				const targetClassId = tempConnector.targetPort.id.split(":")[0];
				const targetNttId = tempConnector.targetPort.id.split(":")[1];
				$scope.newArrowDetails = {
					objectId: objectId,
					name: $scope.currentDiagram.name.concat(": Arrow"),
					shortDesc: null,
					startClassId: sourceClassId,
					startNttId: sourceNttId,
					endClassId: targetClassId,
					endNttId: targetNttId
				};
				//$scope.showNewConnectorDialog();
				$scope.addArrow($scope.newArrowDetails);
				//remove the temporary connector
				$scope.canvas.remove(tempConnector);
			}
		);
		return(tempConnector);
	}

	$scope.addShape = function (clickX, clickY, shapeDetails) {
		const POST_URL = apiHost + '/data-models/shape';

		HttpService.call(POST_URL,"POST",shapeDetails,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.addShapeSCB(response.data.result_data, clickX, clickY);
					}
				}
			]
		);
	};

	$scope.addShapeSCB = function(resultData, clickX, clickY){
		let entity = {
			x : clickX-15,
			y : clickY-15,
			nttId : resultData.nttId,
			classId : resultData.classId,
			nttName : resultData.name,
			nttDesc : resultData.shortDesc,
			colorDefn : '{"bgColor":"#FFFFFF","strokeColor":"#000000"}',
			colorSource: 2
		}
		entity = $.extend(entity, resultData);
		entity.height = 30;
		entity.width = 30;
		DiagramService.addEntity(entity);
	}

	$scope.updateShape = function(shapeDetails){
		const POST_URL = apiHost + '/data-models/shape';
		HttpService.call(POST_URL,"POST",shapeDetails,null,
			[
				{
					status:1,
					callback: function(response){
						const shapeDetails = response.data.result_data;
						if(!shapeDetails.isText){
							$scope.selectedShape.updateText();
						}
						notify({
							message: "Shape details saved to the DB",
							classes: "alert-success",
							templateUrl: $scope.notifyTemplate
						});
					}
				}
			]
		);
	}

	$scope.updateArrow = function(arrowDetails){
		const POST_URL = apiHost + '/data-models/arrow';
		HttpService.call(POST_URL,"POST",arrowDetails,null,
			[
				{
					status:1,
					callback: function(response){
						notify({
							message: "Arrow details saved to the DB",
							classes: "alert-success",
							templateUrl: $scope.notifyTemplate
						});
					}
				}
			]
		);
	}

	$scope.openShapeFormatter = function(){
		$scope.selectedShapes = $scope.canvas.getSelection().all.data;
		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_format_shape.html'),
			controller: 'ontologyFormatShapeController',
			size: 'lg',
			scope: $scope
		});
	}

	$scope.moveShapeFrontBack = function(direction){
		$scope.selectedShapes = $scope.canvas.getSelection().all.data;
		for(let i = 0; i < $scope.selectedShapes.length; i++){
			if(direction === "front"){
				$scope.selectedShapes[i].toFront();
			}
			else if(direction === "back"){
				$scope.selectedShapes[i].toBack();
			}
			$scope.dirtyCanvas = true;
		}
	}

	$scope.groupShapes = function(){
		$scope.canvas.getCommandStack().execute(new draw2d.command.CommandGroup($scope.canvas, $scope.canvas.getSelection()));
		$scope.canvas.setCurrentSelection(null);
		$scope.dirtyCanvas = true;
	}
	$scope.unGroupShapes = function(){
		$scope.canvas.getCommandStack().execute(new draw2d.command.CommandUngroup($scope.canvas, $scope.canvas.getSelection()));
		$scope.canvas.setCurrentSelection(null);
		$scope.dirtyCanvas = true;
	}

	$scope.addArrow = function(arrowDetails){
		const POST_URL = apiHost + '/data-models/arrow';

		HttpService.call(POST_URL,"POST",arrowDetails,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.addArrowSCB(response, arrowDetails);
					}
				}
			]
		);
	}

	$scope.addArrowSCB = function(response, arrowDetails){
		const cnctId = response.data.result_data.returnId;
		const cnctStartNtt = arrowDetails.startClassId + ":" + arrowDetails.startNttId;
		const cnctEndNtt = arrowDetails.endClassId + ":" + arrowDetails.endNttId;

		const connector = {
			cnctId : cnctId,
			classId : MoClassService.getMoClassIdByCode("DIAGRAM_ARROW"),
			cnctName : arrowDetails.name,
			cnctDesc : arrowDetails.shortDesc,
			cnctStartNtt: cnctStartNtt,
			cnctEndNtt: cnctEndNtt,
			cnctVertices: {}
		}
		DiagramService.addConnector(connector);
	}

	/**
	 * Description: handle a double click on the canvas
	 */
	$scope.canvasDblClick = function (dblClickDetails) {
		const noSelections = ($scope.canvas.getSelection().all.data.length === 0);
		if(noSelections){
			let shapeDetails;
			//if the new shape is a text object
			if($scope.currentTemplate === 0){
				shapeDetails = {
					objectId : -1,
					name : $scope.currentDiagram.name.concat(": Text"),
					shortDesc : null,
					shapeTemplateId: $scope.shapeTemplateList[$scope.currentTemplate].templateId,
					isText: true,
					text: "New text field",
					hAlign: "left",
					vAlign: 25
				}
			}
			else{
				shapeDetails = {
					objectId : -1,
					name : $scope.currentDiagram.name.concat(": Shape"),
					shortDesc : null,
					shapeTemplateId: $scope.shapeTemplateList[$scope.currentTemplate].templateId,
					isText: false,
					text: "",
					hAlign: "center",
					vAlign: 25
				}
			}
			$scope.addShape(dblClickDetails.x, dblClickDetails.y, shapeDetails);
		}
	}

	/**
	 * Description: Opens the New shape modal dialog
	 * NOT CURRENTLY USED!
	 */
	$scope.showNewShapeDialog = function (dblClickDetails) {

		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_new_shape.html'),
			controller: 'ontologyNewShapeController',
			size: 'lg',
			resolve: { items: function () { return dblClickDetails; }},
			scope: $scope
		});

	};

	$scope.showNewConnectorDialog = function () {

		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_new_arrow.html'),
			controller: 'ontologyNewArrowController',
			size: 'lg',
			scope: $scope
		});

	};

	/**
	 * Enables editable input fields for the Ibox identified by dlIboxName
	 * @param dlIboxName
	 */
	$scope.dlIboxParentEdit = function(dlIboxName) {
		if (dlIboxName === "shape") {
			$scope.shapeEditingDisabled = false;
		}
	}

	/**
	 * Saves to the DL DB the changes made in the Ibox identified by dlIboxName
	 * @param dlIboxName
	 */
	$scope.dlIboxParentSave = function(dlIboxName) {
		if (dlIboxName === "shape") {
			$scope.shapeEditingDisabled = true;
			//$scope.saveElementCoreDetails();
		}
	}

	/**
	 * Disables editable input fields for the Ibox identified by dlIboxName
	 * @param dlIboxName
	 */
	$scope.dlIboxParentCancel = function(dlIboxName) {
		if (dlIboxName === "shape") {
			$scope.shapeEditingDisabled = true;
		}
	}

	$scope.addClickThrough = function(){

		const currentStateSpec = $stateParams.invocationParams;

		NavigationChainService.storeCurrentState(currentStateSpec);

		const invocationParams = {
			stateName:"datamodels.new_shape_click_through",
			objectId:$scope.selectedShape.nttId,
			classId:$scope.selectedShape.classId,
			parentObjectId:0,
			parentClassId:0,
			stateParams: {}
		};
		$state.go(invocationParams.stateName, {invocationParams:invocationParams});
	}

	$scope.checkClickThrough = function(){
		if($scope.selectedShape.ctObjectId > 0 || $scope.selectedShape.ctClassId > 0){
			$scope.warningDialogSettings = {
				title: "Warning! Click through already defined",
				text:
					"<p>Warning!</p>" +
					"<p>If you change the click through, the current click through will be deleted.</p>" +
					"<p>Do you wish to continue?</p>",
				confirmButton: "Continue",
				cancelButton: "Cancel"
			}
			$uibModal.open({
				templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
				controller: 'CmnWarningController',
				size: 'md',
				scope: $scope
			});
		}
		else{
			$scope.addClickThrough();
		}
	}

	$scope.warningAccepted = function(){
		$scope.addClickThrough();
	}

	$scope.clickThrough = function(){
		const currentStateSpec = $stateParams.invocationParams;

		NavigationChainService.storeCurrentState(currentStateSpec);

		const classId = $scope.selectedShape.ctClassId;
		const objectId = $scope.selectedShape.ctObjectId;

		DetailsClickThroughService.clickThrough(objectId, classId);
	}

	$scope.getShapeTemplates = function(){
		const GET_URL = apiHost + '/data-models/diagram/shape-templates';

		HttpService.call(GET_URL,"GET",null,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.shapeTemplateList = response.data.result_data;
						for(let i = 0; i < $scope.shapeTemplateList.length; i++){
							$scope.shapeTemplateList[i].index = i;
							$scope.shapeTemplateList[i].color = "#1A1A1A";
						}
						$scope.shapeTemplateList[$scope.currentTemplate].color = "#1ab394"; //matches the btn-primary class that is used by the taxonomy arrows.
					}
				}
			]
		);
	}

	$scope.selectTemplate = function(index){
		$scope.shapeTemplateList[$scope.currentTemplate].color = "#1A1A1A";
		$scope.currentTemplate = index;
		$scope.shapeTemplateList[$scope.currentTemplate].color = "#1ab394";
	}

	$scope.syncColorSelection = function () {
		$scope.colorSample = {'background-color': $scope.colorValue};
	}

	$scope.toggleNodes = function() {
		$scope.nodesVisible = !$scope.nodesVisible;
		const nodes = $scope.canvas.getAllPorts().data;
		for(let i = 0; i < nodes.length; i++){
			nodes[i].setVisible($scope.nodesVisible);
		}
	}

	$scope.goBack = function() {
		if($scope.dirtyCanvas){
			const msg = "There are unsaved changes. Please save or close the diagram before going back";
			notify({
				message: msg,
				classes: "alert-warning",
				templateUrl: $scope.notifyTemplate
			});
		}
		else{
			NavigationChainService.goBack();
		}
	}

	$scope.checkRbac = function(module, actionCategory) {
		const mdObjectOwner = 0;  // TODO: implement object ownership check
		return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
	}

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ontologyDiagramController', ontologyDiagramController);
