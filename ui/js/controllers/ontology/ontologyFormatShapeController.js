/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Overlay controller
 */
function ontologyFormatShapeController ($scope, $uibModalInstance, notify) {

    $scope.elementSelector = "bgColor";
    $scope.entityColorSettings = {};

    $scope.noFill = false;
    $scope.noStroke = false;
    $scope.hAlign = null;
    $scope.vAlign = null;

    let colorDefinition = {
        bgColor: "#FFFFFF",
        strokeColor: "#000000"
    };

    // if only 1 shape is selected, get color definition for it
    if ($scope.selectedShapes.length === 1) {
        colorDefinition = $scope.selectedShapes[0].colorDefn;
        if(colorDefinition.bgColor == null){
            $scope.noFill = true;
            $scope.colorValue = "#FFFFFF";
        }
        else{
            $scope.colorValue = colorDefinition.bgColor;
        }
        if(colorDefinition.strokeColor == null){
            $scope.noStroke = true;
        }

    }
    else if($scope.selectedShapes.length === 0) {
        const message = "Please select a shape to format";
        notify({
            message: message,
            classes: "alert-warning",
            duration: 30000,
            templateUrl: $scope.notifyTemplate
        });
        //Todo: find a neater way to do this. Maybe make the check before opening the dialog.
        setTimeout(function(){$uibModalInstance.close();},50);
    }

    $scope.entityColorSettings.bgColor = colorDefinition.bgColor;
    $scope.entityColorSettings.strokeColor = colorDefinition.strokeColor;

    // $scope.colorSample is used to display a color sample for each element on the color formatter dialog
    $scope.colorSample = {};
    $scope.colorSample.bgColor = {'background-color': $scope.entityColorSettings.bgColor};
    $scope.colorSample.strokeColor = {'background-color': $scope.entityColorSettings.strokeColor};

    /**
     * Description: set color specifications for all selected shapes.
     */
    $scope.ok = function () {
        for (let i = 0; i < $scope.selectedShapes.length; i++) {
            const currentShape = $scope.selectedShapes[i];
            const colorDef = currentShape.getColorDefn();
            if($scope.noFill){
                colorDef.bgColor = null;
            }
            else if($scope.entityColorSettings.bgColor){
                colorDef.bgColor = $scope.entityColorSettings.bgColor;
            }

            if($scope.noStroke){
                colorDef.strokeColor = null;
            }
            else if($scope.entityColorSettings.strokeColor){
                colorDef.strokeColor = $scope.entityColorSettings.strokeColor;
            }
            currentShape.setColorDefn(colorDef)

            if($scope.hAlign != null) currentShape.setHAlign($scope.hAlign);
            if($scope.vAlign != null) currentShape.setVAlign($scope.vAlign);
            if($scope.vAlign != null || $scope.hAlign != null){
                let listed = false;
                for(let i = 0; i < $scope.updatedShapes.length; i++){
                    if($scope.updatedShapes[i].id === currentShape.id){
                        listed = true;
                        break;
                    }
                }
                if(!listed) $scope.updatedShapes.push(currentShape);
            }

            currentShape.updateText();
        }

        $scope.$parent.dirtyCanvas = true;
        $uibModalInstance.close();
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    /**
     * Description: set colors based on user selection with radio button
     */
    $scope.selectElement = function () {
        if ($scope.elementSelector === 'bgColor') {
            $scope.colorValue = $scope.entityColorSettings.bgColor;
        } else {
            if ($scope.elementSelector === 'strokeColor') {
                $scope.colorValue = $scope.entityColorSettings.strokeColor;
            }
        }
    }


    /**
     * Description: set colors based on user selection with radio button
     */
    $scope.syncColorSelection = function () {
        if ($scope.elementSelector === 'bgColor') {
            $scope.entityColorSettings.bgColor = $scope.colorValue;
            $scope.colorSample.bgColor = {'background-color': $scope.entityColorSettings.bgColor};
        } else if ($scope.elementSelector === 'strokeColor') {
            $scope.entityColorSettings.strokeColor = $scope.colorValue;
            $scope.colorSample.strokeColor = {'background-color': $scope.entityColorSettings.strokeColor};
        }
    }

    $scope.selectTextHAlign = function(align){
        switch (align) {
            case "start":
            case "end":
                $scope.hAlign = align;
                break;
            default:
                $scope.hAlign = "center";
        }
    }

    $scope.selectTextVAlign = function(align){
        $scope.vAlign = align;
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ontologyFormatShapeController', ontologyFormatShapeController);