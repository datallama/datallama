/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Overlay controller
 */
function ontologyArrowDetailsController ($scope, $uibModalInstance, $uibModal) {

    $scope.validation = {
        name: true
    };

    $scope.clickThroughButton = "Add Click Through";
    if($scope.selectedShape.clickThroughMdContext.length > 0){
        $scope.clickThroughButton = "Change Click Through";
    }

    $scope.ok = function () {
        let valid = true;

        if($scope.selectedShape.cnctName && $scope.selectedShape.cnctName.length > 0){
            $scope.validation.name = true;
        }
        else{
            $scope.validation.name = false;
            valid = false;
        }
        if(valid){
            const startIds = $scope.selectedShape.sourcePort.id.split(":");
            const endIds = $scope.selectedShape.targetPort.id.split(":");
            const arrowDetails = {
                objectId : $scope.selectedShape.cnctId,
                name : $scope.selectedShape.cnctName,
                shortDesc : $scope.selectedShape.cnctDesc,
                startNttId : startIds[1],
                endNttId : endIds[1]
            }
            $scope.updateArrow(arrowDetails);
            $uibModalInstance.close();
        }
    };

    $scope.preCheckClickThrough = function() {
        $uibModalInstance.close();
        $scope.addClickThroughStateParams.objectClass = "Diagram Arrow";
        $scope.addClickThroughStateParams.objectContext = $scope.selectedShape.cnctName;
        $scope.selectedShape.nttId = $scope.selectedShape.cnctId;
        $scope.checkClickThrough();
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ontologyArrowDetailsController', ontologyArrowDetailsController);