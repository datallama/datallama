/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomies Overview view
 */
function OntologyOverviewController($rootScope, $scope, $state, $stateParams, $location, $compile, notify,
                                    NavigationChainService, HttpService, RbacService) {

    $scope.editOverviewDesc = false;
    $scope.ontologyOverviewText = "";
    $scope.ontologyOverviewTextUndo = "";
    $scope.overviewDescReadOnly = null;

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: Initialises the OntologyOverviewController
     */
    $scope.initTaxoOverview = function() {
        $scope.perms.create = !RbacService.checkRbac('ont','create');
        $scope.perms.update = !RbacService.checkRbac('ont','update');
        $scope.perms.delete = !RbacService.checkRbac('ont','delete');
        $scope.perms.publish = !RbacService.checkRbac('ont','publish');

        $scope.overviewDescReadOnly = document.getElementById("overviewDescReadOnly");
        const stateName = $state.current.name.replace(/\./g, "_");

        const GET_URL = apiHost + '/ontology/overview/' + stateName;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.initTaxoOverviewSCB(response);
                    }
                }
            ]
        );
    }

    $scope.initTaxoOverviewSCB = function(response){
        $scope.ontologyOverviewText = response.data.result_data.pageContent;
        $scope.overviewDescReadOnly.innerHTML = $scope.ontologyOverviewText;
        $compile($scope.overviewDescReadOnly)($scope);
    }

    $scope.goBack = function() {
        // TODO: implement go back functionality for top level pages
    }

    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "overview_desc") {
            $scope.ontologyOverviewTextUndo = $scope.ontologyOverviewText;
            $scope.editOverviewDesc = true;
        }
    }

    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "overview_desc") {
            $scope.editOverviewDesc = false;

            const overviewPageContent = {
                overviewId: 0,  // not used in stored proc at present
                stateName: $state.current.name,
                pageContent: $scope.ontologyOverviewText
            }

            // make the REST POST call to save the overview page content to the database
            const POST_URL = apiHost + "/ontology/overview";

            HttpService.call(POST_URL,"POST",overviewPageContent,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.dlIboxSaveSCB(overviewPageContent);
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: Update both read-write and read-only content variables after database update has succeeded
     * @param overviewPageContent
     */
    $scope.dlIboxSaveSCB = function(overviewPageContent){
        $scope.ontologyOverviewText = overviewPageContent.pageContent;
        $scope.overviewDescReadOnly.innerHTML = $scope.ontologyOverviewText;
        $compile($scope.overviewDescReadOnly)($scope);

        notify({
            message: "Changes saved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });
    }

    /**
     * Description: Cancel the edit action in the dlIbox identified by dlIboxToolsName
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "overview_desc") {
            $scope.ontologyOverviewText = $scope.ontologyOverviewTextUndo;
            $scope.overviewDescReadOnly.innerHTML = $scope.ontologyOverviewText;
            $scope.editOverviewDesc = false;
        }
    }

    /**
     * Navigate to the view identified by state targetState
     * @param targetState
     */
    $scope.clickThrough = function(targetState) {
        const currentStateSpec = {
            stateName:"ontology.overview",
            objectId:0,
            classId:0,
            stateParams:{}};
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {  // data store
            stateName: targetState,
            objectId: 0,
            classId: 0,
            stateParams: ""};
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});

    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('OntologyOverviewController', OntologyOverviewController);
