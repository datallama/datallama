/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Format XML Detail view.
 *
 * The structure of the element object stored in $scope.elements, $scope.selectedElement is:
 * {objectId: 0, elementId: 0, classId: 0, parentObjectId: 0, elementLevel: 0, elementLevelSequence: 0, elementType: "s",
 *  elementGUID: "s", elementHierarchyNumber: "s", elementName: "s", elementShortDesc: "s", elementFullPathName: "s",
 *  elementVisible: 0, initialExpandLevel: 0, indentCss": "s", expanded: true, parent: "s", resultSetSequence: 0,
 *  elementSelected: 0, elementMatched: false, mdContext: [{classID: 0 className": "s", objectID": 1, objectName: "s", internalPath: "s"}] }
 */
function SmdPresentationDetailController($scope, $rootScope, $location, $state, $stateParams, $window, $uibModal, NavigationChainService,
                                notify, DTOptionsBuilder, HttpService, RbacService, MoClassService, UtilityService) {
    $scope.elements = [];
    $scope.elementsTableView = [];
    $scope.parentObjectId = 0;
    $scope.parentClassId = 0;
    $scope.presentationSummary = {};
    $scope.selectedElement = null;
    $scope.selectedElementDisplay = null;
    $scope.selectedElementAssociations = [];
    $scope.previousResultSetSequence = 0;
    $scope.newElement = {
        name: "",
        desc: "",
        topLevelElement : true,
        attribute: false
    };

    $scope.customForm = {};

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveElement = {moveMode: false, moveTargetelement: null};

    $scope.editingDisabled = {attr: true, cfrm: true};
    $scope.editingUndo = {attr: {}, cfrm: {}};
    $scope.activeDetailTab = {attributes:true, associations:false, comments:false,  customform:false, more:false, activeIndex:1};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'REST API Elements'},
        ]);

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";
    $scope.deleteAll = true;

    /**
     * Description: initialisation function for the controller
     */
    $scope.initPresentationDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.parentObjectId = $scope.invocationParams.parentObjectId;
        $scope.parentClassId = $scope.invocationParams.parentClassId;
        $scope.activeDetailTab = $scope.invocationParams.stateParams.activeDetailTab;

        $scope.getElementType();
        $scope.readElements();

        if($scope.invocationParams.objectId > 0) { 
            $scope.loadSelectedElement($scope.invocationParams.objectId);
        }
    }

    $scope.getElementType = function() {
        
        switch ($scope.parentClassId) {
            case  MoClassService.getMoClassIdByCode('PRSNT_RPT'):
                $scope.elementClass = {
                    name: 'Field',
                    classId: MoClassService.getMoClassIdByCode('PRSNT_RPT_FIELD')
                };
                break;
            case  MoClassService.getMoClassIdByCode('PRSNT_FORM'):
                $scope.elementClass = {
                    name: 'Field',
                    classId: MoClassService.getMoClassIdByCode('PRSNT_FORM_FIELD')
                };
                break;
            case  MoClassService.getMoClassIdByCode('PRSNT_VIS'):
                $scope.elementClass = {
                    name: 'Feature',
                    classId: MoClassService.getMoClassIdByCode('PRSNT_VIS_FEATURE')
                };
                break;
        }

    }

    $scope.readElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/smd/presentation/summary/' + $scope.parentObjectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.presentationSummary = response.data.result_data.presentation;
                        $scope.elements = response.data.result_data.presentation_details;

                        // Parse the JSON values for custom form
                        $scope.customForm.customFormId = response.data.result_data.presentation.customForm.customFormId;
                        $scope.customForm.customFormName = response.data.result_data.presentation.customForm.customFormName;
                        $scope.customForm.customFormShortDesc = response.data.result_data.presentation.customForm.customFormShortDesc;
                        $scope.customForm.schemaDefinition = response.data.result_data.presentation.customForm.schemaDefinitionJson;
                        $scope.customForm.formDefinition = JSON.parse(response.data.result_data.presentation.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse('{}');
                        $scope.disableCustomFormFields();

                        angular.copy($scope.elements, $scope.elementsTableView);
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: loads the selected presentation details when returning to the view from a Go Back action
     * @param objectId
     */
    $scope.loadSelectedElement = function(objectId) {

        // 2. get full details of the node from the DL database
        const GET_URL = apiHost + '/smd/presentation/detail/' + objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processSelectedElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the modal dialag that allows the user to import an XML file
     */
    $scope.openLoadXMLMessageFormatsDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/load_xml_msg_format.html'),
            controller: 'LoadXMLFormatController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: enables fields in selected element display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "You have not selected an element. No edits can be made.",
                classes: "alert-warning",
                duration: 5000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            if (dlIboxName === "ted") {
                angular.copy($scope.selectedElementDisplay, $scope.editingUndo.attr);
                $scope.editingDisabled.attr = false;
            }
            else if (dlIboxName === "cfrm") {
                $scope.enableCustomFormFields();
                $scope.editingDisabled.cfrm = false;
            }
        }
    }

    /**
     * Description: saves changes made by user to selected element
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.editingDisabled.attr = true;
            $scope.saveElementCoreDetails();
        }
        else if (dlIboxName === "cfrm") {
            $scope.editingDisabled.cfrm = true;
            $scope.saveSelectedCustomFormData();
            $scope.disableCustomFormFields();
        }
    }

    /**
     * Description:
     * @param dlIboxName
     */
    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.editingDisabled.attr = true;
            // reset all fields to the pre-edit state
            angular.copy($scope.editingUndo.attr, $scope.selectedElementDisplay);
        }
        else if (dlIboxName === "cfrm") {
            $scope.editingDisabled.cfrm = false;
        }
    }

    $scope.remove = function(scope) {
        scope.remove();
    };
    $scope.toggle = function(scope) {
        scope.toggle();
    };
    $scope.moveLastToTheBeginning = function () {
        const a = $scope.data.pop();
        $scope.data.splice(0,0, a);
    };

    $scope.initiateNewElement = function() {
        $scope.newElementConfig = {
            topLevelElement: false,
            pathSelectable: false,
            methodSelectable: false,
            responseSelectable: false,
            parentElementClassId: 0,
            elementType: "path"
        };

        if ($scope.selectedElement == null) {
            $scope.newElementConfig.topLevelElement = true;
            $scope.newElementConfig.pathSelectable = true;
            $scope.newElementConfig.methodSelectable = false;
            $scope.newElementConfig.responseSelectable = false;
            $scope.newElementConfig.parentElementClassId = 0;
            $scope.newElementConfig.elementType = "path";
        }
        else {
            $scope.newElementConfig.topLevelElement = false;
            $scope.newElementConfig.parentElementClassId = $scope.selectedElement.parentClassId;
        }

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_presentation_detail.html'),
            controller: 'CreatePresentationDetailController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: collapses the whole taxonomy down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.elements.length; i++) {
            if ($scope.elements[i].elementLevel > 0) {
                $scope.elements[i].elementIsVisible = 0;
            }
            $scope.elements[i].elementIsExpanded = false;
        }
    };

    /**
     * Description: expands every element in the taxonomy
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.elements.length; i++) {
            $scope.elements[i].elementIsVisible = 1;
            $scope.elements[i].elementIsExpanded = true;
        }
    };

    /**
     * Description: expands the taxonomy to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.elements.length; i++) {
            if ($scope.elements[i].elementLevel < $scope.config.visibilityLevel - 1) {
                $scope.elements[i].elementIsVisible = 1;
                $scope.elements[i].elementIsExpanded = true;
            }
            else if ($scope.elements[i].elementLevel === $scope.config.visibilityLevel - 1) {
                $scope.elements[i].elementIsVisible = 1;
                $scope.elements[i].elementIsExpanded = false;
            }
            else {
                $scope.elements[i].elementIsVisible = 0;
                $scope.elements[i].elementIsExpanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param element
     */
    $scope.toggleElementExpansion = function(element) {
        const setVisibility = (element.elementIsExpanded) ? 0 : 1;
        const ehnMatch = element.elementHierarchyNumber + ".";

        if (element.elementIsExpanded) {         // collapse all levels below this element
            for(let i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.elements[i].elementLevel > element.elementLevel){
                    $scope.elements[i].elementIsVisible = 0;
                    $scope.elements[i].elementIsExpanded = false;
                }
                if ($scope.elements[i].objectId === element.objectId)
                    $scope.elements[i].elementIsExpanded = false;
            }
        }
        else {  // expand all levels below this element
            for(let i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.elements[i].elementLevel === element.elementLevel + 1){
                    $scope.elements[i].elementIsVisible = 1;
                }
                if ($scope.elements[i].objectId === element.objectId)
                    $scope.elements[i].elementIsExpanded = true;
            }
        }
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.elementsTableView = [];
        if ($scope.config.endnodes) {
            let endnodes = [];
            for(let i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].elementIsParent === false) {
                    endnodes.push($scope.elements[i]);
                }
            }
            angular.copy(endnodes, $scope.elementsTableView);
        }
        else {
            angular.copy($scope.elements, $scope.elementsTableView);
        }
    }

    /**
     * Description: saves a new Presentation Detail to the DL database
     */
    $scope.saveNewElement = function(newElementSpec) {

        let parentElementObjectId = 0;
        if(!newElementSpec.topLevelElement){
            parentElementObjectId = $scope.selectedElement.objectId;
        }

        let classId = $scope.elementClass.classId;

        const newElement = {
            objectId: -1,
            classId: classId,
            parentObjectId: $scope.parentObjectId,
            parentClassId: $scope.parentClassId,
            parentElementObjectId: parentElementObjectId,
            elementName: newElementSpec.name,
            elementShortDesc: newElementSpec.shortdesc,

        };

        const POST_URL = apiHost + "/smd/presentation/detail";

        HttpService.call(POST_URL, "POST", newElement, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processNewElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     *  Splices a new Presentation Detail into the $scope.elements array
     * @param response
     */
    $scope.processNewElementResponse = function(response){
        // TODO: new element should be spliced into the $scope.elements array. For now just refresh the entire list
        $scope.readElements();
    }

    /**
     * Description: saves to the DL database the updated core details for the currently selected Presentation Detail
     */
    $scope.saveElementCoreDetails = function() {
        const POST_URL = apiHost + "/smd/presentation/detail";

        HttpService.call(POST_URL, "POST", $scope.selectedElementDisplay, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        // update the selected json node with any values changed in the LHS display panel
                        $scope.selectedElement.elementName = $scope.selectedElementDisplay.elementName;
                        $scope.selectedElement.elementShortDesc = $scope.selectedElementDisplay.elementShortDesc;

                        notify({
                            message: "Changes were successfully saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form attributes for this table/view and all columns " +
                "that inherit the custom form definition will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: This function is invoked when the Change Custom Form warning dialog is closed by the OK button.
     */
    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/format/customform/";
        const postValues = {
            "objectId": $scope.parentObjectId,
            "classId": $scope.parentClassId,
            "customFormId": selectedCustomForm.customFormId
        };

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form association successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.updateCustomFormDefinition(selectedCustomForm.customFormId);
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the custom form definition after it has been change by the user.
     * @param customFormId
     */
    $scope.updateCustomFormDefinition = function(customFormId) {
        const GET_URL = apiHost + "/common/customform/" + customFormId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.customForm = response.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data to the DL DB for the selected Presentation Detail
     */
    $scope.saveSelectedCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedElementDisplay.objectId,
            classId: $scope.selectedElementDisplay.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description; reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedElement.objectId + "/" + $scope.selectedElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    $scope.toggleCommentPanel = function() {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "You have not selected an element. No comments can be added.",
                classes: "alert-warning",
                duration: 5000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden;
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedElement == null) {
            notify({
                message: "You have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedElement.objectId,
                classId: $scope.selectedElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL,"POST",newComment,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a taxonomy element when it is clicked in the hierarchy view
     * @param selectedElement
     * TODO: need to find a way to shift focus to Attributes tab
     */
    $scope.showElementDetails = function(selectedElement) {

        if ($scope.moveElement.moveMode) {
            if (selectedElement.elementHierarchyNumber.startsWith($scope.selectedElement.elementHierarchyNumber)) {
                notify({ message: "You cannot move an element to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.moveElement.moveTargetelement = selectedElement;
                $scope.setActiveTab(4);
            }
        }
        else {
            $scope.elements[$scope.previousResultSetSequence].elementIsSelected = 0;

            $scope.selectedElement = $scope.elements[selectedElement.resultSetSequence];
            $scope.elements[$scope.selectedElement.resultSetSequence].elementIsSelected = 1;
            $scope.previousResultSetSequence = $scope.selectedElement.resultSetSequence;

            $scope.resetEditing();
            $window.scrollTo(0, 0);

            let GET_URL = apiHost + '/smd/presentation/detail/' + $scope.selectedElement.objectId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.processSelectedElementResponse(response);
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: processes the element details returned by API to support display in the view
     * @param response
     */
    $scope.processSelectedElementResponse = function(response) {
        $scope.selectedElementDisplay = response.data.result_data.attributes;
        $scope.selectedElementDisplay.elementHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedElementDisplay.elementHierarchyNumber);
        $scope.selectedElementAssociations = response.data.result_data.associations;
        $scope.customForm.formData = JSON.parse(response.data.result_data.custom_form.formData);
        $scope.readElementComments();
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteAll) {
        $scope.deleteAll = deleteAll
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    $scope.deleteMdObject = function () {
        if($scope.deleteAll){
            $scope.deletePresentation()
        }
        else{
            $scope.deletePresentationDetail()
        }
    }

    $scope.deletePresentation = function (){
        const DELETE_URL = apiHost + "/smd/presentation/" + $scope.parentObjectId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Presentation successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }


    $scope.deletePresentationDetail = function (){
        const DELETE_URL = apiHost + "/smd/presentation/detail/" + $scope.selectedElement.objectId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Presentation Detail successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.readElements();
                    }
                }
            ]
        );
    }

    /**
     * Description: invokes the $scope.clickThrough function to open the Full Association View for the Presentation definition
     */
    $scope.openFullAssociationView = function(){
        const stateParams = {
            clickThroughMode: "full_association_view",
            objectClass: "PRESENTATION",
            objectContext: ""
        };

        // because the full association view displays details for the Presentation definition, the objectId and classId parameters
        // for the clickThrough function are set to parentObjectId and parentClassId from the invocation Params
        $scope.clickThrough($scope.invocationParams.parentObjectId, $scope.invocationParams.parentClassId,
            0, 0, stateParams);
    }

    /**
     * Description: opens the Object Association modal dialog
     */
    $scope.newAssociation = function() {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "You have not selected an element. No associations can be made.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            // execute the navigation transition
            // navigate to the New Association view
            const stateParams = {
                clickThroughMode: "new_association",
                objectClass: "PRESENTATION_DETAIL",
                objectContext: $scope.selectedElement.mdContext[1].internalPath
            };

            $scope.clickThrough($scope.selectedElement.objectId, $scope.selectedElement.classId,
                $scope.invocationParams.parentObjectId, $scope.invocationParams.parentClassId,
                stateParams);
        }
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.activeDetailTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
                break;
            case 2:
                $scope.activeDetailTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "more":false, "activeIndex":2};
                break;
            case 3:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "more":false, "activeIndex":3};
                break;
            case 4:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "more":false, "activeIndex":4};
                break;
            case 5:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":false, "customform":false, "more":true, "activeIndex":5};
                break;
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.tedEditingDisabled = true;
        $scope.cfrmEditingDisabled = true;
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
        $scope.disableCustomFormFields();
    }

    /**
     * Description: click through to detail
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, stateParams) {
        let currentStateObjectId = 0;
        let currentStateClassId = 0;
        if ($scope.selectedElement != null) {
            currentStateObjectId = $scope.selectedElement.objectId;
            currentStateClassId = $scope.selectedElement.classId;
        }

        var currentStateSpec = {
            stateName: "smd.smd_presentation_detail",
            objectId: currentStateObjectId,
            classId: currentStateClassId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            stateParams:{
                activeDetailTab: $scope.activeDetailTab
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {stateName: "", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};

        let targetState = "not_defined";
        if (stateParams.hasOwnProperty("clickThroughMode")) {
            switch (stateParams.clickThroughMode) {
                case "full_association_view":
                    targetState = "associations.mdobject_fullview";
                    break;
                case "new_association":
                    targetState = "associations.new_association";
                    break;
                case "click_through":
                    let moClass = MoClassService.getMoClassByClassId(classId);
                    targetState = moClass.objectDetailState;
                    break;
            }
        }

        invocationParams.stateName = targetState;
        invocationParams.objectId = objectId;
        invocationParams.classId = classId;
        invocationParams.parentObjectId = parentObjectId;
        invocationParams.parentClassId = parentClassId;
        invocationParams.stateParams = stateParams;

        if (invocationParams.stateName.length > 0) {
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdPresentationDetailController', SmdPresentationDetailController);


