/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main database detail view
 */
function SmdColumnDetailController($scope, $rootScope, $location, $state, $stateParams, $uibModal, NavigationChainService, notify,
                                   UtilityService, DTOptionsBuilder, HttpService, RbacService, MoClassService, DataSensitivityService) {

    $scope.columnDetails = {coreDetails:{}, associations:[], fullDescription: ""};
    $scope.columnCoreDetailsUndo = {};
    $scope.customForm = {};
    $scope.customFormUndo = {};
    $scope.gridData01 = [];
    $scope.gridData02 = [];
    $scope.dlDsTags = [];


    $scope.editingDisabled = {"coreDetails":true, "associations":true}

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'SMD Column Details'},
            {extend: 'pdf', title: 'SMD Column Details'}
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialising function for the controller. Read details of the database from DL database
     */
    $scope.initSmdColumnDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');

        $scope.invocationParams = $stateParams.invocationParams;

        $scope.refreshColumnDetails($scope.invocationParams.objectId);
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.columnDetails.coreDetails, $scope.columnCoreDetailsUndo)
            $scope.editingDisabled.coreDetails = false;
        }
        else if (dlIboxToolsName === "associations") {
            $scope.editingDisabled.associations = false;
        }
        else if (dlIboxToolsName === "custom_form") {
            angular.copy($scope.customForm, $scope.customFormUndo);
            $scope.enableCustomFormFields();
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            $scope.saveCoreDetails();
            $scope.editingDisabled.coreDetails = true;
        }
        else if (dlIboxToolsName === "associations") {
            $scope.editingDisabled.associations = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            $scope.disableCustomFormFields();
            $scope.saveCustomFormData();
        }
    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.columnCoreDetailsUndo, $scope.databaseCoreDetails);
            $scope.editingDisabled.coreDetails = true;
        }
        else if (dlIboxToolsName === "associations") {
            $scope.editingDisabled.associations = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            angular.copy($scope.customFormUndo, $scope.customForm);
            $scope.disableCustomFormFields();
        }
    }

    /**
     * Returns the CSS Class for the specified data sensitivity by invoking the DataSensitivityService.
     * @param dataSensitivityLevel
     * @returns {*}
     */
    $scope.getDataSensitivityCssClass = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Saves core details of the database to the DL database
     */
    $scope.saveCoreDetails = function() {
        const postValues = {
            columnId:$scope.columnDetails.coreDetails.columnId,
            columnShortDesc:$scope.columnDetails.coreDetails.columnShortDesc,

        };
        const POST_URL = apiHost + "/smd/database/column/summary";

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Column description saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves custom form data to the DL database
     */
    $scope.saveCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.columnDetails.coreDetails.columnId,
            classId: $scope.columnDetails.coreDetails.classId,
            parentObjectId: $scope.columnDetails.coreDetails.parentObjectId,
            parentClassId: $scope.columnDetails.coreDetails.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: read details of the column from the DL DB, including associations and full descriptions
     * @param columnId
     */
    $scope.refreshColumnDetails = function(columnId) {
        const GET_URL = apiHost + "/smd/database/column/" + columnId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.columnDetails.coreDetails = response.data.result_data.core_details;
                        $scope.customForm = response.data.result_data.core_details.customForm;
                        $scope.customForm.schemaDefinition = $scope.customForm.schemaDefinitionJson;
                        $scope.customForm.formDefinition = JSON.parse(response.data.result_data.core_details.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse(response.data.result_data.core_details.customForm.formData);

                        $scope.columnDetails.associations = response.data.result_data.associations;
                        $scope.disableCustomFormFields();
                    }
                }
            ]
        );
    }

    /**
     * Description: goes to the Object Association view
     */
    $scope.newAssociation = function() {
        const currentStateSpec = {
            stateName:"smd.column_detail",
            objectId:$scope.invocationParams.objectId,
            classId:$scope.invocationParams.classId,
            stateParams:{
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const classDetails = MoClassService.getMoClassByClassId($scope.invocationParams.classId);
        const invocationParams = {
            stateName:"associations.new_association",
            objectId:$scope.invocationParams.objectId,
            classId:$scope.columnDetails.coreDetails.classId,
            stateParams:{
                objectClass:classDetails.className,
                objectContext:$scope.columnDetails.coreDetails.columnName
            }
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        console.log("smdColumnDetail - disableCustomFormFields");
        console.log(customFormCopy);
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        /*
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
        */
        console.log("enableCustomFormFields");
        console.log($scope.customForm);
        console.log("process formDefinition");
        for (let i = 0; i < $scope.customForm.formDefinition.length; i++) {
            console.log(fieldDef);
            console.log(typeof(fieldDef));
        }

        for (const fieldDef in $scope.customForm.schemaDefinition.properties) {
            const setreadonly = "$scope.customForm.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        console.log("smdColumnDetail - enableCustomFormFields");
        console.log($scope.customForm);
        $scope.$broadcast('schemaFormRedraw');
    }

    /**
     *
     */
    $scope.openDSassignSingleDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_assign_data_sensitivity_single.html'),
            controller: 'CmnAssignDataSensitivitySingle',
            size: 'md',
                resolve: { currentDsTagId: function () { return $scope.columnDetails.coreDetails.dlDsTag.dlDsTagId; }},
            scope: $scope
        });
    }

    /**
     * Update the assigned data sensitivity tag
     */
    $scope.updateDataSensitivityTag = function (selectedDsTag) {
        $scope.columnDetails.coreDetails.dlDsTag = selectedDsTag;

        const postValues = {
            objectId:$scope.columnDetails.coreDetails.columnId,
            dlDsTagId:selectedDsTag.dlDsTagId
        };
        const POST_URL = apiHost + "/smd/database/column/datasensitivity";

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Column data sensitivity updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Processes customer navigation clicks
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        NavigationChainService.storeCurrentState($scope.invocationParams);
        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:MoClassService.getMoClassByClassId($scope.invocationParams.classId).className,
                    objectContext:$scope.columnDetails.coreDetails.columnName
                }
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }
    
    /**
     * Description: return to the state from which this state was invoked
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdColumnDetailController', SmdColumnDetailController);
