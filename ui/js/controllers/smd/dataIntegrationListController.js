/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Message Formats List
 */
function DataIntegrationListController($scope, $rootScope, $location, $state, $uibModal, notify, DTOptionsBuilder,
                                     NavigationChainService, HttpService, MoClassService, UtilityService) {
    $scope.gridData = [];


    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'FormatList'},
            {extend: 'pdf', title: 'FormatList'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initDataIntegrationList = function () {
        $scope.readGridData();
    }

    /**
     * Click through to the details view for the selected message format
     * @param objectId
     * @param classId
     * @param objectDetailState
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, stateParams) {
        const currentStateSpec = {
            stateName: "smd.dataintegration_list",
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let targetState = "not_defined";
        let clickThroughMode = "not_defined";
        if (stateParams.hasOwnProperty("clickThroughMode")) {
            clickThroughMode = stateParams.clickThroughMode;
        }

        switch (clickThroughMode) {
            case "summary":
                targetState = "smd.dataintegration_summary";
                break;
            case "detail":
                // determine detail view state from parentClassId
                if (parentClassId === MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PROJECT')) {
                    targetState = 'smd.dataintegration_detail';
                }
                break;
        }

        const invocationParams = {
            stateName: targetState,
            objectId: objectId,
            classId: classId,
            parentObjectId: parentObjectId,
            parentClassId: parentClassId,
            stateParams:{activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: opens the create_message_format modal dialog
     */
    $scope.openCreateDataIntegrationDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_dataintegration.html'),
            controller: 'CreateDataIntegrationController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: creates a new message format in table dl_format
     */
    $scope.createDataIntegration = function(newObject) {
        const POST_URL = apiHost + '/smd/dataintegration';

        HttpService.call(POST_URL, "POST", newObject, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.readGridData();
                        notify({
                            message: "New Data Integration Project created successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description:
     */
    $scope.readGridData = function() {
        const GET_URL = apiHost + '/smd/dataintegration/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DataIntegrationListController', DataIntegrationListController);
