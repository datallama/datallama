/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating data model diagrams
 */
function smdConfigureDatabaseController ($scope, $uibModalInstance) {

    $scope.validation = {
        logicalDbName: true,
        dbShortDesc: true
    }

    // TODO: the list of JDBC driver families should be read from the DL database
    $scope.jdbcDriverFamilyList = [
        {"jdbcDfId":1, "jdbcDfName": "mysql", "defaultPort":3306, "pdbnLabel":"Physical Database Name"},
        {"jdbcDfId":2, "jdbcDfName": "sqlserver", "defaultPort":1433, "pdbnLabel":"Physical Database Name"},
        {"jdbcDfId":3, "jdbcDfName": "postgresql", "defaultPort":5432, "pdbnLabel":"Physical Database Name"},
        {"jdbcDfId":4, "jdbcDfName": "oracle", "defaultPort":1521, "pdbnLabel":"Service Name/SID"}
    ];

    $scope.selectedJdbcDriverFamily = {"jdbcDfId":0, "jdbcDfName": "none selected", "defaultPort":0, "pdbnLabel":"Physical Database Name"};

    $scope.newSmdDatabaseConfiguration = {
        databaseId: -1,
        serverId: -1,
        classId: 13,        // TODO: replace with MoClassService
        logicalDatabaseName: "",
        databaseName: "",
        databaseShortDesc: "",
        schemaNameList: "",
        hostname: "",
        port: 0,
        username: "",
        password: "",
        jdbcDriverFamily: ""
    };

    /**
     * Description: sets default values for the JDBC Driver Family variable
     */
    $scope.setJdbcDfDefaults = function () {
        if ($scope.selectedJdbcDriverFamily == undefined) {
            $scope.selectedJdbcDriverFamily = {"jdbcDfId":0, "jdbcDfName": "none selected", "defaultPort":0, "pdbnLabel":"Physical Database Name"};
            $scope.newSmdDatabaseConfiguration.port = 0;
            $scope.newSmdDatabaseConfiguration.jdbcDriverFamily = "";
        }
        else {
            $scope.newSmdDatabaseConfiguration.port = $scope.selectedJdbcDriverFamily.defaultPort;
            $scope.newSmdDatabaseConfiguration.jdbcDriverFamily = $scope.selectedJdbcDriverFamily.jdbcDfName;
        }
    }

    /**
     * Description:
     */
    $scope.save = function () {
        let valid = 0;
        // validate required fields
        if ($scope.newSmdDatabaseConfiguration.logicalDatabaseName.length == 0) {
            $scope.validation.logicalDbName = false;
            valid++;
        }
        if ($scope.newSmdDatabaseConfiguration.databaseShortDesc.length == 0) {
            $scope.validation.dbShortDesc = false;
            valid++;
        }

        // save new smd database core details to DL database
        if (valid == 0) {
            $scope.saveNewDatabaseCoreDetails($scope.newSmdDatabaseConfiguration);
            $uibModalInstance.close();
        }

    };

    /**
     * Description: cancels the user action and closes the overlay modal dialog
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


};

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('smdConfigureDatabaseController', smdConfigureDatabaseController);