/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for Compare Table view. The columns in the source table/view are compared by name with
 *  the columns in the target table/view. The results of the comparison are stored in array $scope.compareColumnsList
 *  The high level structure of the $scope.compareColumnsList is {sourceColumn: {}, targetColumn: {}, matchStatus: 0}
 */
function CompareTablesController($scope, $location, $state, $stateParams, NavigationChainService, notify,
                                   UtilityService, DTOptionsBuilder, HttpService, MoClassService) {
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.databaseList = [];
    $scope.selectedDatabase = {};
    $scope.schemaList = [];
    $scope.selectedSchema = {};
    $scope.tableViewList = [];
    $scope.sourceTableViewDetails = {};
    $scope.compareColumnsList = [];     // an array of objects with structure {sourceColumn: {}, targetColumn: {}, matchStatus: 0}
    $scope.targetTableView = {};
    $scope.viewMode = 1;    // 1 = select target table, 2 = display comparison results

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);


    /**
     * Description: initialises the controller.
     */
    $scope.initCompareTables = function () {
        console.log($stateParams.invocationParams.stateParams.sourceTableViewDetails);
        $scope.sourceTableViewDetails = $stateParams.invocationParams.stateParams.sourceTableViewDetails;

        // 1. create the compareColumnsList array
        for (let i = 0; i < $stateParams.invocationParams.stateParams.sourceTableViewColumns.length; i++) {
            let newCompareColumn = {
                sourceColumn: $stateParams.invocationParams.stateParams.sourceTableViewColumns[i],
                targetColumn: {},
                matchStatus: 0
            };

            $scope.compareColumnsList.push(newCompareColumn);
        }
        console.log("$scope.compareColumnsList");
        console.log(    $scope.compareColumnsList);

        // 2. get the list of all databases. The default selected DB corresponds to the source table.
        const GET_URL = apiHost + '/smd/database/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.databaseList = response.data.result_data;
                    }
                }
            ]
        );

    }

    /**
     * Description: reads all schemata for the database selected by the user
     */
    $scope.readSchemataForDatabase = function() {
        const GET_URL = apiHost + '/smd/database/schema/list/' + $scope.selectedDatabase.databaseId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.schemaList = response.data.result_data;
                    }
                }
            ]
        );

    }

    /**
     * Description: reads all tables/views for the schema selected by the user
     */
    $scope.readTableViewForSchema = function() {
        const GET_URL = apiHost + '/smd/database/schema/tableview/list/' + $scope.selectedSchema.schemaId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        console.log(response);
                        $scope.tableViewList = response.data.result_data.table_view_list;
                    }
                }
            ]
        );

    }

    /**
     * Description: Processes customer navigation clicks
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.showComparisonResults = function (objectId, classId) {
        console.log("target table objectId: " + objectId + ", target table classId: " + classId);
        $scope.viewMode = 2;
        // get the details of the target table
        const GET_URL = apiHost + '/smd/database/tableview/' + objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        console.log(response);
                        $scope.targetTableView = response.data.result_data.core_details;
                        $scope.compareTables();
                    }
                }
            ]
        );

    }

    /**
     * Description: compares by name every column in the source table to columns in target table to determine matches
     */
    $scope.compareTables = function () {
        for (let i = 0; i < $scope.compareColumnsList.length; i++) {
            let sourceColumnName = $scope.compareColumnsList[i].sourceColumn.columnName.toUpperCase();

            for (let j = 0; j < $scope.targetTableView.columnList.length; j++) {
                if (sourceColumnName == $scope.targetTableView.columnList[j].columnName.toUpperCase()) {
                    $scope.compareColumnsList[i].targetColumn = $scope.targetTableView.columnList[j];
                    // TODO: the matched target column definition should be removed from the $scope.targetTableView.columnList array
                    //  This could improve performance for tables with a large number of columns.

                }
            }
        }
        console.log("compareColumnsList after matching ...");
        console.log($scope.compareColumnsList);
    }

    /**
     * Description: allows the user to change the target table/view for comparison to source table/view
     */
    $scope.changeTargetTableView = function () {
        $scope.viewMode = 1;
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CompareTablesController', CompareTablesController);
