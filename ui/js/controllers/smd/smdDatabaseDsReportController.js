/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the SMD Database Data Sensitivity Report view
 */
function smdDatabaseDsReportController($scope, $state, $stateParams, NavigationChainService,
                                   DTOptionsBuilder, HttpService, DataSensitivityService) {

    $scope.gridData = [];
    $scope.objectId = 0;
    $scope.objectName = "";
    $scope.className = "";
    $scope.classCode = "";

    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv', title: 'smd_database_ds_report'}
        ]);

    /**
     * Description: initialisation function for the controller
     */
    $scope.initSmdDatabaseDsReport = function() {
        $scope.objectId = $stateParams.invocationParams.objectId;
        $scope.objectName = $stateParams.invocationParams.stateParams.objectContext;
        $scope.className = $stateParams.invocationParams.stateParams.className;
        $scope.classCode = $stateParams.invocationParams.stateParams.classCode;
        $scope.refreshGrid();
    }


    /**
     * Description: refreshes the grid that lists all SMD database. Invoked after adding a new SMD database definition
     */
    $scope.refreshGrid = function () {
        let GET_URL = apiHost;

        switch ($scope.classCode) {
            case "RELATIONAL_DATABASE":
                GET_URL = GET_URL + '/smd/database/datasensitivity/' + $scope.objectId;
                break;
            case "DB_SCHEMA":
                GET_URL = GET_URL + '/smd/database/schema/datasensitivity/' + $scope.objectId;
                break;
        }

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.gridData = response.data.result_data.ds_report;
                    }
                }
            ]
        );
    }

    $scope.getDsCssClass = function (dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: transitions to detail view states based on user click
     * TODO: implement for user clicks on column name
     */
    $scope.clickThrough = function(objectId, classId) {
        const currentStateSpec = {
            stateName:"smd.smd_database_list",
            objectId:0,
            classId:13,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"smd.smd_database_detail",
            objectId: objectId,
            classId: classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});

    }
}


/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('smdDatabaseDsReportController', smdDatabaseDsReportController);
