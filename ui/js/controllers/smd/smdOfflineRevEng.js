/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The controller for the UIB modal dialogue where you can load a JSON file
 *              and turn it into a JSON message format in the DL data base
 */
function OfflineRevEngController ($uibModalInstance, $scope, $http, notify, HttpService) {
    $scope.dbSmdFileType = "none_selected";
    $scope.validation = {
        filetype: true,
        filetypecheck: true,
        fileselected: true
    };

    $scope.spinnerLoading = "";
    $scope.disableButtons = false;

    /**
     * Description: starts the spinner which indicates that the reverse engineering run is in progress.
     * @param value
     */
    $scope.setSpinnerLoading = function(value) {
        $scope.spinnerLoading = value;
        $scope.disableButtons = (value !== "");
    }

    /**
     * Description: validates that a valid database definition file has been selected.
     *  Valid database definition file formats are:
     *  1. Data Llama database structural metadata JSON
     *  2. SchemaCrawler java serialisation file
     */
    $scope.checkValid = function(){
        let valid = true;
        let inputFile = "";

        // 1. check that am SMD definition file has been selected
        if (document.getElementById('inputFile').files.length == 0) {
            $scope.validation.fileselected = false;
            valid = false;
        }
        else {
            $scope.validation.fileselected = true;
            valid = true;
        }

        // 2. check that a database SMD definition file TYPE has been selected
        if ($scope.dbSmdFileType == "none_selected") {
            $scope.validation.filetype = false;
            valid = false;
        }
        else {
            $scope.validation.filetype = true;
            valid = true;
        }

        // 3. check that the file extension of the selected file conforms with the file extension for the database SMD definition file type
        if (valid) {
            inputFile = (document.getElementById('inputFile')).files[0];
            if(inputFile && inputFile.name) {
                let fileNameParts = inputFile.name.split(".");
                let fileExtension = fileNameParts[fileNameParts.length - 1];
                let validFileExtension = "";

                switch ($scope.dbSmdFileType) {
                    case "dldbsmd_json":
                        validFileExtension = "json";
                        break;
                    case "schemacrawler_ser":
                        validFileExtension = "ser";
                        break;
                }

                if (fileExtension != validFileExtension) {
                    $scope.validation.filetypecheck = false;
                    valid = false;
                }
                else {
                    $scope.validation.filetypecheck = true;
                    valid = true;
                }
            }
            else {
                $scope.validation.filetypecheck = true;
                valid = true;
            }
        }

        if (valid) {
            $scope.reverseEngineer(inputFile);
        }
    }

    /**
     * Description: Executes an database reverse engineering run using the selected database definition file
     * @param inputFile
     */
    $scope.reverseEngineer = function (inputFile) {
        $scope.setSpinnerLoading("sk-loading");
        const formData = new FormData();
        formData.append('file', inputFile);

        let POST_URL = apiHost;
        switch ($scope.dbSmdFileType) {
            case "dldbsmd_json":
                POST_URL += "/reverse-engineer/offline/dldbsmd/json/" + $scope.databaseId;
                break;
            case "schemacrawler_ser":
                POST_URL += "/reverse-engineer/offline/schemacrawler_ser/" + $scope.databaseId;
                break;
            default:
                POST_URL = "none_set";
        }

        const headers = {'Content-Type': undefined};

        if (POST_URL != "none_set") {
            HttpService.call(POST_URL, "POST", formData, headers,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.setSpinnerLoading("");
                            $uibModalInstance.close();
                            notify({
                                message: "Reverse engineering run completed successfully.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.refreshDatabaseDetails();
                        }
                    },
                    {
                        status: -1,
                        callback: function(response){
                            $scope.setSpinnerLoading("");
                            $uibModalInstance.close();
                            let errMsg = "Error when processing offline reverse enginering run from DlDbJson file.\n";
                            errMsg += "Error Number: " + response.data.result_data.errorNumber + "\n";
                            errMsg += "Error Message: " + response.data.result_data.generalErrorMessage;

                            notify({
                                message: errMsg,
                                classes: "alert-warning",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.refreshDatabaseDetails();
                        }
                    }
                ]
            );
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('OfflineRevEngController', OfflineRevEngController);
