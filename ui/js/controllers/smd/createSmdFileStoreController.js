/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for creating a new filestore element
 */
function CreateSmdFileStoreController ($scope, $uibModalInstance, HttpService) {
    $scope.fosClassList = [];
    $scope.selectedFosClass = {};
    $scope.newFilestore = {};

    $scope.filestoreName = "";
    $scope.filestoreShortDesc = "";

    $scope.validation = {
        name: true,
        class: true
    }

    /**
     * Description: initialises the CreateSmdFileStoreController
     */
    $scope.initCreateFileStore = function () {
        const GET_URL = apiHost + "/filestore/class/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.fosClassList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: checks that the dialog fields have been filled in correctly
     */
    $scope.checkValid = function(){

        $scope.validation.name = !!($scope.filestoreName && $scope.filestoreName.length > 0);
        $scope.validation.class = !!($scope.selectedFosClass && $scope.selectedFosClass.classId > 0);

        if($scope.validation.name && $scope.validation.class) {
            $scope.create();
        }
    }

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     */
    $scope.create = function () {

        const newFilestore = {
            objectId: 0,
            objectName: $scope.filestoreName,
            objectShortDesc: $scope.filestoreShortDesc,
            classId: $scope.selectedFosClass.classId
        };

        $scope.createFilestore(newFilestore);    // this function must be defined in the controller that opens the dialog.
        $uibModalInstance.close();

    };

    /**
     * Description: closes the dialog without creating a new File/Object Store
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateSmdFileStoreController', CreateSmdFileStoreController);
