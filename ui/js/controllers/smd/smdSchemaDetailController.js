/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the schema detail view
 */
function SmdSchemaDetailController($scope, $state, $stateParams, $uibModal, notify,
                                     NavigationChainService, DTOptionsBuilder, RbacService, HttpService,
                                     MoClassService, UtilityService, DataSensitivityService) {

    $scope.schemaId = 0;
    $scope.classId = 0;
    $scope.schemaCoreDetails = {};
    $scope.schemaCoreDetailsUndo = {};
    $scope.customForm = {};
    $scope.tableViewCustomForm = {};
    $scope.gridData01 = [];     // main table/view list panel
    $scope.gridData03 = [];     // relationship list panel
    $scope.gridData04 = [];     // reverse engineer run log panel
    $scope.associations = [];   // used for md object assocations panel
    $scope.dlDsTagList = [];

    $scope.coreDetailsDisabled = true;
    $scope.revEngDisabled = true;
    $scope.connectionTestResult = 1;
    $scope.connectionTestErrorMessage = "";

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv', title: 'dlSmdDatabaseDetails'}
        ]);

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv', title: 'dlReverseEngineeringLog'}
        ])
        .withOption('order', [[0, 'desc']]);

    $scope.perms = {create: false, update: false, delete: false, publish: false, configure: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialising function for the controller. Read details of the database from DL database
     */
    $scope.initSmdSchemaDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');
        $scope.perms.configure = !RbacService.checkRbac('penv','configure');

        $scope.spinnerLoading = "sk-loading";

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.schemaId = $scope.invocationParams.objectId;
        $scope.classId = $scope.invocationParams.classId;

        $scope.refreshSchemaDetails();
    }

    /**
     * Description: invokes the DL API endpoint /smd/database/schema/{smdSchemaId} to get details of the SMD database
     *  schema definition identified by $scope.schemaId
     */
    $scope.refreshSchemaDetails = function() {
        const GET_URL = apiHost + "/smd/database/schema/" + $scope.schemaId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        // 1. process core details of the schema
                        $scope.schemaCoreDetails = response.data.result_data.schema_summary;
                        $scope.gridData01 = response.data.result_data.table_view_list;
                        $scope.gridData03 = response.data.result_data.relationship_list;
                        /*$scope.associations = response.data.result_data.associations;*/
                        $scope.spinnerLoading = "";
                        /*$scope.disableCustomFormFields();*/

                        // 2. process data sensitivity tags
                        // NB: the full list of assigned data sensitivity tags (aggregate and specific multi) is returned in response.data.result_data.coreDetails.
                        if ($scope.schemaCoreDetails.dlDsTagList.length == 0) {
                            let newDsTag = {
                                "cssClass": DataSensitivityService.getCssClassForLevel("none"),
                                "tagName": "None"
                            }
                            $scope.dlDsTagList.push(newDsTag);
                        }
                        else {
                            for (let i = 0; i < $scope.schemaCoreDetails.dlDsTagList.length; i++) {
                                let dsTag = $scope.schemaCoreDetails.dlDsTagList[i]

                                let newDsTag = {
                                    "cssClass": DataSensitivityService.getCssClassLvlCmode(dsTag.dlDsLevel, dsTag.createMode),
                                    "tagName": dsTag.dlDsName,
                                    "tagId": dsTag.dlDsTagId,
                                    "createMode": dsTag.createMode
                                }
                                $scope.dlDsTagList.push(newDsTag);
                            }
                        }

                    }
                }
            ]
        );
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.schemaCoreDetails, $scope.schemaCoreDetailsUndo)
            $scope.coreDetailsDisabled = false;
        }
        else if (dlIboxToolsName === "reveng") {
            //angular.copy($scope.databaseCoreDetails, $scope.databaseCoreDetailsUndo)
            $scope.revEngDisabled = false;
        }
        else if (dlIboxToolsName === "custom_form") {
            // TODO: copy current state form values for rollback on cancel
            $scope.enableCustomFormFields();
        }

    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            $scope.saveCoreDetails();
            $scope.coreDetailsDisabled = true;
        }
        else if (dlIboxToolsName === "reveng") {
            $scope.saveConnectionDetails();
            $scope.revEngDisabled = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            // save new custom form values
            $scope.disableCustomFormFields();
            $scope.saveCustomFormData();
        }

    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.schemaCoreDetailsUndo, $scope.schemaCoreDetails);
            $scope.coreDetailsDisabled = true;
        }
        else if (dlIboxToolsName === "reveng") {
            // reset all fields to the pre-edit state
            //angular.copy($scope.databaseCoreDetailsUndo, $scope.databaseCoreDetails);
            $scope.revEngDisabled = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            $scope.disableCustomFormFields();
        }
    }

    /**
     * Description: transitions to the smd.reconcile_reveng_run state to allow the user to reconcile
     *  the most recent reverse engineering run for the currently selected SMD database
     */
    $scope.reconcileRevEngRun = function() {
        const currentStateSpec = {
            stateName:"smd.smd_database_detail",
            objectId: $scope.schemaId,
            classId: $scope.classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: $scope.stateParams
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName: "smd.reconcile_db_reveng_run",
            objectId: $scope.schemaId,
            classId: $scope.classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: {}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});

    }

    /**
     * Stores current state in the NavigationChainService and transitions to the smd.table_view_detail state
     * for entity identified by objectId and classId
     * @param objectId
     * @param classId
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {

        let validMode = false;

        const moClass = MoClassService.getMoClassByClassId(classId);
        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "dsReport":
                validMode = true;
                stateName = "smd.smd_database_ds_report";
                stateParams = {
                    objectId: objectId,
                    className: MoClassService.getMoClassByClassId(classId).className,
                    classCode: MoClassService.getMoClassByClassId(classId).classCode,
                    objectContext: $scope.schemaCoreDetails.schemaName
                }
                break;
            case "tableViewDetail":
                validMode = true;
                stateName = moClass.objectDetailState;
                break;
            case "relationshipDetail":
                validMode = true;
                stateName = moClass.objectDetailState;
                break;
        }

        if (validMode) {
            const currentStateSpec = {
                stateName:"smd.schema_detail",
                objectId: $scope.schemaId,
                classId: $scope.classId,
                parentObjectId: 0,
                parentClassId: 0,
                stateParams: $scope.stateParams
            };

            NavigationChainService.storeCurrentState(currentStateSpec);

            const invocationParams = {
                stateName: stateName,
                objectId: objectId,
                classId: classId,
                parentObjectId: $scope.schemaId,
                parentClassId: $scope.classId,
                stateParams: stateParams
            };

            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
        else {
            notify({
                message: "Internal error. An invalid transition mode was specified.",
                classes: "alert-danger",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
    }


    /*
     * Saves core details of the database to the DL database
     */
    $scope.saveCoreDetails = function() {

        const POST_URL = apiHost + "/smd/database/schema/summary";

        HttpService.call(POST_URL, "POST", $scope.schemaCoreDetails, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Core details successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "update failed, duplicate entry. Make sure that the logical database name is unique",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves custom form data to the DL database
     */
    $scope.saveCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.invocationParams.objectId,
            classId: $scope.invocationParams.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    $scope.saveConnectionDetails = function() {
        const POST_URL = apiHost + "/smd/database/connection";

        HttpService.call(POST_URL, "POST", $scope.schemaCoreDetails, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Connection details successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * refreshes the table view list for the database identified by $scope.schemaId
     */
    $scope.refreshTableViewList = function() {
        const GET_URL = apiHost + "/smd/database/tableview/list/" + $scope.schemaId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.gridData01 = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Opens the modal dialog for changing the password for the database user credentials that are used to
     * reverse engineer a database
     */
    $scope.initiateRevengDbUserPassword = function() {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/smd_database_set_password.html'),
            controller: 'SmdSetPasswordController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Save the password for the database user credentials that are used to reverse engineer a database
     */
    $scope.saveRevengDbUserPassword = function(password) {
        const dbCredentials = {
            databaseId:$scope.schemaId,
            hostname:"",
            dbName:"",
            dbPort:"",
            username:"",
            password:password,
            jdbcDriverFamily:"",
            sqlError:"",
            connectionTestResult:false
        };

        const POST_URL = apiHost + "/smd/database/password";

        HttpService.call(POST_URL, "POST", dbCredentials, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Password successfully reset for database reverse engineering user.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Tests connecting to an SMD Database using the stored credentials
     */
    $scope.testConnection = function() {
        const GET_URL = apiHost + "/reverse-engineer/testconnection/" + $scope.schemaId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 0,
                    callback: function(response){
                        const dbConnection = response.data.result_data;
                        let msg = "FAILED to connect to the database using the credentials provided.\n";
                        msg += dbConnection.sqlError;
                        notify({
                            message: msg,
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: 1,
                    callback: function(response){
                        const dbConnection = response.data.result_data;
                        $scope.connectionTestResult = dbConnection.connectionTestResultAsInteger;
                        notify({
                            message: "Connection test was successful.",
                            classes: "success-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Executes a reverse engineer for the database identified by $scope.schemaId using the SchemaCrawler library.
     * Only databases supported by SchemaCrawler are supported by this function
     */
    $scope.runReverseEngineer = function() {
        $scope.spinnerLoading = "sk-loading";
        const POST_URL = apiHost + "/reverse-engineer/online/dldbsmd/" + $scope.schemaId;

        HttpService.call(POST_URL, "POST", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.spinnerLoading = "";
                        notify({
                            message: "Reverse engineering run completed but requires reconciliation.",
                            classes: "success-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.refreshSchemaDetails();
                    }
                },
                {
                    status: 2,
                    callback: function(response) {
                        $scope.spinnerLoading = "";
                        notify({
                            message: "Reverse engineering run completed successfully.",
                            classes: "success-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.refreshSchemaDetails();
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        $scope.spinnerLoading = "";
                        notify({
                            message: "Unable to connect to the database.\nUpdate the connection credentials and verify using the Test Connection button.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -2,
                    callback: function(response){
                        $scope.spinnerLoading = "";
                        let msg = "Error message: " + response.data.result_data.generalErrorMessage +
                            "\nError number: " + response.data.result_data.errorNumber;
                        notify({
                            message: msg,
                            classes: "alert-warning",
                            duration: 5000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.refreshSchemaDetails();
                    }
                }
            ]
        );
    }

    /**
     * Description: Opens the modal dialog that lets the user execute an Offline Reverse Engineering run by selecting
     *  a *.ser file.
     */
    $scope.openOfflineDatabaseDialog = function(){
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/smd_offline_rev_eng.html'),
            controller: 'OfflineRevEngController',
            size: 'lg',
            scope: $scope

        });
    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     *  TODO: this code needs to be standardised in a service
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form for tables/views <b>ALL</b> existing custom form data for tables/views will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/smd/database/tableview/customform/";
        const postValues = {
            "objectId": $scope.schemaId,
            "classId": $scope.schemaCoreDetails.classId,
            "customFormId": selectedCustomForm.customFormId
        };
        $scope.tableViewCustomForm = selectedCustomForm;

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form selection successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Opens the modal dialog that lets the user edit the table/view short description directly from the cell
     */
    $scope.openInCellEditDialog = function(objectId, classId, objectShortDesc){
        let objectDetails = {
            objectShortDesc: objectShortDesc,
            objectId: objectId,
            classId: classId
        };

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_short_desc_update.html'),
            controller: 'CmnShortDescUpdateController',
            size: 'lg',
            resolve: { objectDetails: function () { return objectDetails; }},
            scope: $scope
        });
    }

    /**
     * Description: Saves the short description for a table/view to the DL database. The short description text is entered
     *  via the cmn_short_desc_update.html modal dialog. This function is called from the CmnShortDescUpdateController
     * @param objectDetails
     */
    $scope.saveShortDescription = function(objectDetails) {
        let postValues = {};
        let POST_URL = apiHost + "/smd/database";
        let postUrlPathEnd = "";

        switch(objectDetails.classId) {
            case MoClassService.getMoClassIdByCode('RDBMS_TABLE'):
            case MoClassService.getMoClassIdByCode('RDBMS_VIEW'):
                postUrlPathEnd = "/tableview/shortdesc";
                postValues.tableViewId = objectDetails.objectId;
                postValues.classId = objectDetails.classId;
                postValues.tableViewShortDesc = objectDetails.objectShortDesc;
                break;
            case MoClassService.getMoClassIdByCode('RDBMS_FOREIGN_KEY'):
                postUrlPathEnd = "/relationship/summary";
                postValues.relationshipId = objectDetails.objectId;
                postValues.classId = objectDetails.classId;
                postValues.relationshipShortDesc = objectDetails.objectShortDesc;
                break;
        }
        POST_URL = POST_URL + postUrlPathEnd;

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.updateShortDescDisplay(objectDetails);
                        notify({
                            message: "Short description saved successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the value of the tableViewShortDesc for the tableViewDefinition object in gridData01 so that
     *  it is displayed in the Tabel/View grid after the user saves the changes in the modal dialog
     * @param objectDetails
     */
    $scope.updateShortDescDisplay = function(objectDetails) {
        switch(objectDetails.classId) {
            case MoClassService.getMoClassIdByCode('RDBMS_TABLE'):
            case MoClassService.getMoClassIdByCode('RDBMS_VIEW'):
                for (let i = 0; i < $scope.gridData01.length; i++) {
                    if ($scope.gridData01[i].tableViewId == objectDetails.objectId) {
                        $scope.gridData01[i].tableViewShortDesc = objectDetails.objectShortDesc;
                        break;
                    }
                }
                break;
            case MoClassService.getMoClassIdByCode('RDBMS_FOREIGN_KEY'):
                for (let i = 0; i < $scope.gridData03.length; i++) {
                    if ($scope.gridData03[i].relationshipId == objectDetails.objectId) {
                        $scope.gridData03[i].relationshipShortDesc = objectDetails.objectShortDesc;
                        break;
                    }
                }
                break;
        }
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: deletes the current Structural Metadata database definition
     */
    $scope.deleteMdObject = function () {

        const DELETE_URL = apiHost + "/smd/database/" + $scope.schemaId;

        HttpService.call(DELETE_URL,"DELETE",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Database successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: opens the Data Sensitivity Multiple Assignment dialog
     */
    $scope.openDSassignMultiDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_assign_data_sensitivity_multi.html'),
            controller: 'CmnAssignDataSensitivityMultiController',
            size: 'md',
            resolve: { currentDsTagList: function () { return $scope.schemaCoreDetails.dlDsTagList; }},
            scope: $scope
        });

    }

    /**
     * Description: Save the list of Data Sensitivity Tags for specific Multi data sensitivity.
     * @param selectedDsTags
     */
    $scope.saveDataSensitivityMulti = function(selectedDsTags) {
        let dlDsMultiUpdate = {
            objectId: $scope.schemaId,
            classId: $scope.classId,
            dlDsTagList: selectedDsTags
        };

        const POST_URL = apiHost + "/common/datasensitivity/multi";

        HttpService.call(POST_URL, "POST", dlDsMultiUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.updateDlDsTagList(response.data.result_data.dlDsTagList);
                        $scope.spinnerLoading = "";
                        notify({
                            message: "Data Sensitivity assignment saved successfully.",
                            classes: "success-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: Processes the data sensititivity tag list returned by the DL API after updating the
     *  specific multi data sensitivity assignment for this database definition md object.
     * @param dlDsTagList
     */
    $scope.updateDlDsTagList = function(dlDsTagList) {
        $scope.schemaCoreDetails.dlDsTagList = dlDsTagList;
        $scope.dlDsTagList = [];
        if (dlDsTagList.length == 0) {
            let newDsTag = {
                "cssClass": DataSensitivityService.getCssClassForLevel("none"),
                "tagName": "None"
            }
            $scope.dlDsTagList.push(newDsTag);
        }
        else {
            for (let i = 0; i < dlDsTagList.length; i++) {
                let dsTag = dlDsTagList[i];

                let newDsTag = {
                    "cssClass": DataSensitivityService.getCssClassForLevel(dsTag.dlDsLevel),
                    "tagName": dsTag.dlDsName,
                    "tagId": dsTag.dlDsTagId,
                    "createMode": dsTag.createMode
                }
                $scope.dlDsTagList.push(newDsTag);
            }
        }

    }


}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdSchemaDetailController', SmdSchemaDetailController);
