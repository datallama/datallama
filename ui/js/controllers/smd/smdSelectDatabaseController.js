/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for selecting an SMD database to be associated with a data model diagram
 */
function SmdSelectDatabaseController ($scope, $rootScope, $uibModalInstance, HttpService, DTOptionsBuilder) {

    $scope.smdDatabaseList = [];

    $scope.dialogSelectedSmdDatabase = {
        databaseId: 0,
        databaseName: ""
    }

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    /**
     * Description: get the list of diagrams from the DB
     */
    $scope.initSmdSelectDatabase = function () {
        const GET_URL = apiHost + "/smd/database/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.smdDatabaseList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: set the diagramId and diagramName for diagram selected in table
     */
    $scope.setSelectedDatabase = function (smdDatabaseId, smdDatabaseName) {
        $scope.dialogSelectedSmdDatabase.databaseId = smdDatabaseId;
        $scope.dialogSelectedSmdDatabase.databaseName = smdDatabaseName;
    }

    /**
     * Description: invokes the loadDiagram function in parent scope passing the selected diagramId as parameter
     *  TODO: the database Id and database name should be returned to function that opened dialog
     */
    $scope.ok = function () {
        $scope.currentDiagram.properties.smdDatabaseId = $scope.dialogSelectedSmdDatabase.databaseId;
        $scope.currentDiagram.properties.smdDatabaseName = $scope.dialogSelectedSmdDatabase.databaseName;
        $scope.$parent.dirtyCanvas = true;
        $uibModalInstance.close();
        $scope.getAllEntities();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdSelectDatabaseController', SmdSelectDatabaseController);
