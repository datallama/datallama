/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The controller for the UIB modal dialogue where you can load a JSON file
 *              and turn it into a JSON message format in the DL data base
 */
function HierarchyRevEngSelectOfflineFileController ($uibModalInstance, $scope, HttpService, notify, MoClassService) {

    $scope.valid = true;
    $scope.spinnerLoading = "";
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.disableButtons = false;

    /**
     * Description: toggles the Loading Spinner state and related buttons
     * @param value
     */
    $scope.setSpinnerLoading = function(value) {
        $scope.spinnerLoading = value;
        $scope.disableButtons = (value !== "");
    }

    /**
     * Description: Check that the file selected by the user is valid.
     */
    $scope.checkValid = function() {
        $scope.valid = false;

        if(document.getElementById('inputFile') && (document.getElementById('inputFile')).files[0]) {
            const inputFile = (document.getElementById('inputFile')).files[0];

            if(inputFile && inputFile.name && inputFile.name.split(".").length === 2 && inputFile.name.split(".")[1] === "json") {
                $scope.valid = true;
                $scope.load(inputFile);
            }
        }
    }

    /**
     * Description: initiates the reverse engineering for a JSON file selected by the user.
     * @param inputFile
     */
    $scope.load = function (inputFile) {
        $scope.setSpinnerLoading("sk-loading");
        const formData = new FormData();
        formData.append('file', inputFile);

        let postUrlPath = "";
        switch ($scope.invocationParams.parentClassId) {
            // TODO: add postUrlPath for FILE_STORE reverse engineer
            case MoClassService.getMoClassIdByCode('OBJECT_STORE_SPACE'):
                postUrlPath = '/smd/fos/reverse-engineer/objectstore/offline/dlossmd/json/';
                break;
            case MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PROJECT'):
                postUrlPath = '/smd/dataintegration/reverse-engineer/offline/dldismd/json/';
                break;
        }

        const POST_URL = apiHost + postUrlPath + $scope.invocationParams.parentObjectId;
        const headers = {'Content-Type': undefined};

        HttpService.call(POST_URL, "POST", formData, headers,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.postRevEngProcessing(1)
                    }
                },
                {
                    status: 2,
                    callback: function(response) {
                        $scope.postRevEngProcessing(2)
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        $scope.reverseEngineerRunFailure(response);
                    }
                },
                {
                    status: -2,
                    callback: function(response) {
                        $scope.reverseEngineerRunFailure(response);
                    }
                }
            ]
        );
    };

    /**
     * Description: display all JSON nodes after a successful reverse engineering run.
     */
    $scope.postRevEngProcessing = function(resultStatus) {
        $uibModalInstance.close();
        $scope.setSpinnerLoading("");

        let msg = "";
        switch (resultStatus) {
            case 1:
                msg = "The reverse engineer run completed. RECONCILIATION is required for one or more deleted elements.";
                break;
            case 2:
                msg = "The reverse engineer run completed. No reconciliation is required.";
                break;
            default:
                msg = "Indeterminate result status for DL API call. Ask the administrator to check the DL API logs.";
                break;
        }

        notify({
            message: msg,
            classes: "alert-success",
            duration: 5000,
            templateUrl: $scope.notifyTemplate
        });

        // refresh the reveng run history. This function is defined in smdHierarchyRevEngController
        $scope.readRevEngRunHistory();
    }

    /**
     * Description: Notify the user the the reverse engineering run failed.
     * @param response
     */
    $scope.reverseEngineerRunFailure = function(response) {
        console.log(response);
        $uibModalInstance.close();
        $scope.setSpinnerLoading("");

        let msg = "";
        let alertLevel = "alert-warning";

        switch (response.data.result_status) {
            case -1:
                msg = "Error: " + response.data.result_data.generalErrorMessage + ". Error number: " + response.data.result_data.errorNumber;
                alertLevel = "alert-danger";
                break;
            case -2:
                msg = "Warning: The reverse engineer run could not be started because the previous run requires reconciliation or is in an indeterminate state. ";
                msg = msg + "Please reconcile or override the previous reverse engineer run."
                alertLevel = "alert-warning";
                break;
            default:
                msg = "An unknown error occurred. Please refer to the DL API logs."
                alertLevel = "alert-danger";
                break;
        }


        notify({
            message: msg,
            classes: alertLevel,
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: cancels the offline reverse engineer action by user action
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('HierarchyRevEngSelectOfflineFileController', HierarchyRevEngSelectOfflineFileController);
