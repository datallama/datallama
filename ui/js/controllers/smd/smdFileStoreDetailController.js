/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Filestore Detail view.
 *  The structure of $scope.selectedDirectory is:
 *  {filestoreId: 0, directoryId: 0, classId: 0, directoryParentId: 0, directoryLevel: 0, directoryLevelSequence: 0, directoryGUID: "",
 *   directoryHierarchyNumber: "", directoryName: "", directoryDesc: "", directoryFullPathName: ""}
 */
function SmdFileStoreDetailController($scope, $rootScope, $state, $stateParams, $window, $document, $anchorScroll, $uibModal, notify,
                                      NavigationChainService, RbacService, HttpService, DTOptionsBuilder, DTColumnBuilder,
                                      MoClassService, UtilityService, DataSensitivityService) {
    $scope.filestoreDirectories = [];   // this array holds all the directories returned from the database
    $scope.filestoreDirectoriesTableView = [];      // this array is used to display on the table view of the filestore

    $scope.filestoreId = 0;      // the filestoreId is specified in the page URL are retrieved with $stateParams
    $scope.objectId = 0;
    $scope.filestore_summary = {};
    $scope.conCredConfig = {};
    $scope.parentClass = {};
    $scope.selectedDirectory = null;
    $scope.selectedDirectoryDisplay = null;
    $scope.previousResultSetSequence = 0;
    $scope.selectedDirectoryAssociations = [];
    $scope.newDirectory = {};
    $scope.newDirectoryId = 0;
    $scope.returnId = 0;
    $scope.newDirectoryConfig = {topLevelDirectory: true};

    $scope.dlDsTagList = [];
    $scope.dlDsOverview = [];

    $scope.customForm = {};

    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveDirectory = {moveMode: false, moveTargetDirectory: null};

    $scope.tedEditingDisabled = true;
    $scope.cfrmEditingDisabled = true;
    $scope.tedActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Filestore Directories'}
        ]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notVisible(),
        DTColumnBuilder.newColumn(1).notVisible(),
        DTColumnBuilder.newColumn(2).notVisible(),
        DTColumnBuilder.newColumn(3).notSortable()
    ];

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.spinnerLoading = "";

    // When calling deleteMdObject() are we deleting the filestore (or just one of the directories)?
    $scope.deleteFilestore = false;

    /**
     * Description: initialisation function for the controller
     */
    $scope.initFilestoreDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;

        $scope.parentClass = MoClassService.getMoClassByClassId($scope.invocationParams.parentClassId);
        $scope.resetSelectedDirectoryDisplay();
        $scope.filestoreId = $scope.invocationParams.parentObjectId;
        $scope.tedActiveTab = $scope.invocationParams.stateParams.tedActiveTab;

        $scope.readFilestoreDirectories();
    }

    /**
     * Description: Open a dialog to select a custom form for the filestore
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *              delete the custom form data in the filestore directories
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Massive Database Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form data for <b>ALL</b> directories of this filestore will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        let customFormSelectionUpdate = {
            objectId: $scope.invocationParams.parentObjectId,
            classId: $scope.invocationParams.parentClassId,
            customFormId: selectedCustomForm.customFormId
        };

        const POST_URL = apiHost + "/filestore/customform/";

        HttpService.call(POST_URL, "POST", customFormSelectionUpdate, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        notify({
                            message: "Custom form selection successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.resetSelectedDirectoryDisplay();
                        $scope.readFilestoreDirectories();
                    }
                }
            ]
        );
    }

    /**
     * Description: reads from the DL database all directories for the filestore identified by $scope.filestoreId
     */
    $scope.readFilestoreDirectories = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/filestore/content/' + $scope.filestoreId + "/" + $scope.config.abbreviation;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.processFilestoreDirectoriesResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: processes Filestore Directories in response from the DL database
     */
    $scope.processFilestoreDirectoriesResponse = function(response) {
        $scope.spinnerLoading = "";

        $scope.filestore_summary.filestoreId = response.data.result_data.filestore_summary.objectId;
        $scope.filestore_summary.objectId = response.data.result_data.filestore_summary.objectId;
        $scope.filestore_summary.abbreviationSchemeId = response.data.result_data.filestore_summary.abbreviationSchemeId;
        $scope.filestore_summary.classId = response.data.result_data.filestore_summary.classId;
        $scope.filestore_summary.objectName = response.data.result_data.filestore_summary.objectName;
        $scope.filestore_summary.className = response.data.result_data.filestore_summary.className;
        $scope.filestore_summary.objectShortDesc = response.data.result_data.filestore_summary.objectShortDesc;
        $scope.filestore_summary.objectGUID = response.data.result_data.filestore_summary.objectGUID;

        $scope.customForm.customFormId = response.data.result_data.filestore_summary.customFormId;
        $scope.customForm.customFormName = response.data.result_data.filestore_summary.customFormName;
        $scope.customForm.schemaDefinition = response.data.result_data.filestore_summary.schemaDefinition;
        $scope.customForm.formDefinition = response.data.result_data.filestore_summary.formDefinition;
        $scope.disableCustomFormFields();
        try {
            $scope.customForm.schemaDefinition = JSON.parse($scope.customForm.schemaDefinition);
            $scope.customForm.formDefinition = JSON.parse($scope.customForm.formDefinition);
            $scope.disableCustomFormFields();
        } catch (e) {
            console.log("Warning! The custom form definition could not be parsed properly")
        }
        $scope.customForm.formData = {};

        $scope.conCredConfig = response.data.result_data.concred_config;

        $scope.filestoreDirectories  = response.data.result_data.filestore_directories;
        angular.copy($scope.filestoreDirectories, $scope.filestoreDirectoriesTableView);
        $scope.refreshSelectedDirectory();
    }

    /**
     * Description: refreshes the selected directory status if directoryId is specified in a navigation transition
     */
    $scope.refreshSelectedDirectory = function() {
        const refreshDirectoryId = $scope.invocationParams.objectId;
        if(refreshDirectoryId > 0) {
            for (let i = 0; i < $scope.filestoreDirectories.length; i++ ) {
                if ($scope.filestoreDirectories[i].directoryId === refreshDirectoryId) {
                    $scope.selectedDirectory = $scope.filestoreDirectories[i];
                    break;
                }
            }
            $scope.loadSelectedDirectory($scope.invocationParams.objectId);
        }
    }

    /**
     * Description: Save the list of Data Sensitivity Tags for specific Multi data sensitivity.
     * @param selectedDsTags
     */
    $scope.saveDataSensitivityMulti = function(selectedDsTags) {
        let dlDsMultiUpdate = {
            objectId: $scope.selectedDirectory.objectId,
            classId: $scope.selectedDirectory.classId,
            dlDsTagList: selectedDsTags
        };

        const POST_URL = apiHost + "/common/datasensitivity/multi";

        HttpService.call(POST_URL, "POST", dlDsMultiUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.updateDlDsTagList(response.data.result_data.dlDsTagList);
                        $scope.spinnerLoading = "";
                        notify({
                            message: "Data Sensitivity assignment saved successfully.",
                            classes: "success-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: Processes the data sensititivity tag list returned by the DL API after updating the
     *  specific multi data sensitivity assignment for this database definition md object.
     * @param dlDsTagList
     */
    $scope.updateDlDsTagList = function(dlDsTagList) {
        $scope.selectedDirectory.dlDsTagList = dlDsTagList;
        $scope.selectedDirectoryDisplay.dlDsTagList = dlDsTagList;
        $scope.dlDsOverview = [];
        if (dlDsTagList.length == 0) {
            let newDsTag = {
                "cssClass": DataSensitivityService.getCssClassForLevel("none"),
                "tagName": "None"
            }
            $scope.dlDsOverview.push(newDsTag);
        }
        else {
            for (let i = 0; i < dlDsTagList.length; i++) {
                let dsTag = dlDsTagList[i];

                let newDsTag = {
                    "cssClass": DataSensitivityService.getCssClassForLevel(dsTag.dlDsLevel),
                    "tagName": dsTag.dlDsName,
                    "tagId": dsTag.dlDsTagId,
                    "createMode": dsTag.createMode
                }
                $scope.dlDsOverview.push(newDsTag);
            }
        }
    }

    /**
     * Description: returns the CSS class name for the provided Data Sensitivity Level
     */
    $scope.getCssClassForLevel = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: Show or hide the comment entry panel.
     */
    $scope.toggleCommentPanel = function(){
        if($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0) {
            notify({
                message: "You have not selected a directory. Select a directory before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden
        }
    }

    $scope.dlIboxParentEdit = function(dlIboxName) {
        if (dlIboxName === "ted") {
            if ($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0) {
                const errorMessage = "You have not selected an directory. No edits can be made.";
                notify({ message: errorMessage,
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.tedEditingDisabled = false;
            }
        }
        else if (dlIboxName === "cfrm") {
            if ($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0) {
                const errorMessage = "You have not selected an directory. No edits can be made.";
                notify({ message: errorMessage,
                    classes: "alert-danger",
                    duration: 30000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.enableCustomFormFields();
                $scope.cfrmEditingDisabled = false;
            }
        }
    }

    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            $scope.saveDirectoryCoreDetails();
        }
        else if (dlIboxName === "cfrm") {
            $scope.cfrmEditingDisabled = true;
            $scope.saveDirectoryCustomFormData();
        }
    }

    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.tedEditingDisabled = true;
            // reset all fields to the pre-edit state
            //angular.copy($scope.filestoreDirectoriesstoreCoreDetailsUndo, $scope.filestoreDirectoriesstoreDetails.core_details);
        }
        else if (dlIboxName === "cfrm") {
            $scope.disableCustomFormFields();
            $scope.cfrmEditingDisabled = true;
        }

    }


     /**
      * Description: resets all elements in $scope.selectedDirectoryDisplay to default values
      */
    $scope.resetSelectedDirectoryDisplay = function() {
        $scope.selectedDirectoryDisplay = {
            directoryId: 0,
            classId: 0,
            directoryParentId: 0,
            directoryLevel: 0,
            directoryLevelSequence: 0,
            objectGUID: "Nothing selected in the hierarchy or table view.",
            directoryHierarchyNumber: "Nothing selected in the hierarchy or table view.",
            directoryName: "Nothing selected in the hierarchy or table view.",
            directoryDesc: "Nothing selected in the hierarchy or table view.",
            directoryFullPathName: "Nothing selected in the hierarchy or table view.",
            directoryVisible: 0,
            initialExpandLevel: 0,
            indentCss: "",
            expanded: false,
            parent: false,
            resultSetSequence: 0,
            directorySelected: 0,
            mdContext: []
        }
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    $scope.moveLastToTheBeginning = function () {
        const a = $scope.filestoreDirectories.pop();
        $scope.filestoreDirectories.splice(0,0, a);
    };

    /**
     * Description: opens the New <MO Class> modal dialog with default field values.
     */
    $scope.initiateNewDirectory = function() {
        $scope.newDirectoryConfig.topLevelDirectory = ($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0);
        $scope.newDirectory = {name: "", desc: "", class: {}};

        // determine the class of the new metadata object to be added
        if ($scope.parentClass.classCode == 'FILE_STORE') {
            $scope.newDirectory.class = MoClassService.getMoClassByCode('FILE_STORE_DIR');
        }
        else if ($scope.parentClass.classCode == 'OBJECT_STORE_SPACE') {
            if ($scope.selectedDirectory == null) {
                $scope.newDirectory.class = MoClassService.getMoClassByCode('OBJECT_STORE_CONTAINER');
            }
            else {
                $scope.newDirectory.class = MoClassService.getMoClassByCode('OBJECT_STORE_DIRECTORY');
            }
        }

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_filestore_directory.html'),
            controller: 'CreateSmdFileStoreDirectoryController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: collapses the whole filestore down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
            if ($scope.filestoreDirectories[i].directoryLevel > 0) {
                $scope.filestoreDirectories[i].directoryVisible = 0;
            }
            $scope.filestoreDirectories[i].expanded = false;
        }
    };

    /**
     * Description: makes visible the single directory identified by directoryId
     */
    $scope.displayNewDirectory = function (directoryId) {
        let searchDirectoryId = directoryId;
        let iterationCount = 1;

        while (searchDirectoryId > 0 && iterationCount < 100) {
            for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
                if($scope.filestoreDirectories[i].directoryId === searchDirectoryId) {
                    if ($scope.filestoreDirectories[i].directoryVisible === 1) {
                        searchDirectoryId = 0;
                    }
                    else {
                        $scope.filestoreDirectories[i].directoryVisible = 1;
                        searchDirectoryId =  $scope.filestoreDirectories[i].directoryParentId;
                    }
                    break;
                }
            }
            iterationCount++;
        }
    }

    /**
     * Description: expands every directory in the filestore
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
            $scope.filestoreDirectories[i].directoryVisible = 1;
            $scope.filestoreDirectories[i].expanded = true;
        }
    };

    /**
     * Description: expands the filestore to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
            if ($scope.filestoreDirectories[i].directoryLevel < $scope.config.visibilityLevel - 1) {
                $scope.filestoreDirectories[i].directoryVisible = 1;
                $scope.filestoreDirectories[i].expanded = true;
            }
            else if ($scope.filestoreDirectories[i].directoryLevel === $scope.config.visibilityLevel - 1) {
                $scope.filestoreDirectories[i].directoryVisible = 1;
                $scope.filestoreDirectories[i].expanded = false;
            }
            else {
                $scope.filestoreDirectories[i].directoryVisible = 0;
                $scope.filestoreDirectories[i].expanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an directory.
     * @param directory
     */
    $scope.toggleDirectoryExpansion = function(directory) {
        const setVisibility = (directory.expanded) ? 0 : 1;
        const ehnMatch = directory.directoryHierarchyNumber + ".";

        if (directory.expanded) { // collapse all levels below this directory
            for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
                if ($scope.filestoreDirectories[i].directoryHierarchyNumber.startsWith(ehnMatch)
                    && $scope.filestoreDirectories[i].directoryLevel > directory.directoryLevel){
                    $scope.filestoreDirectories[i].directoryVisible = 0;
                    $scope.filestoreDirectories[i].expanded = false;
                }
                if ($scope.filestoreDirectories[i].directoryId === directory.directoryId)
                    $scope.filestoreDirectories[i].expanded = false;
            }
        }
        else {  // expand all levels below this directory
            for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
                if ($scope.filestoreDirectories[i].directoryHierarchyNumber.startsWith(ehnMatch)
                    && $scope.filestoreDirectories[i].directoryLevel === directory.directoryLevel + 1){
                    $scope.filestoreDirectories[i].directoryVisible = 1;
                }
                if ($scope.filestoreDirectories[i].directoryId === directory.directoryId)
                    $scope.filestoreDirectories[i].expanded = true;
            }
        }
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.filestoreDirectoriesTableView = [];
        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
                if ($scope.filestoreDirectories[i].parent === false) {
                    endnodes.push($scope.filestoreDirectories[i]);
                }
            }
            angular.copy(endnodes, $scope.filestoreDirectoriesTableView);
        }
        else {
            angular.copy($scope.filestoreDirectories, $scope.filestoreDirectoriesTableView);
        }
    }

    /**
     * Description: saves a new filestore directory to the DL database and splices it into the $scope.filestoreDirectories array
     */
    $scope.saveNewDirectory = function() {
        $scope.newDirectoryId = 0;
        $scope.newDirectoryParent = {parentDirectoryId: 0, parentResultSetSequence: 0}

        if ($scope.newDirectoryConfig.topLevelDirectory === true) {
            $scope.newDirectoryParent.parentDirectoryId = 0;
            $scope.newDirectoryParent.parentResultSetSequence = $scope.filestoreDirectories.length;
        }
        else {
            $scope.newDirectoryParent.parentDirectoryId = $scope.selectedDirectory.directoryId;
            $scope.newDirectoryParent.parentResultSetSequence = $scope.selectedDirectory.resultSetSequence;
        }

        const newDirectory = {
            directoryId: $scope.newDirectoryId,
            classId: $scope.newDirectory.class.classId,
            filestoreId: $scope.filestoreId,
            parentClassId: $scope.parentClass.classId,
            directoryParentId: $scope.newDirectoryParent.parentDirectoryId,
            directoryName: $scope.newDirectory.name,
            directoryDesc: $scope.newDirectory.desc,
            customFormData: null
        };

        const POST_URL = apiHost + "/filestore/directory";

        HttpService.call(POST_URL, "POST", newDirectory, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.processNewDirectoryResponse(response);
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        let sqlError = response.data.sql_error;
                        let msg = "Error: " + sqlError.sqlErrorMessage + " SQL Error State: " + sqlError.sqlErrorState + ". SQL Error Code: " + sqlError.sqlErrorCode;
                        notify.config({duration: 5000});
                        notify({ message: msg,
                            classes: "alert-danger",
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

     /**
      * Description: processes the response from the DL API after a new FOS element has been created
      * @param response
      */
    $scope.processNewDirectoryResponse = function(response) {
        let savedDirectory = response.data.result_data;
        savedDirectory.directoryVisible = 1;

        // splice the new directory into the $scope.filestoreDirectories array at the end of the level_sequence based on parent directory
        let i = 0;
        let newResultSetSequence = 0;
        if ($scope.newDirectoryConfig.topLevelDirectory === false) {
            newResultSetSequence = $scope.newDirectoryParent.parentResultSetSequence + 1;
            for (i = newResultSetSequence; i < $scope.filestoreDirectories.length; i++ ) {
                if ($scope.filestoreDirectories[i].directoryLevel > $scope.selectedDirectory.directoryLevel) {
                    newResultSetSequence++;
                }
                else {
                    break;
                }
            }
            $scope.filestoreDirectories.splice(newResultSetSequence, 0, savedDirectory);

            // update all resultSetSequence values after saved new directory
            for (i = newResultSetSequence; i < $scope.filestoreDirectories.length; i++) {
                $scope.filestoreDirectories[i].resultSetSequence = i;
            }
        }
        else {
            newResultSetSequence = $scope.newDirectoryParent.parentResultSetSequence;
            savedDirectory.resultSetSequence = $scope.filestoreDirectories.length;
            $scope.filestoreDirectories.push(savedDirectory);
        }

        notify({ message: "New filestore directory saved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate});

    }

    /**
     * Description: saves the core details of an updated directory to the DL database
     */
    $scope.saveDirectoryCoreDetails = function() {
        // update the name in selected directory
        $scope.filestoreDirectories[$scope.selectedDirectory.resultSetSequence].directoryName = $scope.selectedDirectoryDisplay.directoryName;

        $scope.coreDetailsDisabled = true;

        const updatedDirectory = {
            objectId: $scope.selectedDirectoryDisplay.directoryId,
            parentObjectId: $scope.filestoreId,
            elementParentObjectId: $scope.selectedDirectoryDisplay.directoryParentId,
            objectGUID: $scope.selectedDirectoryDisplay.objectGUID,
            objectName: $scope.selectedDirectoryDisplay.directoryName,
            objectShortDesc: $scope.selectedDirectoryDisplay.directoryDesc
        };

        // make the REST POST call to save the data store summary info to the database
        const POST_URL = apiHost + "/filestore/directory";

        HttpService.call(POST_URL, "POST", updatedDirectory, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        // notify successful update
                        notify.config({duration: 2000});
                        notify({ message: "Updates to filestore directory saved.",
                            classes: "alert-success",
                            templateUrl: $scope.notifyTemplate});
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data for select directory
     */
    $scope.saveDirectoryCustomFormData = function() {
        let customFormData = {
            objectId: $scope.selectedDirectory.directoryId,
            classId: $scope.selectedDirectory.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        };

        // make the REST POST call to save the data store summary info to the database
        const POST_URL = apiHost + "/filestore/directory/customformdata";

        HttpService.call(POST_URL, "POST", customFormData, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.disableCustomFormFields();
                        // notify successful update
                        notify.config({duration: 2000});
                        notify({ message: "Updates to filestore directory saved.",
                            classes: "alert-success",
                            templateUrl: $scope.notifyTemplate});
                    }
                }
            ]
        );

    }

     /**
      * Description: updates the MO class when a user clicks the 'Add as top level hierarchy element' checkbox
      */
    $scope.updateTopLevelClass = function() {

        if ($scope.parentClass.classCode == 'OBJECT_STORE_SPACE') {
            if ($scope.newDirectoryConfig.topLevelDirectory) {
                $scope.newDirectory.class = MoClassService.getMoClassByCode('OBJECT_STORE_CONTAINER');
            } else {
                $scope.newDirectory.class = MoClassService.getMoClassByCode('OBJECT_STORE_DIRECTORY');
            }
        }

    }

    /**
     * Description: reads all comments for the selected directory
     */
    $scope.readDirectoryComments = function() {
        if ($scope.selectedDirectory != null) {
            const GET_URL = apiHost + '/comment/' + $scope.selectedDirectory.directoryId + "/" + $scope.selectedDirectory.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response) {
                            $scope.processDirectoryCommentsResponse(response);
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: processes directory comments returned from DL database
     * @param response
     */
    $scope.processDirectoryCommentsResponse = function(response) {
        $scope.comment.commentList = response.data.result_data;
    }

    /**
     * Description: saves a new comment for the currently selected directory
     */
    $scope.saveComment = function() {
        if($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0) {
            notify({ message: "You have not selected a directory. Select a directory before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate});
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedDirectory.directoryId,
                classId: $scope.selectedDirectory.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL, "POST", newComment, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            $scope.comment.newCommentText = "";
                            $scope.readDirectoryComments();
                        }
                    }
                ]
            );

        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a filestore directory when it is clicked in the hierarchy view
     * @param selectedDirectory
     * TODO: need to find a way to shift focus to Attributes tab
     */
    $scope.showDirectoryDetails = function(selectedDirectory) {
        $scope.disableCustomFormFields();

        // NOTE: an element must be selected before Move Mode can be enabled, so this function assumes $scope.selectedDirectory will never be null
        if ($scope.moveDirectory.moveMode) {
            if (selectedDirectory.directoryHierarchyNumber.startsWith($scope.selectedDirectory.directoryHierarchyNumber)) {
                notify({ message: "You cannot move an directory to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.moveDirectory.moveTargetDirectory = selectedDirectory;
                $scope.setActiveTab(5);
            }
        }
        else {
            $scope.filestoreDirectories[$scope.previousResultSetSequence].directorySelected = 0;

            $scope.selectedDirectory = $scope.filestoreDirectories[selectedDirectory.resultSetSequence];
            $scope.filestoreDirectories[$scope.selectedDirectory.resultSetSequence].directorySelected = 1;
            $scope.previousResultSetSequence = $scope.selectedDirectory.resultSetSequence;

            $scope.tedActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
            $scope.resetTedEditing();
            $window.scrollTo(0, 0); // scroll to top so that directory details form is visible

            // get the details of the selected FOS element
            const GET_URL = apiHost + '/filestore/directory/' + $scope.selectedDirectory.directoryId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            $scope.processDirectoryDetailsResponse(response);
                        }
                    }
                ]
            );
        }

    }

    /**
     * Description: process the details returned from the DL database for the selected directory
     * @param response
     */
    $scope.processDirectoryDetailsResponse = function(response) {
        // 1. copy directory info to scope vars
        $scope.selectedDirectoryDisplay = response.data.result_data.attributes;
        $scope.selectedDirectoryDisplay.directoryHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedDirectoryDisplay.directoryHierarchyNumber);
        $scope.customForm.formData = JSON.parse($scope.selectedDirectoryDisplay.customFormData);
        $scope.selectedDirectoryAssociations = response.data.result_data.associations;
        $scope.comment.commentList = response.data.result_data.comments;

        // 2. get Data Sensitivity overview
        $scope.dlDsOverview = [];
        if ($scope.selectedDirectoryDisplay.dlDsOverview.length == 0 && $scope.selectedDirectoryDisplay.dlDsTagList.length == 0) {
            let newDsTag = {
                "cssClass": DataSensitivityService.getCssClassForLevel("none"),
                "tagName": "None",
                "tagId": 0,
                "createMode": 0
            }
            $scope.dlDsOverview.push(newDsTag);
        } else {
            for (let i = 0; i < $scope.selectedDirectoryDisplay.dlDsOverview.length; i++) {
                let dsTag = $scope.selectedDirectoryDisplay.dlDsOverview[i];

                let newDsTag = {
                    "cssClass": DataSensitivityService.getCssClassLvlCmode(dsTag.dlDsLevel, dsTag.createMode),
                    "tagName": dsTag.dlDsName,
                    "tagId": dsTag.dlDsTagId,
                    "createMode": dsTag.createMode
                }
                $scope.dlDsOverview.push(newDsTag);
            }
        }
    }

    /**
     * Description: shows details of the md-context node in a modal dialog
     * TODO: this implementation needs to be completed
     * @param contextNode
     */
    $scope.showMdContextNodeDetails = function(contextNode) {
        console.log(contextNode);
    }

    /**
     * Description: loads the directory details for filestore directory identified by param directoryId
     * @param directoryId
     */
    $scope.loadSelectedDirectory = function(directoryId) {
        // TODO: this code can be rationalised with the same code in showDirectoryDetails
        const GET_URL = apiHost + '/filestore/directory/' + directoryId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.selectedDirectoryDisplay = response.data.result_data.attributes;
                        $scope.selectedDirectory = response.data.result_data.attributes;
                        $scope.selectedDirectoryAssociations = response.data.result_data.associations;
                        $scope.comment.commentList = response.data.result_data.comments;
                        for(let i = 0; i < $scope.filestoreDirectories.length; i++){
                            if($scope.filestoreDirectories[i].directoryId === directoryId){
                                $scope.filestoreDirectories[i].directorySelected = 1;
                                $scope.previousResultSetSequence = $scope.filestoreDirectories[i].resultSetSequence;
                                break;
                            }
                        }


                    }
                }
            ]
        );

    }

    /**
     * Description: searches all directories using keyword
     */
    $scope.executeSearch = function() {
        // first, clear the previous search
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.filestoreDirectories[$scope.search.matchList[i].resultSetSequence].directoryMatched = false;
        }
        $scope.search.matchList = [];

        // expand all directories so matches can be displayed
        $scope.expandAll();

        const regexp = new RegExp($scope.search.keyword, "gi");
        for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
            const result = $scope.filestoreDirectories[i].directoryName.match(regexp);
            if (result != null) {
                const matchedDirectory = {
                    directoryId: $scope.filestoreDirectories[i].directoryId,
                    resultSetSequence: $scope.filestoreDirectories[i].resultSetSequence,
                    directoryName: $scope.filestoreDirectories[i].directoryName
                }

                $scope.search.matchList.push(matchedDirectory);
                $scope.filestoreDirectories[i].directoryMatched = true;
            }
        }
        notify.config({duration: 2000});
        const message = "The Search returned " + $scope.search.matchList.length + " matches.";
        notify({
            message: message,
            classes: "alert-success",
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: clears all search results and collapes the hierarchy to 3 levels
     */
    $scope.clearSearch = function() {
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.filestoreDirectories[$scope.search.matchList[i].resultSetSequence].directoryMatched = false;
        }
        $scope.search.matchList = [];
        $scope.search.currentViewIndex = null;
        $scope.search.keyword = "";

        $scope.config.visibilityLevel = 3;
        $scope.expandToLevel();
    }

    /**
     * Description: scrolls through the search results, up or down, bringing each directory into focus
     */
    $scope.scrollSearchResults = function(direction) {
        if ($scope.search.matchList.length > 0){
            if (direction === 'down') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = 0;
                else if ($scope.search.currentViewIndex === $scope.search.matchList.length - 1)
                    $scope.search.currentViewIndex = 0;
                else
                    $scope.search.currentViewIndex++;
            }
            else if (direction === 'up') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else if ($scope.search.currentViewIndex === 0)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else
                    $scope.search.currentViewIndex--;
            }
            $location.hash($scope.search.matchList[$scope.search.currentViewIndex].directoryId);
            $anchorScroll();
        }
    }

    /**
     * Description: toggles the abbreviation setting between 0 (off) and 1 (on)
     * TODO: eventually the user will be able to select abbreviation schemes from this view, which is why it isn't a boolean
     */
    $scope.toggleAbbreviation = function() {
        $scope.config.abbreviation = ($scope.config.abbreviation === 1) ? 0 : 1;
        $scope.readFilestoreDirectories();
    }

    /**
     * Description: sets "move directory mode" so that the user can select the new parent directory
     */
    $scope.toggleMoveMode =  function() {
        if($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0){
            notify({
                message: "You have not selected an directory.",
                classes: "alert-danger",
                duration: 30000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else{
            $scope.moveDirectory.moveMode = !$scope.moveDirectory.moveMode;
            $scope.moveDirectory.moveTargetDirectory = null;
        }
    }

    /**
     * Description: moves the initial selected directory to the selected target parent directory.
     *  It's complicated!
     */
    $scope.executeMove = function() {
        const moveDirectoryDefinition = {
            directoryId: $scope.selectedDirectory.directoryId,
            newParentDirectoryId: $scope.moveDirectory.moveTargetDirectory.directoryId
        };

        const POST_URL = apiHost + "/filestore/directory/move";

        HttpService.call(POST_URL, "POST", moveDirectoryDefinition, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processMoveDirectoryResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: post-processing after the move directory action has responded
     */
    $scope.processMoveDirectoryResponse = function() {
        notify({ message: "Filestore directory successfully moved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate});
        $scope.moveDirectory.moveMode = false;
        $scope.moveDirectory.moveTargetDirectory = null;
        $scope.readFilestoreDirectories();

    }

    /**
     * No longer used?
     */
    $scope.flattenFilestore = function() {
        $scope.flattenedFilestore = [];

        for(let i = 0; i < $scope.filestoreDirectories.length; i++) {
            const newFlattenedDirectory = {
                directoryId: $scope.filestoreDirectories[i].directoryId,
                directoryFullPathName: $scope.filestoreDirectories[i].directoryFullPathName,
                childrenCount: $scope.filestoreDirectories[i].children.length
            }
            $scope.flattenedFilestore.push(newFlattenedDirectory);

            if($scope.filestoreDirectories[i].children.length > 0) {
                $scope.recurseFlattenFilestore($scope.filestoreDirectories[i].children);
            }
        }
    }

    $scope.recurseFlattenFilestore = function(directoryChildren) {
        for(let j = 0; j < directoryChildren.length; j++) {
            const newFlattenedDirectory = {
                directoryId: directoryChildren[j].directoryId,
                directoryFullPathName: directoryChildren[j].directoryFullPathName,
                childrenCount: directoryChildren[j].children.length
            }
            $scope.flattenedFilestore.push(newFlattenedDirectory);

            if(directoryChildren[j].children.length > 0) {
                $scope.recurseFlattenFilestore(directoryChildren[j].children);
            }
        }
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.tedActiveTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
                break;
            case 2:
                $scope.tedActiveTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "more":false, "activeIndex":2};
                break;
            case 3:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "more":false, "activeIndex":3};
                break;
            case 4:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "more":false, "activeIndex":4};
                break;
            case 5:
                $scope.tedActiveTab = {"attributes":false, "associations":false, "comments":false, "customform":false, "more":true, "activeIndex":5};
                break;
        }
    }

    $scope.resetTedEditing = function () {
        $scope.tedEditingDisabled = true;
    }

    /**
     * Description: Clears the SpinnerLoading overlay
     */
    $scope.clearSpinnerLoading = function () {
        $scope.spinnerLoading = "";
    };

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (deleteFilestore) {
        $scope.deleteFilestore = deleteFilestore;
        if(!$scope.deleteFilestore && ($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0)) {
            const errorMessage = "You have not selected an directory.";
            notify({ message: errorMessage,
                classes: "alert-danger",
                duration: 30000,
                templateUrl: $scope.notifyTemplate});
        }
        else{
            $uibModal.open({
                templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
                controller: 'ConfirmDeleteController',
                size: 'sm',
                scope: $scope
            });
        }
    }

    /**
     * Description: Delete the selected File/Object
     */
    $scope.deleteMdObject = function () {
        if ($scope.deleteFilestore) {
            const DELETE_URL = apiHost + "/filestore/" + $scope.filestoreId;
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            notify({
                                message: "Filestore was deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.goBack();
                        }
                    }
                ]
            );

        }
        else {
            const DELETE_URL = apiHost + "/filestore/directory/" + $scope.selectedDirectory.directoryId;
            HttpService.call(DELETE_URL, "DELETE", null, null,
                [
                    {
                        status:1,
                        callback: function(response) {
                            notify({
                                message: "File store directory was deleted.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.moveDirectory.moveMode = false;
                            $scope.moveDirectory.moveTargetDirectory = null;
                            $scope.readFilestoreDirectories();
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

     $scope.checkElementSelected = function(silent) {
         if ($scope.selectedDirectory == null || $scope.selectedDirectory.directoryId <= 0) {
             if(silent !== true){
                 notify({
                     message: "No directory selected",
                     classes: "alert-danger",
                     duration: 30000,
                     templateUrl: $scope.notifyTemplate
                 });
             }
             return false;
         }
         return true;
     }

     /**
      * Description: opens the Data Sensitivity Multiple Assignment dialog
      */
     $scope.openDSassignMultiDialog = function() {
         $uibModal.open({
             templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_assign_data_sensitivity_multi.html'),
             controller: 'CmnAssignDataSensitivityMultiController',
             size: 'md',
             resolve: { currentDsTagList: function () { return $scope.selectedDirectoryDisplay.dlDsOverview; }},
             scope: $scope
         });

     }

     /**
      * Description: transitions to a new AngularJS using the DL NavigationChainService
      * @param objectId
      * @param classId
      * @param parentObjectId
      * @param parentClassId
      * @param mode
      */
     $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {
/*
         if(mode !== "default" && mode !== "fullAssoc" && !$scope.checkElementSelected()){
             return;
         }
*/

         const currentStateSpec = {
             stateName: $scope.invocationParams.stateName,
             objectId: $scope.selectedDirectoryDisplay.directoryId,
             classId: $scope.selectedDirectoryDisplay.classId,
             parentObjectId: $scope.invocationParams.parentObjectId,
             parentClassId: $scope.invocationParams.parentClassId,
             stateParams:{
                 tedActiveTab: $scope.tedActiveTab
             }
         };

         NavigationChainService.storeCurrentState(currentStateSpec);

         let stateName = "";
         let stateParams = {};
         switch (mode) {
             case "assocDetails":
                 classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                 stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                 break;
             case "lineage":
                 stateName = "associations.lineage";
                 break;
             case "fullAssoc":
                 stateName = "associations.smd_hierarchy_fullview";
                 stateParams = {
                     objectClass: "Filestore Directory",
                     objectContext: $scope.selectedDirectoryDisplay.directoryFullPathName
                 }
                 break;
             case "newAssoc":
                 stateName = "associations.new_association";
                 stateParams = {
                     objectClass: "Filestore Directory",
                     objectContext: $scope.selectedDirectoryDisplay.directoryFullPathName
                 };
                 break;
             case "reveng":
                 stateName = "smd.hierarchy_reveng";
                 let selectedConnector = {
                     connectorId: $scope.conCredConfig.connectorId,
                     connectorName: $scope.conCredConfig.connectorName,
                     connectorShortDesc: $scope.conCredConfig.connectorShortDesc
                 };
                 let selectedCredential = {
                     credentialId: $scope.conCredConfig.credentialId,
                     credentialName: $scope.conCredConfig.credentialName,
                     credentialShortDesc: $scope.conCredConfig.credentialShortDesc
                 };
                 
                 stateParams = {
                     objectId: $scope.filestoreId,
                     objectName: $scope.filestore_summary.objectName,
                     classId: $scope.parentClass.classId,
                     className: $scope.parentClass.className,
                     selectedConnector: selectedConnector,
                     selectedCredential: selectedCredential,
                     revengConfig: {online: true, offline: false}
                 };
                 break;
             default:
                 stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                 break;
         }

         $state.go(stateName,
             {
                 invocationParams:{
                     stateName:stateName,
                     objectId:objectId,
                     classId:classId,
                     parentObjectId: parentObjectId,
                     parentClassId: parentClassId,
                     stateParams: stateParams
                 }
             }
         );
     }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('smdFileStoreDetailController', SmdFileStoreDetailController);


