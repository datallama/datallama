/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main database detail view
 */
function SmdTableViewDetailController($scope, $state, $stateParams, $uibModal, notify, NavigationChainService,
                                      DTOptionsBuilder, HttpService, MoClassService, RbacService, UtilityService,
                                      DataSensitivityService) {

    $scope.objectId = 0;
    $scope.tableViewCoreDetails = {};
    $scope.tableViewCoreDetailsUndo = {};
    $scope.customForm = {};
    $scope.customFormUndo = {};
    $scope.columnCustomForm = {};
    $scope.gridData01 = [];     // used for the column list panel
    $scope.associations = [];   // used for md object assocations panel
    $scope.dlDsTagList = [];

    $scope.coreDetailsDisabled = true;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'}
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialising function for the controller. Read details of the database from DL database
     */
    $scope.initSmdTableViewDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');

        $scope.invocationParams = $stateParams.invocationParams;

        $scope.objectId = $scope.invocationParams.objectId;
        $scope.refreshTableViewDetails($scope.invocationParams.objectId);
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.tableViewCoreDetails, $scope.tableViewCoreDetailsUndo);
            $scope.coreDetailsDisabled = false;
        }
        else if (dlIboxToolsName === "custom_form") {
            angular.copy($scope.customForm, $scope.customFormUndo);
            $scope.enableCustomFormFields();
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            $scope.coreDetailsDisabled = true;
            $scope.saveCoreDetails();
        }
        else if (dlIboxToolsName === "custom_form") {
            $scope.disableCustomFormFields();
            $scope.saveCustomFormData();
        }
    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.tableViewCoreDetailsUndo, $scope.tableViewCoreDetails);
            $scope.coreDetailsDisabled = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            angular.copy($scope.customFormUndo, $scope.customForm);
            $scope.disableCustomFormFields();
        }
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /*
     * Description: Saves core details of the database to the DL database
     */
    $scope.saveCoreDetails = function() {
        const postValues = {
            "tableViewId": $scope.tableViewCoreDetails.tableViewId,
            "tableViewShortDesc": $scope.tableViewCoreDetails.tableViewShortDesc
        };
        const POST_URL = apiHost + "/smd/database/tableview/summary";

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Table/View information saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves custom form data to the DL database
     */
    $scope.saveCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.invocationParams.objectId,
            classId: $scope.invocationParams.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: get the details of the table/view identified by $scope.tableViewId from the DL database,
     *  including the list of columns
     */
    $scope.refreshTableViewDetails = function(tableViewId) {
        const GET_URL = apiHost + "/smd/database/tableview/" + tableViewId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        let core_details = response.data.result_data.core_details;

                        $scope.tableViewCoreDetails.tableViewId = core_details.tableViewId;
                        $scope.tableViewCoreDetails.tableViewName = core_details.tableViewName;
                        $scope.tableViewCoreDetails.classId = core_details.classId;
                        $scope.tableViewCoreDetails.schemaId = core_details.schemaId;
                        $scope.tableViewCoreDetails.className = core_details.className;
                        $scope.tableViewCoreDetails.schemaName = core_details.schemaName;
                        $scope.tableViewCoreDetails.tableViewShortDesc = core_details.tableViewShortDesc;
                        $scope.tableViewCoreDetails.objectGUID = core_details.objectGUID;
                        $scope.tableViewCoreDetails.dlDsTagList = core_details.dlDsTagList;
                        $scope.tableViewCoreDetails.mdContext = core_details.mdContext;
                        $scope.customForm.customFormId = core_details.customForm.customFormId;
                        $scope.customForm.customFormName = core_details.customForm.customFormName;
                        $scope.customForm.schemaDefinition = core_details.customForm.schemaDefinitionJson;
                        $scope.customForm.formDefinition = JSON.parse(core_details.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse(core_details.customForm.formData);
                        $scope.columnCustomForm.customFormId = core_details.columnCustomFormId;
                        $scope.columnCustomForm.customFormName = core_details.columnCustomFormName;
                        $scope.gridData01 = core_details.columnList;

                        // process data sensitivity tags
                        if (core_details.dlDsTagList.length == 0) {
                            let newDsTag = {
                                "cssClass": DataSensitivityService.getCssClassForLevel("none"),
                                "tagName": "None"
                            }
                            $scope.dlDsTagList.push(newDsTag);
                        }
                        else {
                            for (let i = 0; i < core_details.dlDsTagList.length; i++) {
                                let dsTag = core_details.dlDsTagList[i]

                                let newDsTag = {
                                    "cssClass": DataSensitivityService.getCssClassLvlCmode(dsTag.dlDsLevel, dsTag.createMode),
                                    "tagName": dsTag.dlDsName,
                                    "tagId": dsTag.dlDsTagId,
                                    "createMode": dsTag.createMode
                                }
                                $scope.dlDsTagList.push(newDsTag);
                            }
                        }

                        $scope.associations = response.data.result_data.associations;
                        $scope.disableCustomFormFields();
                    }
                }
            ]
        );
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Opens the modal dialog that lets the user edit the table/view short description directly from the cell
     */
    $scope.openInCellEditDialog = function(objectId, classId, objectShortDesc){
        let objectDetails = {
            objectShortDesc: objectShortDesc,
            objectId: objectId,
            classId: classId
        };

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_short_desc_update.html'),
            controller: 'CmnShortDescUpdateController',
            size: 'lg',
            resolve: { objectDetails: function () { return objectDetails; }},
            scope: $scope
        });
    }

    /**
     * Description: Saves the short description for a table/view to the DL database. The short description text is entered
     *  via the cmn_short_desc_update.html modal dialog. This function is called from the CmnShortDescUpdateController
     * @param objectDetails
     */
    $scope.saveShortDescription = function(objectDetails) {
        const postValues = {
            "columnId": objectDetails.objectId,
            "columnShortDesc": objectDetails.objectShortDesc
        };
        const POST_URL = apiHost + "/smd/database/column/summary";

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.updateShortDescDisplay(objectDetails);
                        notify({
                            message: "Short description for column saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the value of the tableViewShortDesc for the tableViewDefinition object in gridData01 so that
     *  it is displayed in the Tabel/View grid after the user saves the changes in the modal dialog
     * @param objectDetails
     */
    $scope.updateShortDescDisplay = function(objectDetails) {
        for (let i = 0; i < $scope.gridData01.length; i++) {
            if ($scope.gridData01[i].columnId == objectDetails.objectId) {
                $scope.gridData01[i].columnShortDesc = objectDetails.objectShortDesc;
                break;
            }
        }
    }

    /**
     * Description: Save the list of Data Sensitivity Tags for specific Multi data sensitivity.
     * @param selectedDsTags
     */
    $scope.saveDataSensitivityMulti = function(selectedDsTags) {
        let dlDsMultiUpdate = {
            objectId: $scope.tableViewCoreDetails.tableViewId,
            classId: $scope.tableViewCoreDetails.classId,
            dlDsTagList: selectedDsTags
        };

        const POST_URL = apiHost + "/common/datasensitivity/multi";

        HttpService.call(POST_URL, "POST", dlDsMultiUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.updateDlDsTagList(response.data.result_data.dlDsTagList);
                        $scope.spinnerLoading = "";
                        notify({
                            message: "Data Sensitivity assignment saved successfully.",
                            classes: "success-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: Processes the data sensititivity tag list returned by the DL API after updating the
     *  specific multi data sensitivity assignment for this database definition md object.
     * @param dlDsTagList
     */
    $scope.updateDlDsTagList = function(dlDsTagList) {
        $scope.tableViewCoreDetails.dlDsTagList = dlDsTagList;
        $scope.dlDsTagList = [];
        if (dlDsTagList.length == 0) {
            let newDsTag = {
                "cssClass": DataSensitivityService.getCssClassForLevel("none"),
                "tagName": "None"
            }
            $scope.dlDsTagList.push(newDsTag);
        }
        else {
            for (let i = 0; i < dlDsTagList.length; i++) {
                let dsTag = dlDsTagList[i];

                let newDsTag = {
                    "cssClass": DataSensitivityService.getCssClassForLevel(dsTag.dlDsLevel),
                    "tagName": dsTag.dlDsName,
                    "tagId": dsTag.dlDsTagId,
                    "createMode": dsTag.createMode
                }
                $scope.dlDsTagList.push(newDsTag);
            }
        }

    }

    /**
     * Description: returns the CSS class name for the provided Data Sensitivity Level
     */
    $scope.getCssClassForLevel = function(dataSensitivityLevel) {
        return DataSensitivityService.getCssClassForLevel(dataSensitivityLevel);
    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     *  TODO: this code needs to be standardised in
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form attributes for this table/view and all columns " +
                "that inherit the custom form definition will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Call back from select custom form dialog. Saves the selected custom form as the Column Custom Form.
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/smd/database/column/customform/";
        const postValues = {
            "objectId": $scope.objectId,
            "classId": $scope.invocationParams.classId,
            "customFormId": selectedCustomForm.customFormId
        };

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.columnCustomForm = selectedCustomForm;
                        notify({
                            message: "Custom form association successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the Data Sensitivity Multiple Assignment dialog
     */
    $scope.openDSassignMultiDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_assign_data_sensitivity_multi.html'),
            controller: 'CmnAssignDataSensitivityMultiController',
            size: 'md',
            resolve: { currentDsTagList: function () { return $scope.tableViewCoreDetails.dlDsTagList; }},
            scope: $scope
        });

    }


    /**
     * Description: click through to Column Detail view
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        NavigationChainService.storeCurrentState($scope.invocationParams);
        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:$scope.tableViewCoreDetails.className,
                    objectContext:$scope.tableViewCoreDetails.tableViewName
                }
                break;
            case "fullAssoc":
                stateName = "associations.smd_tableview_fullview";
                stateParams = {
                    objectName: $scope.tableViewCoreDetails.tableViewName
                }
                break;
            case "compare":
                stateName = "smd.compare_tables";
                stateParams = {
                    sourceTableViewDetails: $scope.tableViewCoreDetails,
                    sourceTableViewColumns: $scope.gridData01
                }
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdTableViewDetailController', SmdTableViewDetailController);
