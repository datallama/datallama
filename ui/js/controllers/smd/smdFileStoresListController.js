/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Filestore List view
 */
function SmdFileStoresListController($http, $scope, $rootScope, $state, $stateParams, $uibModal, notify,
                                     HttpService, NavigationChainService, DTOptionsBuilder, MoClassService,
                                     DetailsClickThroughService, UtilityService) {
    $scope.gridData = [];
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},
        ]);

    /**
     * Description: initialisation function for the controller
     */
    $scope.initFilestoresList = function() {
        $scope.readGridData();
    };

    /**
     * Description: reads the filestore list to populate the grid view
     */
    $scope.readGridData = function() {
        const GET_URL = apiHost + '/filestore/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processGridDataResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description:
     * @param response
     */
    $scope.processGridDataResponse = function(response) {
        $scope.gridData = response.data.result_data;
    }

    /**
     * Description: Opens the Create Filestore modal dialog box
     */
    $scope.openCreateFilestoreDialog = function() {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_filestore.html'),
            controller: 'CreateSmdFileStoreController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: write a new filestore to the database based on details entered in the modal dialog
     * @param newFilestore
     */
    $scope.createFilestore = function(newFilestore) {
        console.log(newFilestore);
        const POST_URL = apiHost + "/filestore/summary";

        HttpService.call(POST_URL, "POST", newFilestore, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({ message: "New filestore saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate});
                        $scope.readGridData();
                    }
                }
            ]
        );
    }

    /**
     * Description: click through to filestore detail
     */
    $scope.clickThrough = function (objectId, classId, mode) {

        const currentState = {
            stateName: "smd.smd_filestore_list",
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: {}
        }

        NavigationChainService.storeCurrentState(currentState);

        if (mode === 'detail') {
            const invocationParams = {
                stateName: "smd.smd_filestore_detail",
                objectId: 0,
                classId: 0,
                parentObjectId: objectId,
                parentClassId: classId,
                stateParams: {directoryId:0, tedActiveTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
            };
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
        else if (mode === 'summary') {
            DetailsClickThroughService.clickThrough(objectId, classId);
        }
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdFileStoresListController', SmdFileStoresListController);

