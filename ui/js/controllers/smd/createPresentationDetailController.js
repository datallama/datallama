/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
function CreatePresentationDetailController ($scope, $uibModalInstance) {
    $scope.newElementSpec = {
        name: "",
        shortdesc: "",
        topLevelElement: $scope.newElementConfig.topLevelElement
    };

    $scope.validation = {
        name: true
    }

    /**
     * Saves the new element in the DL database by invoking $scope.saveNewElement which is defined in the parent controller
     */
    $scope.create = function () {
        $scope.saveNewElement($scope.newElementSpec);    // this function is in the controller that invoked the modal dialog
        $uibModalInstance.close();
    };

    /**
     * Check that all mandatory fields have been populated.
     */
    $scope.checkValid = function(){
        $scope.validation.name = !!($scope.newElementSpec.name && $scope.newElementSpec.name.length > 0);


        if($scope.validation.name){
            $scope.create();
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreatePresentationDetailController', CreatePresentationDetailController);
