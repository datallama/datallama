/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Credential List view
 */
function CredentialListController($scope, $rootScope, $location, $state, $stateParams, NavigationChainService, $uibModal,
                                  notify, DTOptionsBuilder, HttpService, RbacService, UtilityService) {

    $scope.credentialList = [];


    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialising function for the controller. Read the credential list from DL database
     */
    $scope.initCredentialList = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');

        $scope.invocationParams = $stateParams.invocationParams;

        $scope.refreshCredentialList();
    }

    /**
     * Description: read details of the column from the DL DB, including associations and full descriptions
     * @param columnId
     */
    $scope.refreshCredentialList = function() {
        const GET_URL = apiHost + "/smd/credential/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.credentialList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the Create FOS Credential modal dialog box which is used to define a new file or object store credential
     */
    $scope.openCreateCredentialDialog = function() {
        $scope.clickThrough(0, 0, 0, 0, "create");

        /*$uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_credential.html'),
            controller: 'CreateCredentialController',
            size: 'md',
            scope: $scope
        });
        */
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: transition to a new state via the NavigationChainService
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        let currentState = {
            stateName:'smd.credential_list',
            objectId:0,
            classId:0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: {}
        }
        NavigationChainService.storeCurrentState(currentState);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "detail":
                stateName = 'smd.credential_detail';
                break;
            case "create":
                stateName = 'smd.credential_create';
                break;
            default:
                stateName = 'smd.credential_list';
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: return to the state from which this state was invoked
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CredentialListController', CredentialListController);
