/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function CreatePresentationController ($scope, $rootScope, $uibModalInstance, HttpService, MoClassService) {
    $scope.presentationClassList = [];
    $scope.selectedPresentationClass = {};

    $scope.presentation = {
        name: "",
        shortDesc: ""
    };

    $scope.validation = {
        name: true,
        class: true
    }

    /**
     * Description: initialises the CreatePresentationController
     */
    $scope.initCreatePresentation = function () {
        const GET_URL = apiHost + "/smd/presentation/class/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.presentationClassList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: checks that the dialog fields have been filled in correctly
     */
    $scope.checkValid = function(){

        $scope.validation.name = !!($scope.presentation.name && $scope.presentation.name.length > 0);
        $scope.validation.class = !!($scope.selectedPresentationClass && $scope.selectedPresentationClass.classId > 0);

        if($scope.validation.name && $scope.validation.class){
            $scope.create();
        }
    }

    /**
     * Description: saves a new Data Presentation Definition to the DL DB via the DL API
     */
    $scope.create = function () {

        const newObject = {
            objectId: -1,
            objectName: $scope.presentation.name,
            objectShortDesc: $scope.presentation.shortDesc,
            classId: $scope.selectedPresentationClass.classId 
        };

        $scope.createPresentation(newObject);    // this function must be defined in the controller that opens the dialog.
        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreatePresentationController', CreatePresentationController);
