/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: controller for the create_rest_element.html modal dialog
 */
function CreateDataIntegrationElementController ($scope, $uibModalInstance) {
    $scope.newElementSpec = {
        name: "",
        type: "",      // $scope.newElementConfig is defined in the parent Controller
        shortDesc: "",
        topLevelElement: $scope.newElementConfig.topLevelElement
    };

    $scope.validation = {
        name: true,
        type: true
    }

    $scope.initCreateDataIntegrationElement = function() {
        if ($scope.newElementConfig.elementType == "job")
            $scope.newElementSpec.type = "job";
    }

    /**
     * Check that all mandatory fields have been populated.
     */
    $scope.checkValid = function() {
        $scope.validation.name = true;
        $scope.validation.type = true;

        if ($scope.newElementSpec.name == null || $scope.newElementSpec.name.length == 0)
            $scope.validation.name = false;

        if ($scope.newElementSpec.type == null || $scope.newElementSpec.type.length == 0)
            $scope.validation.type = false;

        if($scope.validation.name && $scope.validation.type) {
            $scope.create();
        }
    }

    /**
     * Saves the new element in the DL database by invoking $scope.saveNewElement which is defined in the parent controller
     */
    $scope.create = function () {
        $scope.saveNewElement($scope.newElementSpec);    // this function is in the controller that invoked the modal dialog
        $uibModalInstance.close();
    };

    /**
     * Description: cancels from the form
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    /**
     * Description: resets
     * @param elementType
     */
    $scope.setState = function(elementType) {
        $scope.newElementSpec.name = "";
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateDataIntegrationElementController', CreateDataIntegrationElementController);
