/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Credential List view
 */
function CredentialDetailController($scope, $rootScope, $location, $state, $stateParams, NavigationChainService, notify,
                                  HttpService, RbacService, MoClassService) {

    $scope.credentialDetail = {};
    $scope.credentialDetailUndo = {};
    $scope.credentialId = 0;

    $scope.editingDisabled = {summary:true};
    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialising function for the controller. Read the credential list from DL database
     */
    $scope.initCredentialDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.credentialId = $scope.invocationParams.objectId;

        $scope.refreshCredentialDetail();
    }

    /**
     * Description: read details of the column from the DL DB, including associations and full descriptions
     */
    $scope.refreshCredentialDetail = function() {
        const GET_URL = apiHost + "/smd/credential/" + $scope.credentialId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.credentialDetail = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "summary") {
            // capture all field values for undo of edit
            angular.copy($scope.credentialDetail, $scope.credentialDetailUndo)
            $scope.editingDisabled.summary = false;
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "summary") {
            $scope.saveCredentialSummary();
            $scope.editingDisabled.summary = true;
        }
    }

    /**
     * Description: Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "summary") {
            // reset all fields to the pre-edit state
            angular.copy($scope.credentialDetailUndo, $scope.credentialDetail);
            $scope.editingDisabled.summary = true;
        }
    }

    /**
     * Description: saves summary information for a credential (i.e. the short description) in the DL DB
     */
    $scope.saveCredentialSummary = function() {
        let credentialUpdate = {
            credentialId: $scope.credentialId,
            credentialShortDesc: $scope.credentialDetail.credentialShortDesc
        };

        const POST_URL = apiHost + "/smd/credential/summary";

        HttpService.call(POST_URL, "POST", credentialUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Credential updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });

                    }
                }
            ]
        );
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: transitions to a new state via the NavigationChainService
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        let currentState = {
            stateName:'smd.credential_detail',
            objectId:$scope.credentialId,
            classId:0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: {}
        }
        NavigationChainService.storeCurrentState(currentState);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "update":
                stateName = "smd.credential_update";
                break;
            default:
                stateName = "smd.credential_list";
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: return to the state from which this state was invoked
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CredentialDetailController', CredentialDetailController);
