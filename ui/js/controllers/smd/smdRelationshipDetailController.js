/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main database detail view
 */
function SmdRelationshipDetailController($scope, $state, $stateParams, $uibModal, notify, NavigationChainService,
                                      DTOptionsBuilder, HttpService, MoClassService, RbacService, UtilityService) {

    $scope.objectId = 0;
    $scope.classId = 0;
    $scope.relationshipCoreDetails = {};
    $scope.relationshipCoreDetailsUndo = {};
    $scope.columnPairs = [];
    $scope.columnPairsUndo = [];

    $scope.coreDetailsDisabled = true;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialising function for the controller. Read details of the database from DL database
     */
    $scope.initSmdRelationshipDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');

        $scope.invocationParams = $stateParams.invocationParams;

        $scope.objectId = $scope.invocationParams.objectId;
        $scope.classId = $scope.invocationParams.classId;
        $scope.refreshRelationshipDetails($scope.invocationParams.objectId);
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.relationshipCoreDetails, $scope.relationshipCoreDetailsUndo);
            $scope.coreDetailsDisabled = false;
        }
        else if (dlIboxToolsName === "column_pairs") {
            angular.copy($scope.columnPairs, $scope.columnPairsUndo);
            $scope.coreDetailsDisabled = false;
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            $scope.coreDetailsDisabled = true;
            $scope.saveCoreDetails();
        }
        else if (dlIboxToolsName === "column_pairs") {
            $scope.coreDetailsDisabled = true;
        }
    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.relationshipCoreDetailsUndo, $scope.relationshipCoreDetails);
            $scope.coreDetailsDisabled = true;
        }
        else if (dlIboxToolsName === "column_pairs") {
            angular.copy($scope.columnPairsUndo, $scope.columnPairs);
            $scope.disableCustomFormFields();
        }
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /*
     * Description: Saves core details of the database to the DL database
     */
    $scope.saveCoreDetails = function() {
        const postValues = {
            "relationshipId": $scope.objectId,
            "relationshipShortDesc": $scope.relationshipCoreDetails.relationshipShortDesc
        };
        const POST_URL = apiHost + "/smd/database/relationship/summary";
console.log(postValues);
        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Relationship information saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: retrieve from the DL database the details of the SMD Relationship identified by $scope.objectId
     */
    $scope.refreshRelationshipDetails = function(objectId) {
        const GET_URL = apiHost + "/smd/database/relationship/" + objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.relationshipCoreDetails = response.data.result_data;
                        if ($scope.relationshipCoreDetails.createMode == 10)
                            $scope.relationshipCoreDetails.createModeText = "Reverse engineered";
                        else if ($scope.relationshipCoreDetails.createMode == 30)
                            $scope.relationshipCoreDetails.createModeText = "User defined";
                        else
                            $scope.relationshipCoreDetails.createModeText = "Unknown";

                        $scope.columnPairs = response.data.result_data.columnPairs;
                    }
                }
            ]
        );
    }

    /**
     * Description: Opens the modal dialog that lets the user edit the SMD Relationship short description directly from the cell
     */
    $scope.openInCellEditDialog = function(objectId, classId, objectShortDesc){
        let objectDetails = {
            objectShortDesc: objectShortDesc,
            objectId: objectId,
            classId: classId
        };

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_short_desc_update.html'),
            controller: 'CmnShortDescUpdateController',
            size: 'lg',
            resolve: { objectDetails: function () { return objectDetails; }},
            scope: $scope
        });
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: deletes the SMD relationship after the user had confirmed the delete action in the
     *  Confirm Delete dialog
     */
    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/smd/database/relationship/" + $scope.objectId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The relationship was successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                },
                {
                    status: 10100,
                    callback: function(response) {
                        notify({
                            message: "The relationship was reverse engineered. It cannot be deleted.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: 10101,
                    callback: function(response) {
                        notify({
                            message: "The relationship details were not found. It may have been previously deleted.",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: click through to Column Detail view
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        NavigationChainService.storeCurrentState($scope.invocationParams);
        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:$scope.tableViewCoreDetails.className,
                    objectContext:$scope.tableViewCoreDetails.tableViewName
                }
                break;
            case "fullAssoc":
                stateName = "associations.smd_tableview_fullview";
                stateParams = {
                    objectName: $scope.tableViewCoreDetails.tableViewName
                }
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: return to the SMD Database List view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdRelationshipDetailController', SmdRelationshipDetailController);
