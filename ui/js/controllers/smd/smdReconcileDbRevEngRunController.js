/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
function SmdReconcileDbRevEngRunController($scope, $state, $stateParams, $uibModal, notify,
                                         NavigationChainService, DTOptionsBuilder, RbacService, HttpService,
                                         MoClassService) {
    $scope.databaseId = 0;
    $scope.classId = 0;
    $scope.reconcileReport = {schema: {}, tableView: {}, column: {}, relationship: {}};
    $scope.matchSchema = {source: {objectName: "None Selected"}, target: {}};
    $scope.matchTableView = {source: {objectName: "None Selected"}, target: {}};
    $scope.matchColumn = {source: {objectName: "None Selected"}, target: {}};
    $scope.matchRelationship = {source: {objectName: "None Selected"}, target: {}};
    $scope.ctrlConfig = {matchmode: {schema: false, tableview: false, column: false, relationship: false},
                         reconRequired: 0};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('language', {'sEmptyTable': 'No objects deleted in most recent reverse engineering run'} );

    $scope.perms = {create: false, update: false, delete: false, publish: false, configure: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';

    const UNRECONCILED = 0;
    const MATCHING = 1;
    const MATCHED = 2;
    const DELETED = 3;

    /**
     * Description: initialising function for the controller. Read details of the database from DL database
     */
    $scope.initSmdReconcileDbRevEngRun = function () {
        $scope.perms.create = !RbacService.checkRbac('reveng','create');
        $scope.perms.update = !RbacService.checkRbac('reveng','update');
        $scope.perms.delete = !RbacService.checkRbac('reveng','delete');
        $scope.perms.publish = !RbacService.checkRbac('reveng','publish');
        $scope.perms.configure = !RbacService.checkRbac('reveng','configure');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.databaseId = $scope.invocationParams.objectId;
        $scope.classId = $scope.invocationParams.classId;

        $scope.getRevEngReconciliationReport($scope.databaseId);
    }

    /**
     * Description: retrieves from the DL DB the reconciliation report for the most recent reverse engineering run
     * @param databaseId
     */
    $scope.getRevEngReconciliationReport = function (databaseId) {
        const GET_URL = apiHost + "/reverse-engineer/reconcile/" + databaseId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function (response) {
                        $scope.reconcileReport.schema = response.data.result_data.schema;
                        $scope.reconcileReport.tableView = response.data.result_data.tableview;
                        $scope.reconcileReport.column = response.data.result_data.column;
                        $scope.reconcileReport.relationship = response.data.result_data.relationship;
                        $scope.checkReconciliationRequired();
                    }
                }
            ]
        );
    }

    /**
     * Description: determines whether reconciliation actions are required based on the deleted arrays
     */
    $scope.checkReconciliationRequired = function () {
        $scope.ctrlConfig.reconRequired = 0;

        $scope.ctrlConfig.reconRequired += $scope.reconcileReport.schema.deleted.length;
        $scope.ctrlConfig.reconRequired += $scope.reconcileReport.tableView.deleted.length;
        $scope.ctrlConfig.reconRequired += $scope.reconcileReport.column.deleted.length;
        $scope.ctrlConfig.reconRequired += $scope.reconcileReport.relationship.deleted.length;
    }

    /**
     * Description: prepares a deleted Schema to be matched with an added Schema for reconciliation
     * @param deletedSchema
     */
    $scope.matchToAddedSchema = function (deletedSchema) {
        $scope.matchSchema.source = deletedSchema;
        $scope.ctrlConfig.matchmode.schema = true;
        deletedSchema.reconciliationStatus = MATCHING;
    }

    /**
     * Description: associates an added schema with a deleted schema
     * @param addedSchema
     */
    $scope.matchToDeletedSchema = function (addedSchema) {
        $scope.matchSchema.source.reconciledObjectId = addedSchema.objectId;
        $scope.matchSchema.source.reconciledClassId = addedSchema.classId;
        $scope.matchSchema.source.reconciledObjectName = addedSchema.objectName;
        $scope.matchSchema.source.reconciliationStatus = MATCHED;
        $scope.ctrlConfig.matchmode.schema = false;
    }

    /**
     * Description: reset migration matching for identified deleted tableView
     * @param deletedSchema
     */
    $scope.clearMatchSchema = function (deletedSchema) {
        $scope.matchSchema.source = {objectName: "None Selected"};
        deletedSchema.reconciledObjectId = 0;
        deletedSchema.reconciledClassId = 0;
        deletedSchema.reconciledObjectName = "";
        deletedSchema.reconciliationStatus = UNRECONCILED;
        $scope.ctrlConfig.matchmode.schema = false;
    }

    /**
     * Description: sends the deletedTableView list to the DL API to action the reconciliation instructions
     */
    $scope.reconcileSchema = function() {
        const POST_URL = apiHost + "/reverse-engineer/reconcile/" + $scope.databaseId + "/schema";

        HttpService.call(POST_URL, "POST", $scope.reconcileReport.schema.deleted, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Deleted Schemas successfully reconciled.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Error encounted while trying to reconcile deleted Schema",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: prepares a deleted TableView to be matched with an added TableView for reconciliation
     * @param deletedSchema
     */
    $scope.acceptDeletedSchema = function (deletedSchema) {
        $scope.matchSchema.source = {objectName: "None Seleted"};
        $scope.ctrlConfig.matchmode.schema = false;
        deletedSchema.reconciliationStatus = DELETED;

        // update all tableViews for this schema to reconciliationStatus = DELETED.
        // User can overwrite in Columns Tab if desired
        for (let i = 0; i < $scope.reconcileReport.tableView.deleted.length; i++) {
            let deletedTableView = $scope.reconcileReport.tableView.deleted[i];
            if (deletedTableView.parentObjectId == deletedSchema.objectId && deletedTableView.parentClassId == deletedSchema.classId) {
                deletedTableView.reconciliationStatus = DELETED;
            }
        }

        // update all columns for all tables ....
    }

    /**
     * Description: prepares a deleted TableView to be matched with an added TableView for reconciliation
     * @param deletedTableView
     */
    $scope.matchToAddedTV = function (deletedTableView) {
        $scope.matchTableView.source = deletedTableView;
        $scope.ctrlConfig.matchmode.tableview = true;
        deletedTableView.reconciliationStatus = MATCHING;
    }

    /**
     * Description: associates an added tableView with a deleted tableView
     * @param addedTableView
     */
    $scope.matchToDeletedTV = function (addedTableView) {
        $scope.matchTableView.source.reconciledObjectId = addedTableView.objectId;
        $scope.matchTableView.source.reconciledClassId = addedTableView.classId;
        $scope.matchTableView.source.reconciledObjectName = addedTableView.objectName;
        $scope.matchTableView.source.reconciliationStatus = MATCHED;
        $scope.ctrlConfig.matchmode.tableview = false;
    }

    /**
     * Description: reset migration matching for identified deleted tableView
     * @param deletedTableView
     */
    $scope.clearMatchTV = function (deletedTableView) {
        $scope.matchTableView.source = {objectName: "None Selected"};
        deletedTableView.reconciledObjectId = 0;
        deletedTableView.reconciledClassId = 0;
        deletedTableView.reconciledObjectName = "";
        deletedTableView.reconciliationStatus = UNRECONCILED;
        $scope.ctrlConfig.matchmode.tableview = false;
    }

    /**
     * Description: sends the deletedTableView list to the DL API to action the reconciliation instructions
     */
    $scope.reconcileTV = function() {
        const POST_URL = apiHost + "/reverse-engineer/reconcile/" + $scope.databaseId + "/tableview";

        HttpService.call(POST_URL, "POST", $scope.reconcileReport.tableView.deleted, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.reconcileReport.tableView = response.data.result_data;
                        $scope.checkReconciliationRequired();
                        notify({
                            message: "Deleted Tables/Views successfully reconciled.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Error encounted while trying to reconcile deleted Tables/Views",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: prepares a deleted TableView to be matched with an added TableView for reconciliation
     * @param deletedTableView
     */
    $scope.acceptDeletedTV = function (deletedTableView) {
        $scope.matchTableView.source = {objectName: "None Seleted"};
        $scope.ctrlConfig.matchmode.tableview = false;
        deletedTableView.reconciliationStatus = DELETED;

        // update all columns for this table to reconciliationStatus = DELETED. User can overwrite in Columns Tab if desired
        for (let i = 0; i < $scope.reconcileReport.column.deleted.length; i++) {
            let deletedColumn = $scope.reconcileReport.column.deleted[i];
            if (deletedColumn.parentObjectId == deletedTableView.objectId && deletedColumn.parentClassId == deletedTableView.classId) {
                deletedColumn.reconciliationStatus = DELETED;
            }
        }
    }

    /**
     * Description: prepares a deleted Column to be matched with an added TableView for reconciliation
     * @param deletedColumn
     */
    $scope.matchToAddedCol = function (deletedColumn) {
        $scope.matchColumn.source = deletedColumn;
        $scope.ctrlConfig.matchmode.column = true;
        deletedColumn.reconciliationStatus = MATCHING;
    }

    /**
     * Description: associates an added Column with a deleted tableView
     * @param addedColumn
     */
    $scope.matchToDeletedCol = function (addedColumn) {
        $scope.matchColumn.source.reconciledObjectId = addedColumn.objectId;
        $scope.matchColumn.source.reconciledClassId = addedColumn.classId;
        $scope.matchColumn.source.reconciledObjectName = addedColumn.parentObjectName + "." + addedColumn.objectName;
        $scope.matchColumn.source.reconciliationStatus = MATCHED;
        $scope.ctrlConfig.matchmode.column = false;
    }

    /**
     * Description: reset migration matching for identified deleted tableView
     * @param deletedColumn
     */
    $scope.clearMatchCol = function (deletedColumn) {
        $scope.matchColumn.source = {objectName: "None Selected"};
        deletedColumn.reconciledObjectId = 0;
        deletedColumn.reconciledClassId = 0;
        deletedColumn.reconciledObjectName = "";
        deletedColumn.reconciliationStatus = UNRECONCILED;
        $scope.ctrlConfig.matchmode.column = false;
    }

    /**
     * Description: prepares a deleted Column to be matched with an added Column for reconciliation
     * @param deletedColumn
     */
    $scope.acceptDeletedCol = function (deletedColumn) {
        $scope.matchColumn.source = {objectName: "None Seleted"};
        $scope.ctrlConfig.matchmode.column = false;
        deletedColumn.reconciliationStatus = DELETED;
    }

    /**
     * Description: sends the deleted Column list to the DL API to action the reconciliation instructions
     */
    $scope.reconcileCol = function() {
        const POST_URL = apiHost + "/reverse-engineer/reconcile/" + $scope.databaseId + "/column";

        HttpService.call(POST_URL, "POST", $scope.reconcileReport.column.deleted, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.reconcileReport.column = response.data.result_data;
                        $scope.checkReconciliationRequired();
                        notify({
                            message: "Deleted Columns successfully reconciled.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Error encounted while trying to reconcile deleted Columns",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    }

    /**
     * Description: prepares a deleted Column to be matched with an added TableView for reconciliation
     * @param deletedColumn
     */
    $scope.matchToAddedRel = function (deletedRelationship) {
        $scope.matchRelationship.source = deletedRelationship;
        $scope.ctrlConfig.matchmode.relationship = true;
        deletedRelationship.reconciliationStatus = MATCHING;
    }

    /**
     * Description: associates an added Column with a deleted tableView
     * @param addedColumn
     */
    $scope.matchToDeletedRel = function (addedRelationship) {
        $scope.matchRelationship.source.reconciledObjectId = addedRelationship.objectId;
        $scope.matchRelationship.source.reconciledClassId = addedRelationship.classId;
        $scope.matchRelationship.source.reconciledObjectName = addedRelationship.objectName;
        $scope.matchRelationship.source.reconciliationStatus = MATCHED;
        $scope.ctrlConfig.matchmode.relationship = false;
    }

    /**
     * Description: reset migration matching for identified deleted tableView
     * @param deletedColumn
     */
    $scope.clearMatchRel = function (deletedRelationship) {
        $scope.matchRelationship.source = {objectName: "None Selected"};
        deletedRelationship.reconciledObjectId = 0;
        deletedRelationship.reconciledClassId = 0;
        deletedRelationship.reconciledObjectName = "";
        deletedRelationship.reconciliationStatus = UNRECONCILED;
        $scope.ctrlConfig.matchmode.relationship = false;
    }

    /**
     * Description: prepares a deleted Column to be matched with an added Column for reconciliation
     * @param deletedColumn
     */
    $scope.acceptDeletedRel = function (deletedRelationship) {
        $scope.matchRelationship.source = {objectName: "None Seleted"};
        $scope.ctrlConfig.matchmode.relationship = false;
        deletedRelationship.reconciliationStatus = DELETED;
    }

    /**
     * Description: sends the deleted Relationship list ($scope.reconcileReport.relationship.deleted)to the DL API
     *  to action the reconciliation instructions
     */
    $scope.reconcileRel = function () {
        console.log($scope.reconcileReport.relationship.deleted);
        const POST_URL = apiHost + "/reverse-engineer/reconcile/" + $scope.databaseId + "/relationship";

        HttpService.call(POST_URL, "POST", $scope.reconcileReport.relationship.deleted, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        console.log(response.data.result_data);
                        $scope.reconcileReport.relationship = response.data.result_data;
                        $scope.checkReconciliationRequired();
                        notify({
                            message: "Deleted Relationships successfully reconciled.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Error encounted while trying to reconcile deleted Relationships",
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: return to the previous state in the Navigation Chain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Stores current state in the NavigationChainService and transitions to the smd.table_view_detail state
     * for entity identified by objectId and classId
     * @param objectId
     * @param classId
     */
    $scope.clickThrough = function (objectId, classId) {

        // TODO: need correct click through from database list controller
        const currentStateSpec = {
            stateName:"smd.reconcile_reveng_run",
            objectId: $scope.databaseId,
            classId: $scope.classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams: $scope.stateParams
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const moClass = MoClassService.getMoClassByClassId(classId);

        const invocationParams = {
            stateName: moClass.objectDetailState,
            objectId: objectId,
            classId: classId,
            parentObjectId: $scope.databaseId,
            parentClassId: $scope.classId,
            stateParams: {}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdReconcileDbRevEngRunController', SmdReconcileDbRevEngRunController);
