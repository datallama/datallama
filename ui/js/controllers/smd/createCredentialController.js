/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function CreateCredentialController ($scope, $stateParams, notify, HttpService, NavigationChainService) {
    $scope.invocationParams = {};

    $scope.credentialTypeList = [];
    $scope.credentialType = {
        credentialTypeId: 0,
        credentialTypeName: "",
        credentialTypeShortDesc: "",
        credentialTemplate: {},
        formDefinition: [],
        schemaDefinition: {}
    };
    $scope.customForm = {
        formData: {},
        schemaDefinition: {},
        formDefinition: []
    };

    $scope.schemaDefinition = {};
    $scope.formDefinition = [];

    $scope.newCredential = {
        credentialName: "",
        credentialShortDesc: "",
        credentialTypeId: 0,
        credentialDetail: {}
    };

    $scope.validation = {
        credentialName: true,
        credentialType: true,
        credentialDetail: true
    };

    $scope.credentialTypeDisabled = false;
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialise the controller with defaults and the credential type list
     */
    $scope.initCreateCredential = function () {
        $scope.invocationParams = $stateParams.invocationParams;
        if ($scope.invocationParams.stateParams.credentialTypeId != null) {
            $scope.credentialType.credentialTypeId = $scope.invocationParams.stateParams.credentialTypeId;
            $scope.credentialTypeDisabled = true;
        }

        const GET_URL = apiHost + "/smd/credential/type_list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        for(let i = 0; i < response.data.result_data.length; i++) {
                            let credentialType = {
                                credentialTypeId: response.data.result_data[i].credentialTypeId,
                                credentialTypeName: response.data.result_data[i].credentialTypeName,
                                credentialTypeShortDesc: response.data.result_data[i].credentialTypeShortDesc,
                                credentialTemplate: JSON.parse(response.data.result_data[i].credentialTemplate),
                                schemaDefinition: JSON.parse(response.data.result_data[i].schemaDefinition),
                                formDefinition: JSON.parse(response.data.result_data[i].formDefinition)
                            };
                            $scope.credentialTypeList.push(credentialType);

                            // set the selected credentialType if specified in invocationParam.stateParams
                            if (credentialType.credentialTypeId == $scope.credentialType.credentialTypeId) {
                                $scope.credentialType = credentialType;
                                $scope.resetCredentialFormData();
                            }
                        }
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "An error occurred when retrieving the list of credential types.",
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: resets the form data in element newCredential.credentialDetail when user changes credential type selection
     */
    $scope.resetCredentialFormData = function () {
        $scope.newCredential.credentialDetail = {};
        $scope.customForm.schemaDefinition = $scope.credentialType.schemaDefinition;
        $scope.customForm.formDefinition = $scope.credentialType.formDefinition;
    }

    /**
     * Description: invokes the DL API to create a new credential
     */
    $scope.create = function () {

        // 1. validate input fields
        $scope.validation.credentialName = !!($scope.newCredential.credentialName && $scope.newCredential.credentialName.length > 0);
        $scope.validation.credentialType = !!($scope.credentialType.credentialTypeId > 0);
        $scope.$broadcast('schemaFormValidate');

        if (this.credentialForm.$valid) {
            $scope.validation.credentialDetail = true;
        } else {
            $scope.validation.credentialDetail = false;
        }

        // 2. create the new credential if validation is successful
        if($scope.validation.credentialName && $scope.validation.credentialType && $scope.validation.credentialDetail) {
            const POST_URL = apiHost + "/smd/credential";

            let newCredentialPost = {
                credentialId: -1,
                credentialTypeId: $scope.credentialType.credentialTypeId,
                credentialName: $scope.newCredential.credentialName,
                credentialShortDesc: $scope.newCredential.credentialShortDesc,
                credentialDetail: JSON.stringify($scope.customForm.formData)
            };

            HttpService.call(POST_URL, "POST", newCredentialPost, null,
                [
                    {
                        status: 1,
                        callback: function(response) {
                            $scope.goBack();
                            notify({
                                message: "New credential created successfully.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    },
                    {
                        status: -20,
                        callback: function(response) {
                            notify({
                                message: "A credential with that name already exists. Please specify a unique credential name.",
                                classes: "alert-warning",
                                duration: 5000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );
        }
        else {
            notify({
                message: "There are input errors in the New Credential form, as indicated. Please correct and click Create again.",
                classes: "alert-danger",
                duration: 5000,
                templateUrl: $scope.notifyTemplate
            });
        }

    }

    /**
     * Description: invokes the DL API to test connectivity for a credential
     */
    $scope.testConnection = function () {
        // TODO: implement the test
    }

    /**
     * Description: close the dialog without executing any actions.
     */
    $scope.goBack = function () {
        NavigationChainService.goBack();
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateCredentialController', CreateCredentialController);
