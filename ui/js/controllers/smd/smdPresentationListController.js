/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function SmdPresentationListController($scope, $rootScope, $location, $state, $uibModal, notify, DTOptionsBuilder,
                                     NavigationChainService, HttpService, MoClassService, UtilityService) {
    $scope.gridData = [];


    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'FormatList'},
            {extend: 'pdf', title: 'FormatList'}
        ]);

    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialisation function for the controller
     */
    $scope.initPresentationList = function () {
        $scope.readGridData();
    }

    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, stateParams) {
        const currentStateSpec = {
            stateName: "smd.smd_presentation_list",
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let targetState = "not_defined";
        let clickThroughMode = "not_defined";
        if (stateParams.hasOwnProperty("clickThroughMode")) {
            clickThroughMode = stateParams.clickThroughMode;
        }

        switch (clickThroughMode) {
            case "summary":
                targetState = "smd.smd_presentation_summary";
                break;
            case "detail":
                // determine detail view state from parentClassId
                if (parentClassId === MoClassService.getMoClassIdByCode('PRSNT_RPT') || parentClassId === MoClassService.getMoClassIdByCode('PRSNT_FORM') || parentClassId === MoClassService.getMoClassIdByCode('PRSNT_VIS')) {
                    targetState = 'smd.smd_presentation_detail';
                }
                break;
        }

        const invocationParams = {
            stateName: targetState,
            objectId: objectId,
            classId: classId,
            parentObjectId: parentObjectId,
            parentClassId: parentClassId,
            stateParams:{activeDetailTab:{attributes:true, associations:false, comments:false, activeIndex:1}}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    $scope.openCreatePresentationDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_presentation.html'),
            controller: 'CreatePresentationController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: creates a new message format in table dl_format
     */
    $scope.createPresentation = function(newObject) {
        const POST_URL = apiHost + '/smd/presentation/summary';

        HttpService.call(POST_URL, "POST", newObject, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.readGridData();
                        notify({
                            message: "New Presentation definition saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description:
     */
    $scope.readGridData = function() {
        const GET_URL = apiHost + '/smd/presentation/list';

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdPresentationListController', SmdPresentationListController);
