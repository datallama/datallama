/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Filestore Detail view.
 *  The structure of $scope.selectedDirectory is:
 *  {filestoreId: 0, directoryId: 0, classId: 0, directoryParentId: 0, directoryLevel: 0, directoryLevelSequence: 0, directoryGUID: "",
 *   directoryHierarchyNumber: "", directoryName: "", directoryDesc: "", directoryFullPathName: ""}
 */
function SmdHierarchyRevEngController($scope, $state, $stateParams, $uibModal, notify, NavigationChainService, RbacService,
                                      HttpService, DTOptionsBuilder, DTColumnBuilder, MoClassService, UtilityService) {
    const UNRECONCILED = 0;     // these constants are used for reverse engineer run reconciliation
    const MATCHING = 1;
    const MATCHED = 2;
    const DELETED = 3;

    $scope.invocationParams = {};
    $scope.connectorList = [];
    $scope.selectedConnector = null;
    $scope.selectedConnectorUndo = null;
    $scope.credentialList = [];
    $scope.selectedCredential = null;
    $scope.selectedCredentialUndo = null;

    $scope.revEngRunLog = [];   // used by reverse engineer run log grid
    $scope.matchElement = {source: {objectName: "None Selected"}, target: {}};

    $scope.perms = {
        penv: {create: false, update: false, delete: false, publish: false},
        reveng: {create: false, update: false, delete: false, publish: false}
    };
    $scope.editingDisabled = true;
    $scope.validation = {connector: true, credential: true};

    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    $scope.dtOptions01 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'csv', title: 'dlReverseEngineeringLog'}
        ])
        .withOption('order', [[0, 'desc']]);
    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([])
        .withOption('language', {'sEmptyTable': 'No elements requiring reconciliation'} );

    $scope.breadcrumbConfig = {
        lvl3displayName: '',
        lvl3stateName: ''
    };

    /**
     * Description: initialises the angularJs controller with default values
     */
    $scope.initSmdHierarchyRevEng = function () {
        // 1. process any parameters sent with state transition
        $scope.invocationParams = $stateParams.invocationParams;

        switch ($scope.invocationParams.parentClassId) {    // TODO: maybe the displayName and stateName should be passed in as invocationParams.stateParams
            case MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PROJECT'):
                $scope.breadcrumbConfig.lvl3displayName = 'Data Integration';
                $scope.breadcrumbConfig.lvl3stateName = 'smd.dataintegration_list';
                break;
            case MoClassService.getMoClassIdByCode('FILE_STORE'):
                $scope.breadcrumbConfig.lvl3displayName = 'File & Object Stores';
                $scope.breadcrumbConfig.lvl3stateName = 'smd.smd_filestore_list';
                break;
            case MoClassService.getMoClassIdByCode('FILE_STORE'):
                $scope.breadcrumbConfig.lvl3displayName = 'File & Object Stores';
                $scope.breadcrumbConfig.lvl3stateName = 'smd.smd_filestore_list';
                break;
            default:
                $scope.breadcrumbConfig.lvl3displayName = 'File & Object Stores';
                $scope.breadcrumbConfig.lvl3stateName = 'smd.smd_filestore_list';
                break;
        }

        if ($stateParams.invocationParams.stateParams.editingDisabled == false) {
            $scope.editingDisabled = false;
            $scope.dlIboxToolsShowEditIcon = false;
        }
        else {
            $scope.editingDisabled = true;
            $scope.dlIboxToolsShowEditIcon = true;
        }

        if ($stateParams.invocationParams.stateParams.selectedCredential != null) {
            $scope.selectedCredential = $stateParams.invocationParams.stateParams.selectedCredential;
        }
        else
            $scope.selectedCredential = null;

        if ($stateParams.invocationParams.stateParams.selectedConnector != null) {
            $scope.selectedConnector = $stateParams.invocationParams.stateParams.selectedConnector;
            $scope.connectorSelectionChange();
        }
        else
            $scope.selectedConnector = null;

        $scope.perms.penv.create = !RbacService.checkRbac('penv','create');
        $scope.perms.penv.update = !RbacService.checkRbac('penv','update');
        $scope.perms.penv.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.penv.publish = !RbacService.checkRbac('penv','publish');
        $scope.perms.reveng.create = !RbacService.checkRbac('reveng','create');
        $scope.perms.reveng.update = !RbacService.checkRbac('reveng','update');
        $scope.perms.reveng.delete = !RbacService.checkRbac('reveng','delete');
        $scope.perms.reveng.publish = !RbacService.checkRbac('reveng','publish');

        // 2. retrieve data sets for view
        $scope.readRevEngConnectorList();
        $scope.readRevEngRunHistory();
    }

    /**
     * Description: invokes the DL API to get the list of reverse engineering connectors configured for this MO class
     */
    $scope.readRevEngConnectorList = function() {
        const GET_URL = apiHost + '/smd/reverse-engineer/connector/list/forclass/' + $scope.invocationParams.parentClassId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.connectorList = response.data.result_data;
                    }
                }
            ]
        );

    }

    /**
     * Description: updates credential list when reverse engineer connector is selected.
     */
    $scope.connectorSelectionChange = function() {
        const GET_URL = apiHost + '/smd/credential/list/forconnector/' + $scope.selectedConnector.connectorId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.credentialList = response.data.result_data;
                    }
                }
            ]
        );

    }

    /**
     * Description: validate connection & credential configuration and invoke the DL API to execute a reverse engineer run
     */
    $scope.executeOnlineRevEngRun = function() {
        // 1. validate the connection and credential configuration
        let valid = true;
        if ($scope.selectedConnector == null)
            valid = false;
        else if ($scope.selectedConnector.connectorId <= 0)
            valid = false;
        if ($scope.selectedCredential == null)
            valid = false;
        else if ($scope.selectedCredential.credentialId <= 0)
            valid = false;

        // 2. invoke DL API if valid or notify user if invalid
        if (valid) {
            let postUrlPath = "";
            switch ($scope.invocationParams.parentClassId) {
                // TODO: add postUrlPath for FILE_STORE reverse engineer
                case MoClassService.getMoClassIdByCode('OBJECT_STORE_SPACE'):
                    postUrlPath = '/smd/fos/reverse-engineer/objectstore/online/';
                    break;
                case MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PROJECT'):
                    postUrlPath = '/smd/dataintegration/reverse-engineer/online/';
                    break;
            }
            $scope.setSpinnerLoading(true);
            const POST_URL = apiHost + postUrlPath + $scope.invocationParams.parentObjectId;

            HttpService.call(POST_URL, "POST", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.readRevEngRunHistory();
                            // $scope.getRevEngReconcileReport(); TODO: implement reverse engineer run reconciliation
                            $scope.setSpinnerLoading(false);
                            notify({
                                message: "The reverse engineer run completed. RECONCILIATION is required for one or more deleted elements.",
                                classes: "alert-warning",
                                duration: 5000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    },
                    {
                        status: 2,
                        callback: function(response){
                            $scope.readRevEngRunHistory();
                            $scope.setSpinnerLoading(false);
                            notify({
                                message: "The reverse engineer run completed. No reconciliation is required.",
                                classes: "alert-success",
                                duration: 5000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    },
                    {
                        status: -1,
                        callback: function(response) {
                            $scope.setSpinnerLoading(false);
                            let msg = "ERROR: the reverse engineer run failed with an error.\n";
                            msg = msg + "Error Number: " + response.data.result_data.errorNumber + "\n";
                            msg = msg + "Error Message: " + response.data.result_data.generalErrorMessage;

                            notify({
                                message: msg,
                                classes: "alert-warning",
                                duration: 5000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: Opens the smdHierarchyRevEngOffline dialog box to allows the user to select an offline reverse engineer file
     */
    $scope.offlineRevEngSelectFileDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/hierarchy_reveng_select_offline_file.html'),
            controller: 'HierarchyRevEngSelectOfflineFileController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: invokes the DL API to get the history of reverse engineering runs for the active file/object store
     */
    $scope.readRevEngRunHistory = function() {
        let postUrlPath = "";
        switch ($scope.invocationParams.parentClassId) {
            case MoClassService.getMoClassIdByCode('OBJECT_STORE_SPACE'):
                postUrlPath = '/smd/fos/reverse-engineer/history/';
                break;
            case MoClassService.getMoClassIdByCode('FILE_STORE'):
                postUrlPath = '/smd/fos/reverse-engineer/history/';     // TODO: update when the Filestore reveng has been implemented
                break;
            case MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PROJECT'):
                postUrlPath = '/smd/dataintegration/reverse-engineer/history/';
                break;
        }

        const GET_URL = apiHost + postUrlPath + $scope.invocationParams.parentObjectId + '/' + $scope.invocationParams.parentClassId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.revEngRunLog = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: enables editing when user clicks the Edit icon in a dlIbox
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        if (dlIboxName === "config") {
            $scope.selectedConnectorUndo = Object.assign({}, $scope.selectedConnector);
            $scope.selectedCredentialUndo = Object.assign({}, $scope.selectedCredential);
            $scope.editingDisabled = false;
        }
    }

    /**
     * Description: saves changes and disables editing when user clicks the Save icon in a dlIbox
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        let result = 1;
        if (dlIboxName === "config") {
            // validate input form
            if ($scope.selectedConnector == null) {
                $scope.validation.connector = false;
                result--;
            }
            if ($scope.selectedCredential == null) {
                $scope.validation.credential = false;
                result--;
            }
            if ($scope.validation.connector && $scope.validation.credential) {
                $scope.editingDisabled = true;
                $scope.saveConCredConfig();
            }
        }
        return result;
    }

    /**
     * Description: disables editing without saving when user clicks the Cancel icon in a dlIbox
     * @param dlIboxName
     */
    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "config") {
            $scope.editingDisabled = true;
            $scope.validation.connector = true;
            $scope.validation.credential = true;

            // reset all fields to the pre-edit state
            if ($scope.selectedConnectorUndo != null && $scope.selectedConnectorUndo.connectorId != $scope.selectedConnector.connectorId)
                $scope.selectedConnector = Object.assign({}, $scope.selectedConnectorUndo);

            let undoCredential = false;
            if ($scope.selectedCredential == null)
                undoCredential = true;
            else {
                if ($scope.selectedCredentialUndo != null && $scope.selectedCredentialUndo.credentialId != $scope.selectedCredential.credentialId)
                    undoCredential = true;
            }

            if (undoCredential) {
                $scope.selectedCredential = Object.assign({}, $scope.selectedCredentialUndo);
                $scope.connectorSelectionChange();
            }

        }
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: invokes the DL API to save the connector/credential configuration
     */
    $scope.saveConCredConfig = function(){
        const POST_URL = apiHost + '/smd/reverse-engineer/concredconfig/';

        let newConfigPost = {
            objectId: $scope.invocationParams.parentObjectId,
            classId: $scope.invocationParams.parentClassId,
            connectorId: $scope.selectedConnector.connectorId,
            credentialId: $scope.selectedCredential.credentialId
        }

        HttpService.call(POST_URL, "POST", newConfigPost, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "New connector and credential configuration saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );

    };

    /**
     * Description: transitions to the Create Credential state if user has selected a connector
     */
    $scope.newCredential = function() {
        if ($scope.selectedConnector.connectorId == 0) {
            notify({
                message: "Please select a Reverse Engineer Connector before creating a new credential.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.clickThrough(0, 0, 0, 0, 'newcredential');
        }
    }

    /**
     * Description: turns the spinner on/off and
     * @param value
     */
    $scope.setSpinnerLoading = function(value){
        $scope.spinnerLoading = value ? "sk-loading" : "";
        // $scope.config.btnlock = value;
    }

    /**
     * Description: transitions to a new AngularJS using the DL NavigationChainService
     * @param objectId
     * @param classId
     * @param parentObjectId
     * @param parentClassId
     * @param mode
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, mode) {

        const currentStateSpec = {
            stateName: $scope.invocationParams.stateName,
            objectId: 0,
            classId: 0,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            stateParams: {
                selectedConnector: $scope.selectedConnector,
                selectedCredential: $scope.selectedCredential,
                editingDisabled: $scope.editingDisabled,
                objectName: $scope.invocationParams.stateParams.objectName,
                className: $scope.invocationParams.stateParams.className
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "newcredential":
                stateName = "smd.credential_create";
                stateParams = {
                    credentialTypeId: $scope.selectedConnector.credentialTypeId
                };
                break;
            default:
                stateName = "";
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: return to previous AJS state via NavigationChainService.
     */
    $scope.goBack = function () {
        NavigationChainService.goBack();
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdHierarchyRevEngController', SmdHierarchyRevEngController);


