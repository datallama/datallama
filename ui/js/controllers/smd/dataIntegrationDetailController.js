/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Data Integration project detail view.
 *
 * The structure of the element object stored in $scope.elements, $scope.selectedElement is:
 * {objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, parentElementObjectId: 0, elementLevel: 0, elementLevelSequence: 0,
 *  elementHierarchyNumber: "s",  elementIsParent: false, objectName: "s", objectShortDesc: "s", objectDescRevEng: "s", objectGUID: "s",
 *  mdContext: [{classID: 0 className": "s", objectID": 1, objectName: "s", internalPath: "s"}]
 *  elementIsVisible: 0, initialExpandLevel: 0, elementIsExpanded: true, elementIsSelected: false, elementIsMatched: false,
 *  resultSetSequence: 0 }
 */
function DataIntegrationDetailController($scope, $rootScope, $location, $state, $stateParams, $window, $uibModal, $anchorScroll, notify,
                                         NavigationChainService, DTOptionsBuilder, HttpService, RbacService, MoClassService, UtilityService) {
    $scope.elements = [];       // this array holds the hierarchy of elements associated with the API definition
    $scope.elementsTableView = [];
    $scope.parentObjectId = 0;        // this is the objectId of the API Definition MO.
    $scope.parentClassId = 0;
    $scope.dataIntegrationSummary = {};
    $scope.selectedElement = null;
    $scope.selectedElementDisplay = null;
    $scope.selectedElementAssociations = [];
    $scope.previousResultSetSequence = 0;
    $scope.newElement = {
        name: "",
        desc: "",
        topLevelElement : true,
        attribute: false
    };

    $scope.customForm = {};
    $scope.conCredConfig = {};
    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false,
        revengdesc: false, methodparams: false, deletemode: ""};
    $scope.search = {keyword: "", currentViewIndex: 0, matchList: []};
    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};
    $scope.moveElement = {moveMode: false, moveTargetElement: null};

    $scope.editingDisabled = {attr: true, cfrm: true};
    $scope.editingUndo = {attr: {}, cfrm: {}};
    $scope.activeDetailTab = {attributes:true, associations:false, comments:false,  customform:false, more:false, activeIndex:1};
    $scope.elementDisplayTitle = "Element";

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv', title: 'REST API Elements'}
        ]);

    $scope.dtOptions02 = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'}
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialisation function for the controller
     */
    $scope.initDataIntegrationDetail = function() {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.parentObjectId = $scope.invocationParams.parentObjectId;
        $scope.parentClassId = $scope.invocationParams.parentClassId;
        $scope.activeDetailTab = $scope.invocationParams.stateParams.activeDetailTab;

        $scope.readElements();

        if($scope.invocationParams.objectId > 0) {  // load the API element specified by objectId
            $scope.loadSelectedElement($scope.invocationParams.objectId);
        }
    }

    /**
     * Reads from the DL database all elements in associated with the REST API Definition identified by $scope.parentObjectId
     */
    $scope.readElements = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/smd/dataintegration/content/' + $scope.parentObjectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.dataIntegrationSummary = response.data.result_data.dataintegration_summary;
                        $scope.elements = response.data.result_data.dataintegration_elements;

                        // Parse the JSON values for custom form
                        $scope.customForm.customFormId = response.data.result_data.dataintegration_summary.customForm.customFormId;
                        $scope.customForm.customFormName = response.data.result_data.dataintegration_summary.customForm.customFormName;
                        $scope.customForm.customFormShortDesc = response.data.result_data.dataintegration_summary.customForm.customFormShortDesc;
                        $scope.customForm.schemaDefinition = JSON.parse(response.data.result_data.dataintegration_summary.customForm.schemaDefinition);
                        $scope.customForm.formDefinition = JSON.parse(response.data.result_data.dataintegration_summary.customForm.formDefinition);
                        $scope.customForm.formData = JSON.parse('{}');
                        $scope.disableCustomFormFields();

                        $scope.conCredConfig = response.data.result_data.concred_config;

                        angular.copy($scope.elements, $scope.elementsTableView);
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: loads the selected REST API eoement when returning to the view from a Go Back action
     * @param objectId
     */
    $scope.loadSelectedElement = function(objectId) {

        // 2. get full details of the node from the DL database
        const GET_URL = apiHost + '/smd/dataintegration/element/' + objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.processSelectedElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the modal dialag that allows the user to import an XML file
     */
    $scope.openLoadXMLMessageFormatsDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/formats/load_xml_msg_format.html'),
            controller: 'LoadXMLFormatController',
            size: 'md',
            scope: $scope
        });

    }

    /**
     * Description: enables fields in selected element display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxParentEdit = function(dlIboxName) {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            notify({
                message: "You have not selected an element. No edits can be made.",
                classes: "alert-warning",
                duration: 5000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            if (dlIboxName === "ted") {
                angular.copy($scope.selectedElementDisplay, $scope.editingUndo.attr);
                $scope.editingDisabled.attr = false;
            }
            else if (dlIboxName === "cfrm") {
                $scope.enableCustomFormFields();
                $scope.editingDisabled.cfrm = false;
            }
        }
    }

    /**
     * Description: saves changes made by user to selected element
     * @param dlIboxName
     */
    $scope.dlIboxParentSave = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.editingDisabled.attr = true;
            $scope.saveElementCoreDetails();
        }
        else if (dlIboxName === "cfrm") {
            $scope.editingDisabled.cfrm = true;
            $scope.saveSelectedCustomFormData();
            $scope.disableCustomFormFields();
        }
    }

    /**
     * Description:
     * @param dlIboxName
     */
    $scope.dlIboxParentCancel = function(dlIboxName) {
        if (dlIboxName === "ted") {
            $scope.editingDisabled.attr = true;
            // reset all fields to the pre-edit state
            angular.copy($scope.editingUndo.attr, $scope.selectedElementDisplay);
        }
        else if (dlIboxName === "cfrm") {
            $scope.editingDisabled.cfrm = false;
        }
    }

    /**
     * Description: checks whether an element in the hierarchy has been selected. If parameter silent is true then notification is not provided
     * @param silent
     * @returns {boolean}
     */
    $scope.checkElementSelected = function(silent) {
        if ($scope.selectedElement == null || $scope.selectedElement.objectId <= 0) {
            if(silent !== true){
                notify({
                    message: "No element selected.",
                    classes: "alert-danger",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate
                });
            }
            return false;
        }
        return true;
    }

    $scope.remove = function(scope) {
        scope.remove();
    };
    $scope.toggle = function(scope) {
        scope.toggle();
    };
    $scope.moveLastToTheBeginning = function () {
        const a = $scope.data.pop();
        $scope.data.splice(0,0, a);
    };

    /**
     * Description: initialises a new REST API Element
     */
    $scope.initiateNewElement = function() {
        $scope.newElementConfig = {
            topLevelElement: false,
            packageSelectable: true,
            jobSelectable: true,
            parentElementClassId: 0,
            elementType: "package"
        };

        if ($scope.selectedElement == null) {
            $scope.newElementConfig.topLevelElement = true;
            $scope.newElementConfig.packageSelectable = true;
            $scope.newElementConfig.jobSelectable = true;
            $scope.newElementConfig.parentElementClassId = 0;
            $scope.newElementConfig.elementType = "package";
        }
        else {
            $scope.newElementConfig.topLevelElement = false;

            switch ($scope.selectedElement.classId) {
                case  MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PACKAGE'):
                    $scope.newElementConfig.packageSelectable = true;
                    $scope.newElementConfig.jobSelectable = true;
                    $scope.newElementConfig.elementType = "package";
                    break;
                case  MoClassService.getMoClassIdByCode('DATA_INTEGRATION_JOB'):
                    $scope.newElementConfig.packageSelectable = false;
                    $scope.newElementConfig.jobSelectable = true;
                    $scope.newElementConfig.elementType = "job";
                    break;
            }
            $scope.newElementConfig.parentElementClassId = $scope.selectedElement.parentClassId;
        }

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/create_dataintegration_element.html'),
            controller: 'CreateDataIntegrationElementController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: collapses the whole taxonomy down to level 0
     */
    $scope.collapseAll = function() {
        for(let i = 0; i < $scope.elements.length; i++) {
            if ($scope.elements[i].elementLevel > 0) {
                $scope.elements[i].elementIsVisible = 0;
            }
            $scope.elements[i].elementIsExpanded = false;
        }
    };

    /**
     * Description: expands every element in the taxonomy
     */
    $scope.expandAll = function() {
        for(let i = 0; i < $scope.elements.length; i++) {
            $scope.elements[i].elementIsVisible = 1;
            $scope.elements[i].elementIsExpanded = true;
        }
    };

    /**
     * Description: expands the taxonomy to the level specified by the user in input dl-taxo-vis-level
     */
    $scope.expandToLevel = function() {
        if ($scope.config.visibilityLevel < 1)
            $scope.config.visibilityLevel = 1;

        for(let i = 0; i < $scope.elements.length; i++) {
            if ($scope.elements[i].elementLevel < $scope.config.visibilityLevel - 1) {
                $scope.elements[i].elementIsVisible = 1;
                $scope.elements[i].elementIsExpanded = true;
            }
            else if ($scope.elements[i].elementLevel === $scope.config.visibilityLevel - 1) {
                $scope.elements[i].elementIsVisible = 1;
                $scope.elements[i].elementIsExpanded = false;
            }
            else {
                $scope.elements[i].elementIsVisible = 0;
                $scope.elements[i].elementIsExpanded = false;
            }
        }
    };

    /**
     * Description: toggles the expanded view of an element.
     * @param element
     */
    $scope.toggleElementExpansion = function(element) {
        const setVisibility = (element.elementIsExpanded) ? 0 : 1;
        const ehnMatch = element.elementHierarchyNumber + ".";

        if (element.elementIsExpanded) {         // collapse all levels below this element
            for(let i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.elements[i].elementLevel > element.elementLevel){
                    $scope.elements[i].elementIsVisible = 0;
                    $scope.elements[i].elementIsExpanded = false;
                }
                if ($scope.elements[i].objectId === element.objectId)
                    $scope.elements[i].elementIsExpanded = false;
            }
        }
        else {  // expand all levels below this element
            for(let i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].elementHierarchyNumber.startsWith(ehnMatch)
                    && $scope.elements[i].elementLevel === element.elementLevel + 1){
                    $scope.elements[i].elementIsVisible = 1;
                }
                if ($scope.elements[i].objectId === element.objectId)
                    $scope.elements[i].elementIsExpanded = true;
            }
        }
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.elementsTableView = [];
        if ($scope.config.endnodes) {
            let endnodes = [];
            for(let i = 0; i < $scope.elements.length; i++) {
                if ($scope.elements[i].elementIsParent === false) {
                    endnodes.push($scope.elements[i]);
                }
            }
            angular.copy(endnodes, $scope.elementsTableView);
        }
        else {
            angular.copy($scope.elements, $scope.elementsTableView);
        }
    }

    /**
     * Description: saves a new REST API element to the DL database
     */
    $scope.saveNewElement = function(newElementSpec) {

        let parentElementObjectId = 0;
        if(!newElementSpec.topLevelElement){
            parentElementObjectId = $scope.selectedElement.objectId;
        }

        let classId = 0;
        switch (newElementSpec.type) {
            case "package":
                classId = MoClassService.getMoClassIdByCode("DATA_INTEGRATION_PACKAGE");
                break;
            case "job":
                classId = MoClassService.getMoClassIdByCode("DATA_INTEGRATION_JOB");
                break;
        }

        const newElement = {
            objectId: -1,
            classId: classId,
            parentObjectId: $scope.parentObjectId,
            parentClassId: $scope.parentClassId,
            parentElementObjectId: parentElementObjectId,
            objectName: newElementSpec.name,
            objectShortDesc: newElementSpec.shortDesc
        };

        const POST_URL = apiHost + "/smd/dataintegration/element";

        HttpService.call(POST_URL, "POST", newElement, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.processNewElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     *  Splices a new REST API element into the $scope.elements array
     * @param response
     */
    $scope.processNewElementResponse = function(response){
        // TODO: new element should be spliced into the $scope.elements array. For now just refresh the entire list
        $scope.readElements();
    }

    /**
     * Description: saves to the DL database the updated core details for the currently selected REST ELement
     */
    $scope.saveElementCoreDetails = function() {
        const POST_URL = apiHost + "/smd/dataintegration/element";

        HttpService.call(POST_URL, "POST", $scope.selectedElementDisplay, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        // update the selected element with any values changed in the LHS display panel
                        $scope.selectedElement.objectName = $scope.selectedElementDisplay.objectName;
                        $scope.selectedElement.objectShortDesc = $scope.selectedElementDisplay.objectShortDesc;

                        notify({
                            message: "Changes were saved successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {
        const customFormCopy = {};
        if ($scope.customForm != null) {
            angular.copy($scope.customForm, customFormCopy);
            for (const fieldDef in customFormCopy.schemaDefinition.properties) {
                const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
                eval(setreadonly);
            }
            angular.copy(customFormCopy, $scope.customForm);
        }
    }

    /**
     * Description: Open a dialog to warn that changing the custom form ID will
     *  delete the custom form data in the taxonomy elements
     */
    $scope.openSelectCustomFormWarningDialog = function() {
        $scope.warningDialogSettings = {
            title: "Warning! Critical Data Change",
            text:
                "<p>Warning!</p>" +
                "<p>If you change the Custom Form ID <b>ALL</b> custom form attributes for this table/view and all columns " +
                "that inherit the custom form definition will be deleted.</p>" +
                "<p>Do you wish to continue?</p>",
            confirmButton: "Continue",
            cancelButton: "Cancel"
        }
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
            controller: 'CmnWarningController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: This function is invoked when the Change Custom Form warning dialog is closed by the OK button.
     */
    $scope.warningAccepted = function() {
        $scope.openSelectCustomFormDialog();
    }

    /**
     * Description: Open a dialog to select a custom form for the taxonomy
     */
    $scope.openSelectCustomFormDialog = function() {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_select_custom_form.html'),
            controller: 'CmnSelectCustomFormController',
            size: 'md',
            scope: $scope
        });
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: Call back from select custom form dialog
     */
    $scope.saveSelectedCustomForm = function(selectedCustomForm){
        const POST_URL = apiHost + "/smd/dataintegration/customform/";
        const postValues = {
            "objectId": $scope.parentObjectId,
            "classId": $scope.parentClassId,
            "customFormId": selectedCustomForm.customFormId
        };

        HttpService.call(POST_URL, "POST", postValues, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        notify({
                            message: "Custom form association successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.updateCustomFormDefinition(selectedCustomForm.customFormId);
                    }
                }
            ]
        );
    }

    /**
     * Description: updates the custom form definition after it has been change by the user.
     * @param customFormId
     */
    $scope.updateCustomFormDefinition = function(customFormId) {
        const GET_URL = apiHost + "/common/customform/" + customFormId;
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.customForm = response.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data to the DL DB for the selected REST API element
     */
    $scope.saveSelectedCustomFormData = function() {
        const POST_URL = apiHost + "/common/customform/data";

        let moCustomFormData = {
            objectId: $scope.selectedElementDisplay.objectId,
            classId: $scope.selectedElementDisplay.classId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            customFormId: $scope.customForm.customFormId,
            formData: JSON.stringify($scope.customForm.formData)
        }

        HttpService.call(POST_URL, "POST", moCustomFormData, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Custom form data successfully updated.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response){
                        notify({
                            message: "Custom form data update failed.",
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: reads all comments for the selected element
     */
    $scope.readElementComments = function() {
        if ($scope.selectedElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedElement.objectId + "/" + $scope.selectedElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status:1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: toggles visibility of the comment panel
     */
    $scope.toggleCommentPanel = function() {
        if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
            notify({
                message: "You have not selected an element. No comments can be added.",
                classes: "alert-warning",
                duration: 5000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.comment.commentPanelHidden = !$scope.comment.commentPanelHidden;
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedElement == null) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedElement.objectId,
                classId: $scope.selectedElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL,"POST",newComment,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: display details of a taxonomy element when it is clicked in the hierarchy view.
     *  If the user has selected "Move Element" mode then details for the selected element are NOT displayed.
     *  Instead, the element is selected as the new parent for the move operation.
     * @param selectedElement
     * TODO: need to find a way to shift focus to Attributes tab
     */
    $scope.showElementDetails = function(selectedElement) {

        // 1. test whether 'move element' mode is selected.
        if ($scope.moveElement.moveMode) {
            if (selectedElement.elementHierarchyNumber.startsWith($scope.selectedElement.elementHierarchyNumber)) {
                notify({ message: "You cannot move an element to one of its children.",
                    classes: "alert-warning",
                    duration: 2000,
                    templateUrl: $scope.notifyTemplate});
            }
            else {
                $scope.moveElement.moveTargetElement = selectedElement;
                $scope.setActiveTab(5);
            }
        }
        else {
            $scope.elements[$scope.previousResultSetSequence].elementIsSelected = 0;

            $scope.selectedElement = $scope.elements[selectedElement.resultSetSequence];
            $scope.elements[$scope.selectedElement.resultSetSequence].elementIsSelected = 1;
            $scope.previousResultSetSequence = $scope.selectedElement.resultSetSequence;

            $scope.resetEditing();
            $window.scrollTo(0, 0);

            // 2. get API element details.
            let GET_URL = apiHost + '/smd/dataintegration/element/' + $scope.selectedElement.objectId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response) {
                            $scope.processSelectedElementResponse(response);
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: processes the element details returned by API to support display in the view
     * @param response
     */
    $scope.processSelectedElementResponse = function(response) {
        $scope.selectedElementDisplay = response.data.result_data.attributes;
        $scope.selectedElementDisplay.methodParameters = JSON.parse("[]");
        $scope.config.methodparams = false;

        if ($scope.selectedElementDisplay.elementDescReveng == null || $scope.selectedElementDisplay.elementDescReveng.length == 0) {
            $scope.config.revengdesc = false;
        } else {
            $scope.config.revengdesc = true;
        }

        switch ($scope.selectedElementDisplay.classId) {
            case MoClassService.getMoClassIdByCode("DATA_INTEGRATION_PACKAGE"):
                $scope.elementDisplayTitle = "Package";
                break;
            case MoClassService.getMoClassIdByCode("DATA_INTEGRATION_JOB"):
                $scope.elementDisplayTitle = "Job";
                break;
        }

        $scope.selectedElementDisplay.elementHierarchyNumber = UtilityService.compressHierarchNumber($scope.selectedElementDisplay.elementHierarchyNumber);
        $scope.selectedElementAssociations = response.data.result_data.associations;
        $scope.customForm.formData = JSON.parse(response.data.result_data.custom_form.formData);
        $scope.readElementComments();

    }

    /**
     * Returns the display name for the REST API metadata object class identified by classId
     * @param classId
     */
    $scope.getClassDisplayName = function(classId) {
        const classDetails = MoClassService.getMoClassByClassId(classId);
        let classDisplayName = "unknown";

        switch (classDetails.classCode) {
            case 'DATA_INTEGRATION_PACKAGE':
                classDisplayName = 'package';
                break;
            case 'DATA_INTEGRATION_JOB':
                classDisplayName = 'job';
                break;
        }
        return classDisplayName;
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: checks the user's RBAC status for this view
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function (mode) {
        $scope.config.deletemode = mode;
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: invokes the DL API to delete a REST Element
     */
    $scope.deleteMdObject = function () {
        let DELETE_URL = "";
        if ($scope.config.deletemode == "project") {
            DELETE_URL = apiHost + "/smd/dataintegration/" + $scope.parentObjectId;
        }
        else {
            DELETE_URL = apiHost + "/smd/dataintegration/element/" + $scope.selectedElement.objectId;
        }


        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Data Integration project successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.postDeleteAction();
                    }
                }
            ]
        );
    }

    /**
     * Description: redirects to appropriate AJS State after Data Integration project has been deleted
     */
    $scope.postDeleteAction = function() {
        if ($scope.config.deletemode == "element") {
            $scope.readElements();
        }
        else if ($scope.config.deletemode == "project") {
            let invocationParams = {stateName: "smd.dataintegration_list", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
    }

    /**
     * Description: Opens a model dialog that allows the user to select an API Definition file
     */
    $scope.openRevEngDetail = function() {
        let stateParams = {"clickThroughMode":"reveng"};
        $scope.clickThrough(0, 0, $scope.invocationParams.parentObjectId, $scope.invocationParams.parentClassId, stateParams);
    }

    /**
     * Description: invokes the $scope.clickThrough function to open the Full Association View for the GHMF definition
     */
    $scope.openFullAssociationView = function(){
        const stateParams = {
            clickThroughMode: "full_association_view",
            objectClass: "REST API",
            objectContext: ""
        };

        // because the full association view displays details for the API definition, the objectId and classId parameters
        // for the clickThrough function are set to parentObjectId and parentClassId from the invocation Params
        $scope.clickThrough($scope.invocationParams.parentObjectId, $scope.invocationParams.parentClassId,
            0, 0, stateParams);
    }

    /**
     * Description: opens the Object Association modal dialog
     */
    $scope.newAssociation = function() {
        if ($scope.selectedElement == null || $scope.selectedElement.elementId <= 0) {
            notify({
                message: "You have not selected an element. No associations can be made.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            // execute the navigation transition
            // navigate to the New Association view
            const stateParams = {
                clickThroughMode: "new_association",
                objectClass: "REST API Element",
                objectContext: $scope.selectedElement.mdContext[1].internalPath
            };

            $scope.clickThrough($scope.selectedElement.objectId, $scope.selectedElement.classId,
                $scope.invocationParams.parentObjectId, $scope.invocationParams.parentClassId,
                stateParams);
        }
    }

    /**
     * Description: sets the active tab programatically
     * @param activeTabIndex
     * TODO: the true/false attributes may be redundant
     */
    $scope.setActiveTab = function (activeTabIndex) {
        switch (activeTabIndex) {
            case 1:
                $scope.activeDetailTab = {"attributes":true, "associations":false, "comments":false, "customform":false, "more":false, "activeIndex":1};
                break;
            case 2:
                $scope.activeDetailTab = {"attributes":false, "associations":true, "comments":false, "customform":false, "more":false, "activeIndex":2};
                break;
            case 3:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":true, "customform":false, "more":false, "activeIndex":3};
                break;
            case 4:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":false, "customform":true, "more":false, "activeIndex":4};
                break;
            case 5:
                $scope.activeDetailTab = {"attributes":false, "associations":false, "comments":false, "customform":false, "more":true, "activeIndex":5};
                break;
        }
    }

    /**
     * Description: resets the edit status for all tabs in LHS detail panel
     */
    $scope.resetEditing = function () {
        $scope.tedEditingDisabled = true;
        $scope.cfrmEditingDisabled = true;
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
        $scope.disableCustomFormFields();
    }

    /**
     * Description: searches all elements using keyword
     */
    $scope.executeSearch = function() {
        // 1. clear the previous search
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.elements[$scope.search.matchList[i].resultSetSequence].elementIsMatched = false;
        }
        $scope.search.matchList = [];

        // 2. expand all elements so matches can be displayed
        $scope.expandAll();

        const regexp = new RegExp($scope.search.keyword, "gi");
        for(let i = 0; i < $scope.elements.length; i++) {
            const result = $scope.elements[i].objectName.match(regexp);
            if (result != null) {
                const matchedElement = {
                    objectId: $scope.elements[i].objectId,
                    resultSetSequence: $scope.elements[i].resultSetSequence,
                    objectName: $scope.elements[i].objectName
                }

                $scope.search.matchList.push(matchedElement);
                $scope.elements[i].elementIsMatched = true;
            }
        }
        notify.config({duration: 2000});
        const message = "The Search returned " + $scope.search.matchList.length + " matches.";
        notify({
            message: message,
            classes: "alert-success",
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Description: clears all search results and collapes the hierarchy to 3 levels
     */
    $scope.clearSearch = function() {
        for (let i = 0; i < $scope.search.matchList.length; i++){
            $scope.elements[$scope.search.matchList[i].resultSetSequence].elementIsMatched = false;
        }
        $scope.search.matchList = [];
        $scope.search.currentViewIndex = null;
        $scope.search.keyword = "";

        $scope.config.visibilityLevel = 3;
        $scope.expandToLevel();
    }

    /**
     * Description: scrolls through the search results, up or down, bringing each element into focus
     */
    $scope.scrollSearchResults = function(direction) {
        if ($scope.search.matchList.length > 0){
            if (direction === 'down') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = 0;
                else if ($scope.search.currentViewIndex === $scope.search.matchList.length - 1)
                    $scope.search.currentViewIndex = 0;
                else
                    $scope.search.currentViewIndex++;
            }
            else if (direction === 'up') {
                if ($scope.search.currentViewIndex == null)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else if ($scope.search.currentViewIndex === 0)
                    $scope.search.currentViewIndex = $scope.search.matchList.length - 1;
                else
                    $scope.search.currentViewIndex--;
            }
            $location.hash($scope.search.matchList[$scope.search.currentViewIndex].objectId);
            $anchorScroll();
        }
    }

    /**
     * Description: sets "move element mode" so that the user can select the new parent element
     */
    $scope.toggleMoveMode =  function() {
        if($scope.selectedElement == null || $scope.selectedElement.elementId <= 0){
            notify({
                message: "You have not selected an element.",
                classes: "alert-danger",
                duration: 30000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else{
            $scope.moveElement.moveMode = !$scope.moveElement.moveMode;
            $scope.moveElement.moveTargetElement = null;
        }
    }

    /**
     * Description: moves the initial selected element to the selected target parent element.
     *  It's complicated!
     */
    $scope.executeMove = function() {
        const moveElementDefinition = {
            objectId: $scope.selectedElement.objectId,
            newParentElementObjectId: $scope.moveElement.moveTargetElement.objectId
        };

        const POST_URL = apiHost + "/smd/dataintegration/element/move";

        HttpService.call(POST_URL, "POST", moveElementDefinition, null,
            [
                {
                    status:1,
                    callback: function(response) {
                        $scope.processMoveElementResponse(response);
                    }
                }
            ]
        );
    }

    /**
     * Description: post-processing after the move element action has responded
     */
    $scope.processMoveElementResponse = function() {
        notify({ message: "Taxonomy element successfully moved.",
            classes: "alert-success",
            duration: 2000,
            templateUrl: $scope.notifyTemplate});
        $scope.moveElement.moveMode = false;
        $scope.moveElement.moveTargetElement = null;
        $scope.readElements();

    }

    /**
     * Description: click through to the appropriate AJS state base on stateParams.clickThroughMode
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId, stateParams) {
        let currentStateObjectId = 0;
        let currentStateClassId = 0;
        if ($scope.selectedElement != null) {
            currentStateObjectId = $scope.selectedElement.objectId;
            currentStateClassId = $scope.selectedElement.classId;
        }

        var currentStateSpec = {
            stateName: "smd.dataintegration_detail",
            objectId: currentStateObjectId,
            classId: currentStateClassId,
            parentObjectId: $scope.invocationParams.parentObjectId,
            parentClassId: $scope.invocationParams.parentClassId,
            stateParams:{
                activeDetailTab: $scope.activeDetailTab
            }
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {stateName: "", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};

        let targetState = "not_defined";
        let newStateParams = {};

        if (stateParams.hasOwnProperty("clickThroughMode")) {
            switch (stateParams.clickThroughMode) {
                case "full_association_view":
                    targetState = "associations.mdobject_fullview";
                    break;

                case "new_association":
                    targetState = "associations.new_association";
                    break;

                case "click_through":
                    let moClass = MoClassService.getMoClassByClassId(classId);
                    targetState = moClass.objectDetailState;
                    break;

                case "lineage":
                    targetState = "associations.lineage";
                    break;

                case "reveng":
                    targetState = "smd.hierarchy_reveng";
                    let selectedConnector = {
                        connectorId: $scope.conCredConfig.connectorId,
                        connectorName: $scope.conCredConfig.connectorName,
                        connectorShortDesc: $scope.conCredConfig.connectorShortDesc
                    };
                    let selectedCredential = {
                        credentialId: $scope.conCredConfig.credentialId,
                        credentialName: $scope.conCredConfig.credentialName,
                        credentialShortDesc: $scope.conCredConfig.credentialShortDesc
                    };

                    //let classDetails = MoClassService.getMoClassByClassId($scope.invocationParams.parentClassId);
                    newStateParams = {
                        objectId: $scope.parentObjectId,
                        objectName: $scope.dataIntegrationSummary.objectName,
                        classId: $scope.dataIntegrationSummary.classId,
                        className: $scope.dataIntegrationSummary.className,
                        selectedConnector: selectedConnector,
                        selectedCredential: selectedCredential,
                        revengConfig: {online: false, offline: true}
                    };
                    break;

                default:
                    targetState = "smd.dataintegration_summary";
                    break;
            }
        }

        invocationParams.stateName = targetState;
        invocationParams.objectId = objectId;
        invocationParams.classId = classId;
        invocationParams.parentObjectId = parentObjectId;
        invocationParams.parentClassId = parentClassId;
        if (newStateParams.hasOwnProperty("objectId"))
            invocationParams.stateParams = newStateParams;

        if (invocationParams.stateName.length > 0) {
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DataIntegrationDetailController', DataIntegrationDetailController);
