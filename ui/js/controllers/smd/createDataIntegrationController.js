/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function CreateDataIntegrationController ($scope, $rootScope, $uibModalInstance, HttpService, MoClassService) {
    $scope.classList = [];

    $scope.dataIntegration = {
        name: "",
        shortDesc: "",
        classId: 0
    };
    $scope.selectedClass = {};
    $scope.validation = {
        name: true,
        class: true
    }
    /**
     * Description: get the list of message format classes
     */
    $scope.initCreateDataIntegration = function () {

        const GET_URL = apiHost + "/data-models/apidefinition/class/list";

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.classList = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: checks that the input form is valid
     */
    $scope.checkValid = function(){
        $scope.validation.name = !!($scope.dataIntegration.name && $scope.dataIntegration.name.length > 0);
        // $scope.validation.class = !!($scope.selectedClass && $scope.selectedClass.classId > 0);

        if($scope.validation.name){
            $scope.create();
        }
    }

    /**
     * Description: invokes the $scope.createDataIntegration function in parent controller
     */
    $scope.create = function () {

        const newObject = {
            objectId: -1,
            objectName: $scope.dataIntegration.name,
            objectShortDesc: $scope.dataIntegration.shortDesc,
            classId: MoClassService.getMoClassIdByCode('DATA_INTEGRATION_PROJECT')
            // TODO: may need to be replaced with a class selection
            // classId: $scope.selectedClass.classId
        };

        $scope.createDataIntegration(newObject);    // this function must be defined in the controller that opens the dialog.
        $uibModalInstance.close();

    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('CreateDataIntegrationController', CreateDataIntegrationController);
