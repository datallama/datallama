/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Format Summary view.
 *
 */
function DataIntegrationSummaryController($scope, $state, $stateParams, $uibModal, notify, DTOptionsBuilder,
                                          NavigationChainService, HttpService, RbacService, MoClassService, UtilityService) {
    $scope.dataIntegrationSummary = {
        objectId: 0,
        classId: 0,
        className: "",
        parentObjectId: 0,
        parentClassId: 0,
        objectName: "",
        objectShortDesc: "",
        objectGUID: ""
    };
    $scope.dataIntegrationSummaryUndo = {};
    $scope.associatedObjects = [];

    $scope.comment = {commentPanelHidden: true, newCommentText: "", commentList: []};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    $scope.editingDisabled = true;
    $scope.associationsDisabled = true;

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Description: initialisation function for the controller
     */
    $scope.initDataIntegrationSummary = function() {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.dataIntegrationSummary.objectId = $scope.invocationParams.objectId;
        $scope.dataIntegrationSummary.classId = $scope.invocationParams.classId;

        $scope.readDataIntegrationSummary();
    }

    /**
     * Reads all xml nodes from the DL database for the xml format identified by $scope.formatId
     */
    $scope.readDataIntegrationSummary = function() {
        $scope.spinnerLoading = "sk-loading";

        const GET_URL = apiHost + '/smd/dataintegration/content/' + $scope.dataIntegrationSummary.objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.dataIntegrationSummary.objectName = response.data.result_data.dataintegration_summary.objectName;
                        $scope.dataIntegrationSummary.objectShortDesc = response.data.result_data.dataintegration_summary.objectShortDesc;
                        $scope.dataIntegrationSummary.className = response.data.result_data.dataintegration_summary.className;
                        $scope.associatedObjects = response.data.result_data.associations;
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: enables fields in selected node display so user can make changes
     * @param dlIboxName
     */
    $scope.dlIboxEdit = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.dataIntegrationSummary, $scope.dataIntegrationSummaryUndo);

            $scope.editingDisabled = false;
        }
        else if (dlIboxName === "associations") {
            $scope.associationsDisabled = false;
        }
    }

    $scope.dlIboxSave = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            $scope.saveDataIntegrationSummaryCoreDetails();
            $scope.editingDisabled = true;
        }
        else if (dlIboxName === "associations") {
            $scope.associationsDisabled = true;
        }
    }

    $scope.dlIboxCancel = function(dlIboxName) {
        if (dlIboxName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.dataIntegrationSummaryUndo, $scope.dataIntegrationSummary);
            $scope.editingDisabled = true;
        }
        else if (dlIboxName === "associations") {
            $scope.associationsDisabled = true;
        }
    }

    /**
     * Description: saves a updated data integration project core details to the DL DB
     */
    $scope.saveDataIntegrationSummaryCoreDetails = function() {
        const dataIntegrationSummaryUpdate = {
            objectId: $scope.dataIntegrationSummary.objectId,
            classId: $scope.dataIntegrationSummary.classId,
            objectName: $scope.dataIntegrationSummary.formatName,
            objectShortDesc: $scope.dataIntegrationSummary.formatShortDesc
        };
        $scope.coreDetailsDisabled = true;

        const POST_URL = apiHost + "/smd/dataintegration/summary";
        HttpService.call(POST_URL, "POST", dataIntegrationSummaryUpdate, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "The data integration project summary was updated successfully.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        let exception = response.data.result_data.exception;
                        let msg = "Exception Class: " + exception.exceptionClass + ". Exception Message: " + exception.exceptionMessage + ".";
                        notify({
                            message: msg,
                            classes: "alert-danger",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description; reads all comments for the selected element
     */
    $scope.readComments = function() {
        if ($scope.selectedXmlElement != null){
            const GET_URL = apiHost + '/comment/' + $scope.selectedXmlElement.objectId + "/" + $scope.selectedXmlElement.classId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.comment.commentList = response.data.result_data;
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: saves a new comment for the currently selected element
     */
    $scope.saveComment = function() {
        if($scope.selectedXmlElement == null) {
            notify({
                message: "Your have not selected an element. Select an element before trying to add a comment.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const newComment = {
                commentId: -1,
                objectId: $scope.selectedXmlElement.objectId,
                classId: $scope.selectedXmlElement.classId,
                commentText: $scope.comment.newCommentText
            }

            const POST_URL = apiHost + "/comment/";

            HttpService.call(POST_URL,"POST",newComment,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // refresh the UI comment list
                            $scope.comment.newCommentText = "";
                            $scope.readElementComments();
                        }
                    }
                ]
            );
        }
        $scope.comment.commentPanelHidden = true;
    }

    /**
     * Description: cancels a new comment action
     */
    $scope.cancelComment = function() {
        $scope.comment.commentPanelHidden = true;
        $scope.comment.newCommentText = "";
    }

    /**
     * Description: checks whether the user has permissions to perform the action identified by actionCategory in
     *  the module identified by module.
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: deletes the format definition after the user confirms the delete action
     */
    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/smd/dataintegration/" + $scope.dataIntegrationSummary.objectId;

        HttpService.call(DELETE_URL, "DELETE", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Data integration project successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.postDeleteAction();
                    }
                }
            ]
        );
    }

    /**
     * Description: redirects to appropriate AJS State after Data Integration project has been deleted
     */
    $scope.postDeleteAction = function() {
        let invocationParams = {stateName: "smd.dataintegration_list", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: creates a new association between the current Data Integration Project and another metadata object
     */
    $scope.newAssociation = function() {

        NavigationChainService.storeCurrentState($scope.invocationParams);

        const invocationParams = {
            stateName:"associations.new_association",
            objectId:$scope.dataIntegrationSummary.objectId,
            classId:$scope.dataIntegrationSummary.classId,
            stateParams:{
                objectClass:$scope.dataIntegrationSummary.className,
                objectContext:$scope.dataIntegrationSummary.formatName
            }
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: transitions to either a format detail view or an associated objects view.
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, transitionMode) {
        // execute the navigation transition
        const currentStateSpec = {
            stateName: "smd.dataintegration_summary",
            objectId: $scope.dataIntegrationSummary.objectId,
            classId: $scope.dataIntegrationSummary.classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let targetState = "";
        let invocationParams = {
            stateName: "",
            objectId: 0,
            classId: 0,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };

        if (transitionMode == "DETAIL") {
            invocationParams.stateName = "smd.dataintegration_detail";
            invocationParams.parentObjectId = $scope.dataIntegrationSummary.objectId;
            invocationParams.parentClassId = $scope.dataIntegrationSummary.classId;
        }
        /* TODO: implement associations for data integration summary
        else if (transitionMode == "ASSOC") {
            let moClass = MoClassService.getMoClassByClassId(classId);
            invocationParams.stateName = moClass.objectDetailState;
            invocationParams.objectId = objectId;
            invocationParams.classId = classId;
            invocationParams.parentObjectId = parentObjectId;
            invocationParams.parentClassId = parentClassId;
        }
        */

        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

    /**
     * Description: returns to previous page viewed
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DataIntegrationSummaryController', DataIntegrationSummaryController);
