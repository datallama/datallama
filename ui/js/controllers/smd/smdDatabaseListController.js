/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main SMD Database grid view
 */
function smdDatabaseListController($scope, $rootScope, $location, $uibModal, $state, NavigationChainService, notify,
                                   DTOptionsBuilder, HttpService, UtilityService) {

    $scope.gridData = [];
    $scope.smdDatabase = {};

    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv', title: 'SMD_Database_List'}
        ]);

    /**
     * Description: initialisation function for the controller
     */
    this.initSmdDatabaseList = function() {
        $scope.refreshGrid();
    }


    /**
     * Description: refreshes the grid that lists all SMD database. Invoked after adding a new SMD database definition
     */
    $scope.refreshGrid = function () {
        const GET_URL = apiHost + '/smd/database/list';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );
    }

    /**
     * Description: Show the Create Diagram dialog
     */
    $scope.openConfigureDatabaseDialog = function () {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/smd/smd_configure_database.html'),
            controller: 'smdConfigureDatabaseController',
            size: 'lg',
            scope: $scope

        });

    };

    /**
     * Description: saves a new SMD Database Configuration to the DL database (table dl_smd_database)
     * @param newSmdDatabaseConfiguration
     */
    $scope.saveNewDatabaseCoreDetails = function (newSmdDatabaseConfiguration) {
        const POST_URL = apiHost + "/smd/database/configure";

        HttpService.call(POST_URL,"POST",newSmdDatabaseConfiguration,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.refreshGrid();

                        notify({
                            message: "Database configuration saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: transitions to the smd_database_detail state and puts current state on the end of the Navigation Chain.
     */
    $scope.clickThrough = function(objectId, classId) {
        const currentStateSpec = {
            stateName:"smd.smd_database_list",
            objectId:0,
            classId:13,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"smd.smd_database_detail",
            objectId: objectId,
            classId: classId,
            parentObjectId: 0,
            parentClassId: 0,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});

    }
}



/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('smdDatabaseListController', smdDatabaseListController);
