/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main datastore detail view
 *  The structure of $scope.dataflowDetails is:
 *      {
 *          core_details: {
 *              connectorId: number, classId: number, connectorName: string, connectorShortDesc: string,
 *              startEntityId: number, startEntityClassId: number, startEntityName: string,
 *              endEntityId: number, endEntityClassId: number, endEntityName: string,
 *              businessContactId: number, businessContactName: string,
 *              technicalContactId: number, technicalContactName: string,
 *              dataflowFullDesc: string
 *          }
 *      }
 */
function DsdfDataflowDetailController($scope, $rootScope, $state, $stateParams, $uibModal, notify,
                                      NavigationChainService, DTOptionsBuilder, RbacService, HttpService,
                                      MoClassService, UtilityService) {

    $scope.dataflowId = 0;
    $scope.dataflowDetails = {};
    $scope.dataflowSummaryUndo = {};
    $scope.gridData02 = [];     // used for diagram list
    $scope.gridData03 = [];     // used for associated SMD database list

    $scope.coreDetailsDisabled = true;
    $scope.editFullDesc = false;

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Data Flow Details'},
            {extend: 'pdf', title: 'Data Flow Details'},
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Description: initialising function for the controller
     */
    $scope.initDsdfDataflowDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('dsdf','create');
        $scope.perms.update = !RbacService.checkRbac('dsdf','update');
        $scope.perms.delete = !RbacService.checkRbac('dsdf','delete');
        $scope.perms.publish = !RbacService.checkRbac('dsdf','publish');

        $scope.classId = 2;
        $scope.associateDisabled = true;
        $scope.invocationParams = $stateParams.invocationParams;

        $scope.dataflowId = $scope.invocationParams.objectId;

        const GET_URL = apiHost + "/dsdf/dataflow/detail/" + $scope.dataflowId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.dataflowDetails = response.data.result_data;
                        $scope.gridData02 = $scope.dataflowDetails.diagrams;
                        $scope.gridData03 = $scope.dataflowDetails.associations;
                        const fullDescReadOnly = document.getElementById("fullDescReadOnly");
                        fullDescReadOnly.innerHTML = $scope.dataflowDetails.core_details.dataflowFullDesc;
                    }
                }
            ]
        );
    }

    /**
     * Description: return to the previous state in the NavigationChain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: initiates the edit action for the dlIbox identified by dlIboxToolsName
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.dataflowDetails.core_details, $scope.dataflowSummaryUndo)
            $scope.coreDetailsDisabled = false;
        }
        else if (dlIboxToolsName === "dataflow_associations") {
            $scope.associateDisabled = false;
        }
        else if (dlIboxToolsName === "full_desc") {
            $scope.editFullDesc = true;
        }
    }

    /**
     * Description: saves changes made in an ibox when the save icon on the dlIboxTools menu is clicked
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            $scope.coreDetailsDisabled = true;
            console.log($scope.dataflowDetails);
            const dataflowDetail = {
                dataflowId: $scope.dataflowDetails.core_details.connectorId,
                classId: $scope.classId,
                dataflowName: $scope.dataflowDetails.core_details.connectorName,
                dataflowShortDesc: $scope.dataflowDetails.core_details.connectorShortDesc,
                businessContact: $scope.dataflowDetails.core_details.businessContactName,
                technicalContact: $scope.dataflowDetails.core_details.technicalContactName,
                endNttId: $scope.dataflowDetails.core_details.endEntityId,
                startNttId: $scope.dataflowDetails.core_details.startEntityId
            };

            // make the REST POST call to save the data store summary info to the database
            const POST_URL = apiHost + "/dsdf/dataflow";

            HttpService.call(POST_URL,"POST",dataflowDetail,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            notify({
                                message: "Data store summary information saved.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );
        }
        else if (dlIboxToolsName === "dataflow_associations") {
            $scope.associateDisabled = true;
        }
        else if (dlIboxToolsName === "full_desc") {
            $scope.editFullDesc = false;

            const dataflowFullDesc = {
                connectorId: $scope.dataflowDetails.core_details.connectorId,
                dataflowFullDesc: $scope.dataflowDetails.core_details.dataflowFullDesc
            }

            // make the REST POST call to save the data store summary info to the database
            const POST_URL = apiHost + "/dsdf/dataflow/fulldesc";

            HttpService.call(POST_URL,"POST",dataflowFullDesc,null,
                [
                    {
                        status:1,
                        callback: function(response){
                            // full description update has succeeded so update $scope.dataflowDetails
                            $scope.dataflowDetails.core_details.dataflowFullDesc = dataflowFullDesc.dataflowFullDesc;
                            const fullDescReadOnly = document.getElementById("fullDescReadOnly");
                            fullDescReadOnly.innerHTML = $scope.dataflowDetails.core_details.dataflowFullDesc;

                            notify({
                                message: "Data flow full description saved.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: discards any changes made in an ibox when the cancel icon is clicked on the dlIboxTools menu
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.dataflowSummaryUndo, $scope.dataflowDetails.core_details);
            $scope.coreDetailsDisabled = true;
        }
        else if (dlIboxToolsName === "dataflow_associations") {
            $scope.associateDisabled = true;
        }
        else if (dlIboxToolsName === "full_desc") {
            $scope.editFullDesc = false;
        }
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/dsdf/dataflow/" + $scope.dataflowId;

        HttpService.call(DELETE_URL,"DELETE",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Data flow successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: transitions to the appropriate state for classId and put current state on the end of the Navigation Chain.
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        NavigationChainService.storeCurrentState($scope.invocationParams);
        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:"Data Flow",
                    objectContext:$scope.dataflowDetails.core_details.connectorName
                }
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

}


/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfDataflowDetailController', DsdfDataflowDetailController);
