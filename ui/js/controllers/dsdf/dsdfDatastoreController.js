/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main datastore grid view
 */
function dsdfDatastoreController($scope, $rootScope, $state, $uibModal, notify,
                                 NavigationChainService, DTOptionsBuilder, HttpService, UtilityService) {
    $scope.gridData = [];
    $scope.datastoreClassList = [];

    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},

            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]);


	/**
	 * Description: initialisation function for the controller
	 */
	this.initDSDFdatastore = function() {

	    // 1. get the list of landscape objects (aka datastores)
        let GET_URL = apiHost + '/dsdf/datastore/list';
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data;
                    }
                }
            ]
        );

        // 2. get the list of defined metadata object classes relating to landscape objects
        GET_URL = apiHost + '/dsdf/datastore/class/list';
        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.datastoreClassList = response.data.result_data;
                    }
                }
            ]
        );

    }

    $scope.showNewDataStoreDialog = function () {

        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_new_datastore.html'),
            controller: 'dsdfNewDataStoreCtrl2',
            size: 'lg',
            scope: $scope
        });

    };

    /**
     * Description:
     * @param landscapeObjectDetails
     */
    $scope.addDataStore = function (landscapeObjectDetails) {
        const POST_URL = apiHost + '/dsdf/datastore/';

        HttpService.call(POST_URL, "POST", landscapeObjectDetails, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        let businessConact = "None Specified";
                        let technicalContact = "None Specified";
                        let shortDesc = "None Specified";
                        if(landscapeObjectDetails.objectShortDesc && landscapeObjectDetails.objectShortDesc.length > 0) {
                            shortDesc = landscapeObjectDetails.objectShortDesc;
                        }
                        const newDataStore = {
                            class_Id: landscapeObjectDetails.classId,
                            class_name: landscapeObjectDetails.className,
                            datastore_id: response.data.result_data.returnId,
                            datastore_name: landscapeObjectDetails.objectName,
                            datastore_shortdesc: shortDesc,
                            business_contact: businessConact,
                            technical_contact: technicalContact
                        };
                        $scope.gridData.push(newDataStore);
                        notify({
                            message: "New landscape object created successfully.",
                            classes: "alert-success",
                            duration: 1000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                },
                {
                    status: -1,
                    callback: function(response) {
                        notify({
                            message: response.data.result_data.errorMessage,
                            classes: "alert-warning",
                            duration: 1000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    };

    /**
     * Stores current state in the NavigationChainService and transitions to the smd.table_view_detail state
     * for entity identified by objectId and classId
     * @param objectId
     * @param classId
     */
    $scope.clickThrough = function (objectId, classId) {

        // TODO: need correct click through from database list controller
        const currentStateSpec = {
            stateName:"dsdf.datastore_summary",
            objectId:0,
            classId:0,
            stateParams:""
        };
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"dsdf.datastore_detail",
            objectId:objectId,
            classId:classId,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

}

/**
 * Description: AJS Controller for the New Datastore modal dialog (views/dsdf/dsdf_new_datastore.html)
 * @param $scope
 * @param $uibModalInstance
 */
function dsdfNewDataStoreCtrl2 ($scope, $uibModalInstance) {
    $scope.newDataStoreDetails =  {};
    $scope.validation = {
        objectType: true,
        objectName: true
    };

    /**
     * Description: create a new data store when user clicks OK.
     */
    $scope.ok = function (result) {
        let valid = true;

        if($scope.newDataStoreDetails.name && $scope.newDataStoreDetails.name.length > 0){
            $scope.validation.objectName = true;
        }
        else{
            $scope.validation.objectName = false;
            valid = false;
        }

        if($scope.newDataStoreDetails.objectType && $scope.newDataStoreDetails.objectType.classId > 0){
            $scope.validation.objectType = true;
        }
        else{
            $scope.validation.objectType = false;
            valid = false;
        }
        if(valid){
            let desc = null;
            if($scope.newDataStoreDetails.description){
                desc = $scope.newDataStoreDetails.description;
            }
            const newLandscapeObject = {
                objectId : -1,
                objectName : $scope.newDataStoreDetails.name,
                classId: $scope.newDataStoreDetails.objectType.classId,
                className: $scope.newDataStoreDetails.objectType.className,
                objectShortDesc : desc
            }
            $scope.addDataStore(newLandscapeObject);
            $uibModalInstance.close();
        }
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}



/** * * * * * * * * * * * * * * * * * * * * * 
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('dsdfDatastoreController', dsdfDatastoreController)
    .controller('dsdfNewDataStoreCtrl2', dsdfNewDataStoreCtrl2)
