/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the PDM Diagrams List view
 */
function DsdfDiagramListController($scope, $rootScope, $state, $uibModal, notify, NavigationChainService,
                                   DTOptionsBuilder, HttpService, UtilityService) {

    $scope.dmDiagramClass = {
        classId: 3,
        className: "Data Store and Data Flow"
    };

    // info entered by user when creating a new diagram
    $scope.newDiagram = {
        diagramId: -1,
        classId: 3,
        diagramName: "",
        diagramShortDesc: "",
        diagramProperties : {
            "colorDefs":[
                {
                    "id":"default",
                    "colorDef":{
                        "bgColor":"#FFFFFF",
                        "iconColor":"#272727"
                    }
                }
            ],
            allEntities:true, customConfig:true
        }
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    /**
     * Description: get the list of diagrams from the DB
     */
    $scope.initDsdfDiagramList = function () {
        $scope.diagramList = [];

        const GET_URL = apiHost + "/dsdf/diagram/list";

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.diagramList = response.data.result_data;
                    }
                }
            ]
        );
    };

    /**
     * Description: Show the Create Diagram dialog
     */
    $scope.createDiagram = function () {
        const modalInstance = $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_create_diagram.html'),
            controller: 'CmnCreateDiagramController',
            size: 'md',
            scope: $scope
        });
    };

    /**
     * Description: Create new PDM Diagram in database
     */
    $scope.createDiagramInDb = function () {
        const POST_URL = apiHost + "/data-models/diagram/create";

        HttpService.call(POST_URL,"POST",$scope.newDiagram,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.clickThrough(response.data.result_data.returnID,3);
                    }
                }
            ]
        );
    };

    /**
     * Description: click through to pdm diagram view
     */
    $scope.clickThrough = function (objectId, classId) {
        const currentStateSpec = {
            stateName:"dsdf.diagrams_list",
            objectId:0,
            classId:3,
            stateParams: {}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"dsdf.diagrams_view",
            objectId:objectId,
            classId:classId,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    };
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfDiagramListController', DsdfDiagramListController);

