/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the report creation view.
 *
 */

function DsdfCreateReportController($scope, $rootScope, $location, $state, $stateParams, NavigationChainService, notify, HttpService) {

    $scope.newReport = {
        reportId: -1,
        reportName: "",
        reportShortDesc: "",
        objectFilter: "",
        columnSet: ""
    }

    $scope.classLhsSelect = [];
    $scope.classRhsSelect = [];

    $scope.columnLhsSelect = [];
    $scope.columnRhsSelect = [];

    $scope.unselectedClasses = [
        {typeName: "Internal Data Store", 				classId: 1},
        {typeName: "External Data Store", 				classId: 21},
        {typeName: "Inbound Data Feed", 				classId: 22},
        {typeName: "Outbound Data Feed", 				classId: 23},
        {typeName: "ETL Platform", 						classId: 24},
        {typeName: "Data Stream Processor", 			classId: 25},
        {typeName: "Data Visualisation application", 	classId: 26},
        {typeName: "Data Reporting application", 		classId: 27}
    ];
    $scope.selectedClasses = [];

    $scope.unselectedColumns = [];
    $scope.selectedColumns = [];

    $scope.validation = {
        name: true,
        classes: true,
        columns: true
    }

    $scope.initDsdfCreateReport = function() {
        //Init stuff goes here but we don't need it yet I guess
    }

    $scope.checkValid = function() {
        $scope.validation.name = !!($scope.newReport.reportName && $scope.newReport.reportName.length > 0);
        $scope.validation.classes = !!($scope.selectedClasses && $scope.selectedClasses.length > 0);
        $scope.validation.columns = !!($scope.selectedColumns && $scope.selectedColumns.length > 0);

        if($scope.validation.name && $scope.validation.classes && $scope.validation.columns){
            const classList = [];
            for(let i = 0; i < $scope.selectedClasses.length; i++){
                classList.push($scope.selectedClasses[i].classId);
            }
            const objectFilter = [{"field":"class_id", "values":classList}];
            $scope.newReport.objectFilter = JSON.stringify(objectFilter);

            const columnSet = [];
            for(let i = 0; i < $scope.selectedColumns.length; i++){
                columnSet.push($scope.selectedColumns[i].columnName);
            }
            $scope.newReport.columnSet = JSON.stringify(columnSet);
            $scope.writeReportToDb($scope.newReport);
        }
    }

    $scope.writeReportToDb = function(reportDefinition) {

        const POST_URL = apiHost + "/dsdf/report";

        HttpService.call(POST_URL,"POST",reportDefinition,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.goToReportView(response.data.result_data.returnId);
                    }
                }
            ]
        );
    }

    $scope.goToReportView = function(reportId){
        const newStateSpec = {
            stateName:"dsdf.report_view",
            objectId:reportId,
            classId:0,
            stateParams:{}
        }
        $state.go(newStateSpec.stateName, {invocationParams: newStateSpec});
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    $scope.selectClasses = function() {
        for(let i = 0; i < $scope.classLhsSelect.length; i++){
            $scope.selectedClasses.push($scope.classLhsSelect[i]);
            for(let j = 0; j < $scope.unselectedClasses.length; j++){
                if($scope.unselectedClasses[j].classId === $scope.classLhsSelect[i].classId){
                    $scope.unselectedClasses.splice(j,1);
                    break;
                }
            }
        }
        $scope.classLhsSelect = [];
        $scope.updateColumns();
    };

    $scope.unselectClasses = function() {
        for(let i = 0; i < $scope.classRhsSelect.length; i++){
            $scope.unselectedClasses.push($scope.classRhsSelect[i]);
            for(let j = 0; j < $scope.selectedClasses.length; j++){
                if($scope.selectedClasses[j].classId === $scope.classRhsSelect[i].classId){
                    $scope.selectedClasses.splice(j,1);
                    break;
                }
            }
        }
        $scope.classRhsSelect = [];
        $scope.updateColumns();
    }

    $scope.selectColumns = function() {
        for(let i = 0; i < $scope.columnLhsSelect.length; i++){
            $scope.selectedColumns.push($scope.columnLhsSelect[i]);
            for(let j = 0; j < $scope.unselectedColumns.length; j++){
                if($scope.unselectedColumns[j].columnId === $scope.columnLhsSelect[i].columnId){
                    $scope.unselectedColumns.splice(j,1);
                    break;
                }
            }
        }
        $scope.columnLhsSelect = [];
    };

    $scope.unselectColumns = function() {
        for(let i = 0; i < $scope.columnRhsSelect.length; i++){
            $scope.unselectedColumns.push($scope.columnRhsSelect[i]);
            for(let j = 0; j < $scope.selectedColumns.length; j++){
                if($scope.selectedColumns[j].columnId === $scope.columnRhsSelect[i].columnId){
                    $scope.selectedColumns.splice(j,1);
                    break;
                }
            }
        }
        $scope.columnRhsSelect = [];
    }

    $scope.updateColumns = function() {
        const classList = [];
        for(let i = 0; i < $scope.selectedClasses.length; i++){
            classList.push($scope.selectedClasses[i].classId);
        }

        const GET_URL = apiHost + "/dsdf/report/columns";

        //This is really a get function but because I have to pass in data I have to use the POST call.
        HttpService.call(GET_URL,"POST",classList,null,
            [
                {
                    status:1,
                    callback: function(response){
                        const columns = response.data.result_data;
                        $scope.selectedColumns = [];
                        $scope.unselectedColumns = [];
                        for(let i = 0; i < columns.length; i++){
                            const column = {columnName: columns[i], columnId: i};
                            $scope.unselectedColumns.push(column);
                        }
                    }
                }
            ]
        );
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfCreateReportController', DsdfCreateReportController);