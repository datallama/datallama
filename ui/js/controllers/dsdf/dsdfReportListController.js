/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the DSDF report List view
 */
function DsdfReportListController($scope, $rootScope, $state, $uibModal, NavigationChainService, DTOptionsBuilder, notify, HttpService) {

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([]);

    /**
     * Description: get the list of diagrams from the DB
     */
    $scope.initDsdfReportList = function () {
        $scope.reportList = [];

        const GET_URL = apiHost + "/dsdf/report/list";

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.reportList = response.data.result_data;
                    }
                }
            ]
        );
    };

    /**
     * Description: click through to dsdf report view
     */
    $scope.clickThrough = function (objectId) {
        const currentStateSpec = {
            stateName:"dsdf.reports_list",
            objectId:0,
            classId:0,
            stateParams: {}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"dsdf.report_view",
            objectId:objectId,
            classId:0,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    };

    /**
     * Description: click through to report create view
     */
    $scope.goToCreateView = function () {
        const currentStateSpec = {
            stateName:"dsdf.reports_list",
            objectId:0,
            classId:0,
            stateParams: {}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"dsdf.create_report",
            objectId:0,
            classId:0,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    };
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfReportListController', DsdfReportListController);

