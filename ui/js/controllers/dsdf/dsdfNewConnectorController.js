/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: modal dialog for adding new connections.
 *
 */
function dsdfNewConnectorController ($scope, $location, $uibModalInstance) {
    $scope.validation = {
        connectorName: true
    };

    /**
     * Description: create a new data store when user clicks OK.
     */
    $scope.ok = function () {
        let valid = true;

        if($scope.newDataflowDetails.dataflowName && $scope.newDataflowDetails.dataflowName.length > 0){
            $scope.validation.connectorName = true;
        }
        else{
            $scope.validation.connectorName = false;
            valid = false;
        }

        if(valid){
            $scope.addDataFlow($scope.newDataflowDetails);
            $uibModalInstance.close();
        }
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('dsdfNewConnectorController', dsdfNewConnectorController);