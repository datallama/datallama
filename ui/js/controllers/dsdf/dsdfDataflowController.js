/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main datastore grid view
 */

function DsdfDataflowController($scope, $rootScope, $location, $state, NavigationChainService, notify, DTOptionsBuilder, HttpService) {

    $scope.gridData = [];

    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},

            {extend: 'print',
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]);


	/**
	 * Description: initialisation function for the controller
	 */
	this.initDSDFdataflow = function() {

        const GET_URL = apiHost + '/dsdf/dataflow/list';

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.gridData = response.data.result_data.dataflow_list;
                    }
                }
            ]
        );
	}

	$scope.showNewDataFlowDialog = function(){
        notify({
            message: "This feature is scheduled for inclusion in the next release of Data Llama",
            classes: "alert",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });
    }

    /**
     * Stores current state in the NavigationChainService and transitions to the smd.table_view_detail state
     * for entity identified by objectId and classId
     * @param objectId
     * @param classId
     */
    $scope.clickThrough = function (objectId, classId) {

        // TODO: need correct click through from database list controller
        const currentStateSpec = {
            stateName:"dsdf.dataflow_summary",
            objectId:0,
            classId:0,
            stateParams:""
        };
        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
            stateName:"dsdf.dataflow_detail",
            objectId:objectId,
            classId:classId,
            stateParams:{}
        };
        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
    }

}



/** * * * * * * * * * * * * * * * * * * * * * 
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfDataflowController', DsdfDataflowController)
