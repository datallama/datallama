/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the DSDF Diagrams view
 */
function dsdfDiagramController($scope, $rootScope, $stateParams, $state, $location, $uibModal, notify,
							   NavigationChainService, DTOptionsBuilder, DiagramService, HttpService,
							   MoClassService, UtilityService) {

	$scope.selectedEntity = null; // used for showing info about entities.
    $scope.entityColorSettings = {};
	$scope.selectedDatastores = [];
	$scope.processorDetailsSidebar = false;
	$scope.processorColorConfigured = false;
	$scope.landscapeObjectList = [];	// used by the Add Landscape Object to Diagram dialog

	$scope.selectedShapes = [];
	$scope.shapeTemplateList = [];
	$scope.palletSidebar = true;
	$scope.currentTemplate = 0;
	$scope.shapeEditingDisabled = true;
	$scope.updatedShapes = [];

	$scope.dirtyCanvas = false;
	$scope.currentDiagram = {
		name: "",
		id: 0,
		properties: {},
		customData: {
			processors: []
		}
	}
	$scope.viewMode = 1;

	$scope.datastoreSummary = {};

	$scope.newDataflowDetails = {
		dataflowId: 0,
		dataflowName: "",
		dataflowShortDesc: "",
		startClassId: 0,
		startNttId: 0,
		endClassId: 0,
		endNttId: 0,
		businessContact: null,
		technicalContact: null
	};
	$scope.tempId = 0;
	$scope.newDiagram = {
		diagramId : -1,
		classId : MoClassService.getMoClassIdByCode("DSDF_DIAGRAM"),
		diagramName : "",
		diagramDesc : "",
		diagramProperties : {
			"colorDefs":[
				{
					"id":"default",
					"colorDef":{
						"bgColor":"#FFFFFF",
						"iconColor":"#272727"
					}
				}
			],
			allEntities:true, customConfig:true
		}
	};

	$scope.addClickThroughStateParams = {
		objectClass:"Diagram Shape",
		objectContext:"",
		assocType: "Click Through",
		changeAssocType: false,
		multiSelect: false
	}

	// TODO: get this from the data base.
	$scope.entityTypeList = [
		{typeName: "Internal Data Store", 				classId: 1},
		{typeName: "External Data Store", 				classId: 21},
		{typeName: "Inbound Data Feed", 				classId: 22},
		{typeName: "Outbound Data Feed", 				classId: 23},
		{typeName: "ETL Platform", 						classId: 24},
		{typeName: "Data Stream Processor", 			classId: 25},
		{typeName: "Data Visualisation application", 	classId: 26},
		{typeName: "Data Reporting application", 		classId: 27}
	];

	$scope.dmDiagramClass = {
		classId: MoClassService.getMoClassIdByCode("DSDF_DIAGRAM"),
		className: "Data Store and Data Flow"
	};

    $scope.notifyTemplate = 'views/common/notify.html';

	$scope.dtOptions = DTOptionsBuilder.newOptions()
		.withDOM('<"html5buttons"B>lTfgitp')
		.withButtons([
			{extend: 'copy'},
			{extend: 'csv'},
			{extend: 'excel', title: 'Data Landscape Objects'}]);

	$scope.dtOptions02 = DTOptionsBuilder.newOptions()
		.withDOM('<"html5buttons"B>lTfgitp')
		.withButtons([]);

	/**
	 * Description: initialising function for the controller. For diagram controllers the init function is called by the Draw2d canvas.
	 */
	$scope.initDiagram = function() {
		$scope.currentDiagram.id = $stateParams.invocationParams.objectId;
		$scope.getShapeTemplates();

		// 1. Configure the Diagram Service
		DiagramService.setSelectedDiagramId($scope.currentDiagram.id);
		DiagramService.setGetDiagramContentURL(apiHost + "/dsdf/diagram/content/");
		DiagramService.setPostDiagramContentURL(apiHost + "/dsdf/diagram/content");
		DiagramService.setCreateDiagramURL(apiHost + "/data-models/diagram/create");
		DiagramService.setDeleteDiagramURL(apiHost + "/data-models/diagram/");
		DiagramService.setGetEntityDetailURL(apiHost + "/dsdf/icon/");
		DiagramService.setLocationAfterClose("/dsdf/dsdf_diagrams_list");
		DiagramService.setNewDiagram($scope.newDiagram);
		DiagramService.setNewEntity($scope.newEntity);
		DiagramService.setNewConnector($scope.newConnector);
		DiagramService.setCanvas($scope.canvas);
		DiagramService.setScope($scope);
		DiagramService.setCustomConfig($scope.config)

		// 2. init scope variables that will be handled by the diagram service
		DiagramService.initScopeVariables();

		// 3. Load the diagram
		DiagramService.readDiagramFromDb();
	}

	$scope.updateGetAllEntitiesURL = function() {
		DiagramService.setGetAllEntitiesURL(apiHost + "/dsdf/diagram/entities/" + $scope.currentDiagram.id);

		//Todo add this to the diagram service and include colour definitions in the PDM diagrams
		if(!$scope.dirtyCanvas){
			$scope.updateColorDefList()
		}
	}

	/**
	 * Description: Applies the color definition for all processors.
	 * TODO: Rename this function to be more meaningful
	 */
	$scope.config = function(){
		for(let i = 0; i < $scope.currentDiagram.customData.processors.length; i++){
			try{
				$scope.currentDiagram.customData.processors[i].colorDef = JSON.parse($scope.currentDiagram.customData.processors[i].colorDef);
			}catch{
				console.log("Error: The processor color definition is invalid");
			}
		}
	}

  $scope.zoomIn = function(){
    DiagramService.zoomIn()
  }
  $scope.zoomOut = function(){
    DiagramService.zoomOut()
  }
  $scope.zoomReset = function(){
    DiagramService.zoomReset()
  }

	$scope.getShapeTemplates = function(){
		const GET_URL = apiHost + '/data-models/diagram/shape-templates';

		HttpService.call(GET_URL, "GET", null, null,
			[
				{
					status: 1,
					callback: function(response){
						$scope.shapeTemplateList = response.data.result_data;
						$scope.shapeTemplateList.unshift({
							height: 480,
							width: 480,
							templateId: -1,
							templateName: "Landscape Object",
							// templateSvg: "M 22.677734 0 C 10.114586 2.3684758e-15 2.3684758e-15 10.114586 0 22.677734 L 0 90.708984 C 2.3684758e-15 103.27213 10.114586 113.38672 22.677734 113.38672 L 128.50391 113.38672 C 141.06706 113.38672 151.18164 103.27213 151.18164 90.708984 L 151.18164 22.677734 C 151.18164 10.114586 141.06706 2.3684758e-15 128.50391 0 L 22.677734 0 z M 22.677734 15.117188 L 128.50391 15.117188 C 132.69162 15.117188 136.0625 18.490018 136.0625 22.677734 L 136.0625 90.708984 C 136.0625 94.896701 132.69162 98.267578 128.50391 98.267578 L 22.677734 98.267578 C 18.490018 98.267578 15.117187 94.896701 15.117188 90.708984 L 15.117188 22.677734 C 15.117188 18.490018 18.490018 15.117187 22.677734 15.117188 z M 30.236328 22.677734 C 26.048612 22.677734 22.677734 26.048609 22.677734 30.236328 L 22.677734 83.150391 C 22.677734 87.338107 26.048612 90.708984 30.236328 90.708984 L 64.251953 90.708984 C 68.43967 90.708984 71.810547 87.338107 71.810547 83.150391 C 71.810547 78.962674 68.43967 75.589844 64.251953 75.589844 L 37.794922 75.589844 L 37.794922 30.236328 C 37.794922 26.048609 34.424045 22.677734 30.236328 22.677734 z M 102.04688 22.677734 C 89.48372 22.677734 79.369141 32.790366 79.369141 45.353516 L 79.369141 68.03125 C 79.369141 80.5944 89.48372 90.708984 102.04688 90.708984 L 105.82617 90.708984 C 118.38932 90.708984 128.50391 80.5944 128.50391 68.03125 L 128.50391 45.353516 C 128.50391 32.790366 118.38932 22.677734 105.82617 22.677734 L 102.04688 22.677734 z M 101.36914 37.794922 L 106.50586 37.794922 C 110.31773 37.794922 113.38672 41.291926 113.38672 45.634766 L 113.38672 67.751953 C 113.38672 72.094793 110.31773 75.589844 106.50586 75.589844 L 101.36914 75.589844 C 97.557273 75.589844 94.488281 72.094793 94.488281 67.751953 L 94.488281 45.634766 C 94.488281 41.291926 97.557273 37.794922 101.36914 37.794922 z "
							templateSvg: "M439.5,50.581h-416c-12.958,0-23.5,10.542-23.5,23.5v229.675c0,0.002,0,0.003,0,0.005v85.159 c0,12.958,10.542,23.5,23.5,23.5h416c12.958, 0,23.5-10.542,23.5-23.5V74.081C463,61.123,452.458,50.581,439.5,50.581z M23.5,65.581 h416c4.687,0,8.5,3.813,8.5,8.5v236.629h-76.103L222.352,161.165c-2.929-2.929-7.678-2.929-10.606,0l-81.407,81.406l-30.826-30.826 c-2.929-2.929-7.678-2.929-10.606,0L15,285.651V74.081C15,69.394,18.813,65.581,23.5,65.581z M439.5,397.419h-416 c-4.687,0-8.5-3.813-8.5-8.5v-82.055l79.209-79.209l30.823,30.823c0.001,0.001,0.001,0.002,0.002,0.002s0.001,0.001,0.002,0.002 l36.127,36.127c2.929,2.929,7.678,2.929,10.606,0c2.929-2.929,2.929-7.678,0-10.606l-30.826-30.826l76.103-76.103l146.438,146.438 c1.406,1.407,3.314,2.197,5.303,2.197H448v63.209C448,393.606,444.187,397.419,439.5,397.419z  M354.338,195.645c20.073,0,36.403-16.331,36.403-36.403s-16.331-36.403-36.403-36.403s-36.403,16.33-36.403,36.403 S334.266,195.645,354.338,195.645z M354.338,137.839c11.802,0,21.403,9.601,21.403,21.403s-9.602,21.403-21.403,21.403 s-21.403-9.602-21.403-21.403S342.537,137.839,354.338,137.839z"
						})
						for(let i = 0; i < $scope.shapeTemplateList.length; i++){
							$scope.shapeTemplateList[i].index = i;
							$scope.shapeTemplateList[i].color = "#9A9A9A";
						}
						$scope.shapeTemplateList[$scope.currentTemplate].color = "#1ab394"; //matches the btn-primary class that is used by the taxonomy arrows.
					}
				}
			]
		);
	}

	$scope.selectTemplate = function(index){
		$scope.shapeTemplateList[$scope.currentTemplate].color = "#9A9A9A";
		$scope.currentTemplate = index;
		$scope.shapeTemplateList[$scope.currentTemplate].color = "#1ab394";
	}

	$scope.updateColorDefList = function(){
		$scope.colorDefList = JSON.parse(JSON.stringify($scope.entityTypeList));
		for(let i = 0; i < $scope.colorDefList.length; i++){
			$scope.colorDefList[i].defined = false;
			$scope.colorDefList[i].bgColor = null;
			$scope.colorDefList[i].iconColor = null;
			$scope.colorDefList[i].sample = null;
		}

		let defaultColorDef = {
			typeName: "Default",
			classId: "default",
			defined: true
		};
		for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++) {
			if($scope.currentDiagram.properties.colorDefs[i].id === "default") {
				defaultColorDef = $.extend(defaultColorDef,JSON.parse(JSON.stringify($scope.currentDiagram.properties.colorDefs[i].colorDef)));
			}
			else{
				for(let j = 0; j < $scope.colorDefList.length; j++){
					if($scope.colorDefList[j].classId.toString() === $scope.currentDiagram.properties.colorDefs[i].id){
						$scope.colorDefList[j].bgColor = $scope.currentDiagram.properties.colorDefs[i].colorDef.bgColor;
						$scope.colorDefList[j].iconColor = $scope.currentDiagram.properties.colorDefs[i].colorDef.iconColor;
						$scope.colorDefList[j].bgSample = {'background-color': $scope.colorDefList[j].bgColor};
						$scope.colorDefList[j].iconSample = {'background-color': $scope.colorDefList[j].iconColor};
						$scope.colorDefList[j].defined = true;
						break;
					}
				}
			}
		}
		defaultColorDef.bgSample = {'background-color': defaultColorDef.bgColor};
		defaultColorDef.iconSample = {'background-color': defaultColorDef.iconColor};
		$scope.colorDefList.unshift(defaultColorDef);
	}

	$scope.newEntity = function(entityDetails){
		if(entityDetails.classId === MoClassService.getMoClassIdByCode("DIAGRAM_SHAPE")){
			return($scope.newShape(entityDetails));
		}
		else{
			return($scope.newDataStore(entityDetails));
		}
	};

	$scope.newDataStore = function(entityDetails){
		//If the entity is using the default color scheme
		if(entityDetails.colorSource > 0){
			let defined = false;
			for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++) {
				const colorDef = $scope.currentDiagram.properties.colorDefs[i];
				if(colorDef.id === entityDetails.classId.toString()){
					entityDetails.colorDefn = JSON.stringify(colorDef.colorDef);
					entityDetails.colorSource = 1;
					defined = true;
					break;
				}
			}
			if(!defined){
				for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++) {
					const colorDef = $scope.currentDiagram.properties.colorDefs[i];
					if(colorDef.id === "default"){
						entityDetails.colorDefn = JSON.stringify(colorDef.colorDef);
						break;
					}
				}
			}
		}

		const newEntity = new DlDiagramEntity(entityDetails);

		const icon = new DlIcon({width:15, height:15, stroke:0, margin:35, iconPath: entityDetails.iconSvgString});
		if(newEntity.colorDefn.iconColor){
			icon.setBackgroundColor(newEntity.colorDefn.iconColor);
		}
		newEntity.add(icon, new DlInsideBottomLeftLocator());

		newEntity.on(
			"contextmenu",
			function(){
				const details = {
					dsID: entityDetails.nttId,
				}
				$scope.showInfoDialog(entityDetails.classId,details);
			}
		);

		return(newEntity);
	}

	$scope.newShape = function(entityDetails){
		let newEntity;
		if(entityDetails.isText){
			newEntity = new DlDiagramText(entityDetails);
			newEntity.installEditor(new draw2d.ui.LabelInplaceEditor({
				//for when updating the text.
				onCommit: $.proxy(function(value){
					const shapeDetails = {
						objectId : entityDetails.nttId,
						name : "",
						shortDesc : null,
						shapeTemplateId: 1, //This is the id for the text template
						isText: true,
						text: value,
						hAlign: entityDetails.hAlign,
						vAlign: entityDetails.vAlign
					}
					$scope.updateShape(shapeDetails);
				},this)
			}));
		}
		else{
			newEntity = new DlDiagramShape(entityDetails);
			newEntity.on(
				"contextmenu",
				function(){
					if($scope.dirtyCanvas){
						const msg = "There are unsaved changes. Please save the diagram before viewing details";
						notify({
							message: msg,
							classes: "alert-warning",
							templateUrl: $scope.notifyTemplate
						});
					}
					else{
						$scope.selectedShape = newEntity;
						$scope.shapeEditingDisabled = true;
						$uibModal.open({
							templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_shape_details.html'),
							controller: 'ontologyShapeDetailsController',
							size: 'lg',
							scope: $scope
						});
					}
				}
			);
		}
		return(newEntity);
	}

	$scope.updateShape = function(shapeDetails){
		const POST_URL = apiHost + '/data-models/shape';
		HttpService.call(POST_URL,"POST",shapeDetails,null,
			[
				{
					status:1,
					callback: function(response){
						const shapeDetails = response.data.result_data;
						if(!shapeDetails.isText){
							$scope.selectedShape.updateText();
						}
						notify({
							message: "Shape details saved to the DB",
							classes: "alert-success",
							templateUrl: $scope.notifyTemplate
						});
					}
				}
			]
		);
	}

	$scope.newConnector = function(connectorDetails){
		if(connectorDetails.classId === MoClassService.getMoClassIdByCode("DIAGRAM_ARROW")){
			return($scope.newArrow(connectorDetails));
		}
		else{
			return($scope.newDataFlow(connectorDetails));
		}
	};

	$scope.newArrow = function(connectorDetails){
		const newConnector = new DlDiagramArrow(connectorDetails)
		newConnector.on(
			"contextmenu",
			function(){
				if($scope.dirtyCanvas){
					const msg = "There are unsaved changes. Please save the diagram before viewing details";
					notify({
						message: msg,
						classes: "alert-warning",
						templateUrl: $scope.notifyTemplate
					});
				}
				else{
					$scope.selectedShape = newConnector;
					$scope.shapeEditingDisabled = true;
					$uibModal.open({
						templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_arrow_details.html'),
						controller: 'ontologyArrowDetailsController',
						size: 'lg',
						scope: $scope
					});
				}
			}
		);
		return(newConnector);
	};

	$scope.newDataFlow = function(connectorDetails){
		const newConnector = new DlDiagramConnector(connectorDetails)
		newConnector.on(
			"contextmenu",
			function(){
				$scope.showConnectorInfoDialog(connectorDetails.cnctId);
			}
		);
		return(newConnector);
	};

	$scope.setProcessorIndicator = function(processor){
		const connectors = $scope.canvas.getLines().data;
		for(let i = 0; i < processor.connectorList.length; i++){
			for(let j = 0; j < connectors.length; j++){
				if(connectors[j].cnctId === processor.connectorList[i]){
					if(processor.show){
						const colorDef = processor.colorDef
						connectors[j].setColor(colorDef["background-color"]);
						connectors[j].setStroke(4);
					}
					else{
						connectors[j].setColor("#999999");
						connectors[j].setStroke(2);
					}

					break;
				}
			}
		}
	}

	$scope.selectProcessorColor = function(processor){
		$scope.formatProcessorId = processor.id;
		$scope.colorSample = processor.colorDef;
		$scope.colorValue = "";
		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_format_processor.html'),
			controller: 'dsdfSelectProcessorColorController',
			size: 'lg',
			scope: $scope
		});
	}

	$scope.formatProcessor = function(color){
		for(let i = 0; i < $scope.currentDiagram.customData.processors.length; i++){
			if($scope.currentDiagram.customData.processors[i].id === $scope.formatProcessorId){
				$scope.currentDiagram.customData.processors[i].colorDef = {"background-color": color};

				const POST_URL = apiHost + '/dsdf/processor/color/' + $scope.formatProcessorId;
				HttpService.call(POST_URL,"POST",JSON.stringify($scope.currentDiagram.customData.processors[i].colorDef),null,
					[
						{
							status:1,
							callback: function(response){
							}
						}
					]
				);

				$scope.formatProcessorId = 0;
				break;
			}
		}
	}

	$scope.newConnectionCreator = function () {
		$scope.tempId -= 1;

		//This is a temporary connector and will be replaced after the
		//data flow has been saved to the database.
		const tempConnector = new draw2d.Connection({id: $scope.tempId});
		tempConnector.on(
			"added",
			function(){
				const sourceClassId = tempConnector.sourcePort.id.split(":")[0];
				const sourceNttId = tempConnector.sourcePort.id.split(":")[1];
				const targetClassId = tempConnector.targetPort.id.split(":")[0];
				const targetNttId = tempConnector.targetPort.id.split(":")[1];
				//If one of the entities being connected is a plain shape then make the connection an arrow
				if(Number(sourceClassId) === MoClassService.getMoClassIdByCode("DIAGRAM_SHAPE") || Number(targetClassId) === MoClassService.getMoClassIdByCode("DIAGRAM_SHAPE")){
					const objectId = -1;
					$scope.newArrowDetails = {
						objectId: objectId,
						name: $scope.currentDiagram.name.concat(": Arrow"),
						shortDesc: null,
						startClassId: sourceClassId,
						startNttId: sourceNttId,
						endClassId: targetClassId,
						endNttId: targetNttId
					};
					$scope.addArrow($scope.newArrowDetails);
					//remove the temporary connector
					$scope.canvas.remove(tempConnector);
				}
				else{
					const dataflowId = -1;
					const sourceName = $scope.canvas.getFigure(sourceClassId+":"+sourceNttId).nttName;
					const targetName = $scope.canvas.getFigure(targetClassId+":"+targetNttId).nttName;

					$scope.newDataflowDetails = {
						dataflowId: dataflowId,
						dataflowName: sourceName+"->"+targetName,
						dataflowShortDesc: "",
						startClassId: sourceClassId,
						startNttId: sourceNttId,
						endClassId: targetClassId,
						endNttId: targetNttId,
						businessContact: null,
						technicalContact: null
					};
					$scope.showNewConnectorDialog();
					//remove the temporary connector
					$scope.canvas.remove(tempConnector);
				}

			}
		);
		return(tempConnector);
	}

	$scope.addArrow = function(arrowDetails){
		const POST_URL = apiHost + '/data-models/arrow';

		HttpService.call(POST_URL,"POST",arrowDetails,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.addArrowSCB(response, arrowDetails);
					}
				}
			]
		);
	}

	$scope.addArrowSCB = function(response, arrowDetails){
		const cnctId = response.data.result_data.returnId;
		const cnctStartNtt = arrowDetails.startClassId + ":" + arrowDetails.startNttId;
		const cnctEndNtt = arrowDetails.endClassId + ":" + arrowDetails.endNttId;

		const connector = {
			cnctId : cnctId,
			classId : MoClassService.getMoClassIdByCode("DIAGRAM_ARROW"),
			cnctName : arrowDetails.name,
			cnctDesc : arrowDetails.shortDesc,
			cnctStartNtt: cnctStartNtt,
			cnctEndNtt: cnctEndNtt,
			cnctVertices: {}
		}
		DiagramService.addConnector(connector);
	}

	$scope.showNewConnectorDialog = function () {

		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_new_dataflow.html'),
			controller: 'dsdfNewConnectorController',
			size: 'lg',
			scope: $scope
		});

	};

	/**
	 * Description: Creates a new landscape object definition based on the user supplied details and then adds
	 * 	the new landscape object to the diagram at point clickX, clickY
	 * @param clickX
	 * @param clickY
	 * @param datastoreDetails
	 */
	$scope.addDataStore = function (clickX, clickY, landscapeObject) {
		const POST_URL = apiHost + '/dsdf/datastore';

		const datastoreDetails = {
			datastoreId: landscapeObject.objectId,
			classId: landscapeObject.classId,
			datastoreName: landscapeObject.objectName,
			datastoreShortDesc: landscapeObject.objectShortDesc
		};

		HttpService.call(POST_URL, "POST", landscapeObject, null,
			[
				{
					status: 1,
					callback: function(response) {
						const newObjectId = response.data.result_data.returnId;
						$scope.getIconString(newObjectId, clickX, clickY, datastoreDetails);
					}
				},
				{
					status: -1,
					callback: function(response) {
						notify({
							message: response.data.result_data.errorMessage,
							classes: "alert-warning",
							templateUrl: $scope.notifyTemplate
						});
					}
				}
			]
		);
	};

	/**
	 * Description: Adds an existing landscape object definition to the diagram at point clickX, clickY
	 * @param clickX
	 * @param clickY
	 * @param landscapeObjectDetails
	 */
	$scope.addExistingLandscapeObject = function (clickX, clickY, landscapeObject) {
		const datastoreDetails = {
			datastoreId: landscapeObject.objectId,
			classId: landscapeObject.classId,
			datastoreName: landscapeObject.objectName,
			datastoreShortDesc: landscapeObject.objectShortDesc
		};
		const GET_URL = apiHost + "/dsdf/datastore/detail/" + landscapeObject.objectId;

		HttpService.call(GET_URL, "GET", null, null,
			[
				{
					status: 1,
					callback: function(response) {
						$scope.addExistingLOprocessResponse(response.data.result_data, clickX, clickY);
					}
				}
			]
		);

	};

	/**
	 * Description:
	 * @param nttId
	 * @param clickX
	 * @param clickY
	 * @param datastoreDetails
	 */
	$scope.getIconString = function(nttId, clickX, clickY, datastoreDetails) {
		const GET_URL = apiHost + "/dsdf/icon/" + nttId;

		HttpService.call(GET_URL, "GET", null, null,
			[
				{
					status: 1,
					callback: function(response) {
						$scope.getIconStringSCB(response.data.result_data, clickX, clickY, nttId, datastoreDetails);
					}
				}
			]
		);
	}

	/**
	 * Description:
	 * @param resultData
	 * @param clickX
	 * @param clickY
	 * @param nttId
	 * @param datastoreDetails
	 */
	$scope.getIconStringSCB = function(resultData, clickX, clickY, nttId, datastoreDetails) {
		let entity = {
			x: clickX,
			y: clickY,
			nttId: nttId,
			classId: datastoreDetails.classId,
			nttName: datastoreDetails.datastoreName,
			nttDesc: datastoreDetails.datastoreShortDesc,
			colorDefn: '{"bgColor":"#FFFFFF"}',
			colorSource: 2
		}
		let description = "No description specified";
		if(datastoreDetails.datastoreShortDesc && datastoreDetails.datastoreShortDesc.length > 0){
			description = datastoreDetails.datastoreShortDesc;
		}
		const landscapeObject = {
			object_id: nttId,
			object_name: datastoreDetails.datastoreName,
			object_shortdesc: description,
			class_id: datastoreDetails.classId,
			class_name: datastoreDetails.className,
			selected: 1,
			selection_change: 1
		}
		DiagramService.currentDiagram.allEntities.push(landscapeObject);
		entity = $.extend(entity, resultData);
		DiagramService.addEntity(entity);
	}

	/**
	 * Description: processes the results from the $scope.addExistingLandscapeObject function
	 * 	NOTE: not currently used.
	 * @param landscapeObject
	 * @param clickX
	 * @param clickY
	 */
	$scope.addExistingLOprocessResponse = function(landscapeObjectDetails, clickX, clickY) {
		const loCoreDetails = landscapeObjectDetails.core_details;
		const loDataflows = landscapeObjectDetails.dataflows;
		let entity = {
			x: clickX,
			y: clickY,
			nttId: loCoreDetails.objectId,
			classId: loCoreDetails.classId,
			nttName: loCoreDetails.objectName,
			nttDesc: loCoreDetails.objectShortDesc,
			colorDefn: '{"bgColor":"#FFFFFF"}',
			colorSource: 2
		}
		let description = "No description specified";
		if(loCoreDetails.objectShortDesc && loCoreDetails.objectShortDesc.length > 0){
			description = loCoreDetails.objectShortDesc;
		}
		const landscapeObjectLegacy = {
			object_id: loCoreDetails.objectId,
			object_name: loCoreDetails.objectName,
			object_shortdesc: description,
			class_id: loCoreDetails.classId,
			class_name: loCoreDetails.className,
			selected: 1,
			selection_change: 1
		}
		DiagramService.currentDiagram.allEntities.push(landscapeObjectLegacy);
		entity = $.extend(entity, loCoreDetails);
		DiagramService.addEntity(entity);

		// add any existing data flows to other landscape objects in the diagram
		for (let i = 0; i < loDataflows.length; i++) {
			const dataflow = loDataflows[i];
			const connectedEntityId =  dataflow.connected_ntt_id;

			for (let j = 0; j < DiagramService.currentDiagram.allEntities.length; j++) {
				const landscapeObject = DiagramService.currentDiagram.allEntities[j];

			}
		}
	}

	/**
	 * Description: Adds a new data flow to the diagram in response to a user action
	 * @param dataflowDetails
	 */
	$scope.addDataFlow = function(dataflowDetails){
		const POST_URL = apiHost + '/dsdf/dataflow';

		HttpService.call(POST_URL, "POST", dataflowDetails, null,
			[
				{
					status: 1,
					callback: function(response){
						$scope.addDataFlowSCB(response, dataflowDetails);
					}
				}
			]
		);
	}

	/**
	 * Description: Adds a data flow definition as a connector to the diagram.
	 * @param response
	 * @param dataflowDetails
	 */
	$scope.addDataFlowSCB = function(response, dataflowDetails){
		const cnctId = response.data.result_data.returnId;
		const cnctStartNtt = dataflowDetails.startClassId + ":" + dataflowDetails.startNttId;
		const cnctEndNtt = dataflowDetails.endClassId + ":" + dataflowDetails.endNttId;

		const connector = {
			cnctId : cnctId,
			classId : MoClassService.getMoClassIdByCode("DATA_FLOW"),
			cnctName : dataflowDetails.dataflowName,
			cnctDesc : dataflowDetails.dataflowShortDesc,
			cnctStartNtt: cnctStartNtt,
			cnctEndNtt: cnctEndNtt,
			cnctVertices: {}
		}
		DiagramService.addConnector(connector);
	}

	/**
	 * Description: returns to the previous view in the navigation chain
	 */
	$scope.goBack = function() {
		if($scope.dirtyCanvas){
			const msg = "There are unsaved changes. Please save or close the diagram before going back";
			notify({
				message: msg,
				classes: "alert-warning",
				templateUrl: $scope.notifyTemplate
			});
		}
		else{
			NavigationChainService.goBack();
		}
	}

	$scope.openShapeFormatter = function(){
		$scope.selectedShapes = $scope.canvas.getSelection().all.data;
		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/ontology/ontology_format_shape.html'),
			controller: 'ontologyFormatShapeController',
			size: 'lg',
			scope: $scope
		});
	}

	$scope.moveShapeFrontBack = function(direction){
		$scope.selectedShapes = $scope.canvas.getSelection().all.data;
		for(let i = 0; i < $scope.selectedShapes.length; i++){
			if(direction === "front"){
				$scope.selectedShapes[i].toFront();
			}
			else if(direction === "back"){
				$scope.selectedShapes[i].toBack();
			}
			$scope.dirtyCanvas = true;
		}
	}

	$scope.groupShapes = function(){
		$scope.canvas.getCommandStack().execute(new draw2d.command.CommandGroup($scope.canvas, $scope.canvas.getSelection()));
		$scope.canvas.setCurrentSelection(null);
		$scope.dirtyCanvas = true;
	}

	$scope.unGroupShapes = function(){
		$scope.canvas.getCommandStack().execute(new draw2d.command.CommandUngroup($scope.canvas, $scope.canvas.getSelection()));
		$scope.canvas.setCurrentSelection(null);
		$scope.dirtyCanvas = true;
	}

	/**
	 * Enables editable input fields for the Ibox identified by dlIboxName
	 * @param dlIboxName
	 */
	$scope.dlIboxParentEdit = function(dlIboxName) {
		if (dlIboxName === "shape") {
			$scope.shapeEditingDisabled = false;
		}
	}

	/**
	 * Saves to the DL DB the changes made in the Ibox identified by dlIboxName
	 * @param dlIboxName
	 */
	$scope.dlIboxParentSave = function(dlIboxName) {
		if (dlIboxName === "shape") {
			$scope.shapeEditingDisabled = true;
			//$scope.saveElementCoreDetails();
		}
	}

	/**
	 * Disables editable input fields for the Ibox identified by dlIboxName
	 * @param dlIboxName
	 */
	$scope.dlIboxParentCancel = function(dlIboxName) {
		if (dlIboxName === "shape") {
			$scope.shapeEditingDisabled = true;
		}
	}

	$scope.saveDsdfDiagram = function(){
		const POST_URL = apiHost + '/data-models/shape/align';

		for(let i = 0; i <$scope.updatedShapes.length; i++){
			const shapeDetails = {
				objectId : $scope.updatedShapes[i].nttId,
				hAlign : $scope.updatedShapes[i].hAlign,
				vAlign : $scope.updatedShapes[i].vAlign
			}
			HttpService.call(POST_URL,"POST",shapeDetails,null,
				[
					{
						status:1,
						callback: function(response){
							if(i === $scope.updatedShapes.length){
								$scope.updatedShapes = [];
							}
						}
					}
				]
			);
		}

		$scope.saveDiagram();
	}

	$scope.addClickThrough = function(){

		const currentStateSpec = $stateParams.invocationParams;

		NavigationChainService.storeCurrentState(currentStateSpec);

		const invocationParams = {
			stateName:"associations.new_association",
			objectId:$scope.selectedShape.nttId,
			classId:$scope.selectedShape.classId,
			parentObjectId:0,
			parentClassId:0,
			stateParams: $scope.addClickThroughStateParams
		};
		$state.go(invocationParams.stateName, {invocationParams:invocationParams});
	}

	$scope.checkClickThrough = function(){
		if($scope.selectedShape.clickThroughMdContext.length > 0){
			$scope.warningDialogSettings = {
				title: "Warning! Click through already defined",
				text:
					"<p>Warning!</p>" +
					"<p>If you change the click through, the current click through will be deleted.</p>" +
					"<p>Do you wish to continue?</p>",
				confirmButton: "Continue",
				cancelButton: "Cancel"
			}
			$uibModal.open({
				templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_warning.html'),
				controller: 'CmnWarningController',
				size: 'md',
				scope: $scope
			});
		}
		else{
			$scope.addClickThrough();
		}
	}

	$scope.warningAccepted = function(){
		const DELETE_URL = apiHost + '/ontology/diagram/click-through-association/'+$scope.selectedShape.nttId+'/'+$scope.selectedShape.classId;

		HttpService.call(DELETE_URL,"DELETE",null,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.addClickThrough();
					}
				}
			]
		);
	}

	$scope.clickThrough = function(){
		const currentStateSpec = $stateParams.invocationParams;

		NavigationChainService.storeCurrentState(currentStateSpec);

		const classId = $scope.selectedShape.clickThroughMdContext[$scope.selectedShape.clickThroughMdContext.length-1].class_id;
		const objectId = $scope.selectedShape.clickThroughMdContext[$scope.selectedShape.clickThroughMdContext.length-1].object_id;
		const moClass = MoClassService.getMoClassByClassId(classId);
		let parentClassId = 0;
		let parentObjectId = 0;

		if($scope.selectedShape.clickThroughMdContext.length>1){
			parentClassId = $scope.selectedShape.clickThroughMdContext[$scope.selectedShape.clickThroughMdContext.length-2].class_id;
			parentObjectId = $scope.selectedShape.clickThroughMdContext[$scope.selectedShape.clickThroughMdContext.length-2].object_id;
		}

		const invocationParams = {
			stateName: moClass.objectDetailState,
			objectId:objectId,
			classId:classId,
			parentObjectId:parentObjectId,
			parentClassId:parentClassId
		}
		$state.go(invocationParams.stateName, {invocationParams:invocationParams});
	}

	/**
	 * Description: handle a right click on a data store. Called from the on("rightclick",...) function of a draw2D shape.
	 */
	$scope.showInfoDialog = function (entityTypeID, entityDetails) {
		// reset state of all edit actions
		$scope.selectedEntity = {entityTypeID: entityTypeID, entityDetails: entityDetails };

		// second: get the persistence technology hierarchy values from the database
		const GET_URL = apiHost + "/dsdf/datastore/summary/" + entityDetails.dsID;

		HttpService.call(GET_URL,"GET",null,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.datastoreSummary = response.data.result_data.core_details;

						$uibModal.open({
							templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_datastore_info_dialog.html'),
							controller: 'dsdfDataStoreDetailsCtrl',
							size: 'lg',
							resolve: { selectedEntity: function () { return $scope.selectedEntity; }},
							scope: $scope
						});
					}
				}
			]
		);
	};

	$scope.showConnectorInfoDialog = function (dataflowId) {

		// second: get the persistence technology hierarchy values from the database
		const GET_URL = apiHost + "/dsdf/dataflow/detail/" + dataflowId;

		HttpService.call(GET_URL,"GET",null,null,
			[
				{
					status:1,
					callback: function(response){
						$scope.dataflowSummary = response.data.result_data.core_details;

						$uibModal.open({
							templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_dataflow_info_dialog.html'),
							controller: 'dsdfDataflowInfoController',
							size: 'lg',
							scope: $scope
						});
					}
				}
			]
		);
	};

	/**
	 * Description: handle a double click on the canvas
	 * @param dblClickDetails
	 */
	$scope.canvasDblClick = function (dblClickDetails) {
		// if the selected diagram object template is a landscape object then open the new landscape object dialog
		if($scope.currentTemplate === 0) {
			$scope.showNewDataStoreDialog(dblClickDetails);
		}
		else {	// else just add the shape to the diagram
			$scope.prepareNewShape(dblClickDetails);
		}

	}

	/**
	 * Description: TBD
	 * @param dblClickDetails
	 */
	$scope.prepareNewShape = function(dblClickDetails) {
		const noSelections = ($scope.canvas.getSelection().all.data.length === 0);
		if(noSelections){
			let shapeDetails;

			// 1. check if the new shape is a text object
			if($scope.currentTemplate === 1) {
				shapeDetails = {
					objectId : -1,
					name : $scope.currentDiagram.name.concat(": Text"),
					shortDesc : null,
					shapeTemplateId: $scope.shapeTemplateList[$scope.currentTemplate].templateId,
					isText: true,
					text: "New text field",
					hAlign: "left",
					vAlign: 25
				}
			}
			else {
				shapeDetails = {
					objectId : -1,
					name : $scope.currentDiagram.name.concat(": Shape"),
					shortDesc : null,
					shapeTemplateId: $scope.shapeTemplateList[$scope.currentTemplate].templateId,
					isText: false,
					text: "",
					hAlign: "center",
					vAlign: 25
				}
			}

			// 2. add the shape to the diagram and the save in the DL DB
			$scope.addShape(dblClickDetails.x, dblClickDetails.y, shapeDetails);
		}
	}

	/**
	 * Description: Adds a shape to a Data flow diagram
	 * @param clickX
	 * @param clickY
	 * @param shapeDetails
	 */
	$scope.addShape = function (clickX, clickY, shapeDetails) {
		const POST_URL = apiHost + '/data-models/shape';

		HttpService.call(POST_URL, "POST", shapeDetails, null,
			[
				{
					status: 1,
					callback: function(response) {
						$scope.addShapeSCB(response.data.result_data, clickX, clickY);
					}
				}
			]
		);
	};

	/**
	 * Description: TBD
	 * @param resultData
	 * @param clickX
	 * @param clickY
	 */
	$scope.addShapeSCB = function(resultData, clickX, clickY){
		let entity = {
			x : clickX-15,
			y : clickY-15,
			nttId : resultData.nttId,
			classId : resultData.classId,
			nttName : resultData.name,
			nttDesc : resultData.shortDesc,
			colorDefn : '{"bgColor":"#FFFFFF","strokeColor":"#000000"}',
			colorSource: 2
		}
		entity = $.extend(entity, resultData);
		entity.height = 30;
		entity.width = 30;
		DiagramService.addEntity(entity);
	}

	/**
	 * Description: Opens the New Data Store modal dialog when a user double clicks the canvas
	 * @param doubleClickDetails
	 */
	$scope.showNewDataStoreDialog = function (doubleClickDetails) {
		const GET_URL = apiHost + '/dsdf/datastore/list';

		HttpService.call(GET_URL, "GET", null, null,
			[
				{
					status: 1,
					callback: function(response) {
						$scope.landscapeObjectList = response.data.result_data;
						$uibModal.open({
							templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_add_lo_to_diagram.html'),
							controller: 'dsdfNewLandscapeObjectCtrl',
							size: 'lg',
							resolve: { items: function () { return doubleClickDetails; }},
							scope: $scope
						});

					}
				}
			]
		);

	};

	/**
	 * Description: opend the datastore formatter modal dialog
	 */
	$scope.openDSformatter = function (classId) {
		if(classId){
			$scope.editClassColorDef = true;
			$scope.classColorDefId = classId;
		}
		else{
			$scope.selectedDatastores = $scope.canvas.getSelection().all.data.slice();
			for(let i = 0; i < $scope.selectedDatastores.length; i++){
				if($scope.selectedDatastores[i].classId === MoClassService.getMoClassIdByCode("DIAGRAM_SHAPE") || $scope.selectedDatastores[i].classId === MoClassService.getMoClassIdByCode("DIAGRAM_ARROW") || $scope.selectedDatastores[i].classId === MoClassService.getMoClassIdByCode("DATA_FLOW")){
					$scope.selectedDatastores.splice(i,1);
					i--;
				}
			}
		}

		$uibModal.open({
			templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_format_datastore.html'),
			controller: 'dsdfFormatDataStoreCtrl',
			size: 'lg',
			scope: $scope
		});

	}

	$scope.updateColors = function(classColorDef) {
		const figures = $scope.canvas.getFigures().data;
		let defaultColorDef = {};
		for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++) {
			const colorDef = $scope.currentDiagram.properties.colorDefs[i];
			if(colorDef.id === "default"){
				defaultColorDef = colorDef;
				break;
			}
		}
		for(let i = 0; i < figures.length; i++){
			const currentFigure = figures[i];
			if (classColorDef && currentFigure.classId.toString() === classColorDef.id && currentFigure.colorSource !== 0){
				currentFigure.setBackgroundColor(classColorDef.colorDef.bgColor);
				currentFigure.setIconColor(classColorDef.colorDef.iconColor);
				currentFigure.colorSource = 1;
			}
			else if (currentFigure.colorSource === 2){
				currentFigure.setBackgroundColor(defaultColorDef.colorDef.bgColor);
				currentFigure.setIconColor(defaultColorDef.colorDef.iconColor);
			}
		}
		for(let i = 0; i < $scope.colorDefList.length; i++){
			const currentDef = $scope.colorDefList[i];
			if(classColorDef && currentDef.classId && (currentDef.classId.toString() === classColorDef.id)){
				currentDef.bgColor = classColorDef.colorDef.bgColor;
				currentDef.iconColor = classColorDef.colorDef.iconColor;
				currentDef.bgSample = {'background-color': currentDef.bgColor};
				currentDef.iconSample = {'background-color': currentDef.iconColor};
				currentDef.defined = true;
			}
		}
		$scope.viewMode = 1;
	}

	$scope.wait = function(callBack) {
		$scope.viewMode = 1;
		setTimeout(function(){callBack()},50)
	}

	$scope.updateSelectedColorDefs = function() {
		const newColorDefs = [];
		for(let i = 0; i < $scope.colorDefList.length; i++){
			const currentColorDef = $scope.colorDefList[i];
			if(currentColorDef.defined || currentColorDef.classId === "default"){
				const newColorDef = {
					id: currentColorDef.classId.toString(),
					colorDef: {
						bgColor: currentColorDef.bgColor,
						iconColor: currentColorDef.iconColor
					}
				}
				newColorDefs.push(newColorDef);
			}
		}
		$scope.currentDiagram.properties.colorDefs = newColorDefs;
		$scope.updateColorDefList();
		const figures = $scope.canvas.getFigures().data;
		for(let i = 0; i < figures.length; i++){
			const currentFigure = figures[i];
			if(currentFigure.colorSource === 1){
				currentFigure.colorSource = 2;
			}
		}
		$scope.updateColors();
		$scope.dirtyCanvas = true;
	}

}


/* * * * * * * * * * * * * * * * * * * * * * *
 * Overlay controllers
 * * * * * * * * * * * * * * * * * * * * * * */

/**
 * Description: Overlay controller - canvas double click - create new data store
 */
function dsdfNewLandscapeObjectCtrl ($scope, $uibModalInstance, items) {
	$scope.selectedLandscapeObject = {objectId: 0, classId: 0, objectName: "None Selected"};

	$scope.newLandscapeObjectDetails =  {};
	$scope.activeTab = {selectExisting: false, createNew: true, activeIndex: 2};
	$scope.validation = {
		objectType: true,
		objectName: true
	};

	/**
	 * Description: initialises the modal dialog by retrieving the list of landscape objects
	 */
	$scope.initNewLandscapeObjectCtrl = function () {
	}

	/**
	 * Description: set the diagramId and diagramName for diagram selected in table
	 */
	$scope.setSelectedLandscapeObject = function (objectId, classId, objectName, objectShortDesc) {
		$scope.selectedLandscapeObject.objectId = objectId;
		$scope.selectedLandscapeObject.classId = classId;
		$scope.selectedLandscapeObject.objectName = objectName;
		$scope.selectedLandscapeObject.objectShortDesc = objectShortDesc;
	}

	/**
	 * Description: sets the active tab programatically
	 * @param activeTabIndex
	 */
	$scope.setActiveTab = function (activeTabIndex) {
		switch (activeTabIndex) {
			case 1:
				$scope.activeTab = {selectExisting:true, createNew:false, activeIndex:1};
				break;
			case 2:
				$scope.activeTab = {selectExisting:false, createNew:true, activeIndex:2};
				break;
		}
	}

	/**
	 * Description: create a new data store when user clicks OK.
	 * @param result
	 */
	$scope.ok = function (result) {
		let valid = true;

		if ($scope.activeTab.selectExisting) {	// the user has selected an existing Landscape Object to add to the diagram
			if ($scope.selectedLandscapeObject.objectId > 0) {
				$scope.validation.objectName = true;
			}
			else {
				$scope.validation.objectName = false;
				valid = false;
			}
			if(valid) {
				$scope.addExistingLandscapeObject(items.x, items.y, $scope.selectedLandscapeObject);
			}
		}
		else {	// the user is creating a new Landscape Object to add to the diagram
			if($scope.newLandscapeObjectDetails.objectName && $scope.newLandscapeObjectDetails.objectName.length > 0){
				$scope.validation.objectName = true;
			}
			else {
				$scope.validation.objectName = false;
				valid = false;
			}

			if($scope.newLandscapeObjectDetails.objectType && $scope.newLandscapeObjectDetails.objectType.classId > 0){
				$scope.validation.objectType = true;
			}
			else{
				$scope.validation.objectType = false;
				valid = false;
			}

			if(valid) {
				let desc = null;
				if($scope.newLandscapeObjectDetails.description){
					desc = $scope.newLandscapeObjectDetails.description;
				}
				const newLandscapeObject = {
					objectId : -1,
					objectName : $scope.newLandscapeObjectDetails.objectName,
					classId: $scope.newLandscapeObjectDetails.objectType.classId,
					className: $scope.newLandscapeObjectDetails.objectType.typeName,
					persistentTechnologyId: 0,
					hostedRealmId: 0,
					businessContact: null,
					technicalContact: null,
					objectShortDesc : desc
				}
				$scope.addDataStore(items.x, items.y, newLandscapeObject);
			}

		}
		if (valid) $uibModalInstance.close();
	};

	/**
     * Description: close the modal when cancel button is clicked
     */
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

}

/**
 * Description: data store right click - show DS information
 */
function dsdfDataStoreDetailsCtrl ($scope, $state, $uibModalInstance, NavigationChainService, selectedEntity) {

	// selectedEntity is passed from function showInfoDialog()
	$scope.selectedEntity = angular.copy(selectedEntity);
	$scope.editstate = {"datastore_type": false, "datastore_name":false, "datastore_desc":false, "hosted_realm":false};

    /**
	 *
     * @param result
     */
	$scope.showDatastoreFullDetails = function (result) {

		$uibModalInstance.close();

        const currentStateSpec = {
            stateName: "dsdf.diagrams_view",
            objectId: $scope.currentDiagram.id,
            classId: 0,
            stateParams: {}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        const invocationParams = {
        	stateName: "dsdf.datastore_detail",
			objectId: $scope.selectedEntity.entityDetails.dsID,
			classId: $scope.selectedEntity.entityTypeID,
			stateParams: {}
        };

        $state.go(invocationParams.stateName, {invocationParams:invocationParams});
	}

	/**
     * Description: close the modal when cancel button is clicked
     */
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

}

/**
 * Description: Overlay controller - menu command Data Store -> Format
 * @param $scope
 * @param $uibModalInstance
 * @param notify
 */
function dsdfFormatDataStoreCtrl ($scope, $uibModalInstance, notify) {

	$scope.elementSelector = "bgColor";
	$scope.singleElement = false;
	$scope.applyAll = false;
	$scope.objectName = "Data Landscape Objects";

	let colorDefinition = {};

	// if only 1 datastore selected, get color definition for it
	if ($scope.selectedDatastores.length === 1) {
        colorDefinition = $scope.selectedDatastores[0].colorDefn;
		$scope.colorValue = colorDefinition.bgColor;
		$scope.singleElement = true;
		$scope.objectName = $scope.selectedDatastores[0].nttName;

		for(let i = 0; i < $scope.entityTypeList.length; i++){
			if($scope.entityTypeList[i].classId === $scope.selectedDatastores[0].classId){
				$scope.className = $scope.entityTypeList[i].typeName;
				break;
			}
		}

		switch($scope.selectedDatastores[0].colorSource){
			case 0:
				$scope.colorSource = "Colour definition is: Object defined";
				break;
			case 1:
				$scope.colorSource = "Colour definition is: Class defined";
				break;
			case 2:
				$scope.colorSource = "Colour definition is: Default";
				break;
		}
    }
	else if($scope.editClassColorDef){
		$scope.objectName = "Class Definition";
		for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++) {
			const currentColorDef = $scope.currentDiagram.properties.colorDefs[i];
			if(currentColorDef.id === $scope.classColorDefId.toString()){
				colorDefinition = currentColorDef.colorDef;
				break;
			}
		}
	}
	else if($scope.selectedDatastores.length === 0) {
		const message = "Please select an entity to format";
		notify({
			message: message,
			classes: "alert-warning",
			duration: 30000,
			templateUrl: $scope.notifyTemplate
		});
		//Todo: find a neater way to do this. Maybe make the check before opening the dialog.
		setTimeout(function(){$uibModalInstance.close();},50);
	}

	$scope.entityColorSettings.bgColor = colorDefinition.bgColor;
	$scope.entityColorSettings.iconColor = colorDefinition.iconColor;

	// $scope.colorSample is used to display a color sample for each element on the color formatter dialog
	$scope.colorSample = {};
	$scope.colorSample.bgColor = {'background-color': $scope.entityColorSettings.bgColor};
	$scope.colorSample.iconColor = {'background-color': $scope.entityColorSettings.iconColor};

	/**
     * Description: set color specifications for all selected data stores.
     */
	$scope.ok = function (result) {
		$scope.wait($scope.processOk);
	};

	$scope.processOk = function() {
		if($scope.applyAll || $scope.editClassColorDef){
			let colorDef = {};
			for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++) {
				const currentColorDef = $scope.currentDiagram.properties.colorDefs[i];
				if(currentColorDef.id === "default"){
					colorDef = JSON.parse(JSON.stringify(currentColorDef.colorDef));
					break;
				}
			}
			if($scope.entityColorSettings.bgColor){
				colorDef.bgColor = $scope.entityColorSettings.bgColor;
			}
			if($scope.entityColorSettings.iconColor){
				colorDef.iconColor = $scope.entityColorSettings.iconColor;
			}

			let id;
			if($scope.editClassColorDef){
				id = $scope.classColorDefId.toString()
			}else{
				id = $scope.selectedDatastores[0].classId.toString();
			}
			const classColorDef = {
				id: id,
				colorDef: colorDef
			}
			//remove the old definition if there is one
			for(let i = 0; i < $scope.currentDiagram.properties.colorDefs.length; i++){
				if($scope.currentDiagram.properties.colorDefs[i].id === classColorDef.id){
					$scope.currentDiagram.properties.colorDefs.splice(i,1);
					//if I know there is only going to ever be one duplicate I can just break
					//but to be safe will go through all of them and check
					i--;
				}
			}
			$scope.currentDiagram.properties.colorDefs.push(classColorDef);
			$scope.updateColors(classColorDef);
		}
		else{
			for (let i = 0; i < $scope.selectedDatastores.length; i++) {
				const currentDS = $scope.selectedDatastores[i];
				if($scope.entityColorSettings.bgColor){
					currentDS.setBackgroundColor($scope.entityColorSettings.bgColor);
					currentDS.setColorSource(0);
				}
				if($scope.entityColorSettings.iconColor){
					currentDS.setIconColor($scope.entityColorSettings.iconColor);
					currentDS.setColorSource(0);
				}
			}
		}

		$scope.$parent.dirtyCanvas = true;
		$scope.$parent.editClassColorDef = false;
		$uibModalInstance.close();
	}

	/**
     * Description: close the modal when cancel button is clicked
     */
	$scope.cancel = function () {
		$scope.$parent.editClassColorDef = false;
		$uibModalInstance.dismiss('cancel');
	};


	/**
	 * Description: set colors based on user selection with radio button
	 */
	$scope.selectElement = function () {
		if ($scope.elementSelector === 'bgColor') {
			$scope.colorValue = $scope.entityColorSettings.bgColor;
		} else {
			if ($scope.elementSelector === 'iconColor') {
                $scope.colorValue = $scope.entityColorSettings.iconColor;
            }
		}
	}


	/**
	 * Description: set colors based on user selection with radio button
	 */
	$scope.syncColorSelection = function () {
		if ($scope.elementSelector === 'bgColor') {
			$scope.entityColorSettings.bgColor = $scope.colorValue;
			$scope.colorSample.bgColor = {'background-color': $scope.entityColorSettings.bgColor};
		} else {
			if ($scope.elementSelector === 'iconColor') {
                $scope.entityColorSettings.iconColor = $scope.colorValue;
				$scope.colorSample.iconColor = {'background-color': $scope.entityColorSettings.iconColor};
			}
		}
	}

}

/**
 * Description: Overlay controller - menu command Data Store -> Format
 */
function dsdfSelectProcessorColorController ($scope, $uibModalInstance) {

	$scope.syncColorSelection = function () {
		$scope.colorSample = {'background-color': $scope.colorValue};
	}

	$scope.ok = function () {
		$scope.formatProcessor($scope.colorValue);
		$uibModalInstance.close();
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};


}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('dsdfDiagramController', dsdfDiagramController)
    .controller('dsdfNewLandscapeObjectCtrl', dsdfNewLandscapeObjectCtrl)
    .controller('dsdfDataStoreDetailsCtrl', dsdfDataStoreDetailsCtrl)
    .controller('dsdfFormatDataStoreCtrl', dsdfFormatDataStoreCtrl)
	.controller('dsdfSelectProcessorColorController', dsdfSelectProcessorColorController);
