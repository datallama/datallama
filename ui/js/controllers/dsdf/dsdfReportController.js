/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the report view.
 *
 */

function DsdfReportController($scope, $rootScope, $location, $state, $stateParams, $uibModal, notify,
                              NavigationChainService, RbacService, HttpService, UtilityService) {

    $scope.repActiveTab = {
        content: true,
        details: false
    };

    $scope.editingDisabled = {
        core: true,
        filters: true,
        columns: true
    }

    $scope.report = {
        reportId: -1,
        reportName: "",
        reportShortDesc: "",
        objectFilter: "",
        columnSet: ""
    }

    $scope.currentName = "";
    $scope.currentShortDesc = "";

    $scope.select = {
        classLhs:[],
        classRhs:[],
        columnLhs:[],
        columnRhs:[]
    }

    $scope.unselectedClasses = [
        {typeName: "Internal Data Store", 				classId: 1},
        {typeName: "External Data Store", 				classId: 21},
        {typeName: "Inbound Data Feed", 				classId: 22},
        {typeName: "Outbound Data Feed", 				classId: 23},
        {typeName: "ETL Platform", 						classId: 24},
        {typeName: "Data Stream Processor", 			classId: 25},
        {typeName: "Data Visualisation application", 	classId: 26},
        {typeName: "Data Reporting application", 		classId: 27}
    ];
    $scope.selectedClasses = [];
    $scope.unselectedColumns = [];
    $scope.selectedColumns = [];

    $scope.currentUnselectedClasses = [];
    $scope.currentSelectedClasses = [];
    $scope.currentUnselectedColumns = [];
    $scope.currentSelectedColumns = [];

    $scope.validation = {
        name: true,
        classes: true,
        columns: true
    }

    $scope.initDsdfReportDetail = function() {
        $scope.selectedReportId = $stateParams.invocationParams.objectId;

        const GET_URL = apiHost + "/dsdf/report/content/" + $scope.selectedReportId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        //The report data is an array of objects each
                        //with IDENTICAL fields but unique values.
                        $scope.reportData = response.data.result_data.reportData;
                        $scope.report.reportId = $scope.selectedReportId;
                        $scope.report.reportName = response.data.result_data.reportName;
                        $scope.report.reportShortDesc = response.data.result_data.reportShortDesc;
                        $scope.columns = response.data.result_data.selectedColumns;
                        $scope.filters = response.data.result_data.selectedFilters;
                        $scope.reportColumns = [];

                        $scope.initSelections();

                        //Configure the data tables columns array.
                        if($scope.reportData.length>0){
                            for(const column in $scope.reportData[0]){
                                if($scope.reportData[0].hasOwnProperty(column)){
                                    const newColumn = {title: column, data: column};
                                    $scope.reportColumns.push(newColumn);
                                }
                            }

                            //display the data table
                            $('#report').DataTable( {
                                destroy: true,
                                data: $scope.reportData,
                                columns: $scope.reportColumns
                            } );
                        }
                        else{
                            notify({
                                message: "This report seems to be empty. Something may have gone wrong. Click Go Back to return to the list view.",
                                classes: "alert-warning",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                        }
                    }
                },
                {
                    status:-11,
                    callback: function(response){
                        notify({
                            message: response.data.result_data.message,
                            classes: "alert-warning",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        console.log(response.data.result_data.exception)
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.initSelections = function() {
        let classes = [];
        for(let i = 0; i < $scope.filters.length; i++){
            if($scope.filters[i].field === "class_id"){
                classes = $scope.filters[i].values;
                break;
            }
        }
        for(let i = 0; i < $scope.unselectedClasses.length; i++){
            for(let j = 0; j < classes.length; j++){
                if($scope.unselectedClasses[i].classId === classes[j]){
                    $scope.selectedClasses.push($scope.unselectedClasses[i]);
                    $scope.unselectedClasses.splice(i,1);
                }
            }
        }
        $scope.updateColumns();
    }

    $scope.setActiveTab = function(activeTabIndex){
        switch (activeTabIndex) {
            case 1:
                $state.go($state.current, {}, {reload: true});
                break;
            case 2:
                $scope.tedActiveTab = {content: false, details: true};
                break;
        }
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    $scope.selectClasses = function() {
        for(let i = 0; i < $scope.select.classLhs.length; i++){
            $scope.selectedClasses.push($scope.select.classLhs[i]);
            for(let j = 0; j < $scope.unselectedClasses.length; j++){
                if($scope.unselectedClasses[j].classId === $scope.select.classLhs[i].classId){
                    $scope.unselectedClasses.splice(j,1);
                    break;
                }
            }
        }
        $scope.select.classLhs = [];
        $scope.updateColumns();
    };

    $scope.unselectClasses = function() {
        for(let i = 0; i < $scope.select.classRhs.length; i++){
            $scope.unselectedClasses.push($scope.select.classRhs[i]);
            for(let j = 0; j < $scope.selectedClasses.length; j++){
                if($scope.selectedClasses[j].classId === $scope.select.classRhs[i].classId){
                    $scope.selectedClasses.splice(j,1);
                    break;
                }
            }
        }
        $scope.select.classRhs = [];
        $scope.updateColumns();
    }

    $scope.selectColumns = function() {
        for(let i = 0; i < $scope.select.columnLhs.length; i++){
            $scope.selectedColumns.push($scope.select.columnLhs[i]);
            for(let j = 0; j < $scope.unselectedColumns.length; j++){
                if($scope.unselectedColumns[j].columnId === $scope.select.columnLhs[i].columnId){
                    $scope.unselectedColumns.splice(j,1);
                    break;
                }
            }
        }
        $scope.columns = [];
        for(let i = 0; i < $scope.selectedColumns.length; i++){
            $scope.columns.push($scope.selectedColumns[i].columnName);
        }
        $scope.select.columnLhs = [];
    };

    $scope.unselectColumns = function() {
        for(let i = 0; i < $scope.select.columnRhs.length; i++){
            $scope.unselectedColumns.push($scope.select.columnRhs[i]);
            for(let j = 0; j < $scope.selectedColumns.length; j++){
                if($scope.selectedColumns[j].columnId === $scope.select.columnRhs[i].columnId){
                    $scope.selectedColumns.splice(j,1);
                    break;
                }
            }
        }
        $scope.columns = [];
        for(let i = 0; i < $scope.selectedColumns.length; i++){
            $scope.columns.push($scope.selectedColumns[i].columnName);
        }
        $scope.select.columnRhs = [];
    }

    $scope.updateColumns = function() {
        const classList = [];
        for(let i = 0; i < $scope.selectedClasses.length; i++){
            classList.push($scope.selectedClasses[i].classId);
        }

        const GET_URL = apiHost + "/dsdf/report/columns";

        HttpService.call(GET_URL,"POST",classList,null,
            [
                {
                    status:1,
                    callback: function(response){
                        const columns = response.data.result_data;
                        $scope.selectedColumns = [];
                        $scope.unselectedColumns = [];
                        for(let i = 0; i < columns.length; i++){
                            const column = {columnName: columns[i], columnId: i};
                            $scope.unselectedColumns.push(column);
                        }
                        if($scope.columns && $scope.columns.length>0){
                            for(let i = 0; i < $scope.columns.length; i++){
                                for(let j = 0; j < $scope.unselectedColumns.length; j++){
                                    const currentColumn = $scope.unselectedColumns[j]
                                    if(currentColumn.columnName === $scope.columns[i]){
                                        $scope.selectedColumns.push(currentColumn);
                                        $scope.unselectedColumns.splice(j,1);
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        );
    }

    $scope.dlIboxParentEdit = function(iboxName) {
        switch (iboxName) {
            case "core_details":
                $scope.editingDisabled.core = false;
                $scope.currentName = $scope.report.reportName;
                $scope.currentShortDesc = $scope.report.reportShortDesc;
                break;
            case "report_filters":
                $scope.editingDisabled.filters = false;
                $scope.currentSelectedClasses = [];
                $scope.currentUnselectedClasses = [];
                $scope.currentUnselectedClasses.push(...$scope.unselectedClasses);
                $scope.currentSelectedClasses.push(...$scope.selectedClasses);
                break;
            case "report_columns":
                $scope.editingDisabled.columns = false;
                $scope.currentSelectedColumns = [];
                $scope.currentSelectedColumns = [];
                $scope.currentUnselectedColumns.push(...$scope.unselectedColumns);
                $scope.currentSelectedColumns.push(...$scope.selectedColumns);
                break;
        }
    }

    $scope.dlIboxParentSave = function(iboxName) {

        let newReport = {
            reportId: 0,
            reportName: null,
            reportShortDesc: null,
            objectFilter: null,
            columnSet: null
        }

        switch (iboxName) {
            case "core_details":
                $scope.validation.name = !!($scope.report.reportName && $scope.report.reportName.length > 0);
                if($scope.validation.name){
                    $scope.editingDisabled.core = true;
                    newReport.reportId = $scope.report.reportId;
                    newReport.reportName = $scope.report.reportName;
                    newReport.reportShortDesc = $scope.report.reportShortDesc;
                    console.log("test name")
                }
                break;
            case "report_filters":
                $scope.validation.classes = !!($scope.selectedClasses && $scope.selectedClasses.length > 0);
                if($scope.validation.classes){
                    $scope.editingDisabled.filters = true;
                    const classList = [];
                    for(let i = 0; i < $scope.selectedClasses.length; i++){
                        classList.push($scope.selectedClasses[i].classId);
                    }
                    const objectFilter = [{"field":"class_id", "values":classList}];
                    newReport.reportId = $scope.report.reportId;
                    newReport.objectFilter = JSON.stringify(objectFilter);
                }
                break;
            case "report_columns":
                $scope.validation.columns = !!($scope.selectedColumns && $scope.selectedColumns.length > 0);
                if($scope.validation.columns){
                    $scope.editingDisabled.columns = true;
                    const columnSet = [];
                    for(let i = 0; i < $scope.selectedColumns.length; i++){
                        columnSet.push($scope.selectedColumns[i].columnName);
                    }
                    newReport.reportId = $scope.report.reportId;
                    newReport.columnSet = JSON.stringify(columnSet);
                }
                break;
        }
        if(newReport.reportId > 0){
            $scope.saveReportUpdates(newReport);
        }
    }

    $scope.dlIboxParentCancel = function(iboxName) {

        switch (iboxName) {
            case "core_details":
                $scope.editingDisabled.core = true;
                $scope.report.reportName = $scope.currentName;
                $scope.report.reportShortDesc = $scope.currentShortDesc;
                break;
            case "report_filters":
                $scope.editingDisabled.filters = true;
                $scope.unselectedClasses = [];
                $scope.selectedClasses = [];
                $scope.unselectedClasses.push(...$scope.currentUnselectedClasses);
                $scope.selectedClasses.push(...$scope.currentSelectedClasses);
                break;
            case "report_columns":
                $scope.editingDisabled.columns = true;
                $scope.unselectedColumns = [];
                $scope.selectedColumns = [];
                $scope.unselectedColumns.push(...$scope.currentUnselectedColumns);
                $scope.selectedColumns.push(...$scope.currentSelectedColumns);
                break;
        }
    }

    $scope.saveReportUpdates = function(newReport){
        const POST_URL = apiHost + "/dsdf/report/update";

        HttpService.call(POST_URL,"POST",newReport,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Report updates have been saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    /**
     * Description: lets the user delete a txonomy element
     */
    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/dsdf/report/" + $scope.report.reportId;

        HttpService.call(DELETE_URL,"DELETE",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Report successfully deleted",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    $scope.goBack = function() {
        NavigationChainService.goBack();
    }
}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfReportController', DsdfReportController);