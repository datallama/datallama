/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the main datastore detail view
 *  The structure of $scope.datastoreDetails is:
 *      {
 *          core_details: {classId:0, objectId:0, parentClassId:0, parentObjectId:0, objectName:"", className:"",
 *              persistentTechnologyId:0, hostedRealmId:0, customFormId:0, businessContact:"", technicalContact:"",
 *              objectShortDesc:"", customForm: {
 *                  customFormId:0, customFormName:"", schemaDefinition:"", formDefinition:"", formData:"",
 *                  classId:0, objectId:0, parentClassId:0, parentObjectId:0}
 *              },
 *          dataflows: [ {flow_direction_id:0, flow_direction_name:"", dataflow_id:0, class_id:0,
 *              dataflow_name:"", connected_ntt_id:0, connected_ntt_name:""} ],
 *          diagrams: [ {dgrm_id:0, class_id:0, class_name:"", dgrm_name:"", dgrm_desc: ""} ],
 *          associations: []
 *      }
 */
function DsdfDatastoreDetailController($scope, $rootScope, $location, $state, $stateParams, $uibModal, notify,
                                       NavigationChainService, DTOptionsBuilder, RbacService, HttpService,
                                       MoClassService, UtilityService) {

    $scope.datastoreId = 0;
    $scope.classId = 0;
    $scope.className = "";
    $scope.datastoreDetails = {core_details: {}, associations: [], diagrams: [], dataflows: []};
    $scope.datastoreCoreDetailsUndo = {};
    $scope.customForm = {};
    $scope.persistenceTechnologyHierarchy = [];
    $scope.hostedRealmHierarchy = [];
    $scope.editPersistenceTechnologyId = 0;
    $scope.editPersistenceTechnologyName = "";
    $scope.editHostedRealmId = 0;
    $scope.editHostedRealmName = "";
    $scope.selectedSMDdatabase = {};
    $scope.associatedSMDdatabaseUndo = [];
    $scope.associatedDBedits = [];
    $scope.dlDsTags = [];

    $scope.coreDetailsDisabled = true;
    $scope.databasesDisabled = true;

    $scope.gridData01 = [];     // used for dataflow list
    $scope.gridData02 = [];     // used for diagram list
    $scope.gridData03 = [];     // used for associated SMD database list

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
        ]);

    $scope.perms = {create: false, update: false, delete: false, publish: false};
    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';


    /**
     * Description: initialising function for the controller. Read details of the datastore from DL database
     */
    $scope.initDsdfDatastoreDetail = function() {
        $scope.perms.create = !RbacService.checkRbac('dsdf','create');
        $scope.perms.update = !RbacService.checkRbac('dsdf','update');
        $scope.perms.delete = !RbacService.checkRbac('dsdf','delete');
        $scope.perms.publish = !RbacService.checkRbac('dsdf','publish');

        $scope.invocationParams = $stateParams.invocationParams;
        $scope.classId = $scope.invocationParams.classId;
        $scope.datastoreId = $scope.invocationParams.objectId;

        const GET_URL = apiHost + "/dsdf/datastore/detail/" + $scope.datastoreId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        // capture core details
                        $scope.datastoreDetails.core_details.classId = response.data.result_data.core_details.classId;
                        $scope.datastoreDetails.core_details.objectId = response.data.result_data.core_details.objectId;
                        $scope.datastoreDetails.core_details.parentClassId = response.data.result_data.core_details.parentClassId;
                        $scope.datastoreDetails.core_details.parentObjectId = response.data.result_data.core_details.parentObjectId;
                        $scope.datastoreDetails.core_details.objectName = response.data.result_data.core_details.objectName;
                        $scope.datastoreDetails.core_details.objectShortDesc = response.data.result_data.core_details.objectShortDesc;
                        $scope.datastoreDetails.core_details.className = response.data.result_data.core_details.className;
                        $scope.datastoreDetails.core_details.hostedRealmId = response.data.result_data.core_details.hostedRealmId;
                        $scope.datastoreDetails.core_details.hostedRealName = "None Defined"; // response.data.result_data.core_details.hostedRealmName;
                        $scope.datastoreDetails.core_details.businessContact = response.data.result_data.core_details.businessContact;
                        $scope.datastoreDetails.core_details.technicalContact = response.data.result_data.core_details.technicalContact;
                        $scope.datastoreDetails.associations = response.data.result_data.associations;
                        $scope.datastoreDetails.dataflows = response.data.result_data.dataflows;
                        $scope.datastoreDetails.diagrams = response.data.result_data.diagrams;

                        // capture custom form TODO: update to handle schema definition enum associations
                        $scope.customForm.customFormId = response.data.result_data.core_details.customForm.customFormId;
                        $scope.customForm.customFormName = response.data.result_data.core_details.customForm.customFormName;
                        $scope.customForm.schemaDefinition = JSON.parse(response.data.result_data.core_details.customForm.schemaDefinition);
                        $scope.customForm.formDefinition = JSON.parse(response.data.result_data.core_details.customForm.formDefinition);
                        if (response.data.result_data.core_details.customForm.formData == null)
                            $scope.customForm.formData = {};
                        else
                            $scope.customForm.formData = JSON.parse(response.data.result_data.core_details.customForm.formData);
                        $scope.disableCustomFormFields();

                        $scope.gridData01 = $scope.datastoreDetails.dataflows;
                        $scope.gridData02 = $scope.datastoreDetails.diagrams;
                        $scope.gridData03 = $scope.datastoreDetails.associations;
                    }
                }
            ]
        );
    }

    /**
     * Description: return to the previous state in the NavigationChain
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: initiate the edit action for the dlIbox identified by dlIboxToolsName
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // capture all field values for undo of edit
            angular.copy($scope.datastoreDetails.core_details, $scope.datastoreCoreDetailsUndo)
            $scope.coreDetailsDisabled = false;
        }
        else if (dlIboxToolsName === "datastore_databases") {
            $scope.databasesDisabled = false;
        }
        else if (dlIboxToolsName === "custom_form") {
            $scope.enableCustomFormFields();
        }
    }

    /**
     * Description: save all edits made in the dlIbox identified by dlIboxToolsName
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        let saveResult = 1;
        if (dlIboxToolsName === "core_details") {
            $scope.saveCoreDetails();
        }
        else if (dlIboxToolsName === "datastore_databases") {
            $scope.databasesDisabled = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            saveResult = $scope.saveCustomFormData();
        }
        return saveResult;
    }

    /**
     * Description: cancel the edit action for the dlIbox identified by dlIboxToolsName
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "core_details") {
            // reset all fields to the pre-edit state
            angular.copy($scope.datastoreCoreDetailsUndo, $scope.datastoreDetails.core_details);
            $scope.coreDetailsDisabled = true;
        }
        else if (dlIboxToolsName === "datastore_databases") {
            $scope.databasesDisabled = true;
        }
        else if (dlIboxToolsName === "custom_form") {
            $scope.disableCustomFormFields();
        }

    }

    /**
     * Description: Saves to the DL DB changes made to the core details of the datastore.
     */
    $scope.saveCoreDetails = function() {
        $scope.coreDetailsDisabled = true;
        const datastoreSummary = {
            classId: $scope.datastoreDetails.core_details.classId,
            objectId: $scope.datastoreDetails.core_details.objectId,
            parentClassId: $scope.datastoreDetails.core_details.parentClassId,
            parentObjectId: $scope.datastoreDetails.core_details.parentObjectId,
            objectName: $scope.datastoreDetails.core_details.objectName,
            objectShortDesc: $scope.datastoreDetails.core_details.objectShortDesc,
            persistentTechnologyId: $scope.datastoreDetails.core_details.persistentTechnologyId,
            hostedRealmId: $scope.datastoreDetails.core_details.hostedRealmId,
            businessContact: $scope.datastoreDetails.core_details.businessContact,
            technicalContact: $scope.datastoreDetails.core_details.technicalContact
        };

        // make the REST POST call to save the data store summary info to the database
        const POST_URL = apiHost + "/dsdf/datastore";

        HttpService.call(POST_URL, "POST", datastoreSummary, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        notify({
                            message: "Data store summary information saved.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                    }
                }
            ]
        );
    }

    /**
     * Description: saves the custom form data for select element
     */
    $scope.saveCustomFormData = function() {
        let validationResult = 1;

        // 1. validate the custom form
        $scope.$broadcast('schemaFormValidate');

        // Then we check if the form is valid
        if ($scope.customForm.$valid) {
            const updatedCustomFormData = {
                classId: $scope.classId,
                objectId: $scope.datastoreDetails.core_details.objectId,
                parentClassId: 0,   // landscape objects do not have a parent
                parentObjectId: 0,
                customFormId: $scope.customForm.customFormId,
                formData: JSON.stringify($scope.customForm.formData)
            };

            // make the REST POST call to save the data store summary info to the database
            const POST_URL = apiHost + "/common/customform/data";

            HttpService.call(POST_URL, "POST", updatedCustomFormData, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            notify({
                                message: "Updates to custom form saved.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.disableCustomFormFields();
                        }
                    }
                ]
            );
        } else {
            validationResult = -1;
        }

        return validationResult;
    }

    /**
     * Description: displays the overlay dialog for selecting a persistence technology for the selected data store
     */
    $scope.showPersistenceTechnologyDialog = function() {

        // first: get the persistence technology hierarchy
        const GET_URL = apiHost + "/dsdf/datastore/persistence-technology/list";

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.persistenceTechnologyHierarchy = response.data.result_data;
                    }
                }
            ]
        );

        // second: set the edit persistenceTechnology values to current values
        $scope.editPersistenceTechnologyId = $scope.datastoreDetails.core_details.pt_id;
        $scope.editPersistenceTechnologyName = $scope.datastoreDetails.core_details.pt_name;

        // third: open the dialog
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_persistence_technology_dialog.html'),
            controller: 'DsdfPersistenceTechnologyOverlayController',
            size: 'lg',
            resolve: { selectedEntity: function () { return $scope.selectedEntity; }},
            scope: $scope
        });

    }


    /**
     * Description: displays the overlay dialog for selecting a hosted realm for the selected data store
     */
    $scope.showHostedRealmDialog = function() {

        // first: get the hosted realm hierarchy
        const GET_URL = apiHost + "/dsdf/datastore/hosted-realm/list";

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.hostedRealmHierarchy = response.data.result_data;
                    }
                }
            ]
        );

        // second: set the edit persistenceTechnology values to current values
        $scope.editHostedRealmId = $scope.datastoreDetails.core_details.hr_id;
        $scope.editHostedRealmName = $scope.datastoreDetails.core_details.hr_name;

        // third: open the dialog
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dsdf/dsdf_hosted_realm_dialog.html'),
            controller: 'DsdfHostedRealmOverlayController',
            size: 'lg',
            resolve: { selectedEntity: function () { return $scope.selectedEntity; }},
            scope: $scope
        });

    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/dsdf/datastore/" + $scope.datastoreId;

        HttpService.call(DELETE_URL,"DELETE",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Landscape Object successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    /**
     * Description: Checks whether the logged in user has permissions to perform an action
     * @param module
     * @param actionCategory
     * @returns {*}
     */
    $scope.checkRbac = function(module, actionCategory) {
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    /**
     * Description: collapses all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.collapseAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);   // $('#myid')
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideUp(200);

            icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
            ibox.addClass('border-bottom');
        }
    };

    /**
     * Description: expands all dlIbox areas defined on the view. The dlIbox DIV must have a unique elementId
     */
    $scope.expandAllDlIbox = function () {
        const allDlIbox = $('div.ibox');

        for (let i = 0; i < allDlIbox.length; i++) {
            const jqSelector = "#" + allDlIbox[i].id;

            const ibox = $(jqSelector);
            const icon = ibox.find('i:first');
            const content = ibox.children('.ibox-content');
            content.slideDown(200);

            icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
            ibox.removeClass('border-bottom');
        }
    };

    /**
     * Description: disables all fields in the custom form
     */
    $scope.disableCustomFormFields = function () {

        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = true;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: enables all fields in the custom form
     */
    $scope.enableCustomFormFields = function () {
        const customFormCopy = {};
        angular.copy($scope.customForm, customFormCopy);
        for (const fieldDef in customFormCopy.schemaDefinition.properties) {
            const setreadonly = "customFormCopy.schemaDefinition.properties." + fieldDef + ".readonly = false;";
            eval(setreadonly);
        }
        angular.copy(customFormCopy, $scope.customForm);
    }

    /**
     * Description: transitions to the appropriate state for classId and put current state on the end of the Navigation Chain.
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {
        NavigationChainService.storeCurrentState($scope.invocationParams);
        let stateName = "";
        let stateParams = {};
        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:$scope.datastoreDetails.core_details.datastore_type,
                    objectContext:$scope.datastoreDetails.core_details.datastore_name
                };
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

}


/**
 * Description: Overlay controller - Persistence Technology dialog
 */
function DsdfPersistenceTechnologyOverlayController ($scope, $location, $uibModalInstance) {

    /**
     * Description:
     */
    $scope.updatePersistenceTechnology = function () {
        $scope.datastoreDetails.core_details.pt_id = $scope.editPersistenceTechnologyId;
        $scope.datastoreDetails.core_details.pt_name = $scope.editPersistenceTechnologyName;
        $uibModalInstance.close();
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    /**
     * Description: set the persistence technology for a data store
     */
    $scope.setPersistenceTechnology = function(ptNode) {
        $scope.editPersistenceTechnologyId = ptNode.ptId;
        $scope.editPersistenceTechnologyName = ptNode.ptName;
    }

}


/**
 * Description: Overlay controller - Persistence Technology dialog
 */
function DsdfHostedRealmOverlayController ($scope, $location, $uibModalInstance) {

    /**
     * Description: update the currently selected data store details with the dialog selection
     */
    $scope.updateHostedRealm = function () {
        $scope.datastoreDetails.core_details.hr_id = $scope.editHostedRealmId;
        $scope.datastoreDetails.core_details.hr_name = $scope.editHostedRealmName;
        $uibModalInstance.close();
    };

    /**
     * Description: close the modal when cancel button is clicked
     */
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    /**
     * Description: set the persistence technology for a data store
     */
    $scope.setHostedRealm = function(ptNode) {
        $scope.editHostedRealmId = ptNode.hrId;
        $scope.editHostedRealmName = ptNode.hrName;
    }

}


/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('DsdfDatastoreDetailController', DsdfDatastoreDetailController)
    .controller('DsdfPersistenceTechnologyOverlayController', DsdfPersistenceTechnologyOverlayController)
    .controller('DsdfHostedRealmOverlayController', DsdfHostedRealmOverlayController);
