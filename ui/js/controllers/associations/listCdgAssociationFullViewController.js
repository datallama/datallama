/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the list associations overview view
 */
function ListCdgAssociationFullViewController($rootScope, $scope, $state, $stateParams, NavigationChainService, notify,
                                              DTOptionsBuilder) {

    $scope.gridData = [];
    $scope.fullAssocResults = [];
    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.authQueryString = "?dlAuth=" + $rootScope.authToken;

    /**
     * Initialises the controller.
     */
    $scope.initListCdgAssociationFullView = function () {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.refreshGridData($scope.invocationParams.objectId);

    }

    /**
     * Calls the REST API /association/taxonomy/fullview/{taxonomyId} to get the full list of metadata objects
     *  associated with the taxonomy identified by taxonomyId
     * @param taxonomyId
     */
    $scope.refreshGridData = function (taxonomyId) {

        notify({
            message: "The API call to retrieve all associations for this Ontology List has not been implemented.",
            classes: "alert-warning",
            duration: 2000,
            templateUrl: $scope.notifyTemplate
        });

    }

    /**
     * Standard Go Back navigation action
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('ListCdgAssociationFullViewController', ListCdgAssociationFullViewController);
