/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomies Overview view
 */
function SmdHierarchyAssociationFullViewController($rootScope, $scope, $state, $stateParams, $location, notify,
                                               NavigationChainService, DTOptionsBuilder, HttpService, MoClassService) {

    $scope.gridData = [];
    $scope.fullAssocResults = [];
    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';

    /**
     * Initialises the controller.
     */
    $scope.initSmdHierarchyAssociationFullView = function () {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.refreshGridData($scope.invocationParams.objectId);

    }

    /**
     * Calls the REST API /association/taxonomy/fullview/{taxonomyId} to get the full list of metadata objects
     *  associated with the taxonomy identified by taxonomyId
     * @param taxonomyId
     */
    $scope.refreshGridData = function (objectId) {
        const GET_URL = apiHost + "/association/fullview/smd/hierarchy/" + objectId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response){
                        $scope.fullAssocResults = response.data.result_data;
                        angular.copy($scope.fullAssocResults, $scope.gridData);
                    }
                }
            ]
        );
    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.gridData = [];
        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.fullAssocResults.length; i++) {
                if ($scope.fullAssocResults[i].parent === false) {
                    endnodes.push($scope.fullAssocResults[i]);
                }
            }
            angular.copy(endnodes, $scope.gridData);
        }
        else {
            angular.copy($scope.fullAssocResults, $scope.gridData);
        }
    }

    /**
     * Standard Go Back navigation action
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

    /**
     * Description: click through to taxonomy detail
     */
    $scope.clickThrough = function (objectId, classId, parentObjectId, parentClassId) {
        var currentStateSpec = {
            stateName:"associations.taxonomy_fullview",
            objectId:$stateParams.invocationParams.objectId,
            classId:$stateParams.invocationParams.classId,
            parentObjectId:0,
            parentClassId:0,
            stateParams:{}
        };

        NavigationChainService.storeCurrentState(currentStateSpec);

        let invocationParams = {stateName: "", objectId: 0, classId: 0, parentObjectId: 0, parentClassId: 0, stateParams: {}};
        let moClass = MoClassService.getMoClassByClassId(classId);
        invocationParams.stateName = moClass.objectDetailState;
        invocationParams.objectId = objectId;
        invocationParams.classId = classId;
        invocationParams.parentObjectId = parentObjectId;
        invocationParams.parentClassId = parentClassId;
        invocationParams.stateParams = {};

        if (invocationParams.stateName.length > 0) {
            $state.go(invocationParams.stateName, {invocationParams:invocationParams});
        }
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('SmdHierarchyAssociationFullViewController', SmdHierarchyAssociationFullViewController);
