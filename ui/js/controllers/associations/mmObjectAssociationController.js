/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description:
 */
function MmObjectAssociationController ($scope, $rootScope, $state, $stateParams, $window, NavigationChainService, notify, DTOptionsBuilder, HttpService) {
    $scope.gridData = [];
    $scope.assocTypes = [];
    $scope.assocChanges = [];
    $scope.assocDirection = 1;  // 1 = default direction (initiating MD Object is the source). 0 = reverse (initiating MD Object is the target)
    $scope.currentAssociations = [];
    $scope.selectedObject = {};
    $scope.invokingState = {};
    $scope.invocationParams = {};
    $scope.assocShortDesc = "";
    $scope.searchParams = {
        keyword: "",
        objectName: true,
        objectNameExact: false,
        objectShortDesc: true,
        taxonomy: true,
        messageFormat: true,
        landscapeObjects: true,
        dataFlows: true,
        dataBases: true,
        diagrams: true,
        endNodesOnly: true
    };
    $scope.config = {
        filterPanelHidden: true,
        changeAssocType: true,
        multiSelect: true
    };

    $scope.validation = {
        selectedObject: true,
        assocType: true
    }

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'}
        ]);

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Initialisation function for the MmObjectAssociationController.
     */
    $scope.initMmObjectAssociation = function () {
        $scope.invocationParams = $stateParams.invocationParams;
        if($scope.invocationParams.stateParams.changeAssocType === false){
            $scope.config.changeAssocType = false;
        }

        if($scope.invocationParams.stateParams.multiSelect === false){
            $scope.config.multiSelect = false;
        }

        $scope.selectedObject.objectName = "";
        $scope.selectedObject.className = "";
        $scope.selectedObject.objectShortDesc = "";
        $scope.selectedObject.objectId = 0;
        $scope.selectedObject.classId = 0;
        $scope.selectedObject.parentObjectId = 0;
        $scope.selectedObject.parentObjectName = "";
        $scope.selectedObject.displayName = "";
        $scope.selectedObject.assocType = {};
        $scope.readAssocTypes();
    }

    $scope.readAssocTypes = function() {
        $scope.spinnerLoading = "sk-loading";
        const GET_URL = apiHost + "/association/types"

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.assocTypes = response.data.result_data;
                        $scope.selectedObject.assocType = $scope.assocTypes[0];
                        for(let i = 0; i < $scope.assocTypes.length; i++){
                            if($scope.assocTypes[i].assocTypeName === $scope.invocationParams.stateParams.assocType){
                                $scope.selectedObject.assocType = $scope.assocTypes[i];
                                break;
                            }
                        }
                        $scope.spinnerLoading = "";
                    }
                }
            ]
        );
    }

    /**
     * Description: reverses the direction of the association based on current direction setting
     */
    $scope.reverseAssociationDirection = function() {
        $scope.assocDirection = !($scope.assocDirection);
    }

    /**
     * Description: Executes the Metadata Object search when the search button is clicked
     */
    $scope.executeSearch = function() {
        $scope.spinnerLoading = "sk-loading";

        // 1. clear the currentAssociations array
        $scope.assocChanges = [];

        // 2. execute the search
        const keyword = String($scope.searchParams.keyword);
        if (keyword.length > 0) {
            const POST_URL = apiHost + "/home/search/filtered";

            HttpService.call(POST_URL, "POST", $scope.searchParams, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.executeSearchSCB(response);
                        }
                    }
                ]
            );

        }
    }

    /**
     * Description: Processes the results of the Metadata Object search, including checks for existing associations
     * @param response
     */
    $scope.executeSearchSCB = function(response) {
        $scope.spinnerLoading = "";
        $scope.gridData = response.data.result_data;
        if ($scope.gridData.length === 0) {
            notify({
                message: "No rows returned for you search",
                classes: "alert-success",
                duration: 1000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            const GET_URL = apiHost + "/associations/" + $scope.invocationParams.objectId + "/" + $scope.invocationParams.classId;
            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.currentAssociations = response.data.result_data;
                            for(let i = 0; i < $scope.gridData.length; i++){
                                const row = $scope.gridData[i];
                                let selected = false;
                                for(let j = 0; j < $scope.currentAssociations.length; j++){
                                    if($scope.currentAssociations[j].classId === row.classId && $scope.currentAssociations[j].objectId === row.objectId){
                                        selected = true;
                                        break;
                                    }
                                }
                                $.extend(row, {selected: selected, selectionChange: false});
                            }
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: Clears the current search
     */
    $scope.clearSearch = function() {
        $scope.searchParams.keyword = "";
        $scope.gridData = [];
    }

    /**
     * Description: toggles visibility of the search filter panel
     */
    $scope.toggleFilterPanel = function() {
        $scope.config.filterPanelHidden = !$scope.config.filterPanelHidden
    }

    /**
     * Description: toggles the meta class selection on/off when meta class name is clicked on the Search Filter Classes drop down
     * @param paramName
     */
    $scope.toggleSearchParams = function(paramName) {
        let cmd = "$scope.searchParams." + paramName + " = !($scope.searchParams." + paramName + ")";
        eval(cmd);
    }

    /**
     * Description: save the associations to the DL database and navigates back to the state that invoked the object association view
     */
    $scope.saveAssociation = function () {
        const assocList = [];
        let sourceObjectId = 0;
        let sourceClassId = 0;
        let targetObjectId = 0;
        let targetClassId = 0;

        if (!$scope.config.multiSelect && $scope.assocChanges.length > 1){
            notify({
                message: "This association doesn't allow multiple selections. Please only choose one object to associate.",
                classes: "alert-warning",
                duration: 1000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            for (let i = 0; i < $scope.assocChanges.length; i++) {
                const assocChange = $scope.assocChanges[i];
                if (assocChange.selectionChange){
                    if ($scope.assocDirection) {    // the default direction: initiating MD Object is source
                        sourceObjectId = $scope.invocationParams.objectId;
                        sourceClassId = $scope.invocationParams.classId;
                        targetObjectId = assocChange.objectId;
                        targetClassId = assocChange.classId;
                    }
                    else {      // the reverse direction: initiating MD Object is target
                        sourceObjectId = assocChange.objectId;
                        sourceClassId = assocChange.classId;
                        targetObjectId = $scope.invocationParams.objectId;
                        targetClassId = $scope.invocationParams.classId;
                    }

                    const assoc = {
                        assocId : -1,
                        sourceObjectId: sourceObjectId,
                        sourceClassId: sourceClassId,
                        targetObjectId: targetObjectId,
                        targetClassId: targetClassId,
                        assocShortDesc: $scope.assocShortDesc,
                        assocTypeId: $scope.selectedObject.assocType.assocTypeId
                    }
                    assocList.push(assoc);
                }
                else {
                    console.log("That's weird, it looks like you tried to remove an association but you're not allowed to do that!")
                }
            }

            const POST_URL = apiHost + "/association/content";

            HttpService.call(POST_URL, "POST", assocList, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            notify({
                                message: "New association(s) saved.",
                                classes: "alert-success",
                                duration: 2000,
                                templateUrl: $scope.notifyTemplate
                            });
                            $scope.goBack();
                        }
                    }
                ]
            );
        }
    }

    /**
     * Description: Update the list of association changes. We can't just use gridData because if you make a change
     *  and then make a new search you will wipe the info.
     * @param objectId
     * @param classId
     * @param selectionChange
     * @param name
     */
    $scope.updateAssocChanges = function(objectId, classId, selectionChange, name) {

        if(selectionChange == 1) {
            // add selected metadata object to assocChanges array
            $scope.assocChanges.push({classId:classId, objectId:objectId, selectionChange:selectionChange, name:name});
        }
        else if (selectionChange == 0) {
            // delete selected metadata object from assocChanges array
            for(let i = 0; i < $scope.assocChanges.length; i++) {
                const change = $scope.assocChanges[i];
                if(change.classId == classId && change.objectId == objectId){
                    $scope.assocChanges.splice(i,1);
                    break;
                }
            }
        }
    }

    /**
     * Description: checks whether a valid selection has been made for the association before saving the association.
     */
    $scope.checkValid = function() {
        $scope.validation.selectedObject = !!($scope.assocChanges && $scope.assocChanges.length > 0);
        $scope.validation.assocType = !!($scope.selectedObject.assocType && $scope.selectedObject.assocType.assocTypeId
            && $scope.selectedObject.assocType.assocTypeId > 0);
        if ($scope.validation.selectedObject && $scope.validation.assocType) {
            $scope.saveAssociation();
        }
        else {
            notify({
                message: "You must select at least one metadata object from the search results.",
                classes: "alert-warning",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
    }

    /**
     * Description: return to the Data Store Summary view
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MmObjectAssociationController', MmObjectAssociationController);

