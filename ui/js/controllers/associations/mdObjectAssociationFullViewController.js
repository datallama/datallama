/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: Controller for the Taxonomies Overview view
 */
function MdObjectAssociationFullViewController($rootScope, $scope, $state, $stateParams, $location, notify,
                                               NavigationChainService, DTOptionsBuilder, DTColumnBuilder, HttpService, MoClassService, DetailsClickThroughService) {

    $scope.gridData = [];
    $scope.fullAssocResults = [];
    $scope.config = {visibilityLevel: 2, searchPanelHidden: true, abbreviation: 0, endnodes: false};

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withButtons([
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'DatastoreSummary'},
            {extend: 'pdf', title: 'DatastoreSummary'},
        ]);
    $scope.dtColumns = [
        DTColumnBuilder.newColumn(0).notVisible(),
        DTColumnBuilder.newColumn(1).notSortable(),
        DTColumnBuilder.newColumn(2).notSortable()
    ];

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.stateParams = {};
    $scope.notifyTemplate = 'views/common/notify.html';
    $scope.spinnerLoading = "";

    /**
     * Initialises the controller.
     */
    $scope.initMdObjectAssociationFullView = function () {
        $scope.invocationParams = $stateParams.invocationParams;
        $scope.refreshGridData($scope.invocationParams.objectId, $scope.invocationParams.classId);

    }

    /**
     * TODO: the full association REST end point URL for each class should be defined in and retrieved from the dl_class tables
     * @param objectId
     * @param classId
     */
    $scope.refreshGridData = function (objectId, classId) {
        let urlPathStub = "NOT_SET";
        switch(classId) {
            case MoClassService.getMoClassIdByCode('REST_API'):
                urlPathStub = "/api/rest/";
                break;
            case MoClassService.getMoClassIdByCode('JSON_MESSAGE_FORMAT'):
                urlPathStub = "/format/json/";
                break;
            case MoClassService.getMoClassIdByCode('GHMF'):
                urlPathStub = "/format/ghmf/";
                break;
            /*case MoClassService.getMoClassIdByCode('XML_MESSAGE_FORMAT'):
                urlPathStub = "/format/xml/";
                break;*/
            case MoClassService.getMoClassIdByCode('DELIMITED_FLATFILE'):
            case MoClassService.getMoClassIdByCode('FIXED_WIDTH_FLATFILE'):
                urlPathStub = "/format/flatfile/";
                break;
        }

        if (urlPathStub == "NOT_SET") {
            let notifyMsg = "Full Association View has not been implemented for Metadata Object Class " + MoClassService.getMoClassByClassId(classId).className;
            notify({
                message: notifyMsg,
                classes: "alert-success",
                duration: 2000,
                templateUrl: $scope.notifyTemplate
            });
        }
        else {
            $scope.spinnerLoading = "sk-loading";
            const GET_URL = apiHost + "/association/fullview" + urlPathStub + objectId;

            HttpService.call(GET_URL, "GET", null, null,
                [
                    {
                        status: 1,
                        callback: function(response){
                            $scope.fullAssocResults = response.data.result_data;
                            angular.copy($scope.fullAssocResults, $scope.gridData);
                            $scope.spinnerLoading = "";
                        }
                    }
                ]
            );
        }

    }

    /**
     * Description: shows only end nodes (i.e. nodes that are not parents) in the table view when $scope.config.endnodes = true
     * Otherwise it shows all nodes
     */
    $scope.showEndNodesOnly = function() {
        $scope.config.endnodes = !$scope.config.endnodes;

        $scope.gridData = [];
        if ($scope.config.endnodes) {
            const endnodes = [];
            for(let i = 0; i < $scope.fullAssocResults.length; i++) {
                if ($scope.fullAssocResults[i].parent === false) {
                    endnodes.push($scope.fullAssocResults[i]);
                }
            }
            angular.copy(endnodes, $scope.gridData);
        }
        else {
            angular.copy($scope.fullAssocResults, $scope.gridData);
        }
    }

    /**
     * Description: click through to taxonomy detail
     * @param objectId
     * @param classId
     */
    $scope.clickThrough = function (objectId, classId) {
        NavigationChainService.storeCurrentState($scope.invocationParams);


        DetailsClickThroughService.clickThrough(objectId, classId);
    }

    /**
     * Standard Go Back navigation action
     */
    $scope.goBack = function() {
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MdObjectAssociationFullViewController', MdObjectAssociationFullViewController);
