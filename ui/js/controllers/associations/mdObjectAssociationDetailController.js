/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The controller for the association detail view
 */
function MdObjectAssociationDetailController ($scope, $stateParams, $uibModal, notify, HttpService,
                                              NavigationChainService, RbacService, UtilityService) {

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.editingDisabled = {
        details: true
    }

    $scope.assocDetails = {
        assocId: 0,
        assocTypeId: 0,
        assocTypeName: "",
        assocSourcePhrase: "",
        assocTargetPhrase: "",
        assocShortDesc: "",
        sourceClassId: 0,
        sourceObjectId: 0,
        sourceMdContext: [],
        sourceClassName: "",
        sourceObjectName: "",
        sourceShortDesc: "",
        targetClassId: 0,
        targetObjectId: 0,
        targetMdContext: [],
        targetClassName: "",
        targetObjectName: "",
        targetShortDesc: ""
    }

    $scope.assocTypes = [];
    $scope.assocEdits = {
        assocType : {},
        assocShortDesc : ""
    };
    $scope.assocEditsUndo = {};
    $scope.perms = {create: false, update: false, delete: false, publish: false, configure: false};

    /**
     * Description: initialises this Controller
     */
    $scope.initAssociationDetail = function () {
        $scope.perms.create = !RbacService.checkRbac('penv','create');
        $scope.perms.update = !RbacService.checkRbac('penv','update');
        $scope.perms.delete = !RbacService.checkRbac('penv','delete');
        $scope.perms.publish = !RbacService.checkRbac('penv','publish');
        $scope.perms.configure = !RbacService.checkRbac('penv','configure');
        console.log($scope.perms);

        $scope.invocationParams = $stateParams.invocationParams;
        const GET_URL = apiHost + "/association/" + $scope.invocationParams.objectId;

        HttpService.call(GET_URL,"GET",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.initSCB(response);
                    }
                }
            ]
        );

    }

    $scope.initSCB = function(prevResponse) {
        $scope.assocDetails = prevResponse.data.result_data;

        const GET_URL = apiHost + "/association/types"

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        $scope.assocEdits.assocShortDesc = $scope.assocDetails.assocShortDesc;
                        $scope.assocTypes = response.data.result_data;
                        for(let i = 0; i < $scope.assocTypes.length; i++){
                            if($scope.assocTypes[i].assocTypeName === $scope.assocDetails.assocTypeName){
                                $scope.assocEdits.assocType = $scope.assocTypes[i];
                                break;
                            }
                        }
                        angular.copy($scope.assocEdits, $scope.assocEditsUndo);
                    }
                }
            ]
        );
    }

    /**
     * Edit actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxEdit = function(dlIboxToolsName) {
        if (dlIboxToolsName === "assoc_details") {
            angular.copy($scope.assocEdits, $scope.assocEditsUndo);
            $scope.editingDisabled.details = false;
        }
    }

    /**
     * Save actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxSave = function(dlIboxToolsName) {
        if (dlIboxToolsName === "assoc_details") {
            $scope.editingDisabled.details = true;
            $scope.saveAssocDetails();
        }

    }

    /**
     * Cancel actions for dlIboxTools on the page
     * @param dlIboxToolsName
     */
    $scope.dlIboxCancel = function(dlIboxToolsName) {
        if (dlIboxToolsName === "assoc_details") {
            angular.copy($scope.assocEditsUndo, $scope.assocEdits);
            $scope.editingDisabled.details = true;
        }

    }

    $scope.saveAssocDetails = function(){
        const POST_URL = apiHost + "/association/content"
        const assoc = [{
            assocId : $scope.assocDetails.assocId,
            sourceObjectId: $scope.assocDetails.sourceObjectId,
            sourceClassId: $scope.assocDetails.sourceClassId,
            targetObjectId: $scope.assocDetails.targetObjectId,
            targetClassId: $scope.assocDetails.targetClassId,
            assocShortDesc: $scope.assocEdits.assocShortDesc,
            assocTypeId: $scope.assocEdits.assocType.assocTypeId
        }]
        HttpService.call(POST_URL, "POST", assoc, null,
            [
                {
                    status: 1,
                    callback: function(response) {
                        angular.copy($scope.assocEdits, $scope.assocEditsUndo);
                        $scope.assocDetails.assocSourcePhrase = $scope.assocEdits.assocType.sourcePhrase;
                        $scope.assocDetails.assocTargetPhrase = $scope.assocEdits.assocType.targetPhrase;
                    }
                }
            ]
        );
    }

    /**
     * Description: opens the confirm delete modal dialog
     */
    $scope.confirmDelete = function () {
        $uibModal.open({
            templateUrl: UtilityService.getReleaseTemplateUrl('views/dl-common/cmn_confirm_delete.html'),
            controller: 'ConfirmDeleteController',
            size: 'sm',
            scope: $scope
        });
    }

    $scope.deleteMdObject = function () {
        const DELETE_URL = apiHost + "/association/" + $scope.assocDetails.assocId;

        HttpService.call(DELETE_URL,"DELETE",null,null,
            [
                {
                    status:1,
                    callback: function(response){
                        notify({
                            message: "Association successfully deleted.",
                            classes: "alert-success",
                            duration: 2000,
                            templateUrl: $scope.notifyTemplate
                        });
                        $scope.goBack();
                    }
                }
            ]
        );
    }

    $scope.checkRbac = function(module, actionCategory){
        const mdObjectOwner = 0;  // TODO: implement object ownership check
        return RbacService.checkRbac(module, actionCategory, mdObjectOwner);
    }

    $scope.goBack = function(){
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('MdObjectAssociationDetailController', MdObjectAssociationDetailController);
