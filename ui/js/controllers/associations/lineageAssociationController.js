/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: The controller for the lineage association view. Lineage presents the full chain of Lineage associations
 *  coming into and going out of a DL Metadata Object.
 */
function LineageAssociationController ($scope, $stateParams, $state, HttpService, NavigationChainService, MoClassService) {

    $scope.invocationParams = $stateParams.invocationParams;
    $scope.notifyTemplate = 'views/common/notify.html';

    $scope.lineage = [];

    /**
     * Description: initialise the controller. Retrieve lineage data from DL DB
     */
    $scope.initLineageDetail = function () {

        $scope.invocationParams = $stateParams.invocationParams;
        const GET_URL = apiHost + "/association/lineage/" + $scope.invocationParams.objectId + "/" + $scope.invocationParams.classId;

        HttpService.call(GET_URL, "GET", null, null,
            [
                {
                    status:1,
                    callback: function(response){
                        $scope.lineage = response.data.result_data;

                        //TODO: make a better way of changing the bg colour of the items in the lineage
                        for(let i = 0; i < $scope.lineage.incoming.length; i++){
                            if($scope.lineage.incoming[i].repeated){
                                $scope.lineage.incoming[i].bg = "#f1a899";
                            }
                            else{
                                $scope.lineage.incoming[i].bg = "#e6f1ff";
                            }
                        }
                        for(let i = 0; i < $scope.lineage.outgoing.length; i++){
                            if($scope.lineage.outgoing[i].repeated){
                                $scope.lineage.outgoing[i].bg = "#f1a899";
                            }
                            else{
                                $scope.lineage.outgoing[i].bg = "#e6f1ff";
                            }
                        }
                    }
                }
            ]
        );

    }

    /**
     * Description: transitions to the appropriate state for classId and put current state on the end of the Navigation Chain.
     */
    $scope.clickThrough = function(objectId, classId, parentObjectId, parentClassId, mode) {

        NavigationChainService.storeCurrentState($scope.invocationParams);
        let stateName = "";
        let stateParams = {};

        switch (mode) {
            case "assocDetails":
                classId = MoClassService.getMoClassIdByCode("OBJECT_ASSOCIATION");
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            case "lineage":
                stateName = "associations.lineage";
                break;
            case "newAssoc":
                stateName = "associations.new_association";
                stateParams={
                    objectClass:"NA",
                    objectContext:"NA"
                }
                break;
            case "clickthrough":
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
            default:
                stateName = MoClassService.getMoClassByClassId(classId).objectDetailState;
                break;
        }

        $state.go(stateName,
            {
                invocationParams:{
                    stateName:stateName,
                    objectId:objectId,
                    classId:classId,
                    parentObjectId: parentObjectId,
                    parentClassId: parentClassId,
                    stateParams: stateParams
                }
            }
        );
    }

    /**
     * Description: navigate to previous view in the Navigation Chain
     */
    $scope.goBack = function(){
        NavigationChainService.goBack();
    }

}

/** * * * * * * * * * * * * * * * * * * * * *
 * Pass function into module as controller  *
 ** * * * * * * * * * * * * * * * * * * * * */
angular
    .module('datallama')
    .controller('LineageAssociationController', LineageAssociationController);
