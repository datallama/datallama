/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: the primary configuration file for the Data Llama UI
 */

// the apiHost variable must include the scheme and the port for the Data Llama API Java Spring application
const apiHost = "https://datallama.localdomain/api";
const authMethod = "dlint";
const authConfig = [
    { authMethod:"msoidc",
        loginPageURL: "views/common/msoidc_login.html",
        authData: "https://login.microsoftonline.com/4a1ae6af-55d7-486c-a9dc-bd9693f8ebe9/oauth2/v2.0/authorize?client_id=d62b0457-3817-4382-b9e8-b70ef9d87317&response_type=id_token&redirect_uri=http%3A%2F%2Fdatallama.localdomain%3A8080%2Fauth%2Fredirect%2Fmsoidc&response_mode=form_post&scope=openid&nonce=678910",
        logoutURL:"https://login.microsoftonline.com/4a1ae6af-55d7-486c-a9dc-bd9693f8ebe9/oauth2/v2.0/logout?post_logout_redirect_uri=http%3A%2F%2Fdatallama.localdomain%3A8080%2Fauth%2Flogout-redirect%2Fmsoidc"
    },
    { authMethod:"dlint",
        loginPageURL: "views/common/login.html",
        authData: "",
        logoutURL:"https://datallama.localdomain/api/auth/logout02"
    }
];
