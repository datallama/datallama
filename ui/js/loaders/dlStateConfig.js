/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: configuration file for view script names
 */

const stateConfig = [
    {
        "stateName": "home.simple_search",
        "abstract": false,
        "url": "/simple_search",
        "templateUrl": "views/home/simple_search.html",
        "data": { "pageTitle": "Simple Search Everything" }
    },
    {
        "stateName": "home.ds_full_report",
        "abstract": false,
        "url": "/ds_full_report",
        "templateUrl": "views/dl-common/data_sensitivity_full_report.html",
        "data": { "pageTitle": "Data Sensitivity Full Report" }
    },
    {
        "stateName": "dsdf",
        "abstract": true,
        "url": "/dsdf",
        "templateUrl": "views/common/content.html",
        "data": {}
    },
    {
        "stateName": "dsdf.diagrams_list",
        "abstract": false,
        "url": "/dsdf_diagrams_list",
        "templateUrl": "views/dsdf/dsdf_diagrams_list.html",
        "data": { "pageTitle": "Data Landscape Diagram List" }
    },
    {
        "stateName": "dsdf.diagrams_view",
        "abstract": false,
        "url": "/dsdf_diagrams",
        "templateUrl": "views/dsdf/dsdf_diagrams.html",
        "data": { "pageTitle": "Data Landscape Diagram" }
    },
    {
        "stateName": "dsdf.datastore_summary",
        "abstract": false,
        "url": "/dsdf_datastore_summary",
        "templateUrl": "views/dsdf/dsdf_datastore_summary.html",
        "data": { "pageTitle": "Landscape Object Summary" }
    },
    {
        "stateName": "dsdf.datastore_detail",
        "abstract": false,
        "url": "/dsdf_datastore_detail/:datastoreId",
        "templateUrl": "views/dsdf/dsdf_datastore_detail.html",
        "data": { "pageTitle": "Landscape Object Detail" }
    },
    {
        "stateName": "dsdf.dataflow_summary",
        "abstract": false,
        "url": "/dsdf_dataflow_summary",
        "templateUrl": "views/dsdf/dsdf_dataflow_summary.html",
        "data": { "pageTitle": "Data Flow Summary" }
    },
    {
        "stateName": "dsdf.dataflow_detail",
        "abstract": false,
        "url": "/dsdf_dataflow_detail/:dataflowId",
        "templateUrl": "views/dsdf/dsdf_dataflow_detail.html",
        "data": { "pageTitle": "Data Flow Detail" }
    },
    {
        "stateName": "dsdf.reports_list",
        "abstract": false,
        "url": "/dsdf_reports_list",
        "templateUrl": "views/dsdf/dsdf_reports_list.html",
        "data": { "pageTitle": "Report List" }
    },
    {
        "stateName": "dsdf.report_view",
        "abstract": false,
        "url": "/dsdf_report_view",
        "templateUrl": "views/dsdf/dsdf_report_view.html",
        "data": { "pageTitle": "Report Detail" }
    },
    {
        "stateName": "dsdf.create_report",
        "abstract": false,
        "url": "/dsdf_create_report",
        "templateUrl": "views/dsdf/dsdf_create_report.html",
        "data": { "pageTitle": "Create Report" }
    },
    {
        "stateName": "datamodels",
        "abstract": true,
        "url": "/datamodels",
        "templateUrl": "views/common/content.html",
        "data": {}
    },
    {
        "stateName": "datamodels.pdm_diagrams_list",
        "abstract": false,
        "url": "/pdm_diagrams_list",
        "templateUrl": "views/data-models/pdm_diagrams_list.html",
        "data": { "pageTitle": "PDM Diagram List" }
    },
    {
        "stateName": "datamodels.pdm_diagrams_view",
        "abstract": false,
        "url": "/pdm_diagrams_view",
        "templateUrl": "views/data-models/pdm_diagrams.html",
        "data": { "pageTitle": "PDM Diagram" }
    },
    {
        "stateName": "datamodels.pdm_list",
        "abstract": false,
        "url": "/pdm_list",
        "templateUrl": "views/data-models/pdm_list.html",
        "data": { "pageTitle": "PDM List" }
    },
    {
        "stateName": "datamodels.sdm_diagrams",
        "abstract": false,
        "url": "/sdm_diagrams",
        "templateUrl": "views/data-models/sdm_diagrams.html",
        "data": { "pageTitle": "SDM Diagrams" }
    },
    {
        "stateName": "datamodels.new_shape_click_through",
        "abstract": false,
        "url": "/new_shape_click_through",
        "templateUrl": "views/data-models/new_shape_click_through.html",
        "data": {"pageTitle": "New Click Through"}
    },
    {
        "stateName": "taxonomies",
        "abstract": true,
        "url": "/taxonomies",
        "templateUrl": "views/common/content.html",
        "data": {}
    },
    {
        "stateName": "taxonomies.overview",
        "abstract": false,
        "url": "/overview",
        "templateUrl": "views/ontology/ontology_overview.html",
        "data": { "pageTitle": "Ontology Overview" }
    },
    {
        "stateName": "taxonomies.detail",
        "abstract": false,
        "url": "/detail",
        "templateUrl": "views/taxonomies/taxonomy_detail.html",
        "data": { "pageTitle": "Taxonomy Detail" }
    },
    {
        "stateName": "taxonomies.taxonomy_summary",
        "abstract": false,
        "url": "/summary",
        "templateUrl": "views/taxonomies/taxonomy_summary.html",
        "data": { "pageTitle": "Taxonomy Summary" }
    },
    {
        "stateName": "taxonomies.diagrams_list",
        "abstract": false,
        "url": "/ontology_diagrams_list",
        "templateUrl": "views/ontology/ontology_diagrams_list.html",
        "data": {"pageTitle": "Ontology Diagram List"}
    },
    {
        "stateName": "taxonomies.diagrams_view",
        "abstract": false,
        "url": "/ontology_diagrams",
        "templateUrl": "views/ontology/ontology_diagrams.html",
        "data": { "pageTitle": "Ontology Diagram" }
    },
    {
        "stateName": "taxonomies.list",
        "abstract": false,
        "url": "/taxonomies_list",
        "templateUrl": "views/taxonomies/taxonomies_list.html",
        "data": { "pageTitle": "Taxonomies List" }
    },
    {
        "stateName": "taxonomies.list_cdg_list",
        "abstract": false,
        "url": "/list_cdg_list",
        "templateUrl": "views/ontology/list_cdg_list.html",
        "data": { "pageTitle": "CDG Lists" }
    },
    {
        "stateName": "taxonomies.list_cdg_detail",
        "abstract": false,
        "url": "/list_cdg_detail",
        "templateUrl": "views/ontology/list_cdg_detail.html",
        "data": { "pageTitle": "CDG List Detail" }
    },
    {
        "stateName": "taxonomies.list_cdg_summary",
        "abstract": false,
        "url": "/list_cdg_summary",
        "templateUrl": "views/ontology/list_cdg_summary.html",
        "data": { "pageTitle": "CDG List Summary" }
    },
    {
        "stateName": "taxonomies.data_sensitivity_tag_list",
        "abstract": false,
        "url": "/data_sensitivity_detail",
        "templateUrl": "views/common/data_sensitivity_detail.html",
        "data": { "pageTitle": "Data Sensitivity Tags"}
    },
    {
        "stateName": "taxonomies.create_ds_auto_assign_task",
        "abstract": false,
        "url": "/ds_auto_assign_task",
        "templateUrl": "views/common/create_ds_auto_assign_task.html",
        "data": { "pageTitle": "Create DS Auto Assignment Task" }
    },
    {
        "stateName": "taxonomies.ds_auto_assign_task_detail",
        "abstract": false,
        "url": "/ds_auto_assign_task_detail",
        "templateUrl": "views/common/ds_auto_assign_task_detail.html",
        "data": { "pageTitle": "DS Auto Assignment Task Detail" }
    },
    {
        "stateName": "smd",
        "abstract": true,
        "url": "/smd",
        "templateUrl": "views/common/content.html",
        "data": {}
    },
    {
        "stateName": "smd.smd_database_list",
        "abstract": false,
        "url": "/smd_database_list",
        "templateUrl": "views/smd/smd_database_list.html",
        "data": { "pageTitle": "SMD Database List" }
    },
    {
        "stateName": "smd.smd_database_detail",
        "abstract": false,
        "url": "/smd_database_detail",
        "templateUrl": "views/smd/smd_database_detail.html",
        "data": { "pageTitle": "Database Detail" }
    },
    {
        "stateName": "smd.smd_database_ds_report",
        "abstract": false,
        "url": "/smd_database_ds_report",
        "templateUrl": "views/smd/smd_database_ds_report.html",
        "data": { "pageTitle": "Database Data Sensitivity Report" }
    },
    {
        "stateName": "smd.schema_detail",
        "abstract": false,
        "url": "/smd_schema_detail",
        "templateUrl": "views/smd/smd_schema_detail.html",
        "data": { "pageTitle": "Schema Detail" }
    },
    {
        "stateName": "smd.table_view_detail",
        "abstract": false,
        "url": "/smd_table_view_detail",
        "templateUrl": "views/smd/smd_table_view_detail.html",
        "data": { "pageTitle": "SMD Table or View Detail" }
    },
    {
        "stateName": "smd.column_detail",
        "abstract": false,
        "url": "/smd_column_detail",
        "templateUrl": "views/smd/smd_column_detail.html",
        "data": { "pageTitle": "SMD Column Detail" }
    },
    {
        "stateName": "smd.relationship_detail",
        "abstract": false,
        "url": "/smd_relationship_detail",
        "templateUrl": "views/smd/smd_relationship_detail.html",
        "data": { "pageTitle": "SMD Relationship Detail" }
    },
    {
        "stateName": "smd.compare_tables",
        "abstract": false,
        "url": "/compare_tables",
        "templateUrl": "views/smd/compare_tables.html",
        "data": { "pageTitle": "Compare Tables" }
    },
    {
        "stateName": "smd.reconcile_db_reveng_run",
        "abstract": false,
        "url": "/smd_reconcile_db_reveng_run",
        "templateUrl": "views/smd/smd_reconcile_db_reveng_run.html",
        "data": { "pageTitle": "Reconcile SMD DB Reverse Engineering Run"}
    },
    {
        "stateName": "smd.smd_filestore_list",
        "abstract": false,
        "url": "/filestores_list",
        "templateUrl": "views/smd/filestores_list.html",
        "data": { "pageTitle": "File Stores List" }
    },
    {
        "stateName": "smd.smd_filestore_detail",
        "abstract": false,
        "url": "/smd_filestore_detail",
        "templateUrl": "views/smd/filestore_detail.html",
        "data": { "pageTitle": "File Store Detail" }
    },
    {
        "stateName": "smd.smd_filestore_summary",
        "abstract": false,
        "url": "/smd_filestore_summary",
        "templateUrl": "views/smd/smd_filestore_summary.html",
        "data": { "pageTitle": "File Store Summary" }
    },
    {
        "stateName": "smd.msg_fmt",
        "abstract": false,
        "url": "/msg_fmt",
        "templateUrl": "views/formats/msg_formats_list.html",
        "data": {"pageTitle": "Formats"}
    },
    {
        "stateName": "smd.fmt_summary",
        "abstract": false,
        "url": "/fmt_summary",
        "templateUrl": "views/formats/format_summary.html",
        "data": {"pageTitle": "Format Summary"}
    },
    {
        "stateName": "smd.fmt_json_detail",
        "abstract": false,
        "url": "/fmt_json_detail",
        "templateUrl": "views/formats/fmt_json_detail.html",
        "data": { "pageTitle": "JSON Format Detail" }
    },
    {
        "stateName": "smd.fmt_ghmf_detail",
        "abstract": false,
        "url": "/fmt_ghmf_detail",
        "templateUrl": "views/formats/fmt_ghmf_detail.html",
        "data": { "pageTitle": "GHMF Detail" }
    },
    {
        "stateName": "smd.fmt_xml_detail",
        "abstract": false,
        "url": "/fmt_xml_detail",
        "templateUrl": "views/formats/fmt_xml_detail.html",
        "data": { "pageTitle": "XML Format Detail" }
    },
    {
        "stateName": "smd.fmt_flatfile_detail",
        "abstract": false,
        "url": "/fmt_flatfile_detail",
        "templateUrl": "views/formats/fmt_flatfile_detail.html",
        "data": { "pageTitle": "Flat File Format Detail" }
    },
    {
        "stateName": "smd.api_definition",
        "abstract": false,
        "url": "/api_definition",
        "templateUrl": "views/apidefinition/api_definition_list.html",
        "data": {"pageTitle": "API Definitions"}
    },
    {
        "stateName": "smd.api_rest_detail",
        "abstract": false,
        "url": "/api_rest_detail",
        "templateUrl": "views/apidefinition/api_rest_detail.html",
        "data": {"pageTitle": "REST API Detail"}
    },
    {
        "stateName": "smd.api_definition_summary",
        "abstract": false,
        "url": "/api_rest_summary",
        "templateUrl": "views/apidefinition/api_rest_summary.html",
        "data": {"pageTitle": "REST API Summary"}
    },
    {
        "stateName": "smd.api_definition_reveng",
        "abstract": false,
        "url": "/api_definition_reveng",
        "templateUrl": "views/apidefinition/api_definition_reveng.html",
        "data": {"pageTitle": "API Specification Reverse Engineer"}
    },
    {
        "stateName": "smd.smd_dummy1",
        "abstract": false,
        "url": "/smd_dummy1",
        "templateUrl": "views/dummy/dummy_comingsoon.html",
        "data": { "pageTitle": "Coming Soon" }
    },
    {
        "stateName": "smd.smd_presentation_list",
        "abstract": false,
        "url": "/smd_presentation_list",
        "templateUrl": "views/smd/smd_presentation_list.html",
        "data": { "pageTitle": "Data Presentation List" }
    },
    {
        "stateName": "smd.smd_presentation_detail",
        "abstract": false,
        "url": "/smd_presentation_definition",
        "templateUrl": "views/smd/smd_presentation_detail.html",
        "data": { "pageTitle": "Data Presentation Detail" }
    },
    {
        "stateName": "smd.smd_presentation_summary",
        "abstract": false,
        "url": "/smd_presentation_summary",
        "templateUrl": "views/smd/smd_presentation_summary.html",
        "data": { "pageTitle": "Data Presentation Summary" }
    },
    {
        "stateName": "smd.dataintegration_list",
        "abstract": false,
        "url": "/dataintegration_list",
        "templateUrl": "views/smd/dataintegration_list.html",
        "data": { "pageTitle": "Data Integration List" }
    },
    {
        "stateName": "smd.dataintegration_summary",
        "abstract": false,
        "url": "/dataintegration_summary",
        "templateUrl": "views/smd/dataintegration_summary.html",
        "data": { "pageTitle": "Data Integration Summary" }
    },
    {
        "stateName": "smd.dataintegration_detail",
        "abstract": false,
        "url": "/dataintegration_detail",
        "templateUrl": "views/smd/dataintegration_detail.html",
        "data": { "pageTitle": "Data Integration Detail" }
    },
    {
        "stateName": "smd.credential_list",
        "abstract": false,
        "url": "/credential_list",
        "templateUrl": "views/smd/credential_list.html",
        "data": { "pageTitle": "Credential List" }
    },
    {
        "stateName": "smd.credential_detail",
        "abstract": false,
        "url": "/credential_detail",
        "templateUrl": "views/smd/credential_detail.html",
        "data": { "pageTitle": "Credential Detail" }
    },
    {
        "stateName": "smd.credential_create",
        "abstract": false,
        "url": "/credential_create",
        "templateUrl": "views/smd/create_credential.html",
        "data": { "pageTitle": "Create Credential" }
    },
    {
        "stateName": "smd.credential_update",
        "abstract": false,
        "url": "/credential_update",
        "templateUrl": "views/smd/credential_detail_update.html",
        "data": { "pageTitle": "Update Credential Details" }
    },
    {
        "stateName": "smd.hierarchy_reveng",
        "abstract": false,
        "url": "/hierarchy_reveng",
        "templateUrl": "views/smd/hierarchy_reveng.html",
        "data": { "pageTitle": "Reverse Engineer" }
    },
    {
        "stateName": "smd.smd_dummy3",
        "abstract": false,
        "url": "/smd_dummy3",
        "templateUrl": "views/dummy/dummy_comingsoon.html",
        "data": { "pageTitle": "Coming Soon" }
    },
    {
        "stateName": "associations",
        "abstract": true,
        "url": "/associations",
        "templateUrl": "views/common/content.html",
        "data": {}
    },
    {
        "stateName": "associations.association_detail",
        "abstract": false,
        "url": "/association_detail",
        "templateUrl": "views/associations/object_association_detail.html",
        "data": { "pageTitle": "Object Association Details" }
    },
    {
        "stateName": "associations.new_association",
        "abstract": false,
        "url": "/new_association",
        "templateUrl": "views/associations/mm_object_association.html",
        "data": { "pageTitle": "New Object Association" }
    },
    {
        "stateName": "associations.taxonomy_fullview",
        "abstract": false,
        "url": "/taxonomy_association_fullview",
        "templateUrl": "views/associations/taxonomy_association_fullview.html",
        "data": { "pageTitle": "Taxonomy Associations" }
    },
    {
        "stateName": "associations.list_cdg_fullview",
        "abstract": false,
        "url": "/list_cdg_association_fullview",
        "templateUrl": "views/associations/list_cdg_association_fullview.html",
        "data": { "pageTitle": "List Associations" }
    },
    {
        "stateName": "associations.smd_tableview_fullview",
        "abstract": false,
        "url": "/smd_tableview_association_fullview",
        "templateUrl": "views/associations/smd_tableview_association_fullview.html",
        "data": { "pageTitle": "Table/View Associations" }
    },
    {
        "stateName": "associations.smd_hierarchy_fullview",
        "abstract": false,
        "url": "/smd_hierarchy_association_fullview",
        "templateUrl": "views/associations/smd_hierarchy_association_fullview.html",
        "data": { "pageTitle": "All Associations" }
    },
    {
        "stateName": "associations.lineage",
        "abstract": false,
        "url": "/association_lineage",
        "templateUrl": "views/associations/lineage_association.html",
        "data": { "pageTitle": "Lineage Associations" }
    },
    {
        "stateName": "admin",
        "abstract": true,
        "url": "/admin",
        "templateUrl": "views/common/content.html",
        "data": {}
    },
    {
        "stateName": "admin.application_type_list",
        "abstract": false,
        "url": "/association_type_list",
        "templateUrl": "views/admin/association_type_list.html",
        "data": { "pageTitle": "Application Types" }
    },
    {
        "stateName": "admin.custom_form_list",
        "abstract": false,
        "url": "/custom_form_list",
        "templateUrl": "views/admin/custom_form_list.html",
        "data": { "pageTitle": "Custom Forms" }
    },
    {
        "stateName": "admin.custom_form_designer",
        "abstract": false,
        "url": "/custom_form_designer",
        "templateUrl": "views/admin/custom_form_designer.html",
        "data": { "pageTitle": "Custom Form Designer" }
    },
    {
        "stateName": "admin.users_groups_list",
        "abstract": false,
        "url": "/users_groups_list",
        "templateUrl": "views/admin/users_groups_list.html",
        "data": { "pageTitle": "User-Group Admin" }
    },
    {
        "stateName": "admin.user_detail",
        "abstract": false,
        "url": "/user_detail",
        "templateUrl": "views/admin/user_detail.html",
        "data": {"pageTitle": "User Detail"}
    },
    {
        "stateName": "admin.group_detail",
        "abstract": false,
        "url": "/group_detail",
        "templateUrl": "views/admin/group_detail.html",
        "data": {"pageTitle": "Group Detail"}
    },
    {
        "stateName": "admin.mo_class_hierarchy",
        "abstract": false,
        "url": "/mo_class_hierarchy",
        "templateUrl": "views/admin/mo_class_hierarchy.html",
        "data": {"pageTitle": "Metadata Object Class Hierarchy"}
    },
    {
        "stateName": "associations.mdobject_fullview",
        "abstract": false,
        "url": "/mdobject_association_fullview",
        "templateUrl": "views/associations/mdobject_association_fullview.html",
        "data": {"pageTitle": "Metadata Object Associations"}
    }
];
