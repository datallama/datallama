// the apiHost variable must include the scheme and the port for the Data Llama API Java Spring application
const apiHost = "http://localhost:8080";
const authMethod = "dlint";
const authConfig = [
  { authMethod:"msoidc",
    loginPageURL: "views/common/msoidc_login.html",
    authData: "https://login.microsoftonline.com/4a1ae6af-55d7-486c-a9dc-bd9693f8ebe9/oauth2/v2.0/authorize?client_id=d62b0457-3817-4382-b9e8-b70ef9d87317&response_type=id_token&redirect_uri=http%3A%2F%2Flocalhost%2Fauth%2Fredirect%2Fmsoidc&response_mode=form_post&scope=openid&nonce=678910",
    logoutURL:"https://login.microsoftonline.com/4a1ae6af-55d7-486c-a9dc-bd9693f8ebe9/oauth2/v2.0/logout?post_logout_redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fauth%2Flogout-redirect%2Fmsoidc"
  },
  { authMethod:"dlint",
    loginPageURL: "views/common/login.html",
    authData: "",
    logoutURL:"http://localhost:8080/auth/logout02"
  }
];
