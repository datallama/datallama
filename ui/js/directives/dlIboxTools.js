/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: dlIboxTools - Data Llama custom directive for iBox tools elements in right corner of ibox
 */
function dlIboxTools($timeout) {
    return {
        restrict: 'A',
        scope: {
            editable: '@',      // parameter: if true, the collapse,edit,save,cancel icons are available. if false, only the collapse icon is available.
            editing: '=',       // parameter: if specified, the state of edit,save,cancel icon visibility is set accordingly
            editFn: '&',
            saveFn: '&',
            cancelFn: '&'
        },
        templateUrl: 'views/directives/dl_ibox_tools.html',
        controller: function ($scope, $rootScope, $element) {

            $scope.dlIboxToolsName = $element[0].attributes.name.nodeValue;
            $scope.dlIboxToolsShowEditIcon = true;  // determines whether the edit icon or save/cancel icons are displayed
            $scope.dlIboxToolsEditable = false;     // determines whether edit/save/cancel icons are totally hidden or displayed

            if ($scope.editable == 'true') $scope.dlIboxToolsEditable = true;

            if ($scope.editing == true)
                $scope.dlIboxToolsShowEditIcon = false;

            /**
             * Description: collapse the ibox
             */
            $scope.dlIboxToolsShowhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };

            /**
             * Description: this function is invoked when the EDIT icon is clicked.
             */
            $scope.dlIboxToolsEdit = function () {
                $scope.dlIboxToolsShowEditIcon = !($scope.dlIboxToolsShowEditIcon);
                if($scope.editFn() != null) {
                    $scope.editFn()($scope.dlIboxToolsName);
                }
            }

            /**
             * Description: this function is invoked when the SAVE icon is clicked.
             */
            $scope.dlIboxToolsSave = function () {

                if($scope.saveFn() != null) {
                    let validationResult = $scope.saveFn()($scope.dlIboxToolsName);

                    // only reset the icons to the "ready" state if validation is successful
                    if (validationResult === 0) {
                        $scope.dlIboxToolsShowEditIcon = !($scope.dlIboxToolsShowEditIcon);
                    }
                }
            }

            /**
             * Description: this function is invoked when the CANCEL icon is clicked.
             */
            $scope.dlIboxToolsCancel = function () {
                $scope.dlIboxToolsShowEditIcon = !($scope.dlIboxToolsShowEditIcon);
                if($scope.cancelFn() != null) {
                    $scope.cancelFn()($scope.dlIboxToolsName);
                }
            }

        }
    };
}

/**
 * Pass function into module as a directive
 */
angular
    .module('datallama')
    .directive('dlIboxTools', dlIboxTools);
