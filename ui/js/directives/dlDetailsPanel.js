/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Description: dlDetailsPanel - a floating and resizable panel to display the metadata object details for a diagram element
 */
function dlDetailsPanel() {

    return {
        restrict: 'A',
        scope: {
            callback: '&onResize'
        },
        link: function postLink(scope, elem, attrs) {
            elem.draggable({containment: "#ab"});
            elem.resizable({handles: 'w', minWidth: 200});
            elem.on('resize', function (evt, ui) {
                scope.$apply(function () {
                    if (scope.callback) {
                        scope.callback({$evt: evt, $ui: ui});
                    }
                })
            });
        }
    };
}

/**
 * Pass function into module as a directive
 */
angular
    .module('datallama')
    .directive('dlDetailsPanel', dlDetailsPanel);
