/**
 * Copyright 2020 Data Llama FOSS Foundation Incorporated
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * The Inspinia theme uses the AngularUI Router to manage routing and views
 * Each view is defined as a state.
 * TODO: update the taxonomies states to be ontology
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider, IdleProvider) {
    // Get the login page configuration
    let loginPageURL = "views/common/login.html";
    let authData = "dlint";
    for (let i = 0; i < authConfig.length; i++) {
        if (authConfig[i].authMethod === authMethod) {
            loginPageURL = authConfig[i].loginPageURL;
            authData = authConfig[i].authData;
        }
    }

    $httpProvider.defaults.withCredentials = true;

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/logins");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('dummy', {
            abstract: true,
            url: "/dummy",
            templateUrl: "views/common/content.html"
        })
            .state('dummy.comingsoon', {
                url: "/dummy_comingsoon",
                templateUrl: "views/dummy/dummy_comingsoon.html",
                data: { pageTitle: 'Coming Soon' }
            })
        .state("logins", {
            url: "/logins",
            templateUrl: loginPageURL,
            data: { pageTitle: "Login", specialClass: "gray-bg" }
        })
        .state("home", {
            abstract: true,
            url: "/home",
            templateUrl: "views/common/content.html"
        })
            .state("home.overview", {
                url: "/overview",
                templateUrl: "views/common/home_overview.html",
                data: { pageTitle: "Data Llama Overview" },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                serie: true,
                                name: 'summernote',
                                files: ['css/plugins/summernote/summernote.css','css/plugins/summernote/summernote-bs3.css','js/plugins/summernote/summernote.min.js','js/plugins/summernote/angular-summernote.min.js']
                            },
                            {
                                serie: true,
                                files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                            },
                            {
                                serie: true,
                                name: 'datatables',
                                files: ['js/plugins/dataTables/angular-datatables.min.js']
                            },
                            {
                                serie: true,
                                name: 'datatables.buttons',
                                files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                            }
                        ]);
                    }
                }
            })
    ;   // end of StateProvider static setup


    // load the view configuration used for state transitions
    let stateDefinition = {};

    for (i = 0; i < stateConfig.length; i++) {
        stateDefinition = {};

        // get the release specific script name
        const templateUrl = stateConfig[i].templateUrl;
        const releaseNumber = releaseConfig.releaseNumber.replaceAll('.', '');
        const releaseTemplateUrl = templateUrl.substr(0, templateUrl.length - 5) + releaseNumber + ".html";

        stateDefinition.abstract = stateConfig[i].abstract;
        stateDefinition.url = stateConfig[i].url;
        stateDefinition.templateUrl = releaseTemplateUrl;
        stateDefinition.params = {invocationParams:{}};
        stateDefinition.data = stateConfig[i].data;
        $stateProvider.state(stateConfig[i].stateName, stateDefinition);
    }

}
angular
    .module('datallama')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
