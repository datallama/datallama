#!/usr/bin/python3
#
# Copyright 2020 Data Llama FOSS Foundation Incorporated
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import os, os.path, json
import shutil
from optparse import OptionParser
from glob import glob
from datetime import datetime

class DlUiBuild(object):
    # class members
    sSrcBase = ""
    sBuildOutputBase = ""
    sStateConfigFile = ""
    sStateConfig = ""


    '''
    Constructor: initialise the source code base directory
    '''
    def __init__(self):
        self.sSrcBase = os.path.dirname(os.path.realpath(__file__))

    '''
    setConfig defines the configuration items required to build the UI source code
    '''
    def setStateConfig(self):
        exitStatus = 0
        self.sStateConfigFile = self.sSrcBase + "/js/loaders/dlStateConfig.js"

    def parseStateConfig(self):
        exitStatus = 0
        if not(os.path.exists(self.sStateConfigFile)):
            print("ERROR: can't open state configuration file %s." % self.sStateConfigFile)
            exitStatus = 120

        if exitStatus == 0:
            fh = open(self.sStateConfigFile, "r")
            for line in fh.readlines():
                line = line.strip()
                if not (line.startswith("/") or line.startswith("*")):
                    if line.startswith("const stateConfig"):
                        self.sStateConfig = "["
                    elif line.startswith("];"):
                        self.sStateConfig = self.sStateConfig + "]"
                    else:
                        self.sStateConfig = self.sStateConfig + line
            print(self.sStateConfig)
            jStateConfig = json.loads(self.sStateConfig)


    '''
    readEnvConfig reads the DL UI environment settings either from the file specific in argument sEnvFileName or,
    if sEnvFileName is DEFAULT, from the file default_env.json which is assumed to be in the same dir as the execution script.
    '''
    def readEnvConfig(self, sEnvFileName):
        exitStatus = 0

        if (sEnvFileName.upper() == "DEFAULT"):
            sFileNameFullPath = self.sSrcBase + "/default_env.json"
        else:
            sFileNameFullPath = sEnvFileName

        if not(os.path.exists(sFileNameFullPath)):
            print("ERROR: can't open UI enviroment file %s." % sFileNameFullPath)
            exitStatus = 120

        if exitStatus == 0:
            fh = open(sFileNameFullPath, "r")
            jsonDoc = json.load(fh)
            fh.close()

            try:
                self.sBuildOutputBase = jsonDoc["build_output_base"]
                if not(self.sBuildOutputBase.endswith("/")):
                    self.sBuildOutputBase = self.sBuildOutputBase + "/"
            except KeyError:
                print("ERROR: failed to parse UI enviroment settings from JSON file %s." % sFileNameFullPath)
                exitStatus = 122

        return exitStatus

    '''
    Copy DL UI files from repository UI directory to target web server HTML home director
    '''
    def build(self):
        for dirname in next(os.walk('.'))[1]:
            sourceDir = self.sSrcBase + "/" + dirname
            targetDir = self.sBuildOutputBase + dirname
            if(os.path.exists(targetDir)):
                shutil.rmtree(targetDir)
            print("copying %s to %s." % (sourceDir, targetDir))
            shutil.copytree(sourceDir, targetDir)

        # copy the index file and favicon
        sourceFile = self.sSrcBase + "/index.html"
        targetFile = self.sBuildOutputBase + "index.html"
        print("copying %s to %s." % (sourceFile, targetFile))
        shutil.copy(sourceFile, targetFile)

        sourceFile = self.sSrcBase + "/favicon.png"
        targetFile = self.sBuildOutputBase + "favicon.png"
        print("copying %s to %s." % (sourceFile, targetFile))
        shutil.copy(sourceFile, targetFile)

        # generate the UI configuration file
        sourceFile = self.sBuildOutputBase + "js/dataLlamaConfig-template-host.js"
        targetFile = self.sBuildOutputBase + "js/dataLlamaConfig.js"
        print("Generating UI configuration file %s." % targetFile)
        shutil.copy(sourceFile, targetFile)

# call as stand alone script
if __name__ == "__main__":

    # parse command line options
    optParser = OptionParser(description='Builds the DL UI into the target directory indentified in the environment configuration file.')
    optParser.add_option("--env", dest="environment", help="specify an environment configuration file.")

    # parse environment option is provided
    envFile = "default"
    (options, args) = optParser.parse_args()
    if options.environment:
        envFile = options.environment

    uiBuild = DlUiBuild()
    exitStatus = 0
    #uiBuild.setConfig()
    #uiBuild.parseStateConfig()
    exitStatus = uiBuild.readEnvConfig(envFile)

    print(datetime.now().strftime("%d/%m/%Y T %H:%M"))
    if (exitStatus == 0):
        uiBuild.build()
