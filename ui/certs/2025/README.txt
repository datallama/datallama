The server-ca.crt and server.crt certificates are valid for 10 years.
There should be no need to regenerate certificats for the Data Llama containers before 2035.

Instructions for generating the self-signed CA and certificate are at https://datallama.atlassian.net/wiki/spaces/DAT/pages/1608384517/SSL+Cert+for+UI+Container
