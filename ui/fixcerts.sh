#!/usr/bin/env bash
SSL_DIR='/usr/local/apache2/conf/'
#PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && cd ../../.. && pwd )"
OUTPUT_DIR='ui/certs/'

usage(){
cat <<EOF
USAGE:

builds host certificate, assumes local build otherwise bitbucket pipeline build with secrets if defined,
and uses auth.{DOMAINNAME} as signing authority:

fixcerts.sh [ --pipeline ] [ --host HOSTNAME ] [ --domain DOMAINNAME ] [ --help ]
fixcerts.sh [ -p ] [ -H HOSTNAME ] [ -D DOMAINNAME ] [ -h ]

example: ./fixcerts.sh --pipeline --host=datacatalogue --domain=acme.com.au
         ./fixcerts.sh -p -H datacatalogue -D acme.com.au
         ./fixcerts.sh --help

EOF
}

TEMP=$(getopt -o plH:D:h \
    --long pipeline,local,host:,domain:,help \
    -n `basename $0` -- "$@")

if [ $? != 0 ] ; then usage >&2 ; exit 1 ; fi

eval set -- "$TEMP"
while true ; do
        case "$1" in
                -p|--pipeline)       BUILD="pipeline" ; shift ;;
                -H|--host)           HN="${2}" ;             shift 2;;
                -D|--domain)         DOMAIN="${2}" ;         shift 2;;
                -h|--help)           usage &&                exit 1 ;;
                --) shift ; break ;;
                *) usage && exit 1 ;;
        esac
done

BUILD=${BUILD:-local}
DOMAIN=${DOMAIN:-localdomain}
HN=${HN:-datallama}
FQHN=${HN}.${DOMAIN}

cat <<EOF
build_type=$BUILD
domain=$DOMAIN
hostname=$HN
fully_qualified_hostname=$FQHN
EOF

mkdir -p ${OUTPUT_DIR}

if [[ ${BUILD} == 'local' ]]
then
	(openssl verify -x509_strict -CAfile /tmp/server-ca.crt /tmp/server-ca.crt) ||
		openssl req \
		-x509 \
		-sha256 \
		-nodes \
		-days 365 \
		-newkey rsa:2048 \
		-keyout ${OUTPUT_DIR}server-ca.key \
		-out ${OUTPUT_DIR}server-ca.crt \
		-subj "/C=AU/ST=Vic/O=DATALLAMA CA Inc./CN=auth.${DOMAIN}"
elif [[ ${BUILD} == 'pipeline' ]]
then
	#these secret values must be stored in the bitbucket pipeline as base64 encoded values
	echo ${datallama_ca_crt} | base64 --decode >${OUTPUT_DIR}server-ca.crt
        echo ${datallama_ca_key} | base64 --decode >${OUTPUT_DIR}server-ca.key
else
	usage && exit 1;
fi

cat >${OUTPUT_DIR}req.conf <<EOF
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_ca
prompt = no

[req_distinguished_name]
C = AU
ST = VIC
L = Melbourne
O = Datallama Inc.
OU = Home
CN = ${FQHN}

[v3_ca]
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = DNS:${FQHN}
EOF

openssl req \
	-new \
	-nodes \
	-newkey rsa:2048 \
	-keyout ${OUTPUT_DIR}server.key \
	-out ${OUTPUT_DIR}server.csr \
	-config ${OUTPUT_DIR}req.conf \

openssl x509 \
	-req \
	-days 365 \
	-in ${OUTPUT_DIR}server.csr \
	-CA ${OUTPUT_DIR}server-ca.crt \
	-CAkey ${OUTPUT_DIR}server-ca.key \
	-set_serial 01 \
	-out ${OUTPUT_DIR}server.crt \
	-extensions v3_ca \
	-extfile ${OUTPUT_DIR}req.conf
