# Data Llama

Data Llama is a next generation data governance tool that is used to collect metadata about your organisation's data
 landscape and to create associations between metadata objects to provide a comprehensive Data Landscape Map.
 
You can generate metadata about the structure of:

 - databases
 - file systems
 - JSON and XML file or message formats
 - REST APIs
 - any flat or hierarchical file or message format

You can create a business ontology comprising:

 - taxonomies
 - glossaries
 - catalogues
 - dictionaries
 - attribute domains
 - general lists
 
You can associate any definition in your business ontology with any structural metadata object to create semantic definition for all the artefacts that constitute your Enterprise Data Landscape.

# How to Contribute
Data Llama is an Open Source project licensed under the GPLv3. If you would like to contribute to Data Llama please send an email to ![contribution email](http://www.datallama.io/contributeEmail.png)
