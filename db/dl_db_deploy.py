#!/usr/bin/python3
#
# Copyright 2020 Data Llama FOSS Foundation Incorporated
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#
# Description: deploys a single task
#

import os, os.path, sys, shutil, json, subprocess
from optparse import OptionParser

from glob import glob

class DlDbDeploy(object):

    sSrcBase = ""
    sDbHost = ""
    sApiHost = ""
    sDbName = ""
    sDbUsr = ""
    sDbPwd = ""
    sDbDeployUsr = ""
    sDbDeployPwd = ""

    '''
        Constructure initialises the source base directory
    '''
    def __init__(self):
        self.sSrcBase = os.path.dirname(os.path.realpath(__file__))
        #self.sSrcBase = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

    '''
        sets the database deploy username. This must be a user with permissions to create and drop users and databases or schema
    '''
    def setDbDeployUser(self, username):
        self.sDbDeployUsr = username

    '''
        sets the database deploy user password.
    '''
    def setDbDeployPassword(self, password):
        self.sDbDeployPwd = password

    '''
        readEnvConfig reads the Database environment settings either from the file specific in argument sEnvFileName or,
        if sEnvFileName is DEFAULT, from the file default_env.json which is assumed to be in the same dir as the execution script.
    '''
    def readEnvConfig(self, sEnvFileName):
        exitStatus = 0

        if (sEnvFileName.upper() == "DEFAULT"):
            sFileNameFullPath = self.sSrcBase + "/default_env.json"
            #sFileNameFullPath = self.sSrcBase + "/build-deploy/bin/default_env.json"
        else:
            sFileNameFullPath = sEnvFileName

        if not(os.path.exists(sFileNameFullPath)):
            print("ERROR: can't open DB enviroment file %s." % sFileNameFullPath)
            exitStatus = 120

        if exitStatus == 0:
            fh = open(sFileNameFullPath, "r")
            jsonDoc = json.load(fh)
            fh.close()

            try:
                self.sDbHost = jsonDoc["dbhost"]
                self.sApiHost = jsonDoc["apihost"]
                self.sDbName = jsonDoc["dbname"]
                self.sDbUsr = jsonDoc["dbusr"]
                self.sDbPwd = jsonDoc["dbpwd"]
                self.sDbDeployUsr = jsonDoc["dbdeployusr"]
                self.sDbDeployPwd = jsonDoc["dbdeploypwd"]
            except KeyError:
                print("ERROR: can't load all DB enviroment settings from file %s." % sFileNameFullPath)
                exitStatus = 122

        if exitStatus == 0:
            # check that the mysql client is available
            cmd = ["mysql", "--user=root", "--execute=quit"]
            if len(self.sDbDeployPwd)  > 0:
                cmd.append("--password=%s" % self.sDbDeployPwd)
            try:
                output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
            except subprocess.CalledProcessError as exc:
                #print("Status : Login failed but database cli utility found.", exc.returncode, exc.output)
                # check whether this is actually a login error
                print("found database cli utility")
            except OSError as exc:
                print("ERROR: could not execute command %s" % cmd[0])
                print(exc)
                exitStatus = 121

        if exitStatus == 0:
            print("found environment file and database cli utility")

        return exitStatus

    '''
        Builds all DL database objects into the database and primes tables based on the specified primeLevel
        which must be one of [INSTALL|DEMO|DEV]
    '''
    def fullDbDeploy(self, primeLevel):
        exitStatus = 0

        if os.path.exists("/tmp/tmp.sql"):
            try:
                os.remove("/tmp/tmp.sql")
            except:
                print("unable to delete file /tmp/tmp.sql")

        # execute bootstrap commands for DL dbo user and DL database
        fh = open("/tmp/tmp.sql", "w")
        fh.write("create user if not exists %s@'%s' identified by '%s';" % (self.sDbUsr, self.sApiHost, self.sDbPwd))
        fh.write("drop database if exists %s;" % self.sDbName)
        fh.write("create database %s;" % self.sDbName)
        fh.write("grant all privileges on %s.* to %s@'%s';" % (self.sDbName, self.sDbUsr, self.sApiHost))
        fh.write("grant select on mysql.proc TO %s@'%s';" % (self.sDbUsr, self.sApiHost))
        fh.write("SET GLOBAL log_bin_trust_function_creators = 1;")
        fh.close()

        cmd = ["mysql", "--user=root"]
        #cmd = ["mysql"]
        if len(self.sDbDeployUsr) > 0:
            cmd.append("--password=%s" % self.sDbDeployPwd)

        try:
            output = subprocess.check_output(cmd, stdin=open("/tmp/tmp.sql", "r"), stderr=subprocess.STDOUT, universal_newlines=True)
        except subprocess.CalledProcessError as exc:
            print("ERROR: could not execute bootstrap sql commands for user and database.")
            print(exc.output)
            exitStatus = 123
        except OSError as exc:
            print("ERROR: could not execute command %s" % cmd[0])
            print(exc)
            exitStatus = 121

        # execute all table DDL scripts
        if exitStatus == 0:
            sqlScriptSearch = self.sSrcBase + "/table/*.my-tbl"
            allSqlScripts = sorted(glob(sqlScriptSearch))

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "table")
                if result > 0:
                    exitStatus = result

        # execute all index DDL scripts
        if exitStatus == 0:
            sqlScriptSearch = self.sSrcBase + "/index/*.my-ndx"
            allSqlScripts = glob(sqlScriptSearch)

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "index")
                if result > 0:
                    exitStatus = result

        # execute all view DDL scripts
        if exitStatus < 140:
            sqlScriptSearch = self.sSrcBase + "/view/*.my-view"
            allSqlScripts = glob(sqlScriptSearch)

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "view")
                if result > 0:
                    exitStatus = result

        # execute all stored procedure DDL scripts
        if exitStatus < 140:
            sqlScriptSearch = self.sSrcBase + "/proc/"
            allSqlScripts = [os.path.join(dirpath, f)
                           for dirpath, dirnames, files in os.walk(sqlScriptSearch)
                           for f in files if f.endswith('.my-proc')]

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "procedure")
                if result > 0:
                    exitStatus = result

        # execute all function DDL scripts
        if exitStatus < 140:
            sqlScriptSearch = self.sSrcBase + "/func/"
            allSqlScripts = [os.path.join(dirpath, f)
                             for dirpath, dirnames, files in os.walk(sqlScriptSearch)
                             for f in files if f.endswith('.my-func')]

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "function")
                if result > 0:
                    exitStatus = result

        # execute all application priming DML scripts
        if exitStatus < 140:
            allSqlScripts = list()
            if primeLevel == "INSTALL":
                # use configuration script base_install_prime.txt for allSqlScripts
                filename = self.sSrcBase + "/table-prime/base_install_prime.txt"    # TODO: check that file exists
                fh = open(filename, "r")
                for line in fh:
                    scriptFullPath = self.sSrcBase + "/table-prime/" + line.strip()
                    allSqlScripts.append(scriptFullPath)
            elif primeLevel == "DEMO":
                # use configuration script demo_prime.txt for allSqlScripts
                filename = self.sSrcBase + "/table-prime/demo_prime.txt"    # TODO: check that file exists
                fh = open(filename, "r")
                for line in fh:
                    scriptFullPath = self.sSrcBase + "/table-prime/" + line.strip()
                    allSqlScripts.append(scriptFullPath)
            elif primeLevel == "DEV":
                # use configuration script demo_prime.txt for allSqlScripts
                sqlScriptSearch = self.sSrcBase + "/table-prime/*.my-sql"
                allSqlScripts = sorted(glob(sqlScriptSearch))

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "insert")
                if result > 0:
                    exitStatus = result

        # execute all foreign key DDL scripts
        if exitStatus < 140:
            sqlScriptSearch = self.sSrcBase + "/fk/*.my-fk"
            allSqlScripts = glob(sqlScriptSearch)

            for sqlScript in allSqlScripts:
                result = self.executeSqlScript(sqlScript, "fk")
                if result > 0:
                    exitStatus = result


        # clean up temporary files
        if os.path.exists("/tmp/tmp.sql"):
            try:
                os.remove("/tmp/tmp.sql")
            except:
                print("unable to delete file /tmp/tmp.sql")

        return exitStatus

    '''
        deployDBsql executes the SQL script referenced by sFileName using the
    '''
    def deployDBsql(self, sFileName, sFilePath):
        exitStatus = 0

        sFileNameFullPath = self.sSrcBase + "/" + sFileName

        sqlfilearg = "< " + sFileNameFullPath
        cmd = ["mysql", "--user=root", "--execute=quit", sqlfilearg]
        if len(self.sDbDeployUsr) > 0:
            cmd.append("--password=%s" % self.sDbDeployPwd)
        try:
            output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
        except subprocess.CalledProcessError as exc:
            print("Status : Login failed but database cli utility found.", exc.returncode, exc.output)
            # check whether this is actually a login error
        except OSError as exc:
            print("ERROR: could not execute command %s" % cmd[0])
            print(exc)
            exitStatus = 121

        print("Running script " + sFileName)

        return exitStatus

    '''
        Executes the SQL script referenced by sFullFileName using the appropriate database cli utility
    '''
    def executeSqlScript(self, sFullFileName, sScriptType):
        exitStatus = 0

        scriptName = sFullFileName

        if sScriptType == "procedure" or sScriptType == "function":
            if os.path.exists("/tmp/tmp2.sql"):
                try:
                    os.remove("/tmp/tmp2.sql")
                except:
                    print("unable to delete file /tmp/tmp2.sql")

            fh = open(sFullFileName, "r")
            sqlcmd = fh.read()
            fh.close()

            fh = open("/tmp/tmp2.sql", "w")
            fh.write("delimiter $$\n")
            fh.write(sqlcmd)
            fh.write("\n$$")
            fh.close()

            scriptName = "/tmp/tmp2.sql"

        cmd = ["mysql", "--user=" + self.sDbUsr, "--password=" + self.sDbPwd, "--database=" + self.sDbName]
        try:
            output = subprocess.check_output(cmd, stdin=open(scriptName, "r"), stderr=subprocess.STDOUT,
                                             universal_newlines=True)
            print("Successfully executed sql %s script %s" % (sScriptType, sFullFileName))
        except subprocess.CalledProcessError as exc:
            print("ERROR: could not execute sql %s script %s" % (sScriptType, sFullFileName))
            print(exc.output)
            exitStatus = 131
        except OSError as exc:
            print("ERROR: could not execute command %s" % cmd[0])
            print(exc)
            exitStatus = 132

        if os.path.exists("/tmp/tmp2.sql"):
            try:
                os.remove("/tmp/tmp2.sql")
            except:
                print("unable to delete file /tmp/tmp2.sql")

        return exitStatus


# call as stand alone script
if __name__ == "__main__":

    # parse command line options
    optParser = OptionParser(description='Builds database source scripts into the database indentified in environment details.')
    optParser.add_option("--env", dest="environment", help="specify a run environment for the netlLCrunController")
    optParser.add_option("--password", dest="password", help="specify the password for the database super user")
    optParser.add_option("--primelevel", dest="primelevel", help="specify the level of table prime that will be applied. [install|demo|dev]")

    dbDeployer = DlDbDeploy()
    dbDeployer.setDbDeployUser("root")  # this is hard coded for mysql at present

    # parse environment option is provided
    envFile = "default"
    primeLevel = "INSTALL"
    (options, args) = optParser.parse_args()
    if options.environment:
        envFile = options.environment
    if options.password:
        dbDeployer.setDbDeployPassword(options.password)
    if options.primelevel:
        primeLevel = options.primelevel.upper()
        if not(primeLevel == "INSTALL" or primeLevel == "DEMO" or primeLevel == "DEV"):
            primeLevel = "INSTALL"

    print("primeLevel = " + primeLevel)

    # read configuration file
    result = dbDeployer.readEnvConfig(envFile)

    # peform full deploy
    if result == 0:
        result = dbDeployer.fullDbDeploy(primeLevel)

    if result > 0:
        print("\nERRORS: one or more errors encountered during task deploy")
    else:
        print("\nSUCCESS: no errors encountered during task deploy")
