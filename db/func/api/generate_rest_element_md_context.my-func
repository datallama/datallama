--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: generates the metadata context for the XML element identified by _object_id
--
create function generate_rest_element_md_context(_object_id integer)
returns json
deterministic
begin
    declare v_element_internalpath varchar(2048);
    declare v_parent_element_object_id integer;
    declare v_element_name varchar(150);
    declare v_element_class_id integer;
    declare v_element_class_name varchar(150);
    declare v_parent_class_id integer;
    declare v_parent_class_name varchar(150);
    declare v_parent_object_id integer;
    declare v_parent_object_name varchar(150);
    declare v_context json;
    declare v_parent_element_context json;

    -- 1. get details of current XML element including the internal path
    select
        x.element_name, x.parent_element_object_id, x.class_id, c.class_name, x.parent_object_id
        into v_element_name, v_parent_element_object_id, v_element_class_id, v_element_class_name, v_parent_object_id
    from
        dl_api_rest_detail x
        join dl_class_hierarchy c on x.class_id = c.class_id
    where
        object_id = _object_id;

    if (v_parent_element_object_id > 0) then
        select md_context into v_parent_element_context
        from dl_api_rest_detail
        where object_id = v_parent_element_object_id;

        set v_element_internalpath = json_value(json_extract(v_parent_element_context, '$[1]'), '$.internal_path');
        set v_element_internalpath = concat(v_element_internalpath, '.', v_element_name);
    else
        set v_element_internalpath = v_element_name;
    end if;

    -- 2: get details of the parent
    select
        c.class_id, c.class_name, a.object_name
        into v_parent_class_id, v_parent_class_name, v_parent_object_name
    from
        dl_api a
        join dl_class_hierarchy c on a.class_id = c.class_id
    where
        a.object_id = v_parent_object_id;

    set v_context = json_array(
                json_object('class_id', v_parent_class_id, 'class_name', v_parent_class_name,
                    'object_id', v_parent_object_id, 'object_name', v_parent_object_name),
                json_object('class_id', v_element_class_id, 'class_name', v_element_class_name,
                    'object_id', _object_id, 'object_name', v_element_name,
                    'internal_path', v_element_internalpath)
            );
    return json_compact(v_context);
end;
