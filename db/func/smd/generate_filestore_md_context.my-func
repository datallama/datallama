--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: generates the metadata context for a filestore identified by _filestore_id
-- Taxonomy is a parent and a child to none
-- As Taxonomy is a parent there is no v_filestore_fullpath_name
--

create function generate_filestore_md_context(_filestore_id integer)
returns json
begin
	declare v_filestore_fullpath_name varchar(2048);
	declare v_filestore_name varchar(150);
	declare v_filestore_class_id integer;
	declare v_filestore_class_name varchar(150);
	declare v_context json;

	-- Query dl_smd_filestore & dl_class
	select
		c.class_id, c.class_name, t.filestore_name
		into v_filestore_class_id, v_filestore_class_name, v_filestore_name
	from
		dl_smd_filestore t
		join dl_class_hierarchy c on t.class_id = c.class_id
	where
		t.filestore_id = _filestore_id;

	-- Parent's only have a single json object
	set v_context = json_array(
		json_object(
			'class_id', v_filestore_class_id,
			'class_name', v_filestore_class_name,
			'object_id', _filestore_id,
			'object_name', v_filestore_name
		)
	);

	return json_compact(v_context);
end;
