--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: generates the metadata context for a filestore directory identified by _directory_id
--

create function generate_filestore_directory_md_context(_directory_id integer)
returns json
begin
    declare v_directory_fullpath_name varchar(2048);
    declare v_directory_parent_id integer;
    declare v_directory_name varchar(150);
    declare v_directory_class_id integer;
    declare v_directory_class_name varchar(150);
    declare v_base_directory_name varchar(150);

    declare v_parent_class_id integer;
    declare v_parent_class_name varchar(150);
    declare v_parent_object_id integer;
    declare v_parent_object_name varchar(150);

    declare v_context json;

    select
        te.directory_name, te.directory_parent_id, te.class_id, c.class_name
        into v_base_directory_name, v_directory_parent_id, v_directory_class_id, v_directory_class_name
    from
        dl_smd_filestore_directory te
        join dl_class_hierarchy c on te.class_id = c.class_id
    where
        directory_id = _directory_id;

    set v_directory_fullpath_name = v_base_directory_name;

    while v_directory_parent_id > 0 do
        select
            directory_name, directory_parent_id
            into v_directory_name, v_directory_parent_id
        from
            dl_smd_filestore_directory
        where
            directory_id = v_directory_parent_id;

        set v_directory_fullpath_name = concat(v_directory_name, '.', v_directory_fullpath_name);
    end while;

    -- second, get details of the parent
    select
        c.class_id, c.class_name, t.filestore_id, t.filestore_name
        into v_parent_class_id, v_parent_class_name, v_parent_object_id, v_parent_object_name
    from
        dl_smd_filestore t
        join dl_class_hierarchy c on t.class_id = c.class_id
        join dl_smd_filestore_directory te on t.filestore_id = te.filestore_id
    where
        te.directory_id = _directory_id;

    set v_context = json_array(
                json_object('class_id', v_parent_class_id, 'class_name', v_parent_class_name,
                    'object_id', v_parent_object_id, 'object_name', v_parent_object_name),
                json_object('class_id', v_directory_class_id, 'class_name', v_directory_class_name,
                    'object_id', _directory_id, 'object_name', v_base_directory_name,
                    'internal_path', v_directory_fullpath_name)
            );

    return json_compact(v_context);
end;
