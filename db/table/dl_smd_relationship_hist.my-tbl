--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_smd_relationship_hist stores the history of changes made to
--  Structural Metadata (SMD) Relationships stored in table dl_smd_relationship.

create table if not exists dl_smd_relationship_hist (
    archive_ts                  timestamp default current_timestamp,
    change_action_flag          char(1) not null,
    smd_relationship_id         integer not null,
    smd_database_id             integer not null,
    smd_schema_id               integer not null,
    class_id                    integer not null,
    reveng_run_id               integer null,
    parent_table_view_id        integer not null,
    child_table_view_id         integer not null,
    smd_relationship_name       varchar(200) not null,
    smd_relationship_shortdesc  varchar(1000) null,
    create_mode                 tinyint not null default 10,
    md_context                  json null,
    last_change_ts              datetime not null,
    last_change_user_id         integer not null
) engine=innoDB;
