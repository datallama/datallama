--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_smd_hierarchy_detail_hist stores the archive of changes/deletes to the elements
--  of a structural metadata hierarchy.

create table if not exists dl_smd_hierarchy_detail_hist (
    archive_ts                  timestamp default current_timestamp,
    change_action_flag          char(1) not null,
    change_user_id              integer null default 0,
    object_id                   integer not null,
    class_id                    integer not null,
    parent_object_id            integer not null,
    parent_class_id             integer not null,
    parent_element_object_id    integer not null,
    element_level               tinyint unsigned not null,
    element_level_sequence      smallint unsigned not null,
    element_is_parent           boolean null,
    reveng_run_id               integer null,
    reveng_run_count            smallint unsigned null,
    element_hierarchy_number    varchar(100) not null,
    element_type                varchar(50) null,
    object_name                 varchar(150) not null,
    object_guid                 varchar(40) not null,
    object_shortdesc            varchar(500) null,
    object_desc_reveng          varchar(500) null,
    object_config               json null,
    md_context                  json null,
    last_change_ts              timestamp,
    last_change_user_id         integer not null,
    prx_status                  tinyint null,
    data_sensitivity_id         integer null default 0,
    ds_assign_date              timestamp null,
    ds_assign_user_id           integer null,
    ds_approval_status          tinyint null,
    ds_approval_user_id         integer null
) engine=innoDB;
