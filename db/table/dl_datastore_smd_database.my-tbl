--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_datastore_smd_database stores the association between a datastore and one or more SMD databases.

create table if not exists dl_datastore_smd_database (
    ntt_id            integer not null,   -- FK to dl_datastore,
    class_id          integer not null,
    smd_database_id   integer not null,    -- FK to dl_smd_database
    last_change_ts  timestamp default current_timestamp,
    last_change_user_id    integer not null
) engine=innoDB;

create unique index dsd_unqidx on dl_datastore_smd_database(ntt_id, smd_database_id);
