--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: stores the history of changes made to flat file format elements stored in dl_fmt_flatfile_element

create table if not exists dl_smd_flatfile_detail_hist (
    archive_ts          timestamp not null default current_timestamp,
    action_flag         char(1) not null,
    archive_user_id     integer not null,
    object_id           integer not null,
    class_id            integer not null,
    parent_object_id    integer not null,
    parent_class_id     integer not null,
    element_sequence    smallint unsigned not null,
    element_datatype    varchar(50) not null,
    reveng_run_id       integer null default 0,
    reveng_run_count    smallint unsigned null default 0,
    fwf_start_pos       smallint unsigned not null,
    fwf_length          smallint unsigned not null,
    object_name         varchar(150) not null,
    object_shortdesc    varchar(500) null,
    object_guid         varchar(40) null,
    object_desc_reveng  varchar(500) null,
    md_context          json null,
    last_change_ts      timestamp default current_timestamp,
    last_change_user_id integer not null,
    prx_status          tinyint null default 0, -- used when determining reveng reconciliation status
    data_sensitivity_id     integer null default 0,
    ds_assign_date          timestamp null,
    ds_assign_user_id       integer null,
    ds_approval_status      tinyint null,   -- 10 = suggested, 20 = in review, 30 = approved
    ds_approval_user_id     integer null
);

