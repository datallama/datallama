--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_persistent_technology is a reference data hierarchy that stores a taxonomy of persistence technologies

create table if not exists dl_persistence_technology (
    pt_id               integer not null auto_increment,
    class_id	integer not null,
    pt_parent_id        integer not null default 0,
    pt_level            tinyint unsigned not null,
    pt_level_sequence   smallint unsigned not null,
    pt_hierarchy_number varchar(50) not null,
    pt_name             varchar(150) not null,
    pt_desc             varchar(300) null,
    last_change_ts  timestamp default current_timestamp,
    last_change_user_id    integer not null,
    primary key (pt_id)
) engine=innoDB;
