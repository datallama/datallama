--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: stores the history of changes made to table dl_smd_column

create table if not exists dl_smd_column_hist (
    archive_ts              timestamp default current_timestamp,
    change_action_flag      char(1) not null,
    smd_column_id           integer not null,
    smd_table_view_id       integer not null,
    class_id                integer not null,
    reveng_run_id           integer null,
    smd_column_ordinal      integer not null,
    smd_primary_key         bit default 0,
    smd_column_datatype     varchar(200) not null,
    smd_column_name         varchar(200) not null,
    object_guid             varchar(40) not null,
    smd_column_shortdesc    varchar(1000) null,
    custom_form_data        json null,
    md_context              json null,
    last_change_ts          datetime not null,
    last_change_user_id     integer not null,
    data_sensitivity_id     integer null default 0,
    ds_assign_date          timestamp null,
    ds_assign_user_id       integer null,
    ds_approval_status      tinyint null,
    ds_approval_user_id     integer null
) engine=innoDB;
