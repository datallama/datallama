--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_landscape_object stores full details of data landscape objects.

create table if not exists dl_landscape_object (
    object_id               integer not null auto_increment,
    class_id                integer not null,
    hr_id                   integer not null default 0,
    object_name             varchar(150) not null,
    business_contact        varchar(50) null,
    technical_contact       varchar(50) null,
    object_shortdesc        varchar(500) null,
    class_attributes        json null,
    md_context              json null,
    last_change_ts          timestamp not null default current_timestamp,
    last_change_user_id     integer not null,
    color_defn			    varchar(500) not null default '{"background-color": "#FFFFFF"}',
    primary key (object_id)
) engine=innoDB;
