--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_abbr_scheme_detail stores the mappings between full term and abbreviation for an abbreviation scheme
--

create table if not exists dl_abbr_scheme_detail (
    abbr_id           integer not null auto_increment,
    abbr_scheme_id    integer not null,
    full_term         varchar(200) not null,
    abbreviation      varchar(40) not null,
    last_change_ts      timestamp default current_timestamp,
    last_change_user_id integer not null,
    primary key (abbr_id)
);
