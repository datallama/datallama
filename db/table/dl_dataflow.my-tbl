--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_dataflow stores details for all connectors with meta_class_id = 2, i.e. data flows.

create table if not exists dl_dataflow (
		object_id           integer not null auto_increment,
		class_id  		    integer not null,
		object_name		    varchar(150) not null,
		source_object_id	integer not null,
		target_object_id	integer not null,
		business_contact	varchar(50) null,
		technical_contact   varchar(50) null,
		object_shortdesc	varchar(500) null,
		class_attributes    json null,
		custom_form_data    json null,
		md_context          json null,
		last_change_ts	    timestamp default current_timestamp,
		last_change_user_id	integer not null,
		primary key (object_id)
) engine=innoDB;
