--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_smd_hierarchy_summary stores the summary definition of a structural metadata hierarchy.

create table if not exists dl_smd_hierarchy_summary (
    object_id           integer not null auto_increment,
    class_id            integer not null,
    abbr_scheme_id      integer not null default 0,
    custom_form_id      integer not null default 1,
    defn_method         tinyint null default 1,     -- indicates whether the object was defined manually or via a reverse engineer run.
    reveng_run_id       integer null default 0,
    reveng_run_count    smallint unsigned null default 0,
    object_name         varchar(150) not null,
    object_guid         varchar(40) not null,
    object_shortdesc    varchar(500) null,
    object_config       json null default json_object(),
    connector_id        integer null,
    credential_id       integer null,
    md_context          json null default json_object(),
    last_change_ts      timestamp default current_timestamp,
    last_change_user_id integer not null,
    primary key (object_id)
) engine=innoDB;
