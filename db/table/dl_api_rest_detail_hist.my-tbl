--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
create table if not exists dl_api_rest_detail_hist (
    archive_ts                  timestamp not null default current_timestamp,
    action_flag                 char(5) not null,
    archive_user_id             integer not null,
    object_id                   integer not null,
    class_id                    integer not null,
    parent_object_id            integer not null,
    parent_class_id             integer not null,
    parent_element_object_id    integer not null,
    element_level               tinyint unsigned not null,
    element_level_sequence      smallint unsigned not null,
    is_parent                   boolean null default false,
    reveng_run_id               integer null,
    reveng_run_count            smallint unsigned null,
    element_hierarchy_number    varchar(100) not null,
    element_name                varchar(150) not null,
    element_guid                varchar(40) not null,
    element_shortdesc           varchar(500) null,
    element_parameters          json null,
    http_method                 enum('None','CONNECT', 'DELETE', 'GET', 'HEAD', 'OPTIONS', 'PATCH', 'POST', 'PUT', 'TRACE') null,
    response_code               integer null,
    custom_form_data            json null,
    md_context                  json null,
    last_change_ts              datetime not null,
    last_change_user_id         integer not null,
    prx_status                  tinyint null default 0
) engine=innoDB;
