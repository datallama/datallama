--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_taxonomy_element stores the hierarchical relationship between elements in a taxonomy

create table if not exists dl_taxonomy_element (
    element_id              integer not null auto_increment,
    class_id                integer not null,
    taxonomy_id             integer not null,
    element_parent_id       integer not null,
    element_level           tinyint unsigned not null,
    element_level_sequence  smallint unsigned not null,
    element_is_parent       boolean null default false,
    element_hierarchy_number varchar(100) not null,
    element_name            varchar(150) not null,
    element_shortdesc       varchar(500) null,
    element_guid            varchar(40) null,
    md_context              json null,
    last_change_ts          timestamp default current_timestamp,
    last_change_user_id     integer not null,
    primary key (element_id)
) engine=innoDB;

