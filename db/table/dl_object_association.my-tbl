--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_association stores freeform relationships between DL objects

create table if not exists dl_object_association (
    assoc_id              integer not null auto_increment,
    source_class_id       integer not null,
    source_object_id      integer not null,
    source_md_context     json not null,
    target_class_id       integer not null,
    target_object_id      integer not null,
    target_md_context     json not null,
    assoc_shortdesc       varchar(500) null,
    assoc_type_id         integer not null,
    last_change_ts        timestamp default current_timestamp,
    last_change_user_id   integer not null,
  primary key (assoc_id)
) engine=innoDB;
