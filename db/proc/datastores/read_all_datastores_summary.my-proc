--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads summary information for all datastores.
--

create procedure read_all_datastores_summary()
begin

    select
        lo.object_id as datastore_id,
        lo.class_id,
        cl.class_name,
        lo.object_name as datastore_name,
        ifnull(lo.object_shortdesc, 'None Specified') as datastore_shortdesc,
        ifnull(lo.business_contact, 'None Specified') as business_contact,
        ifnull(lo.technical_contact, 'None Specified') as technical_contact
    from dl_landscape_object lo
        left outer join dl_hosted_realm hr on lo.hr_id = hr.hr_id
        left outer join dl_class_hierarchy cl on lo.class_id = cl.class_id;
end;
