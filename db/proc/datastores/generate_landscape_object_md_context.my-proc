--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: generates the metadata context for a taxonomy identified by _object_id
-- Taxonomy is a parent and a child to none
-- As Taxonomy is a parent there is no v_lansdcape_object_fullpath_name
--

create procedure generate_landscape_object_md_context (
    IN _object_id integer,
    IN _mode char(1)
)
begin
	declare v_object_name varchar(150);
	declare v_class_id integer;
	declare v_class_name varchar(150);
	declare v_md_context json;

	-- 1. get names for object and class
	select
		c.class_id, c.class_name, lo.object_name
		into v_class_id, v_class_name, v_object_name
	from
		dl_landscape_object lo
		join dl_class_hierarchy c on lo.class_id = c.class_id
	where
		lo.object_id = _object_id;

	-- 2. Landscape Object is a top level metadata object so has only a single json object in the md_context
	set v_md_context = json_array(
		json_object(
			'class_id', v_class_id,
			'class_name', v_class_name,
			'object_id', _object_id,
			'object_name', v_object_name
		)
	);

	update dl_landscape_object_vw
	    set md_context = json_compact(v_md_context)
    where object_id = _object_id;

	-- 3. update tables that duplicate the md_context if mode = 'U' (i.e. update md_context action)
	if (_mode = 'U') then
        call update_association_md_contexts(_object_id, v_class_id, v_md_context);
    end if;

end;