--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: updates a JSON node and all its descendents to implement a "move element" action.
--  The node to be moved is called the source element and is identified by _element_id
--  TODO: must incorporate userId for change audit
--

create procedure write_fmt_json_node_move(
  IN _object_id integer,
  IN _new_parent_object_id integer
)
begin
    declare v_move_to_top_level boolean default false;
    declare v_format_id integer;
    declare v_source_level integer;
    declare v_source_level_sequence integer;
    declare v_source_hierarchy_number varchar(50);
    declare v_source_parent_object_id integer;
    declare v_source_parent_child_count integer;

    declare v_target_level integer;
    declare v_target_level_sequence integer;
    declare v_target_hierarchy_number varchar(50);

    declare v_new_level integer;
    declare v_new_level_sequence integer;
    declare v_new_hierarchy_number varchar(50);

    declare v_object_id integer;
    declare v_hierarchy_number_match varchar(50);

    -- the level difference reflects the change in level for source element
    -- and can be applied to all descendants
    declare v_level_diff integer;

    declare v_done tinyint;
    declare csr_update_md_contexts cursor for
        select object_id
        from dl_fmt_json_detail_vw
        where parent_object_id = v_format_id
            and left(element_hierarchy_number, length(v_hierarchy_number_match)) = v_hierarchy_number_match;
    declare continue handler for not found set v_done = 1;

    -- 1. determine whether this is a move to the top level
    if (_new_parent_object_id = 0) then
        set v_move_to_top_level = true;
    end if;

    -- 2. capture the current state attributes of the source element
    select fmt_id, json_node_level, json_node_level_sequence,
           json_node_hierarchy_number, parent_object_id
        into v_format_id, v_source_level, v_source_level_sequence,
            v_source_hierarchy_number, v_source_parent_object_id
    from dl_fmt_json_detail
    where json_node_id = _object_id;

    -- 3. capture the current state attributes of the target element
    if (v_move_to_top_level) then
        select -1, max(json_node_level_sequence), ''
            into v_target_level, v_target_level_sequence, v_target_hierarchy_number
        from dl_fmt_json_detail
        where fmt_id = v_format_id;
    else
        select json_node_level, json_node_level_sequence, json_node_hierarchy_number
            into v_target_level, v_target_level_sequence, v_target_hierarchy_number
        from dl_fmt_json_detail
        where json_node_id = _new_parent_object_id;
    end if;

    /** 4. calculate new values for the element being moved **/
    set v_new_level = v_target_level + 1;

    select ifnull(max(json_node_level_sequence), 0) + 1 into v_new_level_sequence
    from dl_fmt_json_detail
    where parent_object_id = _new_parent_object_id;

    if (v_move_to_top_level) then
        set v_new_hierarchy_number = lpad(v_new_level_sequence, 3, '0');
    else
        set v_new_hierarchy_number = concat(v_target_hierarchy_number, '.', lpad(v_new_level_sequence, 3, '0'));
    end if;

    set v_level_diff = v_new_level - v_source_level;

    -- 5. update the source element with new level, sequence, hierarchy number
    update dl_fmt_json_detail
    set parent_object_id = _new_parent_object_id,
        json_node_level = v_new_level,
        json_node_level_sequence = v_new_level_sequence,
        json_node_hierarchy_number = v_new_hierarchy_number
    where json_node_id = _object_id;

    -- 6. update the target element to be a parent element ... just it case it isn't already
    update dl_fmt_json_detail
    set json_node_is_parent = 1
    where json_node_id = _new_parent_object_id;

    -- 7. update the md_context for the source element
    call generate_json_node_md_context(_object_id, 'U');

    /** 8. update all descendents of source element **/
    select concat(element_hierarchy_number, '.') into v_hierarchy_number_match
    from dl_fmt_ghmf_detail
    where object_id = _object_id;

    open csr_update_md_contexts;
    propagate_update: loop
        fetch csr_update_md_contexts into v_object_id;
        if v_done = 1 then leave propagate_update; end if;

        call generate_json_node_md_context(v_object_id, 'U');
    end loop;
    close csr_update_md_contexts;

    update dl_fmt_json_detail
    set json_node_level = json_node_level + v_level_diff,
        json_node_hierarchy_number = concat(v_new_hierarchy_number, right(json_node_hierarchy_number, length(json_node_hierarchy_number) - length(v_source_hierarchy_number)))
    where fmt_id = v_format_id
        and left(json_node_hierarchy_number, length(v_source_hierarchy_number)) = v_source_hierarchy_number;

    -- 9. update the element_is_parent value for the old parent of the source element, it case it is no longer a parent.
    if v_source_parent_object_id > 0 then
        select count(*) into v_source_parent_child_count
        from dl_fmt_json_detail
        where parent_object_id = v_source_parent_object_id;

        if v_source_parent_child_count = 0 then
            update dl_fmt_json_detail
            set json_node_is_parent = 0
            where json_node_id = v_source_parent_object_id;
        end if;
    end if;
end;

