--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes a new element to a General Hierarchical Message Format (GHMF) definition.
--

create procedure write_fmt_ghmf_element (
    IN _user_id     integer,
    IN _object_id   integer,
    IN _class_id    integer,
    IN _fmt_id      integer,
    IN _parent_object_id   integer,
    IN _element_type_id    integer,
    IN _element_name       varchar(150),
    IN _element_shortdesc  varchar(500)
)
begin
    declare v_return_id integer;
    declare v_level tinyint unsigned;
    declare v_level_sequence smallint unsigned;
    declare v_hierarchy_number varchar(50);
    declare v_object_id integer;
    declare v_object_name varchar(150);

    declare v_done tinyint;

    declare csr_update_md_contexts cursor for
        select object_id
        from dl_fmt_ghmf_detail
        where fmt_id = _fmt_id
            and left(element_hierarchy_number, length(v_hierarchy_number)) = v_hierarchy_number;

    declare continue handler for not found set v_done = 1;


    if (_object_id < 0) then
        -- 1. this is a new GHMF element so insert into dl_fmt_json_detail
        if (_parent_object_id = 0) then
            -- 1.1. this is a new top level node in the json format
            select
                ifnull(max(element_level_sequence), 0) + 1 into v_level_sequence
            from
                dl_fmt_ghmf_detail
            where
                element_level = 0
                and fmt_id = _fmt_id;

            set v_hierarchy_number = lpad(v_level_sequence, 3, '0');
            insert into dl_fmt_ghmf_detail (
                class_id,
                fmt_id,
                element_type_id,
                parent_object_id,
                element_level,
                element_level_sequence,
                element_hierarchy_number,
                element_name,
                element_shortdesc,
                element_guid,
                last_change_user_id
            )
            values (
                _class_id,
                _fmt_id,
                _element_type_id,
                _parent_object_id,
                0,
                v_level_sequence,
                v_hierarchy_number,
                _element_name,
                _element_shortdesc,
                uuid(),
                _user_id
            );

        else
            -- 1.2. this is a new GHMF element at a subordinate level in the hierarchy

            -- 1.2.1. determine the level based on level of parent
            select element_level + 1 into v_level
            from   dl_fmt_ghmf_detail
            where  object_id = _parent_object_id;

            -- 1.2.2 generate hierarchy number based on parent hierarchy number and current level sequence
            select element_hierarchy_number into v_hierarchy_number  -- get hierarchy number for parent
            from   dl_fmt_ghmf_detail
            where  object_id = _parent_object_id;

            select ifnull(max(element_level_sequence), 0) + 1 into v_level_sequence  -- get max sequence at current level
            from   dl_fmt_ghmf_detail
            where  parent_object_id = _parent_object_id;

            set v_hierarchy_number = concat(v_hierarchy_number, '.', lpad(v_level_sequence, 3, '0'));

            insert into dl_fmt_ghmf_detail (
                class_id,
                fmt_id,
                element_type_id,
                parent_object_id,
                element_level,
                element_level_sequence,
                element_hierarchy_number,
                element_name,
                element_shortdesc,
                element_guid,
                last_change_user_id
            )
            values (
                _class_id,
                _fmt_id,
                _element_type_id,
                _parent_object_id,
                v_level,
                v_level_sequence,
                v_hierarchy_number,
                _element_name,
                _element_shortdesc,
                uuid(),
                _user_id
            );

            -- 1.2.3. update the parent element element_is_parent status
            update dl_fmt_ghmf_detail
            set element_is_parent = true
            where object_id = _parent_object_id;
        end if;

        set v_return_id = last_insert_id();

        -- generate the md_context
        call generate_ghmf_element_md_context(v_return_id, 'C');

    else
        -- 2. this is an existing taxonomy element so UPDATE the row accordingly
        select element_name, concat(element_hierarchy_number, '.') into v_object_name, v_hierarchy_number
        from dl_fmt_ghmf_detail
        where object_id = _object_id;

        update dl_fmt_ghmf_detail
        set element_name = _element_name,
            element_type_id = _element_type_id,
            element_shortdesc = _element_shortdesc
        where
            object_id = _object_id;

        -- 2.1. update the md_context if the name has changed
        if (_element_name != v_object_name) then
            call generate_ghmf_element_md_context(_object_id, 'U');

            open csr_update_md_contexts;

            propagate_update: loop
                fetch csr_update_md_contexts into v_object_id;
                if v_done = 1 then leave propagate_update; end if;

                call generate_ghmf_element_md_context(v_object_id, 'U');
            end loop;
            close csr_update_md_contexts;

        end if;

        set v_return_id = _object_id;
    end if;

    select
        fmt_id,
        object_id,
        class_id,
        element_type_id,
        parent_object_id,
        element_level,
        element_level_sequence,
        element_hierarchy_number,
        element_name,
        element_shortdesc,
        element_guid,
        md_context
    from dl_fmt_ghmf_detail
    where object_id = v_return_id;
end;
