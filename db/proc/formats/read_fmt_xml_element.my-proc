--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the detail of the xml element identified by _object_id
--

create procedure read_fmt_xml_element(
    IN _object_id integer
)
begin

    select
        object_id,
        class_id,
        parent_object_id,
        parent_class_id,
        parent_element_object_id,
        xml_element_type,
        xml_element_level,
        xml_element_level_sequence,
        xml_element_hierarchy_number,
        xml_element_name,
        ifnull(xml_element_shortdesc, 'No short description provided.') as xml_element_shortdesc,
        md_context,
        ifnull(element_guid, 'No GUID set') as element_guid

    from
        dl_fmt_xml_detail
    where
        object_id = _object_id;

end;
