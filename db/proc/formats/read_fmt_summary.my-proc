--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the summary information for the format identified by _object_id from either dl_smd_flatfile_summary
--  or dl_smd_hierarchy_summary
--

create procedure read_fmt_summary (
    IN _object_id integer,
    IN _class_id_in integer,
    OUT _object_name varchar(150),
    OUT _object_guid varchar(40),
    OUT _defn_method tinyint,
    OUT _class_id integer,
    OUT _class_name varchar(150),
    OUT _object_shortdesc varchar(500),
    OUT _object_config json,
    OUT _abbr_scheme_id integer,
    OUT _custom_form_id integer,
    OUT _custom_form_name varchar(150),
    OUT _schema_definition json,
    OUT _form_definition json,
    OUT _connector_id integer,
    OUT _credential_id integer,
    OUT _md_context json,
    OUT _class_config_id integer,
    OUT _config_schema_defn json,
    OUT _config_form_defn json
)
begin
    declare v_class_object_table varchar(70);

    select object_table into v_class_object_table
    from dl_class_hierarchy
    where class_id = _class_id_in;

    if (v_class_object_table = 'dl_smd_flatfile_summary') then
        call read_fmt_flatfile_summary(_object_id, _object_name, _object_guid, _defn_method,
            _class_id, _class_name, _object_shortdesc, _object_config,
            _abbr_scheme_id, _custom_form_id, _custom_form_name,
            _schema_definition, _form_definition, _connector_id,
            _credential_id, _md_context, _class_config_id,
            _config_schema_defn, _config_form_defn);
    end if;
    if (v_class_object_table = 'dl_smd_hierarchy_summary') then
        call read_smd_hierarchy_summary(_object_id, _object_name, _object_guid, _defn_method,
            _class_id, _class_name, _object_shortdesc, _object_config,
            _abbr_scheme_id, _custom_form_id, _custom_form_name,
            _schema_definition, _form_definition, _connector_id,
            _credential_id, _md_context, _class_config_id,
            _config_schema_defn, _config_form_defn);
    end if;

end;
