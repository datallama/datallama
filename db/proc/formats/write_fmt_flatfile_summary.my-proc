--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: creates a new message flatfile format definition or updates an existing one.
--

create procedure write_fmt_flatfile_summary (
    IN _user_id integer,
    IN _object_id integer,
    IN _class_id integer,
    IN _object_name varchar(150),
    IN _object_shortdesc varchar(500),
    IN _object_guid varchar(40),
    IN _object_config json,
    IN _defn_method tinyint,
    OUT _new_object_id integer
)
begin
    declare v_exists tinyint;
    declare v_class_code varchar(50);
    declare v_object_guid varchar(40);
    declare v_defn_method tinyint;
    declare v_object_config json default json_object();

    if (_object_id = 0) then
        -- 1. This is a new format definition.

        -- 1.1. check whether a format with the same name already exists. This can occur when a reverse engineer run is repeated
        select ifnull(max(object_id), 0) into v_exists
        from dl_smd_flatfile_summary
        where object_name = _object_name;

        if (v_exists = 0) then
            select class_code into v_class_code
            from dl_class_hierarchy
            where class_id = _class_id;

            if v_class_code = 'DELIMITED_FLATFILE' then
                set v_object_config = json_object('field_delimiter', '', 'line_terminator', '', 'string_delimiter', '', 'text_binary', '', 'character_set', '');
            elseif v_class_code = 'FIXED_WIDTH_FLATFILE' then
                set v_object_config = json_object('text_binary', '', 'character_set', '');
            end if;

            if (_defn_method is null) then
                set v_defn_method = 1;
            else
                set v_defn_method = _defn_method;
            end if;

            if (_object_guid is null or length(_object_guid) = 0) then
                set v_object_guid = uuid();
            else
                -- TODO: check that user specified GUID doesn't exist already in table.
                set v_object_guid = _object_guid;
            end if;

            insert into dl_smd_flatfile_summary (
                class_id,
                defn_method,
                object_name,
                object_shortdesc,
                object_config,
                custom_form_id,
                last_change_user_id
            )
            values (
                _class_id,
                _defn_method,
                _object_name,
                _object_shortdesc,
                v_object_config,
                1,
                _user_id
            );
            set _new_object_id = last_insert_id();

            -- generate the metadata context
            call generate_flatfile_summary_md_context(_new_object_id, 'C');
        else
            set _new_object_id = v_exists;
            -- because this could be a reverse engineer run, update the create_source
            update dl_smd_flatfile_summary
            set defn_method = _defn_method
            where object_id = _new_object_id;
        end if;
    else
        -- 2. this is an existing format definition, so update core fields.
        update dl_smd_flatfile_summary
        set
            object_name = _object_name,
            object_shortdesc = _object_shortdesc,
            object_config = _object_config,
            last_change_user_id = _user_id,
            last_change_ts = now()
        where
            object_id = _object_id;

        -- 2.1. update the GUID if it has been specified
        if not (_object_guid is null or length(_object_guid) = 0) then
            update dl_smd_hierarchy_summary
            set object_guid = _object_guid
            where
                object_id = _object_id;
        end if;

        -- 2.2. regenerate the metadata context
        call generate_flatfile_summary_md_context(_object_id, 'U');

        set _new_object_id = _object_id;
    end if;

end;
