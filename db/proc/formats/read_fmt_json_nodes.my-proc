--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the full node hierarchy for the JSON format identified by _format_id
--

create procedure read_fmt_json_nodes(
    IN _fmt_id integer
)
begin

    select
        fmt_id,
        json_node_id,
        class_id,
        parent_object_id,
        json_node_type,
        json_node_level,
        json_node_level_sequence,
        json_node_hierarchy_number,
        json_node_is_parent,
        json_node_name,
        ifnull(json_node_shortdesc, 'No short description provided.') as json_node_shortdesc,
        json_node_guid,
        md_context
    from
        dl_fmt_json_detail
    where
        fmt_id = _fmt_id
    order by json_node_hierarchy_number;

end;
