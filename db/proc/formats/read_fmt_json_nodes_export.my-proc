--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the full node hierarchy for the JSON format identified by _format_id
--

create procedure read_fmt_json_nodes_export(
    IN _fmt_id integer
)
begin

    select
        jd.fmt_id,
        jd.json_node_id,
        jd.class_id,
        jd.parent_object_id,
        jd.json_node_type,
        jd.json_node_level,
        jd.json_node_level_sequence,
        jd.json_node_hierarchy_number,
        jd.json_node_is_parent,
        jd.json_node_name,
        ifnull(jd.json_node_shortdesc, 'No short description provided.') as json_node_shortdesc,
        jd.json_node_guid,
        jd.md_context,
        cast(ifnull(cfd.custom_form_data, json_object()) as char) as custom_form_data
    from
        dl_fmt_json_detail jd
            left outer join dl_custom_form_data cfd on cfd.object_id = jd.json_node_id
                and cfd.class_id = jd.class_id
    where
        fmt_id = _fmt_id
    order by json_node_hierarchy_number;

end;
