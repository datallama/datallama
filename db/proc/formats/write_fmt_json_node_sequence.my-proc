--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: updates a JSON node and all its descendents to implement a "change sequence position" action.
--  The node to be moved is called the source element and is identified by _object_id
--  TODO: must incorporate userId for change audit
--  NOT CURRENTLY USED

create procedure write_fmt_json_node_sequence(
  IN _object_id integer,
  IN _new_level_sequence integer
)
begin
    declare v_format_id integer;
    declare v_json_node_id integer;
    declare v_source_level integer;
    declare v_source_level_sequence integer;
    declare v_source_hierarchy_number varchar(100);
    declare v_level integer;
    declare v_level_sequence integer;
    declare v_hierarchy_number varchar(100);
    declare v_temp_hierarchy_number varchar(100);
    declare v_new_hierarchy_number varchar(100);
    declare v_new_level_sequence integer;
    declare v_max_level_sequence integer;
    declare v_sequence_change integer;
    declare v_done tinyint;


    declare csr_impacted_elements cursor for
        select json_node_id, json_node_level, json_node_level_sequence, json_node_hierarchy_number
        from _impacted_elements
        order by json_node_level desc;

    declare continue handler for not found set v_done = 1;

    -- 1: create a temporary table for sequence updates to impacted elements
    drop table if exists _impacted_elements;
    create temporary table _impacted_elements
    (
        json_node_id               integer,
        json_node_level            tinyint,
        json_node_level_sequence   smallint,
        json_node_hierarchy_number varchar(200)
    );

    -- 2. capture the current state attributes of the source element
    select fmt_id, json_node_level, json_node_level_sequence,
           json_node_hierarchy_number
        into v_format_id, v_source_level, v_source_level_sequence, v_source_hierarchy_number
    from dl_fmt_json_detail
    where json_node_id = _object_id;

    -- 2.1. check that the new sequence number is within acceptable bounds
    select max(json_node_level_sequence) into v_max_level_sequence
    from dl_fmt_json_detail
    where fmt_id = v_format_id
        and json_node_level = v_source_level;

    if (_new_level_sequence < 1 or _new_level_sequence > v_max_level_sequence) then
        signal sqlstate '45000' set message_text = 'Invalid sequence number.';
    end if;

    -- 2.2. determine the sequence change: positive or negative
    if (_new_level_sequence < v_source_level_sequence) then
        set v_sequence_change = 1;
    else
        set v_sequence_change = -1;
    end if;

    -- 3. disable the hierarchy number for the source object
    select concat(substring(json_node_hierarchy_number, 1, length(json_node_hierarchy_number) - 3), '000')
        into v_temp_hierarchy_number
    from dl_fmt_json_detail
    where json_node_id = _object_id;

    update dl_fmt_json_detail
    set json_node_hierarchy_number = v_temp_hierarchy_number
    where json_node_id = _object_id;

    -- 3.1. disable the hierarchy number for all children of the source object
    update dl_fmt_json_detail
    set json_node_hierarchy_number = concat(v_temp_hierarchy_number, substring(json_node_hierarchy_number, length(v_temp_hierarchy_number) + 1))
    where fmt_id = v_format_id
        and left(json_node_hierarchy_number, length(v_temp_hierarchy_number)) = v_source_hierarchy_number;


    -- 4. update impacted elements
    if (v_sequence_change = 1) then
        insert into _impacted_elements (json_node_id, json_node_level,
                                        json_node_level_sequence, json_node_hierarchy_number)
        select json_node_id, json_node_level, json_node_level_sequence, json_node_hierarchy_number
        from dl_fmt_json_detail
        where fmt_id = v_format_id
            and json_node_level = v_source_level
            and json_node_level_sequence between _new_level_sequence and v_source_level_sequence
            and json_node_id != _object_id;
    else
        insert into _impacted_elements (json_node_id, json_node_level,
                                    json_node_level_sequence, json_node_hierarchy_number)
        select json_node_id, json_node_level, json_node_level_sequence, json_node_hierarchy_number
        from dl_fmt_json_detail
        where fmt_id = v_format_id
            and json_node_level = v_source_level
            and json_node_level_sequence between v_source_level_sequence and _new_level_sequence
            and json_node_id != _object_id;
    end if;

    set v_done = 0;
    open csr_impacted_elements;

    update_impacted_elements: loop
        fetch csr_impacted_elements into v_json_node_id, v_level,
            v_level_sequence, v_hierarchy_number;

        if v_done = 1 then leave update_impacted_elements; end if;

        -- 4.1. change the sequence number and hierarchy number of the impacted object
        set v_new_level_sequence = v_level_sequence + v_sequence_change;
        set v_new_hierarchy_number = concat(substring(v_hierarchy_number, 1, length(v_hierarchy_number) - 3), lpad(v_new_level_sequence, 3, '0'));
        update dl_fmt_json_detail
        set json_node_level_sequence = v_new_level_sequence,
            json_node_hierarchy_number = v_new_hierarchy_number
        where json_node_id = v_json_node_id;

        -- 4.2. update all children of the impacted object
        update dl_fmt_json_detail
        set json_node_hierarchy_number = concat(v_new_hierarchy_number, substring(json_node_hierarchy_number, length(v_new_hierarchy_number) + 1))
        where fmt_id = v_format_id
            and left(json_node_hierarchy_number, length(v_new_hierarchy_number)) = v_hierarchy_number
            and json_node_level > v_source_level;

    end loop update_impacted_elements;

    close csr_impacted_elements;

    -- 5. change the sequence number and hierarchy number of the source object
    select concat(substring(json_node_hierarchy_number, 1, length(json_node_hierarchy_number) - 3), lpad(_new_level_sequence, 3, '0'))
        into v_new_hierarchy_number
    from dl_fmt_json_detail
    where json_node_id = _object_id;

    update dl_fmt_json_detail
    set json_node_level_sequence = _new_level_sequence,
       json_node_hierarchy_number = v_new_hierarchy_number
    where json_node_id = _object_id;

    -- 5.1. update all children of the source object
    update dl_fmt_json_detail
    set json_node_hierarchy_number = concat(v_new_hierarchy_number, substring(json_node_hierarchy_number, length(v_new_hierarchy_number) + 1))
    where fmt_id = v_format_id
        and left(json_node_hierarchy_number, length(v_new_hierarchy_number)) = v_temp_hierarchy_number
        and json_node_level > v_source_level;

end;