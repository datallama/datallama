--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the detail information for the taxonomy element identified by _object_guid
--

create procedure read_taxonomy_element_ids_by_guid (
    IN _object_guid varchar(40),
    OUT _object_id integer,
    OUT _class_id integer,
    OUT _parent_object_id integer,
    OUT _parent_class_id integer
)
begin

    select te.element_id, te.class_id, te.taxonomy_id, t.class_id
        into _object_id, _class_id, _parent_object_id, _parent_class_id
    from dl_taxonomy_element te
        join dl_taxonomy t on te.taxonomy_id = t.taxonomy_id
    where te.element_guid = _object_guid;

    if _object_id is null then
        set _object_id = -1;
        set _class_id = -1;
        set _parent_object_id = -1;
        set _parent_class_id = -1;
    end if;
end;