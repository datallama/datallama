--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: updates an element and all its descendents to implement a "move element" action.
--  The element to be moved is called the source element and is identified by _element_id
--  TODO: must incorporate userId for change audit
--

create procedure write_taxonomy_element_move(
  IN _element_id integer,
  IN _new_parent_element_id integer
)
begin
    declare v_taxonomy_id integer;
    declare v_source_level integer;
    declare v_source_level_sequence integer;
    declare v_source_hierarchy_number varchar(50);
    declare v_source_parent_object_id integer;

    declare v_target_level integer;
    declare v_target_level_sequence integer;
    declare v_target_hierarchy_number varchar(50);

    declare v_new_level integer;
    declare v_new_level_sequence integer;
    declare v_new_hierarchy_number varchar(50);

    -- the level difference reflects the change in level for source element
    -- and can be applied to all descendants
    declare v_level_diff integer;

    -- 1. capture the current state attributes of the source element
    select taxonomy_id, element_level, element_level_sequence,
           element_hierarchy_number, element_parent_id
        into v_taxonomy_id, v_source_level, v_source_level_sequence,
            v_source_hierarchy_number, v_source_parent_object_id
    from dl_taxonomy_element
    where element_id = _element_id;

    -- 2. capture the current state attributes of the target element
    select element_level, element_level_sequence, element_hierarchy_number
        into v_target_level, v_target_level_sequence, v_target_hierarchy_number
    from dl_taxonomy_element
    where element_id = _new_parent_element_id;

    /** 3. calculate new values for the element being moved **/
    set v_new_level = v_target_level + 1;

    select ifnull(max(element_level_sequence), 0) + 1 into v_new_level_sequence
    from dl_taxonomy_element
    where element_parent_id = _new_parent_element_id;

    set v_new_hierarchy_number = concat(v_target_hierarchy_number, '.', v_new_level_sequence);

    set v_level_diff = v_new_level - v_source_level;

    -- 4. update the source element with new level, sequence, hierarchy number
    update dl_taxonomy_element
    set element_parent_id = _new_parent_element_id,
        element_level = v_new_level,
        element_level_sequence = v_new_level_sequence,
        element_hierarchy_number = v_new_hierarchy_number
    where element_id = _element_id;

    -- 5. update the target element to be a parent element ... just it case it isn't already
    update dl_taxonomy_element
    set element_is_parent = 1
    where element_id = _new_parent_element_id;

    -- 6. update the md_context for the source element
    update dl_taxonomy_element
    set md_context = generate_taxonomy_element_md_context(element_id)
    where element_id = _element_id;

    /** 7. update all descendents of source element **/
    update dl_taxonomy_element
    set element_level = element_level + v_level_diff,
        element_hierarchy_number = concat(v_new_hierarchy_number, right(element_hierarchy_number, length(element_hierarchy_number) - length(v_source_hierarchy_number))),
        md_context = generate_taxonomy_element_md_context(element_id)
    where taxonomy_id = v_taxonomy_id
        and left(element_hierarchy_number, length(v_source_hierarchy_number)) = v_source_hierarchy_number;

    -- 8. update the element_is_parent value for the old parent of the source element, it case it is no longer a parent.
end;

