--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes a new reverse engineering run into table dl_reveng_run or updates an existing run record
--

create procedure read_reveng_run_list_cmn(
    IN _object_id integer,
    IN _class_id integer
)
begin
    select
        a.reveng_run_id,
        a.run_datetime,
        a.run_result,
        a.run_result_desc,
        ifnull(a.input_source, 'None Specified') as input_source,
        ifnull(b.user_login, 'None Specified') as user_login
    from dl_reveng_run_cmn a
        left outer join dl_user b on a.user_id = b.user_id
    where object_id = _object_id
        and class_id = _class_id;

end;
