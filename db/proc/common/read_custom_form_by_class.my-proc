--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Reads the full custom form definition associated with the class specified by _class_id
--

create procedure read_custom_form_by_class(
	IN _class_id integer
)
begin
	select
		cf.custom_form_id,
		cf.custom_form_name,
		cf.schema_definition,
		cf.form_definition
	from dl_custom_form cf
	  join dl_class_hierarchy c on c.custom_form_id = cf.custom_form_id
	where c.class_id = _class_id;
end;
