--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the md_context for a metadata object from the object table associated with the
--  metadata class of the metadata object.
--

create procedure read_md_context_from_object_table (
    IN _object_id integer,
    IN _class_id integer,
    OUT _md_context json
)
begin
    declare v_object_table varchar(200);
    declare v_sql_stmnt varchar(500);

    select concat(object_table, '_vw') into v_object_table
    from dl_class_hierarchy
    where class_id = _class_id;

    set v_sql_stmnt = concat('select md_context into @md_context from ', v_object_table, ' where object_id = ', _object_id, ' and class_id = ', _class_id);
    prepare stmnt from v_sql_stmnt;
    execute stmnt;
    deallocate prepare stmnt;

    set _md_context = @md_context;
end;