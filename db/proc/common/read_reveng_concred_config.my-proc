--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: read the reverse engineer connector and credential configuration for the
--  implementation module metadata object identified by _object_id and _class_id

create procedure read_reveng_concred_config (
    IN _object_id           integer,
    IN _class_id            integer,
    OUT _connector_id       integer,
    OUT _connector_name     varchar(100),
    OUT _connector_shortdesc    varchar(500),
    OUT _credential_type_id integer,
    OUT _credential_id      integer,
    OUT _credential_name    varchar(250),
    OUT _credential_shortdesc   varchar(500),
    OUT _return_id          integer
)
begin
    set _return_id = 1;

    -- get the configuration
    select
        ifnull(a.connector_id, 0),
        ifnull(a.credential_id, 0),
        ifnull(rc.connector_name, ''),
        ifnull(rc.connector_shortdesc, ''),
        ifnull(rc.credential_type_id, 0),
        ifnull(c.credential_name, ''),
        ifnull(c.credential_shortdesc, '')
    into
        _connector_id,
        _credential_id,
        _connector_name,
        _connector_shortdesc,
        _credential_type_id,
        _credential_name,
        _credential_shortdesc
    from dl_reveng_concred_config a
        join dl_reveng_connector rc on a.connector_id = rc.connector_id
        join dl_credential c on a.credential_id = c.credential_id
    where a.object_id = _object_id
        and a.class_id = _class_id;

    if (_connector_id is null) then
        set _connector_id = 0, _credential_id = 0, _connector_name = '', _connector_shortdesc = '',
            _credential_name = '', _credential_shortdesc = '', _return_id = 2;
    end if;
end;
