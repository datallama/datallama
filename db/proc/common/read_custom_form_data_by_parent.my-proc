--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Reads custom form data for all metadata objects belonging to the parent
--  identified by _parent_class_id and _parent_object_id
--

create procedure read_custom_form_data_by_parent (
	IN _parent_class_id     integer,
	IN _parent_object_id    integer
)
begin

    -- 1. Get custom form data
    select
        class_id,
        object_id,
        custom_form_id,
        custom_form_data
    from dl_custom_form_data
    where
        parent_class_id = _parent_class_id
        and parent_object_id = _parent_object_id;
end;
