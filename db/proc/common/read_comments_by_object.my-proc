--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Reads all comments created for the metadata object specified by class_id and object_id from the dl_object_comment table
--

create procedure read_comments_by_object(
	IN _object_id integer,
	IN _class_id integer
)
begin
	select
		a.comment_id,
		a.object_id,
		a.class_id,
	  a.user_id,
	  b.user_login,
	  a.last_change_user_id,
	  date_format(a.last_change_ts, '%d/%m/%Y %H:%i') as last_change_ts,
		ifnull(a.comment_text, 'No comment has been created for this object') as comment_text
	from dl_object_comment a
	  join dl_user b on a.user_id = b.user_id
	where a.object_id = _object_id
	and a.class_id = _class_id;
end;
