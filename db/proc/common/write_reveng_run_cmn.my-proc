--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes a new reverse engineering run into table dl_reveng_run or updates an existing run record
--

create procedure write_reveng_run_cmn(
    IN _user_id integer,
    IN _object_id integer,
    IN _class_id integer,
    IN _reveng_run_id integer,
    IN _reveng_run_result integer,
    IN _reveng_run_desc varchar(500),
    IN _input_source varchar(500),
    OUT _return_id integer
)
begin
    declare v_exists tinyint;
    declare v_prev_run_count integer unsigned;
    declare v_prev_run_result integer;

    if (_reveng_run_id = -1) then
        -- this is a new reverse engineering run so create a new reverse engineering run record if the
        -- previous non-error reverse engineering run has been reconciled

        select count(reveng_run_id) into v_prev_run_count
        from dl_reveng_run_cmn
        where object_id = _object_id
        and class_id = _class_id;

        if (v_prev_run_count = 0) then
            set v_prev_run_result = 2;
        else
            select run_result into v_prev_run_result
            from dl_reveng_run_cmn
            where object_id = _object_id
                and class_id = _class_id
                and reveng_run_id = (select max(reveng_run_id)
                    from dl_reveng_run_cmn
                    where object_id = _object_id
                    and class_id = _class_id
                    and run_result > 0
                );
        end if;


        if (v_prev_run_result = 1) then
            -- previous reverse engineering run has not been fully reconciled.
            set _return_id = -3;
        else
            insert into dl_reveng_run_cmn (
                object_id,
                class_id,
                user_id,
                run_result,
                run_result_desc,
                input_source
            )
            values (
                _object_id,
                _class_id,
                _user_id,
                _reveng_run_result,
                _reveng_run_desc,
                _input_source
            );
            set _return_id = last_insert_id();
        end if;

    else
        -- this is an existing reverse engineering run so update the status
        update dl_reveng_run_cmn
            set run_result = _reveng_run_result,
                run_result_desc = _reveng_run_desc
        where reveng_run_id = _reveng_run_id;
        set _return_id = _reveng_run_result;
    end if;
end;
