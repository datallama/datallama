--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: checks whether a reconciliation run for an API Definition has been fully reconciled.
--  If there are no deleted API elements then the reconciliation status will be 2.
--  Otherwise, if deleted elements have associations, manually defined descriptions, comments or custom form data
--  then the reconciliation status will be 1.
--  The reconciliation status is returned in the OUT variable _run_result

create procedure check_api_reveng_run_status (
    IN _reveng_run_id integer,
    OUT _run_result integer,
    OUT _run_result_desc varchar(500)
)
begin
    declare v_object_id integer;
    declare v_class_id integer;
    declare v_user_id integer;
    declare v_deleted_count integer;
    declare v_added_count integer;
    declare v_status integer default 0;

    -- 1: create a temporary table to store details of deleted elements
    -- drop table if exists _deleted_elements;
    -- create temporary table _deleted_elements (
    --     object_id integer not null,
    --     class_id integer not null
    -- );

    -- 2. get the API objectId for the reverse engineering run
    select object_id, class_id, user_id into v_object_id, v_class_id, v_user_id
    from dl_reveng_run_cmn
    where reveng_run_id = _reveng_run_id;

    -- 3. check if there are added elements
    select count(object_id) into v_added_count
    from dl_api_rest_detail
    where parent_object_id = v_object_id
        and reveng_run_id = _reveng_run_id
        and reveng_run_count = 1;

    -- 4. check if there are deleted elements that require reconciliation
    -- insert into _deleted_elements (object_id, class_id)
    -- select object_id, class_id
    select count(object_id) into v_deleted_count
    from dl_api_rest_detail
    where parent_object_id = v_object_id
        and reveng_run_id < _reveng_run_id;

    -- select count(*) into v_deleted_count
    -- from _deleted_elements;

    if (v_deleted_count > 0 and v_added_count > 0) then
        -- check for comments keyed to deleted elements
        update dl_api_rest_detail a
            join dl_object_comment b
                on a.object_id = b.object_id and a.class_id = b.class_id
        set a.prx_status = 1
        where a.parent_object_id = v_object_id
            and a.reveng_run_id < _reveng_run_id;

        -- check for associations keyed to deleted elements
        update dl_api_rest_detail a
            join dl_object_association b
                on a.object_id = a.object_id = b.source_object_id and a.class_id = b.source_class_id
        set a.prx_status = 1
        where a.parent_object_id = v_object_id
            and a.reveng_run_id < _reveng_run_id;

        update dl_api_rest_detail a
            join dl_object_association b
                on a.object_id = a.object_id = b.target_object_id and a.class_id = b.target_class_id
        set a.prx_status = 1
        where a.parent_object_id = v_object_id
            and a.reveng_run_id < _reveng_run_id;

        -- check for custom form data keyed to deleted elements
        update dl_api_rest_detail a
            join dl_custom_form_data b
                on a.object_id = a.object_id = b.object_id and a.class_id = b.class_id
        set a.prx_status = 1
        where a.parent_object_id = v_object_id
            and a.reveng_run_id < _reveng_run_id;
    end if;

    -- 5. set reconciliation status
    select sum(prx_status) into v_status
    from dl_api_rest_detail
    where parent_object_id = v_object_id
        and reveng_run_id < _reveng_run_id;

    if (v_status > 0) then
        set _run_result = 1;
        set _run_result_desc = 'Reverse engineer run completed. RECONCILIATION is required.';
    else
        set _run_result = 2;
        set _run_result_desc = 'Reverse engineer run completed successfully. No reconcilaition required';
    end if;

    -- 6. purge deleted elements based on reconciliation status
    call purge_api_rest_reveng_deleted(v_user_id, v_object_id, _reveng_run_id, _run_result);

    -- 7. update the reverse engineer run status
    update dl_reveng_run_cmn
    set run_result = _run_result, run_result_desc = _run_result_desc
    where reveng_run_id = _reveng_run_id;

end;
