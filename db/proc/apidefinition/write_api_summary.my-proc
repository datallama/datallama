--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: creates a new message format definition or updates an existing one.
--

create procedure write_api_summary(
    IN _user_id             integer,
    IN _object_id           integer,
    IN _class_id            integer,
    IN _object_name         varchar(150),
    IN _object_shortdesc    varchar(500),
    IN _api_base_config     json,
    OUT _return_id          integer
)
begin
    declare v_return_id integer;
    declare v_api_base_config json;

    if (_api_base_config is null) then
        set v_api_base_config = json_object();
    else
        set v_api_base_config = _api_base_config;
    end if;


    if (_object_id < 0) then
        -- this is a new format definition so insert into dl_format
        insert into dl_api (
            class_id,
            object_name,
            object_shortdesc,
            api_base_config,
            last_change_user_id
        )
        values (
            _class_id,
            _object_name,
            _object_shortdesc,
            v_api_base_config,
            _user_id
        );
        set _return_id = last_insert_id();

        -- generate the metadata context
        call generate_api_md_context(_return_id, 'C');

    else
        update dl_api
        set
            object_name = _object_name,
            object_shortdesc = _object_shortdesc,
            api_base_config = v_api_base_config,
            last_change_user_id = _user_id,
            last_change_ts = now()
        where
            object_id = _object_id;

        set _return_id = _object_id;

        -- regenerate the metadata context
        call generate_api_md_context(_return_id, 'U');

    end if;


end;
