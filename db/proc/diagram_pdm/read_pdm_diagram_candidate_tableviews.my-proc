--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads all smd tables/views in the SMD database specified by _database_id
--

create procedure read_pdm_diagram_candidate_tableviews (
    IN _database_id integer,
    IN _dgrm_id integer  -- the diagram ID for a PDM diagram
)
begin

  drop table if exists _diagram_smd_entity;
  create temporary table _diagram_smd_entity (
    object_id               integer not null,
    class_id                integer not null,
    object_name             varchar(200) not null,
    object_shortdesc        varchar(1000) not null,
    smd_schema_id           integer not null,
    smd_schema_name         varchar(200) not null,
    selected                tinyint null default 0, -- indicates whether table/view is selected for the PDM diagram
    selection_change        tinyint null default 0  -- used to capture the change in selection made by a user
  );

  insert into _diagram_smd_entity (object_id, class_id, object_name, object_shortdesc, smd_schema_id, smd_schema_name)
  select
      smdtv.smd_table_view_id,
      smdtv.class_id,
      smdtv.smd_table_view_name,
      ifnull(smdtv.smd_table_view_shortdesc, 'None specified'),
      ifnull(smds.smd_schema_id, 0),
      ifnull(smds.smd_schema_name, 'N/A')
  from
      dl_smd_table_view smdtv
          left outer join dl_smd_schema smds on smdtv.smd_schema_id = smds.smd_schema_id
  where smdtv.smd_database_id = _database_id;

  update _diagram_smd_entity t1
  join dl_pdm_diagram_smd_entity pdse on t1.object_id = pdse.smd_table_view_id
  set t1.selected = 1, t1.selection_change = 1
  where t1.object_id = pdse.smd_table_view_id
  and pdse.dgrm_id = _dgrm_id;

  select * from _diagram_smd_entity;
end;
