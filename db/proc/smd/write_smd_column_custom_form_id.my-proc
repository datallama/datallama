--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: Updates the custom form that will be used for all columns in an SMD table/view
--  Although the custom_form_id is stored in dl_smd_table_view, the corresponding custom form data is stored
--  in dl_smd_column.custom_form_data
--

create procedure write_smd_column_custom_form_id (
    IN _user_id integer,
    IN _object_id integer,  -- the objectId of the SMD table/view definition
    IN _class_id integer,   -- the classId of the SMD table/view definition
    IN _custom_form_id  integer
)
begin

    -- 1. Archive the table/view definition and update it with new custom form Id.
    -- TODO: call archive_smd_table_view(_object_id, 'U');

    update dl_smd_table_view
    set
        custom_form_id = _custom_form_id,
        last_change_user_id = _user_id,
        last_change_ts = now()
    where
        smd_table_view_id = _object_id;

    -- clear all existing custom form data for tables/views in the database
    -- TODO: add archive to history table
    delete from dl_custom_form_data
    where parent_object_id = _object_id
    and parent_class_id = _class_id;

end;
