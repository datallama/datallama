--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: creates a new SMD Presentation detail.
--
create procedure write_smd_presentation_detail(
    IN _user_id                     integer,
    IN _object_id                   integer,
    IN _class_id                    integer,
    IN _parent_object_id            integer,
    IN _parent_class_id             integer,
    IN _parent_element_object_id    integer,
    IN _element_name                varchar(150),
    IN _element_shortdesc           varchar(500),
    IN _import_run_id               integer
)
begin
    declare v_return_id integer;
    declare v_level tinyint unsigned;
    declare v_level_sequence smallint unsigned;
    declare v_hierarchy_number varchar(100);
    declare v_element_exists integer;


    if (_object_id < 0) then
        -- this is a new element so check whether it already exists and if not then insert into dl_smd_presentation_detail
            -- unique constraint is on parent_element_object_id and element_name
        select count(object_id), max(object_id) into v_element_exists, v_return_id
        from dl_smd_presentation_detail
        where parent_element_object_id = _parent_element_object_id
            and element_name = _element_name;

        if (_parent_element_object_id = 0 and v_element_exists = 0) then
            -- this is a new TOP LEVEL element in presentation definition.
            select
                ifnull(max(element_level_sequence), 0) + 1 into v_level_sequence
            from
                dl_smd_presentation_detail
            where
                element_level = 0
                and parent_element_object_id = _parent_element_object_id;

            set v_hierarchy_number = lpad(v_level_sequence, 3, '0');
            insert into dl_smd_presentation_detail(
                class_id,
                parent_object_id,
                parent_class_id,
                parent_element_object_id,
                element_guid,
                element_level,
                element_level_sequence,
                element_hierarchy_number,
                element_name,
                element_shortdesc,
                import_run_id,
                last_change_user_id
            )
            values (
                _class_id,
                _parent_object_id,
                _parent_class_id,
                _parent_element_object_id,
                uuid(),
                0,
                v_level_sequence,
                v_hierarchy_number,
                _element_name,
                _element_shortdesc,
                _import_run_id,
                _user_id
            );
            set v_return_id = last_insert_id();

        elseif (_parent_element_object_id > 0 and v_element_exists = 0) then
            -- this is a new element at a SUBORDINATE LEVEL in the hierarchy
            -- 1. determine the level based on level of parent
            select element_level + 1 into v_level
            from   dl_smd_presentation_detail
            where  object_id = _parent_element_object_id;

            -- 2. generate hierarchy number based on parent hierarchy number and current level sequence
            select element_hierarchy_number into v_hierarchy_number  -- get hierarchy number for parent
            from   dl_smd_presentation_detail
            where  object_id = _parent_element_object_id;

            select ifnull(max(element_level_sequence), 0) + 1 into v_level_sequence  -- get max sequence at current level
            from   dl_smd_presentation_detail
            where  parent_element_object_id = _parent_element_object_id;

            set v_hierarchy_number = concat(v_hierarchy_number, '.', lpad(v_level_sequence, 3, '0'));

            insert into dl_smd_presentation_detail (
                class_id,
                parent_object_id,
                parent_class_id,
                parent_element_object_id,
                element_guid,
                element_level,
                element_level_sequence,
                element_hierarchy_number,
                element_name,
                element_shortdesc,
                import_run_id,
                last_change_user_id
            )
            values (
                _class_id,
                _parent_object_id,
                _parent_class_id,
                _parent_element_object_id,
                uuid(),
                v_level,
                v_level_sequence,
                v_hierarchy_number,
                _element_name,
                _element_shortdesc,
                _import_run_id,
                _user_id
            );
            set v_return_id = last_insert_id();

            -- update the is_parent status of parent node
            update dl_smd_presentation_detail
            set is_parent = true
            where object_id = _parent_element_object_id;
        end if;
    else
        -- this is an existing presentation detail so update the row accordingly
        set v_return_id = _object_id;
        update dl_smd_presentation_detail
        set element_name = _element_name,
            element_shortdesc = _element_shortdesc,
            import_run_id = _import_run_id,
            last_change_user_id = _user_id,
            last_change_ts = current_timestamp
        where
            object_id = _object_id;
    end if;

    -- update the md_context
    update dl_smd_presentation_detail
    set md_context = generate_smd_presentation_detail_md_context(v_return_id)
    where object_id = v_return_id;

    select
        object_id,
        class_id,
        parent_object_id,
        parent_class_id,
        parent_element_object_id,
        element_level,
        element_level_sequence,
        is_parent,
        element_hierarchy_number,
        element_name,
        ifnull(element_shortdesc, 'No short description provided.') as element_shortdesc,
        ifnull(element_guid, 'No GUID set') as element_guid,
        md_context
    from dl_smd_presentation_detail
    where object_id = v_return_id;
end;
