--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: read details for the smd schema specified by _smd_schema_id
-- TODO: this stored proc should be updated to use OUT params

create procedure read_smd_schema (
    IN _smd_schema_id integer
)
begin
    select
        s.smd_schema_id,
        s.smd_database_id,
        s.class_id,
        d.smd_database_name,
        s.smd_schema_name,
        s.object_guid,
        s.smd_schema_shortdesc,
        d.smd_database_shortdesc,
        s.md_context
    from dl_smd_schema s
        join dl_smd_database d on s.smd_database_id = d.smd_database_id
    where
        s.smd_schema_id = _smd_schema_id;
end;