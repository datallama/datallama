--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--

-- Description: reads all FK relationships defined in the DB Schema identified by _object_id

create procedure read_smd_relationship_by_schema (
    IN _object_id integer
)
begin

    -- 1. get the core details from dl_smd_relationship
    select
        r.smd_relationship_id, r.class_id, r.create_mode, r.smd_relationship_name,
        r.parent_table_view_id, ifnull(tv1.class_id, 0) as parent_class_id,
        ifnull(tv1.smd_table_view_name, 'no_parent_table') as parent_table_view_name,
        r.child_table_view_id, tv2.class_id as child_class_id,
        tv2.smd_table_view_name as child_table_view_name,
        r.smd_relationship_shortdesc
    from dl_smd_relationship r
        left outer join dl_smd_table_view tv1 on r.parent_table_view_id = tv1.smd_table_view_id
        join dl_smd_table_view tv2 on r.child_table_view_id = tv2.smd_table_view_id
    where r.smd_schema_id = _object_id;

end;
