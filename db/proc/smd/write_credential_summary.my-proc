--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes a new file store or object store credential to the dl_credential table.
--
create procedure write_credential_summary(
    IN _credential_id integer,
    IN _credential_shortdesc varchar(500),
    IN _user_id integer
)
begin
    declare v_exists integer;

    select count(credential_id) into v_exists
    from dl_credential
    where credential_id = _credential_id;

    if (v_exists = 1) then
        -- 1. update the short description for the credential.
        update dl_credential
            set credential_shortdesc = _credential_shortdesc,
                last_change_user_id = _user_id
        where credential_id = _credential_id;
    end if;

end;