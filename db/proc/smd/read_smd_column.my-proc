--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: read details for the smd column specified by _smd_table_view_id
--

create procedure read_smd_column (
    IN _smd_column_id integer
)
begin
    declare v_parent_class_id integer;
    declare v_custom_form_id integer;
    declare v_custom_form_name varchar(150);
    declare v_schema_definition json;
    declare v_form_definition json;
    declare v_custom_form_data json;

    -- 1. get the custom form details from parent schema.
    select tv.class_id, tv.custom_form_id into v_parent_class_id, v_custom_form_id
    from dl_smd_column c
        join dl_smd_table_view tv on tv.smd_table_view_id = c.smd_table_view_id
    where c.smd_column_id = _smd_column_id;

    select custom_form_name, schema_definition, form_definition
        into v_custom_form_name, v_schema_definition, v_form_definition
    from dl_custom_form
    where custom_form_id = v_custom_form_id;

    -- 2. get the custom form data for the SMD column
    select cfd.custom_form_data into v_custom_form_data
    from dl_custom_form_data cfd
        join dl_smd_column c on c.smd_column_id =  cfd.object_id
            and c.class_id = cfd.class_id
    where c.smd_column_id = _smd_column_id;

    select
        a.smd_column_id,
        a.class_id,
        a.smd_table_view_id,
        v_parent_class_id as parent_class_id,
        a.smd_column_ordinal,
        a.smd_column_datatype,
        a.smd_primary_key,
        b.smd_table_view_name,
        a.smd_column_name,
        ifnull(a.smd_column_shortdesc, '') as smd_column_shortdesc,
        ifnull(b.smd_table_view_shortdesc, '') as smd_table_view_shortdesc,
        v_custom_form_id as custom_form_id,
        v_custom_form_name as custom_form_name,
        v_schema_definition as schema_definition,
        v_form_definition as form_definition,
        v_custom_form_data as custom_form_data,
        a.md_context,
        a.data_sensitivity_id,
        ifnull(ds.data_sensitivity_level, 'none') as data_sensitivity_level,
        ifnull(ds.data_sensitivity_name, 'none') as data_sensitivity_name,
        a.ds_assign_date,
        a.ds_assign_user_id,
        a.ds_approval_status,
        a.ds_approval_user_id
    from dl_smd_column a
        join dl_smd_table_view b on a.smd_table_view_id = b.smd_table_view_id
        left outer join dl_data_sensitivity_tag ds on a.data_sensitivity_id = ds.data_sensitivity_id
    where
        a.smd_column_id = _smd_column_id;
end;