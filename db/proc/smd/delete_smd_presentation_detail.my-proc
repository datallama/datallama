--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--

create procedure delete_smd_presentation_detail(
    IN _user_id integer,
    IN _object_id integer
)
begin
    declare v_child_element_id integer;
    declare v_element_hierarchy_number varchar(160);
    declare v_done tinyint;

    declare csr_child_mdobjects cursor for
        select object_id
        from dl_smd_presentation_detail
        where left(element_hierarchy_number, length(v_element_hierarchy_number)) = v_element_hierarchy_number;

    declare continue handler for not found set v_done = 1;

    select concat(element_hierarchy_number, '.') into v_element_hierarchy_number
    from dl_smd_presentation_detail
    where object_id = _object_id;

    call archive_smd_presentation_detail(_user_id, _object_id, 'D');
    delete from dl_smd_presentation_detail where object_id = _object_id;

    set v_done = 0;
    open csr_child_mdobjects;

    delete_child_object: loop
        fetch csr_child_mdobjects into v_child_element_id;

        if v_done = 1 then leave delete_child_object; end if;
        call archive_smd_presentation_detail(_user_id, v_child_element_id, 'D');
        delete from dl_smd_presentation_detail where object_id = v_child_element_id;
    end loop delete_child_object;

    close csr_child_mdobjects;

end;
