--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: Updates the custom form that will be used for all elements in a dataintegration project
--

create procedure write_dataintegration_custom_form_id (
    IN _user_id integer,
    IN _object_id integer,  -- the objectId of the dataintegration project
    IN _class_id integer,   -- the classId of the dataintegration project
    IN _custom_form_id  integer
)
begin

    -- 1. Archive the dataintegration definition and update it with new custom form Id.
    -- TODO: call archive proc for dl_dataintegration_summary

    update dl_dataintegration_summary
    set
        custom_form_id = _custom_form_id,
        last_change_user_id = _user_id,
        last_change_ts = now()
    where
        object_id = _object_id;

    -- clear all existing custom form data for tables/views in the database
    -- TODO: add archive to history table
    delete from dl_custom_form_data
    where parent_object_id = _object_id
    and parent_class_id = _class_id;

end;
