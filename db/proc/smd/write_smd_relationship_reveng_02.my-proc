--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes details of a structural metadata (SMD) relationship between SMD table/view entities
--  into table dl_smd_relationship as part of a reverse engineering run.
--  The Ids of parent and child tables are not known at the time of the reverse engineer action and thus are
--  determined in this procedure.
--

create procedure write_smd_relationship_reveng_02 (
    IN _user_id integer,
    IN _database_id integer,
    IN _schema_name varchar(200),
    IN _class_id integer,
    IN _reveng_run_id integer,
    IN _parent_schema_name varchar(200),
    IN _parent_table_view_name varchar(200),
    IN _child_schema_name varchar(200),
    IN _child_table_view_name varchar(200),
    IN _relationship_name varchar(200),
    IN _relationship_shortdesc varchar(1000),
    OUT _relationship_id integer,
    OUT _parent_table_view_id integer,
    OUT _child_table_view_id integer
)
begin
    declare v_schema_id integer;
    declare v_exists integer;
    declare v_unique_name varchar(200);

    -- 1. get the Ids for schema and the parent and child tables
    select smd_schema_id into v_schema_id
    from dl_smd_schema
    where smd_database_id = _database_id
        and smd_schema_name = _schema_name;

    select tv.smd_table_view_id into _parent_table_view_id
    from dl_smd_table_view tv
        join dl_smd_schema s on tv.smd_schema_id = s.smd_schema_id
    where s.smd_database_id = _database_id
        and s.smd_schema_name = _parent_schema_name
        and tv.smd_table_view_name = _parent_table_view_name;

    if _parent_table_view_id is null then   -- this can occur if the parent table exists in a separate schema
        set _parent_table_view_id = 0;      -- that has not yet been reverse engineered
    end if;

    select tv.smd_table_view_id into _child_table_view_id
    from dl_smd_table_view tv
        join dl_smd_schema s on tv.smd_schema_id = s.smd_schema_id
    where s.smd_database_id = _database_id
        and s.smd_schema_name = _child_schema_name
        and tv.smd_table_view_name = _child_table_view_name;

    -- 2. determine if the relationship already exists
    select count(smd_relationship_id) into v_exists
    from dl_smd_relationship
    where smd_database_id = _database_id
        and smd_schema_id = v_schema_id
        and parent_table_view_id = _parent_table_view_id
        and child_table_view_id = _child_table_view_id;

    -- 3. create or update the relationship definition
    if (v_exists = 0) then  -- this is a new relationship definition
        -- check if a relationship has been defined manually with the same name
        select count(smd_relationship_name) into v_exists
        from dl_smd_relationship
        where smd_relationship_name = _relationship_name
            and smd_database_id = _database_id
            and smd_schema_id = v_schema_id;

        insert into dl_smd_relationship (
            smd_database_id,
            smd_schema_id,
            class_id,
            reveng_run_id,
            parent_table_view_id,
            child_table_view_id,
            smd_relationship_name,
            smd_relationship_shortdesc,
            last_change_user_id
        )
        values (
            _database_id,
            v_schema_id,
            _class_id,
            _reveng_run_id,
            _parent_table_view_id,
            _child_table_view_id,
            _relationship_name,
            _relationship_shortdesc,
            _user_id
        );
        set _relationship_id = last_insert_id();
    else -- this is an existing relationship definition
        select smd_relationship_id into _relationship_id
        from dl_smd_relationship
        where smd_database_id = _database_id
            and smd_schema_id = v_schema_id
            and parent_table_view_id = _parent_table_view_id
            and child_table_view_id = _child_table_view_id;

        update dl_smd_relationship
        set reveng_run_id = _reveng_run_id,
            smd_relationship_shortdesc = _relationship_shortdesc,
            last_change_user_id = _user_id
        where smd_relationship_id = _relationship_id;

    end if;
end;