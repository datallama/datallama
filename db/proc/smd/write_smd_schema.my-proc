--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes or updates the a structural metadata database schema definition
--

create procedure write_smd_schema (
    IN _user_id integer,
    IN _object_id integer,
    IN _class_id integer,
    IN _parent_object_id integer,
    IN _schema_name varchar(200),
    IN _schema_shortdesc varchar(1000),
    OUT _return_id integer
)
begin
    declare v_check integer default 0;
    declare v_result_status integer default 1;

    -- 1. input validation
    -- 1.1. schema name must not be blank
    if (_schema_name is null or length(_schema_name) = 0) then
        set _return_id = -2;
        set v_result_status = -1;
        signal sqlstate '45000' set mysql_errno = 21101, message_text = 'Schema name must not be blank when defining a new RDBMS schema.';
    end if;

    -- 1.2. check if table/view already exists
    select count(smd_schema_id) into v_check
    from dl_smd_schema
    where smd_schema_name = _schema_name
        and class_id = _class_id
        and smd_database_id = _parent_object_id;

    if (_object_id = 0 and v_check = 1) then
        -- schema already exists so just return the object_id
        select smd_schema_id into _return_id
        from dl_smd_schema
        where smd_schema_name = _schema_name
            and class_id = _class_id
            and smd_database_id = _parent_object_id;
        set v_result_status = 100;
    end if;

    -- 2. insert schema definition or update existing definition
    if (v_result_status = 1) then
        if (_object_id = 0) then
            -- 2.1. this is a new SMD schema definition
            insert into dl_smd_schema (
                smd_database_id,
                class_id,
                reveng_run_id,
                smd_schema_name,
                object_guid,
                smd_schema_shortdesc,
                last_change_user_id
            )
            values (
                _parent_object_id,
                _class_id,
                0,
                _schema_name,
                uuid(),
                _schema_shortdesc,
                _user_id
            );
            set _return_id = last_insert_id();

            -- 2.1.2. add the md_context
            call generate_smd_schema_md_context(_return_id, 'C');
        else
            -- 2.2. The schemaId is > 0 so archive the schema record update the schema info
            call archive_smd_schema (_object_id, 'U');
            update dl_smd_schema
            set reveng_run_id = 0,
                smd_schema_name = _schema_name,
                smd_schema_shortdesc = _schema_shortdesc,
                last_change_ts = current_timestamp,
                last_change_user_id = _user_id
            where smd_schema_id = _object_id;

            -- 2.2.1. update the md_context
            -- TODO: need to propagate the updated md_context!
            call generate_smd_schema_md_context(_object_id, 'U');

            set _return_id = _object_id;

        end if;
    end if;

end;