--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: generates the metadata context for a data smd hierarchy element identified by _object_id
--

create procedure generate_smd_hierarchy_element_md_context(
    IN _object_id integer,
    IN _mode char(1)
)
begin
    declare v_element_fullpath_name varchar(2048);
    declare v_element_parent_id integer;
    declare v_object_name varchar(150);
    declare v_class_id integer;
    declare v_class_name varchar(150);
    declare v_base_object_name varchar(150);

    declare v_parent_class_id integer;
    declare v_parent_class_name varchar(150);
    declare v_parent_object_id integer;
    declare v_parent_object_name varchar(150);

    declare v_md_context json;

    -- 1. get details of current element
    select
        hd.object_name, hd.parent_element_object_id, hd.class_id, c.class_name
        into v_base_object_name, v_element_parent_id, v_class_id, v_class_name
    from
        dl_smd_hierarchy_detail hd
        join dl_class_hierarchy c on hd.class_id = c.class_id
    where
        object_id = _object_id;

    set v_element_fullpath_name = v_base_object_name;

    -- 2. get full path for current element
    while v_element_parent_id > 0 do
        select
            object_name, parent_element_object_id
            into v_object_name, v_element_parent_id
        from
            dl_smd_hierarchy_detail
        where
            object_id = v_element_parent_id;

        set v_element_fullpath_name = concat(v_object_name, '.', v_element_fullpath_name);
    end while;

    -- 3. get details of the parent object
    select
        c.class_id, c.class_name, hs.object_id, hs.object_name
        into v_parent_class_id, v_parent_class_name, v_parent_object_id, v_parent_object_name
    from
        dl_smd_hierarchy_summary hs
        join dl_class_hierarchy c on hs.class_id = c.class_id
        join dl_main.dl_smd_hierarchy_detail hd on hs.object_id = hd.parent_object_id
    where
        hd.object_id = _object_id;

    set v_md_context = json_array(
                json_object('class_id', v_parent_class_id, 'class_name', v_parent_class_name,
                    'object_id', v_parent_object_id, 'object_name', v_parent_object_name),
                json_object('class_id', v_class_id, 'class_name', v_class_name,
                    'object_id', _object_id, 'object_name', v_base_object_name,
                    'internal_path', v_element_fullpath_name)
            );

	-- 3. update the metadata object in dl_smd_hierarchy_summary with the new md_context
	update dl_smd_hierarchy_detail
	    set md_context = json_compact(v_md_context)
	where object_id = _object_id;

    -- 4. update tables that duplicate the md_context if mode = 'U' (i.e. update md_context action)
	if (_mode = 'U') then
        call update_association_md_contexts(_object_id, v_class_id, v_md_context);
    end if;

end;
