--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: generates the metadata context for the smd hierarchy summary identified by _object_id
-- An smd hierarchy summary is a top level metadata object so no full path name is required.
--

create procedure generate_smd_database_md_context(
    IN _object_id integer,
    IN _mode char(1)
)
begin
	declare v_object_name varchar(150);
	declare v_class_id integer;
	declare v_class_name varchar(150);
	declare v_md_context json;

	-- 1. get class & object details
	select
		c.class_id, c.class_name, sdb.logical_database_name
		into v_class_id, v_class_name, v_object_name
	from
		dl_smd_database sdb
		join dl_class_hierarchy c on sdb.class_id = c.class_id
	where
		sdb.smd_database_id = _object_id;

	-- 2. generate the md_context. Top level metadata objects have only a single json object in the md_context
	set v_md_context = json_array(
		json_object(
			'class_id', v_class_id,
			'class_name', v_class_name,
			'object_id', _object_id,
			'object_name', v_object_name
		)
	);

	-- 3. update the metadata object in dl_smd_hierarchy_summary with the new md_context
	update dl_smd_database
	    set md_context = json_compact(v_md_context)
	where smd_database_id = _object_id;

    -- 4. update tables that duplicate the md_context if mode = 'U' (i.e. update md_context action)
	if (_mode = 'U') then
        call update_association_md_contexts(_object_id, v_class_id, v_md_context);
    end if;

end;