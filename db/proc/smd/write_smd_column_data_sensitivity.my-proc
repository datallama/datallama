--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes explicit data sensitivity for the SMD column identified by object_id.
--  Updates the aggregate data sensitivity for the parent object hierarchy - table/view, schema, database

create procedure write_smd_column_data_sensitivity (
    IN _user_id integer,
    IN _object_id integer,
    IN _data_sensitivity_id integer
)
begin
    declare v_current_data_sensitivity_id integer;
    declare v_remaining_ds_tags integer;
    declare v_parent_object_id integer;
    declare v_parent_class_id integer;

    -- 1. get the current data sensitivity tag
    select data_sensitivity_id into v_current_data_sensitivity_id
    from dl_smd_column_vw
    where object_id = _object_id;

    -- 2. update the column md_object with specified data sensitivity tag
    -- TODO: handle error if object_id or user_id don't exist
    update dl_smd_column
        set data_sensitivity_id = _data_sensitivity_id,
            ds_assign_user_id = _user_id,
            ds_assign_date = current_timestamp
    where smd_column_id = _object_id;

    -- 3. update the aggregate data sensitivity for parents in the RDBMS hierarchy
    -- 3.1. update table/view
    select parent_object_id, parent_class_id into v_parent_object_id, v_parent_class_id
    from dl_smd_column_vw
    where object_id = _object_id;

    if (_data_sensitivity_id = 0 and v_current_data_sensitivity_id > 0) then
        select count(data_sensitivity_id) into v_remaining_ds_tags
        from dl_smd_column_vw
        where parent_object_id = v_parent_object_id
            and data_sensitivity_id = v_current_data_sensitivity_id;

        if (v_remaining_ds_tags = 0) then
            call delete_data_sensitivity_multi (_user_id, v_parent_object_id, v_parent_class_id,
                    v_current_data_sensitivity_id);
        end if;
    else
        call write_data_sensitivity_multi (_user_id, v_parent_object_id, v_parent_class_id,
                    _data_sensitivity_id, 20);
    end if;


    -- 3.2. update schema
    select parent_object_id, parent_class_id into v_parent_object_id, v_parent_class_id
    from dl_smd_table_view_vw
    where object_id = v_parent_object_id;

    if (_data_sensitivity_id = 0 and v_current_data_sensitivity_id > 0 and v_remaining_ds_tags = 0) then
        call delete_data_sensitivity_multi (_user_id, v_parent_object_id, v_parent_class_id,
                v_current_data_sensitivity_id);
    else
        call write_data_sensitivity_multi (_user_id, v_parent_object_id, v_parent_class_id,
                _data_sensitivity_id, 20);
    end if;


    -- 3.3. update database
    select parent_object_id, parent_class_id into v_parent_object_id, v_parent_class_id
    from dl_smd_schema_vw
    where object_id = v_parent_object_id;

    if (_data_sensitivity_id = 0 and v_current_data_sensitivity_id > 0 and v_remaining_ds_tags = 0) then
        call delete_data_sensitivity_multi (_user_id, v_parent_object_id, v_parent_class_id,
                v_current_data_sensitivity_id);
    else
        call write_data_sensitivity_multi (_user_id, v_parent_object_id, v_parent_class_id,
                _data_sensitivity_id, 20);
    end if;

end;