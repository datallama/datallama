--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes a column pair for an SMD relationship as part of a Reverse
--  Engineering run.
--

create procedure write_smd_relationship_column_pair_reveng (
    IN _user_id integer,
    IN _relationship_id integer,
    IN _parent_table_view_id integer,
    IN _parent_column_name varchar(200),
    IN _child_table_view_id integer,
    IN _child_column_name varchar(200),
    IN _reveng_run_id integer
)
begin
    declare v_exists tinyint;
    declare v_parent_column_id integer;
    declare v_child_column_id integer;

    -- 1. get Ids for parent and child columns
    select smd_column_id into v_parent_column_id
    from dl_smd_column
    where smd_table_view_id = _parent_table_view_id
        and smd_column_name = _parent_column_name;

    select smd_column_id into v_child_column_id
    from dl_smd_column
    where smd_table_view_id = _child_table_view_id
        and smd_column_name = _child_column_name;

    -- 2. check whether column pair has already been defined
    select count(smd_relationship_id) into v_exists
    from dl_smd_relationship_column_pair
    where smd_relationship_id = _relationship_id
        and parent_column_id = v_parent_column_id
        and child_column_id = v_child_column_id;

    if (v_exists = 0) then
        -- this is a new column pair for the SMD relationship
        insert into dl_smd_relationship_column_pair (
            smd_relationship_id,
            parent_column_id,
            child_column_id,
            reveng_run_id,
            last_change_user_id)
        values (
            _relationship_id,
            v_parent_column_id,
            v_child_column_id,
            _reveng_run_id,
            _user_id
        );
    else
        update dl_smd_relationship_column_pair
        set reveng_run_id = _reveng_run_id
        where smd_relationship_id = _relationship_id;
    end if;

end;