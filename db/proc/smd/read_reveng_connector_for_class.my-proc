--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the list of metadata object classes relating to data presentation implementations
--

create procedure read_reveng_connector_for_class(
    IN _class_id integer
)
begin
    select
        connector_id,
        connector_name,
        connector_version,
        connector_provider,
        ifnull(connector_shortdesc, 'None provided') as connector_shortdesc,
        class_id,
        credential_type_id
    from
        dl_reveng_connector
    where
        class_id = _class_id
        and connector_enabled = 1;

end;
