--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: checks whether a reconciliation run for an SMD Database has been fully reconciled.
--  If there are no deleted schema, tables/views or columns then the reconciliation status will be 2.
--  Otherwise it will be 1.
--  The reconciliation status is returned in the OUT variable _run_result

create procedure check_reveng_run_status (
    IN _reveng_run_id integer,
    OUT _run_result integer
)
begin
    declare v_smd_database_id integer;
    declare v_deleted_count integer;
    declare v_status integer default 0;

    -- 1. get the SMD database Id for the reverse engineering run
    select smd_database_id into v_smd_database_id
    from dl_reveng_run
    where reveng_run_id = _reveng_run_id;

    -- 2. check if there are unreconciled deleted schema, tables/views, columns or relationships
    select count(smd_schema_id) into v_deleted_count
    from dl_smd_schema
    where smd_database_id = v_smd_database_id
        and reveng_run_id < _reveng_run_id;

    set v_status = v_status + v_deleted_count;

    select count(smd_table_view_id) into v_deleted_count
    from dl_smd_table_view
    where smd_database_id = v_smd_database_id
        and reveng_run_id < _reveng_run_id;

    set v_status = v_status + v_deleted_count;

    select count(c.smd_column_id) into v_deleted_count
    from dl_smd_column c
        join dl_smd_table_view tv on c.smd_table_view_id = tv.smd_table_view_id
    where tv.smd_database_id = v_smd_database_id
        and c.reveng_run_id < _reveng_run_id;

    set v_status = v_status + v_deleted_count;

    select count(r.smd_relationship_id) into v_deleted_count
    from dl_smd_relationship r
    where r.smd_database_id = v_smd_database_id
        and r.reveng_run_id < _reveng_run_id;

    set v_status = v_status + v_deleted_count;

    if (v_status > 0) then
        set _run_result = 1;
        update dl_reveng_run
        set run_result = 1
        where reveng_run_id = _reveng_run_id;
    else
        set _run_result = 2;
        update dl_reveng_run
        set run_result = 2
        where reveng_run_id = _reveng_run_id;
    end if;

end;

