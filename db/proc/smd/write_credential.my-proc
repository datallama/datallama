--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes a new file store or object store credential to the dl_credential table.
--
create procedure write_credential(
    IN _credential_id integer,
    IN _credential_name varchar(250),
    IN _credential_shortdesc varchar(500),
    IN _credential_type_id integer,
    IN _credential_detail json,
    IN _key varchar(128),
    IN _user_id integer,
    OUT _return_id integer
)
begin
    declare v_exists integer;
    declare exit handler for 1062 /* Duplicate key*/ SET _return_id = -20;

    if _credential_id = -1 then
        -- 1. this is a new credential definition. insert into table
        insert into dl_credential (
            credential_name,
            credential_shortdesc,
            credential_type_id,
            credential_detail,
            last_change_user_id)
        values (
            _credential_name,
            _credential_shortdesc,
            _credential_type_id,
            aes_encrypt(_credential_detail, _key),
            _user_id
        );
        set _return_id = last_insert_id();

    else
        -- 2. this is an existing credential definition. update short description and details
        update dl_credential
            set credential_detail = aes_encrypt(_credential_detail, _key),
                last_change_user_id = _user_id
        where credential_id = _credential_id;
        set _return_id = _credential_id;

    end if;

end;