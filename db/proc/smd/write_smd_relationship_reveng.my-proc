--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes details of a structural metadata (SMD) relationship between SMD table/view entities
--  into table dl_smd_relationship.
--

create procedure write_smd_relationship_reveng (
    IN _user_id integer,
    IN _database_id integer,
    IN _schema_id integer,
    IN _class_id integer,
    IN _reveng_run_id integer,
    IN _parent_table_view_id integer,
    IN _child_table_view_id integer,
    IN _relationship_name varchar(200),
    IN _relationship_shortdesc varchar(1000),
    OUT _relationship_id integer
)
begin
    declare v_exists integer;
    declare v_create_mode tinyint default 10;

    select count(smd_relationship_name) into v_exists
    from dl_smd_relationship
    where smd_relationship_name = _relationship_name
        and smd_database_id = _database_id
        and smd_schema_id = _schema_id;

    if (v_exists = 0) then
        -- this is a new smd relationship definition
        insert into dl_smd_relationship (
            smd_database_id,
            smd_schema_id,
            class_id,
            create_mode,
            reveng_run_id,
            parent_table_view_id,
            child_table_view_id,
            smd_relationship_name,
            smd_relationship_shortdesc,
            last_change_user_id
        )
        values (
            _database_id,
            _schema_id,
            _class_id,
            v_create_mode,
            _reveng_run_id,
            _parent_table_view_id,
            _child_table_view_id,
            _relationship_name,
            _relationship_shortdesc,
            _user_id
        );
        set _relationship_id = last_insert_id();
    else
        -- the relationship already exists so update the details
        select smd_relationship_id into _relationship_id
        from dl_smd_relationship
        where smd_relationship_name = _relationship_name
            and smd_database_id = _database_id
            and smd_schema_id = _schema_id;

        update dl_smd_relationship
        set reveng_run_id = _reveng_run_id,
            parent_table_view_id = _parent_table_view_id,
            child_table_view_id = _child_table_view_id,
            smd_relationship_shortdesc = _relationship_shortdesc,
            last_change_user_id = _user_id
        where smd_relationship_name = _relationship_name
        and smd_database_id = _database_id
        and smd_schema_id = _schema_id;

    end if;

end;
