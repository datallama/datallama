--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: creates a new SMD Presentation object.
--

create procedure write_smd_presentation(
    IN _user_id             integer,
    IN _object_id           integer,
    IN _class_id            integer,
    IN _object_name         varchar(150),
    IN _object_shortdesc    varchar(500),
    IN _object_config       json,
    OUT _return_id          integer
)
begin
    declare v_return_id integer;
    declare v_object_config json;

    if (_object_config is null) then
        set v_object_config = json_object();
    else
        set v_object_config = _object_config;
    end if;


    if (_object_id < 0) then
        -- this is a new presentation definition so insert into dl_smd_definition
        insert into dl_smd_presentation (
            class_id,
            object_name,
            object_shortdesc,
            object_config,
            last_change_user_id
        )
        values (
            _class_id,
            _object_name,
            _object_shortdesc,
            v_object_config,
            _user_id
        );
        set _return_id = last_insert_id();
    else
        update dl_smd_presentation
        set
            object_name = _object_name,
            object_shortdesc = _object_shortdesc,
            object_config = v_object_config,
            last_change_user_id = _user_id,
            last_change_ts = now()
        where
            object_id = _object_id;

        set _return_id = _object_id;
    end if;

    -- generate the metadata context
    update dl_smd_presentation
    set md_context = generate_smd_presentation_md_context(_return_id)
    where object_id = _return_id;

end;
