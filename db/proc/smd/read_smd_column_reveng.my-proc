--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the list of changes to tables/views after a reverse engineering run
--  The value of mode determines what is returned: 1 = deleted tables/views, 2 = added tables/views

create procedure read_smd_column_reveng (
    IN _database_id integer,
    OUT _latest_reveng_run_id integer
)
begin
    declare v_latest_reveng_run_id integer;
    declare v_previous_reveng_run_id integer;

    -- 1. get latest and previous reverse engineering run Ids
    select max(reveng_run_id) into v_latest_reveng_run_id
    from dl_reveng_run
    where smd_database_id = _database_id;

    set _latest_reveng_run_id = v_latest_reveng_run_id;

    select max(reveng_run_id) into v_previous_reveng_run_id
    from dl_reveng_run
    where smd_database_id = _database_id
        and reveng_run_id != v_latest_reveng_run_id;

    -- 2. get columns that were deleted
    select c.smd_column_id as object_id, c.class_id, c.smd_column_name as object_name,
           s.smd_schema_name, tv.smd_table_view_id as parent_object_id, tv.class_id as parent_class_id,
           tv.smd_table_view_name as parent_object_name, c.reveng_run_id, c.reveng_run_count
    from dl_smd_column c
        join dl_smd_table_view tv on c.smd_table_view_id = tv.smd_table_view_id
        join dl_smd_schema s on tv.smd_schema_id = s.smd_schema_id
    where tv.smd_database_id = _database_id
        and c.reveng_run_id = v_previous_reveng_run_id;

    -- 3. get added that were added
    select c.smd_column_id as object_id, c.class_id, c.smd_column_name as object_name,
           s.smd_schema_name, tv.smd_table_view_id as parent_object_id, tv.class_id as parent_class_id,
           tv.smd_table_view_name as parent_object_name, c.reveng_run_id, c.reveng_run_count
    from dl_smd_column c
        join dl_smd_table_view tv on c.smd_table_view_id = tv.smd_table_view_id
        join dl_smd_schema s on tv.smd_schema_id = s.smd_schema_id
    where tv.smd_database_id = _database_id
        and c.reveng_run_id = v_latest_reveng_run_id
        and c.reveng_run_count = 1;

end;
