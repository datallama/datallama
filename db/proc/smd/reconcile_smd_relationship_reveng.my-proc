--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reconciles the status of a Relationship in the context of the most recent SMD Database reverse engineer run.
--  There are two modes of reconciliation:
--      _reconciliation_action = 2: MATCHED. the deleted relationship identified by _object_id/_class_id is matched to
--          the added relationship identified by _target_object_id/_target_class_id. This means that all associations,
--          comments and custom form data for the deleted relationship are migrated to the added relationship
--      _reconciliation_action = 3: DELETED. the deleted relationship identified by _object_id/_class_id is archived and deleted.
--          All associations, comments and custom form data for the deleted relationship are also archived and deleted.

create procedure reconcile_smd_relationship_reveng (
    IN _object_id integer,
    IN _class_id integer,
    IN _target_object_id integer,
    IN _target_class_id integer,
    IN _reconciliation_action integer
)
begin
    declare v_object_shortdesc varchar(1000);
    declare v_target_shortdesc varchar(1000);
    declare v_migrated_shortdesc varchar(1000);

    -- 1. process a matched reconciliation
    if (_reconciliation_action = 2) then
        -- 1.1 migrate the short description to target relationship
        select smd_relationship_shortdesc into v_object_shortdesc
        from dl_smd_relationship
        where smd_relationship_id = _object_id
            and class_id = _class_id;

        select smd_relationship_shortdesc into v_target_shortdesc
        from dl_smd_relationship
        where smd_relationship_id = _target_object_id
            and class_id = _target_class_id;

        if (length(v_object_shortdesc) > 0) then
            set v_migrated_shortdesc = substr(concat(v_object_shortdesc, ' : ', v_target_shortdesc), 0, 1000);
        else
            set v_migrated_shortdesc = v_target_shortdesc;
        end if;

        update dl_smd_relationship
            set smd_relationship_shortdesc = v_migrated_shortdesc
        where smd_relationship_id = _target_object_id
            and class_id = _target_class_id;

        -- 1.2. migrate associations to target relationship
        update dl_object_association
            set source_object_id = _target_object_id, source_class_id = _target_class_id
        where source_object_id = _object_id
            and source_class_id = _class_id;

        update dl_object_association
            set target_object_id = _target_object_id, target_class_id = _target_class_id
        where target_object_id = _object_id
            and target_class_id = _class_id;

        -- 1.3. migrate comments to target relationship
        update dl_object_comment
            set object_id = _target_object_id, class_id = _target_class_id
        where object_id = _object_id
            and class_id = _class_id;

        -- 1.4. migrate custom form data to target relationship
        update dl_custom_form_data
            set object_id = _target_object_id, class_id = _target_class_id
        where object_id = _object_id
            and class_id = _class_id;

    -- 2. process a deleted reconciliation
    elseif (_reconciliation_action = 3) then
        -- 2.1. migrate associations to target relationship
        delete from dl_object_association
        where source_object_id = _object_id
            and source_class_id = _class_id;

        delete from dl_object_association
        where target_object_id = _object_id
            and target_class_id = _class_id;

        -- 2.2. migrate comments to target relationship
        delete from dl_object_comment
        where object_id = _object_id
            and class_id = _class_id;

        -- 2.3. migrate custom form data to target relationship
        delete from dl_custom_form_data
        where object_id = _object_id
            and class_id = _class_id;
    end if;

    -- 3. archive and delete the relationship definition
    call archive_smd_relationship(_object_id, 'D');
    delete from dl_smd_relationship where smd_relationship_id = _object_id;

end;
