--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
create procedure read_user (
    IN _user_id integer,
    OUT _username varchar(30),
    OUT _firstname varchar(150),
    OUT _lastname varchar(150),
    OUT _enabled integer,      -- JDBC does not recognise boolean out variables
    OUT _email_address varchar(300)
)
begin
    select
        user_login,
        firstname,
        lastname,
        enabled,
        email_address
    into
        _username,
        _firstname,
        _lastname,
        _enabled,
        _email_address
    from dl_user
    where user_id = _user_id;

end;