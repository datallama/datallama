--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--

create procedure write_diagram_arrow(
    IN _user_id integer,
    IN _class_id integer,
    IN _object_id integer,
    IN _source_object_id integer,
    IN _target_object_id integer,
    IN _object_name varchar(150),
    IN _object_shortdesc varchar(1000)
)
begin
    declare v_return_id integer;
    declare v_count_name integer;
    declare v_unique_name varchar(150);
    declare v_count_id integer;

    -- first: check if name already exists, and if it does auto-generate unique name
    select count(_object_name) into v_count_name
    from dl_diagram_arrow
    where object_name = _object_name;

    if v_count_name = 1 then
        select object_id into v_count_id
        from dl_diagram_arrow
        where object_name = _object_name;

        if v_count_id = _object_id then
            set v_unique_name = _object_name;
        else
            -- determine how many auto-generated unique names exist for this name
            set v_unique_name = concat(_object_name, ' (%)');
            select count(object_name) + 1 into v_count_name
            from dl_diagram_arrow
            where object_name like v_unique_name;
            set v_unique_name = concat(_object_name, ' (', v_count_name, ')');
        end if;
    elseif v_count_name > 0 then
        -- second: determine how many auto-generated unique names exist for this name
        set v_unique_name = concat(_object_name, ' (%)');
        select count(object_name) + 1 into v_count_name
        from dl_diagram_arrow
        where object_name like v_unique_name;

        set v_unique_name = concat(_object_name, ' (', v_count_name, ')');
    else
        set v_unique_name = _object_name;
    end if;

    if (_object_id < 0) then
        -- this is a new arrow

        insert into dl_diagram_arrow (
        class_id,
      	source_object_id,
      	target_object_id,
        object_name,
      	object_shortdesc,
      	last_change_user_id
        )
        values (
            _class_id,
            _source_object_id,
            _target_object_id,
            v_unique_name,
            _object_shortdesc,
            _user_id
        );
        set v_return_id = last_insert_id();
    else
        -- this is an update to an existing arrow
        update dl_diagram_arrow set
            last_change_user_id = _user_id,
            object_name = v_unique_name,
            last_change_ts = current_timestamp,
            object_shortdesc = object_shortdesc
        where object_id = _object_id;
        set v_return_id = _object_id;
    end if;

    select v_return_id as returnId;
end;
