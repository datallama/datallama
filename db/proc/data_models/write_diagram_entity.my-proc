--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes details of a diagram entity to the database.
--    diagram entities must have a physical partner for example a datastore
--    in which case the ntt_id must match the datastore_id.

create procedure write_diagram_entity(
    IN _user_id integer,
    IN _class_id integer,
    IN _dgrm_id integer,
    IN _ntt_id integer,
    IN _ntt_x integer,
    IN _ntt_y integer,
    IN _ntt_z integer,
    IN _ntt_height integer,
    IN _ntt_width integer,
    IN _color_defn varchar(500),
    IN _color_source smallint
)
begin
    declare v_count smallint;

    -- check whether the entity has been added to diagram already
    select count(object_id) into v_count
    from dl_diagram_entity
    where dgrm_id = _dgrm_id
    and object_id = _ntt_id
    and class_id = _class_id;

    if (v_count = 0) then
        -- this is a new entity
        insert into dl_diagram_entity (
            last_change_user_id,
            class_id,
            dgrm_id,
            object_id,
            ntt_height,
            ntt_width,
            ntt_x,
            ntt_y,
            ntt_z,
            color_defn,
            color_source
        )
        values (
            _user_id,
            _class_id,
            _dgrm_id,
            _ntt_id,
            _ntt_height,
            _ntt_width,
            _ntt_x,
            _ntt_y,
            _ntt_z,
            _color_defn,
            _color_source
        );
    else
        -- this is an update to an existing entity
        update dl_diagram_entity set
            last_change_user_id = _user_id,
            last_change_ts = current_timestamp,
            ntt_height = _ntt_height,
            ntt_width = _ntt_width,
            ntt_x = _ntt_x,
            ntt_y = _ntt_y,
            ntt_z = _ntt_z,
            color_defn = _color_defn,
            color_source = _color_source
        where dgrm_id = _dgrm_id
        and object_id = _ntt_id
        and class_id = _class_id;
    end if;
end;
