--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes the content of data model to the database or creates a new physical data model if _.
--  The details are inserted into dl_diagram if _dgrm_id = 0 or updated if _dgrm_id > 0.
--
create procedure write_dm_diagram(
    IN _user_id integer,
    IN _dgrm_id integer,
    IN _class_id integer,
    IN _dgrm_name varchar(150),
    IN _dgrm_desc varchar(1000),
    IN _dgrm_prop varchar(4096),
    OUT _return_id integer
)
begin
    declare v_count_name integer;
    declare v_unique_name varchar(150);

    if (_dgrm_id < 0) then
        -- 1. this is a new data model diagram so insert into dl_diagram
        -- 1.1. check if name already exists, and if it does auto-generate unique name
        select count(dgrm_name) into v_count_name
        from dl_diagram
        where dgrm_name = _dgrm_name;

        if v_count_name > 0 then
            -- 1.2. determine how many auto-generated unique names exist for this name
            set v_unique_name = concat(_dgrm_name, ' (%)');
            select count(dgrm_name) + 1 into v_count_name
            from dl_diagram
            where dgrm_name like v_unique_name;

            set v_unique_name = concat(_dgrm_name, ' (', v_count_name, ')');
        else
            set v_unique_name = _dgrm_name;
        end if;

        insert into dl_diagram (
            class_id,
            dgrm_name,
            dgrm_desc,
            dgrm_prop,
            last_change_user_id
        )
        values (
            _class_id,
            v_unique_name,
            _dgrm_desc,
            _dgrm_prop,
            _user_id
        );

        set _return_id = last_insert_id();

        -- 1.3. generate the md_context
        call generate_diagram_md_context(_return_id, 'C');
    else
        -- 2. update diagram summary details
        update dl_diagram
            set dgrm_name = _dgrm_name,
                dgrm_desc = _dgrm_desc,
                last_change_user_id = _user_id
        where dgrm_id = _dgrm_id;

        set _return_id = _dgrm_id;

        -- 2.1. regenerate the md_context
        call generate_diagram_md_context(_return_id, 'U');
    end if;

    select _return_id;

end;
