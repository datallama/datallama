--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: writes the custom form data for an element to the DL database table dl_list_cdg_element.
--

create procedure write_list_cdg_element_custom_form_data(
    IN _user_id integer,
    IN _element_id integer,
    IN _custom_form_data json
)
begin

    update dl_list_cdg_element
    set custom_form_data = _custom_form_data,
        last_change_user_id = _user_id,
        last_change_ts = now()
    where element_id = _element_id;

end;
