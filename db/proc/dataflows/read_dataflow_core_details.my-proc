--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads all data flow definitions, i.e. connectors with meta_class_id = 2.
--

create procedure read_dataflow_core_details(
    IN _dataflow_id integer
)
begin
    declare contact_id integer default 0;

    select
        df.object_id as dataflow_id,
        df.class_id,
        df.object_name as dataflow_name,
        ifnull(df.object_shortdesc, 'None Specified') as dataflow_desc,
        df.source_object_id as dataflow_start_ntt_id,
        n1.class_id as start_ntt_class_id,
        n1.object_name as start_ntt_name,
        df.target_object_id as dataflow_end_ntt_id,
        n2.class_id as end_ntt_class_id,
        n2.object_name as end_ntt_name,
        contact_id as business_contact_id,
        ifnull(df.business_contact, 'None Specified') as business_contact,
        contact_id as technical_contact_id,
        ifnull(df.technical_contact, 'None Specified') as technical_contact,
        ifnull(fd.full_desc, 'No full description has been defined for this object.') as full_desc
    from dl_dataflow df
        join dl_class_hierarchy cl on df.class_id = cl.class_id
        left join dl_full_description fd on df.object_id = fd.object_id and df.class_id = fd.class_id
        join dl_landscape_object n1 on df.source_object_id = n1.object_id
        join dl_landscape_object n2 on df.target_object_id = n2.object_id
    where df.object_id = _dataflow_id;

end;
