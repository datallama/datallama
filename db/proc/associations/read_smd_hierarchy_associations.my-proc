--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads all associations for all elements in an smd hierarchy summary

create procedure read_smd_hierarchy_associations(
    IN _object_id integer   -- object_id of the smd hierarchy summary (i.e. parent_object_id in dl_smd_hierarchy_detail)
)
begin

    -- get the list of associated objects
    select    -- smd hierarchy detail md_object as association source
        hd.object_id,
        hd.class_id,
        oa.assoc_id,
        oa.assoc_shortdesc,
        oa.target_object_id as assoc_object_id,
        oa.target_class_id as assoc_class_id,
        oa.assoc_type_id,
        c.object_name as assoc_object_name,
        c.class_name as assoc_class_name,
        c.md_context as assoc_md_context,
        at.assoc_type_name,
        at.target_phrase as verb_phrase,
        at.source_phrase as reverse_verb_phrase
    from dl_smd_hierarchy_detail hd
        join dl_object_association oa on hd.class_id = oa.source_class_id and hd.object_id = oa.source_object_id
        join dl_class_objects c on oa.target_object_id = c.object_id and oa.target_class_id = c.class_id
        join dl_association_type at on oa.assoc_type_id = at.assoc_type_id
    where hd.parent_object_id = _object_id
    union
    select    -- smd hierarchy detail md_object as association target
        hd.object_id,
        hd.class_id,
        oa.assoc_id,
        oa.assoc_shortdesc,
        oa.source_object_id as assoc_object_id,
        oa.source_class_id as assoc_class_id,
        oa.assoc_type_id,
        c.object_name as assoc_object_name,
        c.class_name as assoc_class_name,
        c.md_context as assoc_md_context,
        at.assoc_type_name,
        at.source_phrase as verb_phrase,
        at.source_phrase as reverse_verb_phrase
    from dl_smd_hierarchy_detail hd
        join dl_object_association oa on hd.class_id = oa.target_class_id and hd.object_id = oa.target_object_id
        join dl_class_objects c on oa.source_object_id = c.object_id and oa.source_class_id = c.class_id
        join dl_association_type at on oa.assoc_type_id = at.assoc_type_id
    where hd.parent_object_id = _object_id;

end;
