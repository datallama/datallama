--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads the outgoing lineage of the object identified by object_id and class_id
--

create procedure read_lineage_outgoing(
  IN _object_id integer,
  IN _class_id integer
)
begin
    declare v_break       integer;

    declare v_lin_object_id           integer;
    declare v_lin_class_id            integer;
    declare v_assoc_id                integer;
    declare v_repeated                tinyint(1);
    declare v_lineage_level           integer;
    declare v_parent_hierarchy_number varchar(100);
    declare v_level_sequence          integer;
    declare v_hierarchy_number        varchar(100);
    declare v_lineage_desc            varchar(500);

    set v_lineage_level = 0;
    set v_parent_hierarchy_number = '';
    set v_level_sequence = 1;
    set v_break = 1;

    -- A temp table to store the lineage association data
    drop table if exists _lineage_set;
    create temporary table _lineage_set (
        lineage_level           integer not null,
        lineage_desc            varchar(500) not null,
        object_id               integer not null,
        class_id                integer not null,
        hierarchy_number        varchar(100) not null,
        md_context              json null,
        repeated                tinyint(1) not null
    );

    -- A temp table to store the status of the objects that have already been searched.
    drop table if exists _working_list;
    create temporary table _working_list(
        object_id               integer not null,
        class_id                integer not null,
        assoc_id                integer not null,
        status                  tinyint(1) not null default 1,
        lineage_level           integer not null,
        lineage_desc            varchar(500) null,
        parent_hierarchy_number varchar(100) not null,
        level_sequence          integer not null
    );

    -- Add the original object to the working list
    insert into _working_list (
        object_id,
        class_id,
        assoc_id,
        lineage_level,
        lineage_desc,
        parent_hierarchy_number,
        level_sequence)
    select
        _object_id,
        _class_id,
        -1,
        v_lineage_level,
        "",
        v_parent_hierarchy_number,
        1;

    -- while there are still objects on the working list that need processing, keep going
    while (v_break > 0) do

        -- select the first un-processed object on the working list
        select
            object_id, class_id, assoc_id, lineage_level, parent_hierarchy_number, level_sequence, lineage_desc
        into
            v_lin_object_id, v_lin_class_id, v_assoc_id, v_lineage_level, v_parent_hierarchy_number, v_level_sequence, v_lineage_desc
        from
            _working_list
        where
            status = 1 limit 1;

        if (v_lineage_level = 0) then
            set v_hierarchy_number = lpad(v_level_sequence, 3, '0');
        else
            set v_hierarchy_number = concat(v_parent_hierarchy_number, '.', lpad(v_level_sequence, 3, '0'));
        end if;

        -- determine if the current object has been added to the _lineage_set or not.
        select count(object_id) from _working_list where object_id = v_lin_object_id and class_id = v_lin_class_id and status = 0 > 0 into v_repeated;

        -- Add the lineage object defined by v_lin_object_id and v_lin_class_id
        insert into _lineage_set (
            lineage_level,
            lineage_desc,
            object_id,
            class_id,
            hierarchy_number,
            repeated)
        select
            v_lineage_level,
            v_lineage_desc,
            v_lin_object_id,
            v_lin_class_id,
            v_hierarchy_number,
            v_repeated;

        -- update the status of the working list
        update _working_list
        set status = 0
        where
            assoc_id = v_assoc_id;

        -- If it's not a repeat, add the source objects from the lineage associations to the working list
        if (v_repeated = false) then

            set @i = 0;
            insert into _working_list (
                object_id,
                class_id,
                assoc_id,
                lineage_level,
                lineage_desc,
                parent_hierarchy_number,
                level_sequence)
            select
                source_object_id,
                source_class_id,
                assoc_id,
                v_lineage_level + 1,
                ifnull(assoc_shortdesc, 'No lineage description provided.') as assoc_shortdesc,
                v_hierarchy_number,
                @i := @i+1
            from dl_object_association
            where
                target_object_id = v_lin_object_id and
                target_class_id = v_lin_class_id and
                assoc_type_id = 999;
        end if;

        select count(object_id) into v_break from _working_list where status = 1;

    end while;

    -- 3. update table _lineage_set with the md_context
    update _lineage_set a2
    join dl_class_objects co
        on a2.object_id = co.object_id
        and a2.class_id = co.class_id
    set a2.md_context = co.md_context;

    select
        lineage_level,
        lineage_desc,
        object_id,
        class_id,
        repeated,
        hierarchy_number,
        md_context
    from _lineage_set
    order by hierarchy_number;

end;
