--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: reads all business ontology md_objects for the purpose of rebuilding the OpenSearch index.
--

create procedure read_ontology_objects_srch ()
begin
    select object_id, class_id, 0 as parent_object_id, 0 as parent_class_id,
            object_name, ifnull(object_shortdesc, '') as object_shortdesc,
            'none' as element_type, md_context, 0 as data_sensitivity_id
    from dl_taxonomy_vw

    union

    select object_id, class_id, parent_object_id, parent_class_id,
        object_name, ifnull(object_shortdesc, '') as object_shortdesc,
        'none' as element_type, md_context, 0 as data_sensitivity_id
    from dl_taxonomy_element_vw

    union

    select object_id, class_id, 0 as parent_object_id, 0 as parent_class_id,
        object_name, ifnull(object_shortdesc, '') as object_shortdesc,
        'none' as element_type, md_context, 0 as data_sensitivity_id
    from dl_list_cdg_vw

    union

    select object_id, class_id, parent_object_id, parent_class_id,
            object_name, ifnull(object_shortdesc, '') as object_shortdesc,
            'none' as element_type, md_context, 0 as data_sensitivity_id
    from dl_list_cdg_element_vw;

end;