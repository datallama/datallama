--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_smd_database_vw is the metadata object standardisation view for dl_smd_database

create view if not exists dl_smd_database_vw
as
    select
        smd_database_id as object_id,
        class_id,
        smd_server_id as parent_object_id,
        0 as parent_class_id,
        logical_database_name as object_name,
        object_guid,
        smd_database_name,
        smd_database_shortdesc as object_shortdesc,
        smd_username,
        smd_password,
        smd_hostname,
        jdbc_driver_family,
        schema_name_list,
        smd_port,
        custom_form_id,
        md_context,
        last_change_ts,
        last_change_user_id
    from dl_smd_database;
