--
-- Copyright 2020 Data Llama FOSS Foundation Incorporated
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--
-- Description: dl_list_cdg_element_vw is the metadata object standardisation view for dl_list_cdg_element
--

create view if not exists dl_list_cdg_element_vw
as
    select
        e.element_id as object_id,
        e.class_id,
        e.list_cdg_id as parent_object_id,
        l.class_id as parent_class_id,
        e.element_name as object_name,
        e.element_shortdesc object_shortdesc,
        e.element_guid as object_guid,
        e.md_context,
        e.last_change_ts,
        e.last_change_user_id
    from dl_list_cdg_element e
        join dl_list_cdg l on e.list_cdg_id = l.list_cdg_id;
