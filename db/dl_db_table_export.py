#!/usr/bin/python3
# 
# deploys a single task 
#

import os, os.path, json
from optparse import OptionParser
import mysql.connector as mdb
from datetime import datetime

class DlTableExporter(object):

    dbConn = ""
    sDBhost = ""
    sDBname = ""
    sDBusr = ""
    sDBpwd = ""
    sDBdeployusr = ""
    sDBdeploypwd = ""
    sSrcBase = ""
    sUtilBase = ""
    sEnvCfgFullPath = ""
    tableName = ""

    '''
        Constructure initialises the source base directory 
    '''
    def __init__(self):
        self.sSrcBase = os.path.dirname(os.path.realpath(__file__))

    '''
        sets the database deploy username. This must be a user with permissions to create and drop users and databases or schema
    '''
    def setDbDeployUser(self, username):
        self.sDbDeployUsr = username

    '''
        sets the database deploy user password.
    '''
    def setDbDeployPassword(self, password):
        self.sDbDeployPwd = password

    '''
        readEnvConfig reads the Database environment settings either from the file specific in argument sEnvFileName or,
        if sEnvFileName is DEFAULT, from the file default_env.json which is assumed to be in the same dir as the execution script.  
    '''
    def readEnvConfig(self, sEnvFileName):
        exitStatus = 0

        if (sEnvFileName.upper() == "DEFAULT"):
            sFileNameFullPath = self.sSrcBase + "/default_env.json"
        else:
            sFileNameFullPath = sEnvFileName

        if not(os.path.exists(sFileNameFullPath)):
            print("ERROR: can't open DB enviroment file %s." % sFileNameFullPath)
            exitStatus = 120

        if exitStatus == 0:
            fh = open(sFileNameFullPath, "r")
            jsonDoc = json.load(fh)
            fh.close()

            try:
                self.sDbHost = jsonDoc["dbhost"]
                self.sDbName = jsonDoc["dbname"]
                self.sDbUsr = jsonDoc["dbusr"]
                self.sDbPwd = jsonDoc["dbpwd"]
                self.sDbDeployUsr = jsonDoc["dbdeployusr"]
                self.sDbDeployPwd = jsonDoc["dbdeploypwd"]
            except KeyError:
                print("ERROR: can't load all DB enviroment settings from file %s." % sFileNameFullPath)
                exitStatus = 122

        if exitStatus == 0:
            print("found environment file")

        # connect to DB
        try:
            self.dbConn = mdb.connect(host=self.sDbHost, user=self.sDbDeployUsr, password=self.sDbDeployPwd, database=self.sDbName)
            print("connected to the database")
        except mdb.Error, e:
            print("ERROR: while connecting to MySQL Server. Error Number [%d]: %s" % (e.args[0], e.args[1]))
            exit(103)

        return exitStatus

    '''
        generate a prime script for the table identified by tableName
    '''
    def generatePrimeScript(self, tableName):
        iExitStatus = 0

        outfile = tableName + "_prime.my-sql"
        fh = open(outfile, 'w')

        print("exporting table %s" % tableName)

        sqlCmd = "select * from " + tableName
        try:
            csr = self.dbConn.cursor()
            csr.execute(sqlCmd)

            # generate the insert clause
            insertClause = "insert into %s (" % tableName
            for colName in csr.column_names:
                if not colName == "last_change_ts":
                    insertClause = insertClause + colName + ","
            insertClause = insertClause[0:len(insertClause) - 1] + ")"
            fh.write(insertClause + "\n")
            fh.write("values" + "\n")

            rows = csr.fetchall()
            rownum = 1
            totalrows = csr.rowcount
            for row in rows:
                valuesClause = "("
                for colValue in row:
                    if colValue == None:
                        valuesClause = valuesClause + 'null,'
                    elif isinstance(colValue, (int,long,float)):
                        valuesClause = valuesClause + str(colValue) + ','
                    elif isinstance(colValue, (str,unicode)):
                        valuesClause = valuesClause + "'" + str(colValue) + "',"

                valuesClause = valuesClause[0:len(valuesClause) - 1] + ")"
                if rownum < totalrows:
                    valuesClause = valuesClause + ","
                else:
                    valuesClause = valuesClause + ";"

                rownum = rownum + 1
                fh.write(valuesClause + "\n")

            csr.close()
            fh.close()
            self.dbConn.close;
        except mdb.Error, e:
            print("ERROR: while exporting data from table %s. Error Number [%d]: %s" % (tableName, e.args[0], e.args[1]))
            iExitStatus = 121

        return iExitStatus


# call as stand alone script
if __name__ == "__main__":

    # parse command line options
    optParser = OptionParser(description="Generates a prime script for the listed table.")
    optParser.add_option("--env", dest="environment", help="specify a run environment for the netlLCrunController")
    optParser.add_option("--password", dest="password", help="specify the password for the database super user")
    optParser.add_option("--table", dest="tablename", help="specify the name of the table to be exported")

    tableExporter = DlTableExporter()

    envFile = "default"
    tableName = ""
    (options, args) = optParser.parse_args()
    if options.environment:
        envFile = options.environment
    if options.password:
        tableExporter.setDbDeployPassword(options.password)
    if options.tablename:
        tableName = options.tablename
    else:
        print("\nERROR: you must specify a table name.")
        exit(10)

    # read configuration file
    result = tableExporter.readEnvConfig(envFile)

    result = tableExporter.generatePrimeScript(tableName)

    ts = datetime.now()    
    if result > 0:
        print("\nERRORS: one or more error encountered during task deploy.  " + ts.strftime("%Y-%m-%d %H:%M:%S"))
    else:
        print("\nSUCCESS: no errors encountered during task deploy.  " + ts.strftime("%Y-%m-%d %H:%M:%S"))
